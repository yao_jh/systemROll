package com.my.business.workflow.workflowdetailperform.dao;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.my.business.workflow.workflowdetailperform.entity.WorkFlowdetailPerform;

/**
 * 步骤执行表dao接口
 * @author  生成器生成
 * @date 2020-09-25 11:10:04
 * */
@Mapper
public interface WorkFlowdetailPerformDao {

	/**
	 * 添加记录
	 * @param workFlowdetailPerform  对象实体
	 * */
	public void insertDataWorkFlowdetailPerform(WorkFlowdetailPerform workFlowdetailPerform);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataWorkFlowdetailPerformOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataWorkFlowdetailPerformMany(String value);
	
	/**
	 * 修改记录
	 * @param workFlowdetailPerform  对象实体
	 * */
	public void updateDataWorkFlowdetailPerform(WorkFlowdetailPerform workFlowdetailPerform);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<WorkFlowdetailPerform> findDataWorkFlowdetailPerformByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataWorkFlowdetailPerformByPageSize();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public WorkFlowdetailPerform findDataWorkFlowdetailPerformByIndocno(@Param("indocno") Long indocno);
    
    public WorkFlowdetailPerform findDataWorkFlowdetailPerformByNodeid(@Param("nodeid") String nodeid);
    
    public WorkFlowdetailPerform findDataWorkFlowdetailPerformByNodeidAndNoOne(@Param("perform_no") String perform_no,@Param("nodeid") String nodeid);
    
    //查询菱形数据
    public WorkFlowdetailPerform findDataWorkFlowdetailPerformByNodeidAndFlowNo(@Param("perform_no") String perform_no,@Param("flow_no") String flow_no);
    
    public List<WorkFlowdetailPerform> findDataWorkFlowdetailPerformByNodeidList(@Param("nodeid") String nodeid);
    
    public List<WorkFlowdetailPerform> findDataWorkFlowdetailPerformByNodeidAndNo(@Param("perform_no") String perform_no,@Param("nodeid") String nodeid);
    
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<WorkFlowdetailPerform> findDataWorkFlowdetailPerform();
	
}
