package com.my.business.workflow.workflowdetailperform.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.workflow.workflowdetailperform.entity.WorkFlowdetailPerform;

import java.util.List;

/**
* 步骤执行表接口服务类
* @author  生成器生成
* @date 2020-09-25 11:10:04
*/
public interface WorkFlowdetailPerformService {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataWorkFlowdetailPerform(String data,Long userId,String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataWorkFlowdetailPerformOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataWorkFlowdetailPerformMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataWorkFlowdetailPerform(String data,Long userId,String sname);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataWorkFlowdetailPerformByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataWorkFlowdetailPerformByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<WorkFlowdetailPerform> findDataWorkFlowdetailPerform();
}
