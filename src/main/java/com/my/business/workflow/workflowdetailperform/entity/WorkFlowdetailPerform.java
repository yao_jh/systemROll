package com.my.business.workflow.workflowdetailperform.entity;

import java.util.List;

import com.my.business.sys.common.entity.BaseEntity;
import com.my.business.workflow.wokrupdatetable.entity.WokrUpdatetable;
import com.my.business.workflow.worklogical.entity.WorkLogical;

/**
* 步骤执行表实体类
* @author  生成器生成
* @date 2020-09-25 11:10:03
*/
public class WorkFlowdetailPerform extends BaseEntity{
		
	private Long indocno;  //主键
	private Long ilinkno;  //关联配置步骤表id
	private String perform_no;  //工作流步骤组编号
	private String nodename;  //步骤名称
	private String nodeid;  //步骤id
	private String flow_no;  //关联工作流编码
	private Long indexs;  //排序号
	private Long step_type;  //步骤类型
	private String nextnodeid;  //下一步骤id
	private String befornodeid;  //上一步骤id
	private String order_name;  //业务名称
	private Long order_id;  //业务主键
	private String smessage;  //推送消息
	private String iurl;  //跳转地址key
	private String surl;  //跳转地址中文显示
	private String event_no;  //事件号
	private Long ifinish;  //是否完成
	private Long ifok;  //是否合格
	private Long iuserorgroup;  //权限类型(人1or组0)
	private Long ugid;  //权限选择id
	private String ugname;  //权限选择名称
	private Long ischeduled;  //是否定时
	private String scheduled_starttime;  //定时开始时间
	private Long scheduled_time;  //定时执行周期
	private Long scheduled_unit;  //定时周期单位
	private Long moveup_time; //提前提醒时间(天)
	
	private List<WokrUpdatetable>  updateList;   //更新表状态
	private List<WorkLogical> logicallist;  //逻辑集合
    
    
	public void setIndocno(Long indocno){
	    this.indocno = indocno;
	}
	public Long getIndocno(){
	    return this.indocno;
	}
	public Long getIlinkno() {
		return ilinkno;
	}
	public void setIlinkno(Long ilinkno) {
		this.ilinkno = ilinkno;
	}
	public Long getIfok() {
		return ifok;
	}
	public void setIfok(Long ifok) {
		this.ifok = ifok;
	}
	public void setPerform_no(String perform_no){
	    this.perform_no = perform_no;
	}
	public String getPerform_no(){
	    return this.perform_no;
	}
	public void setNodename(String nodename){
	    this.nodename = nodename;
	}
	public String getNodename(){
	    return this.nodename;
	}
	public void setNodeid(String nodeid){
	    this.nodeid = nodeid;
	}
	public String getNodeid(){
	    return this.nodeid;
	}
	public void setFlow_no(String flow_no){
	    this.flow_no = flow_no;
	}
	public String getFlow_no(){
	    return this.flow_no;
	}
	public void setIndexs(Long indexs){
	    this.indexs = indexs;
	}
	public Long getIndexs(){
	    return this.indexs;
	}
	public void setStep_type(Long step_type){
	    this.step_type = step_type;
	}
	public Long getStep_type(){
	    return this.step_type;
	}
	public void setNextnodeid(String nextnodeid){
	    this.nextnodeid = nextnodeid;
	}
	public String getNextnodeid(){
	    return this.nextnodeid;
	}
	public void setBefornodeid(String befornodeid){
	    this.befornodeid = befornodeid;
	}
	public String getBefornodeid(){
	    return this.befornodeid;
	}
	public void setOrder_name(String order_name){
	    this.order_name = order_name;
	}
	public String getOrder_name(){
	    return this.order_name;
	}
	public Long getOrder_id() {
		return order_id;
	}
	public void setOrder_id(Long order_id) {
		this.order_id = order_id;
	}
	public void setSmessage(String smessage){
	    this.smessage = smessage;
	}
	public String getSmessage(){
	    return this.smessage;
	}
	public void setIurl(String iurl){
	    this.iurl = iurl;
	}
	public String getIurl(){
	    return this.iurl;
	}
	public void setSurl(String surl){
	    this.surl = surl;
	}
	public String getSurl(){
	    return this.surl;
	}
	public void setEvent_no(String event_no){
	    this.event_no = event_no;
	}
	public String getEvent_no(){
	    return this.event_no;
	}
	public void setIfinish(Long ifinish){
	    this.ifinish = ifinish;
	}
	public Long getIfinish(){
	    return this.ifinish;
	}
	public void setIuserorgroup(Long iuserorgroup){
	    this.iuserorgroup = iuserorgroup;
	}
	public Long getIuserorgroup(){
	    return this.iuserorgroup;
	}
	public void setUgid(Long ugid){
	    this.ugid = ugid;
	}
	public Long getUgid(){
	    return this.ugid;
	}
	public void setUgname(String ugname){
	    this.ugname = ugname;
	}
	public String getUgname(){
	    return this.ugname;
	}
	public void setIscheduled(Long ischeduled){
	    this.ischeduled = ischeduled;
	}
	public Long getIscheduled(){
	    return this.ischeduled;
	}
	public void setScheduled_starttime(String scheduled_starttime){
	    this.scheduled_starttime = scheduled_starttime;
	}
	public String getScheduled_starttime(){
	    return this.scheduled_starttime;
	}
	public void setScheduled_time(Long scheduled_time){
	    this.scheduled_time = scheduled_time;
	}
	public Long getScheduled_time(){
	    return this.scheduled_time;
	}
	public void setScheduled_unit(Long scheduled_unit){
	    this.scheduled_unit = scheduled_unit;
	}
	public Long getScheduled_unit(){
	    return this.scheduled_unit;
	}
	public Long getMoveup_time() {
		return moveup_time;
	}
	public void setMoveup_time(Long moveup_time) {
		this.moveup_time = moveup_time;
	}
	public List<WokrUpdatetable> getUpdateList() {
		return updateList;
	}
	public void setUpdateList(List<WokrUpdatetable> updateList) {
		this.updateList = updateList;
	}
	public List<WorkLogical> getLogicallist() {
		return logicallist;
	}
	public void setLogicallist(List<WorkLogical> logicallist) {
		this.logicallist = logicallist;
	}
}