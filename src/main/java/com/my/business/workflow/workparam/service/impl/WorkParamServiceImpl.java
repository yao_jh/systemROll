package com.my.business.workflow.workparam.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;
import com.my.business.workflow.workparam.dao.WorkParamDao;
import com.my.business.workflow.workparam.entity.JWorkParam;
import com.my.business.workflow.workparam.entity.WorkParam;
import com.my.business.workflow.workparam.service.WorkParamService;
import com.my.business.sys.common.entity.ResultData;

/**
* 工作流参数子表接口具体实现类
* @author  生成器生成
* @date 2020-09-22 14:39:09
*/
@Service
public class WorkParamServiceImpl implements WorkParamService {
	
	@Autowired
	private WorkParamDao workParamDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataWorkParam(String data,Long userId,String sname){
		try{
			JWorkParam jworkParam = JSON.parseObject(data,JWorkParam.class);
    		WorkParam workParam = jworkParam.getWorkParam();
    		CodiUtil.newRecord(userId,sname,workParam);
            workParamDao.insertDataWorkParam(workParam);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataWorkParamOne(Long indocno){
		try {
            workParamDao.deleteDataWorkParamOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataWorkParamMany(String str_id) {
        try {
        	String sql = "delete work_param where indocno in(" + str_id +")";
            workParamDao.deleteDataWorkParamMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataWorkParam(String data,Long userId,String sname){
		try {
			JWorkParam jworkParam = JSON.parseObject(data,JWorkParam.class);
    		WorkParam workParam = jworkParam.getWorkParam();
        	CodiUtil.editRecord(userId,sname,workParam);
            workParamDao.updateDataWorkParam(workParam);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataWorkParamByPage(String data) {
        try {
        	JWorkParam jworkParam = JSON.parseObject(data, JWorkParam.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jworkParam.getPageIndex();
        	Integer pageSize = jworkParam.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jworkParam.getCondition()){
    			jsonObject  = JSON.parseObject(jworkParam.getCondition().toString());
    		}
    
    		List<WorkParam> list = workParamDao.findDataWorkParamByPage((pageIndex-1)*pageSize, pageSize);
    		Integer count = workParamDao.findDataWorkParamByPageSize();
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataWorkParamByIndocno(String data) {
        try {
        	JWorkParam jworkParam = JSON.parseObject(data, JWorkParam.class); 
        	Long indocno = jworkParam.getIndocno();
        	
    		WorkParam workParam = workParamDao.findDataWorkParamByIndocno(indocno);
    		return ResultData.ResultDataSuccess(workParam);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<WorkParam> findDataWorkParam(){
		List<WorkParam> list = workParamDao.findDataWorkParam();
		return list;
	};
}
