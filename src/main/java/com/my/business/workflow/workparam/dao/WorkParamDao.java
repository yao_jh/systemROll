package com.my.business.workflow.workparam.dao;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.my.business.workflow.workparam.entity.WorkParam;

/**
 * 工作流参数子表dao接口
 * @author  生成器生成
 * @date 2020-09-22 14:39:09
 * */
@Mapper
public interface WorkParamDao {

	/**
	 * 添加记录
	 * @param workParam  对象实体
	 * */
	public void insertDataWorkParam(WorkParam workParam);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataWorkParamOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataWorkParamMany(String value);
	
	/**
	 * 修改记录
	 * @param workParam  对象实体
	 * */
	public void updateDataWorkParam(WorkParam workParam);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<WorkParam> findDataWorkParamByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);
    
    /**
     * 根据工作流id查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<WorkParam> findDataWorkParamAll(@Param("flow_id") Long flow_id);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataWorkParamByPageSize();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public WorkParam findDataWorkParamByIndocno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<WorkParam> findDataWorkParam();
	
}
