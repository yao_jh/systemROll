package com.my.business.workflow.workparam.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.workflow.workparam.entity.WorkParam;

import java.util.List;

/**
* 工作流参数子表接口服务类
* @author  生成器生成
* @date 2020-09-22 14:39:09
*/
public interface WorkParamService {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataWorkParam(String data,Long userId,String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataWorkParamOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataWorkParamMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataWorkParam(String data,Long userId,String sname);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataWorkParamByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataWorkParamByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<WorkParam> findDataWorkParam();
}
