package com.my.business.workflow.workparam.entity;

import java.util.Date;
import com.my.business.sys.common.entity.BaseEntity;

/**
* 工作流参数子表实体类
* @author  生成器生成
* @date 2020-09-22 14:39:09
*/
public class WorkParam extends BaseEntity{
		
	private Long indocno;  //主键
	private String flow_no;  //主表编码
	private String field_no;  //字段编码
	private String field_name;  //字段名称
    
    
	public void setIndocno(Long indocno){
	    this.indocno = indocno;
	}
	public Long getIndocno(){
	    return this.indocno;
	}
	public void setFlow_no(String flow_no){
	    this.flow_no = flow_no;
	}
	public String getFlow_no(){
	    return this.flow_no;
	}
	public void setField_no(String field_no){
	    this.field_no = field_no;
	}
	public String getField_no(){
	    return this.field_no;
	}
	public void setField_name(String field_name){
	    this.field_name = field_name;
	}
	public String getField_name(){
	    return this.field_name;
	}

    
}