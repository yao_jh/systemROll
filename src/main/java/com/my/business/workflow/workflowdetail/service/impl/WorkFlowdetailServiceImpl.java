package com.my.business.workflow.workflowdetail.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.workflow.workflowdetail.dao.WorkFlowdetailDao;
import com.my.business.workflow.workflowdetail.entity.JWorkFlowdetail;
import com.my.business.workflow.workflowdetail.entity.WorkFlowdetail;
import com.my.business.workflow.workflowdetail.service.WorkFlowdetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 步骤表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-25 11:10:16
 */
@Service
public class WorkFlowdetailServiceImpl implements WorkFlowdetailService {

    @Autowired
    private WorkFlowdetailDao workFlowdetailDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataWorkFlowdetail(String data, Long userId, String sname) {
        try {
            JWorkFlowdetail jworkFlowdetail = JSON.parseObject(data, JWorkFlowdetail.class);
            WorkFlowdetail workFlowdetail = jworkFlowdetail.getWorkFlowdetail();
            CodiUtil.newRecord(userId, sname, workFlowdetail);
            workFlowdetailDao.insertDataWorkFlowdetail(workFlowdetail);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataWorkFlowdetailOne(Long indocno) {
        try {
            workFlowdetailDao.deleteDataWorkFlowdetailOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataWorkFlowdetailMany(String str_id) {
        try {
            String sql = "delete work_flowdetail where indocno in(" + str_id + ")";
            workFlowdetailDao.deleteDataWorkFlowdetailMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataWorkFlowdetail(String data, Long userId, String sname) {
        try {
            JWorkFlowdetail jworkFlowdetail = JSON.parseObject(data, JWorkFlowdetail.class);
            WorkFlowdetail workFlowdetail = jworkFlowdetail.getWorkFlowdetail();
            CodiUtil.editRecord(userId, sname, workFlowdetail);
            workFlowdetailDao.updateDataWorkFlowdetail(workFlowdetail);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataWorkFlowdetailByPage(String data) {
        try {
            JWorkFlowdetail jworkFlowdetail = JSON.parseObject(data, JWorkFlowdetail.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jworkFlowdetail.getPageIndex();
            Integer pageSize = jworkFlowdetail.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jworkFlowdetail.getCondition()) {
                jsonObject = JSON.parseObject(jworkFlowdetail.getCondition().toString());
            }

            List<WorkFlowdetail> list = workFlowdetailDao.findDataWorkFlowdetailByPage((pageIndex - 1) * pageSize, pageSize);
            Integer count = workFlowdetailDao.findDataWorkFlowdetailByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataWorkFlowdetailByIndocno(String data) {
        try {
            JWorkFlowdetail jworkFlowdetail = JSON.parseObject(data, JWorkFlowdetail.class);
            Long indocno = jworkFlowdetail.getIndocno();

            WorkFlowdetail workFlowdetail = workFlowdetailDao.findDataWorkFlowdetailByIndocno(indocno);
            return ResultData.ResultDataSuccess(workFlowdetail);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<WorkFlowdetail> findDataWorkFlowdetail() {
        List<WorkFlowdetail> list = workFlowdetailDao.findDataWorkFlowdetail();
        return list;
    }

    public String nextDate(String scheduled_starttime, Long scheduled_time, Long scheduled_unit, Long moveup_time) {
        String nextDate = null;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date predate = df.parse(scheduled_starttime);
            c.setTime(predate);
            if (scheduled_unit == 1L) {
                //年
                c.add(Calendar.YEAR, scheduled_time.intValue());
            } else if (scheduled_unit == 2L) {
                //月
                c.add(Calendar.MONTH, scheduled_time.intValue());
            } else if (scheduled_unit == 3L) {
                //日
                c.add(Calendar.DATE, scheduled_time.intValue());
            }
            c.add(Calendar.DATE, moveup_time.intValue() * (-1));
            nextDate = df.format(c.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return nextDate;
    }
}
