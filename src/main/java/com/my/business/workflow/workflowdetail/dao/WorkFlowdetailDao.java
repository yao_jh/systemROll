package com.my.business.workflow.workflowdetail.dao;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.my.business.workflow.workflowdetail.entity.WorkFlowdetail;

/**
 * 步骤表dao接口
 * @author  生成器生成
 * @date 2020-09-25 11:10:16
 * */
@Mapper
public interface WorkFlowdetailDao {

	/**
	 * 添加记录
	 * @param workFlowdetail  对象实体
	 * */
	public void insertDataWorkFlowdetail(WorkFlowdetail workFlowdetail);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataWorkFlowdetailOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataWorkFlowdetailMany(String value);
	
	/**
	 * 修改记录
	 * @param workFlowdetail  对象实体
	 * */
	public void updateDataWorkFlowdetail(WorkFlowdetail workFlowdetail);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<WorkFlowdetail> findDataWorkFlowdetailByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataWorkFlowdetailByPageSize();
    
    /**
     * 查询最大主键值
     * @return 对象数据集合
     */
    public Long findMaxIndocno();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public WorkFlowdetail findDataWorkFlowdetailByIndocno(@Param("indocno") Long indocno);
    
    /***
     * 根据步骤号nodeid查询信息
     * @param indocno 用户id
     * @return
     */
    public WorkFlowdetail findDataWorkFlowdetailByNodeid(@Param("nodeid") String nodeid);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<WorkFlowdetail> findDataWorkFlowdetail();
	
}
