package com.my.business.workflow.flowhistory.dao;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.my.business.workflow.flowhistory.entity.FlowHistory;

/**
 * 工作流历史表dao接口
 * @author  生成器生成
 * @date 2020-09-07 10:37:31
 * */
@Mapper
public interface FlowHistoryDao {

	/**
	 * 添加记录
	 * @param flowHistory  对象实体
	 * */
	public void insertDataFlowHistory(FlowHistory flowHistory);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataFlowHistoryOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataFlowHistoryMany(String value);
	
	/**
	 * 修改记录
	 * @param flowHistory  对象实体
	 * */
	public void updateDataFlowHistory(FlowHistory flowHistory);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<FlowHistory> findDataFlowHistoryByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataFlowHistoryByPageSize();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public FlowHistory findDataFlowHistoryByIndocno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<FlowHistory> findDataFlowHistory();
	
}
