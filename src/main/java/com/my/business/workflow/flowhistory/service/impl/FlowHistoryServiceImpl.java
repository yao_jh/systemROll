package com.my.business.workflow.flowhistory.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;
import com.my.business.workflow.flowhistory.dao.FlowHistoryDao;
import com.my.business.workflow.flowhistory.entity.FlowHistory;
import com.my.business.workflow.flowhistory.entity.JFlowHistory;
import com.my.business.workflow.flowhistory.service.FlowHistoryService;
import com.my.business.sys.common.entity.ResultData;

/**
* 工作流历史表接口具体实现类
* @author  生成器生成
* @date 2020-09-07 10:37:31
*/
@Service
public class FlowHistoryServiceImpl implements FlowHistoryService {
	
	@Autowired
	private FlowHistoryDao flowHistoryDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataFlowHistory(String data,Long userId,String sname){
		try{
			JFlowHistory jflowHistory = JSON.parseObject(data,JFlowHistory.class);
    		FlowHistory flowHistory = jflowHistory.getFlowHistory();
    		CodiUtil.newRecord(userId,sname,flowHistory);
            flowHistoryDao.insertDataFlowHistory(flowHistory);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataFlowHistoryOne(Long indocno){
		try {
            flowHistoryDao.deleteDataFlowHistoryOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataFlowHistoryMany(String str_id) {
        try {
        	String sql = "delete flow_history where indocno in(" + str_id +")";
            flowHistoryDao.deleteDataFlowHistoryMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataFlowHistory(String data,Long userId,String sname){
		try {
			JFlowHistory jflowHistory = JSON.parseObject(data,JFlowHistory.class);
    		FlowHistory flowHistory = jflowHistory.getFlowHistory();
        	CodiUtil.editRecord(userId,sname,flowHistory);
            flowHistoryDao.updateDataFlowHistory(flowHistory);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataFlowHistoryByPage(String data) {
        try {
        	JFlowHistory jflowHistory = JSON.parseObject(data, JFlowHistory.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jflowHistory.getPageIndex();
        	Integer pageSize = jflowHistory.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jflowHistory.getCondition()){
    			jsonObject  = JSON.parseObject(jflowHistory.getCondition().toString());
    		}
    
    		List<FlowHistory> list = flowHistoryDao.findDataFlowHistoryByPage((pageIndex-1)*pageSize, pageSize);
    		Integer count = flowHistoryDao.findDataFlowHistoryByPageSize();
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataFlowHistoryByIndocno(String data) {
        try {
        	JFlowHistory jflowHistory = JSON.parseObject(data, JFlowHistory.class); 
        	Long indocno = jflowHistory.getIndocno();
        	
    		FlowHistory flowHistory = flowHistoryDao.findDataFlowHistoryByIndocno(indocno);
    		return ResultData.ResultDataSuccess(flowHistory);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<FlowHistory> findDataFlowHistory(){
		List<FlowHistory> list = flowHistoryDao.findDataFlowHistory();
		return list;
	};
}
