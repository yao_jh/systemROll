package com.my.business.workflow.flowhistory.entity;

import java.util.Date;
import com.my.business.sys.common.entity.BaseEntity;

/**
* 工作流历史表实体类
* @author  生成器生成
* @date 2020-09-07 10:37:30
*/
public class FlowHistory extends BaseEntity{
		
	private Long indocno;  //主键
	private String nodename;  //步骤名称
	private String username;  //操作人
	private String smessage;  //执行信息
    
    
	public void setIndocno(Long indocno){
	    this.indocno = indocno;
	}
	public Long getIndocno(){
	    return this.indocno;
	}
	public void setNodename(String nodename){
	    this.nodename = nodename;
	}
	public String getNodename(){
	    return this.nodename;
	}
	public void setUsername(String username){
	    this.username = username;
	}
	public String getUsername(){
	    return this.username;
	}
	public void setSmessage(String smessage){
	    this.smessage = smessage;
	}
	public String getSmessage(){
	    return this.smessage;
	}

    
}