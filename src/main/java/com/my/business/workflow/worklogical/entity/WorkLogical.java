package com.my.business.workflow.worklogical.entity;

import java.util.Date;
import com.my.business.sys.common.entity.BaseEntity;

/**
* 逻辑运算配置表实体类
* @author  生成器生成
* @date 2020-09-25 11:11:15
*/
public class WorkLogical extends BaseEntity{
		
	private Long indocno;  //主键
	private Long ilinkno;  //外键
	private Long isection;  //是否区间
	private Double imax;  //最大值
	private Double imin;  //最小值
	private Long ilogical;  //逻辑运算符
	private Double icontrast;  //逻辑操作对比值
	private String ifield;  //对比字段列名
	private String sfield;  //对比字段注释
	private String nextnodeid;  //下一步骤id
	private String nextnodename;  //下一步骤名称
	private Long icustom;  //是否自定义配置（是1or否0）
	private String customkey;  //自定义算法地址
	private String customvalue;  //自定义算法中文名称
    
    
	public void setIndocno(Long indocno){
	    this.indocno = indocno;
	}
	public Long getIndocno(){
	    return this.indocno;
	}
	public Long getIlinkno() {
		return ilinkno;
	}
	public void setIlinkno(Long ilinkno) {
		this.ilinkno = ilinkno;
	}
	public void setIsection(Long isection){
	    this.isection = isection;
	}
	public Long getIsection(){
	    return this.isection;
	}
	public void setImax(Double imax){
	    this.imax = imax;
	}
	public Double getImax(){
	    return this.imax;
	}
	public void setImin(Double imin){
	    this.imin = imin;
	}
	public Double getImin(){
	    return this.imin;
	}
	public void setIlogical(Long ilogical){
	    this.ilogical = ilogical;
	}
	public Long getIlogical(){
	    return this.ilogical;
	}
	public void setIcontrast(Double icontrast){
	    this.icontrast = icontrast;
	}
	public Double getIcontrast(){
	    return this.icontrast;
	}
	public void setIfield(String ifield){
	    this.ifield = ifield;
	}
	public String getIfield(){
	    return this.ifield;
	}
	public void setSfield(String sfield){
	    this.sfield = sfield;
	}
	public String getSfield(){
	    return this.sfield;
	}
	public void setNextnodeid(String nextnodeid){
	    this.nextnodeid = nextnodeid;
	}
	public String getNextnodeid(){
	    return this.nextnodeid;
	}
	public void setNextnodename(String nextnodename){
	    this.nextnodename = nextnodename;
	}
	public String getNextnodename(){
	    return this.nextnodename;
	}
	public void setIcustom(Long icustom){
	    this.icustom = icustom;
	}
	public Long getIcustom(){
	    return this.icustom;
	}
	public void setCustomkey(String customkey){
	    this.customkey = customkey;
	}
	public String getCustomkey(){
	    return this.customkey;
	}
	public void setCustomvalue(String customvalue){
	    this.customvalue = customvalue;
	}
	public String getCustomvalue(){
	    return this.customvalue;
	}

    
}