package com.my.business.workflow.worklogical.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;
import com.my.business.workflow.worklogical.dao.WorkLogicalDao;
import com.my.business.workflow.worklogical.entity.JWorkLogical;
import com.my.business.workflow.worklogical.entity.WorkLogical;
import com.my.business.workflow.worklogical.service.WorkLogicalService;
import com.my.business.sys.common.entity.ResultData;

/**
* 逻辑运算配置表接口具体实现类
* @author  生成器生成
* @date 2020-09-25 11:11:16
*/
@Service
public class WorkLogicalServiceImpl implements WorkLogicalService {
	
	@Autowired
	private WorkLogicalDao workLogicalDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataWorkLogical(String data,Long userId,String sname){
		try{
			JWorkLogical jworkLogical = JSON.parseObject(data,JWorkLogical.class);
    		WorkLogical workLogical = jworkLogical.getWorkLogical();
    		CodiUtil.newRecord(userId,sname,workLogical);
            workLogicalDao.insertDataWorkLogical(workLogical);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataWorkLogicalOne(Long indocno){
		try {
            workLogicalDao.deleteDataWorkLogicalOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataWorkLogicalMany(String str_id) {
        try {
        	String sql = "delete work_logical where indocno in(" + str_id +")";
            workLogicalDao.deleteDataWorkLogicalMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataWorkLogical(String data,Long userId,String sname){
		try {
			JWorkLogical jworkLogical = JSON.parseObject(data,JWorkLogical.class);
    		WorkLogical workLogical = jworkLogical.getWorkLogical();
        	CodiUtil.editRecord(userId,sname,workLogical);
            workLogicalDao.updateDataWorkLogical(workLogical);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataWorkLogicalByPage(String data) {
        try {
        	JWorkLogical jworkLogical = JSON.parseObject(data, JWorkLogical.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jworkLogical.getPageIndex();
        	Integer pageSize = jworkLogical.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jworkLogical.getCondition()){
    			jsonObject  = JSON.parseObject(jworkLogical.getCondition().toString());
    		}
    
    		List<WorkLogical> list = workLogicalDao.findDataWorkLogicalByPage((pageIndex-1)*pageSize, pageSize);
    		Integer count = workLogicalDao.findDataWorkLogicalByPageSize();
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataWorkLogicalByIndocno(String data) {
        try {
        	JWorkLogical jworkLogical = JSON.parseObject(data, JWorkLogical.class); 
        	Long indocno = jworkLogical.getIndocno();
        	
    		WorkLogical workLogical = workLogicalDao.findDataWorkLogicalByIndocno(indocno);
    		return ResultData.ResultDataSuccess(workLogical);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<WorkLogical> findDataWorkLogical(){
		List<WorkLogical> list = workLogicalDao.findDataWorkLogical();
		return list;
	};
}
