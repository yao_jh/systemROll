package com.my.business.workflow.worklogical.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.workflow.worklogical.entity.WorkLogical;

import java.util.List;

/**
* 逻辑运算配置表接口服务类
* @author  生成器生成
* @date 2020-09-25 11:11:16
*/
public interface WorkLogicalService {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataWorkLogical(String data,Long userId,String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataWorkLogicalOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataWorkLogicalMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataWorkLogical(String data,Long userId,String sname);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataWorkLogicalByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataWorkLogicalByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<WorkLogical> findDataWorkLogical();
}
