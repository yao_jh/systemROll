package com.my.business.workflow.worklogical.dao;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.my.business.workflow.worklogical.entity.WorkLogical;

/**
 * 逻辑运算配置表dao接口
 * @author  生成器生成
 * @date 2020-09-25 11:11:16
 * */
@Mapper
public interface WorkLogicalDao {

	/**
	 * 添加记录
	 * @param workLogical  对象实体
	 * */
	public void insertDataWorkLogical(WorkLogical workLogical);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataWorkLogicalOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataWorkLogicalMany(String value);
	
	/**
	 * 修改记录
	 * @param workLogical  对象实体
	 * */
	public void updateDataWorkLogical(WorkLogical workLogical);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<WorkLogical> findDataWorkLogicalByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataWorkLogicalByPageSize();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public WorkLogical findDataWorkLogicalByIndocno(@Param("indocno") Long indocno);
    
    public List<WorkLogical> findDataWorkLogicalByIlinkno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<WorkLogical> findDataWorkLogical();
	
}
