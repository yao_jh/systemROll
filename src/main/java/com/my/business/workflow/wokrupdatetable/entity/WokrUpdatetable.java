package com.my.business.workflow.wokrupdatetable.entity;

import java.util.Date;
import com.my.business.sys.common.entity.BaseEntity;

/**
* 步骤更新状态表实体类
* @author  生成器生成
* @date 2020-09-25 11:10:47
*/
public class WokrUpdatetable extends BaseEntity{
		
	private Long indocno;  //主键
	private Long ilinkno;  //外键
	private String table_name;  //表名
	private String field_name;  //字段名称
	private String field_no;  //字段编码(对应表中的字段)
	private String update_value;  //改变后的值
    
    
	public void setIndocno(Long indocno){
	    this.indocno = indocno;
	}
	public Long getIndocno(){
	    return this.indocno;
	}
	public void setIlinkno(Long ilinkno){
	    this.ilinkno = ilinkno;
	}
	public Long getIlinkno(){
	    return this.ilinkno;
	}
	public void setTable_name(String table_name){
	    this.table_name = table_name;
	}
	public String getTable_name(){
	    return this.table_name;
	}
	public void setField_name(String field_name){
	    this.field_name = field_name;
	}
	public String getField_name(){
	    return this.field_name;
	}
	public void setField_no(String field_no){
	    this.field_no = field_no;
	}
	public String getField_no(){
	    return this.field_no;
	}
	public void setUpdate_value(String update_value){
	    this.update_value = update_value;
	}
	public String getUpdate_value(){
	    return this.update_value;
	}

    
}