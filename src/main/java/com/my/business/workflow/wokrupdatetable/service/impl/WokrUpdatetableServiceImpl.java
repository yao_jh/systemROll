package com.my.business.workflow.wokrupdatetable.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;
import com.my.business.workflow.wokrupdatetable.dao.WokrUpdatetableDao;
import com.my.business.workflow.wokrupdatetable.entity.JWokrUpdatetable;
import com.my.business.workflow.wokrupdatetable.entity.WokrUpdatetable;
import com.my.business.workflow.wokrupdatetable.service.WokrUpdatetableService;
import com.my.business.sys.common.entity.ResultData;

/**
* 步骤更新状态表接口具体实现类
* @author  生成器生成
* @date 2020-09-25 11:10:47
*/
@Service
public class WokrUpdatetableServiceImpl implements WokrUpdatetableService {
	
	@Autowired
	private WokrUpdatetableDao wokrUpdatetableDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataWokrUpdatetable(String data,Long userId,String sname){
		try{
			JWokrUpdatetable jwokrUpdatetable = JSON.parseObject(data,JWokrUpdatetable.class);
    		WokrUpdatetable wokrUpdatetable = jwokrUpdatetable.getWokrUpdatetable();
    		CodiUtil.newRecord(userId,sname,wokrUpdatetable);
            wokrUpdatetableDao.insertDataWokrUpdatetable(wokrUpdatetable);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataWokrUpdatetableOne(Long indocno){
		try {
            wokrUpdatetableDao.deleteDataWokrUpdatetableOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataWokrUpdatetableMany(String str_id) {
        try {
        	String sql = "delete wokr_updatetable where indocno in(" + str_id +")";
            wokrUpdatetableDao.deleteDataWokrUpdatetableMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataWokrUpdatetable(String data,Long userId,String sname){
		try {
			JWokrUpdatetable jwokrUpdatetable = JSON.parseObject(data,JWokrUpdatetable.class);
    		WokrUpdatetable wokrUpdatetable = jwokrUpdatetable.getWokrUpdatetable();
        	CodiUtil.editRecord(userId,sname,wokrUpdatetable);
            wokrUpdatetableDao.updateDataWokrUpdatetable(wokrUpdatetable);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataWokrUpdatetableByPage(String data) {
        try {
        	JWokrUpdatetable jwokrUpdatetable = JSON.parseObject(data, JWokrUpdatetable.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jwokrUpdatetable.getPageIndex();
        	Integer pageSize = jwokrUpdatetable.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jwokrUpdatetable.getCondition()){
    			jsonObject  = JSON.parseObject(jwokrUpdatetable.getCondition().toString());
    		}
    
    		List<WokrUpdatetable> list = wokrUpdatetableDao.findDataWokrUpdatetableByPage((pageIndex-1)*pageSize, pageSize);
    		Integer count = wokrUpdatetableDao.findDataWokrUpdatetableByPageSize();
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataWokrUpdatetableByIndocno(String data) {
        try {
        	JWokrUpdatetable jwokrUpdatetable = JSON.parseObject(data, JWokrUpdatetable.class); 
        	Long indocno = jwokrUpdatetable.getIndocno();
        	
    		WokrUpdatetable wokrUpdatetable = wokrUpdatetableDao.findDataWokrUpdatetableByIndocno(indocno);
    		return ResultData.ResultDataSuccess(wokrUpdatetable);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<WokrUpdatetable> findDataWokrUpdatetable(){
		List<WokrUpdatetable> list = wokrUpdatetableDao.findDataWokrUpdatetable();
		return list;
	};
}
