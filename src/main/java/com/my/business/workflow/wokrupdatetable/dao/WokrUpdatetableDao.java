package com.my.business.workflow.wokrupdatetable.dao;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.my.business.workflow.wokrupdatetable.entity.WokrUpdatetable;

/**
 * 步骤更新状态表dao接口
 * @author  生成器生成
 * @date 2020-09-25 11:10:47
 * */
@Mapper
public interface WokrUpdatetableDao {

	/**
	 * 添加记录
	 * @param wokrUpdatetable  对象实体
	 * */
	public void insertDataWokrUpdatetable(WokrUpdatetable wokrUpdatetable);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataWokrUpdatetableOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataWokrUpdatetableMany(String value);
	
	/**
	 * 修改记录
	 * @param wokrUpdatetable  对象实体
	 * */
	public void updateDataWokrUpdatetable(WokrUpdatetable wokrUpdatetable);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<WokrUpdatetable> findDataWokrUpdatetableByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataWokrUpdatetableByPageSize();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public WokrUpdatetable findDataWokrUpdatetableByIndocno(@Param("indocno") Long indocno);
    
    public List<WokrUpdatetable> findDataWokrUpdatetableByIlinkno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<WokrUpdatetable> findDataWokrUpdatetable();
	
}
