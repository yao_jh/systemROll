package com.my.business.workflow.workflow.dao;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.my.business.workflow.workflow.entity.WorkFlow;

/**
 * 工作流主表dao接口
 * @author  生成器生成
 * @date 2020-09-02 09:13:26
 * */
@Mapper
public interface WorkFlowDao {

	/**
	 * 添加记录
	 * @param workFlow  对象实体
	 * */
	public void insertDataWorkFlow(WorkFlow workFlow);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataWorkFlowOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataWorkFlowMany(String value);
	
	/**
	 * 修改记录
	 * @param workFlow  对象实体
	 * */
	public void updateDataWorkFlow(WorkFlow workFlow);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<WorkFlow> findDataWorkFlowByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataWorkFlowByPageSize();
    
    /***
     * 根据流程编码查询信息
     * @param flow_no 流程编码
     * @return
     */
    public WorkFlow findDataWorkFlowByNo(@Param("flow_no") String flow_no);
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public WorkFlow findDataWorkFlowByIndocno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<WorkFlow> findDataWorkFlow();
	
}
