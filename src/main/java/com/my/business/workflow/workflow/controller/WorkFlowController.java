package com.my.business.workflow.workflow.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.my.business.sys.common.entity.JCommon;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import com.my.business.sys.common.entity.ResultData;
import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.my.business.util.CodiUtil;
import com.my.business.workflow.workflow.entity.FlowJsonEntity;
import com.my.business.workflow.workflow.entity.JPushNewEntity;
import com.my.business.workflow.workflow.entity.JWorkFlow;
import com.my.business.workflow.workflow.entity.WorkFlow;
import com.my.business.workflow.workflow.service.WorkFlowService;

import org.springframework.web.bind.annotation.RequestHeader;


/**
* 工作流主表控制器层
* @author  生成器生成
* @date 2020-09-02 09:13:27
*/
@RestController
@RequestMapping("/workFlow")
public class WorkFlowController {
	
	@Value("${moxue_flow_no}")
    private String moxue_flow_no;
	
	@Autowired
	private WorkFlowService workFlowService;
	
	/**
	 * 直接发推送消息
	 * */
	@CrossOrigin
	@RequestMapping(value={"/pushnew"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData pushnew(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken) throws Exception{
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
    	JPushNewEntity entity = JSON.parseObject(data,JPushNewEntity.class);
    	
    	return workFlowService.toPushNew(entity.getArea_name(),entity.getSmessage(), entity.getModular_no(), entity.getGetUserid(), entity.getGetUser(), userId, sname);
	};
	
	
	/**
	 * 轧机发磨辊间消息
	 * */
	@CrossOrigin
	@RequestMapping(value={"/togr"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData togr(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken) throws Exception{
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
    	FlowJsonEntity flowentity = JSON.parseObject(data,FlowJsonEntity.class);
    	
    	return workFlowService.toGr(flowentity.getIndocno(), userId, sname);
	};
	
	
	/**
	 * 发起冷却测试
	 * @param userId 用户id
     * @param sname 用户姓名
	 * */
	@CrossOrigin
	@RequestMapping(value={"/lq"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData stLq(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken) throws Exception{
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
    	FlowJsonEntity flowentity = JSON.parseObject(data,FlowJsonEntity.class);
    	
    	List<Map<String,String>> list = flowentity.getList();
    	Map<String,String> map = new HashMap<String,String>();
    	if(null != list && list.size() > 0) {
    		map = CodiUtil.toMap(list);
    	}
    	return workFlowService.startWorkFlowLq(map);
	};
	
	/**
	 * 发起
	 * @param userId 用户id
     * @param sname 用户姓名
	 * */
	@CrossOrigin
	@RequestMapping(value={"/startFlow"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData startFlow(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken) throws Exception{
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
    	FlowJsonEntity flowentity = JSON.parseObject(data,FlowJsonEntity.class);
    	
    	List<Map<String,String>> list = flowentity.getList();
    	Map<String,String> map = new HashMap<String,String>();
    	if(null != list && list.size() > 0) {
    		map = CodiUtil.toMap(list);
    	}
    	
    	if(flowentity.getType().equals("xg")) {   //新辊发起
    		return workFlowService.startWorkFlow(map,flowentity.getFlow_no(),userId,CodiUtil.returnLm(sname));
    	}else if(flowentity.getType().equals("mx")) { //磨削
    		return workFlowService.startWorkFlowMx(map,flowentity.getFlow_no(),userId,CodiUtil.returnLm(sname));
    	}
    	return ResultData.ResultDataFaultSelf("没有发现该工作流类型,请进行配置", null); 
//    	if(flowentity.getFlow_no().equals(moxue_flow_no)) {
//    		return workFlowService.startWorkFlowMx(map,flowentity.getFlow_no(),userId,CodiUtil.returnLm(sname));
//    	}else {
//    		return workFlowService.startWorkFlow(map,flowentity.getFlow_no(),userId,CodiUtil.returnLm(sname));
//    	}
	};
	
	/**
	 * 下一步
	 * @param userId 用户id
     * @param sname 用户姓名
	 * */
	@CrossOrigin
	@RequestMapping(value={"/nextFlow"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData nextFlow(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken) throws Exception{
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
    	FlowJsonEntity flowentity = JSON.parseObject(data,FlowJsonEntity.class);
    	
    	List<Map<String,String>> list = flowentity.getList();
    	Map<String,String> map = new HashMap<String,String>();
    	if(null != list && list.size() > 0) {
    		map = CodiUtil.toMap(list);
    	}
    	if(StringUtils.isEmpty(flowentity.getType())) {
    		return workFlowService.nextWorkFlow(flowentity.getIndocno(),flowentity.getFlow_id(),map, flowentity.getPerform_no(),flowentity.getNodeid(),userId,CodiUtil.returnLm(sname));
    	}else {
    		if(flowentity.getType().equals("mxyd")) {
        		return workFlowService.nextWorkFlowMx(flowentity.getIndocno(),flowentity.getFlow_id(),map, flowentity.getPerform_no(),flowentity.getNodeid(),userId,CodiUtil.returnLm(sname));
        	}
    	}
		return null;
	};
	
	
	/**
	 * 直接生成推送消息
	 * @param userId 用户id
     * @param sname 用户姓名
	 * */
//	@CrossOrigin
//	@RequestMapping(value={"/createnew"}, method=RequestMethod.POST)
//	@ResponseBody
//	public ResultData createPushNew(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken) throws Exception{
//		JCommon common = JSON.parseObject(loginToken,JCommon.class);
//    	String sname = common.getSname();
//    	Long userId = common.getUserId();
//    	FlowJsonEntity flowentity = JSON.parseObject(data,FlowJsonEntity.class);
//    	
//    	List<Map<String,String>> list = flowentity.getList();
//    	Map<String,String> map = new HashMap<String,String>();
//    	if(null != list && list.size() > 0) {
//    		map = CodiUtil.toMap(list);
//    	}
//    	return workFlowService.createPushNewAll(flowentity.getFlow_id(), map, flowentity.getPerform_no(), flowentity.getNodeid(), userId, sname);
//	};
	
	
	
	/**
	 * 添加记录
	 * @param userId 用户id
     * @param sname 用户姓名
	 * @throws Exception 
	 * */
	@CrossOrigin
	@RequestMapping(value={"/insert"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData insertDataWorkFlow(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken) throws Exception{
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return workFlowService.insertDataWorkFlow(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
	 * 根据主键删除对象
	 * @param data json字符串
	 * */
	@CrossOrigin
	@RequestMapping(value={"/deleteOne"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData deleteDataWorkFlowOne(@RequestBody String data){
		try{
    		JWorkFlow jworkFlow = JSON.parseObject(data,JWorkFlow.class);
    		return workFlowService.deleteDataWorkFlowOne(jworkFlow.getIndocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
    	try{
    		JWorkFlow jworkFlow = JSON.parseObject(data,JWorkFlow.class);
    		return workFlowService.deleteDataWorkFlowMany(jworkFlow.getStr_indocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
    }
	
	/**
     * 修改记录
     * @param data json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
	@RequestMapping(value={"/update"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData updateDataWorkFlow(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken) throws Exception{
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return workFlowService.updateDataWorkFlow(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
     * 分页查看记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataWorkFlowByPage(@RequestBody String data) {
        return workFlowService.findDataWorkFlowByPage(data);
    }
    
    /**
     * 根据主键查询单条记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataWorkFlowByIndocno(@RequestBody String data) {
        return workFlowService.findDataWorkFlowByIndocno(data);
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	@CrossOrigin
	@RequestMapping(value={"/findAll"}, method=RequestMethod.POST)
	public List<WorkFlow> findDataWorkFlow(){
		return workFlowService.findDataWorkFlow();
	};
}
