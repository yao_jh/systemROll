package com.my.business.workflow.workflow.entity;

public class JPushNewEntity {

	private Long getUserid;
	private String getUser;
	private String modular_no;
	private String smessage;
	private String area_name;
	
	public String getArea_name() {
		return area_name;
	}

	public void setArea_name(String area_name) {
		this.area_name = area_name;
	}

	public Long getGetUserid() {
		return getUserid;
	}

	public void setGetUserid(Long getUserid) {
		this.getUserid = getUserid;
	}

	public String getGetUser() {
		return getUser;
	}

	public void setGetUser(String getUser) {
		this.getUser = getUser;
	}

	public String getModular_no() {
		return modular_no;
	}

	public void setModular_no(String modular_no) {
		this.modular_no = modular_no;
	}

	public String getSmessage() {
		return smessage;
	}

	public void setSmessage(String smessage) {
		this.smessage = smessage;
	}

}
