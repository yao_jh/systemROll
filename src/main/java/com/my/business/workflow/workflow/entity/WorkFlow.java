package com.my.business.workflow.workflow.entity;

import java.util.List;

import com.my.business.sys.common.entity.BaseEntity;
import com.my.business.workflow.workflowdetail.entity.WorkFlowdetail;
import com.my.business.workflow.workparam.entity.WorkParam;

/**
 * 工作流主表实体类
 * 
 * @author 生成器生成
 * @date 2020-09-02 09:13:25
 */
public class WorkFlow extends BaseEntity {

	private Long indocno; // 主键
	private Long flow_typeid; // 分类
	private String flow_type; // 分类id
	private String flow_no; // 工作流编码
	private String flow_name; // 工作流名称
	private String flow_char; // 工作流前台传递的字符串

	private List<WorkFlowdetail> detail; // 工作流子表步骤集合
	private List<WorkParam> paramList; // 工作流配置参数集合
	private String noid_array;  //删除数组,保存工作流步骤中需要删除的nodeid数组

	public void setIndocno(Long indocno) {
		this.indocno = indocno;
	}

	public Long getIndocno() {
		return this.indocno;
	}

	public void setFlow_no(String flow_no) {
		this.flow_no = flow_no;
	}

	public String getFlow_no() {
		return this.flow_no;
	}

	public void setFlow_name(String flow_name) {
		this.flow_name = flow_name;
	}

	public String getFlow_name() {
		return this.flow_name;
	}

	public void setFlow_char(String flow_char) {
		this.flow_char = flow_char;
	}

	public String getFlow_char() {
		return this.flow_char;
	}

	public Long getFlow_typeid() {
		return flow_typeid;
	}

	public void setFlow_typeid(Long flow_typeid) {
		this.flow_typeid = flow_typeid;
	}

	public String getFlow_type() {
		return flow_type;
	}

	public void setFlow_type(String flow_type) {
		this.flow_type = flow_type;
	}

	public List<WorkFlowdetail> getDetail() {
		return detail;
	}

	public void setDetail(List<WorkFlowdetail> detail) {
		this.detail = detail;
	}

	public List<WorkParam> getParamList() {
		return paramList;
	}

	public void setParamList(List<WorkParam> paramList) {
		this.paramList = paramList;
	}

	public String getNoid_array() {
		return noid_array;
	}

	public void setNoid_array(String noid_array) {
		this.noid_array = noid_array;
	}
}