package com.my.business.workflow.workflow.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.workflow.workflow.entity.WorkFlow;
import com.my.business.workflow.workflowdetailperform.entity.WorkFlowdetailPerform;

import java.util.List;
import java.util.Map;

/**
* 工作流主表接口服务类
* @author  生成器生成
* @date 2020-09-02 09:13:27
*/
public interface WorkFlowService {
	
	/***
	 * 直接发推送消息
	 * @param area_name  备辊用的编码,需要翻译
	 * @param smessage  推送消息
	 * @param modular_no 业务编码
	 * @param getuserId   接受用户id
	 * @param getUser    接受用户姓名
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return
	 */
	public ResultData toPushNew(String area_name,String smessage,String modular_no,Long getUserid,String getUser,Long userId,String sname);
	
	/***
	 * 直接发推送消息2
	 * @param smessage  推送消息
	 * @param modular_no 业务编码
	 * @param getuserId   接受用户id
	 * @param getUser    接受用户姓名
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return
	 */
	public ResultData toPushNew2(String roll_no,String tourl,String smessage,String modular_no,Long getUserid,String getUser,Long userId,String sname);
	
	/***
	 * 轧机发磨辊间消息
	 * @param indocno  该业务单据主键
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return
	 */
	public ResultData toGr(Long indocno,Long userId,String sname) throws Exception;
	
	/***
	 * 发起
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param flow_no  流程编码
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return
	 */
	public ResultData startWorkFlow(Map<String,String> map,String flow_no,Long userId,String sname) throws Exception;
	
	
	/***
	 * 第一步磨削流程发起(磨削流程专用)
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param flow_no  流程编码
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return
	 */
	public ResultData startWorkFlowMx(Map<String,String> map,String flow_no,Long userId,String sname) throws Exception;
	
	/***
	 * 冷却流程
	 * @param map  前台传递的json的list集合转换成map集合
	 * @return
	 */
	public ResultData startWorkFlowLq(Map<String,String> map) throws Exception;
	
	
	/***
	 * 下一步
	 * @param flow_id 工作流id
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param perform_no 工作流工作组编号
	 * @param nodeid  当前步骤id
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return
	 */
	public ResultData nextWorkFlow(Long indocno,Long flow_id,Map<String,String> map,String perform_no,String nodeid,Long userId,String sname) throws Exception;
	
	/***
	 * 下一步(磨削流程专用)
	 * @param flow_id 工作流id
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param perform_no 工作流工作组编号
	 * @param nodeid  当前步骤id
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return
	 */
	public ResultData nextWorkFlowMx(Long indocno,Long flow_id,Map<String,String> map,String perform_no,String nodeid,Long userId,String sname) throws Exception;
	
	/***
	 * 根据步骤id和执行编码生成指定推送消息
	 * @param flow_id 工作流id
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param perform_no 工作流工作组编号
	 * @param nodeid  当前步骤id
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return
	 */
	public ResultData createPushNewAll(Long ilinkno,Long machine_no,Long flow_id,Map<String,String> map,String perform_no,String nodeid,Long userId,String sname,Long orderservicedetailindocno) throws Exception;
	
	/***
	 * 定时执行
	 * @param flow_id 工作流id
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param perform_no 工作流工作组编号
	 * @param nodeid  当前步骤id
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return
	 */
	public ResultData createPushNewSchedularAll(String nodeid,Long userId,String sname) throws Exception;
	
	
	
	/***
	 * 退回
	 * @param flow_id 工作流id
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param perform_no  执行步骤组编码
	 * @param nodeid  当前步骤id
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return
	 */
	public ResultData backWorkFlow(Long flow_id,Map<String,String> map,String perform_no,String nodeid,Long userId,String sname) throws Exception;
	
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
	 * @throws Exception 
     */
	public ResultData insertDataWorkFlow(String data,Long userId,String sname) throws Exception;
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataWorkFlowOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataWorkFlowMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataWorkFlow(String data,Long userId,String sname) throws Exception;
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataWorkFlowByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataWorkFlowByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<WorkFlow> findDataWorkFlow();

	/***
	 * 步骤生成推送消息
	 * @param flow_id  工作流id
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param wp  执行步骤对象
	 * @param userId  推送人id
	 * @param sname   推送人姓名
	 * @return
	 */
	public void createPushnew(Long flow_id, Map<String,String> map, WorkFlowdetailPerform wp, Long userId, String sname);
}
