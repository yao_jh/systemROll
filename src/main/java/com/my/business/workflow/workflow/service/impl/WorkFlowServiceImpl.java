package com.my.business.workflow.workflow.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;
import com.my.business.util.DateUtil;
import com.my.business.util.MathUtil;
import com.my.business.workflow.wokrupdatetable.dao.WokrUpdatetableDao;
import com.my.business.workflow.wokrupdatetable.entity.WokrUpdatetable;
import com.my.business.workflow.workflow.dao.WorkFlowDao;
import com.my.business.workflow.workflow.entity.JWorkFlow;
import com.my.business.workflow.workflow.entity.WorkFlow;
import com.my.business.workflow.workflow.service.WorkFlowService;
import com.my.business.workflow.workflowdetail.dao.WorkFlowdetailDao;
import com.my.business.workflow.workflowdetail.entity.WorkFlowdetail;
import com.my.business.workflow.workflowdetailperform.dao.WorkFlowdetailPerformDao;
import com.my.business.workflow.workflowdetailperform.entity.WorkFlowdetailPerform;
import com.my.business.workflow.worklogical.dao.WorkLogicalDao;
import com.my.business.workflow.worklogical.entity.WorkLogical;
import com.my.business.workflow.workparam.dao.WorkParamDao;
import com.my.business.workflow.workparam.entity.WorkParam;

import lombok.extern.apachecommons.CommonsLog;

import com.my.business.modular.orderservice.dao.OrderServiceDao;
import com.my.business.modular.orderservice.entity.OrderService;
import com.my.business.modular.orderservicedetail.dao.OrderServiceDetailDao;
import com.my.business.modular.rollaccident.dao.RollAccidentDao;
import com.my.business.modular.rollaccident.entity.RollAccident;
import com.my.business.modular.rollcoolorder.dao.RollCoolorderDao;
import com.my.business.modular.rollcoolorder.entity.RollCoolorder;
import com.my.business.modular.rollgrindingannex.dao.RollGrindingAnnexDao;
import com.my.business.modular.rollgrindingannex.entity.RollGrindingAnnex;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.modular.rollorderconfig.dao.RollOrderconfigDao;
import com.my.business.modular.rollpushnew.dao.RollPushnewDao;
import com.my.business.modular.rollpushnew.entity.RollPushnew;
import com.my.business.schedule.SystemSchedule;
import com.my.business.sys.common.dao.CommonDao;
import com.my.business.sys.common.entity.ResultData;

/**
* 工作流主表接口具体实现类
* @author  生成器生成
* @date 2020-09-02 09:13:27
*/
@CommonsLog
@Service
public class WorkFlowServiceImpl implements WorkFlowService {
	
	private final static Log logger = LogFactory.getLog(WorkFlowServiceImpl.class);
	
	@Value("${mxyd_flow_no}")
    private String mxyd_flow_no;
	
	@Autowired
	private RollCoolorderDao rollCoolorderDao;
	
	@Value("${dimter}")
    private String dimter;
	
	@Value("${gr_no}")
    private String gr_no;
	
	@Autowired
	private WorkFlowDao workFlowDao;
	
	@Autowired
	private WorkFlowdetailDao workFlowdetailDao;
	
	@Autowired
	private WorkParamDao workParamDao;
	
	@Autowired
	private RollPushnewDao rollPushnewDao;
	
	@Autowired
	private WokrUpdatetableDao wokrUpdatetableDao;
	
	@Autowired
	private WorkLogicalDao workLogicalDao;
	
	@Autowired
	private WorkFlowdetailPerformDao workFlowdetailPerformDao;
	
	@Autowired
	private OrderServiceDao orderServiceDao;
	
	@Autowired
	private OrderServiceDetailDao orderServiceDetailDao;
	
	@Autowired
	private RollOrderconfigDao rollOrderconfigDao;
	
	@Autowired
	private RollGrindingAnnexDao rollGrindingAnnexDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private RollInformationDao rollInformationDao;
	
	@Autowired
	private RollAccidentDao rollAccidentDao;
	
	/***
	 * 直接发推送消息
	 * @param smessage  推送消息
	 * @param modular_no 业务编码
	 * @param getuserId   接受用户id
	 * @param getUser    接受用户姓名
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return
	 */
	public ResultData toPushNew(String area_name,String smessage,String modular_no,Long getUserid,String getUser,Long userId,String sname) {
		try{
			RollPushnew rollPushnew = new RollPushnew(); //推送消息实体类
			rollPushnew.setModular_no(modular_no);   //新辊发起模块
			rollPushnew.setArea_name(area_name);
			
			//基础信息
			//推送消息
			rollPushnew.setNew_note(smessage);  //赋值推送消息
			rollPushnew.setIfinish(0L);
			
//			//权限信息
			rollPushnew.setPush_userid(userId);//赋值推送人id
			rollPushnew.setPush_user(sname); //赋值推送人
			rollPushnew.setPushtime(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));  //赋值推送时间
			
			rollPushnew.setGet_userid(getUserid); //赋值推送用户id
			rollPushnew.setGet_user(getUser); //赋值推送用户名称
			rollPushnewDao.insertDataRollPushnew(rollPushnew);
            return ResultData.ResultDataSuccessSelf("发送成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new RuntimeException(e.getMessage());
    	}
	}
	
	public ResultData toPushNew2(String roll_no,String tourl,String smessage,String modular_no,Long getUserid,String getUser,Long userId,String sname) {
		try{
			RollPushnew rollPushnew = new RollPushnew(); //推送消息实体类
			rollPushnew.setModular_no(modular_no);   //新辊发起模块
			rollPushnew.setRoll_no(roll_no);
			rollPushnew.setTourl(tourl);
			
			//基础信息
			//推送消息
			rollPushnew.setNew_note(smessage);  //赋值推送消息
			rollPushnew.setIfinish(0L);
			
//			//权限信息
			rollPushnew.setPush_userid(userId);//赋值推送人id
			rollPushnew.setPush_user(sname); //赋值推送人
			rollPushnew.setPushtime(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));  //赋值推送时间
			
			rollPushnew.setGet_userid(getUserid); //赋值推送用户id
			rollPushnew.setGet_user(getUser); //赋值推送用户名称
			rollPushnewDao.insertDataRollPushnew(rollPushnew);
            return ResultData.ResultDataSuccessSelf("发送成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new RuntimeException(e.getMessage());
    	}
	}
	
	
	/***
	 * 轧机发磨辊间消息
	 * @param indocno  该业务单据主键
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return createPushAll
	 */
	public ResultData toGr(Long indocno,Long userId,String sname) {
		try{
			WorkFlowdetail  w = workFlowdetailDao.findDataWorkFlowdetailByNodeid(gr_no);
			createPushAll(w,"gr",indocno,userId,sname);
            return ResultData.ResultDataSuccessSelf("发送成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new RuntimeException(e.getMessage());
    	}
	}
	
	
	
	/***
	 * 冷却流程
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param off_line_reason  信号传递的事故辊参数
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public ResultData startWorkFlowLq(Map<String,String> map) throws Exception{
		try{
			String roll_no = "";
			String off_line_reason = "";
		    String production_line_id = null; //产线id
		    String production_line = null; //产线
		    String factory_id = null;  //生产厂家id
		    String factory = null;  //生产厂家
		    String material_id = null;  //材质id
		    String material = null;  //材质
		    String roll_typeid = null;  //轧辊类型id
		    String roll_type = null;  //轧辊类型
		    String frame_noid = null;  //机架号id
		    String frame_no = null;  //机架号
		    String offline_time = null;  //下线时间
		    String uplinecount = null;   //累计上线次数
		    
			for(Map.Entry<String, String> entry : map.entrySet()){
				if(entry.getKey().equals("roll_no")) {
					roll_no = entry.getValue();
				}
				if(entry.getKey().equals("uplinecount")) {
					uplinecount = entry.getValue();
				}
				if(entry.getKey().equals("offline_time")) {
					offline_time = entry.getValue();
				}
				if(entry.getKey().equals("off_line_reason")) {
					off_line_reason = entry.getValue();
				}
				if(entry.getKey().equals("production_line_id")) {
					production_line_id = entry.getValue();
				}
				if(entry.getKey().equals("production_line")) {
					production_line = entry.getValue();
				}
				if(entry.getKey().equals("factory_id")) {
					factory_id = entry.getValue();
				}
				if(entry.getKey().equals("factory")) {
					factory = entry.getValue();
				}
				if(entry.getKey().equals("material_id")) {
					material_id = entry.getValue();
				}
				if(entry.getKey().equals("material")) {
					material = entry.getValue();
				}
				if(entry.getKey().equals("roll_typeid")) {
					roll_typeid = entry.getValue();
				}
				if(entry.getKey().equals("roll_type")) {
					roll_type = entry.getValue();
				}
				if(entry.getKey().equals("frame_noid")) {
					frame_noid = entry.getValue();
				}
				if(entry.getKey().equals("frame_no")) {
					frame_no = entry.getValue();
				}
			}
			
			logger.debug(roll_no + "开始冷却推送,下线原因是" + off_line_reason);
			if(StringUtils.isEmpty(off_line_reason)) {   //事故原因为空，则为正常
				logger.debug("轧辊类型是" + roll_typeid);
				if(Long.valueOf(roll_typeid) == 1) {  //精轧工作辊
					WorkFlowdetail  w = workFlowdetailDao.findDataWorkFlowdetailByNodeid("node931603694924570");
					createPushnewLq(70L,map,w,0L,"信号","lq");
				}else {  //如果轧辊类型不是精轧工作辊,判断材质是否高速钢轧辊
					logger.debug("材质是" + material);
					if(material.equals("高速钢(HSS)")) {  //说明是高速钢轧辊
						logger.debug("累计上线次数是" + uplinecount);
						//判断累计上线次数是否小于3
						if(Long.valueOf(uplinecount) == 3) {  //说明上限次数=3次了，需要待磨削
							rollInformationDao.updateDataRollInformationByRevoleve(roll_no, 6L);
						}else {  //小于3则待上机
							rollInformationDao.updateDataRollInformationByRevoleve(roll_no, 4L);
						}
					}else {//不是高速钢轧辊，且不是精轧工作辊
						logger.debug("不是高速钢轧辊，且不是精轧工作辊");
						String dt="";
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
						Calendar c = Calendar.getInstance();
						try {
						    c.setTime(sdf.parse(offline_time));
						} catch (ParseException e) {
						    e.printStackTrace();
						} 
						c.add(Calendar.DAY_OF_MONTH,2);  // number of days to add
						dt = sdf.format(c.getTime());
						
						RollCoolorder r = new RollCoolorder();
						r.setRoll_no(roll_no);r.setRoll_type(roll_type);r.setRoll_typeid(Long.valueOf(roll_typeid));
						r.setField2(dt);
						rollCoolorderDao.insertDataRollCoolorder(r);
					}
				}
			}else {
				logger.debug("下线原因是" + off_line_reason);
				if(Long.valueOf(off_line_reason) != 1 && Long.valueOf(off_line_reason) != 17) {   //说明是事故辊
					rollInformationDao.updateDataRollInformationByroll_special(roll_no, 6L);
					RollAccident r = new RollAccident();
					
					r.setRoll_no(roll_no);
					r.setProduction_line(production_line);
					if(!StringUtils.isEmpty(production_line_id)) {
						r.setProduction_line_id(Long.valueOf(production_line_id));
					}
					r.setFactory(factory);
					if(!StringUtils.isEmpty(factory_id)) {
						r.setFactory_id(Long.valueOf(factory_id));
					}
					r.setMaterial(material);
					if(!StringUtils.isEmpty(material_id)) {
						r.setMaterial_id(Long.valueOf(material_id));
					}
					r.setRoll_type(roll_type);
					if(!StringUtils.isEmpty(roll_typeid)) {
						r.setRoll_typeid(Long.valueOf(roll_typeid));
					}
					r.setCreatetime(new Date());
					rollAccidentDao.insertDataRollAccident(r);
					
					//自动空冷48小时
					String dt="";
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
					Calendar c = Calendar.getInstance();
					try {
					    c.setTime(sdf.parse(offline_time));
					} catch (ParseException e) {
					    e.printStackTrace();
					} 
					c.add(Calendar.DAY_OF_MONTH,2);  // number of days to add
					dt = sdf.format(c.getTime());
					
					RollCoolorder rc = new RollCoolorder();
					rc.setRoll_no(roll_no);rc.setRoll_type(roll_type);rc.setRoll_typeid(Long.valueOf(roll_typeid));
					rc.setField2(dt);
					rollCoolorderDao.insertDataRollCoolorder(rc);
				}else {
					logger.debug("轧辊类型>>是" + off_line_reason);
					if(Long.valueOf(roll_typeid) == 1) {  //精轧工作辊
						WorkFlowdetail  w = workFlowdetailDao.findDataWorkFlowdetailByNodeid("node931603694924570");
						createPushnewLq(70L,map,w,0L,"信号","lq");
					}else {  //如果轧辊类型不是精轧工作辊,判断材质是否高速钢轧辊
						logger.debug("材质是>>>" + material);
						if(material.equals("高速钢(HSS)")) {  //说明是高速钢轧辊
							//判断累计上线次数是否小于3
							logger.debug("累计>>>>上线次数是" + uplinecount);
							if(Long.valueOf(uplinecount) == 3) {  //说明上限次数=3次了，需要待磨削
								rollInformationDao.updateDataRollInformationByRevoleve(roll_no, 6L);
							}else {  //小于3则待上机
								rollInformationDao.updateDataRollInformationByRevoleve(roll_no, 4L);
							}
						}else {//不是高速钢轧辊，且不是精轧工作辊
							logger.debug("不是高速钢轧辊，且不是精轧工作辊>>>>>>");
							String dt="";
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
							Calendar c = Calendar.getInstance();
							try {
							    c.setTime(sdf.parse(offline_time));
							} catch (ParseException e) {
							    e.printStackTrace();
							} 
							c.add(Calendar.DAY_OF_MONTH,2);  // number of days to add
							dt = sdf.format(c.getTime());
							
							RollCoolorder r = new RollCoolorder();
							r.setRoll_no(roll_no);r.setRoll_type(roll_type);r.setRoll_typeid(Long.valueOf(roll_typeid));
							r.setField2(dt);
							rollCoolorderDao.insertDataRollCoolorder(r);
						}
					}
				}
			}
            return ResultData.ResultDataSuccessSelf("生成成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new RuntimeException(e.getMessage());
    	}
	};
	
	/***
	 * 发起
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param flow_no  流程编码
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public ResultData startWorkFlow(Map<String,String> map,String flow_no,Long userId,String sname) throws Exception{
		try{
			WorkFlow workFlow = workFlowDao.findDataWorkFlowByNo(flow_no);//根据编码查询配置工作流对象
			List<WorkFlowdetail> detail = workFlow.getDetail();
			List<WorkFlowdetailPerform> list = new ArrayList<WorkFlowdetailPerform>();  //第一步骤容器
			String uuid = DateUtil.getDateYmdhms("yyyy-MM-dd HH:mm:ss") + CodiUtil.uuid();  //生成唯一标识uuid
			
			
			//子表插入
			for(WorkFlowdetail entity : detail) {
				WorkFlowdetailPerform workFlowdetailPerform = new WorkFlowdetailPerform();
				BeanUtils.copyProperties(entity, workFlowdetailPerform);  //把entity属性赋值给后面对象
				workFlowdetailPerform.setPerform_no(uuid);  //创建该执行步骤的专属编码
				workFlowdetailPerform.setIlinkno(entity.getIndocno());
				workFlowdetailPerform.setIfinish(0L);
				CodiUtil.newRecord(userId,sname,workFlowdetailPerform);
				if(StringUtils.isEmpty(entity.getBefornodeid())) {//如果上一步骤为空，则为第一步骤
					list.add(workFlowdetailPerform);
				}
				workFlowdetailPerformDao.insertDataWorkFlowdetailPerform(workFlowdetailPerform);
			}
			
			if(flow_no.equals("202010101359579744")) {
				for(WorkFlowdetailPerform step_entity : list) {
					if(step_entity.getNodeid().equals("node21602309435036")) {//尺寸
						createPushnew(workFlow.getIndocno(),map,step_entity,userId,sname);   //根据第一步骤集合生成推送消息
					}
					if(step_entity.getNodeid().equals("node231602309489884")) {//超声波
						createPushnewxgcsb(workFlow.getIndocno(),map,step_entity,userId,sname);
					}
				}
			}else {
				for(WorkFlowdetailPerform step_entity : list) {
					createPushnew(workFlow.getIndocno(),map,step_entity,userId,sname);   //根据第一步骤集合生成推送消息
				}
			}
            return ResultData.ResultDataSuccessSelf("发起成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new RuntimeException(e.getMessage());
    	}
	};
	
	/***
	 * 磨削发起专用(因为涉及俩个流程同时发起，需要定制化)
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param flow_no  流程编码
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public ResultData startWorkFlowMx(Map<String,String> map,String flow_no,Long userId,String sname) throws Exception{
		try{
			
			WorkFlow workFlow = workFlowDao.findDataWorkFlowByNo(flow_no);//根据编码查询配置工作流对象,这里肯定是磨削超声波流程
			String hard_flow_no = "";
			if(flow_no.equals("2020102420034522mx1")) {
				hard_flow_no = "2020102716262170mx1";
			}else if(flow_no.equals("2020102420034522mx2")) {
				hard_flow_no = "2020102716262170mx2";
			}else if(flow_no.equals("2020102420034522mx3")) {
				hard_flow_no = "2020102716262170mx3";
			}else if(flow_no.equals("2020102420034522mx4")) {
				hard_flow_no = "2020102716262170mx4";
			}else if(flow_no.equals("2020102420034522mx5")) {
				hard_flow_no = "2020102716262170mx5";
			}else if(flow_no.equals("2020102420034522mx6")) {
				hard_flow_no = "2020102716262170mx6";
			}
			
			WorkFlow workFlow_hard = workFlowDao.findDataWorkFlowByNo(hard_flow_no);   //根据硬度编码查询流程
			
			List<WorkFlowdetail> detail = workFlow.getDetail();   //超声波流程子项
			List<WorkFlowdetail> detail_hard = workFlow_hard.getDetail();   //硬度流程子项
			
			List<WorkFlowdetailPerform> list = new ArrayList<WorkFlowdetailPerform>();  //第一步骤容器
			String uuid = DateUtil.getDateYmdhms("yyyy-MM-dd HH:mm:ss") + CodiUtil.uuid();  //生成唯一标识uuid
			
			//超声波子表插入
			for(WorkFlowdetail entity : detail) {
				WorkFlowdetailPerform workFlowdetailPerform = new WorkFlowdetailPerform();
				BeanUtils.copyProperties(entity, workFlowdetailPerform);  //把entity属性赋值给后面对象
				workFlowdetailPerform.setPerform_no(uuid);  //创建该执行步骤的专属编码
				workFlowdetailPerform.setIlinkno(entity.getIndocno());
				workFlowdetailPerform.setIfinish(0L);
				CodiUtil.newRecord(userId,sname,workFlowdetailPerform);
				if(StringUtils.isEmpty(entity.getBefornodeid())) {//如果上一步骤为空，则为第一步骤
					list.add(workFlowdetailPerform);
				}
				workFlowdetailPerformDao.insertDataWorkFlowdetailPerform(workFlowdetailPerform);
			}
			
			//硬度子表插入
			for(WorkFlowdetail entity_hard : detail_hard) {
				WorkFlowdetailPerform workFlowdetailPerform_hard = new WorkFlowdetailPerform();
				BeanUtils.copyProperties(entity_hard, workFlowdetailPerform_hard);  //把entity属性赋值给后面对象
				workFlowdetailPerform_hard.setPerform_no(uuid);  //创建该执行步骤的专属编码
				workFlowdetailPerform_hard.setIlinkno(entity_hard.getIndocno());
				workFlowdetailPerform_hard.setIfinish(0L);
				CodiUtil.newRecord(userId,sname,workFlowdetailPerform_hard);
				
				workFlowdetailPerformDao.insertDataWorkFlowdetailPerform(workFlowdetailPerform_hard);
			}
			
			//构造磨削附件实体
			Long indocno = rollGrindingAnnexDao.findMaxIndocno();
			if(indocno != null) {
				indocno ++;
			}else {
				indocno = 1L;
			}
			RollGrindingAnnex r = new RollGrindingAnnex();
			r.setIndocno(indocno);
			r.setCreatetime(new Date());
			for(Map.Entry<String, String> entry : map.entrySet()){
				if(entry.getKey().equals("roll_no")) {
					r.setRoll_no(entry.getValue());
				}
				if(entry.getKey().equals("production_line")) {
					r.setProduction_line(entry.getValue());
				}
				if(entry.getKey().equals("production_line_id")) {
					r.setProduction_line_id(Long.valueOf(entry.getValue()));
				}
				if(entry.getKey().equals("factory_id")) {
					r.setFactory_id(Long.valueOf(entry.getValue()));
				}
				if(entry.getKey().equals("factory")) {
					r.setFactory(entry.getValue());
				}
				if(entry.getKey().equals("material_id")) {
					r.setMaterial_id(Long.valueOf(entry.getValue()));
				}
				if(entry.getKey().equals("material")) {
					r.setMaterial(entry.getValue());
				}
				if(entry.getKey().equals("roll_typeid")) {
					r.setRoll_typeid(Long.valueOf(entry.getValue()));
				}
				if(entry.getKey().equals("roll_type")) {
					r.setRoll_type(entry.getValue());
				}
				if(entry.getKey().equals("frame_noid")) {
					r.setFrame_noid(Long.valueOf(entry.getValue()));
				}
				if(entry.getKey().equals("frame_no")) {
					r.setFrame_no(entry.getValue());
				}
			}
			r.setGrind_starttime(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));
			rollGrindingAnnexDao.insertDataRollGrindingAnnex(r);
			
			
			for(WorkFlowdetailPerform step_entity : list) {
				createPushnewMx(indocno,workFlow.getIndocno(),map,step_entity,userId,sname);   //根据第一步骤集合生成超声波推送消息
			}
			
            return ResultData.ResultDataSuccessSelf("发起成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new RuntimeException(e.getMessage());
    	}
	};
	
	/***
	 * 下一步接口
	 * @param 推送消息id
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param perform_no  执行步骤组编码
	 * @param nodeid  当前步骤id
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	@Override
	public ResultData nextWorkFlow(Long indocno,Long flow_id,Map<String,String> map,String perform_no,String nodeid, Long userId, String sname) throws Exception{
		try{
			rollPushnewDao.updateDataRollPushnewFinish(indocno);
			WorkFlowdetailPerform  stepPerform = workFlowdetailPerformDao.findDataWorkFlowdetailPerformByNodeidAndNoOne(perform_no, nodeid);//根据当前步骤查询当前步骤一条数据
			List<WorkFlowdetailPerform> next_stepList = workFlowdetailPerformDao.findDataWorkFlowdetailPerformByNodeidAndNo(perform_no,stepPerform.getNextnodeid());//查询下一步骤集合
			String roll_no = null;
			for(Map.Entry<String, String> entry : map.entrySet()){
				if(entry.getKey().equals("roll_no")) {
					roll_no = entry.getValue();
				}
			}
			if(next_stepList.size() > 0) {
				//遍历下一步骤集合
				for(WorkFlowdetailPerform entity : next_stepList) {
					if(entity.getStep_type() == 1) {  //正常步骤下一步
						createPushnew(flow_id,map,entity,userId,sname);   //正常步骤生成下一步
						updateTable(entity.getIndocno(),roll_no);
						//当前步骤完成点击下一步骤后，该步骤更新状态已完成
						entity.setIfinish(1l);
						workFlowdetailPerformDao.updateDataWorkFlowdetailPerform(entity);
					}else if(entity.getStep_type() == 0) {  //菱形下一步
							//先更新步骤的状态
							updateTable(stepPerform.getIndocno(),roll_no);
							//当前步骤完成点击下一步骤后，该步骤更新状态已完成
							stepPerform.setIfinish(1l);
							workFlowdetailPerformDao.updateDataWorkFlowdetailPerform(stepPerform);
//						}
						String nextnodeid = "";  //因为是菱形，所以这里的下一步id是菱形的下一步id
						
						String[] befornodeid = entity.getBefornodeid().split(",");
						
						//菱形下一步的话，需要判断上一步骤集合(也就是当前步骤集合，就是上面的wlist)有几条数据，1条，表示不是会签，2条表示会签
						int count = befornodeid.length;
						if(count == 1){  //=1表示上一步骤只有一条,只需要拿到一条结果就行,一条数据不需要且和或
							List<WorkLogical> list = workFlowdetailDao.findDataWorkFlowdetailByIndocno(entity.getIlinkno()).getLogicallist();
							for(WorkLogical logical : list) {
								for(Map.Entry<String, String> entry : map.entrySet()){  //遍历前台的json集合看有没有和配置中相同的，相同的开始比较
									if(entry.getKey().equals(logical.getIfield())) {
										//逻辑运算符解析 =0,>1,<2,>=3,<=4,&&(且)5,||(或)6
										if(logical.getIlogical() == 0) {
											if(MathUtil.getByStringAndDouble(logical.getIcontrast(),entry.getValue(),0L)) {
												nextnodeid = logical.getNextnodeid();
											}
										}else if(logical.getIlogical() == 1) {
											if(MathUtil.getByStringAndDouble(logical.getIcontrast(),entry.getValue(),1L)) {
												nextnodeid = logical.getNextnodeid();
											}
										}else if(logical.getIlogical() == 2) {
											if(MathUtil.getByStringAndDouble(logical.getIcontrast(),entry.getValue(),2L)) {
												nextnodeid = logical.getNextnodeid();
											}
										}else if(logical.getIlogical() == 3) {
											if(MathUtil.getByStringAndDouble(logical.getIcontrast(),entry.getValue(),3L)) {
												nextnodeid = logical.getNextnodeid();
											}
										}else if(logical.getIlogical() == 4) {
											if(MathUtil.getByStringAndDouble(logical.getIcontrast(),entry.getValue(),4L)) {
												nextnodeid = logical.getNextnodeid();
											}
										}
									}
								}
								
							}
						}else if(count > 1) {  //表示上一步骤有两条,是会签,需要获取上面工单的结果做判断
							List<Long> longlist = new ArrayList<Long>();   //保存上一步骤集合的判断结果
							for(String nodi : befornodeid) {
								WorkFlowdetailPerform  nn = workFlowdetailPerformDao.findDataWorkFlowdetailPerformByNodeidAndNoOne(perform_no, nodi);
								if(!StringUtils.isEmpty(nn.getIfok())) {
									longlist.add(nn.getIfok());
								}
							}
							if(longlist.size() == befornodeid.length) {
								List<WorkLogical> list = workFlowdetailDao.findDataWorkFlowdetailByIndocno(entity.getIlinkno()).getLogicallist();
								for(WorkLogical logical : list) {   //遍历菱形子表的数据
									Double i = 0.0;
									boolean b = MathUtil.getBoolean(longlist, logical.getIlogical());  //把上一步骤的结果集合和子表数据的逻辑运算符进行计算,获得结果
									if(b) {
										i = 1.0;
									}
									if(Double.doubleToLongBits(i) == Double.doubleToLongBits(logical.getIcontrast())) {  
										nextnodeid = logical.getNextnodeid();
									}
								}
							}
						}
						
						if(nextnodeid != null && nextnodeid != "") {
							WorkFlowdetailPerform wp = workFlowdetailPerformDao.findDataWorkFlowdetailPerformByNodeidAndNoOne(perform_no,nextnodeid);//根据下一步骤id找到下一步骤
							
							createPushnew(flow_id,map,wp,userId,sname);   //推送消息
							updateTable(entity.getIndocno(),roll_no);
							//当前步骤完成点击下一步骤后，该步骤更新状态已完成
							entity.setIfinish(1l);
							workFlowdetailPerformDao.updateDataWorkFlowdetailPerform(entity);
						}
					}
				}
			}else {
				updateTable(stepPerform.getIndocno(),roll_no);
				//当前步骤完成点击下一步骤后，该步骤更新状态已完成
				stepPerform.setIfinish(1l);
				workFlowdetailPerformDao.updateDataWorkFlowdetailPerform(stepPerform);
				return ResultData.ResultDataSuccessSelf("该步骤是最后一步，不能下一步", null);
			}
			return ResultData.ResultDataSuccessSelf("下一步成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new RuntimeException(e.getMessage());
//    		return ResultData.ResultDataFaultSelf("下一步失败，错误信息为" + e.getMessage(), null); 
    	}
	}
	
	/***
	 * 下一步接口(磨削流程专用)
	 * @param flow_id 工作流id
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param perform_no  执行步骤组编码
	 * @param nodeid  当前步骤id
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	@Override
	public ResultData nextWorkFlowMx(Long indocno,Long flow_id,Map<String,String> map,String perform_no,String nodeid, Long userId, String sname) throws Exception{
		try{
			String roll_no = "";  //辊号
			String mx_num = "";  //磨削量
			String roll_special = ""; //特殊状态  事故辊6,让步新辊4,新材质新厂家7
			String count = "";  //计数器
			String roll_state = "";  //轧辊库存状态  待使用新辊3
			String if_hardness = "";  //是否硬度检测
			String if_wave = "";  //是否超声波检测
			String machine_no = "";  //磨床号
			String ilinkno = "";  //外键
			
			for(Map.Entry<String, String> entry : map.entrySet()){
//				if(entry.getKey().equals("diameter")) {
//					if(!StringUtils.isEmpty(entry.getValue())) {
//						diameter = entry.getValue();
//					}
//				}
				if(entry.getKey().equals("machine_no")) {
					if(!StringUtils.isEmpty(entry.getValue())) {
						machine_no = entry.getValue();
					}
				}
				if(entry.getKey().equals("ilinkno")) {
					if(!StringUtils.isEmpty(entry.getValue())) {
						ilinkno = entry.getValue();
					}
				}
				if(entry.getKey().equals("mx_num")) {
					if(!StringUtils.isEmpty(entry.getValue())) {
						mx_num = entry.getValue();
					}
				}
//				if(entry.getKey().equals("roll_special")) {
//					if(!StringUtils.isEmpty(entry.getValue())) {
//						roll_special = entry.getValue();
//					}
//				}
//				if(entry.getKey().equals("count")) {
//					if(!StringUtils.isEmpty(entry.getValue())) {
//						count = entry.getValue();
//					}
//				}
//				if(entry.getKey().equals("roll_state")) {
//					if(!StringUtils.isEmpty(entry.getValue())) {
//						roll_state = entry.getValue();
//					}
//				}
				if(entry.getKey().equals("if_hardness")) {
					if(!StringUtils.isEmpty(entry.getValue())) {
						if_hardness = entry.getValue();
					}
				}
				if(entry.getKey().equals("if_wave")) {
					if(!StringUtils.isEmpty(entry.getValue())) {
						if_wave = entry.getValue();
					}
				}
				if(entry.getKey().equals("roll_no")) {
					if(!StringUtils.isEmpty(entry.getValue())) {
						roll_no = entry.getValue();
					}
				}
			}
			String wave_node_id = "";//磨削超声波工作流步骤id
			String hard_node_id = "";//磨削硬度工作流步骤id
			if(machine_no.equals("1")) {
				wave_node_id = "node941603540644687mx1";
				hard_node_id = "node401603787213431mx1";
			}else if(machine_no.equals("2")) {
				wave_node_id = "node941603540644687mx2";
				hard_node_id = "node401603787213431mx2";
			}else if(machine_no.equals("3")) {
				wave_node_id = "node941603540644687mx3";
				hard_node_id = "node401603787213431mx3";
			}else if(machine_no.equals("4")) {
				wave_node_id = "node941603540644687mx4";
				hard_node_id = "node401603787213431mx4";
			}else if(machine_no.equals("5")) {
				wave_node_id = "node941603540644687mx5";
				hard_node_id = "node401603787213431mx5";
			}else if(machine_no.equals("6")) {
				wave_node_id = "node941603540644687mx6";
				hard_node_id = "node401603787213431mx6";
			}else {
				return ResultData.ResultDataFaultSelf("没有该磨床,请检查传递参数", null); 
			}
			
			rollPushnewDao.updateDataRollPushnewFinish(indocno);
			RollInformation r = rollInformationDao.findDataRollInformationByRollNo(roll_no);
			Double diameter = r.getBody_diameter();
			roll_special = String.valueOf(r.getRoll_special()); //特殊状态  事故辊6,让步新辊4,新材质新厂家7
			count = String.valueOf(r.getSpecial_state_count());  //计数器
			roll_state = String.valueOf(r.getRoll_state());  //轧辊库存状态  待使用新辊3
			
			
			if(!StringUtils.isEmpty(if_wave)) {
				if(if_wave.equals("1")) {
					createPushNewAll(Long.valueOf(ilinkno),Long.valueOf(machine_no),flow_id,map,perform_no,wave_node_id,userId,sname,82L);
				}
			}
			//判断是否生成硬度工单
			if(!StringUtils.isEmpty(if_hardness)) {
				if(if_hardness.equals("1")) {
					createPushNewAll(Long.valueOf(ilinkno),Long.valueOf(machine_no),flow_id,map,perform_no,hard_node_id,userId,sname,81L);
				}
			}
			
			Long roll_typeid = r.getRoll_typeid();
//			if(roll_typeid == 2 || roll_typeid == 4 || roll_typeid == 7) {
//				createPushNewAll(Long.valueOf(ilinkno),Long.valueOf(machine_no),flow_id,map,perform_no,wave_node_id,userId,sname,82L);   //超声波
//				createPushNewAll(Long.valueOf(ilinkno),Long.valueOf(machine_no),flow_id,map,perform_no,hard_node_id,userId,sname,81L);  //硬度
//			}else {
				//判断是否生成超声波工单
//				if(!StringUtils.isEmpty(if_wave)) {
//					if(if_wave.equals("1")) {
//						createPushNewAll(Long.valueOf(ilinkno),Long.valueOf(machine_no),flow_id,map,perform_no,wave_node_id,userId,sname,82L);
//					}
//				}
//				else {
//					//根据条件判断是否生成超声波工单
////					if(Long.valueOf(mx_num) >= 0.3) {  //超磨量
//					if(Double.doubleToLongBits(Double.valueOf(mx_num)) > Double.doubleToLongBits(Double.valueOf("0.3"))) {
//						createPushNewAll(Long.valueOf(ilinkno),Long.valueOf(machine_no),flow_id,map,perform_no,wave_node_id,userId,sname,82L);
//					}else {
//						if(roll_special.equals("6")) {  //是否事故辊
//							createPushNewAll(Long.valueOf(ilinkno),Long.valueOf(machine_no),flow_id,map,perform_no,wave_node_id,userId,sname,82L);
//						}else {
//							if(roll_special.equals("4") || roll_special.equals("7")) {  //是否让步新辊或者新材质新厂家
//								if(Long.valueOf(count) > 0) {  //是否三次以内(大于0就一定在3以内)
//									createPushNewAll(Long.valueOf(ilinkno),Long.valueOf(machine_no),flow_id,map,perform_no,wave_node_id,userId,sname,82L);
//								}
//							}else {
//								if(roll_state.equals(3)) {   //是否是新辊
//									createPushNewAll(Long.valueOf(ilinkno),Long.valueOf(machine_no),flow_id,map,perform_no,wave_node_id,userId,sname,82L);
//								}
//							}
//						}
//					}
//				}
				
				//判断是否生成硬度工单
//				if(!StringUtils.isEmpty(if_hardness)) {
//					if(if_hardness.equals("1")) {
//						createPushNewAll(Long.valueOf(ilinkno),Long.valueOf(machine_no),flow_id,map,perform_no,hard_node_id,userId,sname,81L);
//					}
//				}
//				else {
//					if(Double.doubleToLongBits(diameter) > Double.doubleToLongBits(Double.valueOf(dimter))) {
//						createPushNewAll(Long.valueOf(ilinkno),Long.valueOf(machine_no),flow_id,map,perform_no,hard_node_id,userId,sname,81L);
//					}
//				}
//			}
			return ResultData.ResultDataSuccessSelf("下一步成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new RuntimeException(e.getMessage());
    	}
	}
	
	/***
	 * 根据主键和轧辊号更新主键关联的表的状态
	 * @param indocno
	 * @param roll_no
	 */
	public void updateTable(Long indocno,String roll_no) {
		
		WorkFlowdetailPerform wts = workFlowdetailPerformDao.findDataWorkFlowdetailPerformByIndocno(indocno);
		List<WokrUpdatetable> list = wokrUpdatetableDao.findDataWokrUpdatetableByIlinkno(wts.getIlinkno());
		for(WokrUpdatetable w : list) {
			String sql = "update " + w.getTable_name() + " set " + w.getField_no() + " = " + w.getUpdate_value() + " where roll_no = '" + roll_no  + "'";
			commonDao.findOneData(sql);
		}
	}
	
	/***
	 * 新辊超声波步骤生成推送消息
	 * @param flow_id  工作流id
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param wp  执行步骤对象
	 * @param userId  推送人id
	 * @param sname   推送人姓名
	 * @return
	 */
	public void createPushnewxgcsb(Long flow_id,Map<String,String> map,WorkFlowdetailPerform wp, Long userId, String sname) {
		String roll_no = null;
		String production_line = null;  //产线
	    String production_line_id = null; //产线id
	    String factory = null;  //生产厂家
	    String factory_id = null;  //生产厂家id
	    String material_id = null;  //材质id
	    String material = null;  //材质
	    String roll_typeid = null;  //轧辊类型id
	    String roll_type = null;  //轧辊类型
	    String frame_noid = null;  //机架号id
	    String frame_no = null;  //机架号
	    
		for(Map.Entry<String, String> entry : map.entrySet()){
			if(entry.getKey().equals("roll_no")) {
				roll_no = entry.getValue();
			}
			if(entry.getKey().equals("production_line")) {
				production_line = entry.getValue();
			}
			if(entry.getKey().equals("production_line_id")) {
				production_line_id = entry.getValue();
			}
			if(entry.getKey().equals("factory")) {
				factory = entry.getValue();
			}
			if(entry.getKey().equals("factory_id")) {
				factory_id = entry.getValue();
			}
			if(entry.getKey().equals("material_id")) {
				material_id = entry.getValue();
			}
			if(entry.getKey().equals("material")) {
				material = entry.getValue();
			}
			if(entry.getKey().equals("roll_typeid")) {
				roll_typeid = entry.getValue();
			}
			if(entry.getKey().equals("roll_type")) {
				roll_type = entry.getValue();
			}
			if(entry.getKey().equals("frame_noid")) {
				frame_noid = entry.getValue();
			}
			if(entry.getKey().equals("frame_no")) {
				frame_no = entry.getValue();
			}
		}	
			RollPushnew rollPushnew = new RollPushnew(); //推送消息实体类
			rollPushnew.setFlowid(flow_id);
			rollPushnew.setRoll_no(roll_no);
			rollPushnew.setModular_no("xg");
			rollPushnew.setProduction_line(production_line);
			if(!StringUtils.isEmpty(production_line_id)) {
				rollPushnew.setProduction_line_id(Long.valueOf(production_line_id));
			}
			rollPushnew.setFactory(factory);
			if(!StringUtils.isEmpty(factory_id)) {
				rollPushnew.setFactory_id(Long.valueOf(factory_id));
			}
			if(!StringUtils.isEmpty(material_id)) {
				rollPushnew.setMaterial_id(Long.valueOf(material_id));
			}
			rollPushnew.setMaterial(material);
			rollPushnew.setRoll_type(roll_type);
			if(!StringUtils.isEmpty(roll_typeid)) {
				rollPushnew.setRoll_typeid(Long.valueOf(roll_typeid));
			}
			rollPushnew.setFrame_no(frame_no);
			if(!StringUtils.isEmpty(frame_noid)) {
				rollPushnew.setFrame_noid(Long.valueOf(frame_noid));
			}
			rollPushnew.setPerform_no(wp.getPerform_no());
			rollPushnew.setNodeid(wp.getNodeid());//赋值当前步骤id
			//基础信息
			//替换推送消息
			String message = wp.getSmessage();   //步骤具体推送消息  重写
			//替换掉配送消息的关键字
			for(Map.Entry<String, String> entry : map.entrySet()){
				if(message.indexOf(entry.getKey()) > 0) {
					message = message.replace(entry.getKey(), entry.getValue());
				}
			}
			rollPushnew.setNew_note(message);  //赋值推送消息
			
			if(!StringUtils.isEmpty(wp.getOrder_id())) {
				rollPushnew.setOrder_no(getOrder_noParam(70L));
				rollPushnew.setMethod_url(orderServiceDao.findDataOrderServiceByIndocno(wp.getOrder_id()).getMethod_url());  //方法地址
				rollPushnew.setOrder_entity(orderServiceDao.findDataOrderServiceByIndocno(wp.getOrder_id()).getOrder_entity());//实体对象
			}
			
			String iurl = wp.getIurl();  //跳转url
			rollPushnew.setTourl(iurl);
//			//权限信息
			rollPushnew.setPush_userid(userId);//赋值推送人id
			rollPushnew.setPush_user(sname); //赋值推送人
			rollPushnew.setPushmodular(wp.getNodename()); //推送步骤名称
			rollPushnew.setPushtime(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));  //赋值推送时间
			
			Long iuserorgroup = wp.getIuserorgroup(); //权限类型1人or0组
			if(iuserorgroup == 0) {  //组
				rollPushnew.setGet_groupid(wp.getUgid());  //赋值推送组id
				rollPushnew.setGet_group(wp.getUgname()); //赋值推送组名称
			}else if(iuserorgroup == 1){ //人
				rollPushnew.setGet_userid(wp.getUgid()); //赋值推送用户id
				rollPushnew.setGet_user(wp.getUgname()); //赋值推送用户名称
			}
			rollPushnewDao.insertDataRollPushnew(rollPushnew);
	}
	
	/***
	 * 步骤生成推送消息通用(根据步骤发送单条语句)
	 * @param wp  步骤对象
	 * @param modular_no  消息类型
	 * @param userId  推送人id
	 * @param sname   推送人姓名
	 * @return
	 */
	public void createPushAll(WorkFlowdetail wp,String modular_no, Long indocno,Long userId, String sname) {
		RollPushnew rollPushnew = new RollPushnew(); //推送消息实体类
		rollPushnew.setModular_no(modular_no);   //新辊发起模块
		rollPushnew.setNodeid(wp.getNodeid());//赋值当前步骤id
		rollPushnew.setEntity_id(indocno);
		//基础信息
		//替换推送消息
		rollPushnew.setNew_note(wp.getSmessage());  //赋值推送消息
		
//		//权限信息
		rollPushnew.setPush_userid(userId);//赋值推送人id
		rollPushnew.setPush_user(sname); //赋值推送人
		rollPushnew.setPushmodular(wp.getNodename()); //推送步骤名称
		rollPushnew.setPushtime(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));  //赋值推送时间
		
		Long iuserorgroup = wp.getIuserorgroup(); //权限类型1人or0组
		if(iuserorgroup == 0) {  //组
			rollPushnew.setGet_groupid(wp.getUgid());  //赋值推送组id
			rollPushnew.setGet_group(wp.getUgname()); //赋值推送组名称
		}else if(iuserorgroup == 1){ //人
			rollPushnew.setGet_userid(wp.getUgid()); //赋值推送用户id
			rollPushnew.setGet_user(wp.getUgname()); //赋值推送用户名称
		}
		rollPushnewDao.insertDataRollPushnew(rollPushnew);
	}
	
	/***
	 * 步骤生成推送消息
	 * @param flow_id  工作流id
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param wp  执行步骤对象
	 * @param userId  推送人id
	 * @param sname   推送人姓名
	 * @return
	 */
	public void createPushnew(Long flow_id,Map<String,String> map,WorkFlowdetailPerform wp, Long userId, String sname) {
		String roll_no = null;
		String production_line = null;  //产线
	    String production_line_id = null; //产线id
	    String factory = null;  //生产厂家
	    String factory_id = null;  //生产厂家id
	    String material_id = null;  //材质id
	    String material = null;  //材质
	    String roll_typeid = null;  //轧辊类型id
	    String roll_type = null;  //轧辊类型
	    String frame_noid = null;  //机架号id
	    String frame_no = null;  //机架号
	    
		for(Map.Entry<String, String> entry : map.entrySet()){
			if(entry.getKey().equals("roll_no")) {
				roll_no = entry.getValue();
			}
			if(entry.getKey().equals("production_line")) {
				production_line = entry.getValue();
			}
			if(entry.getKey().equals("production_line_id")) {
				production_line_id = entry.getValue();
			}
			if(entry.getKey().equals("factory")) {
				factory = entry.getValue();
			}
			if(entry.getKey().equals("factory_id")) {
				factory_id = entry.getValue();
			}
			if(entry.getKey().equals("material_id")) {
				material_id = entry.getValue();
			}
			if(entry.getKey().equals("material")) {
				material = entry.getValue();
			}
			if(entry.getKey().equals("roll_typeid")) {
				roll_typeid = entry.getValue();
			}
			if(entry.getKey().equals("roll_type")) {
				roll_type = entry.getValue();
			}
			if(entry.getKey().equals("frame_noid")) {
				frame_noid = entry.getValue();
			}
			if(entry.getKey().equals("frame_no")) {
				frame_no = entry.getValue();
			}
		}	
			RollPushnew rollPushnew = new RollPushnew(); //推送消息实体类
			rollPushnew.setFlowid(flow_id);
			rollPushnew.setRoll_no(roll_no);
			rollPushnew.setModular_no("xg");   //新辊发起模块
			rollPushnew.setProduction_line(production_line);
			if(!StringUtils.isEmpty(production_line_id)) {
				rollPushnew.setProduction_line_id(Long.valueOf(production_line_id));
			}
			rollPushnew.setFactory(factory);
			if(!StringUtils.isEmpty(factory_id)) {
				rollPushnew.setFactory_id(Long.valueOf(factory_id));
			}
			if(!StringUtils.isEmpty(material_id)) {
				rollPushnew.setMaterial_id(Long.valueOf(material_id));
			}
			rollPushnew.setMaterial(material);
			rollPushnew.setRoll_type(roll_type);
			if(!StringUtils.isEmpty(roll_typeid)) {
				rollPushnew.setRoll_typeid(Long.valueOf(roll_typeid));
			}
			rollPushnew.setFrame_no(frame_no);
			if(!StringUtils.isEmpty(frame_noid)) {
				rollPushnew.setFrame_noid(Long.valueOf(frame_noid));
			}
			rollPushnew.setPerform_no(wp.getPerform_no());
			rollPushnew.setNodeid(wp.getNodeid());//赋值当前步骤id
			//基础信息
			//替换推送消息
			String message = wp.getSmessage();   //步骤具体推送消息  重写
			//替换掉配送消息的关键字
			for(Map.Entry<String, String> entry : map.entrySet()){
				if(message.indexOf(entry.getKey()) > 0) {
					message = message.replace(entry.getKey(), entry.getValue());
				}
			}
			rollPushnew.setNew_note(message);  //赋值推送消息
			if(!StringUtils.isEmpty(wp.getOrder_id())) {
				rollPushnew.setOrder_no(getOrder_no(map,wp.getOrder_id()));
				rollPushnew.setMethod_url(orderServiceDao.findDataOrderServiceByIndocno(wp.getOrder_id()).getMethod_url());  //方法地址
				rollPushnew.setOrder_entity(orderServiceDao.findDataOrderServiceByIndocno(wp.getOrder_id()).getOrder_entity());//实体对象
			}
			
			String iurl = wp.getIurl();  //跳转url
			rollPushnew.setTourl(iurl);
//			//权限信息
			rollPushnew.setPush_userid(userId);//赋值推送人id
			rollPushnew.setPush_user(sname); //赋值推送人
			rollPushnew.setPushmodular(wp.getNodename()); //推送步骤名称
			rollPushnew.setPushtime(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));  //赋值推送时间
			
			Long iuserorgroup = wp.getIuserorgroup(); //权限类型1人or0组
			if(iuserorgroup == 0) {  //组
				rollPushnew.setGet_groupid(wp.getUgid());  //赋值推送组id
				rollPushnew.setGet_group(wp.getUgname()); //赋值推送组名称
			}else if(iuserorgroup == 1){ //人
				rollPushnew.setGet_userid(wp.getUgid()); //赋值推送用户id
				rollPushnew.setGet_user(wp.getUgname()); //赋值推送用户名称
			}
			rollPushnewDao.insertDataRollPushnew(rollPushnew);
	}
	
	/***
	 * 步骤生成推送消息(赋值对象id,磨削发起专用)
	 * @param flow_id  工作流id
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param wp  执行步骤对象
	 * @param userId  推送人id
	 * @param sname   推送人姓名
	 * @return
	 */
	public void createPushnewMx(Long indocno,Long flow_id,Map<String,String> map,WorkFlowdetailPerform wp, Long userId, String sname) {
		String roll_no = null;
		String production_line = null;  //产线
	    String production_line_id = null; //产线id
	    String factory = null;  //生产厂家
	    String factory_id = null;  //生产厂家id
	    String material_id = null;  //材质id
	    String material = null;  //材质
	    String roll_typeid = null;  //轧辊类型id
	    String roll_type = null;  //轧辊类型
	    String frame_noid = null;  //机架号id
	    String frame_no = null;  //机架号
	    
	    String machine_no = null; //磨床编号
	    
		for(Map.Entry<String, String> entry : map.entrySet()){
			if(entry.getKey().equals("machine_no")) {
				machine_no = entry.getValue();
			}
			if(entry.getKey().equals("roll_no")) {
				roll_no = entry.getValue();
			}
			if(entry.getKey().equals("production_line")) {
				production_line = entry.getValue();
			}
			if(entry.getKey().equals("production_line_id")) {
				production_line_id = entry.getValue();
			}
			if(entry.getKey().equals("factory")) {
				factory = entry.getValue();
			}
			if(entry.getKey().equals("factory_id")) {
				factory_id = entry.getValue();
			}
			if(entry.getKey().equals("material_id")) {
				material_id = entry.getValue();
			}
			if(entry.getKey().equals("material")) {
				material = entry.getValue();
			}
			if(entry.getKey().equals("roll_typeid")) {
				roll_typeid = entry.getValue();
			}
			if(entry.getKey().equals("roll_type")) {
				roll_type = entry.getValue();
			}
			if(entry.getKey().equals("frame_noid")) {
				frame_noid = entry.getValue();
			}
			if(entry.getKey().equals("frame_no")) {
				frame_no = entry.getValue();
			}
		}	
			RollPushnew rollPushnew = new RollPushnew(); //推送消息实体类
			rollPushnew.setEntity_id(indocno);
			rollPushnew.setModular_no("mx");
			rollPushnew.setMachine_no(Long.valueOf(machine_no));   //添加磨床信息
			rollPushnew.setFlowid(flow_id);
			rollPushnew.setRoll_no(roll_no);
			rollPushnew.setProduction_line(production_line);
			if(!StringUtils.isEmpty(production_line_id)) {
				rollPushnew.setProduction_line_id(Long.valueOf(production_line_id));
			}
			rollPushnew.setFactory(factory);
			if(!StringUtils.isEmpty(factory_id)) {
				rollPushnew.setFactory_id(Long.valueOf(factory_id));
			}
			if(!StringUtils.isEmpty(material_id)) {
				rollPushnew.setMaterial_id(Long.valueOf(material_id));
			}
			rollPushnew.setMaterial(material);
			rollPushnew.setRoll_type(roll_type);
			if(!StringUtils.isEmpty(roll_typeid)) {
				rollPushnew.setRoll_typeid(Long.valueOf(roll_typeid));
			}
			rollPushnew.setFrame_no(frame_no);
			if(!StringUtils.isEmpty(frame_noid)) {
				rollPushnew.setFrame_noid(Long.valueOf(frame_noid));
			}
			rollPushnew.setPerform_no(wp.getPerform_no());
			rollPushnew.setNodeid(wp.getNodeid());//赋值当前步骤id
			//基础信息
			//替换推送消息
			String message = wp.getSmessage();   //步骤具体推送消息  重写
			//替换掉配送消息的关键字
			for(Map.Entry<String, String> entry : map.entrySet()){
				if(message.indexOf(entry.getKey()) > 0) {
					message = message.replace(entry.getKey(), entry.getValue());
				}
			}
			rollPushnew.setNew_note(message);  //赋值推送消息
			if(!StringUtils.isEmpty(wp.getOrder_id())) {
				rollPushnew.setOrder_no(getOrder_no(map,wp.getOrder_id()));
				rollPushnew.setMethod_url(orderServiceDao.findDataOrderServiceByIndocno(wp.getOrder_id()).getMethod_url());  //方法地址
				rollPushnew.setOrder_entity(orderServiceDao.findDataOrderServiceByIndocno(wp.getOrder_id()).getOrder_entity());//实体对象
			}
			
			String iurl = wp.getIurl();  //跳转url
			rollPushnew.setTourl(iurl);
//			//权限信息
			rollPushnew.setPush_userid(userId);//赋值推送人id
			rollPushnew.setPush_user(sname); //赋值推送人
			rollPushnew.setPushmodular(wp.getNodename()); //推送步骤名称
			rollPushnew.setPushtime(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));  //赋值推送时间
			
			Long iuserorgroup = wp.getIuserorgroup(); //权限类型1人or0组
			if(iuserorgroup == 2) {  //组
				rollPushnew.setGet_groupid(wp.getUgid());  //赋值推送组id
				rollPushnew.setGet_group(wp.getUgname()); //赋值推送组名称
			}else if(iuserorgroup == 1){ //人
				rollPushnew.setGet_userid(wp.getUgid()); //赋值推送用户id
				rollPushnew.setGet_user(wp.getUgname()); //赋值推送用户名称
			}
			rollPushnewDao.insertDataRollPushnew(rollPushnew);
	}
	
	/***
	 * 定时
	 */
	public ResultData createPushNewSchedularAll(String nodeid,Long userId,String sname) {
		try {
				RollPushnew rollPushnew = new RollPushnew(); //推送消息实体类
				rollPushnewDao.insertDataRollPushnew(rollPushnew);
				
				return ResultData.ResultDataSuccessSelf("下一步成功", null);
		}catch(Exception e) {
			e.printStackTrace();
			return ResultData.ResultDataFaultGd("下一步失败,失败信息为" + e.getMessage(), null);
		}
	}
	
	
	/***
	 * 步骤生成推送消息(赋值对象id,磨削专用)
	 * @param flow_id  工作流id
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param perform_no  执行编码
	 * @param nodeid  步骤id
	 * @param userId  推送人id
	 * @param sname   推送人姓名
	 * @return
	 */
	public ResultData createPushNewAll(Long ilinkno,Long machine_no,Long flow_id,Map<String,String> map,String perform_no,String nodeid,Long userId,String sname,Long orderservicedetailindocno) {
		try {
			WorkFlowdetailPerform wp = workFlowdetailPerformDao.findDataWorkFlowdetailPerformByNodeidAndNoOne(perform_no, nodeid);
			String roll_no = null;
			String production_line = null;  //产线
		    String production_line_id = null; //产线id
		    String factory = null;  //生产厂家
		    String factory_id = null;  //生产厂家id
		    String material_id = null;  //材质id
		    String material = null;  //材质
		    String roll_typeid = null;  //轧辊类型id
		    String roll_type = null;  //轧辊类型
		    String frame_noid = null;  //机架号id
		    String frame_no = null;  //机架号
		    
			for(Map.Entry<String, String> entry : map.entrySet()){
				if(entry.getKey().equals("roll_no")) {
					roll_no = entry.getValue();
				}
				if(entry.getKey().equals("production_line")) {
					production_line = entry.getValue();
				}
				if(entry.getKey().equals("production_line_id")) {
					production_line_id = entry.getValue();
				}
				if(entry.getKey().equals("factory")) {
					factory = entry.getValue();
				}
				if(entry.getKey().equals("factory_id")) {
					factory_id = entry.getValue();
				}
				if(entry.getKey().equals("material_id")) {
					material_id = entry.getValue();
				}
				if(entry.getKey().equals("material")) {
					material = entry.getValue();
				}
				if(entry.getKey().equals("roll_typeid")) {
					roll_typeid = entry.getValue();
				}
				if(entry.getKey().equals("roll_type")) {
					roll_type = entry.getValue();
				}
				if(entry.getKey().equals("frame_noid")) {
					frame_noid = entry.getValue();
				}
				if(entry.getKey().equals("frame_no")) {
					frame_no = entry.getValue();
				}
			}	
				RollPushnew rollPushnew = new RollPushnew(); //推送消息实体类
				rollPushnew.setFlowid(flow_id);
				rollPushnew.setRoll_no(roll_no);
				rollPushnew.setMachine_no(machine_no);
				rollPushnew.setModular_no("mx_order");
				rollPushnew.setEntity_id(ilinkno);
				rollPushnew.setProduction_line(production_line);
				if(!StringUtils.isEmpty(production_line_id)) {
					rollPushnew.setProduction_line_id(Long.valueOf(production_line_id));
				}
				rollPushnew.setFactory(factory);
				if(!StringUtils.isEmpty(factory_id)) {
					rollPushnew.setFactory_id(Long.valueOf(factory_id));
				}
				if(!StringUtils.isEmpty(material_id)) {
					rollPushnew.setMaterial_id(Long.valueOf(material_id));
				}
				rollPushnew.setMaterial(material);
				rollPushnew.setRoll_type(roll_type);
				if(!StringUtils.isEmpty(roll_typeid)) {
					rollPushnew.setRoll_typeid(Long.valueOf(roll_typeid));
				}
				rollPushnew.setFrame_no(frame_no);
				if(!StringUtils.isEmpty(frame_noid)) {
					rollPushnew.setFrame_noid(Long.valueOf(frame_noid));
				}
				rollPushnew.setPerform_no(wp.getPerform_no());
				rollPushnew.setNodeid(wp.getNodeid());//赋值当前步骤id
				//基础信息
				//替换推送消息
				String message = wp.getSmessage();   //步骤具体推送消息  重写
				//替换掉配送消息的关键字
				for(Map.Entry<String, String> entry : map.entrySet()){
					if(message.indexOf(entry.getKey()) > 0) {
						message = message.replace(entry.getKey(), entry.getValue());
					}
				}
				rollPushnew.setNew_note(message);  //赋值推送消息
				if(!StringUtils.isEmpty(wp.getOrder_id())) {
					rollPushnew.setOrder_no(getOrder_noParam(orderservicedetailindocno));
					rollPushnew.setMethod_url(orderServiceDao.findDataOrderServiceByIndocno(wp.getOrder_id()).getMethod_url());  //方法地址
					rollPushnew.setOrder_entity(orderServiceDao.findDataOrderServiceByIndocno(wp.getOrder_id()).getOrder_entity());//实体对象
				}
				
				String iurl = wp.getIurl();  //跳转url
				rollPushnew.setTourl(iurl);
//				//权限信息
				rollPushnew.setPush_userid(userId);//赋值推送人id
				rollPushnew.setPush_user(sname); //赋值推送人
				rollPushnew.setPushmodular(wp.getNodename()); //推送步骤名称
				rollPushnew.setPushtime(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));  //赋值推送时间
				
				Long iuserorgroup = wp.getIuserorgroup(); //权限类型1人or0组
				if(iuserorgroup == 0) {  //组
					rollPushnew.setGet_groupid(wp.getUgid());  //赋值推送组id
					rollPushnew.setGet_group(wp.getUgname()); //赋值推送组名称
				}else if(iuserorgroup == 1){ //人
					rollPushnew.setGet_userid(wp.getUgid()); //赋值推送用户id
					rollPushnew.setGet_user(wp.getUgname()); //赋值推送用户名称
				}
				rollPushnewDao.insertDataRollPushnew(rollPushnew);
				
				return ResultData.ResultDataSuccessSelf("下一步成功", null);
		}catch(Exception e) {
			e.printStackTrace();
			return ResultData.ResultDataFaultGd("下一步失败,失败信息为" + e.getMessage(), null);
		}
	}
	
	/***
	 * 专属水冷工单生成
	 * @param flow_id  工作流id
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param wp  执行步骤对象
	 * @param userId  推送人id
	 * @param sname   推送人姓名
	 * @return
	 */
	public void createPushnewLq(Long flow_id,Map<String,String> map,WorkFlowdetail wp, Long userId, String sname,String modular_no) {
		String roll_no = null;
		String production_line = null;  //产线
	    String production_line_id = null; //产线id
	    String factory = null;  //生产厂家
	    String factory_id = null;  //生产厂家id
	    String material_id = null;  //材质id
	    String material = null;  //材质
	    String roll_typeid = null;  //轧辊类型id
	    String roll_type = null;  //轧辊类型
	    String frame_noid = null;  //机架号id
	    String frame_no = null;  //机架号
	    
		for(Map.Entry<String, String> entry : map.entrySet()){
			if(entry.getKey().equals("roll_no")) {
				roll_no = entry.getValue();
			}
//			if(entry.getKey().equals("production_line")) {
//				production_line = entry.getValue();
//			}
//			if(entry.getKey().equals("production_line_id")) {
//				production_line_id = entry.getValue();
//			}
			if(entry.getKey().equals("factory")) {
				factory = entry.getValue();
			}
			if(entry.getKey().equals("factory_id")) {
				factory_id = entry.getValue();
			}
//			if(entry.getKey().equals("material_id")) {
//				material_id = entry.getValue();
//			}
//			if(entry.getKey().equals("material")) {
//				material = entry.getValue();
//			}
			if(entry.getKey().equals("roll_typeid")) {
				roll_typeid = entry.getValue();
			}
			if(entry.getKey().equals("roll_type")) {
				roll_type = entry.getValue();
			}
			if(entry.getKey().equals("frame_noid")) {
				frame_noid = entry.getValue();
			}
			if(entry.getKey().equals("frame_no")) {
				frame_no = entry.getValue();
			}
		}	
			RollPushnew rollPushnew = new RollPushnew(); //推送消息实体类
			rollPushnew.setFlowid(flow_id);
			rollPushnew.setRoll_no(roll_no);
			rollPushnew.setProduction_line(production_line);
			if(!StringUtils.isEmpty(production_line_id)) {
				rollPushnew.setProduction_line_id(Long.valueOf(production_line_id));
			}
			rollPushnew.setFactory(factory);
			if(!StringUtils.isEmpty(factory_id)) {
				rollPushnew.setFactory_id(Long.valueOf(factory_id));
			}
			if(!StringUtils.isEmpty(material_id)) {
				rollPushnew.setMaterial_id(Long.valueOf(material_id));
			}
			rollPushnew.setMaterial(material);
			rollPushnew.setRoll_type(roll_type);
			if(!StringUtils.isEmpty(roll_typeid)) {
				rollPushnew.setRoll_typeid(Long.valueOf(roll_typeid));
			}
			rollPushnew.setFrame_no(frame_no);
			if(!StringUtils.isEmpty(frame_noid)) {
				rollPushnew.setFrame_noid(Long.valueOf(frame_noid));
			}
			rollPushnew.setModular_no(modular_no);   //冷却模块
			rollPushnew.setNodeid(wp.getNodeid());//赋值当前步骤id
			//基础信息
			//替换推送消息
			String message = wp.getSmessage();   //步骤具体推送消息  重写
			//替换掉配送消息的关键字
			for(Map.Entry<String, String> entry : map.entrySet()){
				if(message.indexOf(entry.getKey()) > 0) {
					message = message.replace(entry.getKey(), entry.getValue());
				}
			}
			rollPushnew.setNew_note(message);  //赋值推送消息
			if(!StringUtils.isEmpty(wp.getOrder_id())) {
				rollPushnew.setOrder_no(getOrder_noParam(78L));
				rollPushnew.setMethod_url(orderServiceDao.findDataOrderServiceByIndocno(wp.getOrder_id()).getMethod_url());  //方法地址
				rollPushnew.setOrder_entity(orderServiceDao.findDataOrderServiceByIndocno(wp.getOrder_id()).getOrder_entity());//实体对象
			}
			
			String iurl = wp.getIurl();  //跳转url
			rollPushnew.setTourl(iurl);
//			//权限信息
			rollPushnew.setPush_userid(userId);//赋值推送人id
			rollPushnew.setPush_user(sname); //赋值推送人
			rollPushnew.setPushmodular(wp.getNodename()); //推送步骤名称
			rollPushnew.setPushtime(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));  //赋值推送时间
			
			Long iuserorgroup = wp.getIuserorgroup(); //权限类型1人or0组
			if(iuserorgroup == 0) {  //组
				rollPushnew.setGet_groupid(wp.getUgid());  //赋值推送组id
				rollPushnew.setGet_group(wp.getUgname()); //赋值推送组名称
			}else if(iuserorgroup == 1){ //人
				rollPushnew.setGet_userid(wp.getUgid()); //赋值推送用户id
				rollPushnew.setGet_user(wp.getUgname()); //赋值推送用户名称
			}
			rollPushnewDao.insertDataRollPushnew(rollPushnew);
	}
	
	
	/***
	 * 根据json参数和工作流配置的服务id来到工单配置里定位哪条工单，返回工单编码
	 * @param map
	 * @param indocno
	 * @return
	 */
	public String getOrder_noParam(Long indocno) {
		Long order_id = orderServiceDetailDao.findOrderIdByparam(indocno);//根据条件定位到工单服务子表中对应工单的主键
		String order_no = rollOrderconfigDao.findDataRollOrderconfigByIndocno(order_id).getOrder_no();//根据主键找到工单中的编码
		return order_no;
	}
	
	/***
	 * 根据json参数和工作流配置的服务id来到工单配置里定位哪条工单，返回工单编码
	 * @param map
	 * @param indocno
	 * @return
	 */
	public String getOrder_no(Map<String,String> map,Long indocno) {
		//根据条件定位到工单
		String production_line_id = null;
		String roll_typeid = null;
		String material_id = null;
		String frame_noid = null;
		String factory_id = null;
		
		String install_location_id = null; // 安装位置id
		String up_location_id = null; // 上机位置id
		String framerangeid = null; // 机架范围id

		for(Map.Entry<String, String> entry : map.entrySet()){
		    if(entry.getKey().equals("production_line_id")) {
		    	production_line_id = entry.getValue();
		    }
		    if(entry.getKey().equals("roll_typeid")) {
		    	roll_typeid = entry.getValue();
		    }
		    if(entry.getKey().equals("material_id")) {
		    	material_id = entry.getValue();
		    }
		    if(entry.getKey().equals("frame_noid")) {
		    	frame_noid = entry.getValue();
		    }
		    if(entry.getKey().equals("factory_id")) {
		    	factory_id = entry.getValue();
		    }
		    if(entry.getKey().equals("install_location_id")) {
		    	install_location_id = entry.getValue();
		    }
		    if(entry.getKey().equals("up_location_id")) {
		    	up_location_id = entry.getValue();
		    }
		    if(entry.getKey().equals("framerangeid")) {
		    	framerangeid = entry.getValue();
		    }
		}
		Long order_id = orderServiceDetailDao.findOrderId(indocno, production_line_id, roll_typeid, material_id, frame_noid, factory_id,install_location_id,up_location_id,framerangeid);//根据条件定位到工单服务子表中对应工单的主键
		String order_no = rollOrderconfigDao.findDataRollOrderconfigByIndocno(order_id).getOrder_no();//根据主键找到工单中的编码
		return order_no;
	}
	
	/***
	 * 退回
	 * @param flow_id 工作流id
	 * @param map  前台传递的json的list集合转换成map集合
	 * @param perform_no  执行步骤组编码
	 * @param nodeid  当前步骤id
	 * @param userId   用户id
	 * @param sname    用户姓名
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public ResultData backWorkFlow(Long flow_id,Map<String,String> map,String perform_no,String nodeid,Long userId,String sname) throws Exception{
		try{
			String roll_no = "";
			for(Map.Entry<String, String> entry : map.entrySet()){
				if(entry.getKey().equals("roll_no")) {
					roll_no = entry.getValue();
				}
			}
			List<WorkFlowdetailPerform> wlist = workFlowdetailPerformDao.findDataWorkFlowdetailPerformByNodeidAndNo(perform_no,nodeid);//根据步骤id和组编码找到当前步骤对象集合
			List<WorkFlowdetailPerform>  befor_step_list = workFlowdetailPerformDao.findDataWorkFlowdetailPerformByNodeidAndNo(perform_no,wlist.get(0).getBefornodeid());  //获取上一步骤对象集合
			for(WorkFlowdetailPerform befor_step : befor_step_list) {
				if(befor_step.getStep_type() == 1) {//是正常步骤
					createPushnew(flow_id,map,befor_step,userId,sname);   //推送消息
					updateTable(befor_step.getIndocno(),roll_no);
					//当前步骤完成点击下一步骤后，该步骤更新状态已完成
					befor_step.setIfinish(0l);
					workFlowdetailPerformDao.updateDataWorkFlowdetailPerform(befor_step);
				}else if(befor_step.getStep_type() == 0) {  //是菱形
					List<WorkFlowdetailPerform>  ling_step_list = workFlowdetailPerformDao.findDataWorkFlowdetailPerformByNodeidAndNo(perform_no,befor_step.getBefornodeid());  //获取菱形上一步骤对象集合
					for(WorkFlowdetailPerform wf : ling_step_list) {
						createPushnew(flow_id,map,wf,userId,sname);   //推送消息
						updateTable(wf.getIndocno(),roll_no);
						//当前步骤完成点击下一步骤后，该步骤更新状态已完成
						wf.setIfinish(0l);
						workFlowdetailPerformDao.updateDataWorkFlowdetailPerform(wf);
					}
				}
			}
			return ResultData.ResultDataSuccessSelf("退回成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new RuntimeException(e.getMessage());
//    		return ResultData.ResultDataFaultSelf("退回失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	@Transactional(rollbackFor = Exception.class)
	public ResultData insertDataWorkFlow(String data,Long userId,String sname) throws Exception{
		try{
			JWorkFlow jworkFlow = JSON.parseObject(data,JWorkFlow.class);
			WorkFlow workFlow = jworkFlow.getWorkFlow();
			String uuid = DateUtil.getDateYmdhms("yyyy-MM-dd HH:mm:ss") + CodiUtil.uuid();  //生成唯一标识uuid
			//主表数据保存
    		workFlow.setFlow_no(uuid);
    		CodiUtil.newRecord(userId,sname,workFlow);
            workFlowDao.insertDataWorkFlow(workFlow);
            
            //子表参数数据保存
            List<WorkParam> paramlist = workFlow.getParamList();
            if(paramlist.size() > 0) {
            	 for(WorkParam paramentity : paramlist) {
                 	paramentity.setFlow_no(uuid);
                 	CodiUtil.newRecord(userId,sname,paramentity);
                 	workParamDao.insertDataWorkParam(paramentity);
                 }
            }
           
            //子表数据保存
            List<WorkFlowdetail> detail = workFlow.getDetail();
            if(detail.size() > 0 ) {
            	Long indocno = workFlowdetailDao.findMaxIndocno();  //获取最大主键值
                if(indocno == null) {  //如果为空表示为第一条数据
                	indocno = 1L; 
                }else {  //不为空主键+1
                	indocno ++; 
                }
                
                for(int i = 0;i<detail.size();i++) {
                	detail.get(i).setIndocno(indocno + Long.valueOf(i));
                	detail.get(i).setFlow_no(uuid);
                	workFlowdetailDao.insertDataWorkFlowdetail(detail.get(i));
                	
                	//子表更新表格保存
                	if(!StringUtils.isEmpty(detail.get(i).getUpdateList())) {
                		for(WokrUpdatetable updateentity : detail.get(i).getUpdateList()) {
                			updateentity.setIlinkno(indocno);
                			wokrUpdatetableDao.insertDataWokrUpdatetable(updateentity);
                		}
                	}
                	
                	//子表逻辑配置表保存
                	if(!StringUtils.isEmpty(detail.get(i).getLogicallist())) {
                		for(WorkLogical logicalentity : detail.get(i).getLogicallist()) {
                			logicalentity.setIlinkno(indocno + Long.valueOf(i));
                			workLogicalDao.insertDataWorkLogical(logicalentity);
                		}
                		
                	}
                }
            }
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new RuntimeException(e.getMessage());
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataWorkFlowOne(Long indocno){
		try {
            workFlowDao.deleteDataWorkFlowOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataWorkFlowMany(String str_id) {
        try {
        	String sql = "delete work_flow where indocno in(" + str_id +")";
            workFlowDao.deleteDataWorkFlowMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
    @Transactional(rollbackFor = Exception.class)
	public ResultData updateDataWorkFlow(String data,Long userId,String sname) throws Exception{
		try {
			
			JWorkFlow jworkFlow = JSON.parseObject(data,JWorkFlow.class);
    		WorkFlow workFlow = jworkFlow.getWorkFlow();
        	CodiUtil.editRecord(userId,sname,workFlow);
        	workFlowDao.updateDataWorkFlow(workFlow);
        	
        	
        	//子表参数数据保存
            List<WorkParam> paramlist = workFlow.getParamList();
            for(WorkParam paramentity : paramlist) {
            	if(!StringUtils.isEmpty(paramentity.getIndocno())) {//主键不为空则表示更新操作
            		CodiUtil.editRecord(userId,sname,paramentity);
                	workParamDao.updateDataWorkParam(paramentity);
            	}else {												//主键为空则表示添加操作
            		paramentity.setFlow_no(workFlow.getFlow_no());
            		CodiUtil.newRecord(userId, sname, paramentity);
            		workParamDao.insertDataWorkParam(paramentity);
            	}
            }
            
            //子表数据保存
            List<WorkFlowdetail> detail = workFlow.getDetail();
            
            List<String> nodeid_array = JSON.parseArray(workFlow.getNoid_array(), String.class);
            
            if(nodeid_array.size() >0) {  //如果删除数组不为空，说明有需要删除的数据
            	for(String nodeid : nodeid_array) {  //遍历数组
            		WorkFlowdetail wt = workFlowdetailDao.findDataWorkFlowdetailByNodeid(nodeid);  //根据步骤号查询数据
            		if(wt != null) {  //查询的步骤不为空,则删除
            			workFlowdetailDao.deleteDataWorkFlowdetailOne(wt.getIndocno());
            		}
            	}
            }else {
            	for(int i = 0;i<detail.size();i++) {
                	WorkFlowdetail w = workFlowdetailDao.findDataWorkFlowdetailByNodeid(detail.get(i).getNodeid());
                	if(!StringUtils.isEmpty(w)) {//对象不为空则表示更新操作
                		CodiUtil.editRecord(userId,sname,detail.get(i));
                		detail.get(i).setIndocno(w.getIndocno());
                		workFlowdetailDao.updateDataWorkFlowdetail(detail.get(i));
                	}else {	//对象为空则表示添加操作
                		Long indocno = workFlowdetailDao.findMaxIndocno();  //获取最大主键值
                        if(indocno == null) {  //如果为空表示为第一条数据
                        	indocno = 1L; 
                        }else {  //不为空主键+1
                        	indocno ++; 
                        }
                		CodiUtil.newRecord(userId, sname, detail.get(i));
                		detail.get(i).setIndocno(indocno);
                		detail.get(i).setFlow_no(workFlow.getFlow_no());
                		workFlowdetailDao.insertDataWorkFlowdetail(detail.get(i));
                	}
                	
                	//子表更新表格保存
                	if(!StringUtils.isEmpty(detail.get(i).getUpdateList())) {
                		for(WokrUpdatetable updateentity : detail.get(i).getUpdateList()) {
                			if(!StringUtils.isEmpty(updateentity.getIndocno())) {//主键不为空则表示更新操作
                        		CodiUtil.editRecord(userId,sname,updateentity);
                        		wokrUpdatetableDao.updateDataWokrUpdatetable(updateentity);
                        	}else {												//主键为空则表示添加操作
                        		CodiUtil.newRecord(userId, sname, updateentity);
                        		updateentity.setIlinkno(detail.get(i).getIndocno());
                        		wokrUpdatetableDao.insertDataWokrUpdatetable(updateentity);
                        	}
                		}
                	}
                	
                	//子表逻辑配置表保存
                	if(!StringUtils.isEmpty(detail.get(i).getLogicallist())) {
                		for(WorkLogical logicalentity : detail.get(i).getLogicallist()) {
                			if(!StringUtils.isEmpty(logicalentity.getIndocno())) {//主键不为空则表示更新操作
                        		CodiUtil.editRecord(userId,sname,logicalentity);
                        		workLogicalDao.updateDataWorkLogical(logicalentity);
                        	}else {												//主键为空则表示添加操作
                        		CodiUtil.newRecord(userId, sname, logicalentity);
                        		workLogicalDao.insertDataWorkLogical(logicalentity);
                        	}
                		}
                		
                	}
                }
            }
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
//            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataWorkFlowByPage(String data) {
        try {
        	JWorkFlow jworkFlow = JSON.parseObject(data, JWorkFlow.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jworkFlow.getPageIndex();
        	Integer pageSize = jworkFlow.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jworkFlow.getCondition()){
    			jsonObject  = JSON.parseObject(jworkFlow.getCondition().toString());
    		}
    
    		List<WorkFlow> list = workFlowDao.findDataWorkFlowByPage((pageIndex-1)*pageSize, pageSize);
    		
    		for(WorkFlow flow : list) {
    			for(WorkFlowdetail entity : flow.getDetail()) {
    				List<WokrUpdatetable> uplist = wokrUpdatetableDao.findDataWokrUpdatetableByIlinkno(entity.getIndocno());
    				List<WorkLogical> logicallist = workLogicalDao.findDataWorkLogicalByIlinkno(entity.getIndocno());
    				entity.setUpdateList(uplist);
    				entity.setLogicallist(logicallist);
    			}
    		}
    		
    		Integer count = workFlowDao.findDataWorkFlowByPageSize();
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataWorkFlowByIndocno(String data) {
        try {
        	JWorkFlow jworkFlow = JSON.parseObject(data, JWorkFlow.class); 
        	Long indocno = jworkFlow.getIndocno();
        	
    		WorkFlow workFlow = workFlowDao.findDataWorkFlowByIndocno(indocno);
    		return ResultData.ResultDataSuccess(workFlow);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<WorkFlow> findDataWorkFlow(){
		List<WorkFlow> list = workFlowDao.findDataWorkFlow();
		return list;
	}

}
