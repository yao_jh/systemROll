package com.my.business.workflow.workflow.entity;

import java.util.List;
import java.util.Map;

public class FlowJsonEntity {

	private Long flow_id;  //工作流id
	private String perform_no;  //执行步骤组号编码
	private String flow_no; // 工作流编码
	private String nodeid; // 步骤id
	private List<Map<String,String>> list;   //前天传递的json集合
	private Long indocno; //推送消息主键
	private String type; //推送消息类型
	
	public String getPerform_no() {
		return perform_no;
	}
	public void setPerform_no(String perform_no) {
		this.perform_no = perform_no;
	}
	public Long getFlow_id() {
		return flow_id;
	}
	public void setFlow_id(Long flow_id) {
		this.flow_id = flow_id;
	}
	public String getFlow_no() {
		return flow_no;
	}
	public void setFlow_no(String flow_no) {
		this.flow_no = flow_no;
	}
	public String getNodeid() {
		return nodeid;
	}
	public void setNodeid(String nodeid) {
		this.nodeid = nodeid;
	}
	public List<Map<String, String>> getList() {
		return list;
	}
	public void setList(List<Map<String, String>> list) {
		this.list = list;
	}
	public Long getIndocno() {
		return indocno;
	}
	public void setIndocno(Long indocno) {
		this.indocno = indocno;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
