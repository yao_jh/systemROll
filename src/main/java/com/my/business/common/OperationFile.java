package com.my.business.common;

import com.my.business.sys.common.entity.ResultData;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * @author Ferdinand
 * @version 1.0
 * @date 2020/06/11 9:09
 * @To do :
 */
public class OperationFile {

    @Value("${upload_url}")
    private static String file_url;  //从配置文件中获取文件路径

    //移除文件
    public static void removeFile(String fileUrl) {
        //数据库信息保存失败，删除上传的文件。
        File file = new File(fileUrl);
        if (file.exists()) {//文件是否存在
            file.delete();//删除文件
        }
    }

    //上传文件
    /*
    * 返回值  1：成功，0：文件为空，—1上传异常
    * */
    public static int uploadFile(MultipartFile file) {
        if (file != null) {
            try {
                String filename = file.getOriginalFilename();// 文件原名称
                String path = file_url + "/" + filename;
                File localFile = new File(path);
                file.transferTo(localFile);
                return 1;
            } catch (Exception e) {
                e.printStackTrace();
                return -1;
            }
        } else {
            return 0;
        }
    }





}
