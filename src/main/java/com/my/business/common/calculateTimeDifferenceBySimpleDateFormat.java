package com.my.business.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 用SimpleDateFormat计算时间差
 */
public interface calculateTimeDifferenceBySimpleDateFormat {
    SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    SimpleDateFormat simpleFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static Double days(String date1, String date2) throws ParseException {
        /*天数差*/
        Date fromDate1 = simpleFormat.parse(date1);
        Date toDate1 = simpleFormat.parse(date2);
        long from1 = fromDate1.getTime();
        long to1 = toDate1.getTime();
        double days = (double) ((to1 - from1) / (1000 * 60 * 60 * 24));
        System.out.println("两个时间之间的天数差为：" + days);
        return days;
    }


    public static double hours(String date1, String date2) throws ParseException {
        /*小时差*/
        Date fromDate2 = simpleFormat.parse(date1);
        Date toDate2 = simpleFormat.parse(date2);
        long from2 = fromDate2.getTime();
        long to2 = toDate2.getTime();
        double hours = (double) ((to2 - from2) / (1000 * 60 * 60));
        System.out.println("两个时间之间的小时差为：" + hours);
        return hours;
    }

    public static double minutes(String date1, String date2) throws ParseException {
        /*分钟差*/
        Date fromDate3 = simpleFormat.parse(date1);
        Date toDate3 = simpleFormat.parse(date2);
        long from3 = fromDate3.getTime();
        long to3 = toDate3.getTime();
        double minutes = (double) ((to3 - from3) / (1000 * 60));
        System.out.println("两个时间之间的分钟差为：" + minutes);
        return minutes;
    }

    public static double seconds(String date1, String date2) throws ParseException {
        /*分钟差*/
        Date fromDate3 = simpleFormat1.parse(date1);
        Date toDate3 = simpleFormat1.parse(date2);
        long from3 = fromDate3.getTime();
        long to3 = toDate3.getTime();
        double seconds = (double) ((to3 - from3) / 1000);
        System.out.println("两个时间之间的秒钟差为：" + seconds);
        return seconds;
    }
}
