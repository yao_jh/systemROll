package com.my.business.sys.upload.controller;

import com.my.business.sys.common.entity.ResultData;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping(value = {"/file"})
public class UploadController {

    @Value("${upload_url}")
    private String file_url;  //从配置文件中获取文件路径

    /***
     * 单文件上传
     * @param file
     * @return
     */
    @CrossOrigin
    @PostMapping("singleupload")
    @ResponseBody
    public ResultData singleupload(MultipartFile file) {
        if (null != file) {
            String filename = file.getOriginalFilename();// 文件原名称
            String path = file_url + "/" + filename;
            File localFile = new File(path);

            try {
                file.transferTo(localFile);
                return ResultData.ResultDataSuccessSelf("上传成功", null);
            } catch (Exception e) {
                e.printStackTrace();
                return ResultData.ResultDataFaultSelf("上传失败", null);
            }
        } else {
            return ResultData.ResultDataFaultSelf("上传失败,文件为空", null);
        }
    }

    /***
     * uuid生成单文件上传
     * @param file
     * @return
     */
    @CrossOrigin
    @PostMapping("uuidsingleupload")
    @ResponseBody
    public ResultData uuidsingleupload(MultipartFile file) {
        if (null != file) {
            String filename = file.getOriginalFilename();// 文件原名称
            String new_uuid = UUID.randomUUID().toString();
            String suffix = filename.substring(filename.lastIndexOf(".") + 1);
            String new_file_name = new_uuid + "." + suffix;
            String path = file_url + new_file_name;
            File localFile = new File(path);
            try {
                file.transferTo(localFile);
                return ResultData.ResultDataSuccessSelf("上传成功", null);
            } catch (Exception e) {
                e.printStackTrace();
                return ResultData.ResultDataFaultSelf("上传失败", null);
            }
        } else {
            return ResultData.ResultDataFaultSelf("上传失败,文件为空", null);
        }
    }

    /***
     * 多文件上传
     * @return
     */
    @CrossOrigin
    @PostMapping("multiupload")
    @ResponseBody
    public ResultData multiupload(HttpServletRequest request) {
        List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("file");
        if (files.size() > 0) {
            for (MultipartFile file : files) {
                String filename = file.getOriginalFilename();// 文件原名称
                String path = file_url + filename;
                File localFile = new File(path);
                try {
                    file.transferTo(localFile);
                    return ResultData.ResultDataSuccessSelf("上传成功", null);
                } catch (Exception e) {
                    e.printStackTrace();
                    return ResultData.ResultDataFaultSelf("上传失败", null);
                }
            }
        }
        return ResultData.ResultDataFaultSelf("上传失败,文件为空", null);
    }

    /***
     * 生成uuid多文件上传
     * @return
     */
    @CrossOrigin
    @PostMapping("uuidmultiupload")
    @ResponseBody
    public ResultData uuidmultiupload(HttpServletRequest request) {
        List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("file");
        if (files.size() > 0) {
            for (MultipartFile file : files) {
                String filename = file.getOriginalFilename();// 文件原名称
                String new_uuid = UUID.randomUUID().toString();
                String suffix = filename.substring(filename.lastIndexOf(".") + 1);
                String new_file_name = new_uuid + "." + suffix;
                String path = file_url + new_file_name;
                File localFile = new File(path);
                try {
                    file.transferTo(localFile);
                    return ResultData.ResultDataSuccessSelf("上传成功", null);
                } catch (Exception e) {
                    e.printStackTrace();
                    return ResultData.ResultDataFaultSelf("上传失败", null);
                }
            }
        }
        return ResultData.ResultDataFaultSelf("上传失败,文件为空", null);
    }

    /**
     * 下载接口
     *
     * @return
     */
    @CrossOrigin
    @GetMapping("/download")
    @ResponseBody
    public ResultData uploadZip(String file_name, HttpServletRequest request, HttpServletResponse response) throws IOException {
        //构建文件地址
        String downLoadPath = file_url + "//" + file_name;
        //设置请求报文的首行格式信息
        response.setContentType("text/html;charset=utf-8");
        //设置响应报文的编译语言格式
        request.setCharacterEncoding("UTF-8");
        //流的创建
        java.io.BufferedInputStream bis = null;
        java.io.BufferedOutputStream bos = null;
        try {
            long fileLength = new File(downLoadPath).length();
            //设置响应报文的首行格式信息
            response.setContentType("application/json;");
            response.setHeader("Content-disposition", "attachment; filename=" + new String(file_name.getBytes("utf-8"), "ISO8859-1"));
            //设置响应报文的首行格式信息
            response.setHeader("Content-Length", String.valueOf(fileLength));

            //流对象的创建
            bis = new BufferedInputStream(new FileInputStream(downLoadPath));
            bos = new BufferedOutputStream(response.getOutputStream());

            //字节流的写入
            byte[] buff = new byte[2048];
            int bytesRead;
            while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, bytesRead);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("下载失败", null);
        } finally {
            if (bis != null)
                bis.close();
            if (bos != null)
                bos.close();
        }
        return ResultData.ResultDataSuccessSelf("下载成功", null);
    }


}
