package com.my.business.sys.echarts.service.impl;

import com.alibaba.fastjson.JSON;
import com.my.business.sys.common.dao.CommonDao;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.echarts.echartsUtils.EchartsUtils;
import com.my.business.sys.echarts.entity.JEchartsEntity;
import com.my.business.sys.echarts.service.EchartsService;
import com.my.business.sys.echartstable.dao.EchartstableDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

@Service
public class EchartsServiceImpl implements EchartsService {

    @Autowired
    private CommonDao commonDao;
    
    @Autowired
    private EchartstableDao echartsDao;

    /**
     * 根据参数获取map类型的数据，一般用于下拉,适用于任何只需要key value的数据
     * @param data 字符串json
     * @return
     * @author cc
     */
    @Override
    public ResultData findMap(String data) {
        try {
        	JEchartsEntity entity = JSON.parseObject(data, JEchartsEntity.class);
        	String sql = echartsDao.findDataEchartstableByKey(entity.getKey()).getStrsql();
        	if(!StringUtils.isEmpty(sql)) {
        		List<Map<String,Object>> list = commonDao.findManyData(sql);
        		EchartsUtils.getBasicLine(entity, list); //获取基本的折线图数据结构
                return ResultData.ResultDataSuccessSelf("获取成功", entity);
        	}else {
        		return ResultData.ResultDataFaultSelf("你配置的东西找不到,请查看配置项", null);
        	}
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("获取失败" + e.getMessage(), null);
        }
    }

}
