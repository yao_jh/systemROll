package com.my.business.sys.echarts.service;

import com.my.business.sys.common.entity.ResultData;

/***
 * 公共接口服务层
 * @author cc
 * @date 2019-01-17
 *
 */
public interface EchartsService {

    /**
     * 公共查询echarts接口
     *
     * @param data 字符串json
     * @return
     * @author cc
     */
    public ResultData findMap(String data);

}
