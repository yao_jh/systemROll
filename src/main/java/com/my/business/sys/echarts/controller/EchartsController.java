package com.my.business.sys.echarts.controller;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.echarts.service.EchartsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("echarts")
public class EchartsController {

    @Autowired
    private EchartsService echartsService;


    /***
     * 公共查询echarts数据接口
     * @param data
     * @return
     */
    @CrossOrigin
    @PostMapping("findData")
    @ResponseBody
    public ResultData findData(@RequestBody String data) {
        return echartsService.findMap(data);
    }


}
