package com.my.business.sys.echarts.entity;

import java.util.List;

/***
 * echarts的json实体类
 * 
 * @author chenchao
 *
 */
public class JEchartsEntity {

	// JEchartsEntity的内部类
	public static class EchartsEntity {
		private String name;
		private String[] data;
		private String type;
		private Boolean smooth;

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public Boolean getSmooth() {
			return smooth;
		}

		public void setSmooth(Boolean smooth) {
			this.smooth = smooth;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String[] getData() {
			return data;
		}

		public void setData(String[] data) {
			this.data = data;
		}
	}

	private String key;   //echarts的关键字
	private String type; // echarts种类
	private String[] legend; // 传说头数组
	private String[] x_axis; // x轴数组
	private List<EchartsEntity> data; // 数据集合
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String[] getLegend() {
		return legend;
	}

	public void setLegend(String[] legend) {
		this.legend = legend;
	}

	public String[] getX_axis() {
		return x_axis;
	}

	public void setX_axis(String[] x_axis) {
		this.x_axis = x_axis;
	}

	public List<EchartsEntity> getData() {
		return data;
	}

	public void setData(List<EchartsEntity> data) {
		this.data = data;
	}

}
