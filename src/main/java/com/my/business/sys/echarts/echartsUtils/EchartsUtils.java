package com.my.business.sys.echarts.echartsUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.my.business.sys.echarts.entity.JEchartsEntity;
import com.my.business.sys.echarts.entity.JEchartsEntity.EchartsEntity;

/***
 * echarts帮助类
 * @author chenchao
 *
 */
public class EchartsUtils {
	
	/***
	 * 获取基本折线图方法(横坐标为1到12月，需要构造name,加上f1~f12这些字段)
	 * @param entity  echarts的json类
	 * @param list  数据的list的map集合
	 * @return
	 */
	public static JEchartsEntity getBasicLine(JEchartsEntity entity,List<Map<String,Object>> list) {
		String[] axis = entity.getX_axis();  //x轴
    	int i = axis.length;  //获取x轴的个数
    	int count_list = list.size();  //数组长度，用于计算传说头legend的个数
    	int legend_count = 0;   //传说头计算计数器
    	List<EchartsEntity> entity_list = new ArrayList<EchartsEntity>();
    	String[] legend_value = new String[count_list];
    	if(list.size() > 0) {   //如果查询结果不为空，则开始加载数据
    		for(Map<String,Object> map :list) {    //遍历list集合
    			String[] data_value = new String[i];
    			EchartsEntity e = new EchartsEntity();
        		for (Map.Entry<String, Object> m : map.entrySet()) {
        			if(m.getKey().equals("NAME")) {   //如果是name字段（name字段表示折线图中各个数据name属性的名字），则把年份数据赋值给name属性
        				e.setName(m.getValue().toString());
        				legend_value[legend_count] = m.getValue().toString();
        				legend_count++;
        			}else if(m.getKey().equals("F1")){   //如果是x轴的数据
        				data_value[0] = m.getValue().toString();
        			}else if(m.getKey().equals("F2")){   //如果是x轴的数据
        				data_value[1] = m.getValue().toString();
        			}else if(m.getKey().equals("F3")){   //如果是x轴的数据
        				data_value[2] = m.getValue().toString();
        			}else if(m.getKey().equals("F4")){   //如果是x轴的数据
        				data_value[3] = m.getValue().toString();
        			}else if(m.getKey().equals("F5")){   //如果是x轴的数据
        				data_value[4] = m.getValue().toString();
        			}else if(m.getKey().equals("F6")){   //如果是x轴的数据
        				data_value[5] = m.getValue().toString();
        			}else if(m.getKey().equals("F7")){   //如果是x轴的数据
        				data_value[6] = m.getValue().toString();
        			}else if(m.getKey().equals("F8")){   //如果是x轴的数据
        				data_value[7] = m.getValue().toString();
        			}else if(m.getKey().equals("F9")){   //如果是x轴的数据
        				data_value[8] = m.getValue().toString();
        			}else if(m.getKey().equals("F10")){   //如果是x轴的数据
        				data_value[9] = m.getValue().toString();
        			}else if(m.getKey().equals("F11")){   //如果是x轴的数据
        				data_value[10] = m.getValue().toString();
        			}else if(m.getKey().equals("F12")){   //如果是x轴的数据
        				data_value[11] = m.getValue().toString();
        			}
//        			else{   //如果是x轴的数据
//        				if(count<i) {   //先判断计数器是否超过x轴的个数,没有超过则开始计数
//        					data_value[count] = m.getValue().toString();
//        					count++;
//        				}
//        			}
        	    }
        		e.setType("line");
        		e.setSmooth(true);
        		e.setData(data_value);
        		entity_list.add(e);
        	}
    	}
    	entity.setData(entity_list);
    	entity.setLegend(legend_value);
    	return entity;
	}

}
