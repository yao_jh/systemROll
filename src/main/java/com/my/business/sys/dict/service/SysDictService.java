package com.my.business.sys.dict.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.dict.entity.SysDict;

import java.util.List;


/**
 * 数据字典接口服务类
 *
 * @author 生成器生成
 * @date 2019-02-24 14:58:56
 */
public interface SysDictService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataSysDict(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataSysDictOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataSysDictMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData updateDataSysDict(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysDictByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysDictByIndocno(String data);

    /***
     * 根据编码查询数据字典
     * @param dictcode 字典编码
     */
    public ResultData getDictByCode(String dictcode);

    /**
     * 查看记录
     */
    public List<SysDict> findDataSysDict();
}
