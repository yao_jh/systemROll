package com.my.business.sys.dict.entity;

import java.util.List;

/**
 * 下拉数据字典实体类
 *
 * @author cc
 * @date 2019-01-12
 */
public class DpdEntity {

    private String key;
    private String value;
    private List<DpdEntity> children;
    private String valueName ;

    public String getValueName() {
        return valueName;
    }

    public void setValueName(String valueName) {
        this.valueName = valueName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

	public List<DpdEntity> getChildren() {
		return children;
	}

	public void setChildren(List<DpdEntity> children) {
		this.children = children;
	}
}
