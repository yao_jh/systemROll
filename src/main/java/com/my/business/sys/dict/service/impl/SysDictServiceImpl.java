package com.my.business.sys.dict.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.dict.dao.SysDictDao;
import com.my.business.sys.dict.entity.DpdEntity;
import com.my.business.sys.dict.entity.JSysDict;
import com.my.business.sys.dict.entity.SysDict;
import com.my.business.sys.dict.service.SysDictService;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 数据字典接口具体实现类
 *
 * @author 生成器生成
 * @date 2019-02-24 14:58:56
 */
@Service
public class SysDictServiceImpl implements SysDictService {

    @Autowired
    private SysDictDao sysDictDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataSysDict(String data, Long userId, String sname) {
        try {
            JSysDict jsysDict = JSON.parseObject(data, JSysDict.class);
            SysDict sysDict = jsysDict.getSysDict();
            CodiUtil.newRecord(userId, sname, sysDict);
            sysDictDao.insertDataSysDict(sysDict);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    ;

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataSysDictOne(Long indocno) {
        try {
            sysDictDao.deleteDataSysDictOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    ;

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataSysDictMany(String str_id) {
        try {
            String sql = "delete from sys_dict where indocno in(" + str_id + ")";
            sysDictDao.deleteDataSysDictMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param data sysUser 对象实体
     */
    public ResultData updateDataSysDict(String data, Long userId, String sname) {
        try {
            JSysDict jsysDict = JSON.parseObject(data, JSysDict.class);
            SysDict sysDict = jsysDict.getSysDict();
            CodiUtil.editRecord(userId, sname, sysDict);
            sysDictDao.updateDataSysDict(sysDict);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    ;

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysDictByPage(String data) {
        try {
            JSysDict jsysDict = JSON.parseObject(data, JSysDict.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jsysDict.getPageIndex();
            Integer pageSize = jsysDict.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jsysDict.getCondition()) {
                jsonObject = JSON.parseObject(jsysDict.getCondition().toString());
            }

            List<SysDict> list = sysDictDao.findDataSysDictByPage(pageIndex, pageSize);
            Integer count = sysDictDao.findDataSysDictByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysDictByIndocno(String data) {
        try {
            JSysDict jsysDict = JSON.parseObject(data, JSysDict.class);
            Long indocno = jsysDict.getIndocno();

            SysDict sysDict = sysDictDao.findDataSysDictByIndocno(indocno);
            return ResultData.ResultDataSuccess(sysDict);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData getDictByCode(String data) {
        try {
            JSysDict jsysDict = JSON.parseObject(data, JSysDict.class);
            JSONObject jsonObject = null;

            if (null != jsysDict.getCondition()) {
                jsonObject = JSON.parseObject(jsysDict.getCondition().toString());
            }

            String code = null;
            if (!StringUtils.isEmpty(jsonObject.get("code"))) {
                code = jsonObject.get("code").toString();
            }

            List<DpdEntity> dictList = sysDictDao.getDictByCode(code);
            return ResultData.ResultDataSuccess(dictList);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<SysDict> findDataSysDict() {
        List<SysDict> list = sysDictDao.findDataSysDict();
        return list;
    }

    ;
}
