package com.my.business.sys.dict.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 数据字典实体类
 *
 * @author 生成器生成
 * @date 2019-02-24 14:58:55
 */
public class SysDict extends BaseEntity {

    private Long indocno;  //主键
    private String dictcode;  //编码
    private String dictname;  //名称
    private Long iparent;  //父节点ID
    private String realvalue;   //实际值
    private String showvalue;    //显示值

    public String getRealvalue() {
        return realvalue;
    }

    public void setRealvalue(String realvalue) {
        this.realvalue = realvalue;
    }

    public String getShowvalue() {
        return showvalue;
    }

    public void setShowvalue(String showvalue) {
        this.showvalue = showvalue;
    }

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getDictcode() {
        return this.dictcode;
    }

    public void setDictcode(String dictcode) {
        this.dictcode = dictcode;
    }

    public String getDictname() {
        return this.dictname;
    }

    public void setDictname(String dictname) {
        this.dictname = dictname;
    }

    public Long getIparent() {
        return this.iparent;
    }

    public void setIparent(Long iparent) {
        this.iparent = iparent;
    }


}