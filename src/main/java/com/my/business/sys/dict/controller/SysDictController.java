package com.my.business.sys.dict.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.dict.entity.JSysDict;
import com.my.business.sys.dict.entity.SysDict;
import com.my.business.sys.dict.service.SysDictService;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 数据字典控制器层
 *
 * @author 生成器生成
 * @date 2019-02-24 14:58:56
 */
@RestController
@RequestMapping("/sysDict")
public class SysDictController {

    @Autowired
    private SysDictService sysDictService;

    /**
     * 添加记录
     *
     * @param data       数据实体
     * @param loginToken userId 用户id sname  用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataSysDict(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return sysDictService.insertDataSysDict(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysDictOne(@RequestBody String data) {
        try {
            JSysDict jsysDict = JSON.parseObject(data, JSysDict.class);
            return sysDictService.deleteDataSysDictOne(jsysDict.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JSysDict jsysDict = JSON.parseObject(data, JSysDict.class);
            return sysDictService.deleteDataSysDictMany(jsysDict.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataSysDict(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return sysDictService.updateDataSysDict(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysDictByPage(@RequestBody String data) {
        return sysDictService.findDataSysDictByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysDictByIndocno(@RequestBody String data) {
        return sysDictService.findDataSysDictByIndocno(data);
    }

    /**
     * 根据编码获取数据字典的集合
     *
     * @param data code 数据字典编码
     */
    @CrossOrigin
    @RequestMapping(value = {"/getDataByCode"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData getDataByCode(@RequestBody String data) {
        return sysDictService.getDictByCode(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<SysDict> findDataSysDict() {
        return sysDictService.findDataSysDict();
    }

}
