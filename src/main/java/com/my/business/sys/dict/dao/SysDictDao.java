package com.my.business.sys.dict.dao;

import com.my.business.sys.dict.entity.DpdEntity;
import com.my.business.sys.dict.entity.SysDict;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 数据字典dao接口
 *
 * @author 生成器生成
 * @date 2019-02-24 14:58:56
 */
@Mapper
public interface SysDictDao {

    /**
     * 添加记录
     *
     * @param sysDict 对象实体
     */
    public void insertDataSysDict(SysDict sysDict);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public void deleteDataSysDictOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    public void deleteDataSysDictMany(String value);

    /**
     * 修改记录
     *
     * @param sysDict 对象实体
     */
    public void updateDataSysDict(SysDict sysDict);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<SysDict> findDataSysDictByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    public Integer findDataSysDictByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public SysDict findDataSysDictByIndocno(@Param("indocno") Long indocno);

    /***
     * 根据编码查询数据字典
     * @param dictcode 字典编码
     * @return DpdEntity集合
     */
    public List<DpdEntity> getDictByCode(@Param("dictcode") String dictcode);

    /***
     * 根据编码和真实值查询数据字典对象
     * @param dictcode 字典编码
     * @return DpdEntity集合
     */
    public SysDict getDictByCodeAndValue(@Param("dictcode") String dictcode, @Param("realvalue") String realvalue);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    public List<SysDict> findDataSysDict();

}
