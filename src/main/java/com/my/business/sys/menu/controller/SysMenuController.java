package com.my.business.sys.menu.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.menu.entity.JSysMenu;
import com.my.business.sys.menu.entity.SysMenu;
import com.my.business.sys.menu.service.SysMenuService;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 菜单控制器层
 *
 * @author 生成器生成
 * @date 2019-01-11 16:58:55
 */
@RestController
@RequestMapping("/sysMenu")
public class SysMenuController {

    @Autowired
    private SysMenuService sysMenuService;

    /**
     * 添加记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataSysMenu(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return sysMenuService.insertDataSysMenu(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysMenuOne(@RequestBody String data) {
        try {
            JSysMenu jsysMenu = JSON.parseObject(data, JSysMenu.class);
            return sysMenuService.deleteDataSysMenuOne(jsysMenu.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JSysMenu jsysMenu = JSON.parseObject(data, JSysMenu.class);
            return sysMenuService.deleteDataSysMenuMany(jsysMenu.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataSysMenu(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return sysMenuService.updateDataSysMenu(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysMenuByPage(@RequestBody String data) {
        return sysMenuService.findDataSysMenuByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysMenuByIndocno(@RequestBody String data) {
        return sysMenuService.findDataSysMenuByIndocno(data);
    }

    /**
     * 根据用户id和父节点查询子级节点
     *
     * @param data    json字符串
     * @param userId  用户id
     * @param indocno 父节点
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findMenuChild"}, method = RequestMethod.POST)
    public ResultData findDataSysMenuChild(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        try {
            JCommon common = JSON.parseObject(loginToken, JCommon.class);
            Long userId = common.getUserId();
            JSysMenu jsysMenu = JSON.parseObject(data, JSysMenu.class);
            Long indocno = jsysMenu.getIndocno();
            List<SysMenu> list = sysMenuService.findDataSysMenuChildByUserIdAndIparent(userId, indocno);
            return ResultData.ResultDataSuccessSelf("访问菜单成功", list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultGd(e.getMessage(), null);
        }
    }

    /**
     * 查询快捷菜单，生成主页面快捷方式
     *
     */
    @CrossOrigin
    @RequestMapping(value = {"/always"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findAlwaysMenu(@RequestHeader(value = "loginToken") String loginToken) {
        try {
            JCommon common = JSON.parseObject(loginToken, JCommon.class);
            Long userId = common.getUserId();
            List<SysMenu> list = sysMenuService.findAlwaysMenu(userId);
            return ResultData.ResultDataSuccessSelf("查询快捷菜单成功", list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultGd(e.getMessage(), null);
        }
    }
}
