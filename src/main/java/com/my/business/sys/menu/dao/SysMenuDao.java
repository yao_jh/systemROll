package com.my.business.sys.menu.dao;


import com.my.business.sys.menu.entity.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 菜单dao接口
 *
 * @author 生成器生成
 * @date 2019-01-11 16:58:55
 */
@Mapper
public interface SysMenuDao {

    /**
     * 添加记录
     *
     * @param sysMenu 对象实体
     */
    public void insertDataSysMenu(SysMenu sysMenu);

    /**
     * 添加角色菜单关系记录
     *
     * @param roleId 角色id
     * @param menuId 菜单id
     * @param always_menu 快捷菜单（0=否，1=是）
     */
    public void insertDataRoleMenu(@Param("roleId") Long roleId, @Param("menuId") Long menuId, @Param("always_menu") Long always_menu);

    /**
     * 根据角色id删除所有所属菜单
     *
     * @param roleId 角色id
     */
    public void deleteMenuByRoleId(@Param("roleId") Long roleId);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public void deleteDataSysMenuOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    public void deleteDataSysMenuMany(String value);

    /**
     * 修改记录
     *
     * @param sysMenu 对象实体
     */
    public void updateDataSysMenu(SysMenu sysMenu);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<SysMenu> findDataSysMenuByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("menu_name") String menu_name, @Param("menu_level") Long menu_level);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    public Integer findDataSysMenuByPageSize(@Param("menu_name") String menu_name, @Param("menu_level") Long menu_level);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public SysMenu findDataSysMenuByIndocno(@Param("indocno") Long indocno);

    /***
     * 根据主键查询菜单集合信息
     * @param roleId 角色id
     * @return
     */
    public List<SysMenu> findDataMenuListByIndocno(@Param("roleId") Long roleId);


    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    public List<SysMenu> findDataSysMenu();

    /***
     * 根据人员角色id查询根菜单
     * @param value  拼接的sql语句
     * @return 菜单集合
     */
    public List<SysMenu> findDataSysMenuByRole(String value);

    /***
     * 根据人员角色id,菜单id 查询菜单是否为快捷菜单
     * @param value  拼接的sql语句
     * @return 菜单集合
     */
    public Long findDataSysMenuByRoleAndMenu(String value);

    /***
     * 根据菜单主键查询子集合
     * @param value  拼接的sql语句
     * @return 菜单集合
     */
    public List<SysMenu> findDataSysMenuByIparent(String value);

}
