package com.my.business.sys.menu.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.util.List;

/**
 * 菜单实体类
 *
 * @author 生成器生成
 * @date 2019-02-20 15:33:43
 */
public class SysMenu extends BaseEntity {

    private Long menu_level; // 菜单等级
    private Long indocno; // 主键
    private String menu_name; // 菜单名称
    private Long iparent; // 上级菜单主键
    private String sparent; // 上级菜单名称
    private String menu_icon; // 图标
    private String menu_url; // URL
    private Long always_menu; //快捷菜单
    private List<SysMenu> child;

    public List<SysMenu> getChild() {
        return child;
    }

    public void setChild(List<SysMenu> child) {
        this.child = child;
    }

    public Long getMenu_level() {
        return this.menu_level;
    }

    public void setMenu_level(Long menu_level) {
        this.menu_level = menu_level;
    }

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getMenu_name() {
        return this.menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public Long getIparent() {
        return this.iparent;
    }

    public void setIparent(Long iparent) {
        this.iparent = iparent;
    }

    public String getSparent() {
        return this.sparent;
    }

    public void setSparent(String sparent) {
        this.sparent = sparent;
    }

    public String getMenu_icon() {
        return this.menu_icon;
    }

    public void setMenu_icon(String menu_icon) {
        this.menu_icon = menu_icon;
    }

    public String getMenu_url() {
        return this.menu_url;
    }

    public void setMenu_url(String menu_url) {
        this.menu_url = menu_url;
    }

    public Long getAlways_menu() {
        return always_menu;
    }

    public void setAlways_menu(Long always_menu) {
        this.always_menu = always_menu;
    }
}