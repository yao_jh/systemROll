package com.my.business.sys.menu.entity;

/**
 * 角色菜单实体类
 *
 * @author 生成器生成
 * @date 2019-02-20 13:42:25
 */
public class SysRoleMenu {

    private Long indocno;  //主键
    private Long menu_id;  //菜单ID
    private Long role_id;  //角色ID
    private Long always_menu; //快捷菜单判断

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(Long menu_id) {
        this.menu_id = menu_id;
    }

    public Long getRole_id() {
        return this.role_id;
    }

    public void setRole_id(Long role_id) {
        this.role_id = role_id;
    }

    public Long getAlways_menu() {
        return always_menu;
    }

    public void setAlways_menu(Long always_menu) {
        this.always_menu = always_menu;
    }
}