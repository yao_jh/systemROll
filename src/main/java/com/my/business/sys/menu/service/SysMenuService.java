package com.my.business.sys.menu.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.menu.entity.SysMenu;

import java.util.List;

/**
 * 菜单接口服务类
 *
 * @author 生成器生成
 * @date 2019-01-11 16:58:55
 */
public interface SysMenuService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataSysMenu(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataSysMenuOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataSysMenuMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData updateDataSysMenu(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysMenuByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysMenuByIndocno(String data);

    /**
     * 根据登录人id获取该人的一级菜单权限
     *
     * @param userId 登录人id
     * @return 菜单集合
     */
    public List<SysMenu> findDataSysMenuFirstByUserId(Long userId);

    /**
     * 根据父菜单主键和人员id获取菜单集合
     *
     * @param userId  登录人id
     * @param iparent 菜单父节点
     * @return 菜单集合
     */
    public List<SysMenu> findDataSysMenuChildByUserIdAndIparent(Long userId, Long iparent);

    /**
     * 根据登录人id获取该人的菜单权限
     *
     * @param userId 登录人id
     * @return 菜单集合
     */
    public List<SysMenu> findDataSysMenuAllByUserId(Long userId);

    /**
     * 查询快捷菜单，生成主页面快捷方式
     * @param userId 用户id
     * @return 菜单集合
     */
    List<SysMenu> findAlwaysMenu(Long userId);
}
