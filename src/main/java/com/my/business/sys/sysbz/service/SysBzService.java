package com.my.business.sys.sysbz.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.sysbz.entity.SysBz;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * 班组排班表接口服务类
 *
 * @author 生成器生成
 * @date 2020-11-30 11:13:20
 */
public interface SysBzService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataSysBz(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataSysBzOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataSysBzMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataSysBz(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataSysBzByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataSysBzByIndocno(String data);

    /**
     * 查看记录
     */
    List<SysBz> findDataSysBz();

    /**
     * 计算班组班次
     * @param time 时间 yyyy-MM-dd hh:mm:ss
     * @return
     */
    Map<String, String> findbz(String time) throws ParseException;

    Map<String, String> findbznew(String time) throws ParseException;

}
