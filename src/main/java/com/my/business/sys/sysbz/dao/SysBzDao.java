package com.my.business.sys.sysbz.dao;


import com.my.business.sys.sysbz.entity.SysBz;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 班组排班表dao接口
 *
 * @author 生成器生成
 * @date 2020-11-30 11:13:20
 */
@Mapper
public interface SysBzDao {

    /**
     * 添加记录
     *
     * @param sysBz 对象实体
     */
    void insertDataSysBz(SysBz sysBz);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataSysBzOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataSysBzMany(String value);

    /**
     * 修改记录
     *
     * @param sysBz 对象实体
     */
    void updateDataSysBz(SysBz sysBz);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<SysBz> findDataSysBzByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataSysBzByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    SysBz findDataSysBzByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<SysBz> findDataSysBz();

    void updateDataSysBzByMcMessage(SysBz sysBz);
}
