package com.my.business.sys.sysbz.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.sysbz.entity.JSysBz;
import com.my.business.sys.sysbz.entity.SysBz;
import com.my.business.sys.sysbz.service.SysBzService;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 班组排班表控制器层
 *
 * @author 生成器生成
 * @date 2020-11-30 11:13:20
 */
@RestController
@RequestMapping("/sysBz")
public class SysBzController {

    @Autowired
    private SysBzService sysBzService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataSysBz(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return sysBzService.insertDataSysBz(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysBzOne(@RequestBody String data) {
        try {
            JSysBz jsysBz = JSON.parseObject(data, JSysBz.class);
            return sysBzService.deleteDataSysBzOne(jsysBz.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JSysBz jsysBz = JSON.parseObject(data, JSysBz.class);
            return sysBzService.deleteDataSysBzMany(jsysBz.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataSysBz(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return sysBzService.updateDataSysBz(data, userId, CodiUtil.returnLm(sname));
    }


    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysBzByPage(@RequestBody String data) {
        return sysBzService.findDataSysBzByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysBzByIndocno(@RequestBody String data) {
        return sysBzService.findDataSysBzByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<SysBz> findDataSysBz() {
        return sysBzService.findDataSysBz();
    }

    /**
     * 计算当前班组班次接口
     * @return map
     */
    @CrossOrigin
    @RequestMapping(value = {"/findbz"}, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, String> findbz(){
        try {
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sysBzService.findbz(sdf.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 计算当前班组班次接口  New  甲 乙 丙
     * @return map
     */
    @CrossOrigin
    @RequestMapping(value = {"/findbznew"}, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, String> findbznew(){
        try {
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sysBzService.findbznew(sdf.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
