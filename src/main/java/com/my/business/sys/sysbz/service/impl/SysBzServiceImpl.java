package com.my.business.sys.sysbz.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.sysbz.dao.SysBzDao;
import com.my.business.sys.sysbz.entity.JSysBz;
import com.my.business.sys.sysbz.entity.SysBz;
import com.my.business.sys.sysbz.service.SysBzService;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 班组排班表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-11-30 11:13:20
 */
@Service
public class SysBzServiceImpl implements SysBzService {

    @Autowired
    private SysBzDao sysBzDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataSysBz(String data, Long userId, String sname) {
        try {
            JSysBz jsysBz = JSON.parseObject(data, JSysBz.class);
            SysBz sysBz = jsysBz.getSysBz();
            CodiUtil.newRecord(userId, sname, sysBz);
            sysBzDao.insertDataSysBz(sysBz);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataSysBzOne(Long indocno) {
        try {
            sysBzDao.deleteDataSysBzOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataSysBzMany(String str_id) {
        try {
            String sql = "delete sys_bz where indocno in(" + str_id + ")";
            sysBzDao.deleteDataSysBzMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataSysBz(String data, Long userId, String sname) {
        try {
            JSysBz jsysBz = JSON.parseObject(data, JSysBz.class);
            SysBz sysBz = jsysBz.getSysBz();
            CodiUtil.editRecord(userId, sname, sysBz);
            sysBzDao.updateDataSysBz(sysBz);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysBzByPage(String data) {
        try {
            JSysBz jsysBz = JSON.parseObject(data, JSysBz.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jsysBz.getPageIndex();
            Integer pageSize = jsysBz.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jsysBz.getCondition()) {
                jsonObject = JSON.parseObject(jsysBz.getCondition().toString());
            }

            List<SysBz> list = sysBzDao.findDataSysBzByPage((pageIndex - 1) * pageSize, pageSize);
            Integer count = sysBzDao.findDataSysBzByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysBzByIndocno(String data) {
        try {
            JSysBz jsysBz = JSON.parseObject(data, JSysBz.class);
            Long indocno = jsysBz.getIndocno();

            SysBz sysBz = sysBzDao.findDataSysBzByIndocno(indocno);
            return ResultData.ResultDataSuccess(sysBz);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<SysBz> findDataSysBz() {
        List<SysBz> list = sysBzDao.findDataSysBz();
        return list;
    }

    /**
     * 计算班组
     * 1.取磨削开始时间减去2020-01-01，再除以8，取余数，加1可得班组排序主键
     * 2.根据磨削开始时间后半截时间，形成班次排序
     * 3.综合可得班组和班次
     */
    public Map<String, String> findbznew(String time) throws ParseException {
        String st1 = "2020-01-01 00:00:00";
        String st2 = time;

        //计算班组
        String from1 = st1.substring(0,10);
        String to1 = st2.substring(0,10);
        SimpleDateFormat simpleFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = simpleFormat1.parse(from1);
        Date date2 = simpleFormat1.parse(to1);
        int a = (int) ((date2.getTime() - date1.getTime()) / (1000*3600*24));
        int yu = (a + 1) % 8; //取余数
        if (yu == 0){
            yu = 8;
        }else if (yu < 0){
            yu = 8 - Math.abs(yu);
        }
        SysBz sysBz = sysBzDao.findDataSysBzByIndocno((long) yu);

        //计算班次及对应班组
        String bcs = st2.substring(11,13);
        String bc = null;
        int i = 0;
        if (Long.parseLong("08") <= Long.parseLong(bcs) && Long.parseLong("16") > Long.parseLong(bcs)){
            i = 1;
        }else if (Long.parseLong("16") <= Long.parseLong(bcs) && Long.parseLong("23") >= Long.parseLong(bcs)){
            i = 2;
        }else if (Long.parseLong("00") <= Long.parseLong(bcs) && Long.parseLong("08") > Long.parseLong(bcs)){
            i = 3;
        }
        String bz = null;
        if (i == 1){
            bz = sysBz.getFirstbz();
            bc = "白班";
        }else if (i == 2){
            bz = sysBz.getSecondbz();
            bc = "小夜班";
        }else if (i == 3){
            bz = sysBz.getThirdbz();
            bc = "大夜班";
        }else {
            bz = null;
            bc = null;
        }
        Map<String,String> map = new HashMap<>();
        map.put("bz",bz);
        map.put("bc",bc);
        return map;
    }


    public Map<String, String> findbz(String time) throws ParseException {
        String st1 = "2020-01-06 00:00:00";
        String st2 = time;

        //计算班组
        String from1 = st1.substring(0,10);
        String to1 = st2.substring(0,10);
        SimpleDateFormat simpleFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = simpleFormat1.parse(from1);
        Date date2 = simpleFormat1.parse(to1);
        int a = (int) ((date2.getTime() - date1.getTime()) / (1000*3600*24));
        //计算班次及对应班组
        String bcs = st2.substring(11,13);
        String bc = null;
        int i = 0;
        if (Long.parseLong("08") <= Long.parseLong(bcs) && Long.parseLong("20") > Long.parseLong(bcs)){
            i = 1;
        }else if ((Long.parseLong("20") <= Long.parseLong(bcs) && Long.parseLong("23") >= Long.parseLong(bcs))){
            i = 2;
        }else if(Long.parseLong(bcs) <  Long.parseLong("08")){
            i=2;
            a=a-1;
        }


        int yu = (a + 1) % 6; //取余数
        if (yu == 0){
            yu = 6;
        }else if (yu < 0){
            yu = 6 - Math.abs(yu);
        }
        SysBz sysBz = sysBzDao.findDataSysBzByIndocno((long) (yu+100));


        String bz = null;
        if (i == 1){
            bz = sysBz.getDayshift();
            bc = "早班";
        }else if (i == 2){
            bz = sysBz.getNightshift();
            bc = "夜班";
        }else {
            bz = null;
            bc = null;
        }
        Map<String,String> map = new HashMap<>();
        map.put("bz",bz);
        map.put("bc",bc);
        map.put("id",yu+"");
        return map;
    }
}
