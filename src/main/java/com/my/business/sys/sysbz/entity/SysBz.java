package com.my.business.sys.sysbz.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 班组排班表实体类
 *
 * @author 生成器生成
 * @date 2020-11-30 11:13:20
 */
public class SysBz extends BaseEntity {

    private Long indocno;  //主键
    private String firstbz;  //白班组
    private String secondbz;  //小夜班班组
    private String thirdbz;  //大夜班班组

    private String dayshift;//白班
    private String nightshift;//夜班


    private String j1;//甲班一号磨床
    private String y1;//乙班一号磨床
    private String b1;//丙班一号磨床

    private String j2;//甲班2号磨床
    private String y2;//乙班2号磨床
    private String b2;//丙班2号磨床

    private String j3;//甲班3号磨床
    private String y3;//乙班3号磨床
    private String b3;//丙班3号磨床

    private String j4;//甲班4号磨床
    private String y4;//乙班4号磨床
    private String b4;//丙班4号磨床

    private String j5;//甲班5号磨床
    private String y5;//乙班5号磨床
    private String b5;//丙班5号磨床

    public String getJ1() {
        return j1;
    }

    public void setJ1(String j1) {
        this.j1 = j1;
    }

    public String getY1() {
        return y1;
    }

    public void setY1(String y1) {
        this.y1 = y1;
    }

    public String getB1() {
        return b1;
    }

    public void setB1(String b1) {
        this.b1 = b1;
    }

    public String getJ2() {
        return j2;
    }

    public void setJ2(String j2) {
        this.j2 = j2;
    }

    public String getY2() {
        return y2;
    }

    public void setY2(String y2) {
        this.y2 = y2;
    }

    public String getB2() {
        return b2;
    }

    public void setB2(String b2) {
        this.b2 = b2;
    }

    public String getJ3() {
        return j3;
    }

    public void setJ3(String j3) {
        this.j3 = j3;
    }

    public String getY3() {
        return y3;
    }

    public void setY3(String y3) {
        this.y3 = y3;
    }

    public String getB3() {
        return b3;
    }

    public void setB3(String b3) {
        this.b3 = b3;
    }

    public String getJ4() {
        return j4;
    }

    public void setJ4(String j4) {
        this.j4 = j4;
    }

    public String getY4() {
        return y4;
    }

    public void setY4(String y4) {
        this.y4 = y4;
    }

    public String getB4() {
        return b4;
    }

    public void setB4(String b4) {
        this.b4 = b4;
    }

    public String getJ5() {
        return j5;
    }

    public void setJ5(String j5) {
        this.j5 = j5;
    }

    public String getY5() {
        return y5;
    }

    public void setY5(String y5) {
        this.y5 = y5;
    }

    public String getB5() {
        return b5;
    }

    public void setB5(String b5) {
        this.b5 = b5;
    }

    public String getDayshift() {
        return dayshift;
    }

    public void setDayshift(String dayshift) {
        this.dayshift = dayshift;
    }

    public String getNightshift() {
        return nightshift;
    }

    public void setNightshift(String nightshift) {
        this.nightshift = nightshift;
    }

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getFirstbz() {
        return this.firstbz;
    }

    public void setFirstbz(String firstbz) {
        this.firstbz = firstbz;
    }

    public String getSecondbz() {
        return this.secondbz;
    }

    public void setSecondbz(String secondbz) {
        this.secondbz = secondbz;
    }

    public String getThirdbz() {
        return this.thirdbz;
    }

    public void setThirdbz(String thirdbz) {
        this.thirdbz = thirdbz;
    }


}