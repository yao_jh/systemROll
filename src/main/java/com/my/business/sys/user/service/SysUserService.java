package com.my.business.sys.user.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.user.entity.SysUser;

import java.util.List;

/**
 * 人员信息接口服务类
 *
 * @author 生成器生成
 * @date 2019-01-11 15:05:48
 */
public interface SysUserService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataSysUser(String data, Long userId, String sname);
    
    /**
     * 宝信添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataSysUserBx(String data);

    /**
     * 添加用户角色
     *
     * @param str_id 角色id字符串
     * @param userId 人员id
     */
    public ResultData insertDataUserRole(String str_id, Long userId);

    /**
     * 根据主键删除单个对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataSysUserOne(Long indocno);
    
    /**
     * 根据账号删除单个对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataSysUserByAccount(String account);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataSysUserMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData updateDataSysUser(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysUserByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysUserByIndocno(String data);

    /**
     * 查看记录
     */
    public List<SysUser> findDataSysUser();

    /***
     * 根据账号和密码返回对象
     * @param account  账号
     * @param password  密码
     * @return
     */
    public SysUser findDataByAccountAndPassword(String account, String password);

    /**
     * 修改用户密码
     *
     * @param data 参数字符串
     */
    public ResultData updatePassword(String data);

    /**
     * 单点登录
     * @param account 账户
     * @return
     */
    SysUser findDataByAccount(String account);
}
