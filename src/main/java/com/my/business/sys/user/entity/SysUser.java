package com.my.business.sys.user.entity;

import com.my.business.sys.common.entity.BaseEntity;
import com.my.business.sys.role.entity.SysRole;

import java.util.List;

/**
 * 人员信息实体类
 *
 * @author 生成器生成
 * @date 2019-01-11 15:00:25
 */
public class SysUser extends BaseEntity {

    private Long indocno;  //主键
    private String photo;  //头像
    private String account;  //账号
    private String password;  //密码
    private String sname;  //姓名
    private Long sex;  //性别
    private Long areaid;  //作业区id
    private String area;  //作业区
    private Long companyid;   //产线id
    private String company;  //产线

    private String list_roleId;   //角色Id   添加字段
    private List<SysRole> sysRole;   //角色集合
    public String getList_roleId() {
        return list_roleId;
    }

    public void setList_roleId(String list_roleId) {
        this.list_roleId = list_roleId;
    }

    public Long getAreaid() {
        return areaid;
    }

    public void setAreaid(Long areaid) {
        this.areaid = areaid;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Long getCompanyid() {
        return companyid;
    }

    public void setCompanyid(Long companyid) {
        this.companyid = companyid;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAccount() {
        return this.account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSname() {
        return this.sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Long getSex() {
        return this.sex;
    }

    public void setSex(Long sex) {
        this.sex = sex;
    }

    public List<SysRole> getSysRole() {
        return sysRole;
    }

    public void setSysRole(List<SysRole> sysRole) {
        this.sysRole = sysRole;
    }
}