package com.my.business.sys.user.dao;


import com.my.business.sys.user.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 人员信息dao接口
 *
 * @author 生成器生成
 * @date 2019-01-11 15:00:25
 */
@Mapper
public interface SysUserDao {

    /**
     * 添加记录
     *
     * @param sysUser 对象实体
     */
    public void insertDataSysUser(SysUser sysUser);

    /**
     * 根据主键删除单个对象
     *
     * @param indocno 对象主键
     */
    public void deleteDataSysUserOne(@Param("indocno") Long indocno);
    
    /**
     * 根据账号删除单个对象
     *
     * @param indocno 对象主键
     */
    public void deleteDataSysUserByAccount(@Param("account") String account);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    public void deleteDataSysUserMany(String value);

    /**
     * 修改记录
     *
     * @param sysUser 对象实体
     */
    public void updateDataSysUser(SysUser sysUser);

    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @param sname     人名
     * @param account   账号
     * @return 对象数据集合
     */
    public List<SysUser> findDataSysUserByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,
                                               @Param("sname") String sname, @Param("account") String account);

    /**
     * 根据条件查看记录的总数
     *
     * @param sname   人名
     * @param account 账号
     * @return 对象数据集合
     */
    public Integer findDataSysUserByPageSize(@Param("sname") String sname, @Param("account") String account);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public SysUser findDataSysUserByIndocno(@Param("indocno") Long indocno);

    /**
     * 根据角色id查看记录
     *
     * @return 对象数据集合
     */
    public List<SysUser> findDataSysUserByRoleId(@Param("role_id") Long role_id);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    public List<SysUser> findDataSysUser();

    /**
     * 修改用户密码
     *
     * @param userId      用户id
     * @param newPassword 新密码
     */
    public void updatePassword(@Param("userId") Long userId, @Param("password") String newPassword);

    /***
     * 根据账号和密码返回对象
     * @param account  账号
     * @param password  密码
     * @return
     */
    public SysUser findDataByAccountAndPassword(@Param("account") String account, @Param("password") String password);

    /***
     * 根据账号和密码返回对象
     * @param account  账号
     *
     * @return
     */
    public SysUser findDataByAccount(@Param("account") String account);
}
