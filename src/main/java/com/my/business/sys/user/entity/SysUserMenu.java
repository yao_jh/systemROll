package com.my.business.sys.user.entity;

import com.my.business.sys.menu.entity.SysMenu;

import java.util.List;

/***
 * 用于登录返回用户信息和菜单的实体类
 * @author cc
 *
 */
public class SysUserMenu {

    private SysUser user;
    private List<SysMenu> menus;
    private String roles;

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public SysUser getUser() {
        return user;
    }

    public void setUser(SysUser user) {
        this.user = user;
    }

    public List<SysMenu> getMenus() {
        return menus;
    }

    public void setMenus(List<SysMenu> menus) {
        this.menus = menus;
    }

}
