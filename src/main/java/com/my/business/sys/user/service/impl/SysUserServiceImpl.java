package com.my.business.sys.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.role.dao.SysRoleDao;
import com.my.business.sys.role.entity.SysRole;
import com.my.business.sys.role.entity.SysUserRole;
import com.my.business.sys.user.dao.SysUserDao;
import com.my.business.sys.user.entity.JSysUser;
import com.my.business.sys.user.entity.SysUser;
import com.my.business.sys.user.service.SysUserService;
import com.my.business.util.CodiUtil;

import lombok.extern.slf4j.Slf4j;

import org.hibernate.validator.internal.util.logging.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 人员信息接口具体实现类
 *
 * @author 生成器生成
 * @date 2019-01-11 15:08:13
 */
@Service
public class SysUserServiceImpl implements SysUserService {
	
    @Autowired
    private SysUserDao sysUserDao;

    @Autowired
    private SysRoleDao sysRoleDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataSysUser(String data, Long userId, String sname) {
        try {
            JSysUser juser = JSON.parseObject(data, JSysUser.class);
            SysUser sysUser = juser.getSysUser();
            CodiUtil.newRecord(userId, sname, sysUser);
            sysUserDao.insertDataSysUser(sysUser);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }
    
    /**
     * 宝信添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataSysUserBx(String data) {
        try {
        	System.out.println("传入的人员数据为" + data);
        	SysUser sysUser  = JSON.parseObject(data, SysUser.class);
        	SysUser ifuser = sysUserDao.findDataByAccount(sysUser.getAccount());
        	if(ifuser != null) {
        		return ResultData.ResultDataSuccessSelf("该账号用户已存在，请联系管理员", null);
        	}
        	sysUser.setCreatetime(new Date());
        	sysUser.setCreatename("宝信创建");
        	sysUser.setPassword("123456");
            sysUserDao.insertDataSysUser(sysUser);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 添加用户角色
     *
     * @param str_id 角色主键集合字符串
     * @param userId 用户id
     */
    public ResultData insertDataUserRole(String str_id, Long userId) {
        try {
            String[] array_id = str_id.split(",");
            sysRoleDao.deleteRoleByUserId(userId);
                for (String indocno : array_id) {
                    if (!StringUtils.isEmpty(indocno)){
                        sysRoleDao.insertDataUserRole(userId, Long.valueOf(indocno));
                    }
                }
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除单个对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataSysUserOne(Long indocno) {
        try {
            sysUserDao.deleteDataSysUserOne(indocno);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
        return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
    }
    
    /**
     * 宝信根据账号删除单个对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataSysUserByAccount(String data) {
        try {
        	SysUser sysUser  = JSON.parseObject(data, SysUser.class);
        	sysUserDao.deleteDataSysUserByAccount(sysUser.getAccount());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
        return ResultData.ResultDataSuccessSelf("删除数据成功", null);
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataSysUserMany(String str_id) {
        try {
            String sql = "delete sys_user where indocno in(" + str_id + ")";
            sysUserDao.deleteDataSysUserMany(sql);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
        return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param data sysUser 对象实体
     */
    public ResultData updateDataSysUser(String data, Long userId, String sname) {
        try {
            JSysUser juser = JSON.parseObject(data, JSysUser.class);
            SysUser sysUser = juser.getSysUser();
            CodiUtil.editRecord(userId, sname, sysUser);
            sysUserDao.updateDataSysUser(sysUser);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysUserByPage(String data) {
        try {
            JSysUser juser = JSON.parseObject(data, JSysUser.class);
            JSONObject jsonObject = null;
            Integer pageIndex = juser.getPageIndex();
            Integer pageSize = juser.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != juser.getCondition()) {
                jsonObject = JSON.parseObject(juser.getCondition().toString());
            }

            String sname = null;
            if (!StringUtils.isEmpty(jsonObject.get("sname"))) {
                sname = jsonObject.get("sname").toString();
            }

            String account = null;
            if (!StringUtils.isEmpty(jsonObject.get("account"))) {
                account = jsonObject.get("account").toString();
            }

            List<SysUser> list = sysUserDao.findDataSysUserByPage((pageIndex-1)*pageSize, pageSize, sname, account);
            Integer count = sysUserDao.findDataSysUserByPageSize(sname, account);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysUserByIndocno(String data) {
        try {
            JSysUser juser = JSON.parseObject(data, JSysUser.class);
            Long indocno = juser.getIndocno();
            SysUser sysUser = sysUserDao.findDataSysUserByIndocno(indocno);

            /***
             * 根据人员id带入该人员所有的角色集合
             */
            List<SysUserRole> list = sysRoleDao.findRoleByUserId(indocno);
            List<SysRole> sysRoleList = new ArrayList<>();
            String role_list = "";
            if (list.size() > 0) {
                for (SysUserRole userRole : list) {
                    role_list = role_list + userRole.getRole_id() + ",";
                    SysRole sysRole = new SysRole();
                    sysRole.setIndocno(userRole.getRole_id());
                    sysRoleList.add(sysRole);
                }
                role_list = role_list.substring(0, role_list.length() - 1);
            }
            sysUser.setList_roleId(role_list);
            sysUser.setSysRole(sysRoleList);


            return ResultData.ResultDataSuccess(sysUser);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<SysUser> findDataSysUser() {
        List<SysUser> list = sysUserDao.findDataSysUser();
        return list;
    }

    /***
     * 根据账号和密码返回对象
     * @param account  账号
     * @param password  密码
     * @return
     */
    @Override
    public SysUser findDataByAccountAndPassword(String account, String password) {
        return sysUserDao.findDataByAccountAndPassword(account, password);
    }

    public SysUser findDataByAccount(String account) {
        return sysUserDao.findDataByAccount(account);
    }
    /***
     *修改密码
     * @param data  字符串
     * @return
     */
    @Override
    public ResultData updatePassword(String data) {
        try {
            JCommon common = JSON.parseObject(data, JCommon.class);
            sysUserDao.updatePassword(common.getUserId(), common.getPassword());
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改密码失败,原因是" + e.getMessage(), null);
        }
    }
}
