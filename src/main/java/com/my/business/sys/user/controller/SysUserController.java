package com.my.business.sys.user.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.user.entity.JSysUser;
import com.my.business.sys.user.entity.SysUser;
import com.my.business.sys.user.service.SysUserService;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 人员信息控制器层
 *
 * @date 2019-01-11 15:10:27
 */
@Controller
@RequestMapping("/sysUser")
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    /**
     * 添加记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataSysUser(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return sysUserService.insertDataSysUser(data, userId, CodiUtil.returnLm(sname));
    }
    
    /**
     * 宝信添加记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/bxadduser"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData addUser(@RequestBody String data) {
        return sysUserService.insertDataSysUserBx(data);
    }

    /**
     * 添加人员角色记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/insertRole"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRole(@RequestBody String data) {
        try {
            JSysUser juser = JSON.parseObject(data, JSysUser.class);
            return sysUserService.insertDataUserRole(juser.getStr_indocno(), juser.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除单个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserOne(@RequestBody String data) {
        try {
            JSysUser juser = JSON.parseObject(data, JSysUser.class);
            return sysUserService.deleteDataSysUserOne(juser.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }
    
    /**
     * 根据账号删除单个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/bxdeleteuser"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserbx(@RequestBody String data) {
        try {
            return sysUserService.deleteDataSysUserByAccount(data);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JSysUser juser = JSON.parseObject(data, JSysUser.class);
            return sysUserService.deleteDataSysUserMany(juser.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataSysUser(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return sysUserService.updateDataSysUser(data, userId, CodiUtil.returnLm(sname));
    }


    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysUserByPage(@RequestBody String data) {
        return sysUserService.findDataSysUserByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysUserByIndocno(@RequestBody String data) {
        return sysUserService.findDataSysUserByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.GET)
    public List<SysUser> findDataSysUser() {
        return sysUserService.findDataSysUser();
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
//    @CrossOrigin
//    @RequestMapping(value = {"/findAllLayui"}, method = RequestMethod.GET)
//    @ResponseBody
//    public Layui findLayuiDataSysUser() {
//        List<SysUser> list = sysUserService.findDataSysUser();
//        return Layui.data(list.size(), list);
//    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @RequestMapping(value = {"/touser"}, method = RequestMethod.GET)
    public String touser() {
        return "user/user";
    }

    /**
     * 进入修改密码界面
     *
     * @return
     */
    @RequestMapping(value = {"/toUpdatePassword"}, method = RequestMethod.GET)
    public String toUpdatePassword() {
        return "user/updatepassword";
    }

    /**
     * 修改密码
     *
     * @param data json字符串
     * @return
     */
    @CrossOrigin
    @RequestMapping(value = {"/updatePassword"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updatePassword(@RequestBody String data) {
        return sysUserService.updatePassword(data);
    }

}
