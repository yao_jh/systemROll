package com.my.business.sys.controlInterface.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.controlInterface.entity.ControlInterface;

import java.util.List;

/**
 * 对外自定义接口接口服务类
 *
 * @author 生成器生成
 * @date 2019-01-17 14:17:00
 */
public interface ControlInterfaceService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataControlInterface(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataControlInterfaceOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataControlInterfaceMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData updateDataControlInterface(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataControlInterfaceByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataControlInterfaceByIndocno(String data);

    /***
     * 根据
     * @param control_key查询对应的再用接口集合
     * @return
     */
    public List<ControlInterface> findDataByControlKey(String key);
}
