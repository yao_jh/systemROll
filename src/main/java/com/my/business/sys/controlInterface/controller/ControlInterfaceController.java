package com.my.business.sys.controlInterface.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.controlInterface.entity.JControlInterface;
import com.my.business.sys.controlInterface.service.ControlInterfaceService;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 公共接口配置控制器层
 *
 * @author 生成器生成
 * @date 2019-02-22 11:10:40
 */
@RestController
@RequestMapping("/controlInterface")
public class ControlInterfaceController {

    @Autowired
    private ControlInterfaceService controlInterfaceService;

    /**
     * 添加记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataControlInterface(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return controlInterfaceService.insertDataControlInterface(data, userId, CodiUtil.returnLm(sname));
    }

    ;

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataControlInterfaceOne(@RequestBody String data) {
        try {
            JControlInterface jcontrolInterface = JSON.parseObject(data, JControlInterface.class);
            return controlInterfaceService.deleteDataControlInterfaceOne(jcontrolInterface.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    ;

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JControlInterface jcontrolInterface = JSON.parseObject(data, JControlInterface.class);
            return controlInterfaceService.deleteDataControlInterfaceMany(jcontrolInterface.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataControlInterface(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return controlInterfaceService.updateDataControlInterface(data, userId, CodiUtil.returnLm(sname));
    }

    ;

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataControlInterfaceByPage(@RequestBody String data) {
        return controlInterfaceService.findDataControlInterfaceByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataControlInterfaceByIndocno(@RequestBody String data) {
        return controlInterfaceService.findDataControlInterfaceByIndocno(data);
    }

}
