package com.my.business.sys.controlInterface.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.controlInterface.dao.ControlInterfaceDao;
import com.my.business.sys.controlInterface.entity.ControlInterface;
import com.my.business.sys.controlInterface.entity.JControlInterface;
import com.my.business.sys.controlInterface.service.ControlInterfaceService;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 对外自定义接口接口具体实现类
 *
 * @author 生成器生成
 * @date 2019-01-17 14:17:00
 */
@Service
public class ControlInterfaceServiceImpl implements ControlInterfaceService {

    @Autowired
    private ControlInterfaceDao controlInterfaceDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataControlInterface(String data, Long userId, String sname) {
        try {
            JControlInterface jcontrolInterface = JSON.parseObject(data, JControlInterface.class);
            ControlInterface controlInterface = jcontrolInterface.getControlInterface();
            CodiUtil.newRecord(userId, sname, controlInterface);
            controlInterfaceDao.insertDataControlInterface(controlInterface);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    ;

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataControlInterfaceOne(Long indocno) {
        try {
            controlInterfaceDao.deleteDataControlInterfaceOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    ;

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataControlInterfaceMany(String str_id) {
        try {
            String sql = "delete control_interface where indocno in(" + str_id + ")";
            controlInterfaceDao.deleteDataControlInterfaceMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataControlInterface(String data, Long userId, String sname) {
        try {
            JControlInterface jcontrolInterface = JSON.parseObject(data, JControlInterface.class);
            ControlInterface controlInterface = jcontrolInterface.getControlInterface();
            CodiUtil.editRecord(userId, sname, controlInterface);
            controlInterfaceDao.updateDataControlInterface(controlInterface);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    ;

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataControlInterfaceByPage(String data) {
        try {
            JControlInterface jcontrolInterface = JSON.parseObject(data, JControlInterface.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jcontrolInterface.getPageIndex();
            Integer pageSize = jcontrolInterface.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jcontrolInterface.getCondition()) {
                jsonObject = JSON.parseObject(jcontrolInterface.getCondition().toString());
            }

            List<ControlInterface> list = controlInterfaceDao.findDataControlInterfaceByPage(pageIndex, pageSize);
            Integer count = controlInterfaceDao.findDataControlInterfaceByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataControlInterfaceByIndocno(String data) {
        try {
            JControlInterface jcontrolInterface = JSON.parseObject(data, JControlInterface.class);
            Long indocno = jcontrolInterface.getIndocno();

            ControlInterface controlInterface = controlInterfaceDao.findDataControlInterfaceByIndocno(indocno);
            return ResultData.ResultDataSuccess(controlInterface);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<ControlInterface> findDataControlInterface() {
        List<ControlInterface> list = controlInterfaceDao.findDataControlInterface();
        return list;
    }

    ;

    /***
     * 根据
     * @param control_key查询对应的再用接口集合
     * @return
     */
    @Override
    public List<ControlInterface> findDataByControlKey(String key) {
        return controlInterfaceDao.findDataByControlKey(key);
    }

    ;
}
