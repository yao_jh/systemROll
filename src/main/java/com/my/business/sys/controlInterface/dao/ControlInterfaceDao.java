package com.my.business.sys.controlInterface.dao;


import com.my.business.sys.controlInterface.entity.ControlInterface;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 对外自定义接口dao接口
 *
 * @author 生成器生成
 * @date 2019-01-17 14:17:00
 */
@Mapper
public interface ControlInterfaceDao {

    /**
     * 添加记录
     *
     * @param controlInterface 对象实体
     */
    public void insertDataControlInterface(ControlInterface controlInterface);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public void deleteDataControlInterfaceOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    public void deleteDataControlInterfaceMany(String value);

    /**
     * 修改记录
     *
     * @param controlInterface 对象实体
     */
    public void updateDataControlInterface(ControlInterface controlInterface);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<ControlInterface> findDataControlInterfaceByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    public Integer findDataControlInterfaceByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public ControlInterface findDataControlInterfaceByIndocno(@Param("indocno") Long indocno);

    /***
     * 根据
     * @param control_key查询对应的再用接口集合
     * @return
     */
    public List<ControlInterface> findDataByControlKey(String key);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    public List<ControlInterface> findDataControlInterface();

}
