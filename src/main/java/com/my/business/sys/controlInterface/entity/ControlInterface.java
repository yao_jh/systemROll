package com.my.business.sys.controlInterface.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 公共接口配置实体类
 *
 * @author 生成器生成
 * @date 2019-02-22 11:10:40
 */
public class ControlInterface extends BaseEntity {

    private Long indocno;  //主键
    private String control_key;  //控制KEY
    private String control_sql;  //对应SQL

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getControl_key() {
        return this.control_key;
    }

    public void setControl_key(String control_key) {
        this.control_key = control_key;
    }

    public String getControl_sql() {
        return this.control_sql;
    }

    public void setControl_sql(String control_sql) {
        this.control_sql = control_sql;
    }


}