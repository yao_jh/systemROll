package com.my.business.sys.role.dao;

import com.my.business.sys.role.entity.SysRole;
import com.my.business.sys.role.entity.SysUserRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 计划dao接口
 *
 * @author 生成器生成
 * @date 2019-02-20 10:26:38
 */
@Mapper
public interface SysRoleDao {

    /**
     * 添加记录
     *
     * @param sysRole 对象实体
     */
    public void insertDataSysRole(SysRole sysRole);

    /**
     * 添加人员角色关系记录
     *
     * @param userId 用户id
     * @param roleId 角色id
     */
    public void insertDataUserRole(@Param("userId") Long userId, @Param("roleId") Long roleId);

    /**
     * 根据用户id删除所有所属角色
     *
     * @param userId 用户id
     */
    public void deleteRoleByUserId(@Param("userId") Long userId);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public void deleteDataSysRoleOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    public void deleteDataSysRoleMany(String value);

    /**
     * 修改记录
     *
     * @param sysRole 对象实体
     */
    public void updateDataSysRole(SysRole sysRole);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<SysRole> findDataSysRoleByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("role_name") String role_name, @Param("role_code") String role_code);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    public Integer findDataSysRoleByPageSize(@Param("role_name") String role_name, @Param("role_code") String role_code);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public SysRole findDataSysRoleByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    public List<SysRole> findDataSysRole();

    /**
     * 根据用户id获取角色集合
     *
     * @param sql 拼接的sql语句
     * @return 对象数据集合
     */
    public List<SysRole> findDataSysRoleByUserId(String sql);

    /**
     * 根据人员id获取角色集合，用于用户修改功能
     *
     * @return 对象数据集合
     */
    public List<SysUserRole> findRoleByUserId(@Param("userId") Long userId);

    /**
     * 根据磨床角色查询磨床号
     * @param role_id 磨床角色id
     * @return 磨床号
     */
    public Long findMachine(@Param("role_id") Long role_id);
}
