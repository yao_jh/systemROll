package com.my.business.sys.role.entity;

import com.my.business.sys.common.entity.JCommon;

/**
 * 计划json的实体类
 *
 * @author 生成器生成
 * @date 2019-02-20 10:26:38
 */
public class JSysRole extends JCommon {

    private Integer pageIndex; // 第几页
    private Integer pageSize; // 每页多少数据
    private Object condition; // 查询条件
    private SysRole sysRole;   //对应模块的实体类
    private Long indocno;
    private String str_indocno;
    private String str_always;

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Object getCondition() {
        return condition;
    }

    public void setCondition(Object condition) {
        this.condition = condition;
    }

    public SysRole getSysRole() {
        return sysRole;
    }

    public void setSysRole(SysRole sysRole) {
        this.sysRole = sysRole;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getStr_indocno() {
        return str_indocno;
    }

    public void setStr_indocno(String str_indocno) {
        this.str_indocno = str_indocno;
    }

    public String getStr_always() {
        return str_always;
    }

    public void setStr_always(String str_always) {
        this.str_always = str_always;
    }
}