package com.my.business.sys.role.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.role.entity.JSysRole;
import com.my.business.sys.role.entity.SysRole;
import com.my.business.sys.role.service.SysRoleService;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 计划控制器层
 *
 * @author 生成器生成
 * @date 2019-02-20 10:26:38
 */
@RestController
@RequestMapping("/sysRole")
public class SysRoleController {

    @Autowired
    private SysRoleService sysRoleService;

    /**
     * 添加记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataSysRole(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return sysRoleService.insertDataSysRole(data, userId, CodiUtil.returnLm(sname));
    }

    ;

    /**
     * 添加角色菜单记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/insertMenu"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataMenu(@RequestBody String data) {
        try {
            JSysRole jrole = JSON.parseObject(data, JSysRole.class);
            return sysRoleService.insertDataRoleMenu(jrole.getStr_indocno(), jrole.getIndocno(), jrole.getStr_always());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysRoleOne(@RequestBody String data) {
        try {
            JSysRole jsysRole = JSON.parseObject(data, JSysRole.class);
            return sysRoleService.deleteDataSysRoleOne(jsysRole.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    ;

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JSysRole jsysRole = JSON.parseObject(data, JSysRole.class);
            return sysRoleService.deleteDataSysRoleMany(jsysRole.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataSysRole(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return sysRoleService.updateDataSysRole(data, userId, CodiUtil.returnLm(sname));
    }

    ;

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysRoleByPage(@RequestBody String data) {
        return sysRoleService.findDataSysRoleByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysRoleByIndocno(@RequestBody String data) {
        return sysRoleService.findDataSysRoleByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    @ResponseBody
    public List<SysRole> findDataSysRole() {
        return sysRoleService.findDataSysRole();
    }
}
