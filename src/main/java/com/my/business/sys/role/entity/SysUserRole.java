package com.my.business.sys.role.entity;

/**
 * 人员角色实体类
 *
 * @author 生成器生成
 * @date 2019-02-20 13:42:25
 */
public class SysUserRole {

    private Long indocno;  //主键
    private Long user_id;  //人员ID
    private Long role_id;  //角色ID

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getUser_id() {
        return this.user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getRole_id() {
        return this.role_id;
    }

    public void setRole_id(Long role_id) {
        this.role_id = role_id;
    }


}