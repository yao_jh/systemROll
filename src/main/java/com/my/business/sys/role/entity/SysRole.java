package com.my.business.sys.role.entity;

import com.my.business.sys.common.entity.BaseEntity;
import com.my.business.sys.menu.entity.SysMenu;

import java.util.List;

/**
 * 计划实体类
 *
 * @author 生成器生成
 * @date 2019-02-20 10:26:37
 */
public class SysRole extends BaseEntity {

    private Long indocno;  //主键
    private String role_name;  //角色名
    private String role_code;  //角色编码

    private List<SysMenu> sysMenu;   //菜单集合

    public List<SysMenu> getSysMenu() {
        return sysMenu;
    }

    public void setSysMenu(List<SysMenu> sysMenu) {
        this.sysMenu = sysMenu;
    }

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRole_name() {
        return this.role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    public String getRole_code() {
        return this.role_code;
    }

    public void setRole_code(String role_code) {
        this.role_code = role_code;
    }


}