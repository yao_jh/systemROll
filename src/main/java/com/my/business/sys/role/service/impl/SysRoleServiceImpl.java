package com.my.business.sys.role.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.menu.dao.SysMenuDao;
import com.my.business.sys.menu.entity.SysMenu;
import com.my.business.sys.role.dao.SysRoleDao;
import com.my.business.sys.role.entity.JSysRole;
import com.my.business.sys.role.entity.SysRole;
import com.my.business.sys.role.service.SysRoleService;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 计划接口具体实现类
 *
 * @author 生成器生成
 * @date 2019-02-20 10:26:38
 */
@Service
public class SysRoleServiceImpl implements SysRoleService {

    @Autowired
    private SysRoleDao sysRoleDao;

    @Autowired
    private SysMenuDao sysMenuDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataSysRole(String data, Long userId, String sname) {
        try {
            JSysRole jrole = JSON.parseObject(data, JSysRole.class);
            SysRole sysRole = jrole.getSysRole();
            CodiUtil.newRecord(userId, sname, sysRole);
            sysRoleDao.insertDataSysRole(sysRole);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 添加角色菜单
     *
     * @param str_id 菜单主键集合字符串
     * @param roleId 角色id
     * @param str_always 快捷方式
     */
    public ResultData insertDataRoleMenu(String str_id, Long roleId, String str_always) {
        try {
            String[] array_id = str_id.split(",");
            String[] array_always = str_always.split(",");
            sysMenuDao.deleteMenuByRoleId(roleId);
            for (int i = 0;i<array_id.length;i++){
                if (!StringUtils.isEmpty(array_id[i])) {
                    sysMenuDao.insertDataRoleMenu(roleId, Long.valueOf(array_id[i]), Long.valueOf(array_always[i]));
                }
            }
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataSysRoleOne(Long indocno) {
        try {
            sysRoleDao.deleteDataSysRoleOne(indocno);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
        return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataSysRoleMany(String str_id) {
        try {
            String sql = "delete sys_role where indocno in(" + str_id + ")";
            sysRoleDao.deleteDataSysRoleMany(sql);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
        return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataSysRole(String data, Long userId, String sname) {
        try {
            JSysRole jrole = JSON.parseObject(data, JSysRole.class);
            SysRole sysRole = jrole.getSysRole();
            CodiUtil.editRecord(userId, sname, sysRole);
            sysRoleDao.updateDataSysRole(sysRole);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysRoleByPage(String data) {
        try {
            JSysRole jsysRole = JSON.parseObject(data, JSysRole.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jsysRole.getPageIndex();
            Integer pageSize = jsysRole.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jsysRole.getCondition()) {
                jsonObject = JSON.parseObject(jsysRole.getCondition().toString());
            }

            String roleName = null;
            if (null != jsonObject.get("role_name")) {
                roleName = jsonObject.get("role_name").toString();
            }

            String roleCode = null;
            if (null != jsonObject.get("role_code")) {
                roleCode = jsonObject.get("role_code").toString();
            }

            List<SysRole> list = sysRoleDao.findDataSysRoleByPage((pageIndex - 1) * pageSize, pageSize, roleName, roleCode);
            Integer count = sysRoleDao.findDataSysRoleByPageSize(roleName, roleCode);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条角色记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysRoleByIndocno(String data) {
        try {
            JSysRole jsysRole = JSON.parseObject(data, JSysRole.class);
            Long indocno = jsysRole.getIndocno();

            SysRole sysRole = sysRoleDao.findDataSysRoleByIndocno(indocno);
            List<SysMenu> sysMenuList = sysMenuDao.findDataMenuListByIndocno(indocno);
            for (SysMenu entity : sysMenuList){
                String menu_sql = "select a.always_menu from sys_role_menu a where a.menu_id = "+entity.getIndocno()+" and a.role_id = "+ sysRole.getIndocno();
                entity.setAlways_menu(sysMenuDao.findDataSysMenuByRoleAndMenu(menu_sql));
            }
            sysRole.setSysMenu(sysMenuList);
            return ResultData.ResultDataSuccess(sysRole);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<SysRole> findDataSysRole() {
        List<SysRole> list = sysRoleDao.findDataSysRole();
        return list;
    }

    /**
     * 根据用户id获取角色集合
     *
     * @param userid 用户id
     */
    @Override
    public List<SysRole> findDataByUserId(Long userid) {
        String role_sql = "select r.* from sys_role r "
                + "left join sys_user_role sur on r.indocno = sur.role_id "
                + "where sur.user_id = " + userid;
        List<SysRole> roleList = sysRoleDao.findDataSysRoleByUserId(role_sql);
        return roleList;
    }

    @Override
    public String findDataStringByUserId(Long userid) {
        String role_sql = "select r.* from sys_role r "
                + "left join sys_user_role sur on r.indocno = sur.role_id "
                + "where sur.user_id = " + userid;
        List<SysRole> roleList = sysRoleDao.findDataSysRoleByUserId(role_sql);
        String role_string = "";
        for (SysRole role : roleList) {
            role_string += role.getIndocno().toString() + ",";
        }
        if(!StringUtils.isEmpty(role_string)) {
        	role_string = role_string.substring(0, role_string.length() - 1);
        }
        return role_string;
    }

}
