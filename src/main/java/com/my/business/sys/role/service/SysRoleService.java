package com.my.business.sys.role.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.role.entity.SysRole;

import java.util.List;

/**
 * 计划接口服务类
 *
 * @author 生成器生成
 * @date 2019-02-20 10:26:38
 */
public interface SysRoleService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataSysRole(String data, Long userId, String sname);

    /**
     * 添加角色菜单
     *
     * @param str_id 菜单id字符串
     * @param roleId 角色id
     * @param str_always 快捷菜单集合（0=否，1=是）
     */
    public ResultData insertDataRoleMenu(String str_id, Long roleId, String str_always);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataSysRoleOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataSysRoleMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData updateDataSysRole(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysRoleByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysRoleByIndocno(String data);

    /**
     * 查看记录
     */
    public List<SysRole> findDataSysRole();

    /**
     * 根据用户id获取角色集合
     *
     * @param userid 用户id
     */
    public List<SysRole> findDataByUserId(Long userid);

    /**
     * 根据用户id获取角色集合字符串
     *
     * @param userid 用户id
     */
    public String findDataStringByUserId(Long userid);
}
