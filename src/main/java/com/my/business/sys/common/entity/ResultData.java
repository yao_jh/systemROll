package com.my.business.sys.common.entity;

import com.my.business.sys.common.constant.CommonConstant;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 公众返回结果实体类(json的格式)
 *
 * @author cc
 */
public class ResultData {

    private String msg; // 返回信息
    private Integer status; // 状态
    private Object data;
    private Integer count; // 数据总数
    private Integer totle; // 总分(临时添加，可用可不用)

    private BigDecimal number; // 合计数值

    private BigDecimal total;//合计值2

    private List<MapXYEntity> map;

    private Object bean;

    private Object countList;

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }

    public ResultData() {
    }

    public ResultData(String msg, Integer status, Object data) {
        this.msg = msg;
        this.data = data;
        this.status = status;
        this.count = 0;
    }


    public ResultData(String msg, Integer status, Object data, Integer count) {
        this.msg = msg;
        this.data = data;
        this.status = status;
        this.count = count;
    }
   public ResultData(String msg, Integer status, Object data,Integer count,BigDecimal number) {
        this.msg = msg;
        this.data = data;
        this.status = status;
       this.count = count;
        this.number = number;
    }

    public ResultData(String msg, Integer status, Object data,Integer count,BigDecimal number,BigDecimal total) {
        this.msg = msg;
        this.data = data;
        this.status = status;
        this.count = count;
        this.number = number;
        this.total  = total;
    }

    public ResultData(String msg, Integer status, Object data,Integer count,BigDecimal number,BigDecimal total,List<MapXYEntity> map) {
        this.msg = msg;
        this.data = data;
        this.status = status;
        this.count = count;
        this.number = number;
        this.total  = total;
        this.map = map;
    }

    public ResultData(String msg, Integer status, Object data,Integer count,Object bean,List<MapXYEntity> map) {
        this.msg = msg;
        this.data = data;
        this.status = status;
        this.count = count;
        this.bean = bean;
        this.map = map;
    }

    public ResultData(String msg, Integer status, Object data,Integer count,Object bean) {
        this.msg = msg;
        this.data = data;
        this.status = status;
        this.count = count;
        this.bean = bean;
    }
    public ResultData(String msg, Integer status, Object data,Integer count,Object bean,Object countList) {
        this.msg = msg;
        this.data = data;
        this.status = status;
        this.count = count;
        this.bean = bean;
        this.countList = countList;
    }
    /**
     * 分页查询对象
     *
     * @param
     * @param data
     * @return
     */
    public static ResultData ResultDataSuccess(Object data, Integer count) {
        return new ResultData("访问数据接口成功", CommonConstant.success_status, data, count);
    }

    public static ResultData ResultDataSuccess(Object data,Integer count, BigDecimal number) {
        return new ResultData("访问数据接口成功", CommonConstant.success_status, data,count, number);
    }

    public static ResultData ResultDataSuccess(Object data,Integer count, BigDecimal number,BigDecimal total) {
        return new ResultData("访问数据接口成功", CommonConstant.success_status, data,count, number,total);
    }

    public static ResultData ResultDataSuccess(Object data,Integer count, BigDecimal number,BigDecimal total,List<MapXYEntity> map) {
        return new ResultData("访问数据接口成功", CommonConstant.success_status, data,count, number,total,map);
    }
    public static ResultData ResultDataSuccess(Object data,Integer count, Object bean) {
        return new ResultData("访问数据接口成功", CommonConstant.success_status, data,count, bean);
    }
    public static ResultData ResultDataSuccess(Object data,Integer count, Object bean,List<MapXYEntity> map) {
        return new ResultData("访问数据接口成功", CommonConstant.success_status, data,count,bean,map);
    }
    public static ResultData ResultDataSuccess(Object data,Integer count, Object bean,Object countList) {
        return new ResultData("访问数据接口成功", CommonConstant.success_status, data,count, bean,countList);
    }

    /**
     * 访问成功
     *
     * @param
     * @param data
     * @return
     */
    public static ResultData ResultDataSuccess(Object data) {
        return new ResultData("访问数据接口成功", CommonConstant.success_status, data);
    }

    /**
     * 自定义访问成功
     *
     * @param msg
     * @param data
     * @return
     */
    public static ResultData ResultDataSuccessSelf(String msg, Object data) {
        return new ResultData(msg, CommonConstant.success_status, data);
    }

    /**
     * 访问失败自定义
     *
     * @param msg
     * @param data
     * @return
     */
    public static ResultData ResultDataFaultSelf(String msg, Object data) {
        return new ResultData(msg, CommonConstant.fault_status, data);
    }

    /**
     * 访问失败固定
     *
     * @param msg
     * @param data
     * @return
     */
    public static ResultData ResultDataFaultGd(String msg, Object data) {
        return new ResultData("访问数据失败，失败信息为" + msg, CommonConstant.fault_status, data);
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public Integer getTotle() {
        return totle;
    }

    public void setTotle(Integer totle) {
        this.totle = totle;
    }

    public Integer getCount() {
        return count;
    }

    ;

    public void setCount(Integer count) {
        this.count = count;
    }

    ;

    public String getMsg() {
        return msg;
    }

    ;

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public BigDecimal getNumber() {
        return number;
    }

    public void setNumber(BigDecimal number) {
        this.number = number;
    }

    public List<MapXYEntity> getMap() {
        return map;
    }

    public void setMap(List<MapXYEntity> map) {
        this.map = map;
    }

    public Object getCountList() {
        return countList;
    }

    public void setCountList(Object countList) {
        this.countList = countList;
    }
}
