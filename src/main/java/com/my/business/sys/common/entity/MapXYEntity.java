package com.my.business.sys.common.entity;

/***
 * 类似于map的json类
 * @author chenchao
 *
 */
public class MapXYEntity {

    private Object x;
    private Object y1;
    private Object y2;
    private Object y3;
    private Object y4;
    private Object y5;

    public Object getX() {
        return x;
    }

    public void setX(Object x) {
        this.x = x;
    }

    public Object getY1() {
        return y1;
    }

    public void setY1(Object y1) {
        this.y1 = y1;
    }

    public Object getY2() {
        return y2;
    }

    public void setY2(Object y2) {
        this.y2 = y2;
    }

    public Object getY3() {
        return y3;
    }

    public void setY3(Object y3) {
        this.y3 = y3;
    }

    public Object getY4() {
        return y4;
    }

    public void setY4(Object y4) {
        this.y4 = y4;
    }

    public Object getY5() {
        return y5;
    }

    public void setY5(Object y5) {
        this.y5 = y5;
    }
}
