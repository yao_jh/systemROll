package com.my.business.sys.common.entity;

/***
 * 类似于map的json类
 * @author chenchao
 *
 */
public class MapEntity {
    private String tablename;
    private String key;
    private String value;

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
