package com.my.business.sys.common.entity;

import java.util.Date;

public class BaseEntity {

    private Long istate; // 状态
    private Long idel; // 删除标识
    private Date createtime; // 创建时间
    private String createname; // 创建人姓名
    private Long createid; // 创建人ID
    private Date moditime; // 修改时间
    private Long modiid; // 修改人ID
    private String modiname; // 修改人姓名

    public Long getIstate() {
        return istate;
    }

    public void setIstate(Long istate) {
        this.istate = istate;
    }

    public Long getIdel() {
        return idel;
    }

    public void setIdel(Long idel) {
        this.idel = idel;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getCreatename() {
        return createname;
    }

    public void setCreatename(String createname) {
        this.createname = createname;
    }

    public Long getCreateid() {
        return createid;
    }

    public void setCreateid(Long createid) {
        this.createid = createid;
    }

    public Date getModitime() {
        return moditime;
    }

    public void setModitime(Date moditime) {
        this.moditime = moditime;
    }

    public Long getModiid() {
        return modiid;
    }

    public void setModiid(Long modiid) {
        this.modiid = modiid;
    }

    public String getModiname() {
        return modiname;
    }

    public void setModiname(String modiname) {
        this.modiname = modiname;
    }

}
