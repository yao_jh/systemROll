package com.my.business.sys.common.entity;

public class ActEntity extends BaseEntity {
    private Double sact1;  //测量值1
    private Double sact2;  //测量值2
    private Double sact3;  //测量值3
    private Double sact4;  //测量值4
    private Double sact5;  //测量值5
    private Double sact6;  //测量值6
    private Double sact7;  //测量值7
    private Double sact8;  //测量值8
    private Double sact9;  //测量值9
    private Double sactmea;  //实际精度
    private String stesttime;  //测量时间
    private String scode;  //评价分数
    private Long sacctime;  //合格时长（分钟）
    private Long if_ok;   //是否合格
    private Double scope;   //精度范围

    private String scoperate;   //精度保持率
    private String scoperate_old;  //老的精度保持率
    private String scStart;   //前台传递的测量时间开始
    private String scEnd;  //前台传递的测量时间结束
    private String oldStart;  //前台传递的老测量时间开始
    private String oldEnd;   //前台传递的老测量时间结束

    public String getScStart() {
        return scStart;
    }

    public void setScStart(String scStart) {
        this.scStart = scStart;
    }

    public String getScEnd() {
        return scEnd;
    }

    public void setScEnd(String scEnd) {
        this.scEnd = scEnd;
    }

    public String getOldStart() {
        return oldStart;
    }

    public void setOldStart(String oldStart) {
        this.oldStart = oldStart;
    }

    public String getOldEnd() {
        return oldEnd;
    }

    public void setOldEnd(String oldEnd) {
        this.oldEnd = oldEnd;
    }

    public String getScoperate() {
        return scoperate;
    }

    public void setScoperate(String scoperate) {
        this.scoperate = scoperate;
    }

    public String getScoperate_old() {
        return scoperate_old;
    }

    public void setScoperate_old(String scoperate_old) {
        this.scoperate_old = scoperate_old;
    }

    public Long getIf_ok() {
        return if_ok;
    }

    public void setIf_ok(Long if_ok) {
        this.if_ok = if_ok;
    }

    public Double getScope() {
        return scope;
    }

    public void setScope(Double scope) {
        this.scope = scope;
    }

    public Double getSact1() {
        return sact1;
    }

    public void setSact1(Double sact1) {
        this.sact1 = sact1;
    }

    public Double getSact2() {
        return sact2;
    }

    public void setSact2(Double sact2) {
        this.sact2 = sact2;
    }

    public Double getSact3() {
        return sact3;
    }

    public void setSact3(Double sact3) {
        this.sact3 = sact3;
    }

    public Double getSact4() {
        return sact4;
    }

    public void setSact4(Double sact4) {
        this.sact4 = sact4;
    }

    public Double getSact5() {
        return sact5;
    }

    public void setSact5(Double sact5) {
        this.sact5 = sact5;
    }

    public Double getSact6() {
        return sact6;
    }

    public void setSact6(Double sact6) {
        this.sact6 = sact6;
    }

    public Double getSact7() {
        return sact7;
    }

    public void setSact7(Double sact7) {
        this.sact7 = sact7;
    }

    public Double getSact8() {
        return sact8;
    }

    public void setSact8(Double sact8) {
        this.sact8 = sact8;
    }

    public Double getSact9() {
        return sact9;
    }

    public void setSact9(Double sact9) {
        this.sact9 = sact9;
    }

    public Double getSactmea() {
        return sactmea;
    }

    public void setSactmea(Double sactmea) {
        this.sactmea = sactmea;
    }

    public String getStesttime() {
        return stesttime;
    }

    public void setStesttime(String stesttime) {
        this.stesttime = stesttime;
    }

    public String getScode() {
        return scode;
    }

    public void setScode(String scode) {
        this.scode = scode;
    }

    public Long getSacctime() {
        return sacctime;
    }

    public void setSacctime(Long sacctime) {
        this.sacctime = sacctime;
    }
}
