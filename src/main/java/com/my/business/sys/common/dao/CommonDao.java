package com.my.business.sys.common.dao;

import com.my.business.sys.common.entity.MapEntity;
import com.my.business.sys.dict.entity.DpdEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/***
 * 公共的接口,通过传递
 * @author ThinkPad
 *
 */
@Mapper
public interface CommonDao {

    /**
     * 根据sql语句查询多调数据
     *
     * @param sql sql语句
     * @return
     * @author cc
     */
    public List<Map<String, Object>> findManyData(String sql);

    /**
     * 根据sql语句查询单调数据
     *
     * @param sql sql语句
     * @return
     * @author cc
     */
    public Map<String, Object> findOneData(String sql);

    /***
     * 根据sql执行对应的数据库操作，适合所有接口
     * @param sql
     */
    public void performBySql(String sql);

    /***
     * 根据sql执行对应的数据库操作获取记录的某个数值型字段
     * @param sql
     */
    public Integer findNumberBySql(String sql);

    /***
     * 根据参数获取map类型的数据，一般用于下拉,适用于任何只需要key value的数据
     * @param sql
     * @return
     */
    public List<DpdEntity> findMap(String sql);

    /***
     * 根据参数获取map类型的数据，一般用于带有关联查询性质的下拉,适用于任何只需要key value的数据
     * @param sql
     * @return
     */
    public List<DpdEntity> findMapByCondition(String sql);
    
    /***
     * 根据表名查询表结构,字段名和注释
     * @param sql
     * @return
     */
    public List<MapEntity> findTable(@Param("tablename") String tablename);

}
