package com.my.business.sys.common.service.impl;

import com.alibaba.fastjson.JSON;
import com.my.business.sys.common.dao.CommonDao;
import com.my.business.sys.common.entity.MapByConditionEntity;
import com.my.business.sys.common.entity.MapEntity;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.common.service.CommonService;
import com.my.business.sys.dict.entity.DpdEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CommonServiceImpl implements CommonService {

    @Autowired
    private CommonDao commonDao;

    /**
     * 根据sql语句查询多调数据
     *
     * @param sql sql语句
     * @return
     * @author cc
     */
    @Override
    public List<Map<String, Object>> findManyData(String sql) {
        return commonDao.findManyData(sql);
    }


    /**
     * 根据sql语句查询单条数据
     *
     * @param sql sql语句
     * @return
     * @author cc
     */
    @Override
    public Map<String, Object> findOneData(String sql) {
        return commonDao.findOneData(sql);
    }

    /**
     * 根据参数获取map类型的数据，一般用于下拉,适用于任何只需要key value的数据
     *
     * @param data 字符串json
     * @return
     * @author cc
     */
    @Override
    public ResultData findMap(String data) {
        try {
            MapEntity entity = JSON.parseObject(data, MapEntity.class);
            String tablename = entity.getTablename();
            String key = entity.getKey();
            String value = entity.getValue();
            String sql = "select a." + key + " as 'key',a." + value + " as 'value'" + " from " + tablename + " a";
            List<DpdEntity> list = commonDao.findMap(sql);
            return ResultData.ResultDataSuccessSelf("获取成功", list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("获取失败" + e.getMessage(), null);
        }
    }

    /**
     * 根据参数获取map类型的数据，一般用于带有关联查询性质的下拉,适用于任何只需要key value的数据
     *
     * @param data 字符串json itype 用于分类的字段，若没有分类，则将itype和itypevalue设置为相同的字段名即可
     * @return
     * @author dt
     */
    @Override
    public ResultData findMapByCondition(String data) {
        try {
            MapByConditionEntity entity = JSON.parseObject(data, MapByConditionEntity.class);
            String tablename = entity.getTablename();
            String key = entity.getKey();
            String value = entity.getValue();
            String field = entity.getField();
            String fieldvalue = entity.getFieldvalue();
            String itype = entity.getItype();
            String itypevalue = entity.getItypevalue();
            String sql = "select rownum as key,t." + value + " as value from (select a." + value + " from "
                    + tablename + " a where 1 = 1 and a." + field + " = '" + fieldvalue + "' and a." + itype
                    + " = " + itypevalue + " group by a." + value + ")t";
            List<DpdEntity> list = commonDao.findMapByCondition(sql);
            return ResultData.ResultDataSuccessSelf("获取成功", list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("获取失败" + e.getMessage(), null);
        }
    }


    /**
     * 根据表名查询表结构
     *
     * @param tablename
     * @return
     * @author cc
     */
	@Override
	public ResultData findTable(String data) {
		try {
			MapEntity entity = JSON.parseObject(data, MapEntity.class);
			List<MapEntity>  list = commonDao.findTable(entity.getTablename());
			return ResultData.ResultDataSuccessSelf("获取成功", list);
		}catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("获取失败" + e.getMessage(), null);
		}
		
	}
}
