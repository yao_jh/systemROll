package com.my.business.sys.common.service;

import com.my.business.sys.common.entity.ResultData;

import java.util.List;
import java.util.Map;

/***
 * 公共接口服务层
 * @author cc
 * @date 2019-01-17
 *
 */
public interface CommonService {

    /**
     * 根据sql语句查询多调数据
     *
     * @param sql sql语句
     * @return
     * @author cc
     */
    public List<Map<String, Object>> findManyData(String sql);

    /**
     * 根据sql语句查询单调数据
     *
     * @param sql sql语句
     * @return
     * @author cc
     */
    public Map<String, Object> findOneData(String sql);

    /**
     * 根据参数获取map类型的数据，一般用于下拉,适用于任何只需要key value的数据
     *
     * @param data 字符串json
     * @return
     * @author cc
     */
    public ResultData findMap(String data);

    /**
     * 根据参数获取map类型的数据，一般用于带有关联查询性质的下拉,适用于任何只需要key value的数据
     *
     * @param data 字符串json itype 用于分类的字段，若没有分类，则将itype和itypevalue设置为相同的字段名即可
     * @return
     * @author dt
     */
    public ResultData findMapByCondition(String data);
    
    /**
     * 根据表名查询表结构
     *
     * @param tablename
     * @return
     * @author cc
     */
    public ResultData findTable(String tablename);
}
