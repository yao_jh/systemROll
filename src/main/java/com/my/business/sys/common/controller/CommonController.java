package com.my.business.sys.common.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.common.service.CommonService;
import com.my.business.sys.controlInterface.entity.ControlInterface;
import com.my.business.sys.controlInterface.service.ControlInterfaceService;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("common")
public class CommonController {

    @Autowired
    private CommonService commonService;

    @Autowired
    private ControlInterfaceService controlInterfaceService;

    /***
     * 根据参数获取map类型的数据，一般用于下拉,适用于任何只需要key value的数据
     * @param data
     * @return
     */
    @CrossOrigin
    @PostMapping("findMap")
    @ResponseBody
    public ResultData findMap(@RequestBody String data) {
        return commonService.findMap(data);
    }
    
    /***
     * 根据表名查询表结构
     * @param data
     * @return
     */
    @CrossOrigin
    @PostMapping("findtable")
    @ResponseBody
    public ResultData findtable(@RequestBody String data) {
        return commonService.findTable(data);
    }

    /***
     * 根据参数获取map类型的数据，一般用于带有关联查询性质的下拉,适用于任何只需要key value的数据
     * @param data 字符串json itype 用于分类的字段，若没有分类，则将itype和itypevalue设置为相同的字段名即可
     * @return
     * @author dt
     */
    @CrossOrigin
    @PostMapping("findMapByCondition")
    @ResponseBody
    public ResultData findMapByCondition(@RequestBody String data) {
        return commonService.findMapByCondition(data);
    }

    /***
     * 根据自定义sql来生成数据
     * @param data
     * @return
     */
    @CrossOrigin
    @PostMapping("findData")
    @ResponseBody
    public ResultData findData(@RequestBody String data) {
        try {
            JCommon common = JSON.parseObject(data, JCommon.class);
            String key = common.getKey();
            List<ControlInterface> controlList = controlInterfaceService.findDataByControlKey(key);
            if (controlList.size() != 1) {
                return ResultData.ResultDataFaultSelf("你的配置有问题，可能存在重复的配置或者没有该配置或者配置状态没有设置", null);
            }
            String sql = controlList.get(0).getControl_sql();  //执行sql

            /***参数操作 判断是否有参数*/
            List<Map<String, String>> list = common.getList();
            Map<String, String> map = new HashMap<String, String>();
            if (null != list && list.size() > 0) {
                map = CodiUtil.toMap(list);
                sql = CodiUtil.toNewSql(map, sql);
            }
            return ResultData.ResultDataSuccess(commonService.findManyData(sql));
        } catch (Exception e) {
            return ResultData.ResultDataFaultGd(e.getMessage(), null);
        }
    }

}
