package com.my.business.sys.common.entity;

/***
 * 类似于map的json类
 * @author chenchao
 *
 */
public class MapByConditionEntity {
    private String tablename;
    private String key;
    private String value;
    private String field;
    private String fieldvalue;
    private String itype;
    private String itypevalue;

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getFieldvalue() {
        return fieldvalue;
    }

    public void setFieldvalue(String fieldvalue) {
        this.fieldvalue = fieldvalue;
    }

    public String getItype() {
        return itype;
    }

    public void setItype(String itype) {
        this.itype = itype;
    }

    public String getItypevalue() {
        return itypevalue;
    }

    public void setItypevalue(String itypevalue) {
        this.itypevalue = itypevalue;
    }
}
