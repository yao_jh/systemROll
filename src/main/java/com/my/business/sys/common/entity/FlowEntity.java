package com.my.business.sys.common.entity;


public class FlowEntity extends BaseEntity {

    private Long workflowid;  //关联工作流id
    private Long thisstepcode;  //当前步骤编码
    private Long thissteppid;  //当前步骤办理人id
    private String thissteppname;  //当前步骤办理人名称
    private Long nextsteppid;  //下一步骤办理人id
    private String nextsteppname;  //下一步骤办理人名称
    private Long modularid;   //业务单据id

    public Long getWorkflowid() {
        return workflowid;
    }

    public void setWorkflowid(Long workflowid) {
        this.workflowid = workflowid;
    }

    public Long getThisstepcode() {
        return thisstepcode;
    }

    public void setThisstepcode(Long thisstepcode) {
        this.thisstepcode = thisstepcode;
    }

    public Long getThissteppid() {
        return thissteppid;
    }

    public void setThissteppid(Long thissteppid) {
        this.thissteppid = thissteppid;
    }

    public String getThissteppname() {
        return thissteppname;
    }

    public void setThissteppname(String thissteppname) {
        this.thissteppname = thissteppname;
    }

    public Long getNextsteppid() {
        return nextsteppid;
    }

    public void setNextsteppid(Long nextsteppid) {
        this.nextsteppid = nextsteppid;
    }

    public String getNextsteppname() {
        return nextsteppname;
    }

    public void setNextsteppname(String nextsteppname) {
        this.nextsteppname = nextsteppname;
    }

    public Long getModularid() {
        return modularid;
    }

    public void setModularid(Long modularid) {
        this.modularid = modularid;
    }
}
