package com.my.business.sys.common.constant;

/***
 * 配置常量类
 * @author cc
 * @date 2019-01-17
 *
 */
public class CommonConstant {

    public static Integer fault_status = 2002;       //失败状态
    public static Integer success_status = 2000;   //成功状态
}
