package com.my.business.sys.echartstable.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
* echarts图实体类
* @author  生成器生成
* @date 2019-08-21 17:28:31
*/
public class Echartstable extends BaseEntity{
		
	private Long indocno;  //主键
	private String key;  //key
	private String strsql;  //SQL
    
    
	public void setIndocno(Long indocno){
	    this.indocno = indocno;
	}
	public Long getIndocno(){
	    return this.indocno;
	}
	public void setKey(String key){
	    this.key = key;
	}
	public String getKey(){
	    return this.key;
	}
	public void setStrsql(String strsql){
	    this.strsql = strsql;
	}
	public String getStrsql(){
	    return this.strsql;
	}

    
}