package com.my.business.sys.echartstable.dao;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.my.business.sys.echartstable.entity.Echartstable;

/**
 * echarts图dao接口
 * @author  生成器生成
 * @date 2019-08-21 17:28:31
 * */
@Mapper
public interface EchartstableDao {

	/**
	 * 添加记录
	 * @param echartstable  对象实体
	 * */
	public void insertDataEchartstable(Echartstable echartstable);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataEchartstableOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataEchartstableMany(String value);
	
	/**
	 * 修改记录
	 * @param echartstable  对象实体
	 * */
	public void updateDataEchartstable(Echartstable echartstable);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<Echartstable> findDataEchartstableByPage(@Param("key") String key,@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataEchartstableByPageSize(@Param("key") String key);
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public Echartstable findDataEchartstableByIndocno(@Param("indocno") Long indocno);
	
    /***
     * 根据key查询信息
     * @param key
     * @return
     */
    public Echartstable findDataEchartstableByKey(@Param("key") String key);
	
    
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<Echartstable> findDataEchartstable();
	
}
