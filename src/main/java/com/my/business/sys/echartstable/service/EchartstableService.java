package com.my.business.sys.echartstable.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.echartstable.entity.Echartstable;

import java.util.List;

/**
* echarts图接口服务类
* @author  生成器生成
* @date 2019-08-21 17:28:31
*/
public interface EchartstableService {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataEchartstable(String data,Long userId,String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataEchartstableOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataEchartstableMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataEchartstable(String data,Long userId,String sname);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataEchartstableByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataEchartstableByIndocno(String data);
    
    /**
     * 根据key查看一条数据信息
     * @param data 分页参数字符串
     */
    public Echartstable findDataEchartstableByKey(String data);
	
	/**
	 * 查看记录
	 * */
	public List<Echartstable> findDataEchartstable();
}
