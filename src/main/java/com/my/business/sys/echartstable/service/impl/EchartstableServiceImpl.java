package com.my.business.sys.echartstable.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.echartstable.dao.EchartstableDao;
import com.my.business.sys.echartstable.entity.Echartstable;
import com.my.business.sys.echartstable.entity.JEchartstable;
import com.my.business.sys.echartstable.service.EchartstableService;

/**
* echarts图接口具体实现类
* @author  生成器生成
* @date 2019-08-21 17:28:32
*/
@Service
public class EchartstableServiceImpl implements EchartstableService {
	
	@Autowired
	private EchartstableDao echartstableDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataEchartstable(String data,Long userId,String sname){
		try{
			JEchartstable jechartstable = JSON.parseObject(data,JEchartstable.class);
    		Echartstable echartstable = jechartstable.getEchartstable();
    		CodiUtil.newRecord(userId,sname,echartstable);
            echartstableDao.insertDataEchartstable(echartstable);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataEchartstableOne(Long indocno){
		try {
            echartstableDao.deleteDataEchartstableOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataEchartstableMany(String str_id) {
        try {
        	String sql = "delete echartstable where indocno in(" + str_id +")";
            echartstableDao.deleteDataEchartstableMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataEchartstable(String data,Long userId,String sname){
		try {
			JEchartstable jechartstable = JSON.parseObject(data,JEchartstable.class);
    		Echartstable echartstable = jechartstable.getEchartstable();
        	CodiUtil.editRecord(userId,sname,echartstable);
            echartstableDao.updateDataEchartstable(echartstable);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataEchartstableByPage(String data) {
        try {
        	JEchartstable jechartstable = JSON.parseObject(data, JEchartstable.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jechartstable.getPageIndex();
        	Integer pageSize = jechartstable.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jechartstable.getCondition()){
    			jsonObject  = JSON.parseObject(jechartstable.getCondition().toString());
    		}
        	
        	String key = null;
            
            if (!StringUtils.isEmpty(jsonObject.get("key"))) {
            	key = jsonObject.get("key").toString();
            }
    
    		List<Echartstable> list = echartstableDao.findDataEchartstableByPage(key,pageIndex, pageSize);
    		Integer count = echartstableDao.findDataEchartstableByPageSize(key);
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataEchartstableByIndocno(String data) {
        try {
        	JEchartstable jechartstable = JSON.parseObject(data, JEchartstable.class); 
        	Long indocno = jechartstable.getIndocno();
        	
    		Echartstable echartstable = echartstableDao.findDataEchartstableByIndocno(indocno);
    		return ResultData.ResultDataSuccess(echartstable);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<Echartstable> findDataEchartstable(){
		List<Echartstable> list = echartstableDao.findDataEchartstable();
		return list;
	}

	@Override
	public Echartstable findDataEchartstableByKey(String key) {
		try {
    		Echartstable echartstable = echartstableDao.findDataEchartstableByKey(key);
    		return echartstable;
        } catch (Exception e) {
        	e.printStackTrace();
        	return null;
        }
	};
}
