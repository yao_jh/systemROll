package com.my.business.util;

import com.my.business.modular.rollpushnew.entity.RollPushnew;
import com.my.business.sys.common.entity.BaseEntity;
import com.my.business.sys.common.entity.FlowEntity;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 公共方法区
 * @author cc
 * @Date 2019-02-27
 *
 */
public class CodiUtil {

    /***
     * 创建新纪录时给共用字段赋值
     * @param userId
     * @param sname
     * @param entity
     */
    public static void newRecord(Long userId, String sname, BaseEntity entity) {
        entity.setCreateid(userId);
        entity.setCreatename(sname);
        entity.setModiid(userId);
        entity.setModiname(sname);
        entity.setCreatetime(new Date());
        entity.setModitime(new Date());
    }
    
    /***
     * 推送系统初始创建工单赋值
     * @param userId
     * @param sname
     * @param entity
     */
    public static void pushNew(BaseEntity entity) {
        entity.setCreateid(0L);
        entity.setCreatename("推送时系统自动生成");
        entity.setModiid(0L);
        entity.setModiname("推送时系统自动生成");
        entity.setCreatetime(new Date());
        entity.setModitime(new Date());
    }
    
    /***
     * 推送消息时给推送人相关信息赋值
     * @param userId
     * @param sname
     * @param entity
     */
    public static void pushrecode(Long userId, String sname, RollPushnew entity) {
        entity.setPush_userid(userId);
        entity.setPush_user(sname);
        entity.setPushtime(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));
    }
    
    /***
     * 创建新纪录时给共用字段赋值（用于只有创建3个字段）
     * @param userId
     * @param sname
     * @param entity
     */
    public static void newRecordCreate(Long userId, String sname, BaseEntity entity) {
        entity.setCreateid(userId);
        entity.setCreatename(sname);
        entity.setCreatetime(new Date());
    }
    /***
     * 创建新纪录时给共用字段赋值（用于只有修改3个字段）
     * @param userId
     * @param sname
     * @param entity
     */
    public static void newRecordModi(Long userId, String sname, BaseEntity entity) {
        entity.setModiid(userId);
        entity.setModiname(sname);
        entity.setModitime(new Date());
    }


    /***
     * 创建新纪录时给共用字段赋值(用于工作流)
     * @param userId
     * @param sname
     * @param entity
     * @param modularid  绑定的业务单据号
     */
    public static void newRecord(Long userId, String sname, FlowEntity entity, Long modularid) {
        entity.setCreateid(userId);
        entity.setCreatename(sname);
        entity.setModiid(userId);
        entity.setModiname(sname);
        entity.setCreatetime(new Date());
        entity.setModitime(new Date());
        entity.setModularid(modularid);
    }

    /***
     * 创建工作流历史记录时，给相关字段赋值
     * @param workflowid  工作流id
     * @param doctypeid      业务单据主键值
     * @param stepid  当前步骤id
     * @param stepname  当前步骤名称
     * @param userId  当前办理人id
     * @param sname      当前办理人姓名
     * @param topeopleid  接受办理人id
     * @param topeoplename  接受办理人姓名
     * @param commont   意见评价
     * @param entity  工作流历史记录相关实体类
     */
//    public static void newRecordFlow(Long workflowid, Long doctypeid, Long stepid, String stepname, Long userId, String sname, Long topeopleid, String topeoplename, String commont, WorkflowHistory entity) {
//        entity.setSubmitid(userId);//设置当前办理人id
//        entity.setSubmitname(sname);//设置当前办理人姓名
//        entity.setGetid(topeopleid); //设置接受办理人id
//        entity.setGetname(topeoplename);  //设置接受办理人姓名
//        entity.setCommont(commont);  //设置意见评价
//        entity.setWorkflowid(workflowid);   //设置工作流id
//        entity.setDoctypeid(doctypeid);  //设置业务单据主键
//        entity.setStepid(stepid);   //设置步骤id
//        entity.setStepname(stepname);  //设置步骤姓名
//    }

    /***
     * 修改纪录时给共用字段赋值
     * @param userId
     * @param sname
     * @param entity
     */
    public static void editRecord(Long userId, String sname, BaseEntity entity) {
        entity.setModiid(userId);
        entity.setModiname(sname);
        entity.setModitime(new Date());
    }

    /***
     * 转移前台乱码
     * @param lm  乱码字符串
     */
    public static String returnLm(String lm) {
        try {
            return URLDecoder.decode(lm, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * 把map的list集合遍历成一个map集合
     * @param list_map  前台传递的map的json
     * @return
     */
    public static Map<String, String> toMap(List<Map<String, String>> list_map) {
        Map<String, String> map = new HashMap<String, String>();
        for (Map<String, String> m : list_map) {
            for (Map.Entry<String, String> entry : m.entrySet()) {
                map.put(entry.getKey(), entry.getValue());
            }
        }
        return map;
    }

    /***
     * 转换sql
     * @param map
     * @param sql
     * @return
     */
    public static String toNewSql(Map<String, String> map, String sql) {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            sql = sql.replace(entry.getKey(), entry.getValue());
        }
        return sql;
    }
    
    /***
     * 生成五位随机数
     * @return
     */
    public static String uuid() {
    	Long radim = (long) (Math.random()*100000);
    	String uuid = radim.toString();
    	return uuid;
    }
    
    public static void main(String[] args) {
    	System.out.println(uuid());
    }

}
