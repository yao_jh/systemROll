package com.my.business.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/***
 * 时间工具类
 * @author cc
 *
 */
public class DateUtil {
	
	/***
	 * 计算俩个时间类型的时间差，单位为分钟(第一个减第二个)
	 * d1  减数
	 * d2 被减数
	 * @return
	 */
	public static long getDateJf(Date d1,Date d2) {
		long diff = d1.getTime() - d2.getTime();// 这样得到的差值是微秒级别
		long minutes = diff / (1000 * 60);
		return minutes;
	}
	
	
	/***
	 * 根据格式，把当前系统时间转换成string类型
	 * @param format  字符串的时间格式，例如：yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String getDateToString(String format) {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		String dateString = formatter.format(date);
		return dateString;
	}
	
	/***
	 * 根据格式，把string类型时间转换成Date类型
	 * @param format  字符串的时间格式，例如：yyyy-MM-dd HH:mm:ss
	 * @param str_date  字符串时间
	 * @return
	 */
	public static Date getStringToDate(String format,String str_date) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(format);
			Date date = formatter.parse(str_date);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/***
	 * 根据格式，把当前字符串时间年月日时分秒弄成字符串拼接起来
	 * @param format
	 * @return
	 */
	public static String getDateYmdhms(String format) {
		String date = getDateToString(format);
		String year = date.substring(0, 4);
		String month = date.substring(5,7);
		String day = date.substring(8, 10);
		String hour = date.substring(11,13);
		String mid = date.substring(14, 16);
		String second = date.substring(17,19);
		return year+month+day+hour+mid+second;
	}
	
	/***
	 * 如果时间字符串长度=1，那么给她前面加0，一般用于单月，单日，单时，单分，单秒
	 * @param format
	 * @return
	 */
	public static String addZero(String format) {
		if(format.length() == 1) {
			format = "0" + format;
		}
		return format;
	}
	
	
	public static void main(String[] args) {
		System.out.println(getStringToDate("yyyy-MM-dd HH:mm:ss","2012-12-12 12:12:12"));
		System.out.println(getTimeStr(1));
		System.out.println(getTimeStr(2));
//		System.out.println(getDateToString("yyyy-MM-dd"));
	}
//  1 开始时间 2 结束时间  26日八点 到26日八点
	public static String getTimeStr(int i) {
		String timeStr =getDateToString("yyyy-MM-dd HH:mm:ss");
		String year = timeStr.substring(0, 4);
		String month = timeStr.substring(5,7);
		String day = timeStr.substring(8, 10);
		String hour = timeStr.substring(11,13);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(Long.valueOf(day)>26 || (Long.valueOf(day)>26 && Long.valueOf(hour)>=8) ){
			if(i==2){
				return year+"-"+month+"-26 08:00:00";
			}else{
				Calendar calendar = Calendar.getInstance();//日历对象
				calendar.setTime(getStringToDate("yyyy-MM-dd HH:mm:ss",year+"-"+month+"-26 08:00:00"));
				calendar.add(Calendar.MONTH, -1);//月份减一
				return sdf.format(calendar.getTime());
			}
		}else{
			if(i==2){
				Calendar calendar = Calendar.getInstance();//日历对象
				calendar.setTime(getStringToDate("yyyy-MM-dd HH:mm:ss",year+"-"+month+"-26 08:00:00"));
				calendar.add(Calendar.MONTH, -1);//月份减一
				return sdf.format(calendar.getTime());
			}else{
				Calendar calendar = Calendar.getInstance();//日历对象
				calendar.setTime(getStringToDate("yyyy-MM-dd HH:mm:ss",year+"-"+month+"-26 08:00:00"));
				calendar.add(Calendar.MONTH, -2);//月份减2
				return sdf.format(calendar.getTime());
			}
		}
	}

	public static String getTimeStrByMonth(int i,String str) {
		String timeStr =getDateToString("yyyy-MM-dd HH:mm:ss");
		String year = timeStr.substring(0, 4);
		//String month = timeStr.substring(5,7);
		//String day = timeStr.substring(8, 10);
		//String hour = timeStr.substring(11,13);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if(i==2){
				return year+"-"+str+"-26 08:00:00";
			}else{
				Calendar calendar = Calendar.getInstance();//日历对象
				calendar.setTime(getStringToDate("yyyy-MM-dd HH:mm:ss",year+"-"+str+"-26 08:00:00"));
				calendar.add(Calendar.MONTH, -1);//月份减一
				return sdf.format(calendar.getTime());
			}
	}

}
