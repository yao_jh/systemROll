package com.my.business.util;

import com.my.business.sys.common.entity.ActEntity;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/***
 * 算法工具类
 * 
 * @author cc
 *
 */
public class MathUtil {
	
	
	/***
	 * 判断double和字符串是否相等
	 * @param d1  double
	 * @param s1  字符串
	 * @param y 运算符=0,>1,<2,>=3,<=4
	 * @return
	 */
	public static Boolean  getByStringAndDouble(Double d1,String s1,Long y) {
		Double s2 = Double.valueOf(s1);
		if(y == 0) {
			return Double.doubleToLongBits(d1) == Double.doubleToLongBits(s2);
		}else if(y == 1) {
			return Double.doubleToLongBits(d1) > Double.doubleToLongBits(s2);
		}else if(y == 2) {
			return Double.doubleToLongBits(d1) > Double.doubleToLongBits(s2);
		}else if(y == 3) {
			return Double.doubleToLongBits(d1) >= Double.doubleToLongBits(s2);
		}else if(y == 4) {
			return Double.doubleToLongBits(d1) <= Double.doubleToLongBits(s2);
		}
		return false;
	}
	
	/***
	 * 根据数组集合和运算符得到集合数运算后的boolean值
	 * @param list  数据集合
	 * @param ilogical  //逻辑运算符  且&&5  或||6
	 * @return
	 */
	public static Boolean getBoolean(List<Long> list,Long ilogical) {
		int count = list.size();  //数据集合长度
		
		Boolean[] barray = new Boolean[count];  //创建一个布尔数组存放list集合转化后的结果集
		for(int i=0;i<count;i++) {//循环list集合转化结果,把数字变为布尔
			if(list.get(i) == 1L) {
				barray[i] = true;
			}else if(list.get(i) == 0L) {
				barray[i] = false;
			}
		}
		Boolean b = barray[0];   //获取布尔集合第一条数据
		for(int i = 1;i<count;i++) {  //遍历布尔集合，根据逻辑运算符计算
			if(ilogical == 5) {//&&
				b = b && barray[i];
			}else if(ilogical == 6) {//||
				b = b || barray[i];
			}
		}
		return b;
	} 
	
	
	
	
	//根据double类型数组获取数组的平均值
	public static Double getAvgByList(List<Double> list) {
		int count = list.size();
		Double sum = 0d;
		if(count != 0) {
			int i = 0;
			for(Double d : list) {
				if(d != 0) {
					i++;
					sum += d;
				}
			}
			if(i > 0) {
				sum = sum/i;
			}
		}
		return sum;
	}
	
	//double数字求和
	public static Double getSumByList(Double d1,Double d2,Double d3,Double d4,Double d5,Double d6,Double d7,Double d8,Double d9) {
		List<Double> list = new ArrayList<Double>();
		if(d1 != null) {
			list.add(d1);
		}
		if(d2 != null) {
			list.add(d2);
		}
		if(d3 != null) {
			list.add(d3);
		}
		if(d4 != null) {
			list.add(d4);
		}
		if(d5 != null) {
			list.add(d5);
		}
		if(d6 != null) {
			list.add(d6);
		}
		if(d7 != null) {
			list.add(d7);
		}
		if(d8 != null) {
			list.add(d8);
		}
		if(d9 != null) {
			list.add(d9);
		}
		
		Double sum = 0d;
		for(Double d : list) {
			if(d != null) {
				sum += d;
			}
		}
		return sum;
	}
	

	public static String dealDateFormat(String oldDate) {
		Date date1 = null;
		DateFormat df2 = null;
		try {
			oldDate = oldDate.replace("Z", " UTC");
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
			Date date = df.parse(oldDate);
			SimpleDateFormat df1 = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.UK);
			date1 = df1.parse(date.toString());
			df2 = new SimpleDateFormat("yyyy-MM-dd");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return df2.format(date1);
	}

	// 固定长度换行
	public static String getStringByEnter(int length, String string) throws Exception {
		for (int i = 1; i <= string.length(); i++) {
			if (string.substring(0, i).length() > length) {
				return string.substring(0, i - 1) + "\n" + getStringByEnter(length, string.substring(i - 1));
			}
		}
		return string;
	}

	/***
	 * 把字符串装换成时间
	 * 
	 * @param date    传递的字符串
	 * @param pattern 转换的字符串格式，例如：yyyy-MM-dd
	 * @return
	 */
	public static Date convertString2Date(String dateStr, String pattern) throws ParseException {
		if (dateStr == null) {
			return null;
		}
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		return formatter.parse(dateStr);
	}

	/***
	 * 根据时间区间返回时间区间中的时间集合
	 * 
	 * @param starttime
	 * @param endtime
	 * @return
	 */
	public static List<String> getTimeList(String starttime, String endtime) {
		List<String> list = new ArrayList<String>();
		Long count = between_days(starttime, endtime); // 获取天数差
		list.add(starttime); // 无论怎样都把开始时间加入集合中
		if (count > 0) { // 如果日期天数大于0天,那么返回的日期集合就不止一个开始时间
			for (int i = 1; i <= count; i++) {
				String str_time = getAddTime(starttime, i);
				list.add(str_time);
			}
		}
		return list;
	}

	/***
	 * 字符串时间添加天数得到新的字符串时间
	 * 
	 * @param times
	 * @param daytime
	 * @return
	 */
	public static String getAddTime(String times, int daytime) {
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date dd = df.parse(times);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dd);
			calendar.add(Calendar.DAY_OF_MONTH, daytime);// 加一天
			return df.format(calendar.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/***
	 * 根据时间范围计算时间天数差值
	 * 
	 * @param starttime 开始时间
	 * @param endtime   结束时间
	 * @return
	 */
	public static Long between_days(String starttime, String endtime) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");// 自定义时间格式
		Calendar calendar_a = Calendar.getInstance();// 获取日历对象
		Calendar calendar_b = Calendar.getInstance();
		Date date_a = null;
		Date date_b = null;
		try {
			date_a = simpleDateFormat.parse(starttime);// 字符串转Date
			date_b = simpleDateFormat.parse(endtime);
			calendar_a.setTime(date_a);// 设置日历
			calendar_b.setTime(date_b);
		} catch (ParseException e) {
			e.printStackTrace();// 格式化异常
		}
		long time_a = calendar_a.getTimeInMillis();
		long time_b = calendar_b.getTimeInMillis();
		long between_days = (time_b - time_a) / (1000 * 3600 * 24);// 计算相差天数
		return between_days;
	}

	/***
	 * 获取2个数的百分比
	 * 
	 * @param math1 除数
	 * @param math2 被除数
	 * @return
	 */
	public static String getDataRate(Long math1, Long math2) {
		double result = Double.valueOf(math1) / Double.valueOf(math2);
		// 创建一个数值格式化对象     
		NumberFormat nf = NumberFormat.getPercentInstance();
		// 设置精确到小数点后2位     
		nf.setMaximumFractionDigits(2);
		String str = nf.format(result);
		return str;
	}

	/***
	 * 计算2个字符时间的差值
	 * 
	 * @param start 被减数
	 * @param end   减数
	 * @return
	 */
	public static Long getDateSubtraction(String start, String end) {
		SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		Date startDate = null;
		Date endDate = null;
		try {
			startDate = inputFormat.parse(start);
			endDate = inputFormat.parse(end);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		// 得到两个日期对象的总毫秒数
		Long firstDateMilliSeconds = startDate.getTime();
		Long secondDateMilliSeconds = endDate.getTime();
		// 得到两者之差
		Long firstMinusSecond = secondDateMilliSeconds - firstDateMilliSeconds;
		// 得到分钟
		Long totalMinutes = (firstMinusSecond / 1000 / 60);
		return totalMinutes;
	}

	/***
	 * 计算合格时长(分)
	 * 
	 * @param if_ok
	 * @param entity
	 * @param start
	 * @param end
	 */
	public static void setSacctime(Long if_ok, ActEntity entity, String start, String end) {
		Long sacctime = getDateSubtraction(start, end);
		if (if_ok == 1) { // 只要新纪录合格,那么就是合格
			entity.setSacctime(sacctime);
		} else {
			entity.setSacctime(0L);
		}
	}

	/***
	 * 赋值实际精度,精度范围,评价分数,是否合格
	 * 
	 * @param entity
	 * @param min     最小值
	 * @param max     最大值
	 * @param maxmath 最高分
	 */
	public static Long change(ActEntity entity, Double min, Double max, Long maxmath) {
		DecimalFormat df = new DecimalFormat("#0.00");
		List<Double> list = new ArrayList<Double>();
		list.add(entity.getSact1());
		list.add(entity.getSact2());
		list.add(entity.getSact3());
		list.add(entity.getSact4());
		list.add(entity.getSact5());
		list.add(entity.getSact6());
		list.add(entity.getSact7());
		list.add(entity.getSact8());
		list.add(entity.getSact9());
		String val = df.format(subtraction(max, min));
		Double scope = Double.valueOf(val); // 精度范围
		entity.setScope(scope);
		Double average = Double.valueOf(df.format(allAverage(list))); // 实际精度
		entity.setSactmea(average);
		if (average > min && average < max) {
			average -= min;
			Double a = (average / scope) * maxmath;
			String b = df.format(a);
			String pjfs = String.valueOf(df.format((average / scope) * maxmath)); // 评价分数
			entity.setScode(pjfs);
			entity.setIf_ok(1L); // 合格
			return 1L;
		} else {
			entity.setIf_ok(0L); // 不合格
			entity.setScode(String.valueOf(0));
			return 0L;
		}
	}

	/**
	 * 计算多个数的平均值(涉及小数)
	 *
	 * @param args_array 数据数组
	 * @return
	 */
	public static Double allAverage(List<Double> args_array) {
		Double sum = 0.00;
		if (args_array.size() > 0) {
			for (Double d : args_array) {
				sum += d;
			}
		}
		Double average = sum / args_array.size();
		return average;
	}

	/***
	 * 计算2个数的差值，第一个数减第二个数
	 * 
	 * @param a 减数
	 * @param b 被减数
	 * @return
	 */
	public static Double subtraction(Double a, Double b) {
		return a - b;
	}

	/***
	 * 判断是否在否一个区间内
	 * 
	 * @param num 某个数值
	 * @param min 区间的最小值
	 * @param max 区间的最大值
	 * @return
	 */
	public static Boolean ifScope(Double num, Double min, Double max) {
		if (num > min && num < max) {
			return true;
		} else {
			return false;
		}
	}

	/***
	 * 保留小数点后几位
	 * 
	 * @param num 某个数值
	 * @param fix 保留小数后的位数
	 * @return
	 */
	public static Double tofixed(Double num, int fix) {
		String zero = "#0.";
		if (0 == fix) {
			zero = "#0";
		} else {
			for (int i = 1; i <= fix; i++) {
				zero = zero + "0";
			}
		}
		DecimalFormat df = new DecimalFormat(zero);
		Double number = Double.valueOf(df.format(num));
		return number;
	}

	/**************************** 时间操作 ***********************/
	/***
	 * 把时间装换成字符串
	 * 
	 * @param date    传递的时间
	 * @param pattern 转换的字符串格式，例如：yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String convertDate2String(Date date, String pattern) {
		if (date == null)
			return null;
		DateFormat dateFormat = new SimpleDateFormat(pattern);
		return dateFormat.format(date);
	}

	/***
	 * 把时间装换成字符串并获取年份
	 * 
	 * @param date    传递的时间
	 * @param pattern 转换的字符串格式，例如：yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static int getYear(Date date, String pattern) {
		String timeStr = convertDate2String(date, pattern);
		return Integer.parseInt(timeStr.substring(0, 4));
	}

	/***
	 * 把时间装换成字符串并获取月份
	 * 
	 * @param date    传递的时间
	 * @param pattern 转换的字符串格式，例如：yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static int getMonth(Date date, String pattern) {
		String timeStr = convertDate2String(date, pattern);
		return Integer.parseInt(timeStr.substring(5, 7));
	}

	/***
	 * 把时间装换成字符串并获取日
	 * 
	 * @param date    传递的时间
	 * @param pattern 转换的字符串格式，例如：yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static int getDay(Date date, String pattern) {
		String timeStr = convertDate2String(date, pattern);
		return Integer.parseInt(timeStr.substring(8, 10));
	}

	/***
	 * 把时间装换成字符串并获取小时
	 * 
	 * @param date    传递的时间
	 * @param pattern 转换的字符串格式，例如：yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static int getHour(Date date, String pattern) {
		String timeStr = convertDate2String(date, pattern);
		return Integer.parseInt(timeStr.substring(11, 13));
	}

	/***
	 * 把时间装换成字符串并获取分钟
	 * 
	 * @param date    传递的时间
	 * @param pattern 转换的字符串格式，例如：yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static int getMinute(Date date, String pattern) {
		String timeStr = convertDate2String(date, pattern);
		return Integer.parseInt(timeStr.substring(14, 16));
	}

	/***
	 * 把时间装换成字符串并获取秒
	 * 
	 * @param date    传递的时间
	 * @param pattern 转换的字符串格式，例如：yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static int getSecond(Date date, String pattern) {
		String timeStr = convertDate2String(date, pattern);
		return Integer.parseInt(timeStr.substring(17, 19));
	}

	/***
	 * 获取当前时间的时间戳
	 * 
	 * @return
	 */
	public static String getTimeCurent() {
		String timeStr = convertDate2String(new Date(), "yyyy-MM-dd HH:mm:ss");
		String newtime = timeStr.substring(0, 4) + timeStr.substring(5, 7) + timeStr.substring(8, 10)
				+ timeStr.substring(11, 13) + timeStr.substring(14, 16) + timeStr.substring(17, 19);
		return newtime;
	}

}
