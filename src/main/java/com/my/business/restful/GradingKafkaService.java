package com.my.business.restful;


import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.consumepredict.dao.ConsumePredictDao;
import com.my.business.modular.consumepredict.entity.ConsumePredict;
import com.my.business.modular.consumepredict.entity.ConsumePredictYc;
import com.my.business.modular.rollonoffline.entity.RollOnoffLine;
import com.my.business.modular.rollonoffline.entity.RollOnoffLineBc;
import com.my.business.modular.rollwear.entity.RollWearInformation;
import com.my.business.schedule.AllKafka;
import com.my.business.util.CodiUtil;
import com.my.business.util.DateUtil;

import lombok.extern.apachecommons.CommonsLog;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/***
 * 磨削发成本kafka接口
 * @author cc
 *
 */
@CommonsLog
@Service
public class GradingKafkaService {

    private final static Log logger = LogFactory.getLog(GradingKafkaService.class);

    @Autowired
    private ConsumePredictDao consumePredictDao;
    @Autowired
    private AllKafka allKafka;


    /***
     * 磨削前磨削预测发送
     * @param act_coilid   钢卷号
     */
    public void sendForecast(String act_coilid) {
        try {
        	List<ConsumePredict> list = consumePredictDao.findDataConsumePredictByact_coilid(act_coilid);
        	if(list != null && list.size() > 0) {
        		ConsumePredictYc yc = new ConsumePredictYc();
            	Double w_mmconskgt = 0.0;  //工作辊预测消耗
            	Double w_moneyconsume = 0.00; //工作辊预测费用
            	Double r_mmconskgt = 0.0; //支撑辊预测消耗
            	Double r_moneyconsume = 0.0; //支撑辊预测费用
            	for(ConsumePredict c : list) {
            		if(c.getRoll_type().indexOf("工作辊") > 0) {
            			w_mmconskgt  = w_mmconskgt + c.getMmconskgt();
            			w_moneyconsume = w_moneyconsume + c.getMoneyconsume();
            		}
            		if(c.getRoll_type().indexOf("支撑辊") > 0) {
            			r_mmconskgt = r_mmconskgt + c.getMmconskgt();
            			r_moneyconsume = r_moneyconsume + c.getMoneyconsume();
            		}
            	}
            	yc.setAct_coilid(act_coilid);
            	yc.setW_mmconskgt(w_mmconskgt);
            	yc.setW_moneyconsume(w_moneyconsume);
            	yc.setR_mmconskgt(r_mmconskgt);
            	yc.setR_moneyconsume(r_moneyconsume);
                if (yc != null) {
                    JSONObject request = new JSONObject();
                    request.put("id", "GAP_FORECAST");
                    request.put("sender", "GR");
                    String uuid = DateUtil.getDateYmdhms("yyyy-MM-dd HH:mm:ss") + CodiUtil.uuid();
                    request.put("msgid", uuid);

                    yc.setGAPsender("GRJ_01");
                    yc.setGAPreceiver("CB_01");
                    request.put("body", yc);

                    String message = request.toJSONString();

                    System.out.println("预测发送接口数据为" + message);
                    logger.debug("预测发送接口数据为" + message);
                    allKafka.FSPsend(message);
                }
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * 磨削前磨削实际发送
     * @param r   磨削实际费用实体
     */
    public void sendReal(RollWearInformation r) {
        try {
            JSONObject request = new JSONObject();
            request.put("id", "GAP_REAL");
            request.put("sender", "GR");
            String uuid = DateUtil.getDateYmdhms("yyyy-MM-dd HH:mm:ss") + CodiUtil.uuid();
            request.put("msgid", uuid);

            r.setGAPsender("GRJ_02");
            r.setGAPreceiver("CB_02");
            request.put("body", r);

            String message = request.toJSONString();

            logger.debug("实际发送接口数据为" + message);
            allKafka.FSPsend(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * 上下线实绩发成本
     * @param onoffline w_no   工作辊号
     * @param onoffline w_real_stock 工作辊实际磨削量
     * @param onoffline w_roll_type  轧辊类型
     * @param onoffline w_roll_frame_no  机架号
     */
    public void sendonoffline(RollOnoffLine onoffline) {
        try {
            RollOnoffLineBc bc = new RollOnoffLineBc();
            BeanUtils.copyProperties(onoffline, bc);  //把entity属性赋值给后面对象
            JSONObject request = new JSONObject();
            request.put("id", "GAP_ONOFFLINE");
            request.put("sender", "GR");
            String uuid = DateUtil.getDateYmdhms("yyyy-MM-dd HH:mm:ss") + CodiUtil.uuid();
            request.put("msgid", uuid);

            bc.setGAPsender("GRJ_03");
            bc.setGAPreceiver("CB_03");
            request.put("body", bc);
            String message = request.toJSONString();
            logger.debug("上下机实绩数据为" + message);
            allKafka.FSPsend(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
