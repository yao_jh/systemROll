package com.my.business.restful;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;

import lombok.extern.apachecommons.CommonsLog;

/***
 * 磨削预测kafka触发接口
 * @author cc
 *
 */
@CommonsLog
@Service
public class ForecastService {

	private final static Log logger = LogFactory.getLog(GradingKafkaService.class);

	@Autowired
	private GradingKafkaService service;
	
//	@KafkaListener(topicPattern = "DM", groupId = "GR02")
//    private void ruleEngineMsg(ConsumerRecord<String, String> consumerRecord) throws Exception {
//        String message = consumerRecord.value();
//        JSONObject obj = JSONObject.parseObject(message);
//        String id = obj.getString("id");
//        
//        if(id.equals("ConsumPredict")) {
//        	logger.debug("接受的数据为" + message);
//        	String act_coilid = obj.getJSONObject("body").getString("coilid");   //钢卷号
//        	if(!StringUtils.isEmpty(act_coilid)) {
//        		logger.debug("钢卷号为" + act_coilid);
//        		service.sendForecast(act_coilid);
//        	}
//        }
//	}
}
