package com.my.business.restful;

import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.historyrollground.dao.HistoryRollGroundDao;
import com.my.business.modular.historyrollground.entity.HistoryRollGround;
import com.my.business.modular.rolldatafiles.dao.RollDataFilesDao;
import com.my.business.modular.rolldatafiles.entity.RollDataFiles;
import com.my.business.modular.rolldatafiles.service.RollDataFilesService;
import com.my.business.modular.rollgrinding.dao.RollGrindingBFDao;
import com.my.business.modular.rollgrinding.dao.RollGrindingDao;
import com.my.business.modular.rollgrinding.entity.RollGrinding;
import com.my.business.modular.rollgrinding.entity.RollGrindingBF;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.restful.entity.*;
import lombok.extern.apachecommons.CommonsLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@CommonsLog
@Component
public class RestService {
    @Value("${resturlHisttory}")
    private String resturlHisttory;
    @Value("${resturlolldate}")
    private String resturlolldate;
    @Value("${pageSize}")
    private String pageSize;
    @Autowired
    private HistoryRollGroundDao historyRollGroundDao;
    @Autowired
    private RollDataFilesDao rollDataFilesDao;
    @Autowired
    private RollGrindingBFDao rollGrindingBFDao;
    @Autowired
    private RollInformationDao rollInformationDao;
    @Autowired
    private RollDataFilesService rollDataFilesService;


    @Transactional(rollbackFor = {Exception.class})
    //@Scheduled(cron = "${timelist}")
    public void scheduledTasks()
            throws ParseException {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Content-Type", "application/json");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        RestTemplate restTemplate = new RestTemplate();

        for (int k = 1; k < 7; k++) {
            JHistory_roll_ground jHistory_roll_ground = new JHistory_roll_ground();
            jHistory_roll_ground.setPageSize(Integer.valueOf(this.pageSize));
            jHistory_roll_ground.setPageIndex(1);
            Date newTime = this.historyRollGroundDao.findTime((long) k);
            jHistory_roll_ground.setStart_date_time(newTime);
            jHistory_roll_ground.setGrinder_no((long) k);

            JRoll_data_files jRoll_data_files = new JRoll_data_files();
            jRoll_data_files.setPageSize(Integer.valueOf(this.pageSize));
            jRoll_data_files.setPageIndex(1);
            Date newTimeDataFiles = this.rollDataFilesDao.findTime((long) k);
            jRoll_data_files.setInitial_date_time(newTimeDataFiles);
            jRoll_data_files.setGrinder((long) k);
            History_roll_ground[] list = restTemplate.postForObject(this.resturlHisttory, JSONObject.toJSONString(jHistory_roll_ground), History_roll_ground[].class);
            Roll_data_files[] lists = restTemplate.postForObject(this.resturlolldate, JSONObject.toJSONString(jRoll_data_files), Roll_data_files[].class);
            if ((list != null) && (list.length > 0)) {
                HistoryRollGround historyRollGround = new HistoryRollGround();
                for (History_roll_ground entity : list) {
                    BeanUtils.copyProperties(entity, historyRollGround);
                    Integer i = this.historyRollGroundDao.findDataHave(entity.getStart_date_time(), (long) k);
                    if (i == 0) {
                        this.historyRollGroundDao.insertDataHistoryRollGround(historyRollGround);
                    }
                }
            }
            historyRollGroundDao.clear();
            Integer i;
            if ((lists != null) && (lists.length > 0)) {
                RollDataFiles rollDataFiles = new RollDataFiles();
                for (Roll_data_files entity : lists) {
                    BeanUtils.copyProperties(entity, rollDataFiles);
                    rollDataFiles.setFirst_theopro(String.valueOf(entity.getFirst_theopro()));
                    rollDataFiles.setFirst_realpro(String.valueOf(entity.getFirst_realpro()));
                    rollDataFiles.setFinal_theopro(String.valueOf(entity.getFinal_theopro()));
                    rollDataFiles.setFinal_realpro(String.valueOf(entity.getFinal_realpro()));
                    rollDataFiles.setRoundness(String.valueOf(entity.getRoundness()));

                    i = this.rollDataFilesDao.findDataHave(entity.getInitial_date_time(), (long) k);
                    if (i == 0) {
                        this.rollDataFilesDao.insertDataRollDataFiles(rollDataFiles);
                    }
                }
            }
            rollDataFilesDao.clear();


            String start_date_time = this.rollGrindingBFDao.findTime(String.valueOf(k));
            int pageIndex = 1;
            String sdt = null;
            if (!StringUtils.isEmpty(start_date_time)) {
                sdt = start_date_time;
            }
            List<HistoryRollGround> historyRollGroundList = this.historyRollGroundDao.findDataHistoryRollGroundByPage(pageIndex - 1, Integer.valueOf(this.pageSize), sdt, (long) k,null);
            RollGrindingBF rollGrindingBF;
            if ((historyRollGroundList != null) && (historyRollGroundList.size() > 0)) {
                rollGrindingBF = new RollGrindingBF();
                for (HistoryRollGround entitygrinding : historyRollGroundList) {
                    String time = null;
                    if (entitygrinding.getStart_date_time() != null) {
                        time = sdf.format(entitygrinding.getStart_date_time());
                    }
                    Integer j = this.rollGrindingBFDao.findDataHave(time, String.valueOf(k));
                    if (j == 0) {
                        rollGrindingBF.setRoll_no(entitygrinding.getRoll_nb());
                        //rollGrindingBF.setMaterial(entitygrinding.getRoll_material());
                        //rollGrindingBF.setRoll_type(entitygrinding.getRoll_type());
                        if (entitygrinding.getStart_date_time() != null) {
                            rollGrindingBF.setGrind_starttime(sdf.format(entitygrinding.getStart_date_time()));
                        }
                        if (entitygrinding.getEnd_date_time() != null) {
                            rollGrindingBF.setGrind_endtime(sdf.format(entitygrinding.getEnd_date_time()));
                        } else {
                            rollGrindingBF.setGrind_endtime(null);
                        }
                        rollGrindingBF.setMachineno(String.valueOf(entitygrinding.getGrinder_no()));
                        rollGrindingBF.setBefore_diameter(Double.valueOf(String.valueOf(entitygrinding.getDiam_before_mid())));
                        rollGrindingBF.setAfter_diameter(Double.valueOf(String.valueOf(entitygrinding.getPresent_diam_mid())));
                        //rollGrindingBF.setDeviation(Double.valueOf(String.valueOf(entitygrinding.getProfile())));
                        //rollGrindingBF.setDiametermax(Double.valueOf(String.valueOf(entitygrinding.getPresent_diam_max())));
                        //rollGrindingBF.setDiametermin(Double.valueOf(String.valueOf(entitygrinding.getPresent_diam_min())));
                        rollGrindingBF.setTaper(Double.valueOf(String.valueOf(entitygrinding.getTaper())));
                        rollGrindingBF.setRoundness(Double.valueOf(String.valueOf(entitygrinding.getRoundness())));
                        /*if (entitygrinding.getCrack() != null) {
                            rollGrindingBF.setCrack(Double.valueOf(String.valueOf(entitygrinding.getCrack())));
                        }
                        if (entitygrinding.getBruise() != null) {
                            rollGrindingBF.setHidden_flaws((double) entitygrinding.getBruise());
                        }*/
                        if (!StringUtils.isEmpty(entitygrinding.getWheel_dia_start())) {
                            rollGrindingBF.setWheel_dia_start((double) entitygrinding.getWheel_dia_start());
                        }
                        if (!StringUtils.isEmpty(entitygrinding.getWheel_dia_end())) {
                            rollGrindingBF.setWheel_dia_end((double) entitygrinding.getWheel_dia_end());
                        }
                        //rollGrindingBF.setProduction_line_id(1L);
                        //rollGrindingBF.setProduction_line("2250");
                        this.rollGrindingBFDao.insertDataRollGrindingBF(rollGrindingBF);
                        if (!StringUtils.isEmpty(rollGrindingBF.getBefore_diameter()) && !StringUtils.isEmpty(rollGrindingBF.getAfter_diameter())){
                            if (rollGrindingBF.getBefore_diameter() != 0 && rollGrindingBF.getAfter_diameter() != 0){
                                Map<String, String> map = rollDataFilesService.AnalysisDifference(rollGrindingBF.getRoll_no(), rollGrindingBF.getGrind_starttime());
                                Double count = null;
                                if (!StringUtils.isEmpty(map.get("合格点数"))) {
                                    count = Double.valueOf(map.get("合格点数"));
                                }else {
                                    count = null;
                                }
                                rollGrindingBF.setQualifiednum(count);
                                rollGrindingBFDao.updateDataRollGrindingBF(rollGrindingBF);
                            }
                        }

                        RollInformation rollInformation = rollInformationDao.findDataRollInformationByRollNo(rollGrindingBF.getRoll_no());
                        if (!StringUtils.isEmpty(rollInformation)) {
                            if (rollGrindingBF.getAfter_diameter() != 0) {
                                rollInformation.setCurrentdiameter(rollGrindingBF.getAfter_diameter());//更新当前辊径
                            }
                            rollInformation.setLastgrindingtime(rollGrindingBF.getGrind_starttime());//最后一次磨削时间
                            rollInformation.setLastgrindingdepth(rollGrindingBF.getBefore_diameter() - rollGrindingBF.getAfter_diameter());
                            rollInformation.setRoundness(rollGrindingBF.getRoundness());
                            rollInformation.setMachine_id(Long.valueOf(rollGrindingBF.getMachineno()));
                            rollInformation.setUplinecount(0L);
                            rollInformationDao.updateDataRollInformation(rollInformation);
                        }
                    }
                }
            }
        }
    }
}
