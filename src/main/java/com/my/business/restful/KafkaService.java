package com.my.business.restful;

import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollstiffness.dao.RollStiffnessDao;
import com.my.business.modular.rollstiffness.entity.RollStiffness;
import com.my.business.modular.rollstiffness.service.RollStiffnessService;
import com.my.business.modular.rollstiffnessmath.dao.RollStiffnessMathDao;
import com.my.business.modular.rollstiffnessmath.entity.RollStiffnessMath;
import com.my.business.restful.entity.KafkaBody;
import com.my.business.schedule.AllKafka;
import lombok.extern.apachecommons.CommonsLog;
import lombok.extern.slf4j.Slf4j;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
//import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Title:
 * Packet:cn.edu.ustb.nercar.service
 * Description:
 * Author:崔庆胜
 * Create Date: 2020/7/10 5:53 PM
 * Modify User:
 * Modify Date:
 * Modify Description:
 */
@Slf4j
/*@Component*/
@CommonsLog
@Service
public class KafkaService {
    @Value("${pageSize}")
    private String pageSize;
    @Autowired
    private RollStiffnessMathDao rollStiffnessMathDao;
    @Autowired
    private RollStiffnessDao rollStiffnessDao;
    @Autowired
    private RollStiffnessService rollStiffnessService;
    @Autowired
    private AllKafka allKafka;

    /**
     * 钢卷服务模型监听其他模块消息事件
     * * 规则引擎解析请求业务逻辑：
     * * 1、接收的参数包含：钢卷号、规则引擎id，变量信息....
     * * 2、根据规则引擎id去查找规则引擎公式和公式包含变量信息（varInfo）
     * * 3、解析规则引擎公式（包含：公式解析和公式中变量值查找）
     * *    3.1、公式解析：引用别人写好的jar包，调用方法对公式进行解析
     * *    3.2、查找变量值：首先查找 接收到的变量信息，如果没有，则去查找redis中对应钢卷号和变量的值，
     * *       如果还没有则去数据字典中根据API去查找变量值
     * *  4、将解析的结果发送事件给请求方，同时将记录保存数据库等）
     *
     * @param //consumerRecord
     */
    /*@KafkaListener(topicPattern = "DM", groupId = "GRSH_01")
    private void ruleEngineMsg(ConsumerRecord<String, String> consumerRecord) throws Exception {
        String topic = consumerRecord.topic();
        String message = consumerRecord.value();
        JSONObject obj = JSONObject.parseObject(message);
        String eventId = obj.getString("id");
        String receiver = obj.getJSONObject("body").getString("PDPAreceiver");
        if (obj.getJSONObject("body") != null) {
            receiver = obj.getJSONObject("body").getString("PDPAreceiver");
        }
        String msgid = obj.getString("msgId");


        //业务代码开始
        try {
            //规则引擎事件接收
            if (eventId.equals("GAP_ZERO")) {
                if (receiver.equals("GRSH_01")) {
                    log.debug("PDPAreceiver is " + consumerRecord.topic() + ", " + consumerRecord.value());
                    RollStiffnessMath rollStiffnessMath = new RollStiffnessMath();
                    rollStiffnessMath.setProduction_line_id(1L);
                    rollStiffnessMath.setProduction_line("2250");
                    rollStiffnessMath.setFrame_no(obj.getJSONObject("body").getString("frame_no"));
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("retention"))) {
                        rollStiffnessMath.setRetention(Double.valueOf(obj.getJSONObject("body").getString("retention")));//刚度保持率(%)
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("leveling"))) {
                        rollStiffnessMath.setLeveling(Double.valueOf(obj.getJSONObject("body").getString("leveling")));//调平值(mm)
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("bothsides_stiffness"))) {
                        rollStiffnessMath.setBothsides_stiffness(Double.valueOf(obj.getJSONObject("body").getString("bothsides_stiffness")));//两侧刚度偏差(KN/mm)
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("bothposition_stiffness"))) {
                        rollStiffnessMath.setBothposition_stiffness(Double.valueOf(obj.getJSONObject("body").getString("bothposition_stiffness")));  //两侧位置偏差(mm)
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("osposition"))) {
                        rollStiffnessMath.setOsposition(Double.valueOf(obj.getJSONObject("body").getString("osposition")));  //OS出入口位置偏差(mm)
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("dsposition"))) {
                        rollStiffnessMath.setDsposition(Double.valueOf(obj.getJSONObject("body").getString("dsposition")));  //DS出入口位置偏差(mm)
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("rolling_force"))) {
                        rollStiffnessMath.setRolling_force(Double.valueOf(obj.getJSONObject("body").getString("rolling_force")));  //轧制力偏差(KN)
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("osrollgap"))) {
                        rollStiffnessMath.setOsrollgap(Double.valueOf(obj.getJSONObject("body").getString("osrollgap")));  //OS辊缝偏差(mm)
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("dsrollgap"))) {
                        rollStiffnessMath.setDsrollgap(Double.valueOf(obj.getJSONObject("body").getString("dsrollgap")));  //DS辊缝偏差(mm)
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("sendtime"))) {
                        rollStiffnessMath.setDemarcate(obj.getJSONObject("body").getString("sendtime"));//python发送过来的时间
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("demarcate"))) {
                        rollStiffnessMath.setRecordtime(obj.getJSONObject("body").getString("demarcate"));//标定时间
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("gapzeroontime"))) {
                        rollStiffnessMath.setGapzeroontime(obj.getJSONObject("body").getString("gapzeroontime"));//标定开始时间
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("gapzeroofftime"))) {
                        rollStiffnessMath.setGapzeroofftime(obj.getJSONObject("body").getString("gapzeroofftime"));//标定结束时间
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("lcosstiffness"))) {
                        rollStiffnessMath.setLcosstiffness(Double.valueOf(obj.getJSONObject("body").getString("lcosstiffness")));//LC——OS侧刚度
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("lcdsstiffness"))) {
                        rollStiffnessMath.setLcdsstiffness(Double.valueOf(obj.getJSONObject("body").getString("lcdsstiffness")));//LC——DS侧刚度
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("ptosstiffness"))) {
                        rollStiffnessMath.setPtosstiffness(Double.valueOf(obj.getJSONObject("body").getString("ptosstiffness")));//PT——OS侧刚度
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("ptdsstiffness"))) {
                        rollStiffnessMath.setPtdsstiffness(Double.valueOf(obj.getJSONObject("body").getString("ptdsstiffness")));//PT——DS侧刚度
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("osenposition"))) {
                        rollStiffnessMath.setOsenposition(Double.valueOf(obj.getJSONObject("body").getString("osenposition")));//OS入口位置
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("osexposition"))) {
                        rollStiffnessMath.setOsexposition(Double.valueOf(obj.getJSONObject("body").getString("osexposition")));//OS出口位置
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("dsenposition"))) {
                        rollStiffnessMath.setDsenposition(Double.valueOf(obj.getJSONObject("body").getString("dsenposition")));//DS入口位置
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("dsexposition"))) {
                        rollStiffnessMath.setDsexposition(Double.valueOf(obj.getJSONObject("body").getString("dsexposition")));//DS出口位置
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("lcosforce"))) {
                        rollStiffnessMath.setLcosforce(Double.valueOf(obj.getJSONObject("body").getString("lcosforce")));//LC——OS侧轧制力
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("lcdsforce"))) {
                        rollStiffnessMath.setLcdsforce(Double.valueOf(obj.getJSONObject("body").getString("lcdsforce")));//LC——DS侧轧制力
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("ptosforce"))) {
                        rollStiffnessMath.setPtosforce(Double.valueOf(obj.getJSONObject("body").getString("ptosforce")));//PT——OS侧轧制力
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("ptdsforce"))) {
                        rollStiffnessMath.setPtdsforce(Double.valueOf(obj.getJSONObject("body").getString("ptdsforce")));//PT——DS侧轧制力
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("upstepwedge"))) {
                        rollStiffnessMath.setUpstepwedge(Double.valueOf(obj.getJSONObject("body").getString("upstepwedge")));//上阶梯位实际位置
                    }
                    if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("dnstepwedge"))) {
                        rollStiffnessMath.setDnstepwedge(Double.valueOf(obj.getJSONObject("body").getString("dnstepwedge")));//下阶梯位实际位置
                    }
                    //根据标定时间，机架，产线作为联合主键
                    List<RollStiffnessMath> counts = rollStiffnessMathDao.findDataNewCount(1L, obj.getJSONObject("body").getString("frame_no"));
                    if (counts.size()>1){
                        for (int i = 0;i<counts.size();i++){
                            if (i != 0){
                                rollStiffnessMathDao.deleteDataRollStiffnessMathOne(counts.get(i).getIndocno());
                            }
                        }
                    }
                    RollStiffnessMath rm = rollStiffnessMathDao.findDataNew(1L, obj.getJSONObject("body").getString("frame_no"));
                    if (!StringUtils.isEmpty(rm)) {
                        //如果旧数据的后五条是有值的，那就说明旧数据完结，插入新数据就好
                        if (!StringUtils.isEmpty(rm.getLeveling())) {
                            if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("retention")) && StringUtils.isEmpty(obj.getJSONObject("body").getString("leveling"))) {
                                rollStiffnessMathDao.insertDataRollStiffnessMath(rollStiffnessMath);
                            }else{
                                rollStiffnessMathDao.updateDataRollStiffnessMathByCon(rollStiffnessMath,rollStiffnessMath.getRecordtime(),1L,rollStiffnessMath.getFrame_no());
                            }
                        } else {
                            //如果旧数据的后五条是空的，那就说明旧数据没结束
                            //判断传来的是前四还是后五
                            if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("retention"))) {
                                rollStiffnessMath.setIndocno(rm.getIndocno());
                                rollStiffnessMathDao.updateDataRollStiffnessMathA(rollStiffnessMath);
                            } else if (StringUtils.isEmpty(obj.getJSONObject("body").getString("retention"))) {
                                rollStiffnessMath.setIndocno(rm.getIndocno());
                                rollStiffnessMathDao.updateDataRollStiffnessMathB(rollStiffnessMath);
                                RollStiffnessMath rs = rollStiffnessMathDao.findDataRollStiffnessMathByIndocno(rm.getIndocno());
                                score(rs, msgid);
                            }
                        }
                    } else {
                        if (!StringUtils.isEmpty(obj.getJSONObject("body").getString("retention")) && StringUtils.isEmpty(obj.getJSONObject("body").getString("leveling"))) {
                            rollStiffnessMathDao.insertDataRollStiffnessMath(rollStiffnessMath);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    public String jsonToString(RollStiffness rs, String msgid) {
        JSONObject request = new JSONObject();
        request.put("id", "GAP_ZERO");
        request.put("sender", "GRSH");//
        request.put("masgId", msgid);
        KafkaBody body = new KafkaBody();
        body.setStandNo(rs.getFrame_no());  //机架
        body.setZeroTime(rs.getRecordtime());//标定结束最晚时刻
        body.setPDPAreceiver("PDPA");//接收人

        List<Map<String, String>> ic = new ArrayList<Map<String, String>>();

        Map<String, String> m1 = new HashMap<String, String>();
        m1.put("weight", "10");

        if (!StringUtils.isEmpty(rs.getRetention())) {
            m1.put("rigrfRetentionrate", rs.getRetention().toString());
        } else {
            m1.put("rigrfRetentionrate", null);
        }

        if (!StringUtils.isEmpty(rs.getRetention_score())) {
            m1.put("rigrfRetentionrateS", rs.getRetention_score().toString());
        } else {
            m1.put("rigrfRetentionrateS", null);
        }

        Map<String, String> m2 = new HashMap<String, String>();
        m2.put("weight", "10");

        if (!StringUtils.isEmpty(rs.getLeveling())) {
            m2.put("rigrfCalibrationleveling", rs.getLeveling().toString());
        } else {
            m2.put("rigrfCalibrationleveling", null);
        }

        if (!StringUtils.isEmpty(rs.getLeveling_score())) {
            m2.put("rigrfCalibrationlevelingS", rs.getLeveling_score().toString());
        } else {
            m2.put("rigrfCalibrationlevelingS", null);
        }

        Map<String, String> m3 = new HashMap<String, String>();
        m3.put("weight", "30");

        if (!StringUtils.isEmpty(rs.getBothsides_stiffness())) {
            m3.put("rigrfSiRetentioners", rs.getBothsides_stiffness().toString());
        } else {
            m3.put("rigrfSiRetentioners", null);
        }

        if (!StringUtils.isEmpty(rs.getBothsides_stiffness_score())) {
            m3.put("rigrfSiRetentionersS", rs.getBothsides_stiffness_score().toString());
        } else {
            m3.put("rigrfSiRetentionersS", null);
        }

        Map<String, String> m4 = new HashMap<String, String>();
        m4.put("weight", "5");

        if (!StringUtils.isEmpty(rs.getBothposition_stiffness())) {
            m4.put("rigrfSiPositioners", rs.getBothposition_stiffness().toString());
        } else {
            m4.put("rigrfSiPositioners", null);
        }

        if (!StringUtils.isEmpty(rs.getBothposition_stiffness_score())) {
            m4.put("rigrfSiPositionersS", rs.getBothposition_stiffness_score().toString());
        } else {
            m4.put("rigrfSiPositionersS", null);
        }

        Map<String, String> m5 = new HashMap<String, String>();
        m5.put("weight", "10");

        if (!StringUtils.isEmpty(rs.getOsposition())) {
            m5.put("rigrfOsPositioners", rs.getOsposition().toString());
        } else {
            m5.put("rigrfOsPositioners", null);
        }

        if (!StringUtils.isEmpty(rs.getOsposition_score())) {
            m5.put("rigrfOsPositionersS", rs.getOsposition_score().toString());
        } else {
            m5.put("rigrfOsPositionersS", null);
        }

        Map<String, String> m6 = new HashMap<String, String>();
        m6.put("weight", "10");

        if (!StringUtils.isEmpty(rs.getDsposition())) {
            m6.put("rigrfDsPositioners", rs.getDsposition().toString());
        } else {
            m6.put("rigrfDsPositioners", null);
        }

        if (!StringUtils.isEmpty(rs.getDsposition_score())) {
            m6.put("rigrfDsPositionersS", rs.getDsposition_score().toString());
        } else {
            m6.put("rigrfDsPositionersS", null);
        }

        Map<String, String> m7 = new HashMap<String, String>();
        m7.put("weight", "5");

        if (!StringUtils.isEmpty(rs.getRolling_force())) {
            m7.put("rigrfSiForcesideers", rs.getRolling_force().toString());
        } else {
            m7.put("rigrfSiForcesideers", null);
        }

        if (!StringUtils.isEmpty(rs.getRolling_force_score())) {
            m7.put("rigrfSiForcesideersS", rs.getRolling_force_score().toString());
        } else {
            m7.put("rigrfSiForcesideersS", null);
        }

        Map<String, String> m8 = new HashMap<String, String>();
        m8.put("weight", "10");
        if (!StringUtils.isEmpty(rs.getOsrollgap())) {
            m8.put("rigrfOsEnexers", rs.getOsrollgap().toString());
        } else {
            m8.put("rigrfOsEnexers", null);
        }

        if (!StringUtils.isEmpty(rs.getOsrollgap_score())) {
            m8.put("rigrfOsEnexersS", rs.getOsrollgap_score().toString());
        } else {
            m8.put("rigrfOsEnexersS", null);
        }

        Map<String, String> m9 = new HashMap<String, String>();
        m9.put("weight", "10");

        if (!StringUtils.isEmpty(rs.getDsrollgap())) {
            m9.put("rigrfDsEnexers", rs.getDsrollgap().toString());
        } else {
            m9.put("rigrfDsEnexers", null);
        }
        if (!StringUtils.isEmpty(rs.getDsrollgap_score())) {
            m9.put("rigrfDsEnexersS", rs.getDsrollgap_score().toString());
        } else {
            m9.put("rigrfDsEnexersS", null);
        }

        ic.add(m1);
        ic.add(m2);
        ic.add(m3);
        ic.add(m4);
        ic.add(m5);
        ic.add(m6);
        ic.add(m7);
        ic.add(m8);
        ic.add(m9);
        body.setIndexCalRes(ic);
        request.put("body", body);

        return request.toJSONString();
    }

    public void score(RollStiffnessMath rollStiffnessMath, String msgid) {
        //评分并发送
        RollStiffness rollStiffness = new RollStiffness();
        BeanUtils.copyProperties(rollStiffnessMath, rollStiffness);
        rollStiffnessService.insertDataRollStiffnessByMath(rollStiffness);
        RollStiffness rts = rollStiffnessDao.findDataRollStiffnessByMath(rollStiffnessMath.getProduction_line_id(), rollStiffnessMath.getFrame_no(), rollStiffnessMath.getRecordtime());
        if (!StringUtils.isEmpty(rts.getRetention_score()) &&
                !StringUtils.isEmpty(rts.getLeveling_score()) &&
                !StringUtils.isEmpty(rts.getBothposition_stiffness_score()) &&
                !StringUtils.isEmpty(rts.getBothsides_stiffness_score()) &&
                !StringUtils.isEmpty(rts.getOsposition_score()) &&
                !StringUtils.isEmpty(rts.getDsposition_score()) &&
                !StringUtils.isEmpty(rts.getRolling_force_score()) &&
                !StringUtils.isEmpty(rts.getOsrollgap_score()) &&
                !StringUtils.isEmpty(rts.getDsrollgap_score())) {
            String result = jsonToString(rts, msgid); //调用把刚度对象数据构造成kafka需要的json格式字符串
            allKafka.DMPDPAsend(result);
            log.debug("GANGDUmessage is" + result);
        }
    }
}
