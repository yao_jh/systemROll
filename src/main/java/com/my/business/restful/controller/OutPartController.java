package com.my.business.restful.controller;


import com.alibaba.fastjson.JSON;
import com.my.business.modular.dictionary.dao.DictionaryDao;
import com.my.business.modular.monthreport.dao.MonthReportDao;
import com.my.business.modular.monthreport.entity.MonthReport;
import com.my.business.modular.rollgrinding.dao.RollGrindingBFDao;
import com.my.business.modular.rollgrinding.dao.RollGrindingDao;
import com.my.business.modular.rollgrinding.entity.RollGrinding;
import com.my.business.modular.rollgrinding.entity.RollGrindingBF;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.modular.rollonoffline.dao.RollOnoffLineDao;
import com.my.business.modular.rollstiffness.dao.RollStiffnessDao;
import com.my.business.modular.rollstiffness.entity.RollStiffness;
import com.my.business.modular.rollstiffnessmath.dao.RollStiffnessMathDao;
import com.my.business.modular.rollstiffnessmath.entity.RollStiffnessMath;
import com.my.business.restful.entity.*;
import com.my.business.sys.common.entity.ResultData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/***
 * 对外开放的数据接口
 * @author cc
 *
 */
@RestController
@RequestMapping("/OutPart")
@Api(value = "数字化磨辊间接口")
public class OutPartController {

    @Autowired
    private RollGrindingBFDao rollGrindingBFDao;
    @Autowired
    private RollOnoffLineDao rollOnoffLineDao;
    @Autowired
    private RollInformationDao rollInformationDao;
    @Autowired
    private DictionaryDao dictionaryDao;
    @Autowired
    private RollStiffnessDao rollStiffnessDao;
    @Autowired
    private RollStiffnessMathDao rollStiffnessMathDao;
    @Autowired
    private MonthReportDao monthReportDao;

    /**
     * 磨床磨削实绩
     */
    @CrossOrigin
    @RequestMapping(value = {"/grinder_performent"}, method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "磨床磨削实绩", notes = "通过辊号获取信息")
    public List<Grinder_performent> grinder_performent(@ApiParam(value = "({'roll_no':'辊号'})", required = true) @RequestBody String data) {
        try {
            Grinder_performent g = JSON.parseObject(data, Grinder_performent.class);
            return rollGrindingBFDao.findRest(g.getRoll_no());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 轧辊使用实绩
     */
    @CrossOrigin
    @RequestMapping(value = {"/roll_performent"}, method = RequestMethod.POST)
    @ResponseBody
    public List<Roll_performent> roll_performent(@ApiParam(value = "({\"out_stand_start\":\"下机时间开始时间\",\"out_stand_end\":\"下机时间结束时间\",\"frame_no\":\"机架号\"})", required = true) @RequestBody String data) {
        try {
            Roll_performent r = JSON.parseObject(data, Roll_performent.class);
            return rollOnoffLineDao.findRest(r.getOut_stand_start(), r.getOut_stand_end(), r.getFrame_no());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 轧辊清单
     */
    @CrossOrigin
    @RequestMapping(value = {"/rollmap"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData rollmap(@RequestBody String data) {
        RollInformation r = JSON.parseObject(data, RollInformation.class);
        List<JRoll_map> list = new ArrayList<>();
        JRoll_map jRoll_map = new JRoll_map();
        if (null != r.getRoll_no()) {
            String roll_no = r.getRoll_no();
            RollInformation rollInformation = rollInformationDao.findDataRollInformationByRollNo(roll_no);
            RollGrindingBF rollGrindingBF = rollGrindingBFDao.findDataRollGrindingBFByNoNew(roll_no);
            if (!StringUtils.isEmpty(rollInformation)) {
                jRoll_map.setRollID(rollInformation.getRoll_no());
                jRoll_map.setRollType(rollInformation.getRoll_type());
                if (!StringUtils.isEmpty(rollInformation.getCurrentdiameter())) {
                    jRoll_map.setDiam(Float.parseFloat(String.valueOf(rollInformation.getCurrentdiameter())));
                }
                if (!StringUtils.isEmpty(rollInformation.getRoundness())) {
                    jRoll_map.setRoundness(Float.parseFloat(String.valueOf(rollInformation.getRoundness())));
                }
                /*if (!StringUtils.isEmpty(rollGrindingBF)) {
                    jRoll_map.setShapDev(Float.parseFloat(String.valueOf(rollGrindingBF.getDeviation())));
                }*/
                if (!StringUtils.isEmpty(rollInformation.getUplinecount())) {
                    jRoll_map.setTotalUsedTimes(Integer.parseInt(String.valueOf(rollInformation.getUplinecount())));
                }

                String roll_state = dictionaryDao.findMapByForNameByV1("rollstate", rollInformation.getRoll_state());
                jRoll_map.setState(roll_state);
            }
        }
        list.add(jRoll_map);
        return ResultData.ResultDataSuccess(list);
    }
    
    
    /**
     * 设备轧辊轧辊清单数据
     */
    @CrossOrigin
    @RequestMapping(value = {"/rolllist"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData rolllist(@RequestBody String data) {
        RollInformation r = JSON.parseObject(data, RollInformation.class);
        String roll_state = null;
        String roll_revolve = null;
        String roll_special = null;
        
        if(!StringUtils.isEmpty(r.getRoll_state())) {
        	roll_state = String.valueOf(r.getRoll_state());
        }
        if(!StringUtils.isEmpty(r.getRoll_revolve())) {
        	roll_revolve = String.valueOf(r.getRoll_revolve());
        }
        if(!StringUtils.isEmpty(r.getRoll_special())) {
        	roll_special = String.valueOf(r.getRoll_special());
        }
        
        List<RollInformation> list = rollInformationDao.findDataByStateEquip(roll_state,roll_revolve,roll_special);
        return ResultData.ResultDataSuccess(list);
    }

    /**
     * 轧机刚度
     */
    @CrossOrigin
    @RequestMapping(value = {"/millstiff"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData millstiff(@RequestBody String data) {
        RollStiffness r = JSON.parseObject(data, RollStiffness.class);
        JMillStiff jMillStiff = new JMillStiff();
        List<JMillStiff> list = new ArrayList<>();
        if (null != r.getFrame_no()) {
            String frame_no = r.getFrame_no();
            RollStiffness rollStiffness = rollStiffnessDao.findDataRollStiffnessByframenoNew(frame_no);
            if (!StringUtils.isEmpty(rollStiffness)) {
                jMillStiff.setRecordtime(rollStiffness.getRecordtime());
                jMillStiff.setFrame_no(frame_no);
                jMillStiff.setRetention(Float.valueOf(String.valueOf(rollStiffness.getRetention())));
                jMillStiff.setRetention_score(Float.valueOf(String.valueOf(rollStiffness.getRetention_score())));
                jMillStiff.setLeveling(Float.valueOf(String.valueOf(rollStiffness.getLeveling())));
                jMillStiff.setLeveling_score(Float.valueOf(String.valueOf(rollStiffness.getLeveling_score())));
                jMillStiff.setBothsides_stiffness(Float.valueOf(String.valueOf(rollStiffness.getBothsides_stiffness())));
                jMillStiff.setBothsides_stiffness_score(Float.valueOf(String.valueOf(rollStiffness.getBothsides_stiffness_score())));
                jMillStiff.setBothposition_stiffness(Float.valueOf(String.valueOf(rollStiffness.getBothposition_stiffness())));
                jMillStiff.setBothposition_stiffness_score(Float.valueOf(String.valueOf(rollStiffness.getBothposition_stiffness_score())));
                jMillStiff.setOsposition(Float.valueOf(String.valueOf(rollStiffness.getOsposition())));
                jMillStiff.setOsposition_score(Float.valueOf(String.valueOf(rollStiffness.getOsposition_score())));
                jMillStiff.setDsposition(Float.valueOf(String.valueOf(rollStiffness.getDsposition())));
                jMillStiff.setDsposition_score(Float.valueOf(String.valueOf(rollStiffness.getDsposition_score())));
                jMillStiff.setRolling_force(Float.valueOf(String.valueOf(rollStiffness.getRolling_force())));
                jMillStiff.setRolling_force_score(Float.valueOf(String.valueOf(rollStiffness.getRolling_force_score())));
                jMillStiff.setOsrollgap(Float.valueOf(String.valueOf(rollStiffness.getOsrollgap())));
                jMillStiff.setOsrollgap_score(Float.valueOf(String.valueOf(rollStiffness.getOsrollgap_score())));
                jMillStiff.setDsrollgap(Float.valueOf(String.valueOf(rollStiffness.getDsrollgap())));
                jMillStiff.setDsrollgap_score(Float.valueOf(String.valueOf(rollStiffness.getDsrollgap_score())));
            }
        }
        list.add(jMillStiff);
        return ResultData.ResultDataSuccess(list);
    }

    /**
     * 轧机标定信息
     */
    @CrossOrigin
    @RequestMapping(value = {"/gapzero_info"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData gapzero_info(@RequestBody String data) {
        JGapzero_info jGapzero_info = JSON.parseObject(data, JGapzero_info.class);
        String frame_no = jGapzero_info.getFrame_no();
        JGapzero_info js = new JGapzero_info();
        List<JGapzero_info> list = new ArrayList<>();
        if (null != jGapzero_info.getFrame_no()) {
            RollStiffnessMath rollStiffnessMath = rollStiffnessMathDao.findDataNew(1L, frame_no);
            RollStiffness rollStiffness = rollStiffnessDao.findDataRollStiffnessByframenoNew(frame_no);
            if (!StringUtils.isEmpty(rollStiffnessMath)) {
                js.setRecordtime(rollStiffnessMath.getRecordtime());
                js.setFrame_no(frame_no);
                js.setOs_stiffness_in(rollStiffnessMath.getOs_stiffness_in());
                js.setOs_stiffness_out(rollStiffnessMath.getOs_stiffness_out());
                js.setDs_stiffness_in(rollStiffnessMath.getDs_stiffness_in());
                js.setDs_stiffness_out(rollStiffnessMath.getDs_stiffness_out());
                if (!StringUtils.isEmpty(rollStiffness)) {
                    if (!StringUtils.isEmpty(rollStiffness.getUpdownstep())) {
                        List<String> step = Arrays.asList(rollStiffness.getUpdownstep().split("/"));
                        String upstep = step.get(0);  //获取上阶梯垫位置
                        String downstep = step.get(1);  //获取下阶梯垫位置
                        js.setUpstep(upstep);
                        js.setDownstep(downstep);
                    } else {
                        js.setUpstep(null);
                        js.setDownstep(null);
                    }

                    if (!StringUtils.isEmpty(rollStiffness.getW_no())) {
                        List<String> roll_no = Arrays.asList(rollStiffness.getW_no().split("/"));
                        String w_no_up = roll_no.get(0);  //获取上辊辊号
                        String w_no_down = roll_no.get(1);  //获取下辊辊号
                        js.setW_no_up(w_no_up);
                        js.setW_no_down(w_no_down);
                    } else {
                        js.setW_no_up(null);
                        js.setW_no_down(null);
                    }

                    if (!StringUtils.isEmpty(rollStiffness.getB_no())) {
                        List<String> b_no = Arrays.asList(rollStiffness.getB_no().split("/"));
                        String b_no_up = b_no.get(0);  //获取上支撑辊辊号
                        String b_no_down = b_no.get(1);  //获取下支撑辊辊号
                        js.setB_no_up(b_no_up);
                        js.setB_no_down(b_no_down);
                    } else {
                        js.setB_no_up(null);
                        js.setB_no_down(null);
                    }

                    js.setLeveling(rollStiffness.getLeveling());
                }
                js.setOsaddstep(null);
                js.setDsaddstep(null);
            }
        }
        list.add(js);
        return ResultData.ResultDataSuccess(list);
    }

    /**
     * 月报
     */
    @CrossOrigin
    @RequestMapping(value = {"/monthreport"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData monthreport(@RequestBody String data) {
    	try {
    		MonthReport monthReport = JSON.parseObject(data, MonthReport.class);
        	if(!StringUtils.isEmpty(monthReport.getMonth())) {
        		MonthReport monthReport_new = monthReportDao.findDataMonthReportByMonth(monthReport.getMonth());
//        		Map<String,Object> monthReport_new = monthReportDao.findDataToMap(monthReport.getMonth());
        		if(monthReport_new != null) {
        			return ResultData.ResultDataSuccessSelf("请求成功",monthReport_new);
        		}else {
        			return ResultData.ResultDataFaultSelf("该时间下的结果没有，请重新查询", null); 
        		}
        	}else {
        		return ResultData.ResultDataFaultSelf("请按要求输入时间，时间格式是年-月(2019-12)这样", null); 
        	}
    	}catch(Exception e) {
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("请求失败,失败信息为" + e.getMessage(), null); 
    	}
    }

}
