package com.my.business.restful.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollconspredict.entity.RollConspredict;
import com.my.business.restful.GradingKafkaService;
import com.my.business.restful.entity.RealEntity;
import com.my.business.sys.common.entity.ResultData;

/**
* kafka测试控制器层
*/
@RestController
@RequestMapping("/kafka")
public class KafkaController {

	@Autowired
	private GradingKafkaService service;

	/**
	 * 发送预测数据
	 * @param userId 用户id
     * @param sname 用户姓名
	 * */
	@CrossOrigin
	@RequestMapping(value={"/getyc"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData getyc(@RequestBody String data){
		RollConspredict r = JSON.parseObject(data, RollConspredict.class);
//		service.sendForecast(r.getAct_coilid());
		return ResultData.ResultDataSuccessSelf("发送预测数据成功", null);
	};

	/**
	 * 发送实际数据
	 * @param userId 用户id
     * @param sname 用户姓名
	 * */
	@CrossOrigin
	@RequestMapping(value={"/getsj"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData getsj(@RequestBody String data){
		RealEntity r = JSON.parseObject(data, RealEntity.class);
		return ResultData.ResultDataSuccessSelf("发送实际数据成功", null);
	};

}
