package com.my.business.restful.entity;

import java.util.Date;

/***
 * 磨床磨削实绩对外接口
 * 
 * @author cc
 *
 */
public class Grinder_performent {

	private String machine_no; // 磨床号
	private String roll_no; // 辊号
	private String roll_type; // 轧辊类型
	private String ChockOS_ID; // 轴承箱OS号
	private String ChockDS_ID; // 轴承箱DS号
	private String grind_starttime; // 磨削开始时间
	private String grind_endtime; // 磨削结束时间
	private Double before_diameter; // 磨前直径
	private Double after_diameter; // 磨后直径
	private Double deviation; // 辊形偏差
	private Double roundness; // 圆度
	private Double qualifiednum; // 合格点数

	public String getMachine_no() {
		return machine_no;
	}

	public void setMachine_no(String machine_no) {
		this.machine_no = machine_no;
	}

	public String getRoll_no() {
		return roll_no;
	}

	public void setRoll_no(String roll_no) {
		this.roll_no = roll_no;
	}

	public String getRoll_type() {
		return roll_type;
	}

	public void setRoll_type(String roll_type) {
		this.roll_type = roll_type;
	}

	public String getChockOS_ID() {
		return ChockOS_ID;
	}

	public void setChockOS_ID(String chockOS_ID) {
		ChockOS_ID = chockOS_ID;
	}

	public String getChockDS_ID() {
		return ChockDS_ID;
	}

	public void setChockDS_ID(String chockDS_ID) {
		ChockDS_ID = chockDS_ID;
	}

	public String getGrind_starttime() {
		return grind_starttime;
	}

	public void setGrind_starttime(String grind_starttime) {
		this.grind_starttime = grind_starttime;
	}

	public String getGrind_endtime() {
		return grind_endtime;
	}

	public void setGrind_endtime(String grind_endtime) {
		this.grind_endtime = grind_endtime;
	}

	public Double getBefore_diameter() {
		return before_diameter;
	}

	public void setBefore_diameter(Double before_diameter) {
		this.before_diameter = before_diameter;
	}

	public Double getAfter_diameter() {
		return after_diameter;
	}

	public void setAfter_diameter(Double after_diameter) {
		this.after_diameter = after_diameter;
	}

	public Double getDeviation() {
		return deviation;
	}

	public void setDeviation(Double deviation) {
		this.deviation = deviation;
	}

	public Double getRoundness() {
		return roundness;
	}

	public void setRoundness(Double roundness) {
		this.roundness = roundness;
	}

	public Double getQualifiednum() {
		return qualifiednum;
	}

	public void setQualifiednum(Double qualifiednum) {
		this.qualifiednum = qualifiednum;
	}
}
