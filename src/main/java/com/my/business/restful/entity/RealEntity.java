package com.my.business.restful.entity;

import java.util.Date;

/***
 * 磨辊间发成本实际实体类
 * 
 * @author cc
 *
 */
public class RealEntity {

	private String coil_no; // 卷号
	private String w_no; // 工作辊号
	private String w_real_stock; // 工作辊实际预测磨削量
	private String w_roll_type;  //工作辊轧辊类型
	private String frame_no;  //机架号
	private String send_time;  //发送时间

	public String getCoil_no() {
		return coil_no;
	}

	public void setCoil_no(String coil_no) {
		this.coil_no = coil_no;
	}

	public String getW_no() {
		return w_no;
	}

	public void setW_no(String w_no) {
		this.w_no = w_no;
	}

	public String getW_real_stock() {
		return w_real_stock;
	}

	public void setW_real_stock(String w_real_stock) {
		this.w_real_stock = w_real_stock;
	}

	public String getW_roll_type() {
		return w_roll_type;
	}

	public void setW_roll_type(String w_roll_type) {
		this.w_roll_type = w_roll_type;
	}

	public String getFrame_no() {
		return frame_no;
	}

	public void setFrame_no(String frame_no) {
		this.frame_no = frame_no;
	}

	public String getSend_time() {
		return send_time;
	}

	public void setSend_time(String send_time) {
		this.send_time = send_time;
	}
	
}
