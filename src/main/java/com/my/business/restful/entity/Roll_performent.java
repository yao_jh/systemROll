package com.my.business.restful.entity;


/***
 * 轧辊生产实绩对外接口
 * 
 * @author cc
 *
 */
public class Roll_performent {

	private String roll_no; // 辊号
	private String frame_no; // 机架号
	private String loomingposition; // 上机位置
	private String onlinetime; // 上机时间
	private String offlinetime; // 下机时间
	private Double rollkilometer; // 轧制公里数
	private Double rolltonnage; // 轧制吨数
	private String off_line_reason; // 下线原因
	
	private String out_stand_start;  //开始时间
	private String out_stand_end; //结束时间

	public String getRoll_no() {
		return roll_no;
	}

	public void setRoll_no(String roll_no) {
		this.roll_no = roll_no;
	}

	public String getFrame_no() {
		return frame_no;
	}

	public void setFrame_no(String frame_no) {
		this.frame_no = frame_no;
	}

	public String getLoomingposition() {
		return loomingposition;
	}

	public void setLoomingposition(String loomingposition) {
		this.loomingposition = loomingposition;
	}

	public String getOnlinetime() {
		return onlinetime;
	}

	public void setOnlinetime(String onlinetime) {
		this.onlinetime = onlinetime;
	}

	public String getOfflinetime() {
		return offlinetime;
	}

	public void setOfflinetime(String offlinetime) {
		this.offlinetime = offlinetime;
	}

	public Double getRollkilometer() {
		return rollkilometer;
	}

	public void setRollkilometer(Double rollkilometer) {
		this.rollkilometer = rollkilometer;
	}

	public Double getRolltonnage() {
		return rolltonnage;
	}

	public void setRolltonnage(Double rolltonnage) {
		this.rolltonnage = rolltonnage;
	}

	public String getOff_line_reason() {
		return off_line_reason;
	}

	public void setOff_line_reason(String off_line_reason) {
		this.off_line_reason = off_line_reason;
	}

	public String getOut_stand_start() {
		return out_stand_start;
	}

	public void setOut_stand_start(String out_stand_start) {
		this.out_stand_start = out_stand_start;
	}

	public String getOut_stand_end() {
		return out_stand_end;
	}

	public void setOut_stand_end(String out_stand_end) {
		this.out_stand_end = out_stand_end;
	}

}
