package com.my.business.restful.entity;

import java.util.Date;
import com.my.business.sys.common.entity.BaseEntity;

/**
* 轧辊磨削历史实体类
* @author  生成器生成
* @date 2020-08-19 15:19:15
*/
public class History_roll_ground extends BaseEntity{

	private Long grinder_no;  //磨床号
	private Date start_date_time;  //起始时间

	private String rolling_mill;  //机架
	private String roll_type;  //轧辊类型
	private String roll_material;  //材质
	private String roll_nb;  //辊号
	private String roll_code;  //轧辊编码
	private String program_code;  //程序编码
	private String profile_code;  //辊形编码
	private float profile_height; //辊形高度
	private String dress_code;  //砂轮整修代码
	private String dress_height;  //砂轮整修高度
	private float diam_before_head; //磨前头部直径
	private float diam_before_mid; //磨前中部直径
	private float diam_before_tail; //磨前尾部直径
	private Date end_date_time;  //结束日期

	private String regrind_status;  //
	private float present_diam_max; //最大直径
	private float present_diam_min; //最小直径
	private float present_diam_head; //头部端直径
	private float present_diam_mid; //中间直径
	private float present_diam_tail; //尾架端直径
	private float taper; //锥度误差
	private float profile; //辊型误差
	private float roundness; //圆度误差
	private float runout; //偏心误差

	private Long crack;  //最大裂痕
	private float c_position; //最大裂痕位置
	private Long c_angle;  //最大裂痕角度
	private Long bruise;  //最大伤痕
	private float b_position; //最大伤痕位置
	private Long b_angle;  //最大伤痕角度
	private Long maxstructure;  //
	private Long minstructure;  //
	private float ultrasound; //
	private float u_position;
	private float u_angle;
	private float wheel_dia_start; //砂轮开始直径
	private float wheel_dia_end; //砂轮结束直径
	private float roughness_head;
	private float roughness_mid;
	private float roughness_tail;
	private float roughnessaverage;
	private float roughnessdeviation;
	private float hardness_head;
	private float hardness_mid;
	private float hardness_tail;
	private String remarks;  //
	private String validate_discard;  //
	private String operator_code;  //
	private String wheel_type;  //砂轮种类
	private String wheel_no;  //砂轮号
	private float last_use_mid_dia;
	private Long inttime;  //
	private float weightremoved; //切割重量
	private Date lastgrind;  //
	private float diamsemifin;
	private float u_depth;
	private float u_tablelength;

	private Long sent_to_mill;  //
	private float measured_wear;
	private String chock_os;  //
	private String chock_ds;  //
	private String position;  //
	private String mate_roll_nb;  //
	private String max_crown;  //
	private String min_crown;  //
	private Date last_rolled_dt;  //

	public Long getGrinder_no() {
		return grinder_no;
	}

	public void setGrinder_no(Long grinder_no) {
		this.grinder_no = grinder_no;
	}

	public Date getStart_date_time() {
		return start_date_time;
	}

	public void setStart_date_time(Date start_date_time) {
		this.start_date_time = start_date_time;
	}

	public String getRolling_mill() {
		return rolling_mill;
	}

	public void setRolling_mill(String rolling_mill) {
		this.rolling_mill = rolling_mill;
	}

	public String getRoll_type() {
		return roll_type;
	}

	public void setRoll_type(String roll_type) {
		this.roll_type = roll_type;
	}

	public String getRoll_material() {
		return roll_material;
	}

	public void setRoll_material(String roll_material) {
		this.roll_material = roll_material;
	}

	public String getRoll_nb() {
		return roll_nb;
	}

	public void setRoll_nb(String roll_nb) {
		this.roll_nb = roll_nb;
	}

	public String getRoll_code() {
		return roll_code;
	}

	public void setRoll_code(String roll_code) {
		this.roll_code = roll_code;
	}

	public String getProgram_code() {
		return program_code;
	}

	public void setProgram_code(String program_code) {
		this.program_code = program_code;
	}

	public String getProfile_code() {
		return profile_code;
	}

	public void setProfile_code(String profile_code) {
		this.profile_code = profile_code;
	}

	public float getProfile_height() {
		return profile_height;
	}

	public void setProfile_height(float profile_height) {
		this.profile_height = profile_height;
	}

	public String getDress_code() {
		return dress_code;
	}

	public void setDress_code(String dress_code) {
		this.dress_code = dress_code;
	}

	public String getDress_height() {
		return dress_height;
	}

	public void setDress_height(String dress_height) {
		this.dress_height = dress_height;
	}

	public float getDiam_before_head() {
		return diam_before_head;
	}

	public void setDiam_before_head(float diam_before_head) {
		this.diam_before_head = diam_before_head;
	}

	public float getDiam_before_mid() {
		return diam_before_mid;
	}

	public void setDiam_before_mid(float diam_before_mid) {
		this.diam_before_mid = diam_before_mid;
	}

	public float getDiam_before_tail() {
		return diam_before_tail;
	}

	public void setDiam_before_tail(float diam_before_tail) {
		this.diam_before_tail = diam_before_tail;
	}

	public Date getEnd_date_time() {
		return end_date_time;
	}

	public void setEnd_date_time(Date end_date_time) {
		this.end_date_time = end_date_time;
	}

	public String getRegrind_status() {
		return regrind_status;
	}

	public void setRegrind_status(String regrind_status) {
		this.regrind_status = regrind_status;
	}

	public float getPresent_diam_max() {
		return present_diam_max;
	}

	public void setPresent_diam_max(float present_diam_max) {
		this.present_diam_max = present_diam_max;
	}

	public float getPresent_diam_min() {
		return present_diam_min;
	}

	public void setPresent_diam_min(float present_diam_min) {
		this.present_diam_min = present_diam_min;
	}

	public float getPresent_diam_head() {
		return present_diam_head;
	}

	public void setPresent_diam_head(float present_diam_head) {
		this.present_diam_head = present_diam_head;
	}

	public float getPresent_diam_mid() {
		return present_diam_mid;
	}

	public void setPresent_diam_mid(float present_diam_mid) {
		this.present_diam_mid = present_diam_mid;
	}

	public float getPresent_diam_tail() {
		return present_diam_tail;
	}

	public void setPresent_diam_tail(float present_diam_tail) {
		this.present_diam_tail = present_diam_tail;
	}

	public float getTaper() {
		return taper;
	}

	public void setTaper(float taper) {
		this.taper = taper;
	}

	public float getProfile() {
		return profile;
	}

	public void setProfile(float profile) {
		this.profile = profile;
	}

	public float getRoundness() {
		return roundness;
	}

	public void setRoundness(float roundness) {
		this.roundness = roundness;
	}

	public float getRunout() {
		return runout;
	}

	public void setRunout(float runout) {
		this.runout = runout;
	}

	public Long getCrack() {
		return crack;
	}

	public void setCrack(Long crack) {
		this.crack = crack;
	}

	public float getC_position() {
		return c_position;
	}

	public void setC_position(float c_position) {
		this.c_position = c_position;
	}

	public Long getC_angle() {
		return c_angle;
	}

	public void setC_angle(Long c_angle) {
		this.c_angle = c_angle;
	}

	public Long getBruise() {
		return bruise;
	}

	public void setBruise(Long bruise) {
		this.bruise = bruise;
	}

	public float getB_position() {
		return b_position;
	}

	public void setB_position(float b_position) {
		this.b_position = b_position;
	}

	public Long getB_angle() {
		return b_angle;
	}

	public void setB_angle(Long b_angle) {
		this.b_angle = b_angle;
	}

	public Long getMaxstructure() {
		return maxstructure;
	}

	public void setMaxstructure(Long maxstructure) {
		this.maxstructure = maxstructure;
	}

	public Long getMinstructure() {
		return minstructure;
	}

	public void setMinstructure(Long minstructure) {
		this.minstructure = minstructure;
	}

	public float getUltrasound() {
		return ultrasound;
	}

	public void setUltrasound(float ultrasound) {
		this.ultrasound = ultrasound;
	}

	public float getU_position() {
		return u_position;
	}

	public void setU_position(float u_position) {
		this.u_position = u_position;
	}

	public float getU_angle() {
		return u_angle;
	}

	public void setU_angle(float u_angle) {
		this.u_angle = u_angle;
	}

	public float getWheel_dia_start() {
		return wheel_dia_start;
	}

	public void setWheel_dia_start(float wheel_dia_start) {
		this.wheel_dia_start = wheel_dia_start;
	}

	public float getWheel_dia_end() {
		return wheel_dia_end;
	}

	public void setWheel_dia_end(float wheel_dia_end) {
		this.wheel_dia_end = wheel_dia_end;
	}

	public float getRoughness_head() {
		return roughness_head;
	}

	public void setRoughness_head(float roughness_head) {
		this.roughness_head = roughness_head;
	}

	public float getRoughness_mid() {
		return roughness_mid;
	}

	public void setRoughness_mid(float roughness_mid) {
		this.roughness_mid = roughness_mid;
	}

	public float getRoughness_tail() {
		return roughness_tail;
	}

	public void setRoughness_tail(float roughness_tail) {
		this.roughness_tail = roughness_tail;
	}

	public float getRoughnessaverage() {
		return roughnessaverage;
	}

	public void setRoughnessaverage(float roughnessaverage) {
		this.roughnessaverage = roughnessaverage;
	}

	public float getRoughnessdeviation() {
		return roughnessdeviation;
	}

	public void setRoughnessdeviation(float roughnessdeviation) {
		this.roughnessdeviation = roughnessdeviation;
	}

	public float getHardness_head() {
		return hardness_head;
	}

	public void setHardness_head(float hardness_head) {
		this.hardness_head = hardness_head;
	}

	public float getHardness_mid() {
		return hardness_mid;
	}

	public void setHardness_mid(float hardness_mid) {
		this.hardness_mid = hardness_mid;
	}

	public float getHardness_tail() {
		return hardness_tail;
	}

	public void setHardness_tail(float hardness_tail) {
		this.hardness_tail = hardness_tail;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getValidate_discard() {
		return validate_discard;
	}

	public void setValidate_discard(String validate_discard) {
		this.validate_discard = validate_discard;
	}

	public String getOperator_code() {
		return operator_code;
	}

	public void setOperator_code(String operator_code) {
		this.operator_code = operator_code;
	}

	public String getWheel_type() {
		return wheel_type;
	}

	public void setWheel_type(String wheel_type) {
		this.wheel_type = wheel_type;
	}

	public String getWheel_no() {
		return wheel_no;
	}

	public void setWheel_no(String wheel_no) {
		this.wheel_no = wheel_no;
	}

	public float getLast_use_mid_dia() {
		return last_use_mid_dia;
	}

	public void setLast_use_mid_dia(float last_use_mid_dia) {
		this.last_use_mid_dia = last_use_mid_dia;
	}

	public Long getInttime() {
		return inttime;
	}

	public void setInttime(Long inttime) {
		this.inttime = inttime;
	}

	public float getWeightremoved() {
		return weightremoved;
	}

	public void setWeightremoved(float weightremoved) {
		this.weightremoved = weightremoved;
	}

	public Date getLastgrind() {
		return lastgrind;
	}

	public void setLastgrind(Date lastgrind) {
		this.lastgrind = lastgrind;
	}

	public float getDiamsemifin() {
		return diamsemifin;
	}

	public void setDiamsemifin(float diamsemifin) {
		this.diamsemifin = diamsemifin;
	}

	public float getU_depth() {
		return u_depth;
	}

	public void setU_depth(float u_depth) {
		this.u_depth = u_depth;
	}

	public float getU_tablelength() {
		return u_tablelength;
	}

	public void setU_tablelength(float u_tablelength) {
		this.u_tablelength = u_tablelength;
	}

	public Long getSent_to_mill() {
		return sent_to_mill;
	}

	public void setSent_to_mill(Long sent_to_mill) {
		this.sent_to_mill = sent_to_mill;
	}

	public float getMeasured_wear() {
		return measured_wear;
	}

	public void setMeasured_wear(float measured_wear) {
		this.measured_wear = measured_wear;
	}

	public String getChock_os() {
		return chock_os;
	}

	public void setChock_os(String chock_os) {
		this.chock_os = chock_os;
	}

	public String getChock_ds() {
		return chock_ds;
	}

	public void setChock_ds(String chock_ds) {
		this.chock_ds = chock_ds;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getMate_roll_nb() {
		return mate_roll_nb;
	}

	public void setMate_roll_nb(String mate_roll_nb) {
		this.mate_roll_nb = mate_roll_nb;
	}

	public String getMax_crown() {
		return max_crown;
	}

	public void setMax_crown(String max_crown) {
		this.max_crown = max_crown;
	}

	public String getMin_crown() {
		return min_crown;
	}

	public void setMin_crown(String min_crown) {
		this.min_crown = min_crown;
	}

	public Date getLast_rolled_dt() {
		return last_rolled_dt;
	}

	public void setLast_rolled_dt(Date last_rolled_dt) {
		this.last_rolled_dt = last_rolled_dt;
	}
}