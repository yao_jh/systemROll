package com.my.business.restful.entity;

/**
 * 轧辊多变异分析信号表json的实体类
 *
 * @author 生成器生成
 * @date 2020-09-10 20:20:19
 */
public class JTB_STIFFNESS_DATA {

    private String sID;                                   //记录id
    private String sTART_TIME;                          //开始时间
    private String eND_TIME;                            //结束时间
    private String dS_DATA_POINTS;                      //DS侧曲线数据点
    private String oS_DATA_POINTS;                      //OS侧曲线数据点
    private String dS_POINTS_AVE;                        //DS侧均值点
    private String oS_POINTS_AVE;                        //OS侧均值点
    private String dEVICE_FRAME_NAME;                   //设备机架名称
    private String zERO_FLAG_START;                     //0调开始
    private String zERO_FLAG_COMPLETE;                  //0调结束
    private String eNT_POSITION_OFFSET;                  //入口位置偏移
    private String dS_ENT_EXIT_POSITION_OFFSET;          //DS侧入口出口偏移
    private String oS_ENT_EXIT_POSITION_OFFSET;          //OS侧入口出口偏移
    private String fORCE_PT_OFFSET;
    private String sCORE_FORCE_KEEPING_PRESENT;          //刚度保持率得分
    private String sCORE_FORCE_OFFSET;                   //刚度偏差得分
    private String sCORE_ENT_POSITION_OFFSET;            //入口位置偏差得分
    private String sCORE_DS_ENT_EXIT_OFFSET;             //DS侧出入口调平偏差得分
    private String sCORE_OS_ENT_EXIT_OFFSET;             //OS侧出入口调平偏差得分
    private String sCORE_FORCE_PT_OFFSET;                //备用传感器轧制力偏差得分
    private String dEVICE_NO;                           //设备号
    private String fRAME_NAME;                          //机架号
    private String gAP_LEVELING_MANUAL_AVE;
    private String sCORE_GAP_LEVELING_MANUAL;            //辊缝调平得分
    private String dS_FX_ON_ENT_EXIT_OFFSET;             //DS咬钢出入口偏差
    private String oS_FX_ON_ENT_EXIT_OFFSET;             //OS咬钢出入口偏差
    private String sCORE_DS_FX_ON_ENT_EXIT_OFFSET;       //DS咬钢出入口偏差得分
    private String sCORE_OS_FX_ON_ENT_EXIT_OFFSET;       //OS咬钢出入口偏差得分
    private String sCORE_TOTAL;                          //总分

    public String getsID() {
        return sID;
    }

    public void setsID(String sID) {
        this.sID = sID;
    }

    public String getsTART_TIME() {
        return sTART_TIME;
    }

    public void setsTART_TIME(String sTART_TIME) {
        this.sTART_TIME = sTART_TIME;
    }

    public String geteND_TIME() {
        return eND_TIME;
    }

    public void seteND_TIME(String eND_TIME) {
        this.eND_TIME = eND_TIME;
    }

    public String getdS_DATA_POINTS() {
        return dS_DATA_POINTS;
    }

    public void setdS_DATA_POINTS(String dS_DATA_POINTS) {
        this.dS_DATA_POINTS = dS_DATA_POINTS;
    }

    public String getoS_DATA_POINTS() {
        return oS_DATA_POINTS;
    }

    public void setoS_DATA_POINTS(String oS_DATA_POINTS) {
        this.oS_DATA_POINTS = oS_DATA_POINTS;
    }

    public String getdS_POINTS_AVE() {
        return dS_POINTS_AVE;
    }

    public void setdS_POINTS_AVE(String dS_POINTS_AVE) {
        this.dS_POINTS_AVE = dS_POINTS_AVE;
    }

    public String getoS_POINTS_AVE() {
        return oS_POINTS_AVE;
    }

    public void setoS_POINTS_AVE(String oS_POINTS_AVE) {
        this.oS_POINTS_AVE = oS_POINTS_AVE;
    }

    public String getdEVICE_FRAME_NAME() {
        return dEVICE_FRAME_NAME;
    }

    public void setdEVICE_FRAME_NAME(String dEVICE_FRAME_NAME) {
        this.dEVICE_FRAME_NAME = dEVICE_FRAME_NAME;
    }

    public String getzERO_FLAG_START() {
        return zERO_FLAG_START;
    }

    public void setzERO_FLAG_START(String zERO_FLAG_START) {
        this.zERO_FLAG_START = zERO_FLAG_START;
    }

    public String getzERO_FLAG_COMPLETE() {
        return zERO_FLAG_COMPLETE;
    }

    public void setzERO_FLAG_COMPLETE(String zERO_FLAG_COMPLETE) {
        this.zERO_FLAG_COMPLETE = zERO_FLAG_COMPLETE;
    }

    public String geteNT_POSITION_OFFSET() {
        return eNT_POSITION_OFFSET;
    }

    public void seteNT_POSITION_OFFSET(String eNT_POSITION_OFFSET) {
        this.eNT_POSITION_OFFSET = eNT_POSITION_OFFSET;
    }

    public String getdS_ENT_EXIT_POSITION_OFFSET() {
        return dS_ENT_EXIT_POSITION_OFFSET;
    }

    public void setdS_ENT_EXIT_POSITION_OFFSET(String dS_ENT_EXIT_POSITION_OFFSET) {
        this.dS_ENT_EXIT_POSITION_OFFSET = dS_ENT_EXIT_POSITION_OFFSET;
    }

    public String getoS_ENT_EXIT_POSITION_OFFSET() {
        return oS_ENT_EXIT_POSITION_OFFSET;
    }

    public void setoS_ENT_EXIT_POSITION_OFFSET(String oS_ENT_EXIT_POSITION_OFFSET) {
        this.oS_ENT_EXIT_POSITION_OFFSET = oS_ENT_EXIT_POSITION_OFFSET;
    }

    public String getfORCE_PT_OFFSET() {
        return fORCE_PT_OFFSET;
    }

    public void setfORCE_PT_OFFSET(String fORCE_PT_OFFSET) {
        this.fORCE_PT_OFFSET = fORCE_PT_OFFSET;
    }

    public String getsCORE_FORCE_KEEPING_PRESENT() {
        return sCORE_FORCE_KEEPING_PRESENT;
    }

    public void setsCORE_FORCE_KEEPING_PRESENT(String sCORE_FORCE_KEEPING_PRESENT) {
        this.sCORE_FORCE_KEEPING_PRESENT = sCORE_FORCE_KEEPING_PRESENT;
    }

    public String getsCORE_FORCE_OFFSET() {
        return sCORE_FORCE_OFFSET;
    }

    public void setsCORE_FORCE_OFFSET(String sCORE_FORCE_OFFSET) {
        this.sCORE_FORCE_OFFSET = sCORE_FORCE_OFFSET;
    }

    public String getsCORE_ENT_POSITION_OFFSET() {
        return sCORE_ENT_POSITION_OFFSET;
    }

    public void setsCORE_ENT_POSITION_OFFSET(String sCORE_ENT_POSITION_OFFSET) {
        this.sCORE_ENT_POSITION_OFFSET = sCORE_ENT_POSITION_OFFSET;
    }

    public String getsCORE_DS_ENT_EXIT_OFFSET() {
        return sCORE_DS_ENT_EXIT_OFFSET;
    }

    public void setsCORE_DS_ENT_EXIT_OFFSET(String sCORE_DS_ENT_EXIT_OFFSET) {
        this.sCORE_DS_ENT_EXIT_OFFSET = sCORE_DS_ENT_EXIT_OFFSET;
    }

    public String getsCORE_OS_ENT_EXIT_OFFSET() {
        return sCORE_OS_ENT_EXIT_OFFSET;
    }

    public void setsCORE_OS_ENT_EXIT_OFFSET(String sCORE_OS_ENT_EXIT_OFFSET) {
        this.sCORE_OS_ENT_EXIT_OFFSET = sCORE_OS_ENT_EXIT_OFFSET;
    }

    public String getsCORE_FORCE_PT_OFFSET() {
        return sCORE_FORCE_PT_OFFSET;
    }

    public void setsCORE_FORCE_PT_OFFSET(String sCORE_FORCE_PT_OFFSET) {
        this.sCORE_FORCE_PT_OFFSET = sCORE_FORCE_PT_OFFSET;
    }

    public String getdEVICE_NO() {
        return dEVICE_NO;
    }

    public void setdEVICE_NO(String dEVICE_NO) {
        this.dEVICE_NO = dEVICE_NO;
    }

    public String getfRAME_NAME() {
        return fRAME_NAME;
    }

    public void setfRAME_NAME(String fRAME_NAME) {
        this.fRAME_NAME = fRAME_NAME;
    }

    public String getgAP_LEVELING_MANUAL_AVE() {
        return gAP_LEVELING_MANUAL_AVE;
    }

    public void setgAP_LEVELING_MANUAL_AVE(String gAP_LEVELING_MANUAL_AVE) {
        this.gAP_LEVELING_MANUAL_AVE = gAP_LEVELING_MANUAL_AVE;
    }

    public String getsCORE_GAP_LEVELING_MANUAL() {
        return sCORE_GAP_LEVELING_MANUAL;
    }

    public void setsCORE_GAP_LEVELING_MANUAL(String sCORE_GAP_LEVELING_MANUAL) {
        this.sCORE_GAP_LEVELING_MANUAL = sCORE_GAP_LEVELING_MANUAL;
    }

    public String getdS_FX_ON_ENT_EXIT_OFFSET() {
        return dS_FX_ON_ENT_EXIT_OFFSET;
    }

    public void setdS_FX_ON_ENT_EXIT_OFFSET(String dS_FX_ON_ENT_EXIT_OFFSET) {
        this.dS_FX_ON_ENT_EXIT_OFFSET = dS_FX_ON_ENT_EXIT_OFFSET;
    }

    public String getoS_FX_ON_ENT_EXIT_OFFSET() {
        return oS_FX_ON_ENT_EXIT_OFFSET;
    }

    public void setoS_FX_ON_ENT_EXIT_OFFSET(String oS_FX_ON_ENT_EXIT_OFFSET) {
        this.oS_FX_ON_ENT_EXIT_OFFSET = oS_FX_ON_ENT_EXIT_OFFSET;
    }

    public String getsCORE_DS_FX_ON_ENT_EXIT_OFFSET() {
        return sCORE_DS_FX_ON_ENT_EXIT_OFFSET;
    }

    public void setsCORE_DS_FX_ON_ENT_EXIT_OFFSET(String sCORE_DS_FX_ON_ENT_EXIT_OFFSET) {
        this.sCORE_DS_FX_ON_ENT_EXIT_OFFSET = sCORE_DS_FX_ON_ENT_EXIT_OFFSET;
    }

    public String getsCORE_OS_FX_ON_ENT_EXIT_OFFSET() {
        return sCORE_OS_FX_ON_ENT_EXIT_OFFSET;
    }

    public void setsCORE_OS_FX_ON_ENT_EXIT_OFFSET(String sCORE_OS_FX_ON_ENT_EXIT_OFFSET) {
        this.sCORE_OS_FX_ON_ENT_EXIT_OFFSET = sCORE_OS_FX_ON_ENT_EXIT_OFFSET;
    }

    public String getsCORE_TOTAL() {
        return sCORE_TOTAL;
    }

    public void setsCORE_TOTAL(String sCORE_TOTAL) {
        this.sCORE_TOTAL = sCORE_TOTAL;
    }
}