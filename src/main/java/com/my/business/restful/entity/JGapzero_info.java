package com.my.business.restful.entity;

public class JGapzero_info {
    private String recordtime;  //标定时间
    private String frame_no;  //机架号
    private Double os_stiffness_in;  //OS入口刚度
    private Double os_stiffness_out;  //OS出口刚度
    private Double ds_stiffness_in;  //DS入口刚度
    private Double ds_stiffness_out;  //DS出口刚度
    private String upstep;  //上阶梯垫位置
    private String downstep;  //下阶梯垫位置
    private String b_no_up;  //上支撑辊辊号
    private String b_no_down;  //下支撑辊辊号
    private String w_no_up;  //上辊号
    private String w_no_down;  //下辊号
    private Double leveling;  //调平值(mm)
    private String osaddstep;  //加垫记录OS
    private String dsaddstep;  //加垫记录DS

    public String getRecordtime() {
        return recordtime;
    }

    public void setRecordtime(String recordtime) {
        this.recordtime = recordtime;
    }

    public String getFrame_no() {
        return frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public Double getOs_stiffness_in() {
        return os_stiffness_in;
    }

    public void setOs_stiffness_in(Double os_stiffness_in) {
        this.os_stiffness_in = os_stiffness_in;
    }

    public Double getOs_stiffness_out() {
        return os_stiffness_out;
    }

    public void setOs_stiffness_out(Double os_stiffness_out) {
        this.os_stiffness_out = os_stiffness_out;
    }

    public Double getDs_stiffness_in() {
        return ds_stiffness_in;
    }

    public void setDs_stiffness_in(Double ds_stiffness_in) {
        this.ds_stiffness_in = ds_stiffness_in;
    }

    public Double getDs_stiffness_out() {
        return ds_stiffness_out;
    }

    public void setDs_stiffness_out(Double ds_stiffness_out) {
        this.ds_stiffness_out = ds_stiffness_out;
    }

    public String getUpstep() {
        return upstep;
    }

    public void setUpstep(String upstep) {
        this.upstep = upstep;
    }

    public String getDownstep() {
        return downstep;
    }

    public void setDownstep(String downstep) {
        this.downstep = downstep;
    }

    public String getB_no_up() {
        return b_no_up;
    }

    public void setB_no_up(String b_no_up) {
        this.b_no_up = b_no_up;
    }

    public String getB_no_down() {
        return b_no_down;
    }

    public void setB_no_down(String b_no_down) {
        this.b_no_down = b_no_down;
    }

    public String getW_no_up() {
        return w_no_up;
    }

    public void setW_no_up(String w_no_up) {
        this.w_no_up = w_no_up;
    }

    public String getW_no_down() {
        return w_no_down;
    }

    public void setW_no_down(String w_no_down) {
        this.w_no_down = w_no_down;
    }

    public Double getLeveling() {
        return leveling;
    }

    public void setLeveling(Double leveling) {
        this.leveling = leveling;
    }

    public String getOsaddstep() {
        return osaddstep;
    }

    public void setOsaddstep(String osaddstep) {
        this.osaddstep = osaddstep;
    }

    public String getDsaddstep() {
        return dsaddstep;
    }

    public void setDsaddstep(String dsaddstep) {
        this.dsaddstep = dsaddstep;
    }
}
