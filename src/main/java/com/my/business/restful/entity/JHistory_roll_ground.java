package com.my.business.restful.entity;

import com.my.business.sys.common.entity.JCommon;

import java.util.Date;

/**
* 轧辊磨削历史json的实体类
* @author  生成器生成
* @date 2020-08-19 15:19:16
*/
public class
JHistory_roll_ground extends JCommon{

	private Integer pageIndex; // 第几页
	private Integer pageSize; // 每页多少数据
	private Object condition; // 查询条件
	private History_roll_ground history_roll_ground;   //对应模块的实体类
	private Long grinder_no;
	private String str_indocno;
	private Date start_date_time;
	
	public Integer getPageIndex() {
		return pageIndex;
	}
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Object getCondition() {
		return condition;
	}
	public void setCondition(Object condition) {
		this.condition = condition;
	}

	public History_roll_ground getHistory_roll_ground() {
		return history_roll_ground;
	}

	public void setHistory_roll_ground(History_roll_ground history_roll_ground) {
		this.history_roll_ground = history_roll_ground;
	}

	public Long getGrinder_no() {
		return grinder_no;
	}

	public void setGrinder_no(Long grinder_no) {
		this.grinder_no = grinder_no;
	}

	public String getStr_indocno() {
		return str_indocno;
	}
	public void setStr_indocno(String str_indocno) {
		this.str_indocno = str_indocno;
	}

	public Date getStart_date_time() {
		return start_date_time;
	}

	public void setStart_date_time(Date start_date_time) {
		this.start_date_time = start_date_time;
	}
}