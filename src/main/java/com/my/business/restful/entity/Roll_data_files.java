package com.my.business.restful.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.util.Date;

/**
 * 轧辊数据文件实体类
 *
 * @author 生成器生成
 * @date 2020-08-19 15:22:52
 */
public class Roll_data_files extends BaseEntity {

    private Long grinder;  //磨床号
    private String rolling_mill;  //机架
    private String roll_type;  //轧辊类型
    private String roll_material;  //材质
    private String roll_nb;  //辊号
    private Object initial_report;  //初始报告
    private Date initial_date_time;  //开始时间

    private Object initial_diameter;  //磨前辊径
    private Date initial_diam_date_time;  //

    private Object first_theopro;  //磨前标准曲线
    private Date first_theopro_date_time;  //

    private Object first_realpro;  //磨前辊形曲线
    private Date first_realpro_date_time;  //

    private Object next_diameter;  //
    private Date next_diam_date_time;  //

    private Object crack;  //
    private Date crack_date_time;  //

    private Object bruise;  //
    private Date bruise_date_time;  //

    private Object final_theopro;  //磨后标准曲线
    private Date final_theopro_date_time;  //

    private Object final_realpro;  //磨后辊形曲线
    private Date final_realpro_date_time;  //

    private Object roundness;  //圆度曲线
    private Date roundness_date_time;  //

    private Object endcrack;  //
    private Date endcrack_date_time;  //

    private Object endbruise;  //
    private Date endbruise_date_time;  //

    private Object inspekt;  //探伤图
    private Date inspekt_dt;  //

    private Object endinspekt;  //结束探伤图
    private Date endinspekt_dt;  //

    private Object structure;  //
    private Date structure_date_time;  //

    private Object ultrasound;  //超声波探伤图
    private Date ultrasound_dt;  //

    private Object ultrasoundend;  //
    private Date ultrasoundend_dt;  //

    private Object roughness;  //粗糙度
    private Date roughness_dt;  //

    private Object final_report;  //最终报告
    private Date final_report_date_time;  //

    private String validate_discard;  //
    private Object history_grind;  //
    private Object history_pprograms;  //
    private Object hardness;  //硬度
    private Date hardness_dt;  //

    public Long getGrinder() {
        return grinder;
    }

    public void setGrinder(Long grinder) {
        this.grinder = grinder;
    }

    public String getRolling_mill() {
        return rolling_mill;
    }

    public void setRolling_mill(String rolling_mill) {
        this.rolling_mill = rolling_mill;
    }

    public String getRoll_type() {
        return roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public String getRoll_material() {
        return roll_material;
    }

    public void setRoll_material(String roll_material) {
        this.roll_material = roll_material;
    }

    public String getRoll_nb() {
        return roll_nb;
    }

    public void setRoll_nb(String roll_nb) {
        this.roll_nb = roll_nb;
    }

    public Object getInitial_report() {
        return initial_report;
    }

    public void setInitial_report(Object initial_report) {
        this.initial_report = initial_report;
    }

    public Date getInitial_date_time() {
        return initial_date_time;
    }

    public void setInitial_date_time(Date initial_date_time) {
        this.initial_date_time = initial_date_time;
    }

    public Object getInitial_diameter() {
        return initial_diameter;
    }

    public void setInitial_diameter(Object initial_diameter) {
        this.initial_diameter = initial_diameter;
    }

    public Date getInitial_diam_date_time() {
        return initial_diam_date_time;
    }

    public void setInitial_diam_date_time(Date initial_diam_date_time) {
        this.initial_diam_date_time = initial_diam_date_time;
    }

    public Object getFirst_theopro() {
        return first_theopro;
    }

    public void setFirst_theopro(Object first_theopro) {
        this.first_theopro = first_theopro;
    }

    public Date getFirst_theopro_date_time() {
        return first_theopro_date_time;
    }

    public void setFirst_theopro_date_time(Date first_theopro_date_time) {
        this.first_theopro_date_time = first_theopro_date_time;
    }

    public Object getFirst_realpro() {
        return first_realpro;
    }

    public void setFirst_realpro(Object first_realpro) {
        this.first_realpro = first_realpro;
    }

    public Date getFirst_realpro_date_time() {
        return first_realpro_date_time;
    }

    public void setFirst_realpro_date_time(Date first_realpro_date_time) {
        this.first_realpro_date_time = first_realpro_date_time;
    }

    public Object getNext_diameter() {
        return next_diameter;
    }

    public void setNext_diameter(Object next_diameter) {
        this.next_diameter = next_diameter;
    }

    public Date getNext_diam_date_time() {
        return next_diam_date_time;
    }

    public void setNext_diam_date_time(Date next_diam_date_time) {
        this.next_diam_date_time = next_diam_date_time;
    }

    public Object getCrack() {
        return crack;
    }

    public void setCrack(Object crack) {
        this.crack = crack;
    }

    public Date getCrack_date_time() {
        return crack_date_time;
    }

    public void setCrack_date_time(Date crack_date_time) {
        this.crack_date_time = crack_date_time;
    }

    public Object getBruise() {
        return bruise;
    }

    public void setBruise(Object bruise) {
        this.bruise = bruise;
    }

    public Date getBruise_date_time() {
        return bruise_date_time;
    }

    public void setBruise_date_time(Date bruise_date_time) {
        this.bruise_date_time = bruise_date_time;
    }

    public Object getFinal_theopro() {
        return final_theopro;
    }

    public void setFinal_theopro(Object final_theopro) {
        this.final_theopro = final_theopro;
    }

    public Date getFinal_theopro_date_time() {
        return final_theopro_date_time;
    }

    public void setFinal_theopro_date_time(Date final_theopro_date_time) {
        this.final_theopro_date_time = final_theopro_date_time;
    }

    public Object getFinal_realpro() {
        return final_realpro;
    }

    public void setFinal_realpro(Object final_realpro) {
        this.final_realpro = final_realpro;
    }

    public Date getFinal_realpro_date_time() {
        return final_realpro_date_time;
    }

    public void setFinal_realpro_date_time(Date final_realpro_date_time) {
        this.final_realpro_date_time = final_realpro_date_time;
    }

    public Object getRoundness() {
        return roundness;
    }

    public void setRoundness(Object roundness) {
        this.roundness = roundness;
    }

    public Date getRoundness_date_time() {
        return roundness_date_time;
    }

    public void setRoundness_date_time(Date roundness_date_time) {
        this.roundness_date_time = roundness_date_time;
    }

    public Object getEndcrack() {
        return endcrack;
    }

    public void setEndcrack(Object endcrack) {
        this.endcrack = endcrack;
    }

    public Date getEndcrack_date_time() {
        return endcrack_date_time;
    }

    public void setEndcrack_date_time(Date endcrack_date_time) {
        this.endcrack_date_time = endcrack_date_time;
    }

    public Object getEndbruise() {
        return endbruise;
    }

    public void setEndbruise(Object endbruise) {
        this.endbruise = endbruise;
    }

    public Date getEndbruise_date_time() {
        return endbruise_date_time;
    }

    public void setEndbruise_date_time(Date endbruise_date_time) {
        this.endbruise_date_time = endbruise_date_time;
    }

    public Object getInspekt() {
        return inspekt;
    }

    public void setInspekt(Object inspekt) {
        this.inspekt = inspekt;
    }

    public Date getInspekt_dt() {
        return inspekt_dt;
    }

    public void setInspekt_dt(Date inspekt_dt) {
        this.inspekt_dt = inspekt_dt;
    }

    public Object getEndinspekt() {
        return endinspekt;
    }

    public void setEndinspekt(Object endinspekt) {
        this.endinspekt = endinspekt;
    }

    public Date getEndinspekt_dt() {
        return endinspekt_dt;
    }

    public void setEndinspekt_dt(Date endinspekt_dt) {
        this.endinspekt_dt = endinspekt_dt;
    }

    public Object getStructure() {
        return structure;
    }

    public void setStructure(Object structure) {
        this.structure = structure;
    }

    public Date getStructure_date_time() {
        return structure_date_time;
    }

    public void setStructure_date_time(Date structure_date_time) {
        this.structure_date_time = structure_date_time;
    }

    public Object getUltrasound() {
        return ultrasound;
    }

    public void setUltrasound(Object ultrasound) {
        this.ultrasound = ultrasound;
    }

    public Date getUltrasound_dt() {
        return ultrasound_dt;
    }

    public void setUltrasound_dt(Date ultrasound_dt) {
        this.ultrasound_dt = ultrasound_dt;
    }

    public Object getUltrasoundend() {
        return ultrasoundend;
    }

    public void setUltrasoundend(Object ultrasoundend) {
        this.ultrasoundend = ultrasoundend;
    }

    public Date getUltrasoundend_dt() {
        return ultrasoundend_dt;
    }

    public void setUltrasoundend_dt(Date ultrasoundend_dt) {
        this.ultrasoundend_dt = ultrasoundend_dt;
    }

    public Object getRoughness() {
        return roughness;
    }

    public void setRoughness(Object roughness) {
        this.roughness = roughness;
    }

    public Date getRoughness_dt() {
        return roughness_dt;
    }

    public void setRoughness_dt(Date roughness_dt) {
        this.roughness_dt = roughness_dt;
    }

    public Object getFinal_report() {
        return final_report;
    }

    public void setFinal_report(Object final_report) {
        this.final_report = final_report;
    }

    public Date getFinal_report_date_time() {
        return final_report_date_time;
    }

    public void setFinal_report_date_time(Date final_report_date_time) {
        this.final_report_date_time = final_report_date_time;
    }

    public String getValidate_discard() {
        return validate_discard;
    }

    public void setValidate_discard(String validate_discard) {
        this.validate_discard = validate_discard;
    }

    public Object getHistory_grind() {
        return history_grind;
    }

    public void setHistory_grind(Object history_grind) {
        this.history_grind = history_grind;
    }

    public Object getHistory_pprograms() {
        return history_pprograms;
    }

    public void setHistory_pprograms(Object history_pprograms) {
        this.history_pprograms = history_pprograms;
    }

    public Object getHardness() {
        return hardness;
    }

    public void setHardness(Object hardness) {
        this.hardness = hardness;
    }

    public Date getHardness_dt() {
        return hardness_dt;
    }

    public void setHardness_dt(Date hardness_dt) {
        this.hardness_dt = hardness_dt;
    }
}