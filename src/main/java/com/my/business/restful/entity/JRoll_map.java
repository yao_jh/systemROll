package com.my.business.restful.entity;

import java.util.Date;

public class JRoll_map {
    private String RollID;
    private String RollType;
    private float Diam;
    private float Roundness;
    private float ShapDev;
    private int TotalUsedTimes;
    private String State;

    public String getRollID() {
        return RollID;
    }

    public void setRollID(String rollID) {
        RollID = rollID;
    }

    public String getRollType() {
        return RollType;
    }

    public void setRollType(String rollType) {
        RollType = rollType;
    }

    public float getDiam() {
        return Diam;
    }

    public void setDiam(float diam) {
        Diam = diam;
    }

    public float getRoundness() {
        return Roundness;
    }

    public void setRoundness(float roundness) {
        Roundness = roundness;
    }

    public float getShapDev() {
        return ShapDev;
    }

    public void setShapDev(float shapDev) {
        ShapDev = shapDev;
    }

    public int getTotalUsedTimes() {
        return TotalUsedTimes;
    }

    public void setTotalUsedTimes(int totalUsedTimes) {
        TotalUsedTimes = totalUsedTimes;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }
}
