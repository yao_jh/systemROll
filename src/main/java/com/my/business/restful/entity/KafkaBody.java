package com.my.business.restful.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;
import java.util.Map;

public class KafkaBody {

    private String standNo;
    private String zeroTime;
    private List<Map<String, String>> indexCalRes;

    @JSONField(name = "PDPAreceiver")
    private String PDPAreceiver;

    public String getStandNo() {
        return standNo;
    }

    public void setStandNo(String standNo) {
        this.standNo = standNo;
    }

    public String getZeroTime() {
        return zeroTime;
    }

    public void setZeroTime(String zeroTime) {
        this.zeroTime = zeroTime;
    }

    public List<Map<String, String>> getIndexCalRes() {
        return indexCalRes;
    }

    public void setIndexCalRes(List<Map<String, String>> indexCalRes) {
        this.indexCalRes = indexCalRes;
    }

    public String getPDPAreceiver() {
        return PDPAreceiver;
    }

    public void setPDPAreceiver(String PDPAreceiver) {
        this.PDPAreceiver = PDPAreceiver;
    }
}
