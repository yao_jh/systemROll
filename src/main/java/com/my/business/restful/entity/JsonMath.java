package com.my.business.restful.entity;

import com.my.business.sys.common.entity.JCommon;

import java.util.List;

/**
 * 轧辊多变异分析信号表json的实体类
 *
 * @author 生成器生成
 * @date 2020-09-10 20:20:19
 */
public class JsonMath{

   List<String> paramList;

   public List<String> getParamList() {
      return paramList;
   }

   public void setParamList(List<String> paramList) {
      this.paramList = paramList;
   }
}