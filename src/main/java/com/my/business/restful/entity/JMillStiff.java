package com.my.business.restful.entity;

public class JMillStiff {
    private String recordtime; //标定时间
    private String frame_no;  //机架号
    private Float retention;  //刚度保持率(%)
    private Float retention_score;  //刚度保持率得分
    private Float leveling;  //调平值(mm)
    private Float leveling_score;  //调平值得分
    private Float bothsides_stiffness;  //两侧刚度偏差(KN/mm)
    private Float bothsides_stiffness_score;  //两侧刚度偏差得分
    private Float bothposition_stiffness;  //两侧位置偏差(mm)
    private Float bothposition_stiffness_score;  //两侧位置偏差得分
    private Float osposition;  //OS位置偏差(mm)
    private Float osposition_score;  //OS位置偏差得分
    private Float dsposition;  //DS位置偏差(mm)
    private Float dsposition_score;  //DS位置偏差得分
    private Float rolling_force;  //轧制力偏差(KN)
    private Float rolling_force_score;  //轧制力偏差得分
    private Float osrollgap;  //OS辊缝偏差(mm)
    private Float osrollgap_score;  //OS辊缝偏差得分
    private Float dsrollgap;  //DS辊缝偏差(mm)
    private Float dsrollgap_score;  //DS辊缝偏差得分

    public String getRecordtime() {
        return recordtime;
    }

    public void setRecordtime(String recordtime) {
        this.recordtime = recordtime;
    }

    public String getFrame_no() {
        return frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public Float getRetention() {
        return retention;
    }

    public void setRetention(Float retention) {
        this.retention = retention;
    }

    public Float getRetention_score() {
        return retention_score;
    }

    public void setRetention_score(Float retention_score) {
        this.retention_score = retention_score;
    }

    public Float getLeveling() {
        return leveling;
    }

    public void setLeveling(Float leveling) {
        this.leveling = leveling;
    }

    public Float getLeveling_score() {
        return leveling_score;
    }

    public void setLeveling_score(Float leveling_score) {
        this.leveling_score = leveling_score;
    }

    public Float getBothsides_stiffness() {
        return bothsides_stiffness;
    }

    public void setBothsides_stiffness(Float bothsides_stiffness) {
        this.bothsides_stiffness = bothsides_stiffness;
    }

    public Float getBothsides_stiffness_score() {
        return bothsides_stiffness_score;
    }

    public void setBothsides_stiffness_score(Float bothsides_stiffness_score) {
        this.bothsides_stiffness_score = bothsides_stiffness_score;
    }

    public Float getBothposition_stiffness() {
        return bothposition_stiffness;
    }

    public void setBothposition_stiffness(Float bothposition_stiffness) {
        this.bothposition_stiffness = bothposition_stiffness;
    }

    public Float getBothposition_stiffness_score() {
        return bothposition_stiffness_score;
    }

    public void setBothposition_stiffness_score(Float bothposition_stiffness_score) {
        this.bothposition_stiffness_score = bothposition_stiffness_score;
    }

    public Float getOsposition() {
        return osposition;
    }

    public void setOsposition(Float osposition) {
        this.osposition = osposition;
    }

    public Float getOsposition_score() {
        return osposition_score;
    }

    public void setOsposition_score(Float osposition_score) {
        this.osposition_score = osposition_score;
    }

    public Float getDsposition() {
        return dsposition;
    }

    public void setDsposition(Float dsposition) {
        this.dsposition = dsposition;
    }

    public Float getDsposition_score() {
        return dsposition_score;
    }

    public void setDsposition_score(Float dsposition_score) {
        this.dsposition_score = dsposition_score;
    }

    public Float getRolling_force() {
        return rolling_force;
    }

    public void setRolling_force(Float rolling_force) {
        this.rolling_force = rolling_force;
    }

    public Float getRolling_force_score() {
        return rolling_force_score;
    }

    public void setRolling_force_score(Float rolling_force_score) {
        this.rolling_force_score = rolling_force_score;
    }

    public Float getOsrollgap() {
        return osrollgap;
    }

    public void setOsrollgap(Float osrollgap) {
        this.osrollgap = osrollgap;
    }

    public Float getOsrollgap_score() {
        return osrollgap_score;
    }

    public void setOsrollgap_score(Float osrollgap_score) {
        this.osrollgap_score = osrollgap_score;
    }

    public Float getDsrollgap() {
        return dsrollgap;
    }

    public void setDsrollgap(Float dsrollgap) {
        this.dsrollgap = dsrollgap;
    }

    public Float getDsrollgap_score() {
        return dsrollgap_score;
    }

    public void setDsrollgap_score(Float dsrollgap_score) {
        this.dsrollgap_score = dsrollgap_score;
    }
}
