package com.my.business.restful.entity;

import com.my.business.sys.common.entity.JCommon;

import java.util.Date;

/**
* 轧辊数据文件json的实体类
* @author  生成器生成
* @date 2020-08-19 15:22:53
*/
public class JRoll_data_files extends JCommon{

	private Integer pageIndex; // 第几页
	private Integer pageSize; // 每页多少数据
	private Object condition; // 查询条件
	private Roll_data_files roll_data_files;   //对应模块的实体类
	private Long grinder;
	private String str_indocno;
	private Date initial_date_time;
	
	public Integer getPageIndex() {
		return pageIndex;
	}
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Object getCondition() {
		return condition;
	}
	public void setCondition(Object condition) {
		this.condition = condition;
	}

	public Roll_data_files getRoll_data_files() {
		return roll_data_files;
	}

	public void setRoll_data_files(Roll_data_files roll_data_files) {
		this.roll_data_files = roll_data_files;
	}

	public Long getGrinder() {
		return grinder;
	}

	public void setGrinder(Long grinder) {
		this.grinder = grinder;
	}

	public String getStr_indocno() {
		return str_indocno;
	}
	public void setStr_indocno(String str_indocno) {
		this.str_indocno = str_indocno;
	}

	public Date getInitial_date_time() {
		return initial_date_time;
	}

	public void setInitial_date_time(Date initial_date_time) {
		this.initial_date_time = initial_date_time;
	}
}