package com.my.business.restful.entity;

/***
 * 磨辊间发成本预测实体类
 * 
 * @author cc
 *
 */
public class ForecastEntity {

	private String coil_no; // 卷号
	private String w_no; // 工作辊号
	private String w_forecast_stock; // 工作辊预测磨削量
	private String w_roll_type;  //工作辊轧辊类型
	private String frame_no;  //机架号

	public String getCoil_no() {
		return coil_no;
	}

	public void setCoil_no(String coil_no) {
		this.coil_no = coil_no;
	}

	public String getW_no() {
		return w_no;
	}

	public void setW_no(String w_no) {
		this.w_no = w_no;
	}

	public String getW_forecast_stock() {
		return w_forecast_stock;
	}

	public void setW_forecast_stock(String w_forecast_stock) {
		this.w_forecast_stock = w_forecast_stock;
	}

	public String getW_roll_type() {
		return w_roll_type;
	}

	public void setW_roll_type(String w_roll_type) {
		this.w_roll_type = w_roll_type;
	}

	public String getFrame_no() {
		return frame_no;
	}

	public void setFrame_no(String frame_no) {
		this.frame_no = frame_no;
	}

	
	
}
