package com.my.business.python.entity;

import java.util.List;
import java.util.Map;

public class JPythonEntity {
	private String url;
	private List<Map<String,String>> list;
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<Map<String, String>> getList() {
		return list;
	}

	public void setList(List<Map<String, String>> list) {
		this.list = list;
	} 

}
