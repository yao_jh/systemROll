package com.my.business.python.entity;

/***
 * python接口返回值
 * 
 * @author cc
 *
 */
public class PythonResult {
	private String code;
	private Object data;
	private String msg;
	
	public PythonResult(){
		
	}
	
	public PythonResult(String msg, String code, Object data) {
        this.msg = msg;
        this.data = data;
        this.code = code;
    }
	
	/**
     * 自定义访问成功
     *
     * @param msg
     * @param data
     * @return
     */
    public static PythonResult PythonResultSuccessSelf(String msg, Object data) {
        return new PythonResult(msg,"200", data);
    }
    
    /**
     * 自定义访问失败
     *
     * @param msg
     * @param data
     * @return
     */
    public static PythonResult PythonResultFaultSelf(String msg, Object data) {
        return new PythonResult(msg,"202", data);
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	

}
