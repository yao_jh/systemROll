package com.my.business.python.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSON;
import com.my.business.python.entity.JPythonEntity;
import com.my.business.python.entity.PythonResult;
import com.my.business.sys.common.entity.ResultData;


@RestController
@RequestMapping("/rest")
public class PythonController {
	
	/**
     * rest公共查询接口(返回ResultData类型的)
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/findCommon"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findCommon(@RequestBody String data) {
    	try {
    		JPythonEntity jPythonEntity = JSON.parseObject(data, JPythonEntity.class);
    		String url = jPythonEntity.getUrl();
    		List<Map<String,String>> param_list = jPythonEntity.getList();
    		
    		HttpHeaders headers = new HttpHeaders();
    		headers.add("Content-Type", "application/json");
    		MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<>();
    		HttpEntity<MultiValueMap> entity = new HttpEntity<>(paramMap, headers);
    		for(Map<String,String> m : param_list) {
    			for (Map.Entry<String, String> entry : m.entrySet()) {
    				paramMap.add(entry.getKey(), entry.getValue());
                }
    		}
    		RestTemplate restTemplate = new RestTemplate();
    		
    		
    		ResultData ob = restTemplate.postForObject(url, entity, ResultData.class);

//    		
    		return ResultData.ResultDataFaultSelf("调用成功", ob);
    	}catch(Exception e) {
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问失败,失败原因是" + e.getMessage(),null);
    	}
    }
    
    /**
     * python公共查询接口(返回ResultData类型的)
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/python/findCommon"}, method = RequestMethod.POST)
    @ResponseBody
    public PythonResult pythonfindCommon(@RequestBody String data) {
    	try {
    		JPythonEntity jPythonEntity = JSON.parseObject(data, JPythonEntity.class);
    		String url = jPythonEntity.getUrl();
    		List<Map<String,String>> param_list = jPythonEntity.getList();
    		
    		HttpHeaders headers = new HttpHeaders();
    		headers.add("Content-Type", "application/json");
    		MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<>();
    		HttpEntity<Map> entity = new HttpEntity<>(paramMap, headers);
    		for(Map<String,String> m : param_list) {
    			for (Map.Entry<String, String> entry : m.entrySet()) {
                }
    		}
    		RestTemplate restTemplate = new RestTemplate();
    		
    		
//    		String result = restTemplate.postForObject(url, entity, String.class);
//    		restTemplate.postForObject(url, "111111", String.class);
    		String result = restTemplate.postForObject(url, "111111", String.class);
    		
    		PythonResult ob = JSON.parseObject(result, PythonResult.class);
//    		
    		return ob;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return PythonResult.PythonResultFaultSelf("调用失败,失败信息为" + e.getMessage(), null);
    	}
    }

}
