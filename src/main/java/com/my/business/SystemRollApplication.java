package com.my.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@SpringBootApplication
@EnableScheduling
public class SystemRollApplication {
    protected static final Logger logger = LoggerFactory.getLogger(SystemRollApplication.class);
    public static void main(String[] args) {
        SpringApplication.run(SystemRollApplication.class, args);
    }
}
