package com.my.business.schedule;

import com.my.business.modular.basechock.service.BaseBearingService;
import com.my.business.modular.rckurumainfo.dao.RcKurumaInfoDao;
import com.my.business.modular.rckurumainfo.entity.RcKurumaInfo;
import com.my.business.modular.rollcoolorder.dao.RollCoolorderDao;
import com.my.business.modular.rollcoolorder.entity.RollCoolorder;
import com.my.business.modular.rolldatafiles.dao.RollDataFilesDao;
import com.my.business.modular.rollgrinding.dao.RollGrindingBFDao;
import com.my.business.modular.rollgrinding.entity.RollGrindingBF;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.modular.rollinformation.service.RollInformationService;
import com.my.business.modular.rollonoffline.dao.RollOnoffLineDao;
import com.my.business.modular.rollonoffline.entity.RollOnoffLine;
import com.my.business.modular.rollonoffline.entity.RollOnoffLineView;
import com.my.business.modular.rollpaired.dao.RollPairedDao;
import com.my.business.modular.rollprelisthistory.entity.RollPrelistHistory;
import com.my.business.modular.rollwear.dao.RollWearDao;
import com.my.business.modular.rollwear.service.RollWearService;
import com.my.business.sys.sysbz.dao.SysBzDao;
import com.my.business.sys.sysbz.entity.SysBz;
import com.my.business.sys.sysbz.service.SysBzService;
import com.my.business.util.DateUtil;
import com.my.business.workflow.workflow.service.WorkFlowService;
import com.my.business.workflow.workflowdetail.dao.WorkFlowdetailDao;
import com.my.business.workflow.workflowdetail.entity.WorkFlowdetail;
import com.my.business.workflow.workflowdetail.service.impl.WorkFlowdetailServiceImpl;
import lombok.extern.apachecommons.CommonsLog;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@CommonsLog
@Component
public class SystemSchedule {

    private final static Log logger = LogFactory.getLog(SystemSchedule.class);

    @Autowired
    private RollCoolorderDao rollCoolorderDao;

    @Autowired
    private WorkFlowdetailDao workFlowdetailDao;

    @Autowired
    private WorkFlowService workFlowService;

    @Autowired
    private RollOnoffLineDao rollOnoffLineDao;

    @Autowired
    private RollWearService rollWearService;

    @Autowired
    private BaseBearingService baseBearingService;

    @Autowired
    private RollInformationService rollInformationService;

    @Autowired
    private RollWearDao rollWearDao;

    @Autowired
    private RollGrindingBFDao rollGrindingBFDao;

    @Autowired
    private RollInformationDao rollInformationDao;

    @Autowired
    private RcKurumaInfoDao rcKurumaInfoDao;

    @Autowired
    private RollPairedDao rollPairedDao;
    @Autowired
    private SysBzService sysBzService;
    @Autowired
    private SysBzDao sysBzDao;

    @Autowired
    private RollDataFilesDao rollDataFilesDao;

    /**
     * 冷却定时器方法（用于定时比较数据库中冷却工单中的工单结束时间和当前系统时间，如果系统时间超过工单结束时间则生成消息提示操作工冷却完成）
     */
    @Scheduled(cron = "${cooltime}")
    public void scheduledTasks() {

        //获取当前系统时间超过冷却结束时间的数据
        List<RollCoolorder> list = rollCoolorderDao.findDataRollCoolorderByEndTime();

        if (list != null && list.size() > 0) {
            for (RollCoolorder entity : list) {
                String roll_no = "";
                //先把冷却工单记录状态变为已完成
                entity.setIfinish(1L);
                rollCoolorderDao.updateDataRollCoolorder(entity);
            }
        }
    }


    /**
     * 工作流定时任务(拿系统时间和工作流中配置的下一次执行时间比较，系统时间超过配置时间,则执行)
     */
    @Scheduled(cron = "${cooltime}")
    public void workFlowTasks() {
        try {
            List<WorkFlowdetail> list = workFlowdetailDao.findDataWorkFlowdetail();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for (WorkFlowdetail entity : list) {
                if (!StringUtils.isEmpty(entity.getIscheduled())) {
                    if (entity.getIscheduled() == 1) {
                        Date start_time = sdf.parse(entity.getScheduled_starttime());
                        Date sysdata = new Date();  //系统时间
                        if (start_time.getTime() < sysdata.getTime()) {  //如果系统时间超过定时开始时间则开始执行
                            workFlowService.createPushNewSchedularAll("", 0L, "定时");
                            WorkFlowdetailServiceImpl w = new WorkFlowdetailServiceImpl();
                            //根据算法执行下一次执行的时间
                            String time = w.nextDate(entity.getScheduled_starttime(), entity.getScheduled_time(), entity.getScheduled_unit(), entity.getMoveup_time());
                            entity.setScheduled_starttime(time);
                            workFlowdetailDao.updateDataWorkFlowdetail(entity);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 上下线定时检测是否有新数据生成(涉及到发送FSP成本的kafka)
     */
    @Scheduled(cron = "${cooltime}")
    public void lqSingleTasks() {
        try {
            logger.debug("进入方法");
            List<RollOnoffLine> list = rollOnoffLineDao.findDataRollOnoffLineByIstate();
            if (list != null && list.size() > 0) {
                logger.debug("集合数量为" + list.size());
            } else {
                logger.debug("集合数量为0");
            }
            String roll_no_list = "";
            String roll_type = "";
            //删除磨削重复记录
            rollWearDao.deleteDuplicateDataRollGrindingBFMessage();
            //删除 磨削曲线重复数据
            rollDataFilesDao.deleteDuplicateDataRollDataFilesMessage();
            //删除上下机重复记录
            rollOnoffLineDao.deleteDuplicateDataRollOnoffLineDaoMessage();
            for (RollOnoffLine rollOnoffLine : list) {
                logger.debug("轧辊号" + rollOnoffLine.getRoll_no());
                roll_type = rollOnoffLine.getRoll_type();
                roll_no_list = roll_no_list + rollOnoffLine.getRoll_no() + ",";
                rollOnoffLine.setIstate(1L);
                rollOnoffLineDao.updateDataRollOnoffLine(rollOnoffLine);
                //更新全生命周期
                rollWearService.changedata(rollOnoffLine.getRoll_no());
                //计算轴承和密封件的时间
                baseBearingService.changeBearingData(rollOnoffLine);
                //调整棍子的状态信息  待磨削 更改周转状态
                if(!StringUtils.isEmpty(rollOnoffLine.getRoll_no())){
                    rollInformationDao.updateDataRollInformationByRevoleve(rollOnoffLine.getRoll_no(),6L);
                }
                //根据轧辊上下机信息 进行轧辊上下机过程中 的 钢种 宽度 块数 重量等解析处理
                if(!StringUtils.isEmpty(rollOnoffLine.getRollinfo())){
                    parsingRollOnoffLine(rollOnoffLine);
                }

            }
            if (list != null && list.size() > 0) {
                rollPairedDao.updateRollRrevolve();
            }
            logger.debug("循环结束");
            if(!StringUtils.isEmpty(roll_no_list)) {
            	logger.debug("辊号集合为" + roll_no_list.substring(0, roll_no_list.length()-1));
                workFlowService.toPushNew2(roll_no_list.substring(0, roll_no_list.length()-1), "1111","有批"+ roll_type +"已经下线,请冷却", "lq", 51L, "备辊工pad", 0L, "信号");
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }
        //解析轧辊钢种等信息
    private void parsingRollOnoffLine(RollOnoffLine rollOnoffLine) {
        //1 删除此id (parent_id) 的所有新增数据
        rollOnoffLineDao.deleteDataRollOnoffLineView(rollOnoffLine.getIndocno());
        RollOnoffLineView bean=null;
        //2 根据 拼接信息进行数据解析 钢种 宽度 块数 重量  1 2 3 4  5 6 7 8
        String [] arr =  rollOnoffLine.getRollinfo().split("\n");
        if(arr.length>0){
            for(int i = 1 ;i<= arr.length ;i ++ ){
                if(i%4==1){
                    bean = new RollOnoffLineView();
                    BeanUtils.copyProperties(rollOnoffLine,bean);
                    bean.setParent_id(bean.getIndocno());
                    bean.setGz(arr[i-1]);
                }
                if(i%4==2){
                    bean.setKd(BigDecimal.valueOf(Long.valueOf(arr[i-1])));
                }
                if(i%4==3){
                    bean.setKs(BigDecimal.valueOf(Long.valueOf(arr[i-1])));
                }
                if(i%4==0){
                    bean.setZl(BigDecimal.valueOf(Long.valueOf(arr[i-1])));
                    rollOnoffLineDao.insertDataRollOnoffLineView(bean);
                }
            }
        }

    }
    /**
     *  监听磨削信息  7分钟 执行一次  查询 磨削开始时间 在五分钟以内的数据 有就更该轧辊状态 为待上机 且更改磨削为 已采集
     */
    @Scheduled(cron = "${cooltimemx}")
    public void mxSingleTasks() {
        List<RollGrindingBF> list = rollGrindingBFDao.findDataRollGrindingByIstate();
        if(list!=null&&list.size()>0){
            for(RollGrindingBF bean:list){
                if(!StringUtils.isEmpty(bean.getRoll_no())&&!StringUtils.isEmpty(bean.getGrind_starttime())){
                    RollInformation r2 = rollInformationDao.findSynchronousRollGrindingBFByRollno(bean.getRoll_no());
                    //查找对应辊号基本信息
                    RollInformation rollInformation = rollInformationDao.findDataRollInformationByRollNo(bean.getRoll_no());
                    if(rollInformation!=null) {
                        if (r2 != null) {
                            if (r2.getRoll_no() != null) {
                                rollInformation.setLastgrindingdepth(r2.getLastgrindingdepth());
                                rollInformation.setRollshape(r2.getRollshape());
                                rollInformation.setRoundness(r2.getRoundness());
                                rollInformation.setGrindingcount(r2.getGrindingcount());
                                rollInformation.setLastgrindingdepth(r2.getLastgrindingdepth());
                                rollInformation.setCurrentdiameter(r2.getCurrentdiameter());
                            }
                        }
                        rollInformation.setRoll_revolve(4L);
                        rollInformationDao.updateDataRollInformation(rollInformation);
                    }
                    logger.debug("轧辊号 改成待上机" + bean.getRoll_no());
                    //磨削后的辊 修改为 待上机
                    //rollInformationDao.updateDataRollInformationByRevoleve(bean.getRoll_no(),4L);
                    rollGrindingBFDao.updateDataRollGrindingBFForIstate(bean.getIndocno(),1L);

                    //根据此磨床数据的开始时间 判断 查询 班组 班次 人员
                    SysBz sysBz = sysBzDao.findDataSysBzByIndocno(1000L);
                    Map<String, String> map = null;
                    try {
                        map = sysBzService.findbz(bean.getGrind_endtime());

                        if(!StringUtils.isEmpty(map.get("bz"))&&!StringUtils.isEmpty(map.get("bc"))){
                            //如果  当前磨削时间的班次 班组和 填入的对应 那直接去当前查询出来的值
                            if(bean.getClass().equals("A")&&map.get("bz").equals("甲")){
                                if(bean.getMachineno()!=null){
                                    if(bean.getMachineno().equals(1)||bean.getMachineno().equals("1#")||bean.getMachineno().equals("1")){
                                        bean.setOperator(sysBz.getJ1());
                                        bean.setSgroup(map.get("bc"));
                                        bean.setSclass("甲");
                                        rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                    }else if(bean.getMachineno().equals(2)||bean.getMachineno().equals("2#")||bean.getMachineno().equals("2")){
                                        bean.setOperator(sysBz.getJ2());
                                        bean.setSgroup(map.get("bc"));
                                        bean.setSclass("甲");
                                        rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                    }else if(bean.getMachineno().equals(3)||bean.getMachineno().equals("3#")||bean.getMachineno().equals("3")){
                                        bean.setOperator(sysBz.getJ3());
                                        bean.setSgroup(map.get("bc"));
                                        bean.setSclass("甲");
                                        rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                    }else if(bean.getMachineno().equals(4)||bean.getMachineno().equals("4#")||bean.getMachineno().equals("4")){
                                        bean.setOperator(sysBz.getJ4());
                                        bean.setSgroup(map.get("bc"));
                                        bean.setSclass("甲");
                                        rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                    }else if(bean.getMachineno().equals(5)||bean.getMachineno().equals("5#")||bean.getMachineno().equals("5")){
                                        bean.setOperator(sysBz.getJ5());
                                        bean.setSgroup(map.get("bc"));
                                        bean.setSclass("甲");
                                        rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                    }
                                }
                            }
                            if(bean.getClass().equals("B")&&map.get("bz").equals("乙")){
                                if(bean.getMachineno()!=null){
                                    if(bean.getMachineno().equals(1)||bean.getMachineno().equals("1#")||bean.getMachineno().equals("1")){
                                        bean.setOperator(sysBz.getY1());
                                        bean.setSgroup(map.get("bc"));
                                        bean.setSclass("乙");
                                        rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                    }else if(bean.getMachineno().equals(2)||bean.getMachineno().equals("2#")||bean.getMachineno().equals("2")){
                                        bean.setOperator(sysBz.getY2());
                                        bean.setSgroup(map.get("bc"));
                                        bean.setSclass("乙");
                                        rollGrindingBFDao.updateDataRollGrindingBF(bean);
                                    }else if(bean.getMachineno().equals(3)||bean.getMachineno().equals("3#")||bean.getMachineno().equals("3")){
                                        bean.setOperator(sysBz.getY3());
                                        bean.setSgroup(map.get("bc"));
                                        bean.setSclass("乙");
                                        rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                    }else if(bean.getMachineno().equals(4)||bean.getMachineno().equals("4#")||bean.getMachineno().equals("4")){
                                        bean.setOperator(sysBz.getY4());
                                        bean.setSgroup(map.get("bc"));
                                        bean.setSclass("乙");
                                        rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                    }else if(bean.getMachineno().equals(5)||bean.getMachineno().equals("5#")||bean.getMachineno().equals("5")){
                                        bean.setOperator(sysBz.getY5());
                                        bean.setSgroup(map.get("bc"));
                                        bean.setSclass("乙");
                                        rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                    }
                                }
                            }
                            if(bean.getClass().equals("C")&&map.get("bz").equals("丙")){
                                if(bean.getMachineno()!=null){
                                    if(bean.getMachineno().equals(1)||bean.getMachineno().equals("1#")||bean.getMachineno().equals("1")){
                                        bean.setOperator(sysBz.getB1());
                                        bean.setSgroup(map.get("bc"));
                                        bean.setSclass("丙");
                                        rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                    }else if(bean.getMachineno().equals(2)||bean.getMachineno().equals("2#")||bean.getMachineno().equals("2")){
                                        bean.setOperator(sysBz.getB2());
                                        bean.setSgroup(map.get("bc"));
                                        bean.setSclass("丙");
                                        rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                    }else if(bean.getMachineno().equals(3)||bean.getMachineno().equals("3#")||bean.getMachineno().equals("3")){
                                        bean.setOperator(sysBz.getB3());
                                        bean.setSgroup(map.get("bc"));
                                        bean.setSclass("丙");
                                        rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                    }else if(bean.getMachineno().equals(4)||bean.getMachineno().equals("4#")||bean.getMachineno().equals("4")){
                                        bean.setOperator(sysBz.getB4());
                                        bean.setSgroup(map.get("bc"));
                                        bean.setSclass("丙");
                                        rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                    }else if(bean.getMachineno().equals(5)||bean.getMachineno().equals("5#")||bean.getMachineno().equals("5")){
                                        bean.setOperator(sysBz.getB5());
                                        bean.setSgroup(map.get("bc"));
                                        bean.setSclass("丙");
                                        rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                    }
                                }
                            }else{

                                if(map.get("bz").equals("甲")){
                                    if(bean.getMachineno()!=null){
                                        if(bean.getMachineno().equals(1)||bean.getMachineno().equals("1#")||bean.getMachineno().equals("1")){
                                            bean.setOperator(sysBz.getJ1());
                                            bean.setSgroup(map.get("bc"));
                                            bean.setSclass("甲");
                                            rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                        }else if(bean.getMachineno().equals(2)||bean.getMachineno().equals("2#")||bean.getMachineno().equals("2")){
                                            bean.setOperator(sysBz.getJ2());
                                            bean.setSgroup(map.get("bc"));
                                            bean.setSclass("甲");
                                            rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                        }else if(bean.getMachineno().equals(3)||bean.getMachineno().equals("3#")||bean.getMachineno().equals("3")){
                                            bean.setOperator(sysBz.getJ3());
                                            bean.setSgroup(map.get("bc"));
                                            bean.setSclass("甲");
                                            rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                        }else if(bean.getMachineno().equals(4)||bean.getMachineno().equals("4#")||bean.getMachineno().equals("4")){
                                            bean.setOperator(sysBz.getJ4());
                                            bean.setSgroup(map.get("bc"));
                                            bean.setSclass("甲");
                                            rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                        }else if(bean.getMachineno().equals(5)||bean.getMachineno().equals("5#")||bean.getMachineno().equals("5")){
                                            bean.setOperator(sysBz.getJ5());
                                            bean.setSgroup(map.get("bc"));
                                            bean.setSclass("甲");
                                            rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                        }
                                    }
                                }
                                if(map.get("bz").equals("乙")){
                                    if(bean.getMachineno()!=null){
                                        if(bean.getMachineno().equals(1)||bean.getMachineno().equals("1#")||bean.getMachineno().equals("1")){
                                            bean.setOperator(sysBz.getY1());
                                            bean.setSgroup(map.get("bc"));
                                            bean.setSclass("乙");
                                            rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                        }else if(bean.getMachineno().equals(2)||bean.getMachineno().equals("2#")||bean.getMachineno().equals("2")){
                                            bean.setOperator(sysBz.getY2());
                                            bean.setSgroup(map.get("bc"));
                                            bean.setSclass("乙");
                                            rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                        }else if(bean.getMachineno().equals(3)||bean.getMachineno().equals("3#")||bean.getMachineno().equals("3")){
                                            bean.setOperator(sysBz.getY3());
                                            bean.setSgroup(map.get("bc"));
                                            bean.setSclass("乙");
                                            rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                        }else if(bean.getMachineno().equals(4)||bean.getMachineno().equals("4#")||bean.getMachineno().equals("4")){
                                            bean.setOperator(sysBz.getY4());
                                            bean.setSgroup(map.get("bc"));
                                            bean.setSclass("乙");
                                            rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                        }else if(bean.getMachineno().equals(5)||bean.getMachineno().equals("5#")||bean.getMachineno().equals("5")){
                                            bean.setOperator(sysBz.getY5());
                                            bean.setSgroup(map.get("bc"));
                                            bean.setSclass("乙");
                                            rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                        }
                                    }
                                }
                                if(map.get("bz").equals("丙")){
                                    if(bean.getMachineno()!=null){
                                        if(bean.getMachineno().equals(1)||bean.getMachineno().equals("1#")||bean.getMachineno().equals("1")){
                                            bean.setOperator(sysBz.getB1());
                                            bean.setSgroup(map.get("bc"));
                                            bean.setSclass("丙");
                                            rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                        }else if(bean.getMachineno().equals(2)||bean.getMachineno().equals("2#")||bean.getMachineno().equals("2")){
                                            bean.setOperator(sysBz.getB2());
                                            bean.setSgroup(map.get("bc"));
                                            bean.setSclass("丙");
                                            rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                        }else if(bean.getMachineno().equals(3)||bean.getMachineno().equals("3#")||bean.getMachineno().equals("3")){
                                            bean.setOperator(sysBz.getB3());
                                            bean.setSgroup(map.get("bc"));
                                            bean.setSclass("丙");
                                            rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                        }else if(bean.getMachineno().equals(4)||bean.getMachineno().equals("4#")||bean.getMachineno().equals("4")){
                                            bean.setOperator(sysBz.getB4());
                                            bean.setSgroup(map.get("bc"));
                                            bean.setSclass("丙");
                                            rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                        }else if(bean.getMachineno().equals(5)||bean.getMachineno().equals("5#")||bean.getMachineno().equals("5")){
                                            bean.setOperator(sysBz.getB5());
                                            bean.setSgroup(map.get("bc"));
                                            bean.setSclass("丙");
                                            rollGrindingBFDao.updateDataRollGrindingBFPersonal(bean);
                                        }
                                    }
                                }

                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
            }
            rollPairedDao.updateRollRrevolve();
        }
    }

    /**
     *  监听上机信息  6分钟 执行一次  查询已上机信息在 换辊车是不是 有 如果有 则清空 换辊车
     */
    @Scheduled(cron = "${cooltimesj}")
    public void sjSingleTasks() {
        List<RollInformation> list = rollInformationDao.findDataRollInformationByPage(0, 50, null, null, null, null, null, null, 7+"", null, null, null, null, null, null, null, null, null, null, null);
        if(list!=null&&list.size()>0){
            RcKurumaInfo rc = null;
            RcKurumaInfo rcNew = null;
            for (RollInformation bean:list){
                 rc = rcKurumaInfoDao.findDataRcKurumaInfoByrollId(bean.getRoll_no());
                 if(rc!=null){
                     rcNew = new RcKurumaInfo();
                     rcNew.setStandno(rc.getStandno());
                     rcNew.setRoll_position(rc.getRoll_position());
                     rcNew.setRoll_type(rc.getRoll_type());
                     rcKurumaInfoDao.updateDataRcKurumaInfoByPre(rcNew);
                     logger.debug("清空此棍子在换辊车中的信息" + bean.getRoll_no());

                 }
            }
        }
    }

}
