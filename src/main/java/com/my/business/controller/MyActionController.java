package com.my.business.controller;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.my.business.modular.rollstiffness.entity.RollStiffness;
import com.my.business.modular.rollstiffness.service.RollStiffnessService;
import com.my.business.modular.rollstiffnessmath.entity.RollStiffnessMath;
import com.my.business.modular.rollstiffnessmath.service.RollStiffnessMathService;

@Controller
@RequestMapping("/action")
public class MyActionController {
	
	@Autowired
    private RollStiffnessMathService rollStiffnessMathService;
	
	@Autowired
    private RollStiffnessService rollStiffnessService;
	
	@GetMapping("/todo")
    public String html() {
		List<RollStiffnessMath> list_math = rollStiffnessMathService.findDataRollStiffnessMathTest();
		for(RollStiffnessMath entity : list_math) {
			RollStiffness rollStiffness = new RollStiffness();
			BeanUtils.copyProperties(entity, rollStiffness);
			rollStiffnessService.insertDataRollStiffnessByMath(rollStiffness);
		}
        return "成功";
    }

}
