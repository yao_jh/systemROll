package com.my.business.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.menu.service.SysMenuService;
import com.my.business.sys.role.service.SysRoleService;
import com.my.business.sys.user.entity.SysUser;
import com.my.business.sys.user.entity.SysUserMenu;
import com.my.business.sys.user.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class LoginController { 

    @Autowired
    private SysMenuService sysMenuService;

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysUserService sysUserService;

    @GetMapping("toIndex")
    public String toIndex() {
        return "index";
    }

    @GetMapping("login")
    public String login() {
        return "login";
    }

    /***
     * 登录返回人员和所有菜单信息    
     * @param request
     * @param response
     * @return
     */
    @CrossOrigin
    @PostMapping("doLoginAll")
    @ResponseBody
    public ResultData doLoginAll(@RequestBody String data) {
        JCommon common = JSON.parseObject(data, JCommon.class);
        String account = common.getAccount();
        String password = common.getPassword();

        SysUserMenu userMenu = new SysUserMenu();
        try {
            SysUser user = sysUserService.findDataByAccountAndPassword(account, password);
            userMenu.setRoles(sysRoleService.findDataStringByUserId(user.getIndocno()));
            userMenu.setUser(user);
            userMenu.setMenus(sysMenuService.findDataSysMenuAllByUserId(user.getIndocno()));

        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("登陆失败", null);
        }
        return ResultData.ResultDataSuccessSelf("登陆成功", userMenu);
    }

    /***
     * 登录返回人员和一级菜单
     * @param request
     * @param response
     * @return
     */
    @CrossOrigin
    @PostMapping("doLogin")
    @ResponseBody
    public ResultData doLogin(@RequestBody String data) {
        JCommon common = JSON.parseObject(data, JCommon.class);
        String account = common.getAccount();
        String password = common.getPassword();

        SysUserMenu userMenu = new SysUserMenu();
        try {
            SysUser user = sysUserService.findDataByAccountAndPassword(account, password);
            userMenu.setRoles(sysRoleService.findDataStringByUserId(user.getIndocno()));
            userMenu.setUser(user);
            userMenu.setMenus(sysMenuService.findDataSysMenuFirstByUserId(user.getIndocno()));

        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("登陆失败", null);
        }
        return ResultData.ResultDataSuccessSelf("登陆成功", userMenu);
    }

    /***
     * 单点登录——登录返回人员和一级菜单
     * @param request
     * @param response
     * @return
     */
    @CrossOrigin
    @PostMapping("doLoginSingle")
    @ResponseBody
    public ResultData doLoginO(@RequestBody String data) {
        JCommon common = JSON.parseObject(data, JCommon.class);
        String account = common.getAccount();
        String password = common.getPassword();

        SysUserMenu userMenu = new SysUserMenu();
        try {
            SysUser user = sysUserService.findDataByAccount(account);
            userMenu.setRoles(sysRoleService.findDataStringByUserId(user.getIndocno()));
            userMenu.setUser(user);
//            userMenu.setMenus(sysMenuService.findDataSysMenuFirstByUserId(user.getIndocno()));
            userMenu.setMenus(sysMenuService.findDataSysMenuAllByUserId(user.getIndocno()));

        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("登陆失败", null);
        }
        return ResultData.ResultDataSuccessSelf("登陆成功", userMenu);
    }

    @GetMapping("logout")
    public String logout() {
        return "login";
    }

}
