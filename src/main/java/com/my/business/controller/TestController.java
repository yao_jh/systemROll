package com.my.business.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.orderdict.entity.JOrderDict;

@Controller
@RequestMapping("test")
public class TestController {
	
	private Logger logger = LoggerFactory.getLogger(TestController.class);

	@CrossOrigin
    @GetMapping("")
    @ResponseBody
    public String test() {
    	logger.debug("测试成功");
        return "成功";
    }

    @GetMapping("html")
    public String html() {
        return "test";
    }
    
    @CrossOrigin
    @RequestMapping(value = {"/tt"}, method = RequestMethod.POST)
    @ResponseBody
    public String tt(@RequestBody String data) {
    	System.out.println(data);
    	return "success";
    }

}
