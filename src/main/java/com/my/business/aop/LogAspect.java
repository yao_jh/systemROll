package com.my.business.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.my.business.sys.common.entity.JCommon;


/***
 * 日志切面
 * @author cc
 *
 */
@Component
@Aspect
public class LogAspect {
	
	/**
     * 此处的切点是注解的方式，也可以用包名的方式达到相同的效果
     * '@Pointcut("execution(* com.wwj.springboot.service.impl.*.*(..))")'
     */
//    @Pointcut("@annotation(com.my.business.modular.*.service.impl.*.*(..))")
    @Pointcut("@annotation(com.my.business.aop.Log)")
    public void pointcut(){}
    
    @Before("pointcut()")
    public void doBeforeAdvice(JoinPoint joinpoint){
        System.out.println("进入方法前执行.....");
//        for (int i = 0; i < joinpoint.getArgs().length; i++) {     //遍历参数
//            System.out.println(joinpoint.getArgs()[i]);  
//        }
//        String[]  paramArray = ((MethodSignature)joinpoint.getSignature()).getParameterNames();  //参数名变成数组
//        for (int i = 0; i < paramArray.length; i++) {  
//            System.out.println(paramArray[i]);  
//        }
        String paramargs = (String) joinpoint.getArgs()[0];   //参数
        String userargs = (String) joinpoint.getArgs()[1];	//操作接口人员信息
        
        JCommon commonmodular = JSON.parseObject(paramargs,JCommon.class);   //获取操作信息
        JCommon commonuser = JSON.parseObject(userargs,JCommon.class);   //获取人员信息
        
    }

}
