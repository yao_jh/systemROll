package com.my.business.webservice.util;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;

public class WebserviceUtil {

    /***
     * 根据webservice的url和方法名返回对应的结果
     * @param url
     * @param method
     * @return
     */
    public static String getWebservice(String url, String method) {
        JaxWsDynamicClientFactory dcflient = JaxWsDynamicClientFactory.newInstance();

        Client client = dcflient.createClient(url);

        Object[] objectall = null;
        try {
            objectall = client.invoke(method);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("*******" + objectall[0].toString());
        return objectall[0].toString();
    }

    /***
     * 重写别人的webservice生成xml字符创的方法
     * @param str  已生成的字符串
     * @return 改变后的字符串
     */
    public static String overWriteWebservice(String str) {
        String strings = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<string xmlns=\"http://tempuri.org/\">\n" + str + "</string>";
        return strings;
    }

}
