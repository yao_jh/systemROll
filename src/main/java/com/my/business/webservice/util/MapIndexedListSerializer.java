package com.my.business.webservice.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Map;

/**
 * MapIndexedListSerializer.java
 * Created by luckzj on 12/11/17.
 */
public class MapIndexedListSerializer extends StdSerializer<Object[]> {
    private MapAttributeSerializer mapAttributeSerializer = new MapAttributeSerializer(Map.class);
    private Boolean isAttribute = false;

    public MapIndexedListSerializer() {
        super(Object[].class);
    }

    protected MapIndexedListSerializer(Class<Object[]> t) {
        super(t);
    }

    private Boolean getNextIsAttribute(JsonGenerator gen) {
        Boolean ret = false;
        try {
            Field field = gen.getClass().getDeclaredField("_nextIsAttribute");
            field.setAccessible(true);
            ret = field.getBoolean(gen);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return ret;
    }

    @Override
    public void serialize(Object[] value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        isAttribute = getNextIsAttribute(gen);

        gen.writeStartArray(value.length);
        serializeContents(value, gen, provider);
        gen.writeEndArray();
    }

    private void serializeContents(Object[] value, JsonGenerator g, SerializerProvider provider) throws IOException {
        final int len = value.length;
        if (len == 0) {
            return;
        }

        int i = 0;
        try {
            for (; i < len; ++i) {
                Map<String, String> elem = (Map<String, String>) value[i];
                if (elem == null) {
                    provider.defaultSerializeNull(g);
                } else {
                    ((ToXmlGenerator) g).setNextIsAttribute(isAttribute);
                    mapAttributeSerializer.serialize(elem, g, provider);
                }
            }
        } catch (Exception e) {
            wrapAndThrow(provider, e, value, i);
        }
    }

    public static class MapAttributeSerializer extends StdSerializer<Map> {
        protected MapAttributeSerializer(Class<Map> t) {
            super(t);
        }

        @Override
        public void serialize(Map value, JsonGenerator gen, SerializerProvider provider) throws IOException {
            gen.writeStartObject(value);
            if (!value.isEmpty()) {
                serializeFields(value, gen, provider);
            }
            gen.writeEndObject();
        }

        private void serializeFields(Map<String, String> value, JsonGenerator gen, SerializerProvider provider) throws IOException {
            String keyElem = null;
            try {
                for (Map.Entry<String, String> entry : value.entrySet()) {
                    keyElem = entry.getKey();
                    gen.writeObjectField(entry.getKey(), entry.getValue());
                }
            } catch (Exception e) { // Add reference information
                wrapAndThrow(provider, e, value, keyElem);
            }
        }
    }
}

