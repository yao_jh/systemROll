package com.my.business.webservice.util;

/**
 * WebServiceRequestIp.java
 * Created by luckzj on 1/4/18.
 */
public class WebServiceRequestIp {
    private static ThreadLocal<String> clientIp = new ThreadLocal<String>();

    public static void set(String ip) {
        clientIp.set(ip);
    }

    public static String get() {
        return clientIp.get();
    }
}
