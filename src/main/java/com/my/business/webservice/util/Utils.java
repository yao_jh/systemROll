package com.my.business.webservice.util;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Utils.java
 * Created by luckzj on 11/5/17.
 */
public class Utils {
    private static Logger logger = LoggerFactory.getLogger(Utils.class);
    private static ObjectMapper objectMapper = new ObjectMapper();
    private static XmlMapper xmlMapper = new XmlMapper();

    static {
        xmlMapper.getFactory().setXMLTextElementName("_TEXT_");
        // xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    /**
     * Get intersection of String array.
     *
     * @param arr1
     * @param arr2
     * @return intersection array
     */
    public static String[] intersect(String[] arr1, String[] arr2) {
        if (ArrayUtils.isEmpty(arr1) || ArrayUtils.isEmpty(arr2)) {
            return new String[0];
        }
        HashMap<String, Boolean> mark = new HashMap<String, Boolean>();
        for (String str : arr1) {
            if (!mark.containsKey(str)) {
                mark.put(str, Boolean.TRUE);
            }
        }

        List<String> list = new ArrayList<String>();
        for (String str : arr2) {
            if (mark.containsKey(str)) {
                // remove key to remove duplicated intersection result
                mark.remove(str);
                list.add(str);
            }
        }

        return list.toArray(new String[0]);
    }

    /**
     * Clone Map Array
     *
     * @param dataList
     * @return
     */
    public static Map<String, String>[] cloneMapList(Map<String, String>[] dataList) {
        if (dataList == null || dataList.length == 0) {
            return null;
        }

        Map[] ret = new Map[dataList.length];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = new HashMap();
            ret[i].putAll(dataList[i]);
        }

        return ret;
    }

    /**
     * Get Remote IP Address of visitor
     *
     * @param request
     * @return
     */
    public static String getRemoteAddress(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }

        return ip;
    }

    /**
     * Json To Xml
     *
     * @param json
     * @return
     */
    public static String json2Xml(String json) throws java.lang.Exception {
        Object obj = null;

        Map map = null;
        try {
            map = objectMapper.readValue(json, Map.class);
            obj = map;
        } catch (IOException e) {
        }

        if (obj == null) {
            try {
                List list = objectMapper.readValue(json, List.class);
                obj = list;
            } catch (IOException e) {
            }
        }

        if (obj == null) {
            logger.error("json格式错误：{}", json);
            throw new java.lang.Exception("格式转换错误！");
        }

        String xml = xmlMapper.writeValueAsString(obj);
        return xml;
    }

    /**
     * Xml to Json
     *
     * @param xml
     * @return
     */
    public static String xml2Json(String xml) throws IOException {
        List map = xmlMapper.readValue(xml, List.class);

        return objectMapper.writeValueAsString(map);
    }

    public static List xml2List(String xml) throws IOException {
        return xmlMapper.readValue(xml, List.class);
    }

    public static Map<String, Object> xml2Map(String xml) throws IOException {
        return xmlMapper.readValue(xml, Map.class);
    }

    public static <T> T xml2Object(String xml, Class<T> clz) throws IOException {
        return xmlMapper.readValue(xml, clz);
    }

    public static String obj2Xml(Object xmlRequest) throws JsonProcessingException {
        return xmlMapper.writeValueAsString(xmlRequest);
    }

//    public static byte[] downloadFile(String url) {
//        CloseableHttpClient client = HttpClients.createDefault();
//        RequestConfig config = RequestConfig.custom().build();
//        HttpGet httpGet = new HttpGet(url);
//        httpGet.setConfig(config);
//
//        byte[] data = null;
//
//        try {
//            HttpResponse respone = client.execute(httpGet);
//            if(respone.getStatusLine().getStatusCode() != HttpStatus.SC_OK){
//                return null;
//            }
//            HttpEntity entity = respone.getEntity();
//            if(entity != null) {
//                InputStream is = entity.getContent();
//                ByteArrayOutputStream os = new ByteArrayOutputStream();
//                byte[] buffer = new byte[4096];
//                int len = -1;
//                while((len = is.read(buffer) )!= -1){
//                    os.write(buffer, 0, len);
//                }
//                is.close();
//
//                data = os.toByteArray();
//            }
//        } catch (Exception e) {
//
//        }finally{
//            try {
//                client.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//        return data;
//    }
}
