package com.my.business.webservice.controller;


import com.my.business.sys.common.entity.ResultData;
import com.my.business.webservice.service.DataLinkerServicesSoap;
import com.my.business.webservice.util.Utils;
import com.my.business.webservice.util.WebServiceRequestIp;
import com.my.business.webservice.util.WebserviceUtil;
import com.my.business.webservice.vo.ApiResponse;
import com.my.business.webservice.vo.ErrorCode;
import com.my.business.webservice.vo.webservice.*;
import com.my.business.webservice.vo.webservice.xml.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.concurrent.Callable;

/**
 * InvocationTransfomerController.java
 * Created by luckzj on 12/2/17.
 */
@Api(tags = "转调接口", description = "转调WebService服务接口")
@RestController
@RequestMapping(value = "/webservice")
public class WebServiceTransformController {

    @Autowired
    private DataLinkerServicesSoap dataLinkerServicesSoap;

    private static ApiResponse getErrorResponse(WebServiceXmlResponse webServiceXmlResponse) {
        if (webServiceXmlResponse.getSystem() == null) {
            return ApiResponse.createResponse();
        }

        // system.status
        if (webServiceXmlResponse.getSystem().getStatus() != null
                && StringUtils.equalsIgnoreCase(webServiceXmlResponse.getSystem().getStatus().getConnectToTagServer(), "false")) {

            return ApiResponse.createResponse(ErrorCode.ERR_WEBSERVICE_SERVER_NOT_CONNECTED);
        }

        // system.error
        if (webServiceXmlResponse.getSystem().getError() != null) {
            WebServiceXmlResponse.Error error = webServiceXmlResponse.getSystem().getError();
            String msg = String.format("source = [%s], info = [%s]", error.getSource(), error.getInfo());
            return ApiResponse.createResponse(ErrorCode.ERR_WEBSERVICE_SYSERROR, msg);
        }


        return ApiResponse.createResponse();
    }

    @ApiOperation(value = "转调用getHmiData接口")
    @CrossOrigin
    @RequestMapping(value = "/getHmiData", method = RequestMethod.POST)
    public Callable<ApiResponse<HmiDataResponseVO>> getHmiData(
            @RequestBody final HmiDataRequestVO request,
            @ApiIgnore final HttpServletRequest httpServletRequest
    ) throws IOException {

        return new Callable<ApiResponse<HmiDataResponseVO>>() {
            public ApiResponse<HmiDataResponseVO> call() throws Exception {
                HmiDataXmlRequest xmlRequest = HmiDataXmlRequest.fromVO(request);
                String xml = Utils.obj2Xml(xmlRequest);

                WebServiceRequestIp.set(httpServletRequest.getRemoteHost());
                String result = WebserviceUtil.overWriteWebservice(dataLinkerServicesSoap.getHmiData(xml));

                HmiDataXmlResponse xmlResponse = Utils.xml2Object(result, HmiDataXmlResponse.class);
                ApiResponse response = getErrorResponse(xmlResponse);
                if (response.getCode() != 0) {
                    return response;
                }

                HmiDataResponseVO responseVO = HmiDataResponseVO.fromXmlResponse(xmlResponse);
                response.setData(responseVO);
                return response;
            }
        };
    }

    @ApiOperation(value = "转调")
    @RequestMapping(value = "/getLogData", method = RequestMethod.POST)
    public Callable<ApiResponse<LogDataResponseVO>> getLogData(
            @ApiParam(name = "输入参数")
            @RequestBody final LogDataRequestVO requestVO,
            @ApiIgnore final HttpServletRequest httpServletRequest
    ) throws IOException {

        return new Callable<ApiResponse<LogDataResponseVO>>() {
            public ApiResponse<LogDataResponseVO> call() throws Exception {
                if (requestVO.getCount() == null) {
                    requestVO.setCount(0);
                }

                WebServiceRequestIp.set(httpServletRequest.getRemoteHost());
                String result = dataLinkerServicesSoap.getLogData(
                        requestVO.getStartTime(), requestVO.getEndTime()
                        , requestVO.getKeyword(), requestVO.getCount());

                LogDataXmlResponse xmlResponse = Utils.xml2Object(result, LogDataXmlResponse.class);

                ApiResponse response = getErrorResponse(xmlResponse);
                if (response.getCode() != 0) {
                    return response;
                }

                LogDataResponseVO[] responseVO = LogDataResponseVO.fromXmlResponse(xmlResponse);
                response.setData(responseVO);
                return response;
            }
        };

    }

    @ApiOperation(value = "转调GetLoggerServer")
    @RequestMapping(value = "/getLoggerServer", method = RequestMethod.GET)
    public ApiResponse getLoggerServer(@ApiIgnore final HttpServletRequest httpServletRequest) throws IOException {

        WebServiceRequestIp.set(httpServletRequest.getRemoteHost());
        String result = dataLinkerServicesSoap.getLoggerServer();

        LoggerServerXmlResponse xmlResponse = Utils.xml2Object(result, LoggerServerXmlResponse.class);

        ApiResponse response = getErrorResponse(xmlResponse);
        if (response.getCode() != 0) {
            return response;
        }

        LoggerServerResponseVO vo = LoggerServerResponseVO.fromXmlResponse(xmlResponse);

        return ApiResponse.createResponse(vo);
    }

    @ApiOperation(value = "设置消息，转调SendData接口", notes = "每次设置一条消息。后台接口自动生成Ticket并返回给前端")
    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
    public ApiResponse<SendMsgResponseVO> sendMessage(
            @RequestBody SendMsgRequestVO request,
            @ApiIgnore final HttpServletRequest httpServletRequest) throws IOException {

        SendMsgXmlRequest xmlRequest = SendMsgXmlRequest.fromVO(request);

        WebServiceRequestIp.set(httpServletRequest.getRemoteHost());
        String result = dataLinkerServicesSoap.sendData(Utils.obj2Xml(xmlRequest));
        SendMsgXmlResponse xmlResponse = Utils.xml2Object(result, SendMsgXmlResponse.class);

        SendMsgResponseVO responseVO = SendMsgResponseVO.fromXmlResponse(xmlResponse).withTickets(xmlRequest);

        return ApiResponse.createResponse(responseVO);
    }

    @ApiOperation(value = "设置标签值，转调SendData接口", notes = "")
    @RequestMapping(value = "/sendTag", method = RequestMethod.POST)
    public Callable<ApiResponse<WebServiceResponseVO>> sendTag(
            @RequestBody final SendTagRequestVO request,
            @ApiIgnore final HttpServletRequest httpServletRequest) throws IOException {

        return new Callable<ApiResponse<WebServiceResponseVO>>() {
            public ApiResponse<WebServiceResponseVO> call() throws Exception {
                SendTagXmlRequest xmlRequest = SendTagXmlRequest.fromVO(request);

                WebServiceRequestIp.set(httpServletRequest.getRemoteHost());
                String result = dataLinkerServicesSoap.sendData(Utils.obj2Xml(xmlRequest));

                WebServiceXmlResponse xmlResponse = Utils.xml2Object(result, WebServiceXmlResponse.class);

                WebServiceResponseVO responseVO = new WebServiceResponseVO();
                responseVO.parseSystemResult(xmlResponse);
                return ApiResponse.createResponse(responseVO);
            }
        };
    }
    
    @ApiOperation(value = "设置标签值，转调SendData接口", notes = "")
    @RequestMapping(value = "/sendTagNew", method = RequestMethod.POST)
    public ResultData sendTagnew(
            @RequestBody final SendTagRequestVO request,
            @ApiIgnore final HttpServletRequest httpServletRequest){
    			try {
    				SendTagXmlRequest xmlRequest = SendTagXmlRequest.fromVO(request);
        	        WebServiceRequestIp.set(httpServletRequest.getRemoteHost());
        	        String result = dataLinkerServicesSoap.sendData(Utils.obj2Xml(xmlRequest));
        	        return ResultData.ResultDataSuccessSelf("操作数据成功", null);
    			}catch(Exception e) {
    				e.printStackTrace();
    				  return ResultData.ResultDataFaultSelf("操作失败,失败原因" + e.getMessage(), null);
    			}
//        return new Callable<ApiResponse<WebServiceResponseVO>>() {
//            public ApiResponse<WebServiceResponseVO> call() throws Exception {
//                SendTagXmlRequest xmlRequest = SendTagXmlRequest.fromVO(request);
//
//                WebServiceRequestIp.set(httpServletRequest.getRemoteHost());
//                String result = dataLinkerServicesSoap.sendData(Utils.obj2Xml(xmlRequest));
//
//                WebServiceXmlResponse xmlResponse = Utils.xml2Object(result, WebServiceXmlResponse.class);
//
//                WebServiceResponseVO responseVO = new WebServiceResponseVO();
//                responseVO.parseSystemResult(xmlResponse);
//                return ApiResponse.createResponse(responseVO);
//            }
//        };
    	
    }
}
