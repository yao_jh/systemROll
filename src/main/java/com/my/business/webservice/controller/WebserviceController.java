package com.my.business.webservice.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.webservice.entity.WebserviceEntity;
import com.my.business.webservice.util.WebserviceUtil;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("Mywebservice")
public class WebserviceController {

    @CrossOrigin
    @RequestMapping(value = {"/getData"}, method = RequestMethod.POST)
    public ResultData getDataByWebService(@RequestBody String data) {
        try {
            WebserviceEntity entity = JSON.parseObject(data, WebserviceEntity.class);
            String object = WebserviceUtil.getWebservice(entity.getUrl(), entity.getMethod());
            return ResultData.ResultDataSuccessSelf("访问成功", object);
        } catch (Exception e) {
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

}
