package com.my.business.webservice.vo.webservice.xml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.my.business.webservice.vo.webservice.HmiDataRequestVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * HmiDataXmlRequest.java
 * Created by luckzj on 12/9/17.
 */
@JacksonXmlRootElement(localName = "ReqDat")
@JsonIgnoreProperties(ignoreUnknown = true)
public class HmiDataXmlRequest {
    private static Logger logger = LoggerFactory.getLogger(HmiDataXmlRequest.class);
    @JacksonXmlProperty(localName = "Tag")
    @JacksonXmlElementWrapper(useWrapping = true, localName = "TagList")
    private List<TagXmlRequest> tags;
    @JacksonXmlProperty(localName = "Msg")
    @JacksonXmlElementWrapper(useWrapping = true, localName = "MsgList")
    private List<MsgXmlRequest> msgs;

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        HmiDataXmlRequest.logger = logger;
    }

    public static HmiDataXmlRequest fromVO(HmiDataRequestVO requestVO) {
        HmiDataXmlRequest request = new HmiDataXmlRequest();

        if (requestVO.getMsgs() != null && requestVO.getMsgs().size() > 0) {
            request.msgs = new ArrayList<MsgXmlRequest>();
            for (HmiDataRequestVO.MsgRequestVO msg : requestVO.getMsgs()) {
                MsgXmlRequest msgXmlRequest = new MsgXmlRequest();
                msgXmlRequest.id = msg.getId();
                msgXmlRequest.ticket = msg.getTicket();
                request.msgs.add(msgXmlRequest);
            }
        }

        if (requestVO.getTags() != null && requestVO.getTags().size() > 0) {
            request.tags = new ArrayList<TagXmlRequest>();
            for (HmiDataRequestVO.TagRequestVO tag : requestVO.getTags()) {
                TagXmlRequest tagXmlRequest = new TagXmlRequest();
                tagXmlRequest.name = tag.getName();
                tagXmlRequest.ts = tag.getTs();
                request.tags.add(tagXmlRequest);
            }
        }

        return request;
    }

    public List<TagXmlRequest> getTags() {
        return tags;
    }

    public void setTags(List<TagXmlRequest> tags) {
        this.tags = tags;
    }

    public List<MsgXmlRequest> getMsgs() {
        return msgs;
    }

    public void setMsgs(List<MsgXmlRequest> msgs) {
        this.msgs = msgs;
    }

    @JacksonXmlRootElement(localName = "Tag")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class TagXmlRequest {
        @JacksonXmlProperty(localName = "Name", isAttribute = true)
        private String name;

        @JacksonXmlProperty(localName = "TS", isAttribute = true)
        private String ts;


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTs() {
            return ts;
        }

        public void setTs(String ts) {
            this.ts = ts;
        }
    }

    @JacksonXmlRootElement(localName = "Msg")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class MsgXmlRequest {
        @JacksonXmlProperty(localName = "Id", isAttribute = true)
        private String id;

        @JacksonXmlProperty(localName = "Ticket", isAttribute = true)
        private String ticket;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTicket() {
            return ticket;
        }

        public void setTicket(String ticket) {
            this.ticket = ticket;
        }
    }
}
