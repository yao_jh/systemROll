package com.my.business.webservice.vo.webservice;

import java.util.Map;

/**
 * SendTagRequestVO.java
 * Created by luckzj on 12/9/17.
 */
public class SendTagRequestVO {
    private Tag[] tagList;

    public Tag[] getTagList() {
        return tagList;
    }

    public void setTagList(Tag[] tagList) {
        this.tagList = tagList;
    }

    public static class Tag {
        private String name;
        private String value;
        private Map<String, String>[] dataList;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Map<String, String>[] getDataList() {
            return dataList;
        }

        public void setDataList(Map<String, String>[] dataList) {
            this.dataList = dataList;
        }
    }
}
