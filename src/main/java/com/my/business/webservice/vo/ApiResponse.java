package com.my.business.webservice.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ApiResponse
 * Created by luckzj on 10/23/17.
 */
@ApiModel(description = "接口返回")
public class ApiResponse<T> {
    @ApiModelProperty(value = "错误码")
    private Integer code;

    @ApiModelProperty(value = "错误信息")
    private String msg;

    @ApiModelProperty(value = "额外错误信息")
    private String extraMsg;

    @ApiModelProperty(value = "返回数据，如果返回的是数组，则此项为空")
    private T data;

//    @ApiModelProperty(value = "返回数据，如果返回的不是数组，则此项为空")
//    private T[] list;

    /**
     * Initialize a new Instance of ApiResponse
     */
    ApiResponse() {
        setErrorCode(ErrorCode.SUCCESS);
    }

    /**
     * Create a ApiResponse with no data
     */
    public static ApiResponse createResponse() {
        return new ApiResponse();
    }

    /**
     * Create Response with error code and extra message
     *
     * @param code
     * @param extMsg
     * @return
     */
    public static ApiResponse createResponse(ErrorCode code, String extMsg) {
        return createResponse(code, extMsg, (Object) null);
    }

    /**
     * Create Response with extra field and object
     *
     * @return New Instance of ApiResponse Object
     */
    public static <T> ApiResponse createResponse(T data) {
        return createResponse(ErrorCode.SUCCESS, "", data);
    }

    /**
     * Create new Instance of ApiResponse
     *
     * @param code   Error Code
     * @param extMsg Extra Message
     * @param data   Dta
     * @return
     */
    public static <T> ApiResponse createResponse(ErrorCode code, String extMsg, T data) {
        ApiResponse<T> response = new ApiResponse<T>();
        response.setErrorCode(code);
        response.setExtraMsg(extMsg);
        response.setData(data);
        return response;
    }

    /**
     * Set Error Code
     *
     * @param errorCode
     * @return
     */
    public ApiResponse setErrorCode(ErrorCode errorCode) {
        this.code = errorCode.getCode();
        this.msg = errorCode.getMessage();
        this.extraMsg = this.msg;
        return this;
    }

//    /**
//     * Create new Instance of ApiResponse
//     * @param code Error Code
//     * @param extMsg Extra Message
//     * @param list list
//     * @return
//     */
//    public static <T> ApiResponse createResponse(ErrorCode code, String extMsg, T[] list) {
//        ApiResponse<T> response = new ApiResponse<T>();
//        response.setErrorCode(code);
//        response.setExtraMsg(extMsg);
//        response.setList(list);
//        return response;
//    }

    /**
     * Get Json String
     *
     * @return
     */
    public String toJsonString() {
        FastJsonConfig config = new FastJsonConfig();
        config.setDateFormat("yyyy-MM-dd HH:mm:ss");
        return JSON.toJSONString(this, config.getSerializeConfig());
    }

    /**
     * get code
     *
     * @return
     */
    public Integer getCode() {
        return code;
    }

    /**
     * Set code
     *
     * @param code
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * Get Message
     *
     * @return
     */
    public String getMsg() {
        return msg;
    }

    /**
     * Set Message
     *
     * @param msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * Get Extra Message
     *
     * @return
     */
    public String getExtraMsg() {
        return extraMsg;
    }

    /**
     * Set Extra Message
     *
     * @param msg
     * @return
     */
    public ApiResponse setExtraMsg(String msg) {
        this.extraMsg = msg;
        return this;
    }

    /**
     * Get Data
     *
     * @return
     */
    public T getData() {
        return data;
    }

    /**
     * Set Data
     *
     * @param data
     */
    public void setData(T data) {
        this.data = data;
    }

//    /**
//     * Get List
//     * @return
//     */
//    public T[] getList() {
//        return list;
//    }
//
//    /**
//     * Set List
//     * @param list
//     */
//    public void setList(T[] list) {
//        this.list = list;
//    }
}
