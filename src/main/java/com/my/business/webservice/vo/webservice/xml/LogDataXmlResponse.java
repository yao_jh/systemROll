package com.my.business.webservice.vo.webservice.xml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

/**
 * LogDataXmlResponse.java
 * Created by luckzj on 12/12/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class LogDataXmlResponse extends WebServiceXmlResponse {

    @JacksonXmlProperty(localName = "LogList")
    private XmlLogList logList;

    public XmlLogList getLogList() {
        return logList;
    }

    public void setLogList(XmlLogList logList) {
        this.logList = logList;
    }

    @JacksonXmlRootElement(localName = "LogList")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class XmlLogList {
        @JacksonXmlProperty(localName = "Log")
        @JacksonXmlElementWrapper(useWrapping = false)
        private XmlLog[] logs;

        public XmlLog[] getLogs() {
            return logs;
        }

        public void setLogs(XmlLog[] logs) {
            this.logs = logs;
        }
    }

    @JacksonXmlRootElement(localName = "Log")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class XmlLog {
        @JacksonXmlProperty(localName = "Time")
        private String time;

        @JacksonXmlProperty(localName = "Level")
        private String level;

        @JacksonXmlProperty(localName = "No")
        private String no;

        @JacksonXmlProperty(localName = "From")
        private String from;

        @JacksonXmlProperty(localName = "Text")
        private String text;

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public String getNo() {
            return no;
        }

        public void setNo(String no) {
            this.no = no;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
}
