package com.my.business.webservice.vo.webservice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.my.business.webservice.vo.webservice.xml.HmiDataXmlResponse;
import com.my.business.webservice.vo.webservice.xml.WebServiceXmlResponse;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * WebServiceResponseVO.java
 * Created by luckzj on 12/7/17.
 */
public class WebServiceResponseVO {
    private System system;

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public void parseSystemResult(WebServiceXmlResponse xmlResponse) {
        if (xmlResponse == null) {
            this.system = null;
            return;
        }

        this.system = System.fromXmlResponse(xmlResponse.getSystem());
    }

    @ApiModel(description = "系统结果")
    public static class System {
        @ApiModelProperty(value = "错误状态")
        private Status status;

        @ApiModelProperty(value = "错误信息")
        private Error error;

        public static System fromXmlResponse(WebServiceXmlResponse.System xmlSystem) {
            if (xmlSystem == null) {
                return null;
            }

            System system = new System();
            system.status = Status.fromXmlResponse(xmlSystem.getStatus());
            system.error = Error.fromXmlResponse(xmlSystem.getError());
            return system;
        }


        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

        public Error getError() {
            return error;
        }

        public void setError(Error error) {
            this.error = error;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @ApiModel(description = "错误状态")
    public static class Status {
        @JacksonXmlProperty(localName = "ConnectToTagServer", isAttribute = true)
        @ApiModelProperty(value = "是否连接上服务器")
        private String connectToTagServer;

        public static Status fromXmlResponse(HmiDataXmlResponse.Status xmlStatus) {
            if (xmlStatus == null) {
                return null;
            }

            Status status = new Status();
            status.connectToTagServer = xmlStatus.getConnectToTagServer();
            return status;
        }

        public String getConnectToTagServer() {
            return connectToTagServer;
        }

        public void setConnectToTagServer(String connectToTagServer) {
            this.connectToTagServer = connectToTagServer;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @ApiModel(description = "错误信息")
    public static class Error {
        @JacksonXmlProperty(localName = "Source", isAttribute = true)
        @ApiModelProperty(value = "错误来源")
        private String source;
        @JacksonXmlProperty(localName = "Info", isAttribute = true)
        @ApiModelProperty(value = "错误描述")
        private String info;

        public static Error fromXmlResponse(HmiDataXmlResponse.Error xmlError) {
            if (xmlError == null) {
                return null;
            }

            Error error = new Error();
            error.source = xmlError.getSource();
            error.info = xmlError.getInfo();
            return error;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }
    }
}
