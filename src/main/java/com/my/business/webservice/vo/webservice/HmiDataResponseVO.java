package com.my.business.webservice.vo.webservice;


import com.my.business.webservice.util.Utils;
import com.my.business.webservice.vo.webservice.xml.HmiDataXmlResponse;

import java.util.HashMap;
import java.util.Map;


/**
 * HmiDataResponseVO.java
 * Created by luckzj on 12/6/17.
 */
public class HmiDataResponseVO {
    private TagValue[] tagList;
    private MsgValue[] msgList;

    public static HmiDataResponseVO fromXmlResponse(HmiDataXmlResponse xmlResponse) {
        if (xmlResponse == null) return null;

        HmiDataResponseVO vo = new HmiDataResponseVO();

        vo.msgList = MsgValue.fromXmlResponse(xmlResponse.getMsgList());
        vo.tagList = TagValue.fromXmlResponse(xmlResponse.getTagList());

        return vo;
    }

    public TagValue[] getTagList() {
        return tagList;
    }

    public void setTagList(TagValue[] tagList) {
        this.tagList = tagList;
    }

    public MsgValue[] getMsgList() {
        return msgList;
    }

    public void setMsgList(MsgValue[] msgList) {
        this.msgList = msgList;
    }

    public static class TagValue {
        private String name;
        private String ts;
        private String value;
        private Map<String, String>[] dataList;

        public static TagValue[] fromXmlResponse(HmiDataXmlResponse.TagValue[] xmlTags) {
            if (xmlTags == null || xmlTags.length == 0) {
                return null;
            }

            TagValue[] tagList = new TagValue[xmlTags.length];
            for (int i = 0; i < xmlTags.length; i++) {
                tagList[i] = fromXmlResponse(xmlTags[i]);
            }

            return tagList;
        }

        public static TagValue fromXmlResponse(HmiDataXmlResponse.TagValue xmlTag) {
            if (xmlTag == null) {
                return null;
            }

            TagValue tagValue = new TagValue();
            tagValue.name = xmlTag.getName();
            tagValue.ts = xmlTag.getTs();
            tagValue.value = xmlTag.getValue();

            if (xmlTag.getDataList() != null && xmlTag.getDataList().length > 0) {
                tagValue.dataList = new Map[xmlTag.getDataList().length];
                for (int i = 0; i < tagValue.dataList.length; i++) {
                    tagValue.dataList[i] = new HashMap<String, String>();
                    tagValue.dataList[i].putAll(xmlTag.getDataList()[i]);
                }
            }
            return tagValue;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTs() {
            return ts;
        }

        public void setTs(String ts) {
            this.ts = ts;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Map<String, String>[] getDataList() {
            return dataList;
        }

        public void setDataList(Map<String, String>[] dataList) {
            this.dataList = dataList;
        }
    }

    public static class MsgValue {
        private String id;
        private String ticket;
        private Map<String, String>[] dataList;

        public static MsgValue[] fromXmlResponse(HmiDataXmlResponse.MsgValue[] xmlMsgs) {
            if (xmlMsgs == null || xmlMsgs.length == 0) {
                return null;
            }

            MsgValue[] msgList = new MsgValue[xmlMsgs.length];
            for (int i = 0; i < msgList.length; i++) {
                msgList[i] = fromXmlResponse(xmlMsgs[i]);
            }

            return msgList;
        }

        public static MsgValue fromXmlResponse(HmiDataXmlResponse.MsgValue xmlMsg) {
            if (xmlMsg == null) {
                return null;
            }

            MsgValue msgValue = new MsgValue();
            msgValue.id = xmlMsg.getId();
            msgValue.ticket = xmlMsg.getTicket();

            msgValue.dataList = Utils.cloneMapList(xmlMsg.getDataList());
            return msgValue;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTicket() {
            return ticket;
        }

        public void setTicket(String ticket) {
            this.ticket = ticket;
        }

        public Map<String, String>[] getDataList() {
            return dataList;
        }

        public void setDataList(Map<String, String>[] dataList) {
            this.dataList = dataList;
        }
    }
}
