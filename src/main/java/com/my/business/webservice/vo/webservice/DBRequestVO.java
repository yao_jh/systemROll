package com.my.business.webservice.vo.webservice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * WebServiceDBRequestVO.java
 * Created by luckzj on 12/2/17.
 */
@ApiModel(value = "WebService数据库请求")
public class DBRequestVO {
    @ApiModelProperty(value = "连接名称")
    private String connName;

    @ApiModelProperty(value = "连接类型")
    private Integer cmdType;

    @ApiModelProperty(value = "SQL命令")
    private String cmdText;

    @ApiModelProperty(value = "SQL命令参数")
    private String paramXml;

    @ApiModelProperty(value = "是否查询")
    private Boolean queryFlag;

    @ApiModelProperty(value = "返回结果ID")
    private String resultId;

    public String getConnName() {
        return connName;
    }

    public void setConnName(String connName) {
        this.connName = connName;
    }

    public Integer getCmdType() {
        return cmdType;
    }

    public void setCmdType(Integer cmdType) {
        this.cmdType = cmdType;
    }

    public String getCmdText() {
        return cmdText;
    }

    public void setCmdText(String cmdText) {
        this.cmdText = cmdText;
    }

    public String getParamXml() {
        return paramXml;
    }

    public void setParamXml(String paramXml) {
        this.paramXml = paramXml;
    }

    public Boolean getQueryFlag() {
        return queryFlag;
    }

    public void setQueryFlag(Boolean queryFlag) {
        this.queryFlag = queryFlag;
    }

    public String getResultId() {
        return resultId;
    }

    public void setResultId(String resultId) {
        this.resultId = resultId;
    }
}
