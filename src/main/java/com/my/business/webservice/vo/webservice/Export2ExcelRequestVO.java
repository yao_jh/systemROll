package com.my.business.webservice.vo.webservice;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * WebServiceExport2ExcelRequestVO.java
 * Created by luckzj on 12/2/17.
 */
@JacksonXmlRootElement(localName = "TagList")
public class Export2ExcelRequestVO {
    private List<ExportTagInfo> tagList;
    private String template;

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }


    public List<ExportTagInfo> getTagList() {
        return tagList;
    }

    public void setTagList(List<ExportTagInfo> tagList) {
        this.tagList = tagList;
    }

    public static class ExportTagInfo {
        private String name;
        private String value;
        private List<LinkedHashMap> dataList;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public List<LinkedHashMap> getDataList() {
            return dataList;
        }

        public void setDataList(List<LinkedHashMap> dataList) {
            this.dataList = dataList;
        }
    }
}
