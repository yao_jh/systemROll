package com.my.business.webservice.vo.webservice;

import com.my.business.webservice.vo.webservice.xml.LogDataXmlResponse;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * LogDataResponseVO.java
 * Created by luckzj on 12/12/17.
 */
@ApiModel(value = "报警信息接口返回")
public class LogDataResponseVO {

    @ApiModelProperty(value = "报警时间")
    private String time;

    @ApiModelProperty(value = "报警级别")
    private String level;

    @ApiModelProperty(value = "报警号")
    private String no;

    @ApiModelProperty(value = "报警来源")
    private String from;

    @ApiModelProperty(value = "报警信息")
    private String text;

    public static LogDataResponseVO fromXmlResponse(LogDataXmlResponse.XmlLog xmlLog) {
        if (xmlLog == null) {
            return null;
        }

        LogDataResponseVO logData = new LogDataResponseVO();
        logData.from = xmlLog.getFrom();
        logData.time = xmlLog.getTime();
        logData.level = xmlLog.getLevel();
        logData.no = xmlLog.getNo();
        logData.text = xmlLog.getText();
        return logData;
    }

    public static LogDataResponseVO[] fromXmlResponse(LogDataXmlResponse xmlResponse) {
        List<LogDataResponseVO> list = new ArrayList<LogDataResponseVO>();

        if (xmlResponse.getLogList() != null
                && xmlResponse.getLogList().getLogs() != null
                && xmlResponse.getLogList().getLogs().length > 0) {
            LogDataXmlResponse.XmlLog[] xmlLogList = xmlResponse.getLogList().getLogs();

            for (int i = 0; i < xmlLogList.length; i++) {
                list.add(fromXmlResponse(xmlLogList[i]));
            }
        }

        return list.toArray(new LogDataResponseVO[0]);
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
