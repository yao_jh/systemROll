package com.my.business.webservice.vo.webservice;

import com.my.business.webservice.vo.webservice.xml.LoggerServerXmlResponse;

/**
 * LoggerServerResponseVO.java
 * Created by luckzj on 12/21/17.
 */
public class LoggerServerResponseVO {
    private String ip;
    private String port;

    public static LoggerServerResponseVO fromXmlResponse(LoggerServerXmlResponse xmlResponse) {
        if (xmlResponse.getLoggerServer() == null || xmlResponse.getLoggerServer().getConfig() == null) {
            return null;
        }

        LoggerServerXmlResponse.Config config = xmlResponse.getLoggerServer().getConfig();
        LoggerServerResponseVO vo = new LoggerServerResponseVO();
        vo.setIp(config.getIPAddr());
        vo.setPort(config.getPort());
        return vo;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
