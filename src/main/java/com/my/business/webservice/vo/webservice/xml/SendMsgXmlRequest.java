package com.my.business.webservice.vo.webservice.xml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.my.business.webservice.util.MapIndexedListSerializer;
import com.my.business.webservice.util.Utils;
import com.my.business.webservice.vo.webservice.SendMsgRequestVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * SendDataXmlRequest.java
 * Created by luckzj on 12/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = false)
@JacksonXmlRootElement(localName = "MsgList")
public class SendMsgXmlRequest {
    private static Logger logger = LoggerFactory.getLogger(SendMsgXmlRequest.class);
    @JacksonXmlProperty(localName = "Msg")
    @JacksonXmlElementWrapper(useWrapping = false)
    private Msg[] msgList;

    public static SendMsgXmlRequest fromVO(SendMsgRequestVO vo) {
        if (vo == null) {
            return null;
        }

        SendMsgXmlRequest request = new SendMsgXmlRequest();
        request.msgList = new Msg[vo.getMsgList().length];
        for (int i = 0; i < request.msgList.length; i++) {
            request.msgList[i] = new Msg();
            request.msgList[i].id = vo.getMsgList()[i].getId();
            request.msgList[i].timeout = vo.getMsgList()[i].getTimeout();
            request.msgList[i].reply = vo.getMsgList()[i].getReply() != null ? 1 : 0;
            request.msgList[i].ticket = UUID.randomUUID().toString();

            request.msgList[i].data = Utils.cloneMapList(vo.getMsgList()[i].getData());
        }

        return request;
    }

    public static void main(String[] args) throws JsonProcessingException {

        List<SendMsgRequestVO.Msg> msgListVO = new ArrayList<SendMsgRequestVO.Msg>();


        SendMsgRequestVO.Msg msg = new SendMsgRequestVO.Msg();
        msg.setId("SendID");
        msg.setTimeout(10);
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("FILED1", "11");
        data.put("FIELD2", "22");
        Map[] maps = new Map[2];
        maps[0] = data;

        data = new HashMap<String, String>();
        data.put("FILED1", "11");
        data.put("FIELD2", "22");
        maps[1] = data;

        msg.setData(maps);
        msgListVO.add(msg);

        msg = new SendMsgRequestVO.Msg();
        msg.setId("SendID");
        msg.setTimeout(10);
        data = new HashMap<String, String>();
        data.put("FILED1", "11");
        data.put("FIELD2", "22");
        maps = new Map[2];
        maps[0] = data;

        data = new HashMap<String, String>();
        data.put("FILED1", "11");
        data.put("FIELD2", "22");
        maps[1] = data;
        msgListVO.add(msg);

        msg.setData(maps);


        SendMsgRequestVO requestVO = new SendMsgRequestVO();
        requestVO.setMsgList(msgListVO.toArray(new SendMsgRequestVO.Msg[0]));

        SendMsgXmlRequest request = fromVO(requestVO);

        String xml = Utils.obj2Xml(request);

        logger.error(xml);
    }

    public Msg[] getMsgList() {
        return msgList;
    }

    public void setMsgList(Msg[] msgList) {
        this.msgList = msgList;
    }

    @JacksonXmlRootElement(localName = "Msg")
    public static class Msg {
        @JacksonXmlProperty(localName = "Id", isAttribute = true)
        private String id;

        @JacksonXmlProperty(localName = "Ticket", isAttribute = true)
        private String ticket;

        @JacksonXmlProperty(localName = "Timeout", isAttribute = true)
        private Integer timeout;

        @JacksonXmlProperty(localName = "Reply", isAttribute = true)
        private Integer reply;

        @JacksonXmlElementWrapper(useWrapping = false)
        @JacksonXmlProperty(localName = "Dat", isAttribute = true)
        @JsonSerialize(using = MapIndexedListSerializer.class)
        private Map<String, String>[] data;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTicket() {
            return ticket;
        }

        public void setTicket(String ticket) {
            this.ticket = ticket;
        }

        public Integer getTimeout() {
            return timeout;
        }

        public void setTimeout(Integer timeout) {
            this.timeout = timeout;
        }

        public Integer getReply() {
            return reply;
        }

        public void setReply(Integer reply) {
            this.reply = reply;
        }

        public Map<String, String>[] getData() {
            return data;
        }

        public void setData(Map<String, String>[] data) {
            this.data = data;
        }
    }

}
