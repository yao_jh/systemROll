package com.my.business.webservice.vo.webservice.xml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * WebServiceDataXmlResponse.java
 * Created by luckzj on 12/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WebServiceXmlResponse {
    @JacksonXmlProperty(localName = "System")
    private System system;

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class System {
        @JacksonXmlProperty(localName = "Status")
        private Status status;
        @JacksonXmlProperty(localName = "Error")
        private Error error;

        public System() {
        }

        public System(String empty) {
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

        public Error getError() {
            return error;
        }

        public void setError(Error error) {
            this.error = error;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Status {
        @JacksonXmlProperty(localName = "ConnectToTagServer", isAttribute = true)
        private String connectToTagServer;

        public Status() {
        }

        public Status(String empty) {
        }

        public String getConnectToTagServer() {
            return connectToTagServer;
        }

        public void setConnectToTagServer(String connectToTagServer) {
            this.connectToTagServer = connectToTagServer;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Error {
        @JacksonXmlProperty(localName = "Source", isAttribute = true)
        private String source;
        @JacksonXmlProperty(localName = "Info", isAttribute = true)
        private String info;

        public Error() {
        }

        public Error(String empty) {
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }
    }
}
