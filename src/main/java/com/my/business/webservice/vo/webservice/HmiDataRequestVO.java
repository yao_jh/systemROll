package com.my.business.webservice.vo.webservice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * GetHmiDataRequestVO.java
 * Created by luckzj on 12/6/17.
 */
@ApiModel("用于获取Hmi数据的请求")
public class HmiDataRequestVO {
    @ApiModelProperty(value = "需要获取的标签")
    private List<TagRequestVO> tags;
    @ApiModelProperty(value = "需要获取的消息")
    private List<MsgRequestVO> msgs;

    public List<TagRequestVO> getTags() {
        return tags;
    }

    public void setTags(List<TagRequestVO> tags) {
        this.tags = tags;
    }

    public List<MsgRequestVO> getMsgs() {
        return msgs;
    }

    public void setMsgs(List<MsgRequestVO> msgs) {
        this.msgs = msgs;
    }

    public static class TagRequestVO {
        @ApiModelProperty(value = "标签名称")
        private String name;

        @ApiModelProperty(value = "时间戳")
        private String ts;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTs() {
            return ts;
        }

        public void setTs(String ts) {
            this.ts = ts;
        }
    }

    public static class MsgRequestVO {
        private String id;
        private String ticket;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTicket() {
            return ticket;
        }

        public void setTicket(String ticket) {
            this.ticket = ticket;
        }
    }
}

