package com.my.business.webservice.vo.webservice;

import java.util.Map;

/**
 * SendDataRequestVO.java
 * Created by luckzj on 12/7/17.
 */
public class SendMsgRequestVO {
    private Msg[] msgList;

    public Msg[] getMsgList() {
        return msgList;
    }

    public void setMsgList(Msg[] msgList) {
        this.msgList = msgList;
    }

    public static class Msg {
        private String id;
        private Integer timeout;
        private Boolean reply;
        private Map[] data;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Integer getTimeout() {
            return timeout;
        }

        public void setTimeout(Integer timeout) {
            this.timeout = timeout;
        }

        public Boolean getReply() {
            return reply;
        }

        public void setReply(Boolean reply) {
            this.reply = reply;
        }

        public Map[] getData() {
            return data;
        }

        public void setData(Map[] data) {
            this.data = data;
        }
    }
}
