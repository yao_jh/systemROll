package com.my.business.webservice.vo.webservice.xml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

/**
 * LoggerServerXmlResponse.java
 * Created by luckzj on 12/21/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoggerServerXmlResponse extends WebServiceXmlResponse {
    @JacksonXmlProperty(localName = "LoggerServer")
    private LoggerServer loggerServer;

    public LoggerServer getLoggerServer() {
        return loggerServer;
    }

    public void setLoggerServer(LoggerServer loggerServer) {
        this.loggerServer = loggerServer;
    }

    @JacksonXmlRootElement(localName = "LoggerServer")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class LoggerServer {
        @JacksonXmlProperty(localName = "Config")
        private Config config;

        public Config getConfig() {
            return config;
        }

        public void setConfig(Config config) {
            this.config = config;
        }
    }

    @JacksonXmlRootElement(localName = "Config")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Config {
        @JacksonXmlProperty(localName = "IPAddr", isAttribute = true)
        private String IPAddr;

        @JacksonXmlProperty(localName = "Port", isAttribute = true)
        private String Port;

        public String getIPAddr() {
            return IPAddr;
        }

        public void setIPAddr(String IPAddr) {
            this.IPAddr = IPAddr;
        }

        public String getPort() {
            return Port;
        }

        public void setPort(String port) {
            Port = port;
        }
    }
}
