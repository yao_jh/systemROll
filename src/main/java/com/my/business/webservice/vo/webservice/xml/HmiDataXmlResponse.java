package com.my.business.webservice.vo.webservice.xml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.my.business.webservice.util.XmlWhitespaceModule;
import com.my.business.webservice.vo.webservice.HmiDataResponseVO;

import java.util.Map;

/**
 * HmiDataXmlResponse.java
 * Created by luckzj on 07/12/2017
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HmiDataXmlResponse extends WebServiceXmlResponse {

    @JacksonXmlProperty(localName = "TagList")
    @JacksonXmlElementWrapper(useWrapping = true)
    private TagValue[] tagList;
    @JacksonXmlProperty(localName = "MsgList")
    @JacksonXmlElementWrapper(useWrapping = true)
    private MsgValue[] msgList;

    public static void main(String[] args) throws Exception {
        String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<string xmlns=\"http://tempuri.org/\">\n" +
                "<TagList>\n" +
                "<Tag Name=\"CURPLANNO\" TS=\"131567736606895810\" Value=\"abc123\"/>\n" +
                "<Tag Name=\"PDIData\" TS=\"130927309526564631\" Value=\"843\"/>\n" +
                "<Tag Name=\"FCE_PARAM\" TS=\"130915124530316669\">\n" +
                "  <Dat DISCH_TIME=\"60\" CHARGE_DIS_MIN=\"4280\" EXIT_DIS_MAX=\"60700\" FCEZONE_NUM=\"5\" CHARGE_DIS_MAX=\"6025\" FCE_LEN=\"60700\" DIS_LASER_DOOR=\"1700\"/>\n" +
                "  <Dat DISCH_TIME=\"41\" CHARGE_DIS_MIN=\"4280\" EXIT_DIS_MAX=\"60700\" FCEZONE_NUM=\"5\" CHARGE_DIS_MAX=\"6025\" FCE_LEN=\"60700\" DIS_LASER_DOOR=\"1700\"/>\n" +
                "  <Dat DISCH_TIME=\"41\" CHARGE_DIS_MIN=\"4280\" EXIT_DIS_MAX=\"60700\" FCEZONE_NUM=\"5\" CHARGE_DIS_MAX=\"6025\" FCE_LEN=\"60700\" DIS_LASER_DOOR=\"1700\"/>\n" +
                "</Tag>\n" +
                "</TagList>\n" +
                "<MsgList>\n" +
                "<Msg Id=\"a\" Ticket=\"ta\">\n" +
                "  <Dat x=\"x1\" y=\"y1\"/>\n" +
                "  <Dat x=\"x2\" y=\"y2\"/>\n" +
                "</Msg>\n" +
                "<Msg Id=\"b\" Ticket=\"tb\">\n" +
                "  <Dat x=\"x1\" y=\"y1\"/>\n" +
                "  <Dat x=\"x2\" y=\"y2\"/>\n" +
                "</Msg>\n" +
                "</MsgList>" +
                "</string>";

        String str2 = "<string xmlns=\"http://tempuri.org/\"><System><Status ConnectToTagServer = \"false\"/></System></string>";

        String str3 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<string xmlns=\"http://tempuri.org/\"><System><Error Source=\"GetHmiData-1\" Info=\"缺少根元素。\"/></System></string>";
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.registerModule(new XmlWhitespaceModule());
        ;
        HmiDataXmlResponse response = xmlMapper.readValue(str, HmiDataXmlResponse.class);

        HmiDataResponseVO responseVO = HmiDataResponseVO.fromXmlResponse(response);
    }

    public TagValue[] getTagList() {
        return tagList;
    }

    public void setTagList(TagValue[] tagList) {
        this.tagList = tagList;
    }

    public MsgValue[] getMsgList() {
        return msgList;
    }

    public void setMsgList(MsgValue[] msgList) {
        this.msgList = msgList;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JacksonXmlRootElement(localName = "Tag")
    public static class TagValue {
        @JacksonXmlProperty(localName = "Name", isAttribute = true)
        private String name;
        @JacksonXmlProperty(localName = "TS", isAttribute = true)
        private String ts;
        @JacksonXmlProperty(localName = "Value", isAttribute = true)
        private String value;
        @JacksonXmlProperty(localName = "Dat")
        @JacksonXmlElementWrapper(useWrapping = false)
        private Map<String, String>[] dataList;

        public TagValue() {
        }

        public TagValue(String empty) {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTs() {
            return ts;
        }

        public void setTs(String ts) {
            this.ts = ts;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Map<String, String>[] getDataList() {
            return dataList;
        }

        public void setDataList(Map<String, String>[] dataList) {
            this.dataList = dataList;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JacksonXmlRootElement(localName = "Msg")
    public static class MsgValue {
        @JacksonXmlProperty(localName = "Id", isAttribute = true)
        private String id;
        @JacksonXmlProperty(localName = "Ticket", isAttribute = true)
        private String ticket;
        @JacksonXmlProperty(localName = "Dat")
        @JacksonXmlElementWrapper(useWrapping = false)
        @JsonSerialize()
        private Map<String, String>[] dataList;

        public MsgValue() {
        }

        public MsgValue(String empty) {
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTicket() {
            return ticket;
        }

        public void setTicket(String ticket) {
            this.ticket = ticket;
        }

        public Map<String, String>[] getDataList() {
            return dataList;
        }

        public void setDataList(Map<String, String>[] dataList) {
            this.dataList = dataList;
        }
    }
}

