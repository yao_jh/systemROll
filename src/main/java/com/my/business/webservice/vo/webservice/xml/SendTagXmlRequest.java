package com.my.business.webservice.vo.webservice.xml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.my.business.webservice.util.MapIndexedListSerializer;
import com.my.business.webservice.util.Utils;
import com.my.business.webservice.vo.webservice.SendTagRequestVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * SendTagXmlRequest.java
 * Created by luckzj on 12/9/17.
 */
@JacksonXmlRootElement(localName = "TagList")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SendTagXmlRequest {
    private static Logger logger = LoggerFactory.getLogger(SendTagXmlRequest.class);
    @JacksonXmlProperty(localName = "Tag")
    @JacksonXmlElementWrapper(useWrapping = false)
    private XmlTag[] tags;

    public static SendTagXmlRequest fromVO(SendTagRequestVO requestVO) {
        SendTagXmlRequest request = new SendTagXmlRequest();

        if (requestVO.getTagList() != null && requestVO.getTagList().length > 0) {
            request.tags = new XmlTag[requestVO.getTagList().length];

            for (int i = 0; i < requestVO.getTagList().length; i++) {
                request.tags[i] = XmlTag.fromVO(requestVO.getTagList()[i]);
            }
        }

        return request;
    }

    public static void main(String[] args) throws JsonProcessingException {
        SendTagRequestVO request = new SendTagRequestVO();
        request.setTagList(new SendTagRequestVO.Tag[2]);

        SendTagRequestVO.Tag tag = new SendTagRequestVO.Tag();
        tag.setName("TAG1");
        tag.setValue("Tag1 value");
        tag.setDataList(new Map[2]);

        Map<String, String> map = new HashMap<String, String>();
        map.put("ATT1", "att value 1");
        map.put("ATT2", "att value 2");
        tag.getDataList()[0] = map;

        map = new HashMap<String, String>();
        map.put("ATT1", "att value 1");
        map.put("ATT2", "att value 2");
        tag.getDataList()[1] = map;

        request.getTagList()[0] = tag;

        tag = new SendTagRequestVO.Tag();
        tag.setName("TAG2");
        tag.setValue("Tag2 value");
        tag.setDataList(new Map[2]);

        map = new HashMap<String, String>();
        map.put("ATT3", "att value 1");
        map.put("ATT4", "att value 2");
        tag.getDataList()[0] = map;

        map = new HashMap<String, String>();
        map.put("ATT5", "att value 1");
        map.put("ATT6", "att value 2");
        tag.getDataList()[1] = map;

        request.getTagList()[1] = tag;

        SendTagXmlRequest xmlRequest = SendTagXmlRequest.fromVO(request);
        String xml = Utils.obj2Xml(xmlRequest);
        logger.debug(xml);

    }

    public XmlTag[] getTags() {
        return tags;
    }

    public void setTags(XmlTag[] tags) {
        this.tags = tags;
    }

    @JacksonXmlRootElement(localName = "Tag")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class XmlTag {
        @JacksonXmlProperty(localName = "Name", isAttribute = true)
        private String name;

        @JacksonXmlProperty(localName = "Value", isAttribute = true)
        private String value;

        @JacksonXmlProperty(localName = "Dat", isAttribute = true)
        @JacksonXmlElementWrapper(useWrapping = false)
        @JsonSerialize(using = MapIndexedListSerializer.class)
        private Map<String, String>[] dataList;

        public static XmlTag fromVO(SendTagRequestVO.Tag tagVO) {
            XmlTag tag = new XmlTag();
            tag.name = tagVO.getName();
            tag.value = tagVO.getValue();

            tag.dataList = Utils.cloneMapList(tagVO.getDataList());
            return tag;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Map<String, String>[] getDataList() {
            return dataList;
        }

        public void setDataList(Map<String, String>[] dataList) {
            this.dataList = dataList;
        }
    }
}
