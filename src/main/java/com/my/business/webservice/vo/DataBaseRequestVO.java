package com.my.business.webservice.vo;

/**
 * DataBaseRequestVO.java
 * Created by luckzj on 11/5/17.
 */
public class DataBaseRequestVO {
    private String sql;
    private String db;

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }
}
