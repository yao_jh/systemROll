package com.my.business.webservice.vo;

/**
 * ErrorCode.java
 * Created by luckzj on 10/23/17.
 */
public enum ErrorCode {
    SUCCESS(0, ""),
    ERR_DB_FAILED(-1, "数据库访问错误"),
    ERR_INVALID_PARAM(-2, "请求参数非法"),
    ERR_UNAUTHORISED(-3, "访问请求未授权"),
    ERR_INVALID_DOWNLOAD_TICKET(-5, "下载凭据无效"),
    ERR_WEBSERVICE_SERVER_NOT_CONNECTED(-1000, "WebService服务与标签服务未连接"),
    ERR_WEBSERVICE_SYSERROR(-1001, "WebSerice服务返回错误"),
    ERR_INTERNAL(-999, "程序异常");


    private Integer code;
    private String message;
    ErrorCode(Integer code, String msg) {
        this.code = code;
        this.message = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
