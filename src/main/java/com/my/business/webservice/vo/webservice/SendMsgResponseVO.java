package com.my.business.webservice.vo.webservice;

import com.my.business.webservice.vo.webservice.xml.SendMsgXmlRequest;
import com.my.business.webservice.vo.webservice.xml.SendMsgXmlResponse;

/**
 * SendDataResponseVO.java
 * Created by luckzj on 12/7/17.
 */
public class SendMsgResponseVO extends WebServiceResponseVO {
    private MsgResponse[] msgList;

    public static SendMsgResponseVO fromXmlResponse(SendMsgXmlResponse xmlResponse) {
        SendMsgResponseVO vo = new SendMsgResponseVO();
        vo.parseSystemResult(xmlResponse);
        return vo;
    }

    public MsgResponse[] getMsgList() {
        return msgList;
    }

    public void setMsgList(MsgResponse[] msgList) {
        this.msgList = msgList;
    }

    public SendMsgResponseVO withTickets(SendMsgXmlRequest xmlRequest) {
        if (xmlRequest.getMsgList() == null || xmlRequest.getMsgList().length == 0) {
            return this;
        }

        msgList = new MsgResponse[xmlRequest.getMsgList().length];
        for (int i = 0; i < msgList.length; i++) {
            msgList[i] = new MsgResponse();
            msgList[i].id = xmlRequest.getMsgList()[i].getId();
            msgList[i].ticket = xmlRequest.getMsgList()[i].getTicket();
            msgList[i].timeout = xmlRequest.getMsgList()[i].getTimeout();
            msgList[i].reply = xmlRequest.getMsgList()[i].getReply() == 1;
        }

        return this;
    }

    public static class MsgResponse {
        private String id;
        private String ticket;
        private Integer timeout;
        private Boolean reply;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTicket() {
            return ticket;
        }

        public void setTicket(String ticket) {
            this.ticket = ticket;
        }

        public Integer getTimeout() {
            return timeout;
        }

        public void setTimeout(Integer timeout) {
            this.timeout = timeout;
        }

        public Boolean getReply() {
            return reply;
        }

        public void setReply(Boolean reply) {
            this.reply = reply;
        }
    }
}
