package com.my.business.webservice.vo.webservice;

/**
 * WebServiceLogDataRequestVO.java
 * Created by luckzj on 12/2/17.
 */
public class LogDataRequestVO {
    private String startTime;
    private String endTime;
    private String keyword;
    private Integer count;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
