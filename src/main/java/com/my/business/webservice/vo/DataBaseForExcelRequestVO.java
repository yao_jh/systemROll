package com.my.business.webservice.vo;

/**
 * DataBaseForExcelRequestVO.java
 * Created by luckzj on 12/14/17.
 */
public class DataBaseForExcelRequestVO extends DataBaseRequestVO {
    private String template;

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
}
