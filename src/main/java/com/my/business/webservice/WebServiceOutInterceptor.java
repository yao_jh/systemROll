package com.my.business.webservice;

import com.my.business.webservice.util.WebServiceRequestIp;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by luckzj on 04/01/2018
 */
public class WebServiceOutInterceptor extends AbstractPhaseInterceptor<Message> {
    private static Logger logger = LoggerFactory.getLogger(WebServiceOutInterceptor.class);

    public WebServiceOutInterceptor(String phase) {
        super(phase);
    }

    public void handleMessage(Message message) throws Fault {
        Map<String, List> headers = (Map<String, List>) message.get(Message.PROTOCOL_HEADERS);
        headers.put("ClientIp", Arrays.asList(WebServiceRequestIp.get()));
    }
}
