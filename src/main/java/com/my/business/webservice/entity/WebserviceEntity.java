package com.my.business.webservice.entity;

/***
 * webservice的接收前台的json实体类
 * @author cc
 * @Date 2019-03-05
 */
public class WebserviceEntity {

    private String url;   //webservice的url
    private String method;   //调用的方法名称

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }


}
