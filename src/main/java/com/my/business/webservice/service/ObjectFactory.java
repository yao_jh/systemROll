package com.my.business.webservice.service;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the cn.edu.ustb.webservice package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cn.edu.ustb.webservice
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SendData }
     */
    public SendData createSendData() {
        return new SendData();
    }

    /**
     * Create an instance of {@link SendDataResponse }
     */
    public SendDataResponse createSendDataResponse() {
        return new SendDataResponse();
    }

    /**
     * Create an instance of {@link GetHmiData }
     */
    public GetHmiData createGetHmiData() {
        return new GetHmiData();
    }

    /**
     * Create an instance of {@link GetHmiDataResponse }
     */
    public GetHmiDataResponse createGetHmiDataResponse() {
        return new GetHmiDataResponse();
    }

    /**
     * Create an instance of {@link DBExecute }
     */
    public DBExecute createDBExecute() {
        return new DBExecute();
    }

    /**
     * Create an instance of {@link DBExecuteResponse }
     */
    public DBExecuteResponse createDBExecuteResponse() {
        return new DBExecuteResponse();
    }

    /**
     * Create an instance of {@link GetLoggerServer }
     */
    public GetLoggerServer createGetLoggerServer() {
        return new GetLoggerServer();
    }

    /**
     * Create an instance of {@link GetLoggerServerResponse }
     */
    public GetLoggerServerResponse createGetLoggerServerResponse() {
        return new GetLoggerServerResponse();
    }

    /**
     * Create an instance of {@link GetLogData }
     */
    public GetLogData createGetLogData() {
        return new GetLogData();
    }

    /**
     * Create an instance of {@link GetLogDataResponse }
     */
    public GetLogDataResponse createGetLogDataResponse() {
        return new GetLogDataResponse();
    }

    /**
     * Create an instance of {@link ExportToExcel }
     */
    public ExportToExcel createExportToExcel() {
        return new ExportToExcel();
    }

    /**
     * Create an instance of {@link ExportToExcelResponse }
     */
    public ExportToExcelResponse createExportToExcelResponse() {
        return new ExportToExcelResponse();
    }

    /**
     * Create an instance of {@link DBResultToExcel }
     */
    public DBResultToExcel createDBResultToExcel() {
        return new DBResultToExcel();
    }

    /**
     * Create an instance of {@link DBResultToExcelResponse }
     */
    public DBResultToExcelResponse createDBResultToExcelResponse() {
        return new DBResultToExcelResponse();
    }

}
