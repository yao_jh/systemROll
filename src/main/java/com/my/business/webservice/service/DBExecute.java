package com.my.business.webservice.service;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConnName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CmdTypeStr" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CmdTextStr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ParamXmlStr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsQuery" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="ResultId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "connName",
        "cmdTypeStr",
        "cmdTextStr",
        "paramXmlStr",
        "isQuery",
        "resultId"
})
@XmlRootElement(name = "DBExecute")
public class DBExecute {

    @XmlElement(name = "ConnName")
    protected String connName;
    @XmlElement(name = "CmdTypeStr")
    protected int cmdTypeStr;
    @XmlElement(name = "CmdTextStr")
    protected String cmdTextStr;
    @XmlElement(name = "ParamXmlStr")
    protected String paramXmlStr;
    @XmlElement(name = "IsQuery")
    protected boolean isQuery;
    @XmlElement(name = "ResultId")
    protected String resultId;

    /**
     * Gets the value of the connName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getConnName() {
        return connName;
    }

    /**
     * Sets the value of the connName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setConnName(String value) {
        this.connName = value;
    }

    /**
     * Gets the value of the cmdTypeStr property.
     */
    public int getCmdTypeStr() {
        return cmdTypeStr;
    }

    /**
     * Sets the value of the cmdTypeStr property.
     */
    public void setCmdTypeStr(int value) {
        this.cmdTypeStr = value;
    }

    /**
     * Gets the value of the cmdTextStr property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCmdTextStr() {
        return cmdTextStr;
    }

    /**
     * Sets the value of the cmdTextStr property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCmdTextStr(String value) {
        this.cmdTextStr = value;
    }

    /**
     * Gets the value of the paramXmlStr property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getParamXmlStr() {
        return paramXmlStr;
    }

    /**
     * Sets the value of the paramXmlStr property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setParamXmlStr(String value) {
        this.paramXmlStr = value;
    }

    /**
     * Gets the value of the isQuery property.
     */
    public boolean isIsQuery() {
        return isQuery;
    }

    /**
     * Sets the value of the isQuery property.
     */
    public void setIsQuery(boolean value) {
        this.isQuery = value;
    }

    /**
     * Gets the value of the resultId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getResultId() {
        return resultId;
    }

    /**
     * Sets the value of the resultId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setResultId(String value) {
        this.resultId = value;
    }

}
