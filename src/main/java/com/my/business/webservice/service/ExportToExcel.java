package com.my.business.webservice.service;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Template" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DatXml" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "template",
        "datXml"
})
@XmlRootElement(name = "ExportToExcel")
public class ExportToExcel {

    @XmlElement(name = "Template")
    protected String template;
    @XmlElement(name = "DatXml")
    protected String datXml;

    /**
     * Gets the value of the template property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTemplate() {
        return template;
    }

    /**
     * Sets the value of the template property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTemplate(String value) {
        this.template = value;
    }

    /**
     * Gets the value of the datXml property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDatXml() {
        return datXml;
    }

    /**
     * Sets the value of the datXml property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDatXml(String value) {
        this.datXml = value;
    }

}
