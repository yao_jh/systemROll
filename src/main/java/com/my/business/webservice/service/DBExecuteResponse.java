package com.my.business.webservice.service;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DBExecuteResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "dbExecuteResult"
})
@XmlRootElement(name = "DBExecuteResponse")
public class DBExecuteResponse {

    @XmlElement(name = "DBExecuteResult")
    protected String dbExecuteResult;

    /**
     * Gets the value of the dbExecuteResult property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDBExecuteResult() {
        return dbExecuteResult;
    }

    /**
     * Sets the value of the dbExecuteResult property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDBExecuteResult(String value) {
        this.dbExecuteResult = value;
    }

}
