package com.my.business.webservice.service;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConnName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CmdTypeInt" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CmdTextStr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ParamXmlStr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Template" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "connName",
        "cmdTypeInt",
        "cmdTextStr",
        "paramXmlStr",
        "template"
})
@XmlRootElement(name = "DBResultToExcel")
public class DBResultToExcel {

    @XmlElement(name = "ConnName")
    protected String connName;
    @XmlElement(name = "CmdTypeInt")
    protected int cmdTypeInt;
    @XmlElement(name = "CmdTextStr")
    protected String cmdTextStr;
    @XmlElement(name = "ParamXmlStr")
    protected String paramXmlStr;
    @XmlElement(name = "Template")
    protected String template;

    /**
     * Gets the value of the connName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getConnName() {
        return connName;
    }

    /**
     * Sets the value of the connName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setConnName(String value) {
        this.connName = value;
    }

    /**
     * Gets the value of the cmdTypeInt property.
     */
    public int getCmdTypeInt() {
        return cmdTypeInt;
    }

    /**
     * Sets the value of the cmdTypeInt property.
     */
    public void setCmdTypeInt(int value) {
        this.cmdTypeInt = value;
    }

    /**
     * Gets the value of the cmdTextStr property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCmdTextStr() {
        return cmdTextStr;
    }

    /**
     * Sets the value of the cmdTextStr property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCmdTextStr(String value) {
        this.cmdTextStr = value;
    }

    /**
     * Gets the value of the paramXmlStr property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getParamXmlStr() {
        return paramXmlStr;
    }

    /**
     * Sets the value of the paramXmlStr property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setParamXmlStr(String value) {
        this.paramXmlStr = value;
    }

    /**
     * Gets the value of the template property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTemplate() {
        return template;
    }

    /**
     * Sets the value of the template property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTemplate(String value) {
        this.template = value;
    }

}
