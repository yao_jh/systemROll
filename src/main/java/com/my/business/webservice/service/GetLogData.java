package com.my.business.webservice.service;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="KeyWord" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nRecords" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "startTime",
        "endTime",
        "keyWord",
        "nRecords"
})
@XmlRootElement(name = "GetLogData")
public class GetLogData {

    @XmlElement(name = "StartTime")
    protected String startTime;
    @XmlElement(name = "EndTime")
    protected String endTime;
    @XmlElement(name = "KeyWord")
    protected String keyWord;
    protected int nRecords;

    /**
     * Gets the value of the startTime property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * Sets the value of the startTime property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setStartTime(String value) {
        this.startTime = value;
    }

    /**
     * Gets the value of the endTime property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * Sets the value of the endTime property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setEndTime(String value) {
        this.endTime = value;
    }

    /**
     * Gets the value of the keyWord property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getKeyWord() {
        return keyWord;
    }

    /**
     * Sets the value of the keyWord property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setKeyWord(String value) {
        this.keyWord = value;
    }

    /**
     * Gets the value of the nRecords property.
     */
    public int getNRecords() {
        return nRecords;
    }

    /**
     * Sets the value of the nRecords property.
     */
    public void setNRecords(int value) {
        this.nRecords = value;
    }

}
