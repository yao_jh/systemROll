package com.my.business.webservice.service;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetLogDataResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "getLogDataResult"
})
@XmlRootElement(name = "GetLogDataResponse")
public class GetLogDataResponse {

    @XmlElement(name = "GetLogDataResult")
    protected String getLogDataResult;

    /**
     * Gets the value of the getLogDataResult property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getGetLogDataResult() {
        return getLogDataResult;
    }

    /**
     * Sets the value of the getLogDataResult property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setGetLogDataResult(String value) {
        this.getLogDataResult = value;
    }

}
