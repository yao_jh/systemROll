package com.my.business.webservice.service;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReqList" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "reqList"
})
@XmlRootElement(name = "GetHmiData")
public class GetHmiData {

    @XmlElement(name = "ReqList")
    protected String reqList;

    /**
     * Gets the value of the reqList property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getReqList() {
        return reqList;
    }

    /**
     * Sets the value of the reqList property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setReqList(String value) {
        this.reqList = value;
    }

}
