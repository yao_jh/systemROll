package com.my.business.config;

import com.my.business.webservice.WebServiceOutInterceptor;
import com.my.business.webservice.service.DataLinkerServicesSoap;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.phase.Phase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * WebServiceConfig.java
 * Created by luckzj on 12/2/17.
 */
@Configuration
public class WebServiceConfig {

    @Value("${webservice.url}")
    private String url;

    @Bean
    public DataLinkerServicesSoap dataLinkerServicesSoap() {
        JaxWsProxyFactoryBean bean = new JaxWsProxyFactoryBean();
        bean.setServiceClass(DataLinkerServicesSoap.class);
        bean.setAddress(url);
        bean.getOutInterceptors().add(new WebServiceOutInterceptor(Phase.SEND));
        DataLinkerServicesSoap dataLinkerServicesSoap = (DataLinkerServicesSoap) bean.create();
        return dataLinkerServicesSoap;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
