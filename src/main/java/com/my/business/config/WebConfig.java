package com.my.business.config;

import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.MultipartConfigElement;

/**
 * WebMvc配置
 *
 * @author cc
 * @date 2018-05-10
 */
@Configuration
@EnableSwagger2
public class WebConfig implements WebMvcConfigurer {

    /**
     * 文件上传配置
     *
     * @return
     */
    @SuppressWarnings("deprecation")
    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //文件最大  
        factory.setMaxFileSize("200MB"); //KB,MB  
        /// 设置总上传数据总大小  
        factory.setMaxRequestSize("500MB");
        return factory.createMultipartConfig();
    }
    
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.my.business.restful"))
                .paths(PathSelectors.any())
                .build();
    }

    
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("API接口文档")
                .description("用户信息管理")
                .version("1.0.0")
                .build();
    }
}
