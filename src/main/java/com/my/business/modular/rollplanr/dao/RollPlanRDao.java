package com.my.business.modular.rollplanr.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.rollplanr.entity.RollPlanR;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 1580轧辊计划dao接口
 * @author  生成器生成
 * @date 2020-10-10 14:29:14
 * */
@Mapper
public interface RollPlanRDao {

	/**
	 * 添加记录
	 * @param rollPlanR  对象实体
	 * */
	public void insertDataRollPlanR(RollPlanR rollPlanR);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataRollPlanROne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataRollPlanRMany(String value);
	
	/**
	 * 修改记录
	 * @param rollPlanR  对象实体
	 * */
	public void updateDataRollPlanR(RollPlanR rollPlanR);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<RollPlanR> findDataRollPlanRByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataRollPlanRByPageSize();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public RollPlanR findDataRollPlanRByIndocno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<RollPlanR> findDataRollPlanR();
	
}
