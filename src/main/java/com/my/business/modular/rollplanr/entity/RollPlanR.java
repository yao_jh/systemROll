package com.my.business.modular.rollplanr.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 1580轧辊计划实体类
 *
 * @author 生成器生成
 * @date 2020-10-10 14:29:14
 */
public class RollPlanR extends BaseEntity {

    private Long indocno;  //主键
    private String yeard;  //订货年
    private String factory;  //供货商
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private String contract_no;  //合同号
    private String contracttime;  //合同时间
    private Long arrive_total;  //到货数量
    private Long assetsid;  //资产分类id
    private String assets;  //资产分类
    private String assetsno;  //资产编号
    private String assetsnote;  //资产描述
    private Long f1_f4_fw;  //F1-F4工作辊
    private Long f5_f7_fw;  //F5-F7工作辊
    private Long fb;  //F1-F7支撑辊
    private Long f7hw;  //F7花纹辊
    private Long f1e;  //精轧立辊
    private Long r1w;  //R1工作辊
    private Long r2w;  //R2工作辊
    private Long r2b;  //R2支撑辊
    private Long re;  //粗轧立辊
    private Long s1w;  //S1平整工作辊
    private Long s1b;  //S1平整支撑辊
    private Long hm;  //锤头

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getYeard() {
        return yeard;
    }

    public void setYeard(String yeard) {
        this.yeard = yeard;
    }

    public String getFactory() {
        return this.factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public Long getRoll_typeid() {
        return this.roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public String getContract_no() {
        return this.contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    public String getContracttime() {
        return this.contracttime;
    }

    public void setContracttime(String contracttime) {
        this.contracttime = contracttime;
    }

    public Long getArrive_total() {
        return this.arrive_total;
    }

    public void setArrive_total(Long arrive_total) {
        this.arrive_total = arrive_total;
    }

    public Long getAssetsid() {
        return this.assetsid;
    }

    public void setAssetsid(Long assetsid) {
        this.assetsid = assetsid;
    }

    public String getAssets() {
        return this.assets;
    }

    public void setAssets(String assets) {
        this.assets = assets;
    }

    public String getAssetsno() {
        return this.assetsno;
    }

    public void setAssetsno(String assetsno) {
        this.assetsno = assetsno;
    }

    public String getAssetsnote() {
        return this.assetsnote;
    }

    public void setAssetsnote(String assetsnote) {
        this.assetsnote = assetsnote;
    }

    public Long getF1_f4_fw() {
        return this.f1_f4_fw;
    }

    public void setF1_f4_fw(Long f1_f4_fw) {
        this.f1_f4_fw = f1_f4_fw;
    }

    public Long getF5_f7_fw() {
        return this.f5_f7_fw;
    }

    public void setF5_f7_fw(Long f5_f7_fw) {
        this.f5_f7_fw = f5_f7_fw;
    }

    public Long getFb() {
        return this.fb;
    }

    public void setFb(Long fb) {
        this.fb = fb;
    }

    public Long getF7hw() {
        return this.f7hw;
    }

    public void setF7hw(Long f7hw) {
        this.f7hw = f7hw;
    }

    public Long getF1e() {
        return this.f1e;
    }

    public void setF1e(Long f1e) {
        this.f1e = f1e;
    }

    public Long getR1w() {
        return this.r1w;
    }

    public void setR1w(Long r1w) {
        this.r1w = r1w;
    }

    public Long getR2w() {
        return this.r2w;
    }

    public void setR2w(Long r2w) {
        this.r2w = r2w;
    }

    public Long getR2b() {
        return this.r2b;
    }

    public void setR2b(Long r2b) {
        this.r2b = r2b;
    }

    public Long getRe() {
        return this.re;
    }

    public void setRe(Long re) {
        this.re = re;
    }

    public Long getS1w() {
        return this.s1w;
    }

    public void setS1w(Long s1w) {
        this.s1w = s1w;
    }

    public Long getS1b() {
        return this.s1b;
    }

    public void setS1b(Long s1b) {
        this.s1b = s1b;
    }

    public Long getHm() {
        return this.hm;
    }

    public void setHm(Long hm) {
        this.hm = hm;
    }


}