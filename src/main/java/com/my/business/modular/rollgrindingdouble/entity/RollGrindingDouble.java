package com.my.business.modular.rollgrindingdouble.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.math.BigDecimal;

/**
 * 车削实绩表实体类
 *
 * @author 生成器生成
 * @date 2020-11-09 19:26:04
 */
public class RollGrindingDouble extends BaseEntity {

    private Long indocno;  //主键
    private String roll_no;  //辊号
    private Long production_line_id;  //产线id(1-2250,2-1580)
    private String production_line;  //产线
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private String grind_starttime;  //车削开始时间
    private String grind_endtime;  //车削结束时间
    private Float grindst; //车削量
    private String machine_no;  //车床号
    private Double before_diameter;  //车前直径
    private Double after_diameter;  //车后直径
    private Double diametermax;  //最大直径
    private Double diametermin;  //最小直径
    private String sclass;  //班
    private String sgroup;  //班组
    private String operat_user;  //操作人姓名
    private Long operat_userid;  //操作人id
    private String snote;  //备注

    private Double online_wear;//在机磨损量
    private Double lasttime_after_diameter;  //上机前直径

    public Double getOnline_wear() {
        return online_wear;
    }

    public void setOnline_wear(Double online_wear) {
        this.online_wear = online_wear;
    }

    public Double getLasttime_after_diameter() {
        return lasttime_after_diameter;
    }

    public void setLasttime_after_diameter(Double lasttime_after_diameter) {
        this.lasttime_after_diameter = lasttime_after_diameter;
    }

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public Long getProduction_line_id() {
        return this.production_line_id;
    }

    public void setProduction_line_id(Long production_line_id) {
        this.production_line_id = production_line_id;
    }

    public String getProduction_line() {
        return this.production_line;
    }

    public void setProduction_line(String production_line) {
        this.production_line = production_line;
    }

    public Long getRoll_typeid() {
        return this.roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public String getGrind_starttime() {
        return this.grind_starttime;
    }

    public void setGrind_starttime(String grind_starttime) {
        this.grind_starttime = grind_starttime;
    }

    public String getGrind_endtime() {
        return this.grind_endtime;
    }

    public void setGrind_endtime(String grind_endtime) {
        this.grind_endtime = grind_endtime;
    }

    public String getMachine_no() {
        return this.machine_no;
    }

    public void setMachine_no(String machine_no) {
        this.machine_no = machine_no;
    }

    public Double getBefore_diameter() {
        return this.before_diameter;
    }

    public void setBefore_diameter(Double before_diameter) {
        this.before_diameter = before_diameter;
    }

    public Double getAfter_diameter() {
        return this.after_diameter;
    }

    public void setAfter_diameter(Double after_diameter) {
        this.after_diameter = after_diameter;
    }

    public Double getDiametermax() {
        return this.diametermax;
    }

    public void setDiametermax(Double diametermax) {
        this.diametermax = diametermax;
    }

    public Double getDiametermin() {
        return this.diametermin;
    }

    public void setDiametermin(Double diametermin) {
        this.diametermin = diametermin;
    }

    public String getSclass() {
        return this.sclass;
    }

    public void setSclass(String sclass) {
        this.sclass = sclass;
    }

    public String getSgroup() {
        return this.sgroup;
    }

    public void setSgroup(String sgroup) {
        this.sgroup = sgroup;
    }

    public String getOperat_user() {
        return this.operat_user;
    }

    public void setOperat_user(String operat_user) {
        this.operat_user = operat_user;
    }

    public Long getOperat_userid() {
        return this.operat_userid;
    }

    public void setOperat_userid(Long operat_userid) {
        this.operat_userid = operat_userid;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public Float getGrindst() {
        return grindst;
    }

    public void setGrindst(Float grindst) {
        this.grindst = grindst;
    }
}