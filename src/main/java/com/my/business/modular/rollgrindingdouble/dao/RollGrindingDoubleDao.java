package com.my.business.modular.rollgrindingdouble.dao;

import com.my.business.modular.rollgrindingdouble.entity.RollGrindingDouble;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 车削实绩表dao接口
 *
 * @author 生成器生成
 * @date 2020-11-09 19:26:05
 */
@Mapper
public interface RollGrindingDoubleDao {

    /**
     * 添加记录
     *
     * @param rollGrindingDouble 对象实体
     */
	void insertDataRollGrindingDouble(RollGrindingDouble rollGrindingDouble);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
	void deleteDataRollGrindingDoubleOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
	void deleteDataRollGrindingDoubleMany(String value);

    /**
     * 修改记录
     *
     * @param rollGrindingDouble 对象实体
     */
	void updateDataRollGrindingDouble(RollGrindingDouble rollGrindingDouble);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
	List<RollGrindingDouble> findDataRollGrindingDoubleByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("roll_no") String roll_no, @Param("roll_typeid") Long roll_typeid, @Param("dbegin") String dbegin, @Param("dend") String dend, @Param("production_line_id") Long production_line_id);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
	Integer findDataRollGrindingDoubleByPageSize(@Param("roll_no") String roll_no, @Param("roll_typeid") Long roll_typeid, @Param("dbegin") String dbegin, @Param("dend") String dend, @Param("production_line_id") Long production_line_id);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
	RollGrindingDouble findDataRollGrindingDoubleByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
	List<RollGrindingDouble> findDataRollGrindingDouble();

}
