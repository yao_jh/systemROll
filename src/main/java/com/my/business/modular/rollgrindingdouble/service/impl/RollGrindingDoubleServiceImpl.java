package com.my.business.modular.rollgrindingdouble.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollgrindingdouble.dao.RollGrindingDoubleDao;
import com.my.business.modular.rollgrindingdouble.entity.JRollGrindingDouble;
import com.my.business.modular.rollgrindingdouble.entity.RollGrindingDouble;
import com.my.business.modular.rollgrindingdouble.service.RollGrindingDoubleService;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 车削实绩表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-11-09 19:26:05
 */
@Service
public class RollGrindingDoubleServiceImpl implements RollGrindingDoubleService {

    @Autowired
    private RollGrindingDoubleDao rollGrindingDoubleDao;
    @Autowired
    private RollInformationDao rollInformationDao;
    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollGrindingDouble(String data, Long userId, String sname) {
        try {
            JRollGrindingDouble jrollGrindingDouble = JSON.parseObject(data, JRollGrindingDouble.class);
            RollGrindingDouble rollGrindingDouble = jrollGrindingDouble.getRollGrindingDouble();
            if (!StringUtils.isEmpty(rollGrindingDouble.getBefore_diameter()) && !StringUtils.isEmpty(rollGrindingDouble.getAfter_diameter())) {
                rollGrindingDouble.setGrindst(Float.valueOf(String.valueOf(rollGrindingDouble.getBefore_diameter() - rollGrindingDouble.getAfter_diameter())));
            }
            // -- 查询下次磨前直径 （）
            CodiUtil.newRecord(userId, sname, rollGrindingDouble);
            rollGrindingDoubleDao.insertDataRollGrindingDouble(rollGrindingDouble);

            RollInformation rollInformation = rollInformationDao.findDataRollInformationByRollNo(rollGrindingDouble.getRoll_no());
            if (!StringUtils.isEmpty(rollInformation)) {
                if(rollGrindingDouble.getAfter_diameter() >= 0){
                    rollInformation.setCurrentdiameter(rollGrindingDouble.getAfter_diameter());//更新当前辊径
                }
                rollInformation.setLastgrindingtime(rollGrindingDouble.getGrind_starttime());//最后一次磨削时间
                rollInformation.setLastgrindingdepth(rollGrindingDouble.getBefore_diameter() - rollGrindingDouble.getAfter_diameter());
                rollInformation.setMachine_id(Long.valueOf(rollGrindingDouble.getMachine_no()));
                rollInformationDao.updateDataRollInformation(rollInformation);
            }
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollGrindingDoubleOne(Long indocno) {
        try {
            rollGrindingDoubleDao.deleteDataRollGrindingDoubleOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollGrindingDoubleMany(String str_id) {
        try {
            String sql = "delete roll_grinding_double where indocno in(" + str_id + ")";
            rollGrindingDoubleDao.deleteDataRollGrindingDoubleMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollGrindingDouble(String data, Long userId, String sname) {
        try {
            JRollGrindingDouble jrollGrindingDouble = JSON.parseObject(data, JRollGrindingDouble.class);
            RollGrindingDouble rollGrindingDouble = jrollGrindingDouble.getRollGrindingDouble();
            CodiUtil.editRecord(userId, sname, rollGrindingDouble);
            rollGrindingDoubleDao.updateDataRollGrindingDouble(rollGrindingDouble);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollGrindingDoubleByPage(String data) {
        try {
            JRollGrindingDouble jrollGrindingDouble = JSON.parseObject(data, JRollGrindingDouble.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollGrindingDouble.getPageIndex();
            Integer pageSize = jrollGrindingDouble.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollGrindingDouble.getCondition()) {
                jsonObject = JSON.parseObject(jrollGrindingDouble.getCondition().toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

            Long roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = Long.valueOf(jsonObject.get("roll_typeid").toString());
            }

            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            Long production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
            }

            List<RollGrindingDouble> list = rollGrindingDoubleDao.findDataRollGrindingDoubleByPage((pageIndex - 1) * pageSize, pageSize,roll_no, roll_typeid, dbegin, dend,production_line_id);
            Integer count = rollGrindingDoubleDao.findDataRollGrindingDoubleByPageSize(roll_no, roll_typeid, dbegin, dend,production_line_id);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollGrindingDoubleByIndocno(String data) {
        try {
            JRollGrindingDouble jrollGrindingDouble = JSON.parseObject(data, JRollGrindingDouble.class);
            Long indocno = jrollGrindingDouble.getIndocno();

            RollGrindingDouble rollGrindingDouble = rollGrindingDoubleDao.findDataRollGrindingDoubleByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollGrindingDouble);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollGrindingDouble> findDataRollGrindingDouble() {
        List<RollGrindingDouble> list = rollGrindingDoubleDao.findDataRollGrindingDouble();
        return list;
    }

}
