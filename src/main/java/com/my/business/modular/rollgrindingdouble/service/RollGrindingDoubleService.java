package com.my.business.modular.rollgrindingdouble.service;

import com.my.business.modular.rollgrindingdouble.entity.RollGrindingDouble;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 车削实绩表接口服务类
 *
 * @author 生成器生成
 * @date 2020-11-09 19:26:05
 */
public interface RollGrindingDoubleService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
	ResultData insertDataRollGrindingDouble(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
	ResultData deleteDataRollGrindingDoubleOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
	ResultData deleteDataRollGrindingDoubleMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
	ResultData updateDataRollGrindingDouble(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
	ResultData findDataRollGrindingDoubleByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
	ResultData findDataRollGrindingDoubleByIndocno(String data);

    /**
     * 查看记录
     */
	List<RollGrindingDouble> findDataRollGrindingDouble();
}
