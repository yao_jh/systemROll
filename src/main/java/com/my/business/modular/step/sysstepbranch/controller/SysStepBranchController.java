package com.my.business.modular.step.sysstepbranch.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.step.sysstepbranch.entity.JSysStepBranch;
import com.my.business.modular.step.sysstepbranch.entity.SysStepBranch;
import com.my.business.modular.step.sysstepbranch.service.SysStepBranchService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 步骤分支关系表控制器层
 *
 * @author 生成器生成
 * @date 2020-05-27 15:10:51
 */
@RestController
@RequestMapping("/sysStepBranch")
public class SysStepBranchController {

    @Autowired
    private SysStepBranchService sysStepBranchService;

//    /**
//     * 添加记录
//     *
//     * @param userId 用户id
//     * @param sname  用户姓名
//     */
//    @CrossOrigin
//    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
//    @ResponseBody
//    public ResultData insertDataSysStepBranch(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
//        JCommon common = JSON.parseObject(loginToken, JCommon.class);
//        String sname = common.getSname();
//        Long userId = common.getUserId();
//        return sysStepBranchService.insertDataSysStepBranch(data, userId, CodiUtil.returnLm(sname));
//    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysStepBranchOne(@RequestBody String data) {
        try {
            JSysStepBranch jsysStepBranch = JSON.parseObject(data, JSysStepBranch.class);
            return sysStepBranchService.deleteDataSysStepBranchOne(String.valueOf(jsysStepBranch.getIndocno()));
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JSysStepBranch jsysStepBranch = JSON.parseObject(data, JSysStepBranch.class);
            return sysStepBranchService.deleteDataSysStepBranchMany(jsysStepBranch.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataSysStepBranch(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return sysStepBranchService.updateDataSysStepBranch(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysStepBranchByPage(@RequestBody String data) {
        return sysStepBranchService.findDataSysStepBranchByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysStepBranchByIndocno(@RequestBody String data) {
        return sysStepBranchService.findDataSysStepBranchByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<SysStepBranch> findDataSysStepBranch() {
        return sysStepBranchService.findDataSysStepBranch();
    }

}
