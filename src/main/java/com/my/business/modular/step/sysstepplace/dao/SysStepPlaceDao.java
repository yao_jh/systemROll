package com.my.business.modular.step.sysstepplace.dao;

import com.my.business.modular.step.sysstepplace.entity.SysStepPlace;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 步骤场地关系表dao接口
 *
 * @author 生成器生成
 * @date 2020-05-22 13:24:44
 */
@Mapper
public interface SysStepPlaceDao {

    /**
     * 添加记录
     *
     * @param sysStepPlace 对象实体
     */
    void insertDataSysStepPlace(SysStepPlace sysStepPlace);

    /**
     * 根据流程编码删除对象
     *
     * @param work_no 流程编码
     */
    void deleteDataSysStepPlaceOne(@Param("work_id") String work_no);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataSysStepPlaceMany(String value);

    /**
     * 修改记录
     *
     * @param sysStepPlace 对象实体
     */
    void updateDataSysStepPlace(SysStepPlace sysStepPlace);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<SysStepPlace> findDataSysStepPlaceByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataSysStepPlaceByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    SysStepPlace findDataSysStepPlaceByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<SysStepPlace> findDataSysStepPlace();

    /***
     * 根据步骤编码查询信息
     * @param step_no 步骤编码
     * @return
     */
    List<SysStepPlace> findDataSysStepPlaceByStepId(@Param("step_id") String step_no);

    /***
     * 根据流程编码查询信息
     * @param work_no 流程编码
     * @return
     */
    List<SysStepPlace> findDataSysStepPlaceByWorkId(@Param("work_id") String work_no);
}
