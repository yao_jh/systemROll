package com.my.business.modular.step.sysstepbranch.dao;

import com.my.business.modular.step.sysstepbranch.entity.SysStepBranch;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 步骤分支关系表dao接口
 *
 * @author 生成器生成
 * @date 2020-05-27 15:10:51
 */
@Mapper
public interface SysStepBranchDao {

    /**
     * 添加记录
     *
     * @param sysStepBranch 对象实体
     */
    void insertDataSysStepBranch(SysStepBranch sysStepBranch);

    /**
     * 根据流程编码删除对象
     *
     * @param work_no 流程编码
     */
    void deleteDataSysStepBranchOne(@Param("work_id") String work_no);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataSysStepBranchMany(String value);

    /**
     * 修改记录
     *
     * @param sysStepBranch 对象实体
     */
    void updateDataSysStepBranch(SysStepBranch sysStepBranch);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<SysStepBranch> findDataSysStepBranchByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataSysStepBranchByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    SysStepBranch findDataSysStepBranchByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<SysStepBranch> findDataSysStepBranch();

    /***
     * 根据流程编码查询信息
     * @param work_no 用户id
     * @return
     */
    List<SysStepBranch> findDataSysStepBranchByWorkId(@Param("work_id") String work_no);

    /***
     * 根据步骤编码查询信息
     * @param step_no 步骤编码
     * @return
     */
    List<SysStepBranch> findDataSysStepBranchByStepId(@Param("step_id") String step_no);
}
