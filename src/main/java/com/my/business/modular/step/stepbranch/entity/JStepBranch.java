package com.my.business.modular.step.stepbranch.entity;

import com.my.business.sys.common.entity.JCommon;

/**
 * 分支配置表json的实体类
 *
 * @author 生成器生成
 * @date 2020-05-27 15:10:09
 */
public class JStepBranch extends JCommon {

    private Integer pageIndex; // 第几页
    private Integer pageSize; // 每页多少数据
    private Object condition; // 查询条件
    private StepBranch stepBranch;   //对应模块的实体类
    private Long indocno;
    private String str_indocno;

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Object getCondition() {
        return condition;
    }

    public void setCondition(Object condition) {
        this.condition = condition;
    }

    public StepBranch getStepBranch() {
        return stepBranch;
    }

    public void setStepBranch(StepBranch stepBranch) {
        this.stepBranch = stepBranch;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getStr_indocno() {
        return str_indocno;
    }

    public void setStr_indocno(String str_indocno) {
        this.str_indocno = str_indocno;
    }
}