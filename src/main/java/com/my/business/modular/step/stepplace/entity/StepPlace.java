package com.my.business.modular.step.stepplace.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 步骤场地配置表实体类
 *
 * @author 生成器生成
 * @date 2020-05-22 12:59:41
 */
public class StepPlace extends BaseEntity {

    private Long indocno;  //主键
    private String ilinkno; //外键、编码
    private Long itype;  //类型(自动or手动)
    private String areaname;  //场地名称
    private Long areaid;  //场地id
    private String snote;  //说明

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getItype() {
        return this.itype;
    }

    public void setItype(Long itype) {
        this.itype = itype;
    }

    public String getAreaname() {
        return this.areaname;
    }

    public void setAreaname(String areaname) {
        this.areaname = areaname;
    }

    public Long getAreaid() {
        return this.areaid;
    }

    public void setAreaid(Long areaid) {
        this.areaid = areaid;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public String getIlinkno() {
        return ilinkno;
    }

    public void setIlinkno(String ilinkno) {
        this.ilinkno = ilinkno;
    }
}