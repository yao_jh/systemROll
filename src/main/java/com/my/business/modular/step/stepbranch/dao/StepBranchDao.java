package com.my.business.modular.step.stepbranch.dao;

import com.my.business.modular.step.stepbranch.entity.StepBranch;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 分支配置表dao接口
 *
 * @author 生成器生成
 * @date 2020-05-27 15:10:09
 */
@Mapper
public interface StepBranchDao {

    /**
     * 添加记录
     *
     * @param stepBranch 对象实体
     */
    void insertDataStepBranch(StepBranch stepBranch);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataStepBranchOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataStepBranchMany(String value);

    /**
     * 修改记录
     *
     * @param stepBranch 对象实体
     */
    void updateDataStepBranch(StepBranch stepBranch);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<StepBranch> findDataStepBranchByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataStepBranchByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    StepBranch findDataStepBranchByIndocno(@Param("indocno") Long indocno);

    /***
     * 根据步骤编码查询当前分支信息
     * @param step_no 步骤编码
     * @return
     */
    StepBranch findDataStepBranchByStepNo(@Param("step_no") String step_no);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<StepBranch> findDataStepBranch();

    /**
     * 根据编码删除对象
     *
     * @param branch_id 分支编码
     */
    void deleteDataStepBranchOneByIlinkno(@Param("ilinkno") String branch_id);

    /***
     * 根据编码查询信息
     * @param branch_id 分支编码
     * @return
     */
    StepBranch findDataStepBranchByIlinkno(@Param("ilinkno") String branch_id);
}
