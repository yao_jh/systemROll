package com.my.business.modular.step.stepbranch.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 分支配置表实体类
 *
 * @author 生成器生成
 * @date 2020-05-27 15:10:08
 */
public class StepBranch extends BaseEntity {

    private Long indocno;  //主键
    private String ilinkno;  //编码
    private Long ijudge;  //判断逻辑
    private String sfiled;  //判断字段名
    private Long ivalue;  //数值
    private String snote;  //说明

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getIlinkno() {
        return this.ilinkno;
    }

    public void setIlinkno(String ilinkno) {
        this.ilinkno = ilinkno;
    }

    public Long getIjudge() {
        return this.ijudge;
    }

    public void setIjudge(Long ijudge) {
        this.ijudge = ijudge;
    }

    public String getSfiled() {
        return this.sfiled;
    }

    public void setSfiled(String sfiled) {
        this.sfiled = sfiled;
    }

    public Long getIvalue() {
        return this.ivalue;
    }

    public void setIvalue(Long ivalue) {
        this.ivalue = ivalue;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }


}