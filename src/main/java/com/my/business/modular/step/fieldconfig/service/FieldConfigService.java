package com.my.business.modular.step.fieldconfig.service;

import com.my.business.modular.step.fieldconfig.entity.FieldConfig;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 字段配置表接口服务类
 *
 * @author 生成器生成
 * @date 2020-06-15 10:57:46
 */
public interface FieldConfigService {

    /**
     * 添加记录
     *
     * @param fieldConfig 数据
     * @param userId      用户id
     * @param sname       用户姓名
     */
    ResultData insertDataFieldConfig(FieldConfig fieldConfig, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataFieldConfigOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataFieldConfigMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataFieldConfig(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataFieldConfigByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataFieldConfigByIndocno(String data);

    /**
     * 查看记录
     */
    List<FieldConfig> findDataFieldConfig();
}
