package com.my.business.modular.step.stepuser.dao;

import com.my.business.modular.step.stepuser.entity.StepUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 步骤人员配置表dao接口
 *
 * @author 生成器生成
 * @date 2020-05-22 12:54:05
 */
@Mapper
public interface StepUserDao {

    /**
     * 添加记录
     *
     * @param stepUser 对象实体
     */
    void insertDataStepUser(StepUser stepUser);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataStepUserOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataStepUserMany(String value);

    /**
     * 修改记录
     *
     * @param stepUser 对象实体
     */
    void updateDataStepUser(StepUser stepUser);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<StepUser> findDataStepUserByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataStepUserByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    StepUser findDataStepUserByIndocno(@Param("indocno") Long indocno);

    /***
     * 根据步骤编码查询当前步骤执行人信息
     * @param step_no 步骤编码
     * @return
     */
    StepUser findDataStepUserByStepNo(@Param("step_no") String step_no);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<StepUser> findDataStepUser();

    /***
     * 根据编码查询信息
     * @param user_id 人员编码
     * @return
     */
    StepUser findDataStepUserByIlinkno(@Param("ilinkno") String user_id);

    /**
     * 根据人员编码删除对象
     *
     * @param user_id 人员编码
     */
    void deleteDataStepUserOneByIlinkno(@Param("ilinkno") String user_id);
}
