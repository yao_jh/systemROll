package com.my.business.modular.step.stepbranch.service;

import com.my.business.modular.step.stepbranch.entity.StepBranch;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 分支配置表接口服务类
 *
 * @author 生成器生成
 * @date 2020-05-27 15:10:09
 */
public interface StepBranchService {

    /**
     * 添加记录
     *
     * @param stepBranch 数据
     * @param userId     用户id
     * @param sname      用户姓名
     */
    ResultData insertDataStepBranch(StepBranch stepBranch, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataStepBranchOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataStepBranchMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataStepBranch(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataStepBranchByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataStepBranchByIndocno(String data);

    /**
     * 查看记录
     */
    List<StepBranch> findDataStepBranch();
}
