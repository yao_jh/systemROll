package com.my.business.modular.step.sysstepplace.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.step.sysstepplace.entity.JSysStepPlace;
import com.my.business.modular.step.sysstepplace.entity.SysStepPlace;
import com.my.business.modular.step.sysstepplace.service.SysStepPlaceService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 步骤场地关系表控制器层
 *
 * @author 生成器生成
 * @date 2020-05-22 13:24:45
 */
@RestController
@RequestMapping("/sysStepPlace")
public class SysStepPlaceController {

    @Autowired
    private SysStepPlaceService sysStepPlaceService;

//	/**
//	 * 添加记录
//	 * @param userId 用户id
//     * @param sname 用户姓名
//	 * */
//	@CrossOrigin
//	@RequestMapping(value={"/insert"}, method=RequestMethod.POST)
//	@ResponseBody
//	public ResultData insertDataSysStepPlace(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
//		JCommon common = JSON.parseObject(loginToken,JCommon.class);
//    	String sname = common.getSname();
//    	Long userId = common.getUserId();
//		return sysStepPlaceService.insertDataSysStepPlace(data,userId,CodiUtil.returnLm(sname));
//	};

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysStepPlaceOne(@RequestBody String data) {
        try {
            JSysStepPlace jsysStepPlace = JSON.parseObject(data, JSysStepPlace.class);
            return sysStepPlaceService.deleteDataSysStepPlaceOne(String.valueOf(jsysStepPlace.getIndocno()));
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JSysStepPlace jsysStepPlace = JSON.parseObject(data, JSysStepPlace.class);
            return sysStepPlaceService.deleteDataSysStepPlaceMany(jsysStepPlace.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataSysStepPlace(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return sysStepPlaceService.updateDataSysStepPlace(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysStepPlaceByPage(@RequestBody String data) {
        return sysStepPlaceService.findDataSysStepPlaceByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysStepPlaceByIndocno(@RequestBody String data) {
        return sysStepPlaceService.findDataSysStepPlaceByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<SysStepPlace> findDataSysStepPlace() {
        return sysStepPlaceService.findDataSysStepPlace();
    }

}
