package com.my.business.modular.step.sysstepmachine.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 步骤磨床关系表实体类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:27:10
 */
public class SysStepMachine extends BaseEntity {

    private Long indocno;  //主键
    private Long ilinkno;  //外键
    private String work_id;  //流程编码
    private String step_id;  //步骤编码
    private String machine_id;  //磨床编码

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getIlinkno() {
        return this.ilinkno;
    }

    public void setIlinkno(Long ilinkno) {
        this.ilinkno = ilinkno;
    }

    public String getWork_id() {
        return this.work_id;
    }

    public void setWork_id(String work_id) {
        this.work_id = work_id;
    }

    public String getStep_id() {
        return this.step_id;
    }

    public void setStep_id(String step_id) {
        this.step_id = step_id;
    }

    public String getMachine_id() {
        return this.machine_id;
    }

    public void setMachine_id(String machine_id) {
        this.machine_id = machine_id;
    }


}