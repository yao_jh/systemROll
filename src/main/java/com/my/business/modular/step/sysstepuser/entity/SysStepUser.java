package com.my.business.modular.step.sysstepuser.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 步骤人员关系表实体类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:21:15
 */
public class SysStepUser extends BaseEntity {

    private Long indocno;  //主键
    private Long ilinkno;  //外键
    private String work_id;  //流程编码
    private String step_id;  //步骤编码
    private String user_id;  //人员编码

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getIlinkno() {
        return this.ilinkno;
    }

    public void setIlinkno(Long ilinkno) {
        this.ilinkno = ilinkno;
    }

    public String getWork_id() {
        return this.work_id;
    }

    public void setWork_id(String work_id) {
        this.work_id = work_id;
    }

    public String getStep_id() {
        return this.step_id;
    }

    public void setStep_id(String step_id) {
        this.step_id = step_id;
    }

    public String getUser_id() {
        return this.user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }


}