package com.my.business.modular.step.sysstepuser.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.step.sysstepuser.entity.JSysStepUser;
import com.my.business.modular.step.sysstepuser.entity.SysStepUser;
import com.my.business.modular.step.sysstepuser.service.SysStepUserService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 步骤人员关系表控制器层
 *
 * @author 生成器生成
 * @date 2020-05-22 13:21:15
 */
@RestController
@RequestMapping("/sysStepUser")
public class SysStepUserController {

    @Autowired
    private SysStepUserService sysStepUserService;

//	/**
//	 * 添加记录
//	 * @param userId 用户id
//     * @param sname 用户姓名
//	 * */
//	@CrossOrigin
//	@RequestMapping(value={"/insert"}, method=RequestMethod.POST)
//	@ResponseBody
//	public ResultData insertDataSysStepUser(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
//		JCommon common = JSON.parseObject(loginToken,JCommon.class);
//    	String sname = common.getSname();
//    	Long userId = common.getUserId();
//		return sysStepUserService.insertDataSysStepUser(data,userId,CodiUtil.returnLm(sname));
//	};

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysStepUserOne(@RequestBody String data) {
        try {
            JSysStepUser jsysStepUser = JSON.parseObject(data, JSysStepUser.class);
            return sysStepUserService.deleteDataSysStepUserOne(String.valueOf(jsysStepUser.getIndocno()));
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JSysStepUser jsysStepUser = JSON.parseObject(data, JSysStepUser.class);
            return sysStepUserService.deleteDataSysStepUserMany(jsysStepUser.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataSysStepUser(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return sysStepUserService.updateDataSysStepUser(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysStepUserByPage(@RequestBody String data) {
        return sysStepUserService.findDataSysStepUserByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysStepUserByIndocno(@RequestBody String data) {
        return sysStepUserService.findDataSysStepUserByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<SysStepUser> findDataSysStepUser() {
        return sysStepUserService.findDataSysStepUser();
    }

}
