package com.my.business.modular.step.stepuser.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.step.stepuser.dao.StepUserDao;
import com.my.business.modular.step.stepuser.entity.JStepUser;
import com.my.business.modular.step.stepuser.entity.StepUser;
import com.my.business.modular.step.stepuser.service.StepUserService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 步骤人员配置表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-05-22 12:54:05
 */
@Service
public class StepUserServiceImpl implements StepUserService {

    @Autowired
    private StepUserDao stepUserDao;

    /**
     * 添加记录
     *
     * @param stepUser 数据
     * @param userId   用户id
     * @param sname    用户姓名
     */
    public ResultData insertDataStepUser(StepUser stepUser, Long userId, String sname) {
        try {
            CodiUtil.newRecord(userId, sname, stepUser);
            stepUserDao.insertDataStepUser(stepUser);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataStepUserOne(Long indocno) {
        try {
            stepUserDao.deleteDataStepUserOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataStepUserMany(String str_id) {
        try {
            String sql = "delete step_user where indocno in(" + str_id + ")";
            stepUserDao.deleteDataStepUserMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   对象实体
     */
    public ResultData updateDataStepUser(String data, Long userId, String sname) {
        try {
            JStepUser jstepUser = JSON.parseObject(data, JStepUser.class);
            StepUser stepUser = jstepUser.getStepUser();
            CodiUtil.editRecord(userId, sname, stepUser);
            stepUserDao.updateDataStepUser(stepUser);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataStepUserByPage(String data) {
        try {
            JStepUser jstepUser = JSON.parseObject(data, JStepUser.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jstepUser.getPageIndex();
            Integer pageSize = jstepUser.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jstepUser.getCondition()) {
                jsonObject = JSON.parseObject(jstepUser.getCondition().toString());
            }

            List<StepUser> list = stepUserDao.findDataStepUserByPage(pageIndex, pageSize);
            Integer count = stepUserDao.findDataStepUserByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataStepUserByIndocno(String data) {
        try {
            JStepUser jstepUser = JSON.parseObject(data, JStepUser.class);
            Long indocno = jstepUser.getIndocno();

            StepUser stepUser = stepUserDao.findDataStepUserByIndocno(indocno);
            return ResultData.ResultDataSuccess(stepUser);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<StepUser> findDataStepUser() {
        List<StepUser> list = stepUserDao.findDataStepUser();
        return list;
    }

}
