package com.my.business.modular.step.stepplace.dao;

import com.my.business.modular.step.stepplace.entity.StepPlace;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 步骤场地配置表dao接口
 *
 * @author 生成器生成
 * @date 2020-05-22 12:59:41
 */
@Mapper
public interface StepPlaceDao {

    /**
     * 添加记录
     *
     * @param stepPlace 对象实体
     */
    void insertDataStepPlace(StepPlace stepPlace);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataStepPlaceOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataStepPlaceMany(String value);

    /**
     * 修改记录
     *
     * @param stepPlace 对象实体
     */
    void updateDataStepPlace(StepPlace stepPlace);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<StepPlace> findDataStepPlaceByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataStepPlaceByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    StepPlace findDataStepPlaceByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<StepPlace> findDataStepPlace();

    /***
     * 根据编码查询信息
     * @param place_id 场地编码
     * @return
     */
    StepPlace findDataStepPlaceByIlinkno(@Param("ilinkno") String place_id);

    /***
     * 根据步骤编码查询当前步骤场地信息
     * @param step_no 步骤编码
     * @return
     */
    StepPlace findDataStepPlaceByStepNo(@Param("step_no") String step_no);

    /**
     * 根据场地编码删除对象
     *
     * @param place_id 场地编码
     */
    void deleteDataStepPlaceOneByIlinkno(@Param("ilinkno") String place_id);
}
