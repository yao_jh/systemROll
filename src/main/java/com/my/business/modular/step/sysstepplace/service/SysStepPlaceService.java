package com.my.business.modular.step.sysstepplace.service;

import com.my.business.modular.step.sysstepplace.entity.SysStepPlace;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 步骤场地关系表接口服务类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:24:44
 */
public interface SysStepPlaceService {

    /**
     * 添加记录
     *
     * @param sysStepPlace 数据
     * @param userId       用户id
     * @param sname        用户姓名
     */
    ResultData insertDataSysStepPlace(SysStepPlace sysStepPlace, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataSysStepPlaceOne(String indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataSysStepPlaceMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataSysStepPlace(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataSysStepPlaceByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataSysStepPlaceByIndocno(String data);

    /**
     * 查看记录
     */
    List<SysStepPlace> findDataSysStepPlace();
}
