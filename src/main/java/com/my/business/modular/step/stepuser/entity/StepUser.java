package com.my.business.modular.step.stepuser.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 步骤人员配置表实体类
 *
 * @author 生成器生成
 * @date 2020-05-22 12:54:05
 */
public class StepUser extends BaseEntity {

    private Long indocno;  //主键
    private String ilinkno; //外键、编码
    private Long itype;  //类型(自动or手动)
    private String act_user;  //办理人姓名
    private Long act_userid;  //办理人id
    private String act_role; //办理角色
    private Long act_roleid; //办理角色id
    private String snote;  //说明

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getItype() {
        return this.itype;
    }

    public void setItype(Long itype) {
        this.itype = itype;
    }

    public String getAct_user() {
        return this.act_user;
    }

    public void setAct_user(String act_user) {
        this.act_user = act_user;
    }

    public Long getAct_userid() {
        return this.act_userid;
    }

    public void setAct_userid(Long act_userid) {
        this.act_userid = act_userid;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public String getIlinkno() {
        return ilinkno;
    }

    public void setIlinkno(String ilinkno) {
        this.ilinkno = ilinkno;
    }

    public String getAct_role() {
        return act_role;
    }

    public void setAct_role(String act_role) {
        this.act_role = act_role;
    }

    public Long getAct_roleid() {
        return act_roleid;
    }

    public void setAct_roleid(Long act_roleid) {
        this.act_roleid = act_roleid;
    }
}