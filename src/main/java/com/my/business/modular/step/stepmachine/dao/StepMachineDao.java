package com.my.business.modular.step.stepmachine.dao;

import com.my.business.modular.step.stepmachine.entity.StepMachine;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 步骤磨床配置表dao接口
 *
 * @author 生成器生成
 * @date 2020-05-22 13:02:43
 */
@Mapper
public interface StepMachineDao {

    /**
     * 添加记录
     *
     * @param stepMachine 对象实体
     */
    void insertDataStepMachine(StepMachine stepMachine);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataStepMachineOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataStepMachineMany(String value);

    /**
     * 修改记录
     *
     * @param stepMachine 对象实体
     */
    void updateDataStepMachine(StepMachine stepMachine);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<StepMachine> findDataStepMachineByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataStepMachineByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    StepMachine findDataStepMachineByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<StepMachine> findDataStepMachine();

    /***
     * 根据编码查询信息
     * @param machine_id 磨床编号
     * @return
     */
    StepMachine findDataStepMachineByIlinkno(@Param("ilinkno") String machine_id);

    /***
     * 根据步骤编码查询当前步骤磨床信息
     * @param step_no 步骤编码
     * @return
     */
    StepMachine findDataStepMachineByStepNo(@Param("step_no") String step_no);

    /**
     * 根据磨床编码删除对象
     *
     * @param machine_id 磨床编码
     */
    void deleteDataStepMachineOneByIlinkno(@Param("ilinkno") String machine_id);
}
