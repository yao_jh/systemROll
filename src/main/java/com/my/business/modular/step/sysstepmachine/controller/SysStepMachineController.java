package com.my.business.modular.step.sysstepmachine.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.step.sysstepmachine.entity.JSysStepMachine;
import com.my.business.modular.step.sysstepmachine.entity.SysStepMachine;
import com.my.business.modular.step.sysstepmachine.service.SysStepMachineService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 步骤磨床关系表控制器层
 *
 * @author 生成器生成
 * @date 2020-05-22 13:27:11
 */
@RestController
@RequestMapping("/sysStepMachine")
public class SysStepMachineController {

    @Autowired
    private SysStepMachineService sysStepMachineService;

//	/**
//	 * 添加记录
//	 * @param userId 用户id
//     * @param sname 用户姓名
//	 * */
//	@CrossOrigin
//	@RequestMapping(value={"/insert"}, method=RequestMethod.POST)
//	@ResponseBody
//	public ResultData insertDataSysStepMachine(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
//		JCommon common = JSON.parseObject(loginToken,JCommon.class);
//    	String sname = common.getSname();
//    	Long userId = common.getUserId();
//		return sysStepMachineService.insertDataSysStepMachine(data,userId,CodiUtil.returnLm(sname));
//	};

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysStepMachineOne(@RequestBody String data) {
        try {
            JSysStepMachine jsysStepMachine = JSON.parseObject(data, JSysStepMachine.class);
            return sysStepMachineService.deleteDataSysStepMachineOne(String.valueOf(jsysStepMachine.getIndocno()));
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JSysStepMachine jsysStepMachine = JSON.parseObject(data, JSysStepMachine.class);
            return sysStepMachineService.deleteDataSysStepMachineMany(jsysStepMachine.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataSysStepMachine(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return sysStepMachineService.updateDataSysStepMachine(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysStepMachineByPage(@RequestBody String data) {
        return sysStepMachineService.findDataSysStepMachineByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysStepMachineByIndocno(@RequestBody String data) {
        return sysStepMachineService.findDataSysStepMachineByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<SysStepMachine> findDataSysStepMachine() {
        return sysStepMachineService.findDataSysStepMachine();
    }

}
