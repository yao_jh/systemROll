package com.my.business.modular.step.fieldconfig.dao;

import com.my.business.modular.step.fieldconfig.entity.FieldConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 字段配置表dao接口
 *
 * @author 生成器生成
 * @date 2020-06-15 10:57:46
 */
@Mapper
public interface FieldConfigDao {

    /**
     * 添加记录
     *
     * @param fieldConfig 对象实体
     */
    void insertDataFieldConfig(FieldConfig fieldConfig);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataFieldConfigOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataFieldConfigMany(String value);

    /**
     * 修改记录
     *
     * @param fieldConfig 对象实体
     */
    void updateDataFieldConfig(FieldConfig fieldConfig);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<FieldConfig> findDataFieldConfigByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataFieldConfigByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    FieldConfig findDataFieldConfigByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<FieldConfig> findDataFieldConfig();

    /***
     * 根据步骤编号查询信息
     * @param step_no 步骤编号
     * @return
     */
    List<FieldConfig> findDataFieldConfigByStepNo(@Param("step_no") String step_no);

    /***
     * 根据步骤编号删除信息
     * @param step_no 步骤编号
     * @return
     */
    void deleteDataFieldConfigOneByStepNo(@Param("step_no") String step_no);
}
