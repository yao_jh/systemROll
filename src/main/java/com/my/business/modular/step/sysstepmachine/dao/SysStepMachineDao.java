package com.my.business.modular.step.sysstepmachine.dao;

import com.my.business.modular.step.sysstepmachine.entity.SysStepMachine;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 步骤磨床关系表dao接口
 *
 * @author 生成器生成
 * @date 2020-05-22 13:27:11
 */
@Mapper
public interface SysStepMachineDao {

    /**
     * 添加记录
     *
     * @param sysStepMachine 对象实体
     */
    void insertDataSysStepMachine(SysStepMachine sysStepMachine);

    /**
     * 根据流程编码删除对象
     *
     * @param work_no 流程编码
     */
    void deleteDataSysStepMachineOne(@Param("work_id") String work_no);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataSysStepMachineMany(String value);

    /**
     * 修改记录
     *
     * @param sysStepMachine 对象实体
     */
    void updateDataSysStepMachine(SysStepMachine sysStepMachine);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<SysStepMachine> findDataSysStepMachineByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataSysStepMachineByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    SysStepMachine findDataSysStepMachineByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<SysStepMachine> findDataSysStepMachine();

    /***
     * 根据步骤编码查询信息
     * @param step_no 步骤编码
     * @return
     */
    List<SysStepMachine> findDataSysStepMachineByStepId(@Param("step_id") String step_no);

    /***
     * 根据流程编码查询信息
     * @param work_no 流程编码
     * @return
     */
    List<SysStepMachine> findDataSysStepMachineByWorkId(@Param("work_id") String work_no);
}
