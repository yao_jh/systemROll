package com.my.business.modular.step.sysstepmachine.service;

import com.my.business.modular.step.sysstepmachine.entity.SysStepMachine;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 步骤磨床关系表接口服务类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:27:11
 */
public interface SysStepMachineService {

    /**
     * 添加记录
     *
     * @param sysStepMachine 数据
     * @param userId         用户id
     * @param sname          用户姓名
     */
    ResultData insertDataSysStepMachine(SysStepMachine sysStepMachine, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataSysStepMachineOne(String indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataSysStepMachineMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataSysStepMachine(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataSysStepMachineByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataSysStepMachineByIndocno(String data);

    /**
     * 查看记录
     */
    List<SysStepMachine> findDataSysStepMachine();
}
