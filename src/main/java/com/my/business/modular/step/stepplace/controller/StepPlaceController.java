package com.my.business.modular.step.stepplace.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.step.stepplace.entity.JStepPlace;
import com.my.business.modular.step.stepplace.entity.StepPlace;
import com.my.business.modular.step.stepplace.service.StepPlaceService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 步骤场地配置表控制器层
 *
 * @author 生成器生成
 * @date 2020-05-22 12:59:41
 */
@RestController
@RequestMapping("/stepPlace")
public class StepPlaceController {

    @Autowired
    private StepPlaceService stepPlaceService;

//	/**
//	 * 添加记录
//	 * @param userId 用户id
//     * @param sname 用户姓名
//	 * */
//	@CrossOrigin
//	@RequestMapping(value={"/insert"}, method=RequestMethod.POST)
//	@ResponseBody
//	public ResultData insertDataStepPlace(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
//		JCommon common = JSON.parseObject(loginToken,JCommon.class);
//    	String sname = common.getSname();
//    	Long userId = common.getUserId();
//		return stepPlaceService.insertDataStepPlace(data,userId,CodiUtil.returnLm(sname));
//	};

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataStepPlaceOne(@RequestBody String data) {
        try {
            JStepPlace jstepPlace = JSON.parseObject(data, JStepPlace.class);
            return stepPlaceService.deleteDataStepPlaceOne(jstepPlace.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JStepPlace jstepPlace = JSON.parseObject(data, JStepPlace.class);
            return stepPlaceService.deleteDataStepPlaceMany(jstepPlace.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataStepPlace(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return stepPlaceService.updateDataStepPlace(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataStepPlaceByPage(@RequestBody String data) {
        return stepPlaceService.findDataStepPlaceByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataStepPlaceByIndocno(@RequestBody String data) {
        return stepPlaceService.findDataStepPlaceByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<StepPlace> findDataStepPlace() {
        return stepPlaceService.findDataStepPlace();
    }

}
