package com.my.business.modular.step.stepuser.service;

import com.my.business.modular.step.stepuser.entity.StepUser;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 步骤人员配置表接口服务类
 *
 * @author 生成器生成
 * @date 2020-05-22 12:54:05
 */
public interface StepUserService {

    /**
     * 添加记录
     *
     * @param stepUser 数据
     * @param userId   用户id
     * @param sname    用户姓名
     */
    ResultData insertDataStepUser(StepUser stepUser, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataStepUserOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataStepUserMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataStepUser(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataStepUserByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataStepUserByIndocno(String data);

    /**
     * 查看记录
     */
    List<StepUser> findDataStepUser();
}
