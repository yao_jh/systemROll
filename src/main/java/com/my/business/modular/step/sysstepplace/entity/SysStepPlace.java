package com.my.business.modular.step.sysstepplace.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 步骤场地关系表实体类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:24:44
 */
public class SysStepPlace extends BaseEntity {

    private Long indocno;  //主键
    private Long ilinkno;  //外键
    private String work_id;  //流程id
    private String step_id;  //步骤id
    private String place_id;  //场地id

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getIlinkno() {
        return this.ilinkno;
    }

    public void setIlinkno(Long ilinkno) {
        this.ilinkno = ilinkno;
    }

    public String getWork_id() {
        return this.work_id;
    }

    public void setWork_id(String work_id) {
        this.work_id = work_id;
    }

    public String getStep_id() {
        return this.step_id;
    }

    public void setStep_id(String step_id) {
        this.step_id = step_id;
    }

    public String getPlace_id() {
        return this.place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }


}