package com.my.business.modular.step.fieldconfig.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 字段配置表实体类
 *
 * @author 生成器生成
 * @date 2020-06-15 10:57:45
 */
public class FieldConfig extends BaseEntity {

    private Long indocno;  //主键
    private String step_no;  //步骤编码
    private String order_table;  //工单表名
    private String order_no;  //工单类型编码
    private String order_field;  //工单字段
    private String order_label;  //工单lable
    private String snote;  //说明

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getStep_no() {
        return this.step_no;
    }

    public void setStep_no(String step_no) {
        this.step_no = step_no;
    }

    public String getOrder_table() {
        return this.order_table;
    }

    public void setOrder_table(String order_table) {
        this.order_table = order_table;
    }

    public String getOrder_no() {
        return this.order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getOrder_field() {
        return this.order_field;
    }

    public void setOrder_field(String order_field) {
        this.order_field = order_field;
    }

    public String getOrder_label() {
        return this.order_label;
    }

    public void setOrder_label(String order_label) {
        this.order_label = order_label;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }


}