package com.my.business.modular.step.sysstepuser.service;

import com.my.business.modular.step.sysstepuser.entity.SysStepUser;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 步骤人员关系表接口服务类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:21:15
 */
public interface SysStepUserService {

    /**
     * 添加记录
     *
     * @param sysStepUser 数据
     * @param userId      用户id
     * @param sname       用户姓名
     */
    ResultData insertDataSysStepUser(SysStepUser sysStepUser, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataSysStepUserOne(String indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataSysStepUserMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataSysStepUser(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataSysStepUserByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataSysStepUserByIndocno(String data);

    /**
     * 查看记录
     */
    List<SysStepUser> findDataSysStepUser();
}
