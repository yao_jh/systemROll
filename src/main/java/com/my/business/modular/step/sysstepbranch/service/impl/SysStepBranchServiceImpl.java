package com.my.business.modular.step.sysstepbranch.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.step.sysstepbranch.dao.SysStepBranchDao;
import com.my.business.modular.step.sysstepbranch.entity.JSysStepBranch;
import com.my.business.modular.step.sysstepbranch.entity.SysStepBranch;
import com.my.business.modular.step.sysstepbranch.service.SysStepBranchService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 步骤分支关系表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-05-27 15:10:51
 */
@Service
public class SysStepBranchServiceImpl implements SysStepBranchService {

    @Autowired
    private SysStepBranchDao sysStepBranchDao;

    /**
     * 添加记录
     *
     * @param sysStepBranch 数据
     * @param userId        用户id
     * @param sname         用户姓名
     */
    public ResultData insertDataSysStepBranch(SysStepBranch sysStepBranch, Long userId, String sname) {
        try {
//			JSysStepBranch jsysStepBranch = JSON.parseObject(data,JSysStepBranch.class);
//    		SysStepBranch sysStepBranch = jsysStepBranch.getSysStepBranch();
            CodiUtil.newRecord(userId, sname, sysStepBranch);
            sysStepBranchDao.insertDataSysStepBranch(sysStepBranch);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataSysStepBranchOne(String indocno) {
        try {
            sysStepBranchDao.deleteDataSysStepBranchOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataSysStepBranchMany(String str_id) {
        try {
            String sql = "delete sys_step_branch where indocno in(" + str_id + ")";
            sysStepBranchDao.deleteDataSysStepBranchMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   对象实体
     */
    public ResultData updateDataSysStepBranch(String data, Long userId, String sname) {
        try {
            JSysStepBranch jsysStepBranch = JSON.parseObject(data, JSysStepBranch.class);
            SysStepBranch sysStepBranch = jsysStepBranch.getSysStepBranch();
            CodiUtil.editRecord(userId, sname, sysStepBranch);
            sysStepBranchDao.updateDataSysStepBranch(sysStepBranch);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysStepBranchByPage(String data) {
        try {
            JSysStepBranch jsysStepBranch = JSON.parseObject(data, JSysStepBranch.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jsysStepBranch.getPageIndex();
            Integer pageSize = jsysStepBranch.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jsysStepBranch.getCondition()) {
                jsonObject = JSON.parseObject(jsysStepBranch.getCondition().toString());
            }

            List<SysStepBranch> list = sysStepBranchDao.findDataSysStepBranchByPage(pageIndex, pageSize);
            Integer count = sysStepBranchDao.findDataSysStepBranchByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysStepBranchByIndocno(String data) {
        try {
            JSysStepBranch jsysStepBranch = JSON.parseObject(data, JSysStepBranch.class);
            Long indocno = jsysStepBranch.getIndocno();

            SysStepBranch sysStepBranch = sysStepBranchDao.findDataSysStepBranchByIndocno(indocno);
            return ResultData.ResultDataSuccess(sysStepBranch);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<SysStepBranch> findDataSysStepBranch() {
        List<SysStepBranch> list = sysStepBranchDao.findDataSysStepBranch();
        return list;
    }

}
