package com.my.business.modular.step.stepbranch.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.step.stepbranch.dao.StepBranchDao;
import com.my.business.modular.step.stepbranch.entity.JStepBranch;
import com.my.business.modular.step.stepbranch.entity.StepBranch;
import com.my.business.modular.step.stepbranch.service.StepBranchService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 分支配置表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-05-27 15:10:09
 */
@Service
public class StepBranchServiceImpl implements StepBranchService {

    @Autowired
    private StepBranchDao stepBranchDao;

    /**
     * 添加记录
     *
     * @param stepBranch 数据
     * @param userId     用户id
     * @param sname      用户姓名
     */
    public ResultData insertDataStepBranch(StepBranch stepBranch, Long userId, String sname) {
        try {
//			JStepBranch jstepBranch = JSON.parseObject(data,JStepBranch.class);
//    		StepBranch stepBranch = jstepBranch.getStepBranch();
            CodiUtil.newRecord(userId, sname, stepBranch);
            stepBranchDao.insertDataStepBranch(stepBranch);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataStepBranchOne(Long indocno) {
        try {
            stepBranchDao.deleteDataStepBranchOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataStepBranchMany(String str_id) {
        try {
            String sql = "delete step_branch where indocno in(" + str_id + ")";
            stepBranchDao.deleteDataStepBranchMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   对象实体
     */
    public ResultData updateDataStepBranch(String data, Long userId, String sname) {
        try {
            JStepBranch jstepBranch = JSON.parseObject(data, JStepBranch.class);
            StepBranch stepBranch = jstepBranch.getStepBranch();
            CodiUtil.editRecord(userId, sname, stepBranch);
            stepBranchDao.updateDataStepBranch(stepBranch);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataStepBranchByPage(String data) {
        try {
            JStepBranch jstepBranch = JSON.parseObject(data, JStepBranch.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jstepBranch.getPageIndex();
            Integer pageSize = jstepBranch.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jstepBranch.getCondition()) {
                jsonObject = JSON.parseObject(jstepBranch.getCondition().toString());
            }

            List<StepBranch> list = stepBranchDao.findDataStepBranchByPage(pageIndex, pageSize);
            Integer count = stepBranchDao.findDataStepBranchByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataStepBranchByIndocno(String data) {
        try {
            JStepBranch jstepBranch = JSON.parseObject(data, JStepBranch.class);
            Long indocno = jstepBranch.getIndocno();

            StepBranch stepBranch = stepBranchDao.findDataStepBranchByIndocno(indocno);
            return ResultData.ResultDataSuccess(stepBranch);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<StepBranch> findDataStepBranch() {
        List<StepBranch> list = stepBranchDao.findDataStepBranch();
        return list;
    }

}
