package com.my.business.modular.step.fieldconfig.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.step.fieldconfig.entity.FieldConfig;
import com.my.business.modular.step.fieldconfig.entity.JFieldConfig;
import com.my.business.modular.step.fieldconfig.service.FieldConfigService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 字段配置表控制器层
 *
 * @author 生成器生成
 * @date 2020-06-15 10:57:46
 */
@RestController
@RequestMapping("/fieldConfig")
public class FieldConfigController {

    @Autowired
    private FieldConfigService fieldConfigService;

//	/**
//	 * 添加记录
//	 * @param data userId 用户id sname 用户姓名
//	 * */
//	@CrossOrigin
//	@RequestMapping(value={"/insert"}, method=RequestMethod.POST)
//	@ResponseBody
//	public ResultData insertDataFieldConfig(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
//		JCommon common = JSON.parseObject(loginToken,JCommon.class);
//    	String sname = common.getSname();
//    	Long userId = common.getUserId();
//		return fieldConfigService.insertDataFieldConfig(data,userId,CodiUtil.returnLm(sname));
//	};

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataFieldConfigOne(@RequestBody String data) {
        try {
            JFieldConfig jfieldConfig = JSON.parseObject(data, JFieldConfig.class);
            return fieldConfigService.deleteDataFieldConfigOne(jfieldConfig.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JFieldConfig jfieldConfig = JSON.parseObject(data, JFieldConfig.class);
            return fieldConfigService.deleteDataFieldConfigMany(jfieldConfig.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataFieldConfig(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return fieldConfigService.updateDataFieldConfig(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataFieldConfigByPage(@RequestBody String data) {
        return fieldConfigService.findDataFieldConfigByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataFieldConfigByIndocno(@RequestBody String data) {
        return fieldConfigService.findDataFieldConfigByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<FieldConfig> findDataFieldConfig() {
        return fieldConfigService.findDataFieldConfig();
    }

}
