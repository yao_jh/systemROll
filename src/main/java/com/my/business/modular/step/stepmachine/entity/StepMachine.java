package com.my.business.modular.step.stepmachine.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 步骤磨床配置表实体类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:02:42
 */
public class StepMachine extends BaseEntity {

    private Long indocno;  //主键
    private String ilinkno; //外键、编码
    private Long itype;  //类型(自动or手动)
    private String machinename;  //磨床名称
    private Long machineid;  //磨床id
    private String snote;  //说明

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getItype() {
        return this.itype;
    }

    public void setItype(Long itype) {
        this.itype = itype;
    }

    public String getMachinename() {
        return this.machinename;
    }

    public void setMachinename(String machinename) {
        this.machinename = machinename;
    }

    public Long getMachineid() {
        return this.machineid;
    }

    public void setMachineid(Long machineid) {
        this.machineid = machineid;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public String getIlinkno() {
        return ilinkno;
    }

    public void setIlinkno(String ilinkno) {
        this.ilinkno = ilinkno;
    }
}