package com.my.business.modular.step.stepplace.service;

import com.my.business.modular.step.stepplace.entity.StepPlace;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 步骤场地配置表接口服务类
 *
 * @author 生成器生成
 * @date 2020-05-22 12:59:41
 */
public interface StepPlaceService {

    /**
     * 添加记录
     *
     * @param stepPlace 数据
     * @param userId    用户id
     * @param sname     用户姓名
     */
    ResultData insertDataStepPlace(StepPlace stepPlace, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataStepPlaceOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataStepPlaceMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataStepPlace(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataStepPlaceByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataStepPlaceByIndocno(String data);

    /**
     * 查看记录
     */
    List<StepPlace> findDataStepPlace();
}
