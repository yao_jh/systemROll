package com.my.business.modular.step.fieldconfig.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.step.fieldconfig.dao.FieldConfigDao;
import com.my.business.modular.step.fieldconfig.entity.FieldConfig;
import com.my.business.modular.step.fieldconfig.entity.JFieldConfig;
import com.my.business.modular.step.fieldconfig.service.FieldConfigService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 字段配置表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-06-15 10:57:46
 */
@Service
public class FieldConfigServiceImpl implements FieldConfigService {

    @Autowired
    private FieldConfigDao fieldConfigDao;

    /**
     * 添加记录
     *
     * @param fieldConfig 数据
     * @param userId      用户id
     * @param sname       用户姓名
     */
    public ResultData insertDataFieldConfig(FieldConfig fieldConfig, Long userId, String sname) {
        try {
//			JFieldConfig jfieldConfig = JSON.parseObject(data,JFieldConfig.class);
//    		FieldConfig fieldConfig = jfieldConfig.getFieldConfig();
            CodiUtil.newRecord(userId, sname, fieldConfig);
            fieldConfigDao.insertDataFieldConfig(fieldConfig);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataFieldConfigOne(Long indocno) {
        try {
            fieldConfigDao.deleteDataFieldConfigOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataFieldConfigMany(String str_id) {
        try {
            String sql = "delete field_config where indocno in(" + str_id + ")";
            fieldConfigDao.deleteDataFieldConfigMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataFieldConfig(String data, Long userId, String sname) {
        try {
            JFieldConfig jfieldConfig = JSON.parseObject(data, JFieldConfig.class);
            FieldConfig fieldConfig = jfieldConfig.getFieldConfig();
            CodiUtil.editRecord(userId, sname, fieldConfig);
            fieldConfigDao.updateDataFieldConfig(fieldConfig);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataFieldConfigByPage(String data) {
        try {
            JFieldConfig jfieldConfig = JSON.parseObject(data, JFieldConfig.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jfieldConfig.getPageIndex();
            Integer pageSize = jfieldConfig.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jfieldConfig.getCondition()) {
                jsonObject = JSON.parseObject(jfieldConfig.getCondition().toString());
            }

            List<FieldConfig> list = fieldConfigDao.findDataFieldConfigByPage(pageIndex, pageSize);
            Integer count = fieldConfigDao.findDataFieldConfigByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataFieldConfigByIndocno(String data) {
        try {
            JFieldConfig jfieldConfig = JSON.parseObject(data, JFieldConfig.class);
            Long indocno = jfieldConfig.getIndocno();

            FieldConfig fieldConfig = fieldConfigDao.findDataFieldConfigByIndocno(indocno);
            return ResultData.ResultDataSuccess(fieldConfig);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<FieldConfig> findDataFieldConfig() {
        List<FieldConfig> list = fieldConfigDao.findDataFieldConfig();
        return list;
    }

}
