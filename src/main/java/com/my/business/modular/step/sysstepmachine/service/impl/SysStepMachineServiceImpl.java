package com.my.business.modular.step.sysstepmachine.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.step.sysstepmachine.dao.SysStepMachineDao;
import com.my.business.modular.step.sysstepmachine.entity.JSysStepMachine;
import com.my.business.modular.step.sysstepmachine.entity.SysStepMachine;
import com.my.business.modular.step.sysstepmachine.service.SysStepMachineService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 步骤磨床关系表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:27:11
 */
@Service
public class SysStepMachineServiceImpl implements SysStepMachineService {

    @Autowired
    private SysStepMachineDao sysStepMachineDao;

    /**
     * 添加记录
     *
     * @param sysStepMachine 数据
     * @param userId         用户id
     * @param sname          用户姓名
     */
    public ResultData insertDataSysStepMachine(SysStepMachine sysStepMachine, Long userId, String sname) {
        try {
            CodiUtil.newRecord(userId, sname, sysStepMachine);
            sysStepMachineDao.insertDataSysStepMachine(sysStepMachine);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataSysStepMachineOne(String indocno) {
        try {
            sysStepMachineDao.deleteDataSysStepMachineOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataSysStepMachineMany(String str_id) {
        try {
            String sql = "delete sys_step_machine where indocno in(" + str_id + ")";
            sysStepMachineDao.deleteDataSysStepMachineMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   对象实体
     */
    public ResultData updateDataSysStepMachine(String data, Long userId, String sname) {
        try {
            JSysStepMachine jsysStepMachine = JSON.parseObject(data, JSysStepMachine.class);
            SysStepMachine sysStepMachine = jsysStepMachine.getSysStepMachine();
            CodiUtil.editRecord(userId, sname, sysStepMachine);
            sysStepMachineDao.updateDataSysStepMachine(sysStepMachine);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysStepMachineByPage(String data) {
        try {
            JSysStepMachine jsysStepMachine = JSON.parseObject(data, JSysStepMachine.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jsysStepMachine.getPageIndex();
            Integer pageSize = jsysStepMachine.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jsysStepMachine.getCondition()) {
                jsonObject = JSON.parseObject(jsysStepMachine.getCondition().toString());
            }

            List<SysStepMachine> list = sysStepMachineDao.findDataSysStepMachineByPage(pageIndex, pageSize);
            Integer count = sysStepMachineDao.findDataSysStepMachineByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysStepMachineByIndocno(String data) {
        try {
            JSysStepMachine jsysStepMachine = JSON.parseObject(data, JSysStepMachine.class);
            Long indocno = jsysStepMachine.getIndocno();

            SysStepMachine sysStepMachine = sysStepMachineDao.findDataSysStepMachineByIndocno(indocno);
            return ResultData.ResultDataSuccess(sysStepMachine);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<SysStepMachine> findDataSysStepMachine() {
        List<SysStepMachine> list = sysStepMachineDao.findDataSysStepMachine();
        return list;
    }

}
