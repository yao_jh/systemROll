package com.my.business.modular.step.sysstepuser.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.step.sysstepuser.dao.SysStepUserDao;
import com.my.business.modular.step.sysstepuser.entity.JSysStepUser;
import com.my.business.modular.step.sysstepuser.entity.SysStepUser;
import com.my.business.modular.step.sysstepuser.service.SysStepUserService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 步骤人员关系表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:21:15
 */
@Service
public class SysStepUserServiceImpl implements SysStepUserService {

    @Autowired
    private SysStepUserDao sysStepUserDao;

    /**
     * 添加记录
     *
     * @param sysStepUser 数据
     * @param userId      用户id
     * @param sname       用户姓名
     */
    public ResultData insertDataSysStepUser(SysStepUser sysStepUser, Long userId, String sname) {
        try {
            CodiUtil.newRecord(userId, sname, sysStepUser);
            sysStepUserDao.insertDataSysStepUser(sysStepUser);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataSysStepUserOne(String indocno) {
        try {
            sysStepUserDao.deleteDataSysStepUserOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataSysStepUserMany(String str_id) {
        try {
            String sql = "delete sys_step_user where indocno in(" + str_id + ")";
            sysStepUserDao.deleteDataSysStepUserMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataSysStepUser(String data, Long userId, String sname) {
        try {
            JSysStepUser jsysStepUser = JSON.parseObject(data, JSysStepUser.class);
            SysStepUser sysStepUser = jsysStepUser.getSysStepUser();
            CodiUtil.editRecord(userId, sname, sysStepUser);
            sysStepUserDao.updateDataSysStepUser(sysStepUser);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysStepUserByPage(String data) {
        try {
            JSysStepUser jsysStepUser = JSON.parseObject(data, JSysStepUser.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jsysStepUser.getPageIndex();
            Integer pageSize = jsysStepUser.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jsysStepUser.getCondition()) {
                jsonObject = JSON.parseObject(jsysStepUser.getCondition().toString());
            }

            List<SysStepUser> list = sysStepUserDao.findDataSysStepUserByPage(pageIndex, pageSize);
            Integer count = sysStepUserDao.findDataSysStepUserByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysStepUserByIndocno(String data) {
        try {
            JSysStepUser jsysStepUser = JSON.parseObject(data, JSysStepUser.class);
            Long indocno = jsysStepUser.getIndocno();

            SysStepUser sysStepUser = sysStepUserDao.findDataSysStepUserByIndocno(indocno);
            return ResultData.ResultDataSuccess(sysStepUser);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<SysStepUser> findDataSysStepUser() {
        List<SysStepUser> list = sysStepUserDao.findDataSysStepUser();
        return list;
    }

}
