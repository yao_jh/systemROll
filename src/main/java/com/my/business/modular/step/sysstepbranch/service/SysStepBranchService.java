package com.my.business.modular.step.sysstepbranch.service;

import com.my.business.modular.step.sysstepbranch.entity.SysStepBranch;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 步骤分支关系表接口服务类
 *
 * @author 生成器生成
 * @date 2020-05-27 15:10:51
 */
public interface SysStepBranchService {

    /**
     * 添加记录
     *
     * @param sysStepBranch 数据
     * @param userId        用户id
     * @param sname         用户姓名
     */
    ResultData insertDataSysStepBranch(SysStepBranch sysStepBranch, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataSysStepBranchOne(String indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataSysStepBranchMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataSysStepBranch(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataSysStepBranchByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataSysStepBranchByIndocno(String data);

    /**
     * 查看记录
     */
    List<SysStepBranch> findDataSysStepBranch();
}
