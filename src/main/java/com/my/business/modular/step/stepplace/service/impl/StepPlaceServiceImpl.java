package com.my.business.modular.step.stepplace.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.step.stepplace.dao.StepPlaceDao;
import com.my.business.modular.step.stepplace.entity.JStepPlace;
import com.my.business.modular.step.stepplace.entity.StepPlace;
import com.my.business.modular.step.stepplace.service.StepPlaceService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 步骤场地配置表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-05-22 12:59:41
 */
@Service
public class StepPlaceServiceImpl implements StepPlaceService {

    @Autowired
    private StepPlaceDao stepPlaceDao;

    /**
     * 添加记录
     *
     * @param stepPlace 数据
     * @param userId    用户id
     * @param sname     用户姓名
     */
    public ResultData insertDataStepPlace(StepPlace stepPlace, Long userId, String sname) {
        try {
            CodiUtil.newRecord(userId, sname, stepPlace);
            stepPlaceDao.insertDataStepPlace(stepPlace);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataStepPlaceOne(Long indocno) {
        try {
            stepPlaceDao.deleteDataStepPlaceOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataStepPlaceMany(String str_id) {
        try {
            String sql = "delete from step_place where indocno in(" + str_id + ")";
            stepPlaceDao.deleteDataStepPlaceMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataStepPlace(String data, Long userId, String sname) {
        try {
            JStepPlace jstepPlace = JSON.parseObject(data, JStepPlace.class);
            StepPlace stepPlace = jstepPlace.getStepPlace();
            CodiUtil.editRecord(userId, sname, stepPlace);
            stepPlaceDao.updateDataStepPlace(stepPlace);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataStepPlaceByPage(String data) {
        try {
            JStepPlace jstepPlace = JSON.parseObject(data, JStepPlace.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jstepPlace.getPageIndex();
            Integer pageSize = jstepPlace.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jstepPlace.getCondition()) {
                jsonObject = JSON.parseObject(jstepPlace.getCondition().toString());
            }

            List<StepPlace> list = stepPlaceDao.findDataStepPlaceByPage(pageIndex, pageSize);
            Integer count = stepPlaceDao.findDataStepPlaceByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataStepPlaceByIndocno(String data) {
        try {
            JStepPlace jstepPlace = JSON.parseObject(data, JStepPlace.class);
            Long indocno = jstepPlace.getIndocno();

            StepPlace stepPlace = stepPlaceDao.findDataStepPlaceByIndocno(indocno);
            return ResultData.ResultDataSuccess(stepPlace);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<StepPlace> findDataStepPlace() {
        List<StepPlace> list = stepPlaceDao.findDataStepPlace();
        return list;
    }

}
