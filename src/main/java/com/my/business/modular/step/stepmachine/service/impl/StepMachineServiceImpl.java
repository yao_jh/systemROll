package com.my.business.modular.step.stepmachine.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.step.stepmachine.dao.StepMachineDao;
import com.my.business.modular.step.stepmachine.entity.JStepMachine;
import com.my.business.modular.step.stepmachine.entity.StepMachine;
import com.my.business.modular.step.stepmachine.service.StepMachineService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 步骤磨床配置表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:02:43
 */
@Service
public class StepMachineServiceImpl implements StepMachineService {

    @Autowired
    private StepMachineDao stepMachineDao;

    /**
     * 添加记录
     *
     * @param stepMachine 数据
     * @param userId      用户id
     * @param sname       用户姓名
     */
    public ResultData insertDataStepMachine(StepMachine stepMachine, Long userId, String sname) {
        try {
            CodiUtil.newRecord(userId, sname, stepMachine);
            stepMachineDao.insertDataStepMachine(stepMachine);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataStepMachineOne(Long indocno) {
        try {
            stepMachineDao.deleteDataStepMachineOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataStepMachineMany(String str_id) {
        try {
            String sql = "delete step_machine where indocno in(" + str_id + ")";
            stepMachineDao.deleteDataStepMachineMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataStepMachine(String data, Long userId, String sname) {
        try {
            JStepMachine jstepMachine = JSON.parseObject(data, JStepMachine.class);
            StepMachine stepMachine = jstepMachine.getStepMachine();
            CodiUtil.editRecord(userId, sname, stepMachine);
            stepMachineDao.updateDataStepMachine(stepMachine);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataStepMachineByPage(String data) {
        try {
            JStepMachine jstepMachine = JSON.parseObject(data, JStepMachine.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jstepMachine.getPageIndex();
            Integer pageSize = jstepMachine.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jstepMachine.getCondition()) {
                jsonObject = JSON.parseObject(jstepMachine.getCondition().toString());
            }

            List<StepMachine> list = stepMachineDao.findDataStepMachineByPage(pageIndex, pageSize);
            Integer count = stepMachineDao.findDataStepMachineByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataStepMachineByIndocno(String data) {
        try {
            JStepMachine jstepMachine = JSON.parseObject(data, JStepMachine.class);
            Long indocno = jstepMachine.getIndocno();

            StepMachine stepMachine = stepMachineDao.findDataStepMachineByIndocno(indocno);
            return ResultData.ResultDataSuccess(stepMachine);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<StepMachine> findDataStepMachine() {
        List<StepMachine> list = stepMachineDao.findDataStepMachine();
        return list;
    }

}
