package com.my.business.modular.step.stepuser.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.step.stepuser.entity.JStepUser;
import com.my.business.modular.step.stepuser.entity.StepUser;
import com.my.business.modular.step.stepuser.service.StepUserService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 步骤人员配置表控制器层
 *
 * @author 生成器生成
 * @date 2020-05-22 12:54:05
 */
@RestController
@RequestMapping("/stepUser")
public class StepUserController {

    @Autowired
    private StepUserService stepUserService;

//	/**
//	 * 添加记录
//	 * @param userId 用户id
//     * @param sname 用户姓名
//	 * */
//	@CrossOrigin
//	@RequestMapping(value={"/insert"}, method=RequestMethod.POST)
//	@ResponseBody
//	public ResultData insertDataStepUser(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
//		JCommon common = JSON.parseObject(loginToken,JCommon.class);
//    	String sname = common.getSname();
//    	Long userId = common.getUserId();
//		return stepUserService.insertDataStepUser(data,userId,CodiUtil.returnLm(sname));
//	};

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataStepUserOne(@RequestBody String data) {
        try {
            JStepUser jstepUser = JSON.parseObject(data, JStepUser.class);
            return stepUserService.deleteDataStepUserOne(jstepUser.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JStepUser jstepUser = JSON.parseObject(data, JStepUser.class);
            return stepUserService.deleteDataStepUserMany(jstepUser.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataStepUser(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return stepUserService.updateDataStepUser(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataStepUserByPage(@RequestBody String data) {
        return stepUserService.findDataStepUserByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataStepUserByIndocno(@RequestBody String data) {
        return stepUserService.findDataStepUserByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<StepUser> findDataStepUser() {
        return stepUserService.findDataStepUser();
    }

}
