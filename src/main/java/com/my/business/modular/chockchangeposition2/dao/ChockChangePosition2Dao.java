package com.my.business.modular.chockchangeposition2.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.chockchangeposition2.entity.ChockChangePosition2;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 轴承座换区工单——支撑辊2250dao接口
 * @author  生成器生成
 * @date 2020-10-27 10:45:17
 * */
@Mapper
public interface ChockChangePosition2Dao {

	/**
	 * 添加记录
	 * @param chockChangePosition2  对象实体
	 * */
	public void insertDataChockChangePosition2(ChockChangePosition2 chockChangePosition2);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataChockChangePosition2One(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataChockChangePosition2Many(String value);
	
	/**
	 * 修改记录
	 * @param chockChangePosition2  对象实体
	 * */
	public void updateDataChockChangePosition2(ChockChangePosition2 chockChangePosition2);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<ChockChangePosition2> findDataChockChangePosition2ByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataChockChangePosition2ByPageSize();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public ChockChangePosition2 findDataChockChangePosition2ByIndocno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<ChockChangePosition2> findDataChockChangePosition2();
	
}
