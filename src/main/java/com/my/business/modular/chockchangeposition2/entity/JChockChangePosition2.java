package com.my.business.modular.chockchangeposition2.entity;

import com.my.business.sys.common.entity.JCommon;

/**
 * 轴承座换区工单——支撑辊2250json的实体类
 *
 * @author 生成器生成
 * @date 2020-10-27 10:45:17
 */
public class JChockChangePosition2 extends JCommon {

    private Integer pageIndex; // 第几页
    private Integer pageSize; // 每页多少数据
    private Object condition; // 查询条件
    private ChockChangePosition2 chockChangePosition2;   //对应模块的实体类
    private Long indocno;
    private String str_indocno;

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Object getCondition() {
        return condition;
    }

    public void setCondition(Object condition) {
        this.condition = condition;
    }

    public ChockChangePosition2 getChockChangePosition2() {
        return chockChangePosition2;
    }

    public void setChockChangePosition2(ChockChangePosition2 chockChangePosition2) {
        this.chockChangePosition2 = chockChangePosition2;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getStr_indocno() {
        return str_indocno;
    }

    public void setStr_indocno(String str_indocno) {
        this.str_indocno = str_indocno;
    }
}