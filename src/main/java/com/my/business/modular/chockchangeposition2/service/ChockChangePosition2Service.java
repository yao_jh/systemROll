package com.my.business.modular.chockchangeposition2.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.modular.chockchangeposition2.entity.ChockChangePosition2;
import java.util.List;

/**
* 轴承座换区工单——支撑辊2250接口服务类
* @author  生成器生成
* @date 2020-10-27 10:45:17
*/
public interface ChockChangePosition2Service {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataChockChangePosition2(String data, Long userId, String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataChockChangePosition2One(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataChockChangePosition2Many(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataChockChangePosition2(String data, Long userId, String sname);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataChockChangePosition2ByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataChockChangePosition2ByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<ChockChangePosition2> findDataChockChangePosition2();
}
