package com.my.business.modular.chockchangeposition2.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.chockchangeposition2.entity.ChockChangePosition2;
import com.my.business.modular.chockchangeposition2.entity.JChockChangePosition2;
import com.my.business.modular.chockchangeposition2.dao.ChockChangePosition2Dao;
import com.my.business.modular.chockchangeposition2.service.ChockChangePosition2Service;
import com.my.business.sys.common.entity.ResultData;

/**
* 轴承座换区工单——支撑辊2250接口具体实现类
* @author  生成器生成
* @date 2020-10-27 10:45:17
*/
@Service
public class ChockChangePosition2ServiceImpl implements ChockChangePosition2Service {
	
	@Autowired
	private ChockChangePosition2Dao chockChangePosition2Dao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataChockChangePosition2(String data,Long userId,String sname){
		try{
			JChockChangePosition2 jchockChangePosition2 = JSON.parseObject(data,JChockChangePosition2.class);
    		ChockChangePosition2 chockChangePosition2 = jchockChangePosition2.getChockChangePosition2();
    		CodiUtil.newRecord(userId,sname,chockChangePosition2);
            chockChangePosition2Dao.insertDataChockChangePosition2(chockChangePosition2);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataChockChangePosition2One(Long indocno){
		try {
            chockChangePosition2Dao.deleteDataChockChangePosition2One(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataChockChangePosition2Many(String str_id) {
        try {
        	String sql = "delete chock_change_position2 where indocno in(" + str_id +")";
            chockChangePosition2Dao.deleteDataChockChangePosition2Many(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param data sysUser 对象实体
     */
	public ResultData updateDataChockChangePosition2(String data,Long userId,String sname){
		try {
			JChockChangePosition2 jchockChangePosition2 = JSON.parseObject(data,JChockChangePosition2.class);
    		ChockChangePosition2 chockChangePosition2 = jchockChangePosition2.getChockChangePosition2();
        	CodiUtil.editRecord(userId,sname,chockChangePosition2);
            chockChangePosition2Dao.updateDataChockChangePosition2(chockChangePosition2);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataChockChangePosition2ByPage(String data) {
        try {
        	JChockChangePosition2 jchockChangePosition2 = JSON.parseObject(data, JChockChangePosition2.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jchockChangePosition2.getPageIndex();
        	Integer pageSize = jchockChangePosition2.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jchockChangePosition2.getCondition()){
    			jsonObject  = JSON.parseObject(jchockChangePosition2.getCondition().toString());
    		}
    
    		List<ChockChangePosition2> list = chockChangePosition2Dao.findDataChockChangePosition2ByPage((pageIndex-1)*pageSize, pageSize);
    		Integer count = chockChangePosition2Dao.findDataChockChangePosition2ByPageSize();
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataChockChangePosition2ByIndocno(String data) {
        try {
        	JChockChangePosition2 jchockChangePosition2 = JSON.parseObject(data, JChockChangePosition2.class); 
        	Long indocno = jchockChangePosition2.getIndocno();
        	
    		ChockChangePosition2 chockChangePosition2 = chockChangePosition2Dao.findDataChockChangePosition2ByIndocno(indocno);
    		return ResultData.ResultDataSuccess(chockChangePosition2);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<ChockChangePosition2> findDataChockChangePosition2(){
		List<ChockChangePosition2> list = chockChangePosition2Dao.findDataChockChangePosition2();
		return list;
	};
}
