package com.my.business.modular.rollprelisthistory.service;

import com.my.business.modular.rollprelisthistory.entity.RollPrelistHistory;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 备辊操作历史接口服务类
 *
 * @author 生成器生成
 * @date 2020-09-15 16:34:16
 */
public interface RollPrelistHistoryService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollPrelistHistory(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollPrelistHistoryOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollPrelistHistoryMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollPrelistHistory(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPrelistHistoryByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPrelistHistoryByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollPrelistHistory> findDataRollPrelistHistory();
}
