package com.my.business.modular.rollprelisthistory.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 备辊操作历史实体类
 *
 * @author 生成器生成
 * @date 2020-09-15 16:34:15
 */
public class RollPrelistHistory extends BaseEntity {

    private Long indocno;  //主键
    private Long frame_noid;  //机架id
    private String frame_no;  //机架
    private String roll_no;  //辊号
    private Long dotype;  //操作类型
    private String reasonid;  //原因id
    private String reason;  //原因
    private Long if_grind;  //是否重磨
    private String pretime;  //备辊时间
    private String roll_type;  //轧辊类型

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getFrame_noid() {
        return this.frame_noid;
    }

    public void setFrame_noid(Long frame_noid) {
        this.frame_noid = frame_noid;
    }

    public String getFrame_no() {
        return this.frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public Long getDotype() {
        return this.dotype;
    }

    public void setDotype(Long dotype) {
        this.dotype = dotype;
    }

    public String getReasonid() {
        return this.reasonid;
    }

    public void setReasonid(String reasonid) {
        this.reasonid = reasonid;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getIf_grind() {
        return this.if_grind;
    }

    public void setIf_grind(Long if_grind) {
        this.if_grind = if_grind;
    }

    public String getPretime() {
        return pretime;
    }

    public void setPretime(String pretime) {
        this.pretime = pretime;
    }

    public String getRoll_type() {
        return roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }
}