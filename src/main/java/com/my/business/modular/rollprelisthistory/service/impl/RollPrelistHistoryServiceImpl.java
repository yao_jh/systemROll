package com.my.business.modular.rollprelisthistory.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rckurumainfo.dao.RcKurumaInfoDao;
import com.my.business.modular.rckurumainfo.entity.RcKurumaInfo;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.modular.rollpaired.dao.RollPairedDao;
import com.my.business.modular.rollprelisthistory.dao.RollPrelistHistoryDao;
import com.my.business.modular.rollprelisthistory.entity.JRollPrelistHistory;
import com.my.business.modular.rollprelisthistory.entity.RollPrelistHistory;
import com.my.business.modular.rollprelisthistory.service.RollPrelistHistoryService;
import com.my.business.modular.rollprelisthistoryr.dao.RollPrelistHistoryRDao;
import com.my.business.schedule.AllKafka;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 备辊操作历史接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-15 16:34:16
 */
@CommonsLog
@Service
public class RollPrelistHistoryServiceImpl implements RollPrelistHistoryService {

    private final static Log logger = LogFactory.getLog(RollPrelistHistoryServiceImpl.class);
    @Autowired
    private RollPrelistHistoryDao rollPrelistHistoryDao;
    @Autowired
    private RcKurumaInfoDao rcKurumaInfoDao;
    @Autowired
    private RollInformationDao rollInformationDao;
    @Autowired
    private RollPairedDao rollPairedDao;
    @Autowired
    private RollPrelistHistoryRDao rollPrelistHistoryRDao;
    @Autowired
    private AllKafka allKafka;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollPrelistHistory(String data, Long userId, String sname) {
        try {
            JRollPrelistHistory jrollPrelistHistory = JSON.parseObject(data, JRollPrelistHistory.class);
            RollPrelistHistory rollPrelistHistory = jrollPrelistHistory.getRollPrelistHistory();
            CodiUtil.newRecord(userId, sname, rollPrelistHistory);

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            List<String> roll_no = Arrays.asList(rollPrelistHistory.getRoll_no().split("/"));
            String roll_no1 = roll_no.get(0);  //获取上辊辊号
            String roll_no2 = roll_no.get(1);  //获取下辊辊号
            if (rollPrelistHistory.getDotype() == 1L) {
                RollInformation roll1 = rollInformationDao.findDataRollInformationByRollNo(roll_no1);  //获取上辊详情
                if (!StringUtils.isEmpty(roll_no1) && !StringUtils.isEmpty(roll1)) {
                    RcKurumaInfo rc1 = new RcKurumaInfo();
                    rc1.setStandno(rollPrelistHistory.getFrame_no());   //机架
                    rc1.setRoll_id(roll_no1);   //辊号
                    if (!StringUtils.isEmpty(roll1.getBearingchock_no())) {
                        List<String> bearingchock_no = Arrays.asList(roll1.getBearingchock_no().split("-"));
                        rc1.setChock_os(bearingchock_no.get(0));    //OS侧座
                        rc1.setChock_ds(bearingchock_no.get(1));    //DS侧座
                    }
                    rc1.setRoll_type(roll1.getRoll_type());     //轧辊类型
                    if (!StringUtils.isEmpty(roll1.getCurrentdiameter())) {
                        rc1.setRoll_diam(Float.valueOf(String.valueOf(roll1.getCurrentdiameter())));  //辊身直径 对应当前辊径
                    } else {
                        rc1.setRoll_diam(0.00f);
                    }
                    //rc1.setRoll_mat(roll1.getMaterial().substring(roll1.getMaterial().indexOf("(") + 1, roll1.getMaterial().indexOf(")")));       //材质
                    rc1.setRoll_mat(roll1.getMaterial());       //材质
                    rc1.setHssusedcounter(roll1.getUplinecount());       //上线次数
                    if (!StringUtils.isEmpty(roll1.getLastlowlinetime()) && !StringUtils.isEmpty(roll1.getLastuplinetime())) {
                        long diff = formatter.parse(roll1.getLastuplinetime()).getTime() - new Date().getTime();
                        long hour = diff % (1000 * 24 * 60 * 60) / (1000 * 60 * 60) + diff / (1000 * 24 * 60 * 60) * 24;
                        rc1.setHssofflinetime_tot((float) (hour));   //上线间隔时长
                    }
                    if (!StringUtils.isEmpty(roll1.getIni_crown_max())) {
                        rc1.setIni_crown_max(Float.valueOf(String.valueOf(roll1.getIni_crown_max())));
                    } else {
                        rc1.setIni_crown_max(0.00f);
                    }
                    if (!StringUtils.isEmpty(roll1.getIni_crown_min())) {
                        rc1.setIni_crown_min(Float.valueOf(String.valueOf(roll1.getIni_crown_min())));
                    } else {
                        rc1.setIni_crown_min(0.00f);
                    }
                    if (roll1.getRoll_type().contains("立辊") || roll1.getRoll_type().contains("锤头")) {
                        rc1.setRoll_position("OS");
                    } else {
                        rc1.setRoll_position("TOP");
                    }
                    //rcKurumaInfoDao.deleteDataRcKurumaInfoByrollNo(rc1.getStandno(),rc1.getRoll_position(),rc1.getRoll_type());
                    //rcKurumaInfoDao.insertDataRcKurumaInfo(rc1);
                    rcKurumaInfoDao.updateDataRcKurumaInfoByPre(rc1);
                    //周转状态改为在换辊车
                    rollInformationDao.updateDataRollInformationByRevoleve(roll_no1, 3L);
                    rollPairedDao.updateRollRrevolve();

                }
                RollInformation roll2 = rollInformationDao.findDataRollInformationByRollNo(roll_no2);  //获取下辊详情
                if (!StringUtils.isEmpty(roll_no2) && !StringUtils.isEmpty(roll2)) {
                    RcKurumaInfo rc2 = new RcKurumaInfo();
                    rc2.setStandno(rollPrelistHistory.getFrame_no());   //机架
                    rc2.setRoll_id(roll_no2);   //辊号
                    if (!StringUtils.isEmpty(roll2.getBearingchock_no())) {
                        List<String> bearingchock_no2 = Arrays.asList(roll2.getBearingchock_no().split("-"));
                        rc2.setChock_os(bearingchock_no2.get(0));    //OS侧座
                        rc2.setChock_ds(bearingchock_no2.get(1));    //DS侧座
                    }
                    rc2.setRoll_type(roll2.getRoll_type());     //轧辊类型
                    if (!StringUtils.isEmpty(roll2.getCurrentdiameter())) {
                        rc2.setRoll_diam(Float.valueOf(String.valueOf(roll2.getCurrentdiameter())));  //辊身直径 对应当前辊径
                    } else {
                        rc2.setRoll_diam(0.00f);
                    }
                    //rc2.setRoll_mat(roll2.getMaterial().substring(roll2.getMaterial().indexOf("(") + 1, roll2.getMaterial().indexOf(")")));       //材质
                    rc2.setRoll_mat(roll2.getMaterial());       //材质
                    rc2.setHssusedcounter(roll2.getUplinecount());       //上线次数
                    if (!StringUtils.isEmpty(roll2.getLastlowlinetime()) && !StringUtils.isEmpty(roll2.getLastuplinetime())) {
                        rc2.setHssofflinetime_tot((float) (formatter.parse(roll2.getLastlowlinetime()).getTime() - formatter.parse(roll2.getLastuplinetime()).getTime()));   //上线间隔时长
                    }
                    if (!StringUtils.isEmpty(roll2.getIni_crown_max())) {
                        rc2.setIni_crown_max(Float.valueOf(String.valueOf(roll2.getIni_crown_max())));
                    } else {
                        rc2.setIni_crown_max(0.00f);
                    }
                    if (!StringUtils.isEmpty(roll2.getIni_crown_min())) {
                        rc2.setIni_crown_min(Float.valueOf(String.valueOf(roll2.getIni_crown_min())));
                    } else {
                        rc2.setIni_crown_min(0.00f);
                    }
                    if (roll2.getRoll_type().contains("立辊") || roll2.getRoll_type().contains("锤头")) {
                        rc2.setRoll_position("DS");
                    } else {
                        rc2.setRoll_position("BOT");
                    }
                    rcKurumaInfoDao.updateDataRcKurumaInfoByPre(rc2);
                    //rcKurumaInfoDao.deleteDataRcKurumaInfoByrollNo(rc2.getStandno(),rc2.getRoll_position(),rc2.getRoll_type());
                    //rcKurumaInfoDao.insertDataRcKurumaInfo(rc2);
                    //周转状态改为在换辊车
                    rollInformationDao.updateDataRollInformationByRevoleve(roll_no2, 3L);
                    rollPairedDao.updateRollRrevolve();

                }
                //放在备辊车上时，状态改成-1
                rollPrelistHistoryRDao.updateidotype(rollPrelistHistory.getRoll_no());
            } else {
                RcKurumaInfo rc1 = new RcKurumaInfo();
                RcKurumaInfo rc2 = new RcKurumaInfo();
                rc1.setRoll_id(null);
                rc2.setRoll_id(null);
                rc1.setStandno(rollPrelistHistory.getFrame_no());   //机架
                rc2.setStandno(rollPrelistHistory.getFrame_no());   //机架
                if (rollPrelistHistory.getRoll_type().contains("立辊") || rollPrelistHistory.getRoll_type().contains("锤头")) {
                    rc1.setRoll_position("OS");
                    rc2.setRoll_position("DS");
                } else {
                    rc1.setRoll_position("TOP");
                    rc2.setRoll_position("BOT");
                }
                rc1.setRoll_type(rollPrelistHistory.getRoll_type());
                rc2.setRoll_type(rollPrelistHistory.getRoll_type());
                rcKurumaInfoDao.updateDataRcKurumaInfoByPre(rc1);
                rcKurumaInfoDao.updateDataRcKurumaInfoByPre(rc2);

                if (jrollPrelistHistory.getIfcm() == 1) {  //表示重磨
                    //周转状态改为待上机
                    if (!StringUtils.isEmpty(roll_no1)) {
                        rollInformationDao.updateDataRollInformationByRevoleve(roll_no1, 6L);
                    }
                    //周转状态改为待上机
                    if (!StringUtils.isEmpty(roll_no2)) {
                        rollInformationDao.updateDataRollInformationByRevoleve(roll_no2, 6L);
                    }
                } else {  //表示不重磨
                    //周转状态改为待上机
                    if (!StringUtils.isEmpty(roll_no1)) {
                        rollInformationDao.updateDataRollInformationByRevoleve(roll_no1, 4L);
                    }
                    //周转状态改为待上机
                    if (!StringUtils.isEmpty(roll_no2)) {
                        rollInformationDao.updateDataRollInformationByRevoleve(roll_no2, 4L);
                    }

                }
                rollPairedDao.updateRollRrevolve();
            }
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollPrelistHistoryOne(Long indocno) {
        try {
            rollPrelistHistoryDao.deleteDataRollPrelistHistoryOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollPrelistHistoryMany(String str_id) {
        try {
            String sql = "delete from roll_prelist_history where indocno in(" + str_id + ")";
            rollPrelistHistoryDao.deleteDataRollPrelistHistoryMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollPrelistHistory(String data, Long userId, String sname) {
        try {
            JRollPrelistHistory jrollPrelistHistory = JSON.parseObject(data, JRollPrelistHistory.class);
            RollPrelistHistory rollPrelistHistory = jrollPrelistHistory.getRollPrelistHistory();
            CodiUtil.editRecord(userId, sname, rollPrelistHistory);
            rollPrelistHistoryDao.updateDataRollPrelistHistory(rollPrelistHistory);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPrelistHistoryByPage(String data) {
        try {
            JRollPrelistHistory jrollPrelistHistory = JSON.parseObject(data, JRollPrelistHistory.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollPrelistHistory.getPageIndex();
            Integer pageSize = jrollPrelistHistory.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollPrelistHistory.getCondition()) {
                jsonObject = JSON.parseObject(jrollPrelistHistory.getCondition().toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

            List<RollPrelistHistory> list = rollPrelistHistoryDao.findDataRollPrelistHistoryByPage((pageIndex - 1) * pageSize, pageSize, roll_no);
            Integer count = rollPrelistHistoryDao.findDataRollPrelistHistoryByPageSize(roll_no);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPrelistHistoryByIndocno(String data) {
        try {
            JRollPrelistHistory jrollPrelistHistory = JSON.parseObject(data, JRollPrelistHistory.class);
            Long indocno = jrollPrelistHistory.getIndocno();

            RollPrelistHistory rollPrelistHistory = rollPrelistHistoryDao.findDataRollPrelistHistoryByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollPrelistHistory);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollPrelistHistory> findDataRollPrelistHistory() {
        List<RollPrelistHistory> list = rollPrelistHistoryDao.findDataRollPrelistHistory();
        return list;
    }

}
