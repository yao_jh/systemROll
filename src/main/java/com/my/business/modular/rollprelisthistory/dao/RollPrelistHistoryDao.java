package com.my.business.modular.rollprelisthistory.dao;

import com.my.business.modular.rollprelisthistory.entity.RollPrelistHistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 备辊操作历史dao接口
 *
 * @author 生成器生成
 * @date 2020-09-15 16:34:16
 */
@Mapper
public interface RollPrelistHistoryDao {

    /**
     * 添加记录
     *
     * @param rollPrelistHistory 对象实体
     */
    void insertDataRollPrelistHistory(RollPrelistHistory rollPrelistHistory);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollPrelistHistoryOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollPrelistHistoryMany(String value);

    /**
     * 修改记录
     *
     * @param rollPrelistHistory 对象实体
     */
    void updateDataRollPrelistHistory(RollPrelistHistory rollPrelistHistory);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollPrelistHistory> findDataRollPrelistHistoryByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("roll_no") String roll_no);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollPrelistHistoryByPageSize(@Param("roll_no") String roll_no);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollPrelistHistory findDataRollPrelistHistoryByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollPrelistHistory> findDataRollPrelistHistory();

}
