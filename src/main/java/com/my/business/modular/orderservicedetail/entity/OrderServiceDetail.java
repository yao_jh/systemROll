package com.my.business.modular.orderservicedetail.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 业务单据工单配置表实体类
 * 
 * @author 生成器生成
 * @date 2020-09-25 17:00:46
 */
public class OrderServiceDetail extends BaseEntity {

	private Long indocno; // 主键
	private Long ilinkno; // 外键
	private Long roll_typeid; // 轧辊类型id
	private String roll_type; // 轧辊类型
	private String production_line; // 产线
	private Long production_line_id; // 产线id
	private Long factory_id; // 生产厂家id
	private String factory; // 生产厂家
	private Long material_id; // 材质id
	private String material; // 材质
	private Long frame_noid; // 机架号id
	private String frame_no; // 机架号
	private Long order_id; // 关联工单主键
	private String order_name; // 关联工单名称
	private Long install_location_id; // 安装位置id
	private String install_location; // 安装位置
	private Long up_location_id; // 上机位置id
	private String up_location; // 上机位置
	private Long framerangeid; // 机架范围id
	private String framerange; // 机架范围

	public void setIndocno(Long indocno) {
		this.indocno = indocno;
	}

	public Long getIndocno() {
		return this.indocno;
	}

	public void setIlinkno(Long ilinkno) {
		this.ilinkno = ilinkno;
	}

	public Long getIlinkno() {
		return this.ilinkno;
	}

	public void setProduction_line(String production_line) {
		this.production_line = production_line;
	}

	public String getProduction_line() {
		return this.production_line;
	}

	public void setProduction_line_id(Long production_line_id) {
		this.production_line_id = production_line_id;
	}

	public Long getProduction_line_id() {
		return this.production_line_id;
	}

	public void setRoll_typeid(Long roll_typeid) {
		this.roll_typeid = roll_typeid;
	}

	public Long getRoll_typeid() {
		return this.roll_typeid;
	}

	public void setRoll_type(String roll_type) {
		this.roll_type = roll_type;
	}

	public String getRoll_type() {
		return this.roll_type;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getMaterial() {
		return this.material;
	}

	public void setFrame_no(String frame_no) {
		this.frame_no = frame_no;
	}

	public String getFrame_no() {
		return this.frame_no;
	}

	public void setFrame_noid(Long frame_noid) {
		this.frame_noid = frame_noid;
	}

	public Long getFrame_noid() {
		return this.frame_noid;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}

	public String getFactory() {
		return this.factory;
	}

	public void setOrder_id(Long order_id) {
		this.order_id = order_id;
	}

	public Long getOrder_id() {
		return this.order_id;
	}

	public void setOrder_name(String order_name) {
		this.order_name = order_name;
	}

	public String getOrder_name() {
		return this.order_name;
	}

	public Long getFactory_id() {
		return factory_id;
	}

	public void setFactory_id(Long factory_id) {
		this.factory_id = factory_id;
	}

	public Long getMaterial_id() {
		return material_id;
	}

	public void setMaterial_id(Long material_id) {
		this.material_id = material_id;
	}

	public Long getInstall_location_id() {
		return install_location_id;
	}

	public void setInstall_location_id(Long install_location_id) {
		this.install_location_id = install_location_id;
	}

	public String getInstall_location() {
		return install_location;
	}

	public void setInstall_location(String install_location) {
		this.install_location = install_location;
	}

	public Long getUp_location_id() {
		return up_location_id;
	}

	public void setUp_location_id(Long up_location_id) {
		this.up_location_id = up_location_id;
	}

	public String getUp_location() {
		return up_location;
	}

	public void setUp_location(String up_location) {
		this.up_location = up_location;
	}

	public Long getFramerangeid() {
		return framerangeid;
	}

	public void setFramerangeid(Long framerangeid) {
		this.framerangeid = framerangeid;
	}

	public String getFramerange() {
		return framerange;
	}

	public void setFramerange(String framerange) {
		this.framerange = framerange;
	}
}