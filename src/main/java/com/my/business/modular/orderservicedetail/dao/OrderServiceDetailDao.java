package com.my.business.modular.orderservicedetail.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.orderservicedetail.entity.OrderServiceDetail;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 业务单据工单配置表dao接口
 * @author  生成器生成
 * @date 2020-09-25 17:00:46
 * */
@Mapper
public interface OrderServiceDetailDao {

	/**
	 * 添加记录
	 * @param orderServiceDetail  对象实体
	 * */
	public void insertDataOrderServiceDetail(OrderServiceDetail orderServiceDetail);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataOrderServiceDetailOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataOrderServiceDetailMany(String value);
	
	/**
	 * 修改记录
	 * @param orderServiceDetail  对象实体
	 * */
	public void updateDataOrderServiceDetail(OrderServiceDetail orderServiceDetail);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<OrderServiceDetail> findDataOrderServiceDetailByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize,@Param("ilinkno") String ilinkno);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataOrderServiceDetailByPageSize(@Param("ilinkno") String ilinkno);
    
    
    public Long findOrderId(@Param("ilinkno") Long ilinkno,@Param("production_line_id") String production_line_id,@Param("roll_typeid") String roll_typeid,
    		@Param("material_id") String material_id,@Param("frame_noid") String frame_noid,@Param("factory_id") String factory_id,
    		@Param("install_location_id") String install_location_id,@Param("up_location_id") String up_location_id,@Param("framerangeid") String framerangeid);
    
    
    public Long findOrderIdByparam(@Param("indocno") Long indocno);
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public OrderServiceDetail findDataOrderServiceDetailByIndocno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<OrderServiceDetail> findDataOrderServiceDetail();
	
}
