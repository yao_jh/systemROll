package com.my.business.modular.orderservicedetail.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.modular.orderservicedetail.entity.OrderServiceDetail;
import com.my.business.modular.orderservicedetail.entity.JOrderServiceDetail;
import com.my.business.modular.orderservicedetail.service.OrderServiceDetailService;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import com.my.business.sys.common.entity.ResultData;
import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.my.business.util.CodiUtil;
import org.springframework.web.bind.annotation.RequestHeader;


/**
* 业务单据工单配置表控制器层
* @author  生成器生成
* @date 2020-09-25 17:00:46
*/
@RestController
@RequestMapping("/orderServiceDetail")
public class OrderServiceDetailController {

	@Autowired
	private OrderServiceDetailService orderServiceDetailService;
	
	/**
	 * 添加记录
	 * @param userId 用户id
     * @param sname 用户姓名
	 * */
	@CrossOrigin
	@RequestMapping(value={"/insert"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData insertDataOrderServiceDetail(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return orderServiceDetailService.insertDataOrderServiceDetail(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
	 * 根据主键删除对象
	 * @param data json字符串
	 * */
	@CrossOrigin
	@RequestMapping(value={"/deleteOne"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData deleteDataOrderServiceDetailOne(@RequestBody String data){
		try{
    		JOrderServiceDetail jorderServiceDetail = JSON.parseObject(data,JOrderServiceDetail.class);
    		return orderServiceDetailService.deleteDataOrderServiceDetailOne(jorderServiceDetail.getIndocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
    	try{
    		JOrderServiceDetail jorderServiceDetail = JSON.parseObject(data,JOrderServiceDetail.class);
    		return orderServiceDetailService.deleteDataOrderServiceDetailMany(jorderServiceDetail.getStr_indocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
    }
	
	/**
     * 修改记录
     * @param data json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
	@RequestMapping(value={"/update"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData updateDataOrderServiceDetail(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return orderServiceDetailService.updateDataOrderServiceDetail(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
     * 分页查看记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataOrderServiceDetailByPage(@RequestBody String data) {
        return orderServiceDetailService.findDataOrderServiceDetailByPage(data);
    }
    
    /**
     * 根据主键查询单条记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataOrderServiceDetailByIndocno(@RequestBody String data) {
        return orderServiceDetailService.findDataOrderServiceDetailByIndocno(data);
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	@CrossOrigin
	@RequestMapping(value={"/findAll"}, method=RequestMethod.POST)
	public List<OrderServiceDetail> findDataOrderServiceDetail(){
		return orderServiceDetailService.findDataOrderServiceDetail();
	};
}
