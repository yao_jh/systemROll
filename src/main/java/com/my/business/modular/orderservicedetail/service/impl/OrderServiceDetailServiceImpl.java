package com.my.business.modular.orderservicedetail.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.orderservicedetail.entity.OrderServiceDetail;
import com.my.business.modular.orderservicedetail.entity.JOrderServiceDetail;
import com.my.business.modular.orderservicedetail.dao.OrderServiceDetailDao;
import com.my.business.modular.orderservicedetail.service.OrderServiceDetailService;
import com.my.business.sys.common.entity.ResultData;

/**
* 业务单据工单配置表接口具体实现类
* @author  生成器生成
* @date 2020-09-25 17:00:46
*/
@Service
public class OrderServiceDetailServiceImpl implements OrderServiceDetailService {
	
	@Autowired
	private OrderServiceDetailDao orderServiceDetailDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataOrderServiceDetail(String data,Long userId,String sname){
		try{
			JOrderServiceDetail jorderServiceDetail = JSON.parseObject(data,JOrderServiceDetail.class);
    		OrderServiceDetail orderServiceDetail = jorderServiceDetail.getOrderServiceDetail();
    		CodiUtil.newRecord(userId,sname,orderServiceDetail);
            orderServiceDetailDao.insertDataOrderServiceDetail(orderServiceDetail);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataOrderServiceDetailOne(Long indocno){
		try {
            orderServiceDetailDao.deleteDataOrderServiceDetailOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataOrderServiceDetailMany(String str_id) {
        try {
        	String sql = "delete from order_service_detail where indocno in(" + str_id +")";
            orderServiceDetailDao.deleteDataOrderServiceDetailMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataOrderServiceDetail(String data,Long userId,String sname){
		try {
			JOrderServiceDetail jorderServiceDetail = JSON.parseObject(data,JOrderServiceDetail.class);
    		OrderServiceDetail orderServiceDetail = jorderServiceDetail.getOrderServiceDetail();
        	CodiUtil.editRecord(userId,sname,orderServiceDetail);
            orderServiceDetailDao.updateDataOrderServiceDetail(orderServiceDetail);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataOrderServiceDetailByPage(String data) {
        try {
        	JOrderServiceDetail jorderServiceDetail = JSON.parseObject(data, JOrderServiceDetail.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jorderServiceDetail.getPageIndex();
        	Integer pageSize = jorderServiceDetail.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jorderServiceDetail.getCondition()){
    			jsonObject  = JSON.parseObject(jorderServiceDetail.getCondition().toString());
    		}
        	
        	String ilinkno = null;
            if (!StringUtils.isEmpty(jsonObject.get("ilinkno"))) {
            	ilinkno = jsonObject.get("ilinkno").toString();
            }
    
    		List<OrderServiceDetail> list = orderServiceDetailDao.findDataOrderServiceDetailByPage((pageIndex-1)*pageSize, pageSize,ilinkno);
    		Integer count = orderServiceDetailDao.findDataOrderServiceDetailByPageSize(ilinkno);
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataOrderServiceDetailByIndocno(String data) {
        try {
        	JOrderServiceDetail jorderServiceDetail = JSON.parseObject(data, JOrderServiceDetail.class); 
        	Long indocno = jorderServiceDetail.getIndocno();
        	
    		OrderServiceDetail orderServiceDetail = orderServiceDetailDao.findDataOrderServiceDetailByIndocno(indocno);
    		return ResultData.ResultDataSuccess(orderServiceDetail);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<OrderServiceDetail> findDataOrderServiceDetail(){
		List<OrderServiceDetail> list = orderServiceDetailDao.findDataOrderServiceDetail();
		return list;
	};
}
