package com.my.business.modular.rollaccident.service;

import com.my.business.modular.rollaccident.entity.RollAccident;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 事故辊管理接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-11 16:49:18
 */
public interface RollAccidentService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollAccident(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollAccidentOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollAccidentMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollAccident(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollAccidentByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollAccidentByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollAccident> findDataRollAccident();

    /**
     * 状态变更
     *
     * @param data 参数字符串
     */
    ResultData scrapData(String data,Long v,Long V2);
}
