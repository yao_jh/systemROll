package com.my.business.modular.rollaccident.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollaccident.entity.JRollAccident;
import com.my.business.modular.rollaccident.entity.RollAccident;
import com.my.business.modular.rollaccident.service.RollAccidentService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 事故辊管理控制器层
 *
 * @author 生成器生成
 * @date 2020-08-11 16:49:18
 */
@RestController
@RequestMapping("/rollAccident")
public class RollAccidentController {

    @Autowired
    private RollAccidentService rollAccidentService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollAccident(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollAccidentService.insertDataRollAccident(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollAccidentOne(@RequestBody String data) {
        try {
            JRollAccident jrollAccident = JSON.parseObject(data, JRollAccident.class);
            return rollAccidentService.deleteDataRollAccidentOne(jrollAccident.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollAccident jrollAccident = JSON.parseObject(data, JRollAccident.class);
            return rollAccidentService.deleteDataRollAccidentMany(jrollAccident.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollAccident(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollAccidentService.updateDataRollAccident(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollAccidentByPage(@RequestBody String data) {
        return rollAccidentService.findDataRollAccidentByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollAccidentByIndocno(@RequestBody String data) {
        return rollAccidentService.findDataRollAccidentByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollAccident> findDataRollAccident() {
        return rollAccidentService.findDataRollAccident();
    }

    /**
     * 报废
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/scrap"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteScrap(@RequestBody String data) {
        try {
            return rollAccidentService.scrapData(data,6L,null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 质量异议
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/mass"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData mass(@RequestBody String data) {
        try {
            return rollAccidentService.scrapData(data,7L,null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 送外修复
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/repair"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData repair(@RequestBody String data) {
        try {
            return rollAccidentService.scrapData(data,4L,null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 开座
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/pair"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData pair(@RequestBody String data) {
        try {
            return rollAccidentService.scrapData(data,3L,8L);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 不开座
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/gr"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData gr(@RequestBody String data) {
        try {
            return rollAccidentService.scrapData(data,3L,6L);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 解锁
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/unlock"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData unlock(@RequestBody String data) {
        try {
            return rollAccidentService.scrapData(data,3L,null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }
}
