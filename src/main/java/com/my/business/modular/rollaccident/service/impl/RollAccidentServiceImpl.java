package com.my.business.modular.rollaccident.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollaccident.dao.RollAccidentDao;
import com.my.business.modular.rollaccident.entity.JRollAccident;
import com.my.business.modular.rollaccident.entity.RollAccident;
import com.my.business.modular.rollaccident.service.RollAccidentService;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 事故辊管理接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-11 16:49:18
 */
@Service
public class RollAccidentServiceImpl implements RollAccidentService {

    @Autowired
    private RollAccidentDao rollAccidentDao;
    @Autowired
    private RollInformationDao rollInformationDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollAccident(String data, Long userId, String sname) {
        try {
            JRollAccident jrollAccident = JSON.parseObject(data, JRollAccident.class);
            RollAccident rollAccident = jrollAccident.getRollAccident();
            CodiUtil.newRecord(userId, sname, rollAccident);
            rollAccidentDao.insertDataRollAccident(rollAccident);

            RollInformation rollInformation = rollInformationDao.findDataRollInformationByRollNo(rollAccident.getRoll_no());
            if (!StringUtils.isEmpty(rollInformation)) {
                rollInformation.setRoll_state(5L);
                rollInformationDao.updateDataRollInformation(rollInformation);
            }
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollAccidentOne(Long indocno) {
        try {
            rollAccidentDao.deleteDataRollAccidentOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollAccidentMany(String str_id) {
        try {
            String sql = "delete roll_accident where indocno in(" + str_id + ")";
            rollAccidentDao.deleteDataRollAccidentMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollAccident(String data, Long userId, String sname) {
        try {
            JRollAccident jrollAccident = JSON.parseObject(data, JRollAccident.class);
            RollAccident rollAccident = jrollAccident.getRollAccident();
            CodiUtil.editRecord(userId, sname, rollAccident);
            rollAccidentDao.updateDataRollAccident(rollAccident);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollAccidentByPage(String data) {
        try {
            JRollAccident jrollAccident = JSON.parseObject(data, JRollAccident.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollAccident.getPageIndex();
            Integer pageSize = jrollAccident.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollAccident.getCondition()) {
                jsonObject = JSON.parseObject(jrollAccident.getCondition().toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

            Long roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = Long.valueOf(jsonObject.get("roll_typeid").toString());
            }

            Long framerangeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("framerangeid"))) {
                framerangeid = Long.valueOf(jsonObject.get("framerangeid").toString());
            }

            Long iblockade = null;
            if (!StringUtils.isEmpty(jsonObject.get("iblockade"))) {
                iblockade = Long.valueOf(jsonObject.get("iblockade").toString());
            }

            String production_line = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line"))) {
            	production_line = jsonObject.get("production_line").toString();
            }
            
            String production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
            	production_line_id = jsonObject.get("production_line_id").toString();
            }

            List<RollAccident> list = rollAccidentDao.findDataRollAccidentByPage((pageIndex - 1) * pageSize, pageSize, production_line,production_line_id,roll_no, roll_typeid, framerangeid,iblockade);
            Integer count = rollAccidentDao.findDataRollAccidentByPageSize(production_line,production_line_id,roll_no, roll_typeid, framerangeid,iblockade);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollAccidentByIndocno(String data) {
        try {
            JRollAccident jrollAccident = JSON.parseObject(data, JRollAccident.class);
            Long indocno = jrollAccident.getIndocno();

            RollAccident rollAccident = rollAccidentDao.findDataRollAccidentByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollAccident);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollAccident> findDataRollAccident() {
        List<RollAccident> list = rollAccidentDao.findDataRollAccident();
        return list;
    }

    /**
     * 报废
     *
     * @param data 参数字符串
     */
    public ResultData scrapData(String data,Long v,Long v2) {
        try {
            JRollAccident jrollAccident = JSON.parseObject(data, JRollAccident.class);
            RollAccident rollAccident = jrollAccident.getRollAccident();
            String roll_no = rollAccident.getRoll_no();
            rollAccident.setIblockade(1L);
            rollAccidentDao.updateDataRollAccident(rollAccident);
            RollInformation rollInformation = rollInformationDao.findDataRollInformationByRollNo(roll_no);
            rollInformation.setRoll_state(v);
            if (v == 3L){
                if (v2 != null) {
                    rollInformation.setRoll_revolve(v2);
                }
            }
            rollInformationDao.updateDataRollInformation(rollInformation);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

}
