package com.my.business.modular.rollaccident.dao;

import com.my.business.modular.rollaccident.entity.RollAccident;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 事故辊管理dao接口
 *
 * @author 生成器生成
 * @date 2020-08-11 16:49:18
 */
@Mapper
public interface RollAccidentDao {

    /**
     * 添加记录
     *
     * @param rollAccident 对象实体
     */
    void insertDataRollAccident(RollAccident rollAccident);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollAccidentOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollAccidentMany(String value);

    /**
     * 修改记录
     *
     * @param rollAccident 对象实体
     */
    void updateDataRollAccident(RollAccident rollAccident);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollAccident> findDataRollAccidentByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("production_line") String production_line,@Param("production_line_id") String production_line_id,@Param("roll_no") String roll_no, @Param("roll_typeid") Long roll_typeid, @Param("framerangeid") Long framerangeid,@Param("iblockade") Long iblockade);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollAccidentByPageSize(@Param("production_line") String production_line,@Param("production_line_id") String production_line_id,@Param("roll_no") String roll_no, @Param("roll_typeid") Long roll_typeid, @Param("framerangeid") Long framerangeid,@Param("iblockade") Long iblockade);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollAccident findDataRollAccidentByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollAccident> findDataRollAccident();

}
