package com.my.business.modular.rollaccident.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 事故辊管理实体类
 *
 * @author 生成器生成
 * @date 2020-08-11 16:49:17
 */
public class RollAccident extends BaseEntity {

    private Long indocno;  //主键
    private String production_line;  //产线
    private Long production_line_id; //产线id
    private String roll_no;  //辊号
    private Long factory_id;
    private String factory;  //生产厂家
    private Long material_id;
    private String material;  //材质
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private Long framerangeid;  //机架范围id
    private String framerange;  //机架范围
    private Long trackcount;  //跟踪次数
    private Long checkcount;  //检测次数
    private Long iblockade;  //处理标记
    private String bearingchockno;  //轴承座配对号
    private String lockreason;  //封锁原因
    private String locktime;  //封锁时间
    private String unlocktime;  //解锁时间
    private Long operateid;  //操作人id
    private String operatename;  //操作人
    private String operatetime;  //操作时间

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getFactory() {
        return this.factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public String getMaterial() {
        return this.material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public Long getRoll_typeid() {
        return this.roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }


    public Long getTrackcount() {
        return this.trackcount;
    }

    public void setTrackcount(Long trackcount) {
        this.trackcount = trackcount;
    }

    public Long getCheckcount() {
        return this.checkcount;
    }

    public void setCheckcount(Long checkcount) {
        this.checkcount = checkcount;
    }

    public Long getIblockade() {
        return this.iblockade;
    }

    public void setIblockade(Long iblockade) {
        this.iblockade = iblockade;
    }

    public String getBearingchockno() {
        return this.bearingchockno;
    }

    public void setBearingchockno(String bearingchockno) {
        this.bearingchockno = bearingchockno;
    }

    public String getLockreason() {
        return this.lockreason;
    }

    public void setLockreason(String lockreason) {
        this.lockreason = lockreason;
    }

    public String getLocktime() {
        return this.locktime;
    }

    public void setLocktime(String locktime) {
        this.locktime = locktime;
    }

    public String getUnlocktime() {
        return this.unlocktime;
    }

    public void setUnlocktime(String unlocktime) {
        this.unlocktime = unlocktime;
    }

    public Long getOperateid() {
        return this.operateid;
    }

    public void setOperateid(Long operateid) {
        this.operateid = operateid;
    }

    public String getOperatename() {
        return this.operatename;
    }

    public void setOperatename(String operatename) {
        this.operatename = operatename;
    }

    public String getOperatetime() {
        return this.operatetime;
    }

    public void setOperatetime(String operatetime) {
        this.operatetime = operatetime;
    }

	public String getProduction_line() {
		return production_line;
	}

	public void setProduction_line(String production_line) {
		this.production_line = production_line;
	}

	public Long getProduction_line_id() {
		return production_line_id;
	}

	public void setProduction_line_id(Long production_line_id) {
		this.production_line_id = production_line_id;
	}

	public Long getFactory_id() {
		return factory_id;
	}

	public void setFactory_id(Long factory_id) {
		this.factory_id = factory_id;
	}

	public Long getMaterial_id() {
		return material_id;
	}

	public void setMaterial_id(Long material_id) {
		this.material_id = material_id;
	}

    public Long getFramerangeid() {
        return framerangeid;
    }

    public void setFramerangeid(Long framerangeid) {
        this.framerangeid = framerangeid;
    }

    public String getFramerange() {
        return framerange;
    }

    public void setFramerange(String framerange) {
        this.framerange = framerange;
    }
}