package com.my.business.modular.rollsparechangeinfo.dao;

import com.my.business.modular.rollsparechangeinfo.entity.RollSparechangeinfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 备件数量变动表dao接口
 *
 * @author 生成器生成
 * @date 2020-08-03 15:16:33
 */
@Mapper
public interface RollSparechangeinfoDao {

    /**
     * 添加记录
     *
     * @param rollSparechangeinfo 对象实体
     */
    void insertDataRollSparechangeinfo(RollSparechangeinfo rollSparechangeinfo);

    /**
     * 修改记录
     *
     * @param rollSparechangeinfo 对象实体
     */
    void updateDataRollSparechangeinfo(RollSparechangeinfo rollSparechangeinfo);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollSparechangeinfo> findDataRollSparechangeinfoByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollSparechangeinfoByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollSparechangeinfo findDataRollSparechangeinfoByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollSparechangeinfo> findDataRollSparechangeinfo();

}
