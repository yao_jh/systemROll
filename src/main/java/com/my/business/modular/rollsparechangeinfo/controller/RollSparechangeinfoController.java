package com.my.business.modular.rollsparechangeinfo.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollsparechangeinfo.entity.RollSparechangeinfo;
import com.my.business.modular.rollsparechangeinfo.service.RollSparechangeinfoService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 备件数量变动表控制器层
 *
 * @author 生成器生成
 * @date 2020-08-03 15:16:34
 */
@RestController
@RequestMapping("/rollSparechangeinfo")
public class RollSparechangeinfoController {

    @Autowired
    private RollSparechangeinfoService rollSparechangeinfoService;

    /**
     * 添加记录
     *
     * @param data       用户id
     * @param loginToken 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollSparechangeinfo(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollSparechangeinfoService.insertDataRollSparechangeinfo(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollSparechangeinfo(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollSparechangeinfoService.updateDataRollSparechangeinfo(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollSparechangeinfoByPage(@RequestBody String data) {
        return rollSparechangeinfoService.findDataRollSparechangeinfoByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollSparechangeinfoByIndocno(@RequestBody String data) {
        return rollSparechangeinfoService.findDataRollSparechangeinfoByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollSparechangeinfo> findDataRollSparechangeinfo() {
        return rollSparechangeinfoService.findDataRollSparechangeinfo();
    }
}
