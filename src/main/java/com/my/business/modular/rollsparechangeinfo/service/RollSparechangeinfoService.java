package com.my.business.modular.rollsparechangeinfo.service;

import com.my.business.modular.rollsparechangeinfo.entity.RollSparechangeinfo;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 备件数量变动表接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-03 15:16:33
 */
public interface RollSparechangeinfoService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollSparechangeinfo(String data, Long userId, String sname);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollSparechangeinfo(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollSparechangeinfoByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollSparechangeinfoByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollSparechangeinfo> findDataRollSparechangeinfo();
}
