package com.my.business.modular.rollsparechangeinfo.entity;

import com.my.business.sys.common.entity.BaseEntity;
import lombok.Data;

/**
 * 备件数量变动表实体类
 *
 * @author 生成器生成
 * @date 2020-08-03 15:16:32
 */
@Data
public class RollSparechangeinfo extends BaseEntity {

    private Long indocno;  //主键
    private Long spare_info_id;  //备件主键
    private int operation;  //操作类型（入库/出库，1/0）
    private Long change_quantity;  //变动数量
    private String snote;  //备注

}