package com.my.business.modular.rollsparechangeinfo.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollsparechangeinfo.dao.RollSparechangeinfoDao;
import com.my.business.modular.rollsparechangeinfo.entity.JRollSparechangeinfo;
import com.my.business.modular.rollsparechangeinfo.entity.RollSparechangeinfo;
import com.my.business.modular.rollsparechangeinfo.service.RollSparechangeinfoService;
import com.my.business.modular.rollsparepartinfo.service.RollSparepartinfoService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 备件数量变动表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-03 15:16:34
 */
@Service
public class RollSparechangeinfoServiceImpl implements RollSparechangeinfoService {

    private static final Integer OPERATION_PLUS = 1;// 操作 入库
    private static final Integer OPERATION_MINUS = 0;// 操作 出库
    @Autowired
    private RollSparechangeinfoDao rollSparechangeinfoDao;
    @Autowired
    private RollSparepartinfoService rollSparepartinfoService;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollSparechangeinfo(String data, Long userId, String sname) {
//		try{
//			JRollSparechangeinfo jrollSparechangeinfo = JSON.parseObject(data,JRollSparechangeinfo.class);
//    		RollSparechangeinfo rollSparechangeinfo = jrollSparechangeinfo.getRollSparechangeinfo();
//
//    		// 同步在 备件信息表 中更新仓库内存
//			Long spareInfoId = rollSparechangeinfo.getSpare_info_id();
//			RollSparepartinfo rollSparepartinfo = (RollSparepartinfo) rollSparepartinfoService.findDataRollSparepartinfoByIndocno(spareInfoId).getData();
//			int operation = rollSparechangeinfo.getOperation();
//			Long depositNumber = rollSparepartinfo.getDeposit_number();
//			Long changeNumber = rollSparechangeinfo.getChange_quantity();
//			if (operation == OPERATION_PLUS){
//				depositNumber = depositNumber + changeNumber;
//			}else if(operation == OPERATION_MINUS){
//				depositNumber = depositNumber - changeNumber;
//			}else{
//				return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + "输入的参数有误，operatio 只能是0 和 1", null);
//			}
//			rollSparepartinfo.setDeposit_number(depositNumber);
//			rollSparepartinfoService.updateRollSparepartinfo(rollSparepartinfo, userId, sname);
//    		CodiUtil.newRecord(userId,sname,rollSparechangeinfo);
//            rollSparechangeinfoDao.insertDataRollSparechangeinfo(rollSparechangeinfo);
//            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
//    	}catch(Exception e){
//    		e.printStackTrace();
//    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
//    	}
        return null;
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   对象实体
     */
    public ResultData updateDataRollSparechangeinfo(String data, Long userId, String sname) {
        try {
            JRollSparechangeinfo jrollSparechangeinfo = JSON.parseObject(data, JRollSparechangeinfo.class);
            RollSparechangeinfo rollSparechangeinfo = jrollSparechangeinfo.getRollSparechangeinfo();
            CodiUtil.editRecord(userId, sname, rollSparechangeinfo);
            rollSparechangeinfoDao.updateDataRollSparechangeinfo(rollSparechangeinfo);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollSparechangeinfoByPage(String data) {
        try {
            JRollSparechangeinfo jrollSparechangeinfo = JSON.parseObject(data, JRollSparechangeinfo.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollSparechangeinfo.getPageIndex();
            Integer pageSize = jrollSparechangeinfo.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollSparechangeinfo.getCondition()) {
                jsonObject = JSON.parseObject(jrollSparechangeinfo.getCondition().toString());
            }

            List<RollSparechangeinfo> list = rollSparechangeinfoDao.findDataRollSparechangeinfoByPage(pageIndex, pageSize);
            Integer count = rollSparechangeinfoDao.findDataRollSparechangeinfoByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollSparechangeinfoByIndocno(String data) {
        try {
            JRollSparechangeinfo jrollSparechangeinfo = JSON.parseObject(data, JRollSparechangeinfo.class);
            Long indocno = jrollSparechangeinfo.getIndocno();

            RollSparechangeinfo rollSparechangeinfo = rollSparechangeinfoDao.findDataRollSparechangeinfoByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollSparechangeinfo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollSparechangeinfo> findDataRollSparechangeinfo() {
        List<RollSparechangeinfo> list = rollSparechangeinfoDao.findDataRollSparechangeinfo();
        return list;
    }
}
