package com.my.business.modular.rollstiffness.service;

import com.my.business.modular.rollstiffness.entity.RollStiffness;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.dict.entity.DpdEntity;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.List;
import java.util.Map;

/**
 * 轧辊多变异分析接口服务类
 *
 * @author 生成器生成
 * @date 2020-09-08 11:24:48
 */
public interface RollStiffnessService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollStiffness(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollStiffnessOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollStiffnessMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollStiffness(String data, Long userId, String sname);

    /**
     * 修改记录
     *
     */
    void updateDataRollStiffnessByMath(RollStiffness rollStiffness);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollStiffnessByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollStiffnessByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollStiffness> findDataRollStiffness();

    /**
     * 图形化——支撑辊查询记录
     *
     * @param data 分页参数字符串
     */
    ResultData findEchartsB(String data);

    /**
     * 添加记录——信号表
     *
     * @param rollStiffness json字符串
     */
    ResultData insertDataRollStiffnessByMath(RollStiffness rollStiffness);

    /**
     * 图形化——时间总分查询记录
     *
     * @param data 分页参数字符串
     */
    ResultData findEchartsA(String data);

    /**
     * 表格总分查询记录
     *
     * @param data 分页参数字符串
     */
    ResultData findTable(String data);
    
    /**
     * 表格总分查询记录
     *
     * @param data 分页参数字符串
     */
    ResultData findTablenew(String data);

    /**
     * 导出excel
     *
     * @param data 分页参数字符串
     */
    String excel(HSSFWorkbook workbook, String recordtime,String production_line_id, String dbegin, String dend);

    /**
     * 时间段
     *
     * @param data 分页参数字符串
     */
    List<DpdEntity> findlist(String data);

    /**
     * 三级部门专用查询
     * @param data 数据
     * @return
     */
    ResultData findByThird(String data);

}
