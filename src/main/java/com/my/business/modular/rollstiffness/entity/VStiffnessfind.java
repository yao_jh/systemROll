package com.my.business.modular.rollstiffness.entity;

import java.util.List;

public class VStiffnessfind {

	private List<VStiffness> all_score; // 时间范围内总的得分
	private List<VStiffnessNew> list_array; // 集合数组

	public List<VStiffness> getAll_score() {
		return all_score;
	}

	public void setAll_score(List<VStiffness> all_score) {
		this.all_score = all_score;
	}

	public List<VStiffnessNew> getList_array() {
		return list_array;
	}

	public void setList_array(List<VStiffnessNew> list_array) {
		this.list_array = list_array;
	}

}
