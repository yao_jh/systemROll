package com.my.business.modular.rollstiffness.dao;

import com.my.business.modular.rollstiffness.entity.RollStiffness;
import com.my.business.modular.rollstiffness.entity.RollStiffnessEchartsB;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轧辊多变异分析dao接口
 *
 * @author 生成器生成
 * @date 2020-09-08 11:24:48
 */
@Mapper
public interface RollStiffnessDao {

    /**
     * 添加记录
     *
     * @param rollStiffness 对象实体
     */
    void insertDataRollStiffness(RollStiffness rollStiffness);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollStiffnessOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollStiffnessMany(String value);

    /**
     * 修改记录
     *
     * @param rollStiffness 对象实体
     */
    void updateDataRollStiffness(RollStiffness rollStiffness);

    /**
     * 修改记录
     *
     * @param rollStiffness 对象实体
     */
    void updateDataRollStiffnessA(RollStiffness rollStiffness);

    /**
     * 修改记录
     *
     * @param rollStiffness 对象实体
     */
    void updateDataRollStiffnessB(RollStiffness rollStiffness);

    /**
     * 更新总分
     *
     * @param rollStiffness 对象实体
     */
    void updateItotal(RollStiffness rollStiffness);
    /**
     * 分页查看查看记录
     *
     * @param pageIndex  第几页
     * @param pageSize   每页总数
     * @param dbegin     开始时间
     * @param dend       结束时间
     * @param frame_noid 机架号id
     * @param w_no       工作辊辊号组合
     * @param b_no       支撑辊辊号组合
     * @return 对象数据集合
     */
    List<RollStiffness> findDataRollStiffnessByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("dbegin") String dbegin, @Param("dend") String dend, @Param("frame_noid") Long frame_noid, @Param("w_no") String w_no, @Param("b_no") String b_no, @Param("production_line_id") Long production_line_id);

    /**
     * 根据条件查看记录的总数
     *
     * @param dbegin     开始时间
     * @param dend       结束时间
     * @param frame_noid 机架号id
     * @param w_no       工作辊辊号组合
     * @param b_no       支撑辊辊号组合
     * @return 对象数据集合
     */
    Integer findDataRollStiffnessByPageSize(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("frame_noid") Long frame_noid, @Param("w_no") String w_no, @Param("b_no") String b_no, @Param("production_line_id") Long production_line_id);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollStiffness findDataRollStiffnessByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollStiffness> findDataRollStiffness();

    /**
     * 图形化——支撑辊 根据条件查看工作辊轴承座号
     *
     * @param dbegin     开始时间
     * @param dend       结束时间
     * @param frame_noid 机架号id
     * @return 对象数据集合
     */
    List<RollStiffnessEchartsB> findEchartsbearingchock(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("frame_noid") Long frame_noid, @Param("production_line_id") Long production_line_id);

    /**
     * 图形化——支撑辊 根据条件查看支撑辊号
     *
     * @param dbegin     开始时间
     * @param dend       结束时间
     * @param frame_noid 机架号id
     * @return 对象数据集合
     */
    List<RollStiffnessEchartsB> findEchartsBNo(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("frame_noid") Long frame_noid, @Param("production_line_id") Long production_line_id);

    /**
     * 图形化——支撑辊 根据条件查看各项平均数
     *
     * @param dbegin     开始时间
     * @param dend       结束时间
     * @param frame_noid 机架号id
     * @param w_no       工作辊辊号组合
     * @param b_no       支撑辊辊号组合
     * @return 对象数据集合
     */
    RollStiffnessEchartsB findEchartsLast(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("frame_noid") Long frame_noid, @Param("w_no") String w_no, @Param("b_no") String b_no, @Param("bearingchock_no") String bearingchock_no, @Param("production_line_id") Long production_line_id);

    /**
     * 图形化——阶梯 根据条件查看阶梯位置
     *
     * @param dbegin     开始时间
     * @param dend       结束时间
     * @param frame_noid 机架号id
     * @return 对象数据集合
     */
    List<RollStiffnessEchartsB> findEchartsUpdownstep(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("frame_noid") Long frame_noid, @Param("production_line_id") Long production_line_id);

    /**
     * 图形化——支撑辊 根据条件查看各项平均数
     *
     * @param dbegin     开始时间
     * @param dend       结束时间
     * @param frame_noid 机架号id
     * @param w_no       工作辊辊号组合
     * @param updownstep 阶梯位置
     * @return 对象数据集合
     */
    RollStiffnessEchartsB findEchartsLastU(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("frame_noid") Long frame_noid, @Param("w_no") String w_no, @Param("updownstep") String updownstep, @Param("bearingchock_no") String bearingchock_no);

    /**
     * 查找最新的日期时间
     *
     * @return string
     */
    String findTime(@Param("production_line_id") Long production_line_id, @Param("frame_no") String frame_no);

    /**
     * 查找重复数量
     *
     * @param recordtime
     * @return
     */
    Integer findDataHave(@Param("recordtime") String recordtime, @Param("production_line_id") Long production_line_id, @Param("frame_no") String frame_no);

    /**
     * 查出条件范围内所有机架号
     *
     * @param dbegin 开始时间
     * @param dend   结束时间
     */
    List<RollStiffness> findframeno(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("frame_no") String frame_no, @Param("production_line_id") Long production_line_id);

    /**
     * 查出条件范围内时间点最小值
     *
     * @param dbegin 开始时间
     * @param dend   结束时间
     */
    String findminTime(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("frame_no") String frame_no, @Param("production_line_id") Long production_line_id);

    /**
     * 查出条件范围内时间点最大值
     *
     * @param dbegin 开始时间
     * @param dend   结束时间
     */

    String findmaxTime(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("frame_no") String frame_no, @Param("production_line_id") Long production_line_id);

    List<RollStiffness> findTable(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("production_line_id") String production_line_id);
    
    List<RollStiffness> findTableExcelone(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("production_line_id") String production_line_id, @Param("recordtime") String recordtime);
    
    
    
    List<RollStiffness> findTablenew(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("production_line_id") String production_line_id);
    
    List<RollStiffness> findTablenewSize(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("production_line_id") String production_line_id);
    
    Integer findTableSize(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("production_line_id") String production_line_id);
    
    
    List<RollStiffness> findTableByRecordtime(@Param("recordtime") String recordtime,@Param("production_line_id") String production_line_id);

    /**
     * 查出条件范围内各项信息
     *
     * @param dbegin   开始时间
     * @param dend     结束时间
     * @param frame_no 机架号
     */
    List<RollStiffness> findEchartsA(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("frame_no") String frame_no, @Param("production_line_id") Long production_line_id);

    /**
     * 查出条件范围内各项信息
     *
     * @param production_line_id 产线
     * @param frame_no           机架号
     * @param recordtime         时间
     */
    RollStiffness findDataRollStiffnessByMath(@Param("production_line_id") Long production_line_id, @Param("frame_no") String frame_no, @Param("recordtime") String recordtime);

    /**
     * 时间段
     * @param dbegin  开始时间
     * @param dend     结束时间
     * @param production_line_id 产线
     * @return 时间
     */
    List<RollStiffness> findlist(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("production_line_id") String production_line_id);

    /**
     * 根据机架号查询最新一条数据
     * @param frame_no 机架号
     * @return 实体类
     */
    RollStiffness findDataRollStiffnessByframenoNew(@Param("frame_no") String frame_no);

    /**
     * 查出条件范围内所有机架号
     *
     * @param dbegin 开始时间
     * @param dend   结束时间
     */
    List<RollStiffness> findByThird(String dbegin, String dend, Long frame_noid, String w_no, String b_no, Long production_line_id);
}
