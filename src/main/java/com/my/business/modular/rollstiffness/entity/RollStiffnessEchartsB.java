package com.my.business.modular.rollstiffness.entity;

/**
 * 轧辊多变异分析图形化——支撑辊实体类
 *
 * @author 生成器生成
 * @date 2020-09-08 11:24:47
 */
public class RollStiffnessEchartsB {

    private String bearingchock_no;  //工作辊轴承座号(上OS-上DS/下OS-下DS)
    private String b_no;  //支撑辊辊号
    private String updownstep;  //支撑辊辊号
    private Double itotal;  //总分
    private Double retention_score;  //刚度保持率得分
    private Double leveling_score;  //调平值得分
    private Double bothsides_stiffness_score;  //两侧刚度偏差得分
    private Double bothposition_stiffness_score;  //两侧位置偏差得分
    private Double osposition_score;  //OS位置偏差得分
    private Double dsposition_score;  //DS位置偏差得分
    private Double rolling_force_score;  //轧制力偏差得分
    private Double osrollgap_score;  //OS辊缝偏差得分
    private Double dsrollgap_score;  //DS辊缝偏差得分

    public String getBearingchock_no() {
        return bearingchock_no;
    }

    public void setBearingchock_no(String bearingchock_no) {
        this.bearingchock_no = bearingchock_no;
    }

    public String getB_no() {
        return b_no;
    }

    public void setB_no(String b_no) {
        this.b_no = b_no;
    }

    public String getUpdownstep() {
        return updownstep;
    }

    public void setUpdownstep(String updownstep) {
        this.updownstep = updownstep;
    }

    public Double getItotal() {
        return itotal;
    }

    public void setItotal(Double itotal) {
        this.itotal = itotal;
    }

    public Double getRetention_score() {
        return retention_score;
    }

    public void setRetention_score(Double retention_score) {
        this.retention_score = retention_score;
    }

    public Double getLeveling_score() {
        return leveling_score;
    }

    public void setLeveling_score(Double leveling_score) {
        this.leveling_score = leveling_score;
    }

    public Double getBothsides_stiffness_score() {
        return bothsides_stiffness_score;
    }

    public void setBothsides_stiffness_score(Double bothsides_stiffness_score) {
        this.bothsides_stiffness_score = bothsides_stiffness_score;
    }

    public Double getBothposition_stiffness_score() {
        return bothposition_stiffness_score;
    }

    public void setBothposition_stiffness_score(Double bothposition_stiffness_score) {
        this.bothposition_stiffness_score = bothposition_stiffness_score;
    }

    public Double getOsposition_score() {
        return osposition_score;
    }

    public void setOsposition_score(Double osposition_score) {
        this.osposition_score = osposition_score;
    }

    public Double getDsposition_score() {
        return dsposition_score;
    }

    public void setDsposition_score(Double dsposition_score) {
        this.dsposition_score = dsposition_score;
    }

    public Double getRolling_force_score() {
        return rolling_force_score;
    }

    public void setRolling_force_score(Double rolling_force_score) {
        this.rolling_force_score = rolling_force_score;
    }

    public Double getOsrollgap_score() {
        return osrollgap_score;
    }

    public void setOsrollgap_score(Double osrollgap_score) {
        this.osrollgap_score = osrollgap_score;
    }

    public Double getDsrollgap_score() {
        return dsrollgap_score;
    }

    public void setDsrollgap_score(Double dsrollgap_score) {
        this.dsrollgap_score = dsrollgap_score;
    }
}