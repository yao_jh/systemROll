package com.my.business.modular.rollstiffness.entity;

import java.util.List;

/**
 * 刚度视图新实体类
 *
 * @author 生成器生成
 * @date 2020-09-19 14:23:57
 */
public class VStiffnessNew {
	
	private String recordtime;  //时间
	private Double f1_scoreAll; //f1机架得分
	private Double f2_scoreAll; //f2机架得分
	private Double f3_scoreAll; //f3机架得分
	private Double f4_scoreAll; //f4机架得分
	private Double f5_scoreAll; //f5机架得分
	private Double f6_scoreAll; //f6机架得分
	private Double f7_scoreAll; //f7机架得分
	private Double r1_scoreAll; //f6机架得分
	private Double r2_scoreAll; //f7机架得分
	
	private List<VStiffness> data_array;  //页面下方的表格数据

	public String getRecordtime() {
		return recordtime;
	}

	public void setRecordtime(String recordtime) {
		this.recordtime = recordtime;
	}

	public Double getF1_scoreAll() {
		return f1_scoreAll;
	}

	public void setF1_scoreAll(Double f1_scoreAll) {
		this.f1_scoreAll = f1_scoreAll;
	}

	public Double getF2_scoreAll() {
		return f2_scoreAll;
	}

	public void setF2_scoreAll(Double f2_scoreAll) {
		this.f2_scoreAll = f2_scoreAll;
	}

	public Double getF3_scoreAll() {
		return f3_scoreAll;
	}

	public void setF3_scoreAll(Double f3_scoreAll) {
		this.f3_scoreAll = f3_scoreAll;
	}

	public Double getF4_scoreAll() {
		return f4_scoreAll;
	}

	public void setF4_scoreAll(Double f4_scoreAll) {
		this.f4_scoreAll = f4_scoreAll;
	}

	public Double getF5_scoreAll() {
		return f5_scoreAll;
	}

	public void setF5_scoreAll(Double f5_scoreAll) {
		this.f5_scoreAll = f5_scoreAll;
	}

	public Double getF6_scoreAll() {
		return f6_scoreAll;
	}

	public void setF6_scoreAll(Double f6_scoreAll) {
		this.f6_scoreAll = f6_scoreAll;
	}

	public Double getF7_scoreAll() {
		return f7_scoreAll;
	}

	public void setF7_scoreAll(Double f7_scoreAll) {
		this.f7_scoreAll = f7_scoreAll;
	}

	public List<VStiffness> getData_array() {
		return data_array;
	}

	public void setData_array(List<VStiffness> data_array) {
		this.data_array = data_array;
	}

	public Double getR1_scoreAll() {
		return r1_scoreAll;
	}

	public void setR1_scoreAll(Double r1_scoreAll) {
		this.r1_scoreAll = r1_scoreAll;
	}

	public Double getR2_scoreAll() {
		return r2_scoreAll;
	}

	public void setR2_scoreAll(Double r2_scoreAll) {
		this.r2_scoreAll = r2_scoreAll;
	}
	
}
