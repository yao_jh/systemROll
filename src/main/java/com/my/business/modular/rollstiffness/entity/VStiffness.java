package com.my.business.modular.rollstiffness.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 刚度视图实体类
 *
 * @author 生成器生成
 * @date 2020-09-19 14:23:57
 */
public class VStiffness extends BaseEntity {

    private String xm;  //项目
    private String unit;  //单位
    private String qz;  //权重
    private Double f1;
    private Double f1_score;
    private Double f2;
    private Double f2_score;
    private Double f3;
    private Double f3_score;
    private Double f4;
    private Double f4_score;
    private Double f5;
    private Double f5_score;
    private Double f6;
    private Double f6_score;
    private Double f7;
    private Double f7_score;
    private Double r1;
    private Double r1_score;
    private Double r2;
    private Double r2_score;


    private VStiffness() {
    }

    public VStiffness(String xm, String unit, String qz) {
        super();
        this.xm = xm;
        this.unit = unit;
        this.qz = qz;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getQz() {
        return qz;
    }

    public void setQz(String qz) {
        this.qz = qz;
    }

    public Double getF1() {
        return f1;
    }

    public void setF1(Double f1) {
        this.f1 = f1;
    }

    public Double getF1_score() {
        return f1_score;
    }

    public void setF1_score(Double f1_score) {
        this.f1_score = f1_score;
    }

    public Double getF2() {
        return f2;
    }

    public void setF2(Double f2) {
        this.f2 = f2;
    }

    public Double getF2_score() {
        return f2_score;
    }

    public void setF2_score(Double f2_score) {
        this.f2_score = f2_score;
    }

    public Double getF3() {
        return f3;
    }

    public void setF3(Double f3) {
        this.f3 = f3;
    }

    public Double getF3_score() {
        return f3_score;
    }

    public void setF3_score(Double f3_score) {
        this.f3_score = f3_score;
    }

    public Double getF4() {
        return f4;
    }

    public void setF4(Double f4) {
        this.f4 = f4;
    }

    public Double getF4_score() {
        return f4_score;
    }

    public void setF4_score(Double f4_score) {
        this.f4_score = f4_score;
    }

    public Double getF5() {
        return f5;
    }

    public void setF5(Double f5) {
        this.f5 = f5;
    }

    public Double getF5_score() {
        return f5_score;
    }

    public void setF5_score(Double f5_score) {
        this.f5_score = f5_score;
    }

    public Double getF6() {
        return f6;
    }

    public void setF6(Double f6) {
        this.f6 = f6;
    }

    public Double getF6_score() {
        return f6_score;
    }

    public void setF6_score(Double f6_score) {
        this.f6_score = f6_score;
    }

    public Double getF7() {
        return f7;
    }

    public void setF7(Double f7) {
        this.f7 = f7;
    }

    public Double getF7_score() {
        return f7_score;
    }

    public void setF7_score(Double f7_score) {
        this.f7_score = f7_score;
    }

    public Double getR1() {
        return r1;
    }

    public void setR1(Double r1) {
        this.r1 = r1;
    }

    public Double getR1_score() {
        return r1_score;
    }

    public void setR1_score(Double r1_score) {
        this.r1_score = r1_score;
    }

    public Double getR2() {
        return r2;
    }

    public void setR2(Double r2) {
        this.r2 = r2;
    }

    public Double getR2_score() {
        return r2_score;
    }

    public void setR2_score(Double r2_score) {
        this.r2_score = r2_score;
    }

}