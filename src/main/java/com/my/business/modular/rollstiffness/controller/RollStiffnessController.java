package com.my.business.modular.rollstiffness.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollstiffness.entity.JRollStiffness;
import com.my.business.modular.rollstiffness.entity.RollStiffness;
import com.my.business.modular.rollstiffness.service.RollStiffnessService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.dict.entity.DpdEntity;
import com.my.business.util.CodiUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

/**
 * 轧辊多变异分析控制器层
 *
 * @author 生成器生成
 * @date 2020-09-08 11:24:48
 */
@RestController
@RequestMapping("/rollStiffness")
public class RollStiffnessController {

    @Autowired
    private RollStiffnessService rollStiffnessService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollStiffness(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollStiffnessService.insertDataRollStiffness(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollStiffnessOne(@RequestBody String data) {
        try {
            JRollStiffness jrollStiffness = JSON.parseObject(data, JRollStiffness.class);
            return rollStiffnessService.deleteDataRollStiffnessOne(jrollStiffness.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollStiffness jrollStiffness = JSON.parseObject(data, JRollStiffness.class);
            return rollStiffnessService.deleteDataRollStiffnessMany(jrollStiffness.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollStiffness(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollStiffnessService.updateDataRollStiffness(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollStiffnessByPage(@RequestBody String data) {
        return rollStiffnessService.findDataRollStiffnessByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollStiffnessByIndocno(@RequestBody String data) {
        return rollStiffnessService.findDataRollStiffnessByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollStiffness> findDataRollStiffness() {
        return rollStiffnessService.findDataRollStiffness();
    }

    /**
     * 图形化——支撑辊查询接口
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findEchartsB"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findEchartsB(@RequestBody String data) {
        return rollStiffnessService.findEchartsB(data);
    }

    /**
     * 图形化——时间总分查询接口
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findEchartsA"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findEchartsA(@RequestBody String data) {
        return rollStiffnessService.findEchartsA(data);
    }


    /**
     * 表格查询接口
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findtableold"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findtable(@RequestBody String data) {
        return rollStiffnessService.findTable(data);
    }
    
    /**
     * 表格查询接口
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findtable"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findtablenew(@RequestBody String data) {
        return rollStiffnessService.findTablenew(data);
    }


    /**
     * 表格查询接口查询所有数据
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/excel"}, method = RequestMethod.GET)
    @ResponseBody
    public void getExcel(@RequestParam("production_line_id") String production_line_id, @RequestParam("dbegin") String dbegin, @RequestParam("dend") String dend, HttpServletResponse response, HttpServletRequest request) throws IOException {
    	String recordtime = null;
    	HSSFWorkbook workbook = new HSSFWorkbook();
        String fileName = "";//设置要导出的文件的名字
        fileName = rollStiffnessService.excel(workbook, recordtime,production_line_id, dbegin, dend);
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1) + ".xls");//URLEncoder.encode(fileName, "utf-8")
        workbook.write(response.getOutputStream());
    }
    
    /**
     * 表格查询接口,查询某一条数据
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/excelone"}, method = RequestMethod.GET)
    @ResponseBody
    public void getExcel(@RequestParam("recordtime") String recordtime,@RequestParam("production_line_id") String production_line_id, @RequestParam("dbegin") String dbegin, @RequestParam("dend") String dend, HttpServletResponse response, HttpServletRequest request) throws IOException {
    	HSSFWorkbook workbook = new HSSFWorkbook();
        String fileName = "";//设置要导出的文件的名字
        fileName = rollStiffnessService.excel(workbook, recordtime,production_line_id, dbegin, dend);
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1) + ".xls");//URLEncoder.encode(fileName, "utf-8")
        workbook.write(response.getOutputStream());
    }

    /**
     * 表格查询接口
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findlist"}, method = RequestMethod.POST)
    @ResponseBody
    public List<DpdEntity> findlist(@RequestBody String data) {
        return rollStiffnessService.findlist(data);
    }

    /**
     * 同步刚度数据用
     */
    @CrossOrigin
    @RequestMapping(value = {"/gangdu"}, method = RequestMethod.POST)
    @ResponseBody
    public void gangdu(@RequestBody String data){
        JRollStiffness jrollStiffness = JSON.parseObject(data, JRollStiffness.class);
        RollStiffness rollStiffness = jrollStiffness.getRollStiffness();
        rollStiffnessService.insertDataRollStiffnessByMath(rollStiffness);
    }

    /**
     * 三级部门其他项目专用
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByThird"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findByThird(@RequestBody String data) {
        return rollStiffnessService.findByThird(data);
    }
}
