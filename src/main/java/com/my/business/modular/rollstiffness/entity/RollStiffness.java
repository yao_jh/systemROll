package com.my.business.modular.rollstiffness.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轧辊多变异分析实体类
 *
 * @author 生成器生成
 * @date 2020-09-08 11:24:47
 */
public class RollStiffness extends BaseEntity {

    private Long indocno;  //主键
    private String roll_no;  //辊号
    private String factory;  //供应商
    private String material;  //材质
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private Long frame_noid;  //机架号id
    private String frame_no;  //机架号
    private String w_no;  //工作辊辊号(上/下)
    private String workroll_uptime;  //工作辊上机时间
    private String workroll_downtime;  //工作辊下机时间
    private String bearingchock_no;  //工作辊轴承座号(上OS-上DS/下OS-下DS)
    private String b_no;  //支撑辊辊号
    private String backuproll_uptime;  //支撑辊上机时间
    private String backuproll_downtime;  //支撑辊下机时间
    private String updownstep;  //上/下阶梯垫位置
    private Long production_line_id;  //产线id
    private String production_line;  //产线
    private String gapzeroontime;  //标定开始时间
    private String gapzeroofftime;  //标定结束时间
    private Double lcosstiffness;  //LC——OS侧刚度
    private Double lcdsstiffness;  //LC——DS侧刚度
    private Double ptosstiffness;  //PT——OS侧刚度
    private Double ptdsstiffness;  //PT——DS侧刚度
    private Double osenposition;  //OS入口位置
    private Double osexposition;  //OS出口位置
    private Double dsenposition;  //DS入口位置
    private Double dsexposition;  //DS出口位置
    private Double lcosforce;  //LC——OS侧轧制力
    private Double lcdsforce;  //LC——DS侧轧制力
    private Double ptosforce;  //PT——OS侧轧制力
    private Double ptdsforce;  //PT——DS侧轧制力
    private Double itotal;  //总分
    private Double retention;  //刚度保持率(%)
    private Double retention_score;  //刚度保持率得分
    private Double leveling;  //调平值(mm)
    private Double leveling_score;  //调平值得分
    private Double bothsides_stiffness;  //两侧刚度偏差(KN/mm)
    private Double bothsides_stiffness_score;  //两侧刚度偏差得分
    private Double bothposition_stiffness;  //两侧位置偏差(mm)
    private Double bothposition_stiffness_score;  //两侧位置偏差得分
    private Double osposition;  //OS位置偏差(mm)
    private Double osposition_score;  //OS位置偏差得分
    private Double dsposition;  //DS位置偏差(mm)
    private Double dsposition_score;  //DS位置偏差得分
    private Double rolling_force;  //轧制力偏差(KN)
    private Double rolling_force_score;  //轧制力偏差得分
    private Double osrollgap;  //OS辊缝偏差(mm)
    private Double osrollgap_score;  //OS辊缝偏差得分
    private Double dsrollgap;  //DS辊缝偏差(mm)
    private Double dsrollgap_score;  //DS辊缝偏差得分
    private String evaluation;  //轧机刚度评价
    private String recordtime; //标定时间
    private String snote;  //备注
    private String demarcate;   //发送时间
    private String recordtime_new;   //标定时间截取字段  数据库中没有
    private Double upstepwedge;  //上阶梯位实际值
    private Double dnstepwedge;  //下阶梯位实际值

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getFactory() {
        return this.factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public String getMaterial() {
        return this.material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public Long getRoll_typeid() {
        return this.roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public Long getFrame_noid() {
        return this.frame_noid;
    }

    public void setFrame_noid(Long frame_noid) {
        this.frame_noid = frame_noid;
    }

    public String getFrame_no() {
        return this.frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public String getW_no() {
        return this.w_no;
    }

    public void setW_no(String w_no) {
        this.w_no = w_no;
    }

    public String getWorkroll_uptime() {
        return this.workroll_uptime;
    }

    public void setWorkroll_uptime(String workroll_uptime) {
        this.workroll_uptime = workroll_uptime;
    }

    public String getWorkroll_downtime() {
        return this.workroll_downtime;
    }

    public void setWorkroll_downtime(String workroll_downtime) {
        this.workroll_downtime = workroll_downtime;
    }

    public String getBearingchock_no() {
        return this.bearingchock_no;
    }

    public void setBearingchock_no(String bearingchock_no) {
        this.bearingchock_no = bearingchock_no;
    }

    public String getB_no() {
        return this.b_no;
    }

    public void setB_no(String b_no) {
        this.b_no = b_no;
    }

    public String getBackuproll_uptime() {
        return this.backuproll_uptime;
    }

    public void setBackuproll_uptime(String backuproll_uptime) {
        this.backuproll_uptime = backuproll_uptime;
    }

    public String getBackuproll_downtime() {
        return this.backuproll_downtime;
    }

    public void setBackuproll_downtime(String backuproll_downtime) {
        this.backuproll_downtime = backuproll_downtime;
    }



    public Double getItotal() {
        return itotal;
    }

    public void setItotal(Double itotal) {
        this.itotal = itotal;
    }

    public Double getRetention() {
        return retention;
    }

    public void setRetention(Double retention) {
        this.retention = retention;
    }

    public Double getRetention_score() {
        return retention_score;
    }

    public void setRetention_score(Double retention_score) {
        this.retention_score = retention_score;
    }

    public Double getLeveling() {
        return leveling;
    }

    public void setLeveling(Double leveling) {
        this.leveling = leveling;
    }

    public Double getLeveling_score() {
        return leveling_score;
    }

    public void setLeveling_score(Double leveling_score) {
        this.leveling_score = leveling_score;
    }

    public Double getBothsides_stiffness() {
        return bothsides_stiffness;
    }

    public void setBothsides_stiffness(Double bothsides_stiffness) {
        this.bothsides_stiffness = bothsides_stiffness;
    }

    public Double getBothsides_stiffness_score() {
        return bothsides_stiffness_score;
    }

    public void setBothsides_stiffness_score(Double bothsides_stiffness_score) {
        this.bothsides_stiffness_score = bothsides_stiffness_score;
    }

    public Double getBothposition_stiffness() {
        return bothposition_stiffness;
    }

    public void setBothposition_stiffness(Double bothposition_stiffness) {
        this.bothposition_stiffness = bothposition_stiffness;
    }

    public Double getBothposition_stiffness_score() {
        return bothposition_stiffness_score;
    }

    public void setBothposition_stiffness_score(Double bothposition_stiffness_score) {
        this.bothposition_stiffness_score = bothposition_stiffness_score;
    }

    public Double getOsposition() {
        return osposition;
    }

    public void setOsposition(Double osposition) {
        this.osposition = osposition;
    }

    public Double getOsposition_score() {
        return osposition_score;
    }

    public void setOsposition_score(Double osposition_score) {
        this.osposition_score = osposition_score;
    }

    public Double getDsposition() {
        return dsposition;
    }

    public void setDsposition(Double dsposition) {
        this.dsposition = dsposition;
    }

    public Double getDsposition_score() {
        return dsposition_score;
    }

    public void setDsposition_score(Double dsposition_score) {
        this.dsposition_score = dsposition_score;
    }

    public Double getRolling_force() {
        return rolling_force;
    }

    public void setRolling_force(Double rolling_force) {
        this.rolling_force = rolling_force;
    }

    public Double getRolling_force_score() {
        return rolling_force_score;
    }

    public void setRolling_force_score(Double rolling_force_score) {
        this.rolling_force_score = rolling_force_score;
    }

    public Double getOsrollgap() {
        return osrollgap;
    }

    public void setOsrollgap(Double osrollgap) {
        this.osrollgap = osrollgap;
    }

    public Double getOsrollgap_score() {
        return osrollgap_score;
    }

    public void setOsrollgap_score(Double osrollgap_score) {
        this.osrollgap_score = osrollgap_score;
    }

    public Double getDsrollgap() {
        return dsrollgap;
    }

    public void setDsrollgap(Double dsrollgap) {
        this.dsrollgap = dsrollgap;
    }

    public Double getDsrollgap_score() {
        return dsrollgap_score;
    }

    public void setDsrollgap_score(Double dsrollgap_score) {
        this.dsrollgap_score = dsrollgap_score;
    }

    public String getEvaluation() {
        return this.evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public String getRecordtime() {
        return recordtime;
    }

    public void setRecordtime(String recordtime) {
        this.recordtime = recordtime;
    }

    public String getUpdownstep() {
        return updownstep;
    }

    public void setUpdownstep(String updownstep) {
        this.updownstep = updownstep;
    }

    public Long getProduction_line_id() {
        return production_line_id;
    }

    public void setProduction_line_id(Long production_line_id) {
        this.production_line_id = production_line_id;
    }

    public String getProduction_line() {
        return production_line;
    }

    public void setProduction_line(String production_line) {
        this.production_line = production_line;
    }

    public String getDemarcate() {
        return demarcate;
    }

    public void setDemarcate(String demarcate) {
        this.demarcate = demarcate;
    }

	public String getRecordtime_new() {
		return recordtime_new;
	}

	public void setRecordtime_new(String recordtime_new) {
		this.recordtime_new = recordtime_new;
	}

    public String getGapzeroontime() {
        return gapzeroontime;
    }

    public void setGapzeroontime(String gapzeroontime) {
        this.gapzeroontime = gapzeroontime;
    }

    public String getGapzeroofftime() {
        return gapzeroofftime;
    }

    public void setGapzeroofftime(String gapzeroofftime) {
        this.gapzeroofftime = gapzeroofftime;
    }

    public Double getLcosstiffness() {
        return lcosstiffness;
    }

    public void setLcosstiffness(Double lcosstiffness) {
        this.lcosstiffness = lcosstiffness;
    }

    public Double getLcdsstiffness() {
        return lcdsstiffness;
    }

    public void setLcdsstiffness(Double lcdsstiffness) {
        this.lcdsstiffness = lcdsstiffness;
    }

    public Double getPtosstiffness() {
        return ptosstiffness;
    }

    public void setPtosstiffness(Double ptosstiffness) {
        this.ptosstiffness = ptosstiffness;
    }

    public Double getPtdsstiffness() {
        return ptdsstiffness;
    }

    public void setPtdsstiffness(Double ptdsstiffness) {
        this.ptdsstiffness = ptdsstiffness;
    }

    public Double getOsenposition() {
        return osenposition;
    }

    public void setOsenposition(Double osenposition) {
        this.osenposition = osenposition;
    }

    public Double getOsexposition() {
        return osexposition;
    }

    public void setOsexposition(Double osexposition) {
        this.osexposition = osexposition;
    }

    public Double getDsenposition() {
        return dsenposition;
    }

    public void setDsenposition(Double dsenposition) {
        this.dsenposition = dsenposition;
    }

    public Double getDsexposition() {
        return dsexposition;
    }

    public void setDsexposition(Double dsexposition) {
        this.dsexposition = dsexposition;
    }

    public Double getLcosforce() {
        return lcosforce;
    }

    public void setLcosforce(Double lcosforce) {
        this.lcosforce = lcosforce;
    }

    public Double getLcdsforce() {
        return lcdsforce;
    }

    public void setLcdsforce(Double lcdsforce) {
        this.lcdsforce = lcdsforce;
    }

    public Double getPtosforce() {
        return ptosforce;
    }

    public void setPtosforce(Double ptosforce) {
        this.ptosforce = ptosforce;
    }

    public Double getPtdsforce() {
        return ptdsforce;
    }

    public void setPtdsforce(Double ptdsforce) {
        this.ptdsforce = ptdsforce;
    }

    public Double getUpstepwedge() {
        return upstepwedge;
    }

    public void setUpstepwedge(Double upstepwedge) {
        this.upstepwedge = upstepwedge;
    }

    public Double getDnstepwedge() {
        return dnstepwedge;
    }

    public void setDnstepwedge(Double dnstepwedge) {
        this.dnstepwedge = dnstepwedge;
    }
}