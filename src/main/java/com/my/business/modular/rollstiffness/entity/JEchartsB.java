package com.my.business.modular.rollstiffness.entity;


import java.util.List;

/**
 * 轧辊多变异分析图形化——支撑辊实体类
 *
 * @author 生成器生成
 * @date 2020-09-08 11:24:47
 */
public class JEchartsB {

    private List<String> catalog;
    private List<Sts> series;  //支撑辊辊号

    public List<String> getCatalog() {
        return catalog;
    }

    public void setCatalog(List<String> catalog) {
        this.catalog = catalog;
    }

    public List<Sts> getSeries() {
        return series;
    }

    public void setSeries(List<Sts> series) {
        this.series = series;
    }

    public static class Sts {
        String name;
        List<Double> data;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Double> getData() {
            return data;
        }

        public void setData(List<Double> data) {
            this.data = data;
        }
    }
}