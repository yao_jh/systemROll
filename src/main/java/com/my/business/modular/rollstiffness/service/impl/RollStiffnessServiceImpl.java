package com.my.business.modular.rollstiffness.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.chockdismounting.dao.ChockDismountingDao;
import com.my.business.modular.chockdismounting.entity.ChockDismounting;
import com.my.business.modular.dictionary.dao.DictionaryDao;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.modular.rollstiffness.dao.RollStiffnessDao;
import com.my.business.modular.rollstiffness.entity.*;
import com.my.business.modular.rollstiffness.service.RollStiffnessService;
import com.my.business.modular.stiffnessproject.dao.StiffnessProjectDao;
import com.my.business.modular.stiffnessproject.entity.StiffnessProject;
import com.my.business.modular.stiffnessscore.dao.StiffnessScoreDao;
import com.my.business.modular.stiffnessscore.entity.StiffnessScore;
import com.my.business.modular.stiffnessstandard.dao.StiffnessStandardDao;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.dict.entity.DpdEntity;
import com.my.business.util.CodiUtil;
import com.my.business.util.MathUtil;
import lombok.extern.apachecommons.CommonsLog;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 轧辊多变异分析接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-08 11:24:48
 */
@Slf4j
@CommonsLog
@Service
public class RollStiffnessServiceImpl implements RollStiffnessService {

    private final static Log logger = LogFactory.getLog(RollStiffnessServiceImpl.class);

    @Autowired
    private RollStiffnessDao rollStiffnessDao;
    @Autowired
    private DictionaryDao dictionaryDao;
    @Autowired
    private RollInformationDao rollInformationDao;
    @Autowired
    private StiffnessStandardDao stiffnessStandardDao;
    @Autowired
    private StiffnessProjectDao stiffnessProjectDao;
    @Autowired
    private StiffnessScoreDao stiffnessScoreDao;
    @Autowired
    private ChockDismountingDao chockDismountingDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollStiffness(String data, Long userId, String sname) {
        try {
            JRollStiffness jrollStiffness = JSON.parseObject(data, JRollStiffness.class);
            RollStiffness rollStiffness = jrollStiffness.getRollStiffness();
            CodiUtil.newRecord(userId, sname, rollStiffness);
            rollStiffnessDao.insertDataRollStiffness(rollStiffness);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollStiffnessOne(Long indocno) {
        try {
            rollStiffnessDao.deleteDataRollStiffnessOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollStiffnessMany(String str_id) {
        try {
            String sql = "delete roll_stiffness where indocno in(" + str_id + ")";
            rollStiffnessDao.deleteDataRollStiffnessMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollStiffness(String data, Long userId, String sname) {
        try {
            JRollStiffness jrollStiffness = JSON.parseObject(data, JRollStiffness.class);
            RollStiffness rollStiffness = jrollStiffness.getRollStiffness();
            CodiUtil.editRecord(userId, sname, rollStiffness);

            rollStiffnessDao.updateDataRollStiffness(rollStiffness);

            return ResultData.ResultDataSuccessSelf("修改成功", rollStiffness);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }


    /**
     * 修改记录
     */
    public void updateDataRollStiffnessByMath(RollStiffness rollStiffness) {
        try {
            RollStiffness news = score(rollStiffness);
            if (news.getProduction_line_id() == 2L) {
                rollStiffnessDao.updateDataRollStiffness(news);
            } else {
                if (StringUtils.isEmpty(news.getLeveling())) {
                    rollStiffnessDao.updateDataRollStiffnessA(news);
                } else if (!StringUtils.isEmpty(news.getLeveling()) && !StringUtils.isEmpty(news.getRetention())) {
                    rollStiffnessDao.updateDataRollStiffnessB(news);
                }
            }

            RollStiffness newsx = rollStiffnessDao.findDataRollStiffnessByIndocno(rollStiffness.getIndocno());
            Double Retention_score = 0.00;
            Double Leveling_score = 0.00;
            Double Bothsides_stiffness_score = 0.00;
            Double Bothposition_stiffness_score = 0.00;
            Double Osposition_score = 0.00;
            Double Dsposition_score = 0.00;
            Double Rolling_force_score = 0.00;
            Double Osrollgap_score = 0.00;
            Double Dsrollgap_score = 0.00;
            if (!StringUtils.isEmpty(newsx.getRetention_score())) {
                Retention_score = newsx.getRetention_score();
            }
            if (!StringUtils.isEmpty(newsx.getLeveling_score())) {
                Leveling_score = newsx.getLeveling_score();
            }
            if (!StringUtils.isEmpty(newsx.getBothsides_stiffness_score())) {
                Bothsides_stiffness_score = newsx.getBothsides_stiffness_score();
            }
            if (!StringUtils.isEmpty(newsx.getBothposition_stiffness_score())) {
                Bothposition_stiffness_score = newsx.getBothposition_stiffness_score();
            }
            if (!StringUtils.isEmpty(newsx.getOsposition_score())) {
                Osposition_score = newsx.getOsposition_score();
            }
            if (!StringUtils.isEmpty(newsx.getDsposition_score())) {
                Dsposition_score = newsx.getDsposition_score();
            }
            if (!StringUtils.isEmpty(newsx.getRolling_force_score())) {
                Rolling_force_score = newsx.getRolling_force_score();
            }
            if (!StringUtils.isEmpty(newsx.getOsrollgap_score())) {
                Osrollgap_score = newsx.getOsrollgap_score();
            }
            if (!StringUtils.isEmpty(newsx.getDsrollgap_score())) {
                Dsrollgap_score = newsx.getDsrollgap_score();
            }

            newsx.setItotal(Retention_score + Leveling_score + Bothsides_stiffness_score
                    + Bothposition_stiffness_score + Osposition_score + Dsposition_score
                    + Rolling_force_score + Osrollgap_score + Dsrollgap_score);
            rollStiffnessDao.updateItotal(newsx);//更新总分
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollStiffnessByPage(String data) {
        try {
            JRollStiffness jrollStiffness = JSON.parseObject(data, JRollStiffness.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollStiffness.getPageIndex();
            Integer pageSize = jrollStiffness.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollStiffness.getCondition()) {
                jsonObject = JSON.parseObject(jrollStiffness.getCondition().toString());
            }

            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            Long frame_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
                frame_noid = Long.valueOf(jsonObject.get("frame_noid").toString());
            }

            //产线id
            Long production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
            }

            String w_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("w_no"))) {
                w_no = jsonObject.get("w_no").toString();
            }

            String b_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("b_no"))) {
                b_no = jsonObject.get("b_no").toString();
            }
            List<RollStiffness> list = rollStiffnessDao.findDataRollStiffnessByPage((pageIndex - 1) * pageSize, pageSize, dbegin, dend, frame_noid, w_no, b_no, production_line_id);
            Integer count = rollStiffnessDao.findDataRollStiffnessByPageSize(dbegin, dend, frame_noid, w_no, b_no, production_line_id);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollStiffnessByIndocno(String data) {
        try {
            JRollStiffness jrollStiffness = JSON.parseObject(data, JRollStiffness.class);
            Long indocno = jrollStiffness.getIndocno();

            RollStiffness rollStiffness = rollStiffnessDao.findDataRollStiffnessByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollStiffness);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollStiffness> findDataRollStiffness() {
        List<RollStiffness> list = rollStiffnessDao.findDataRollStiffness();
        return list;
    }

    /**
     * 图形化——支撑辊查询接口
     */
    public ResultData findEchartsB(String data) {
        try {
            JRollStiffness jrollStiffness = JSON.parseObject(data, JRollStiffness.class);
            JSONObject jsonObject = null;

            if (null != jrollStiffness.getCondition()) {
                jsonObject = JSON.parseObject(jrollStiffness.getCondition().toString());
            }

            //开始时间
            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            //结束时间
            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            //机架号id
            Long frame_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
                frame_noid = Long.valueOf(jsonObject.get("frame_noid").toString());
            }

            //工作辊辊号组合
            String w_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("w_no"))) {
                w_no = jsonObject.get("w_no").toString();
            }

            //产线id
            Long production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
            }

            //指标代码
            Long project = null;
            if (!StringUtils.isEmpty(jsonObject.get("project"))) {
                project = Long.valueOf(jsonObject.get("project").toString());
            }

            //图形指向代码
            Long pic = null;
            if (!StringUtils.isEmpty(jsonObject.get("pic"))) {
                pic = Long.valueOf(jsonObject.get("pic").toString());
            }

            //查出条件范围内所有工作辊轴承座号
            List<RollStiffnessEchartsB> listA = rollStiffnessDao.findEchartsbearingchock(dbegin, dend, frame_noid, production_line_id);
            //查出条件范围内所有支撑辊号
            List<RollStiffnessEchartsB> listB = new ArrayList<>();
            if (pic == 1L) {
                //查出条件范围内所有支撑辊号
                listB = rollStiffnessDao.findEchartsBNo(dbegin, dend, frame_noid, production_line_id);
            } else {
                //查出条件范围内所有阶梯位置
                listB = rollStiffnessDao.findEchartsUpdownstep(dbegin, dend, frame_noid, production_line_id);
            }
            JEchartsB jEchartsB = new JEchartsB();
            List<String> catalog = new ArrayList<>();
            List<JEchartsB.Sts> stsList = new ArrayList<>();
            if (!StringUtils.isEmpty(listA)) {
                //依次循环工作辊轴承座号
                for (int i = 0; i < listA.size(); i++) {
                    //去除空值
                    if (!StringUtils.isEmpty(listA.get(i))) {
                        if (!StringUtils.isEmpty(listA.get(i).getBearingchock_no()) && !listA.get(i).getBearingchock_no().equals("-/-")) {
                            String name = listA.get(i).getBearingchock_no();
                            JEchartsB.Sts sts = new JEchartsB.Sts();
                            List<Double> datas = new ArrayList<>();
                            //依次循环支承辊号或者阶梯位置
                            for (int j = 0; j < listB.size(); j++) {
                                String bu = null;
                                if (!StringUtils.isEmpty(listB.get(j))) {
                                    if (pic == 1L) {
                                        bu = listB.get(j).getB_no();
                                    } else {
                                        bu = listB.get(j).getUpdownstep();
                                    }
                                }

                                //去除空值
                                if (!StringUtils.isEmpty(bu)) {
                                    RollStiffnessEchartsB rollStiffnessEchartsB = new RollStiffnessEchartsB();
                                    //查询该图形化所需全部指标
                                    rollStiffnessEchartsB = rollStiffnessDao.findEchartsLast(dbegin, dend, frame_noid, w_no, bu, listA.get(i).getBearingchock_no(), production_line_id);


                                    /**根据选择的指标来输出
                                     * 指标代码对应如下：
                                     * 1 —— 刚度保持率    retention_score
                                     * 2 —— 调平值        leveling_score
                                     * 3 —— 两侧刚度偏差   bothsides_stiffness_score
                                     * 4 —— 两侧位置偏差   bothposition_stiffness_score
                                     * 5 —— OS位置偏差     osposition_score
                                     * 6 —— DS位置偏差     dsposition_score
                                     * 7 —— 轧制力偏差     rolling_force_score
                                     * 8 —— OS辊缝偏差     osrollgap_score
                                     * 9 —— DS辊缝偏差     dsrollgap_score
                                     * 10 —— 总得分        itotal
                                     */
                                    Double das = null;
                                    if (rollStiffnessEchartsB != null) {
                                        if (project == 1L) {
                                            das = rollStiffnessEchartsB.getRetention_score();
                                        } else if (project == 2L) {
                                            das = rollStiffnessEchartsB.getLeveling_score();
                                        } else if (project == 3L) {
                                            das = rollStiffnessEchartsB.getBothsides_stiffness_score();
                                        } else if (project == 4L) {
                                            das = rollStiffnessEchartsB.getBothposition_stiffness_score();
                                        } else if (project == 5L) {
                                            das = rollStiffnessEchartsB.getOsposition_score();
                                        } else if (project == 6L) {
                                            das = rollStiffnessEchartsB.getDsposition_score();
                                        } else if (project == 7L) {
                                            das = rollStiffnessEchartsB.getRolling_force_score();
                                        } else if (project == 8L) {
                                            das = rollStiffnessEchartsB.getOsrollgap_score();
                                        } else if (project == 9L) {
                                            das = rollStiffnessEchartsB.getDsrollgap_score();
                                        } else if (project == 10L) {
                                            das = rollStiffnessEchartsB.getItotal();
                                        }
                                    }
                                    datas.add(das);
                                }
                            }
                            sts.setName(name);
                            sts.setData(datas);
                            stsList.add(sts);
                        }
                    }
                }
            }
            //收集支承辊号
            for (int k = 0; k < listB.size(); k++) {
                if (pic == 1L) {
                    //去除空值
                    if (!StringUtils.isEmpty(listB.get(k))) {
                        if (!StringUtils.isEmpty(listB.get(k).getB_no())) {
                            catalog.add(listB.get(k).getB_no());
                        }
                    }

                } else {
                    //去除空值
                    if (!StringUtils.isEmpty(listB.get(k))) {
                        if (!StringUtils.isEmpty(listB.get(k).getUpdownstep())) {
                            catalog.add(listB.get(k).getUpdownstep());
                        }
                    }
                }
            }

            //形成json
            jEchartsB.setCatalog(catalog);
            jEchartsB.setSeries(stsList);

            //返回json格式
            return ResultData.ResultDataSuccess(jEchartsB);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 添加记录——信号表
     *
     * @param rollStiffness json字符串
     */
    public ResultData insertDataRollStiffnessByMath(RollStiffness rollStiffness) {
        try {
            //保存
            if (rollStiffness.getProduction_line_id() == 1L) {
                rollStiffnessDao.insertDataRollStiffness(score(rollStiffness));
            } else {
                rollStiffnessDao.insertDataRollStiffness(rollStiffness);
            }
            return ResultData.ResultDataSuccessSelf("添加数据成功", rollStiffness);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    public RollStiffness score(RollStiffness rollStiffness) {
        //获取机架号
        String frame_no = rollStiffness.getFrame_no();

        //反查——获取机架号id
        Long frame_noid = Long.valueOf(dictionaryDao.findMapBynameV1("frameteam", frame_no));

        //根据机架号id、周转状态为在机查询对应内容
        Long roll_revolve = 7L;
        List<RollInformation> list = rollInformationDao.findDataRollInformationByMath(frame_noid, roll_revolve, rollStiffness.getProduction_line_id());

        //定义 工作辊组合辊号
        String w_no = null;
        //定义 支撑辊组合辊号
        String b_no = null;
        //定义 轴承座组合号
        String bearingchock_no = null;
        String w_u_no = ""; //上辊
        String w_d_no = ""; //下辊
        String b_u_no = ""; //上辊
        String b_d_no = ""; //下辊
        String ubos_no = ""; //上辊 OS侧轴承座号
        String dbos_no = ""; //下辊 OS侧轴承座号
        String ubds_no = ""; //上辊 DS侧轴承座号
        String dbds_no = ""; //下辊 DS侧轴承座号
        String updownstep = null; //上/下阶梯垫位置
        //非空集
        if (list != null && list.size() > 0) {
            for (RollInformation entity : list) {
                logger.debug("辊号为" + entity.getRoll_no());
                if (entity.getRoll_no().contains("FW") || entity.getRoll_no().contains("FB") || entity.getRoll_no().contains("RW") || entity.getRoll_no().contains("RB")) {
                    logger.debug("辊号为" + entity.getRoll_no() + "进入判断");

                    //轴承座，判断和查询取值
                    ChockDismounting chockDismounting = chockDismountingDao.findDataNewest(entity.getRoll_no());
                    if (chockDismounting.getUp_location_id() == 1) {
                        logger.debug("位置为" + chockDismounting.getUp_location());
                        if (entity.getRoll_no().contains("FW") || entity.getRoll_no().contains("RW")) {
                            w_u_no = entity.getRoll_no();
                            ubos_no = chockDismounting.getOs_no();
                            ubds_no = chockDismounting.getDs_no();
                        } else {
                            b_u_no = entity.getRoll_no();
                        }
                    } else if (chockDismounting.getUp_location_id() == 2) {
                        logger.debug("位置为" + chockDismounting.getUp_location());
                        if (entity.getRoll_no().contains("FW") || entity.getRoll_no().contains("RW")) {
                            w_d_no = entity.getRoll_no();
                            dbos_no = chockDismounting.getOs_no();
                            dbds_no = chockDismounting.getDs_no();
                        } else {
                            b_d_no = entity.getRoll_no();
                        }
                    } else {
                        logger.debug("缺少位置信息");
                        w_u_no = "该位置暂无信息"; //上辊
                        w_d_no = "该位置暂无信息"; //下辊
                        ubos_no = "该位置暂无信息"; //上辊 OS侧轴承座号
                        dbos_no = "该位置暂无信息"; //下辊 OS侧轴承座号
                        ubds_no = "该位置暂无信息"; //上辊 DS侧轴承座号
                        dbds_no = "该位置暂无信息"; //下辊 DS侧轴承座号
                    }
                }
            }
            //轴承座
            w_no = w_u_no + "/" + w_d_no;
            b_no = b_u_no + "/" + b_d_no;
            bearingchock_no = ubos_no + "-" + ubds_no + "/" + dbos_no + "-" + dbds_no;
            if (StringUtils.isEmpty(w_u_no) && StringUtils.isEmpty(w_d_no)) {
                w_no = null;
            }
            if (StringUtils.isEmpty(b_u_no) && StringUtils.isEmpty(b_u_no)) {
                b_no = null;
            }
            if (StringUtils.isEmpty(ubos_no) && StringUtils.isEmpty(ubds_no) && StringUtils.isEmpty(dbos_no) && StringUtils.isEmpty(dbds_no)) {
                bearingchock_no = null;
            }
        } else {
            logger.debug("基本信息表中没有相关辊号，机架为：" + frame_no + " 产线id为：" + rollStiffness.getProduction_line_id());
        }

        //阶梯位
        updownstep = rollStiffness.getUpstepwedge() + "/" + rollStiffness.getDnstepwedge();
        //评分
        //根据机架号、产线查询标准主表主键
        Long inmain = stiffnessStandardDao.findIndocno(frame_noid, rollStiffness.getProduction_line_id());

        //根据主表主键查子表项目列表
        List<StiffnessProject> listp = stiffnessProjectDao.findDataStiffnessProjectByIlinkno(inmain);

        for (StiffnessProject sp : listp) {
            //根据指标主键查询子表得分
            List<StiffnessScore> listsc = stiffnessScoreDao.findDataStiffnessScoreByIlinkno(sp.getIndocno());
            for (StiffnessScore entity : listsc) {
                //min max 有 无
                if (!StringUtils.isEmpty(entity.getMin()) && StringUtils.isEmpty(entity.getMax())) {
                    //判断是哪个指标
                    if (sp.getField_name().equals("retention")) {
                        //对比数据大小
                        if (!StringUtils.isEmpty(rollStiffness.getRetention())) {
                            if (rollStiffness.getRetention() >= entity.getMin()) {
                                rollStiffness.setRetention_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setRetention_score(null);
                        }
                    } else if (sp.getField_name().equals("leveling")) {
                        if (!StringUtils.isEmpty(rollStiffness.getLeveling())) {
                            if (Math.abs(rollStiffness.getLeveling()) >= entity.getMin()) {
                                rollStiffness.setLeveling_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setLeveling_score(null);
                        }
                    } else if (sp.getField_name().equals("bothsides_stiffness")) {
                        if (!StringUtils.isEmpty(rollStiffness.getBothsides_stiffness())) {
                            if (Math.abs(rollStiffness.getBothsides_stiffness()) >= entity.getMin()) {
                                rollStiffness.setBothsides_stiffness_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setBothsides_stiffness_score(null);
                        }
                    } else if (sp.getField_name().equals("bothposition_stiffness")) {
                        if (!StringUtils.isEmpty(rollStiffness.getBothposition_stiffness())) {
                            if (Math.abs(rollStiffness.getBothposition_stiffness()) >= entity.getMin()) {
                                rollStiffness.setBothposition_stiffness_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setBothposition_stiffness_score(null);
                        }
                    } else if (sp.getField_name().equals("osposition")) {
                        if (!StringUtils.isEmpty(rollStiffness.getOsposition())) {
                            if (Math.abs(rollStiffness.getOsposition()) >= entity.getMin()) {
                                rollStiffness.setOsposition_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setOsposition_score(null);
                        }
                    } else if (sp.getField_name().equals("dsposition")) {
                        if (!StringUtils.isEmpty(rollStiffness.getDsposition())) {
                            if (Math.abs(rollStiffness.getDsposition()) >= entity.getMin()) {
                                rollStiffness.setDsposition_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setDsposition_score(null);
                        }
                    } else if (sp.getField_name().equals("rolling_force")) {
                        if (!StringUtils.isEmpty(rollStiffness.getRolling_force())) {
                            if (Math.abs(rollStiffness.getRolling_force()) >= entity.getMin()) {
                                rollStiffness.setRolling_force_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setRolling_force_score(null);
                        }
                    } else if (sp.getField_name().equals("osrollgap")) {
                        if (!StringUtils.isEmpty(rollStiffness.getOsrollgap())) {
                            if (Math.abs(rollStiffness.getOsrollgap()) >= entity.getMin()) {
                                rollStiffness.setOsrollgap_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setOsrollgap_score(null);
                        }
                    } else if (sp.getField_name().equals("dsrollgap")) {
                        if (!StringUtils.isEmpty(rollStiffness.getDsrollgap())) {
                            if (Math.abs(rollStiffness.getDsrollgap()) >= entity.getMin()) {
                                rollStiffness.setDsrollgap_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setDsrollgap_score(null);
                        }
                    }
                }
                //无 有
                else if (StringUtils.isEmpty(entity.getMin()) && !StringUtils.isEmpty(entity.getMax())) {
                    //判断是哪个指标
                    if (sp.getField_name().equals("retention")) {
                        //对比数据大小
                        if (!StringUtils.isEmpty(rollStiffness.getRetention())) {
                            if (rollStiffness.getRetention() < entity.getMax()) {
                                rollStiffness.setRetention_score(entity.getScore());
                                System.out.println(rollStiffness.getRetention_score());
                            }
                        } else {
                            rollStiffness.setRetention_score(null);
                        }
                    } else if (sp.getField_name().equals("leveling")) {
                        if (!StringUtils.isEmpty(rollStiffness.getLeveling())) {
                            if (Math.abs(rollStiffness.getLeveling()) < entity.getMax()) {
                                rollStiffness.setLeveling_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setLeveling_score(null);
                        }
                    } else if (sp.getField_name().equals("bothsides_stiffness")) {
                        if (!StringUtils.isEmpty(rollStiffness.getBothsides_stiffness())) {
                            if (Math.abs(rollStiffness.getBothsides_stiffness()) < entity.getMax()) {
                                rollStiffness.setBothsides_stiffness_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setBothsides_stiffness_score(null);
                        }
                    } else if (sp.getField_name().equals("bothposition_stiffness")) {
                        if (!StringUtils.isEmpty(rollStiffness.getBothposition_stiffness())) {
                            if (Math.abs(rollStiffness.getBothposition_stiffness()) < entity.getMax()) {
                                rollStiffness.setBothposition_stiffness_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setBothposition_stiffness_score(null);
                        }
                    } else if (sp.getField_name().equals("osposition")) {
                        if (!StringUtils.isEmpty(rollStiffness.getOsposition())) {
                            if (Math.abs(rollStiffness.getOsposition()) < entity.getMax()) {
                                rollStiffness.setOsposition_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setOsposition_score(null);
                        }
                    } else if (sp.getField_name().equals("dsposition")) {
                        if (!StringUtils.isEmpty(rollStiffness.getDsposition())) {
                            if (Math.abs(rollStiffness.getDsposition()) < entity.getMax()) {
                                rollStiffness.setDsposition_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setDsposition_score(null);
                        }
                    } else if (sp.getField_name().equals("rolling_force")) {
                        if (!StringUtils.isEmpty(rollStiffness.getRolling_force())) {
                            if (Math.abs(rollStiffness.getRolling_force()) < entity.getMax()) {
                                rollStiffness.setRolling_force_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setRolling_force_score(null);
                        }
                    } else if (sp.getField_name().equals("osrollgap")) {
                        if (!StringUtils.isEmpty(rollStiffness.getOsrollgap())) {
                            if (Math.abs(rollStiffness.getOsrollgap()) < entity.getMax()) {
                                rollStiffness.setOsrollgap_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setOsrollgap_score(null);
                        }
                    } else if (sp.getField_name().equals("dsrollgap")) {
                        if (!StringUtils.isEmpty(rollStiffness.getDsrollgap())) {
                            if (Math.abs(rollStiffness.getDsrollgap()) < entity.getMax()) {
                                rollStiffness.setDsrollgap_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setDsrollgap_score(null);
                        }
                    }
                }
                //有 有
                else if (!StringUtils.isEmpty(entity.getMin()) && !StringUtils.isEmpty(entity.getMax())) {
                    //判断是哪个指标
                    if (sp.getField_name().equals("retention")) {
                        //对比数据大小
                        if (!StringUtils.isEmpty(rollStiffness.getRetention())) {
                            if (rollStiffness.getRetention() < entity.getMax() && rollStiffness.getRetention() >= entity.getMin()) {
                                rollStiffness.setRetention_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setRetention_score(null);
                        }
                    } else if (sp.getField_name().equals("leveling")) {
                        if (!StringUtils.isEmpty(rollStiffness.getLeveling())) {
                            if (Math.abs(rollStiffness.getLeveling()) < entity.getMax() && Math.abs(rollStiffness.getLeveling()) >= entity.getMin()) {
                                rollStiffness.setLeveling_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setLeveling_score(null);
                        }
                    } else if (sp.getField_name().equals("bothsides_stiffness")) {
                        if (!StringUtils.isEmpty(rollStiffness.getBothsides_stiffness())) {
                            if (Math.abs(rollStiffness.getBothsides_stiffness()) < entity.getMax() && Math.abs(rollStiffness.getBothsides_stiffness()) >= entity.getMin()) {
                                rollStiffness.setBothsides_stiffness_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setBothsides_stiffness_score(null);
                        }
                    } else if (sp.getField_name().equals("bothposition_stiffness")) {
                        if (!StringUtils.isEmpty(rollStiffness.getBothposition_stiffness())) {
                            if (Math.abs(rollStiffness.getBothposition_stiffness()) < entity.getMax() && Math.abs(rollStiffness.getBothposition_stiffness()) >= entity.getMin()) {
                                rollStiffness.setBothposition_stiffness_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setBothposition_stiffness_score(null);
                        }
                    } else if (sp.getField_name().equals("osposition")) {
                        if (!StringUtils.isEmpty(rollStiffness.getOsposition())) {
                            if (Math.abs(rollStiffness.getOsposition()) < entity.getMax() && Math.abs(rollStiffness.getOsposition()) >= entity.getMin()) {
                                rollStiffness.setOsposition_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setOsposition_score(null);
                        }
                    } else if (sp.getField_name().equals("dsposition")) {
                        if (!StringUtils.isEmpty(rollStiffness.getDsposition())) {
                            if (Math.abs(rollStiffness.getDsposition()) < entity.getMax() && Math.abs(rollStiffness.getDsposition()) >= entity.getMin()) {
                                rollStiffness.setDsposition_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setDsposition_score(null);
                        }
                    } else if (sp.getField_name().equals("rolling_force")) {
                        if (!StringUtils.isEmpty(rollStiffness.getRolling_force())) {
                            if (Math.abs(rollStiffness.getRolling_force()) < entity.getMax() && Math.abs(rollStiffness.getRolling_force()) >= entity.getMin()) {
                                rollStiffness.setRolling_force_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setRolling_force_score(null);
                        }
                    } else if (sp.getField_name().equals("osrollgap")) {
                        if (!StringUtils.isEmpty(rollStiffness.getOsrollgap())) {
                            if (Math.abs(rollStiffness.getOsrollgap()) < entity.getMax() && Math.abs(rollStiffness.getOsrollgap()) >= entity.getMin()) {
                                rollStiffness.setOsrollgap_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setOsrollgap_score(null);
                        }
                    } else if (sp.getField_name().equals("dsrollgap")) {
                        if (!StringUtils.isEmpty(rollStiffness.getDsrollgap())) {
                            if (Math.abs(rollStiffness.getDsrollgap()) < entity.getMax() && Math.abs(rollStiffness.getDsrollgap()) >= entity.getMin()) {
                                rollStiffness.setDsrollgap_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setDsrollgap_score(null);
                        }
                    }

                }
                //无 无
                else if (StringUtils.isEmpty(entity.getMin()) && StringUtils.isEmpty(entity.getMax())) {

                }
                //其他
                else {
                    switch (sp.getField_name()) {
                        case "retention":
                            rollStiffness.setRetention_score(0.00);
                            break;
                        case "leveling":
                            rollStiffness.setLeveling_score(0.00);
                            break;
                        case "bothsides_stiffness":
                            rollStiffness.setBothsides_stiffness_score(0.00);
                            break;
                        case "bothposition_stiffness":
                            rollStiffness.setBothposition_stiffness_score(0.00);
                            break;
                        case "osposition":
                            rollStiffness.setOsposition_score(0.00);
                            break;
                        case "dsposition":
                            rollStiffness.setDsposition_score(0.00);
                            break;
                        case "rolling_force":
                            rollStiffness.setRolling_force_score(0.00);
                            break;
                        case "osrollgap":
                            rollStiffness.setOsrollgap_score(0.00);
                            break;
                        case "dsrollgap":
                            rollStiffness.setDsrollgap_score(0.00);
                            break;
                    }
                }
            }
        }

        //基本信息赋值
        rollStiffness.setW_no(w_no);
        rollStiffness.setB_no(b_no);
        rollStiffness.setBearingchock_no(bearingchock_no);
        rollStiffness.setFrame_noid(frame_noid);
        rollStiffness.setUpdownstep(updownstep);
        Double Retention_score = 0.00;
        Double Leveling_score = 0.00;
        Double Bothsides_stiffness_score = 0.00;
        Double Bothposition_stiffness_score = 0.00;
        Double Osposition_score = 0.00;
        Double Dsposition_score = 0.00;
        Double Rolling_force_score = 0.00;
        Double Osrollgap_score = 0.00;
        Double Dsrollgap_score = 0.00;
        if (!StringUtils.isEmpty(rollStiffness.getRetention_score())) {
            Retention_score = rollStiffness.getRetention_score();
        }
        if (!StringUtils.isEmpty(rollStiffness.getLeveling_score())) {
            Leveling_score = rollStiffness.getLeveling_score();
        }
        if (!StringUtils.isEmpty(rollStiffness.getBothsides_stiffness_score())) {
            Bothsides_stiffness_score = rollStiffness.getBothsides_stiffness_score();
        }
        if (!StringUtils.isEmpty(rollStiffness.getBothposition_stiffness_score())) {
            Bothposition_stiffness_score = rollStiffness.getBothposition_stiffness_score();
        }
        if (!StringUtils.isEmpty(rollStiffness.getOsposition_score())) {
            Osposition_score = rollStiffness.getOsposition_score();
        }
        if (!StringUtils.isEmpty(rollStiffness.getDsposition_score())) {
            Dsposition_score = rollStiffness.getDsposition_score();
        }
        if (!StringUtils.isEmpty(rollStiffness.getRolling_force_score())) {
            Rolling_force_score = rollStiffness.getRolling_force_score();
        }
        if (!StringUtils.isEmpty(rollStiffness.getOsrollgap_score())) {
            Osrollgap_score = rollStiffness.getOsrollgap_score();
        }
        if (!StringUtils.isEmpty(rollStiffness.getDsrollgap_score())) {
            Dsrollgap_score = rollStiffness.getDsrollgap_score();
        }


        rollStiffness.setItotal(Retention_score + Leveling_score + Bothsides_stiffness_score
                + Bothposition_stiffness_score + Osposition_score + Dsposition_score
                + Rolling_force_score + Osrollgap_score + Dsrollgap_score);
        return rollStiffness;
    }

    /**
     * 图形化——时间总分查询接口
     */
    public ResultData findEchartsA(String data) {
        try {
            JRollStiffness jrollStiffness = JSON.parseObject(data, JRollStiffness.class);
            JSONObject jsonObject = null;

            if (null != jrollStiffness.getCondition()) {
                jsonObject = JSON.parseObject(jrollStiffness.getCondition().toString());
            }

            //开始时间
            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            //结束时间
            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }
            //产线id
            Long production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
            }

            String frame_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_no"))) {
                frame_no = jsonObject.get("frame_no").toString();
            }

            JEchartsA jEchartsA = new JEchartsA();
            List<String> catalog = new ArrayList<>();
            List<JEchartsA.xy> xyList = new ArrayList<>();
            //查出条件范围内所有机架号
            List<RollStiffness> frameList = rollStiffnessDao.findframeno(dbegin, dend, frame_no, production_line_id);

            //查出条件范围内时间点最大值和最小值
            String minTime = rollStiffnessDao.findminTime(dbegin, dend, frame_no, production_line_id);
            String maxTime = rollStiffnessDao.findmaxTime(dbegin, dend, frame_no, production_line_id);

            //获得X轴
            catalog.add(minTime);
            catalog.add(maxTime);

            //循环机架号获取指定记录
            for (int i = 0; i < frameList.size(); i++) {
                if (!StringUtils.isEmpty(frameList.get(i).getFrame_no())) {
                    JEchartsA.xy xy = new JEchartsA.xy();
                    List<String> x = new ArrayList<>();
                    List<Double> y = new ArrayList<>();
                    List<Double> y2 = new ArrayList<>();
                    List<Double> y3 = new ArrayList<>();
                    List<Double> y4 = new ArrayList<>();
                    List<Double> y5 = new ArrayList<>();
                    List<Double> y6 = new ArrayList<>();
                    List<Double> y7 = new ArrayList<>();
                    List<Double> y8 = new ArrayList<>();
                    List<Double> y9 = new ArrayList<>();
                    List<Double> y10 = new ArrayList<>();
                    //查出条件范围内时间点、总分
                    List<RollStiffness> EchartsAList = rollStiffnessDao.findEchartsA(dbegin, dend, frameList.get(i).getFrame_no(), production_line_id);
                    //循环取出时间点和总分，放入各自的集合
                    for (RollStiffness entity : EchartsAList) {
                        x.add(entity.getRecordtime());
                        y.add(entity.getItotal());
                        y2.add(entity.getRetention_score());
                        y3.add(entity.getLeveling_score());
                        y4.add(entity.getBothsides_stiffness_score());
                        y5.add(entity.getBothposition_stiffness_score());
                        y6.add(entity.getOsposition_score());
                        y7.add(entity.getDsposition_score());
                        y8.add(entity.getRolling_force_score());
                        y9.add(entity.getOsrollgap_score());
                        y10.add(entity.getDsrollgap_score());
                    }
                    //集合放入各自的实体类
                    xy.setXst(x);
                    xy.setYst(y);
                    xy.setYst2(y2);
                    xy.setYst3(y3);
                    xy.setYst4(y4);
                    xy.setYst5(y5);
                    xy.setYst6(y6);
                    xy.setYst7(y7);
                    xy.setYst8(y8);
                    xy.setYst9(y9);
                    xy.setYst10(y10);
                    xy.setName(frameList.get(i).getFrame_no());
                    //实体类变成集合
                    xyList.add(xy);
                }
            }
            jEchartsA.setCatalog(catalog);
            jEchartsA.setSeries(xyList);
            //返回json格式
            return ResultData.ResultDataSuccess(jEchartsA);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    @Override
    public ResultData findTablenew(String data) {
        try {
            JRollStiffness jrollStiffness = JSON.parseObject(data, JRollStiffness.class);
            JSONObject jsonObject = null;

            Integer pageIndex = jrollStiffness.getPageIndex();
            Integer pageSize = jrollStiffness.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollStiffness.getCondition()) {
                jsonObject = JSON.parseObject(jrollStiffness.getCondition().toString());
            }

            //开始时间
            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            //结束时间
            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            //产线id
            String production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = jsonObject.get("production_line_id").toString();
            }

            //根据时间戳获取数据
            List<RollStiffness> list_new = rollStiffnessDao.findTablenew((pageIndex - 1) * pageSize, pageSize, dbegin, dend, production_line_id);//根据时间区间查询集合数据
            Integer count = rollStiffnessDao.findTablenewSize(dbegin, dend, production_line_id).size();

            List<VStiffnessNew> list = new ArrayList<VStiffnessNew>();

            for (RollStiffness entity : list_new) {
                VStiffnessNew v_entity = new VStiffnessNew();
                List<RollStiffness> find_data = rollStiffnessDao.findTableByRecordtime(entity.getRecordtime().substring(0, 13), production_line_id);
                List<VStiffness> totle_list = getVStiffnessList(find_data);

                v_entity.setRecordtime(entity.getRecordtime().substring(0, 19));
                v_entity.setF1_scoreAll(totle_list.get(9).getF1_score());
                v_entity.setF2_scoreAll(totle_list.get(9).getF2_score());
                v_entity.setF3_scoreAll(totle_list.get(9).getF3_score());
                v_entity.setF4_scoreAll(totle_list.get(9).getF4_score());
                v_entity.setF5_scoreAll(totle_list.get(9).getF5_score());
                v_entity.setF6_scoreAll(totle_list.get(9).getF6_score());
                v_entity.setF7_scoreAll(totle_list.get(9).getF7_score());
                v_entity.setR1_scoreAll(totle_list.get(9).getR1_score());
                v_entity.setR2_scoreAll(totle_list.get(9).getR2_score());
                v_entity.setData_array(totle_list);
                list.add(v_entity);
            }

            List<RollStiffness> all_list = rollStiffnessDao.findTable(dbegin, dend, production_line_id);//根据时间区间查询集合数据
            List<VStiffness> all_totle_list = getVStiffnessList(all_list);

            VStiffnessfind v = new VStiffnessfind();
            v.setAll_score(all_totle_list);
            v.setList_array(list);

            return ResultData.ResultDataSuccess(v, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    @Override
    public ResultData findTable(String data) {
        try {
            JRollStiffness jrollStiffness = JSON.parseObject(data, JRollStiffness.class);
            JSONObject jsonObject = null;

            if (null != jrollStiffness.getCondition()) {
                jsonObject = JSON.parseObject(jrollStiffness.getCondition().toString());
            }

            //开始时间
            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            //结束时间
            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            //产线id
            String production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = jsonObject.get("production_line_id").toString();
            }


            //按照excel中从上到下的顺序依次排序，所以下面的注解按照这个思路就行
            List<RollStiffness> list = rollStiffnessDao.findTable(dbegin, dend, production_line_id);//根据时间区间查询集合数据

            List<VStiffness> totle_list = getVStiffnessList(list);
            return ResultData.ResultDataSuccess(totle_list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /****
     * 根据RollStiffness查询的数据结果，把结果进行计算生成对应的json
     * @param list
     * @return
     */
    private List<VStiffness> getVStiffnessList(List<RollStiffness> list) {
        VStiffness v1 = new VStiffness("轧机刚度保持率", "%", "10");
        List<Double> vfram1 = new ArrayList<Double>();   //保存f1机架刚度的数据
        List<Double> score1 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> vfram2 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> score2 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> vfram3 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> score3 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> vfram4 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> score4 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> vfram5 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> score5 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> vfram6 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> score6 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> vfram7 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> score7 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> vfram8 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> score8 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> vfram9 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> score9 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        VStiffness v2 = new VStiffness("轧机标定调平值", "mm", "10");
        List<Double> v2vfram1 = new ArrayList<Double>();   //保存f1机架刚度的数据
        List<Double> v2score1 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v2vfram2 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v2score2 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v2vfram3 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v2score3 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v2vfram4 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v2score4 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v2vfram5 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v2score5 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v2vfram6 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v2score6 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v2vfram7 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v2score7 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v2vfram8 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v2score8 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v2vfram9 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v2score9 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        VStiffness v3 = new VStiffness("标定OS/DS两侧刚度偏差", "KN/mm", "30");
        List<Double> v3vfram1 = new ArrayList<Double>();   //保存f1机架刚度的数据
        List<Double> v3score1 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v3vfram2 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v3score2 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v3vfram3 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v3score3 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v3vfram4 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v3score4 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v3vfram5 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v3score5 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v3vfram6 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v3score6 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v3vfram7 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v3score7 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v3vfram8 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v3score8 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v3vfram9 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v3score9 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        VStiffness v4 = new VStiffness("标定OS/DS两侧位置偏差", "mm", "5");
        List<Double> v4vfram1 = new ArrayList<Double>();   //保存f1机架刚度的数据
        List<Double> v4score1 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v4vfram2 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v4score2 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v4vfram3 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v4score3 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v4vfram4 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v4score4 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v4vfram5 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v4score5 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v4vfram6 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v4score6 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v4vfram7 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v4score7 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v4vfram8 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v4score8 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v4vfram9 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v4score9 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        VStiffness v5 = new VStiffness("通板OS出入口位置偏差", "mm", "10");
        List<Double> v5vfram1 = new ArrayList<Double>();   //保存f1机架刚度的数据
        List<Double> v5score1 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v5vfram2 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v5score2 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v5vfram3 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v5score3 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v5vfram4 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v5score4 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v5vfram5 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v5score5 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v5vfram6 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v5score6 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v5vfram7 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v5score7 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v5vfram8 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v5score8 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v5vfram9 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v5score9 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        VStiffness v6 = new VStiffness("通板DS出入口位置偏差", "mm", "10");
        List<Double> v6vfram1 = new ArrayList<Double>();   //保存f1机架刚度的数据
        List<Double> v6score1 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v6vfram2 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v6score2 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v6vfram3 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v6score3 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v6vfram4 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v6score4 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v6vfram5 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v6score5 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v6vfram6 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v6score6 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v6vfram7 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v6score7 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v6vfram8 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v6score8 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v6vfram9 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v6score9 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        VStiffness v7 = new VStiffness("标定成功时，备用压力传感器两侧轧制力偏差", "KN", "5");
        List<Double> v7vfram1 = new ArrayList<Double>();   //保存f1机架刚度的数据
        List<Double> v7score1 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v7vfram2 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v7score2 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v7vfram3 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v7score3 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v7vfram4 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v7score4 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v7vfram5 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v7score5 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v7vfram6 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v7score6 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v7vfram7 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v7score7 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v7vfram8 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v7score8 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v7vfram9 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v7score9 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        VStiffness v8 = new VStiffness("咬钢时OS入出口偏差变化", "mm", "10");
        List<Double> v8vfram1 = new ArrayList<Double>();   //保存f1机架刚度的数据
        List<Double> v8score1 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v8vfram2 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v8score2 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v8vfram3 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v8score3 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v8vfram4 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v8score4 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v8vfram5 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v8score5 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v8vfram6 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v8score6 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v8vfram7 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v8score7 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v8vfram8 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v8score8 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v8vfram9 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v8score9 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        VStiffness v9 = new VStiffness("咬钢时DS入出口偏差变化", "mm", "10");
        List<Double> v9vfram1 = new ArrayList<Double>();   //保存f1机架刚度的数据
        List<Double> v9score1 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v9vfram2 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v9score2 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v9vfram3 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v9score3 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v9vfram4 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v9score4 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v9vfram5 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v9score5 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v9vfram6 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v9score6 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v9vfram7 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v9score7 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v9vfram8 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v9score8 = new ArrayList<Double>(); //保存f1机架刚度得分集合
        List<Double> v9vfram9 = new ArrayList<Double>();   //保存f1机架传输过来的数据集合
        List<Double> v9score9 = new ArrayList<Double>(); //保存f1机架刚度得分集合

        VStiffness v10 = new VStiffness("机架评分", "", "100");   //总计行

        for (RollStiffness entity : list) {
            if (entity.getFrame_noid() == 1) {
                if (!StringUtils.isEmpty(entity.getRetention())) {
                    vfram1.add(entity.getRetention());
                } else {
                    vfram1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRetention_score())) {
                    score1.add(entity.getRetention_score());
                } else {
                    score1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling())) {
                    vfram2.add(entity.getLeveling());
                } else {
                    vfram2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling_score())) {
                    score2.add(entity.getLeveling_score());
                } else {
                    score2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness())) {
                    vfram3.add(entity.getBothsides_stiffness());
                } else {
                    vfram3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness_score())) {
                    score3.add(entity.getBothsides_stiffness_score());
                } else {
                    score3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness())) {
                    vfram4.add(entity.getBothposition_stiffness());
                } else {
                    vfram4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness_score())) {
                    score4.add(entity.getBothposition_stiffness_score());
                } else {
                    score4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition())) {
                    vfram5.add(entity.getOsposition());
                } else {
                    vfram5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition_score())) {
                    score5.add(entity.getOsposition_score());
                } else {
                    score5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition())) {
                    vfram6.add(entity.getDsposition());
                } else {
                    vfram6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition_score())) {
                    score6.add(entity.getDsposition_score());
                } else {
                    score6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force())) {
                    vfram7.add(entity.getRolling_force());
                } else {
                    vfram7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force_score())) {
                    score7.add(entity.getRolling_force_score());
                } else {
                    score7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap())) {
                    vfram8.add(entity.getOsrollgap());
                } else {
                    vfram8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap_score())) {
                    score8.add(entity.getOsrollgap_score());
                } else {
                    score8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap())) {
                    vfram9.add(entity.getDsrollgap());
                } else {
                    vfram9.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap_score())) {
                    score9.add(entity.getDsrollgap_score());
                } else {
                    score9.add(0d);
                }
            } else if (entity.getFrame_noid() == 2) {
                if (!StringUtils.isEmpty(entity.getRetention())) {
                    v2vfram1.add(entity.getRetention());
                } else {
                    v2vfram1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRetention_score())) {
                    v2score1.add(entity.getRetention_score());
                } else {
                    v2score1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling())) {
                    v2vfram2.add(entity.getLeveling());
                } else {
                    v2vfram2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling_score())) {
                    v2score2.add(entity.getLeveling_score());
                } else {
                    v2score2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness())) {
                    v2vfram3.add(entity.getBothsides_stiffness());
                } else {
                    v2vfram3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness_score())) {
                    v2score3.add(entity.getBothsides_stiffness_score());
                } else {
                    v2score3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness())) {
                    v2vfram4.add(entity.getBothposition_stiffness());
                } else {
                    v2vfram4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness_score())) {
                    v2score4.add(entity.getBothposition_stiffness_score());
                } else {
                    v2score4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition())) {
                    v2vfram5.add(entity.getOsposition());
                } else {
                    v2vfram5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition_score())) {
                    v2score5.add(entity.getOsposition_score());
                } else {
                    v2score5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition())) {
                    v2vfram6.add(entity.getDsposition());
                } else {
                    v2vfram6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition_score())) {
                    v2score6.add(entity.getDsposition_score());
                } else {
                    v2score6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force())) {
                    v2vfram7.add(entity.getRolling_force());
                } else {
                    v2vfram7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force_score())) {
                    v2score7.add(entity.getRolling_force_score());
                } else {
                    v2score7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap())) {
                    v2vfram8.add(entity.getOsrollgap());
                } else {
                    v2vfram8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap_score())) {
                    v2score8.add(entity.getOsrollgap_score());
                } else {
                    v2score8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap())) {
                    v2vfram9.add(entity.getDsrollgap());
                } else {
                    v2vfram9.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap_score())) {
                    v2score9.add(entity.getDsrollgap_score());
                } else {
                    v2score9.add(0d);
                }
            } else if (entity.getFrame_noid() == 3) {
                if (!StringUtils.isEmpty(entity.getRetention())) {
                    v3vfram1.add(entity.getRetention());
                } else {
                    v3vfram1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRetention_score())) {
                    v3score1.add(entity.getRetention_score());
                } else {
                    v3score1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling())) {
                    v3vfram2.add(entity.getLeveling());
                } else {
                    v3vfram2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling_score())) {
                    v3score2.add(entity.getLeveling_score());
                } else {
                    v3score2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness())) {
                    v3vfram3.add(entity.getBothsides_stiffness());
                } else {
                    v3vfram3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness_score())) {
                    v3score3.add(entity.getBothsides_stiffness_score());
                } else {
                    v3score3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness())) {
                    v3vfram4.add(entity.getBothposition_stiffness());
                } else {
                    v3vfram4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness_score())) {
                    v3score4.add(entity.getBothposition_stiffness_score());
                } else {
                    v3score4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition())) {
                    v3vfram5.add(entity.getOsposition());
                } else {
                    v3vfram5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition_score())) {
                    v3score5.add(entity.getOsposition_score());
                } else {
                    v3score5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition())) {
                    v3vfram6.add(entity.getDsposition());
                } else {
                    v3vfram6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition_score())) {
                    v3score6.add(entity.getDsposition_score());
                } else {
                    v3score6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force())) {
                    v3vfram7.add(entity.getRolling_force());
                } else {
                    v3vfram7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force_score())) {
                    v3score7.add(entity.getRolling_force_score());
                } else {
                    v3score7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap())) {
                    v3vfram8.add(entity.getOsrollgap());
                } else {
                    v3vfram8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap_score())) {
                    v3score8.add(entity.getOsrollgap_score());
                } else {
                    v3score8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap())) {
                    v3vfram9.add(entity.getDsrollgap());
                } else {
                    v3vfram9.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap_score())) {
                    v3score9.add(entity.getDsrollgap_score());
                } else {
                    v3score9.add(0d);
                }
            } else if (entity.getFrame_noid() == 4) {
                if (!StringUtils.isEmpty(entity.getRetention())) {
                    v4vfram1.add(entity.getRetention());
                } else {
                    v4vfram1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRetention_score())) {
                    v4score1.add(entity.getRetention_score());
                } else {
                    v4score1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling())) {
                    v4vfram2.add(entity.getLeveling());
                } else {
                    v4vfram2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling_score())) {
                    v4score2.add(entity.getLeveling_score());
                } else {
                    v4score2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness())) {
                    v4vfram3.add(entity.getBothsides_stiffness());
                } else {
                    v4vfram3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness_score())) {
                    v4score3.add(entity.getBothsides_stiffness_score());
                } else {
                    v4score3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness())) {
                    v4vfram4.add(entity.getBothposition_stiffness());
                } else {
                    v4vfram4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness_score())) {
                    v4score4.add(entity.getBothposition_stiffness_score());
                } else {
                    v4score4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition())) {
                    v4vfram5.add(entity.getOsposition());
                } else {
                    v4vfram5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition_score())) {
                    v4score5.add(entity.getOsposition_score());
                } else {
                    v4score5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition())) {
                    v4vfram6.add(entity.getDsposition());
                } else {
                    v4vfram6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition_score())) {
                    v4score6.add(entity.getDsposition_score());
                } else {
                    v4score6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force())) {
                    v4vfram7.add(entity.getRolling_force());
                } else {
                    v4vfram7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force_score())) {
                    v4score7.add(entity.getRolling_force_score());
                } else {
                    v4score7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap())) {
                    v4vfram8.add(entity.getOsrollgap());
                } else {
                    v4vfram8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap_score())) {
                    v4score8.add(entity.getOsrollgap_score());
                } else {
                    v4score8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap())) {
                    v4vfram9.add(entity.getDsrollgap());
                } else {
                    v4vfram9.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap_score())) {
                    v4score9.add(entity.getDsrollgap_score());
                } else {
                    v4score9.add(0d);
                }
            } else if (entity.getFrame_noid() == 5) {
                if (!StringUtils.isEmpty(entity.getRetention())) {
                    v5vfram1.add(entity.getRetention());
                } else {
                    v5vfram1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRetention_score())) {
                    v5score1.add(entity.getRetention_score());
                } else {
                    v5score1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling())) {
                    v5vfram2.add(entity.getLeveling());
                } else {
                    v5vfram2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling_score())) {
                    v5score2.add(entity.getLeveling_score());
                } else {
                    v5score2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness())) {
                    v5vfram3.add(entity.getBothsides_stiffness());
                } else {
                    v5vfram3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness_score())) {
                    v5score3.add(entity.getBothsides_stiffness_score());
                } else {
                    v5score3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness())) {
                    v5vfram4.add(entity.getBothposition_stiffness());
                } else {
                    v5vfram4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness_score())) {
                    v5score4.add(entity.getBothposition_stiffness_score());
                } else {
                    v5score4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition())) {
                    v5vfram5.add(entity.getOsposition());
                } else {
                    v5vfram5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition_score())) {
                    v5score5.add(entity.getOsposition_score());
                } else {
                    v5score5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition())) {
                    v5vfram6.add(entity.getDsposition());
                } else {
                    v5vfram6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition_score())) {
                    v5score6.add(entity.getDsposition_score());
                } else {
                    v5score6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force())) {
                    v5vfram7.add(entity.getRolling_force());
                } else {
                    v5vfram7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force_score())) {
                    v5score7.add(entity.getRolling_force_score());
                } else {
                    v5score7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap())) {
                    v5vfram8.add(entity.getOsrollgap());
                } else {
                    v5vfram8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap_score())) {
                    v5score8.add(entity.getOsrollgap_score());
                } else {
                    v5score8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap())) {
                    v5vfram9.add(entity.getDsrollgap());
                } else {
                    v5vfram9.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap_score())) {
                    v5score9.add(entity.getDsrollgap_score());
                } else {
                    v5score9.add(0d);
                }
            } else if (entity.getFrame_noid() == 6) {
                if (!StringUtils.isEmpty(entity.getRetention())) {
                    v6vfram1.add(entity.getRetention());
                } else {
                    v6vfram1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRetention_score())) {
                    v6score1.add(entity.getRetention_score());
                } else {
                    v6score1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling())) {
                    v6vfram2.add(entity.getLeveling());
                } else {
                    v6vfram2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling_score())) {
                    v6score2.add(entity.getLeveling_score());
                } else {
                    v6score2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness())) {
                    v6vfram3.add(entity.getBothsides_stiffness());
                } else {
                    v6vfram3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness_score())) {
                    v6score3.add(entity.getBothsides_stiffness_score());
                } else {
                    v6score3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness())) {
                    v6vfram4.add(entity.getBothposition_stiffness());
                } else {
                    v6vfram4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness_score())) {
                    v6score4.add(entity.getBothposition_stiffness_score());
                } else {
                    v6score4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition())) {
                    v6vfram5.add(entity.getOsposition());
                } else {
                    v6vfram5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition_score())) {
                    v6score5.add(entity.getOsposition_score());
                } else {
                    v6score5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition())) {
                    v6vfram6.add(entity.getDsposition());
                } else {
                    v6vfram6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition_score())) {
                    v6score6.add(entity.getDsposition_score());
                } else {
                    v6score6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force())) {
                    v6vfram7.add(entity.getRolling_force());
                } else {
                    v6vfram7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force_score())) {
                    v6score7.add(entity.getRolling_force_score());
                } else {
                    v6score7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap())) {
                    v6vfram8.add(entity.getOsrollgap());
                } else {
                    v6vfram8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap_score())) {
                    v6score8.add(entity.getOsrollgap_score());
                } else {
                    v6score8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap())) {
                    v6vfram9.add(entity.getDsrollgap());
                } else {
                    v6vfram9.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap_score())) {
                    v6score9.add(entity.getDsrollgap_score());
                } else {
                    v6score9.add(0d);
                }
            } else if (entity.getFrame_noid() == 7) {
                if (!StringUtils.isEmpty(entity.getRetention())) {
                    v7vfram1.add(entity.getRetention());
                } else {
                    v7vfram1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRetention_score())) {
                    v7score1.add(entity.getRetention_score());
                } else {
                    v7score1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling())) {
                    v7vfram2.add(entity.getLeveling());
                } else {
                    v7vfram2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling_score())) {
                    v7score2.add(entity.getLeveling_score());
                } else {
                    v7score2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness())) {
                    v7vfram3.add(entity.getBothsides_stiffness());
                } else {
                    v7vfram3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness_score())) {
                    v7score3.add(entity.getBothsides_stiffness_score());
                } else {
                    v7score3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness())) {
                    v7vfram4.add(entity.getBothposition_stiffness());
                } else {
                    v7vfram4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness_score())) {
                    v7score4.add(entity.getBothposition_stiffness_score());
                } else {
                    v7score4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition())) {
                    v7vfram5.add(entity.getOsposition());
                } else {
                    v7vfram5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition_score())) {
                    v7score5.add(entity.getOsposition_score());
                } else {
                    v7score5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition())) {
                    v7vfram6.add(entity.getDsposition());
                } else {
                    v7vfram6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition_score())) {
                    v7score6.add(entity.getDsposition_score());
                } else {
                    v7score6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force())) {
                    v7vfram7.add(entity.getRolling_force());
                } else {
                    v7vfram7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force_score())) {
                    v7score7.add(entity.getRolling_force_score());
                } else {
                    v7score7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap())) {
                    v7vfram8.add(entity.getOsrollgap());
                } else {
                    v7vfram8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap_score())) {
                    v7score8.add(entity.getOsrollgap_score());
                } else {
                    v7score8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap())) {
                    v7vfram9.add(entity.getDsrollgap());
                } else {
                    v7vfram9.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap_score())) {
                    v7score9.add(entity.getDsrollgap_score());
                } else {
                    v7score9.add(0d);
                }
            } else if (entity.getFrame_noid() == 8) {
                if (!StringUtils.isEmpty(entity.getRetention())) {
                    v8vfram1.add(entity.getRetention());
                } else {
                    v8vfram1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRetention_score())) {
                    v8score1.add(entity.getRetention_score());
                } else {
                    v8score1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling())) {
                    v8vfram2.add(entity.getLeveling());
                } else {
                    v8vfram2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling_score())) {
                    v8score2.add(entity.getLeveling_score());
                } else {
                    v8score2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness())) {
                    v8vfram3.add(entity.getBothsides_stiffness());
                } else {
                    v8vfram3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness_score())) {
                    v8score3.add(entity.getBothsides_stiffness_score());
                } else {
                    v8score3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness())) {
                    v8vfram4.add(entity.getBothposition_stiffness());
                } else {
                    v8vfram4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness_score())) {
                    v8score4.add(entity.getBothposition_stiffness_score());
                } else {
                    v8score4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition())) {
                    v8vfram5.add(entity.getOsposition());
                } else {
                    v8vfram5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition_score())) {
                    v8score5.add(entity.getOsposition_score());
                } else {
                    v8score5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition())) {
                    v8vfram6.add(entity.getDsposition());
                } else {
                    v8vfram6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition_score())) {
                    v8score6.add(entity.getDsposition_score());
                } else {
                    v8score6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force())) {
                    v8vfram7.add(entity.getRolling_force());
                } else {
                    v8vfram7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force_score())) {
                    v8score7.add(entity.getRolling_force_score());
                } else {
                    v8score7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap())) {
                    v8vfram8.add(entity.getOsrollgap());
                } else {
                    v8vfram8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap_score())) {
                    v8score8.add(entity.getOsrollgap_score());
                } else {
                    v8score8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap())) {
                    v8vfram9.add(entity.getDsrollgap());
                } else {
                    v8vfram9.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap_score())) {
                    v8score9.add(entity.getDsrollgap_score());
                } else {
                    v8score9.add(0d);
                }
            } else if (entity.getFrame_noid() == 9) {
                if (!StringUtils.isEmpty(entity.getRetention())) {
                    v9vfram1.add(entity.getRetention());
                } else {
                    v9vfram1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRetention_score())) {
                    v9score1.add(entity.getRetention_score());
                } else {
                    v9score1.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling())) {
                    v9vfram2.add(entity.getLeveling());
                } else {
                    v9vfram2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getLeveling_score())) {
                    v9score2.add(entity.getLeveling_score());
                } else {
                    v9score2.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness())) {
                    v9vfram3.add(entity.getBothsides_stiffness());
                } else {
                    v9vfram3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothsides_stiffness_score())) {
                    v9score3.add(entity.getBothsides_stiffness_score());
                } else {
                    v9score3.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness())) {
                    v9vfram4.add(entity.getBothposition_stiffness());
                } else {
                    v9vfram4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getBothposition_stiffness_score())) {
                    v9score4.add(entity.getBothposition_stiffness_score());
                } else {
                    v9score4.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition())) {
                    v9vfram5.add(entity.getOsposition());
                } else {
                    v9vfram5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsposition_score())) {
                    v9score5.add(entity.getOsposition_score());
                } else {
                    v9score5.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition())) {
                    v9vfram6.add(entity.getDsposition());
                } else {
                    v9vfram6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsposition_score())) {
                    v9score6.add(entity.getDsposition_score());
                } else {
                    v9score6.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force())) {
                    v9vfram7.add(entity.getRolling_force());
                } else {
                    v9vfram7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getRolling_force_score())) {
                    v9score7.add(entity.getRolling_force_score());
                } else {
                    v9score7.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap())) {
                    v9vfram8.add(entity.getOsrollgap());
                } else {
                    v9vfram8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getOsrollgap_score())) {
                    v9score8.add(entity.getOsrollgap_score());
                } else {
                    v9score8.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap())) {
                    v9vfram9.add(entity.getDsrollgap());
                } else {
                    v9vfram9.add(0d);
                }
                if (!StringUtils.isEmpty(entity.getDsrollgap_score())) {
                    v9score9.add(entity.getDsrollgap_score());
                } else {
                    v9score9.add(0d);
                }
            }
        }

        RollStiffness rollStiffness1 = new RollStiffness();
        RollStiffness rollStiffness2 = new RollStiffness();
        RollStiffness rollStiffness3 = new RollStiffness();
        RollStiffness rollStiffness4 = new RollStiffness();
        RollStiffness rollStiffness5 = new RollStiffness();
        RollStiffness rollStiffness6 = new RollStiffness();
        RollStiffness rollStiffness7 = new RollStiffness();
        RollStiffness rollStiffness8 = new RollStiffness();
        RollStiffness rollStiffness9 = new RollStiffness();

        rollStiffness1.setProduction_line_id(1L);
        rollStiffness2.setProduction_line_id(1L);
        rollStiffness3.setProduction_line_id(1L);
        rollStiffness4.setProduction_line_id(1L);
        rollStiffness5.setProduction_line_id(1L);
        rollStiffness6.setProduction_line_id(1L);
        rollStiffness7.setProduction_line_id(1L);
        rollStiffness8.setProduction_line_id(1L);
        rollStiffness9.setProduction_line_id(1L);

        rollStiffness1.setFrame_no("F1");
        rollStiffness2.setFrame_no("F2");
        rollStiffness3.setFrame_no("F3");
        rollStiffness4.setFrame_no("F4");
        rollStiffness5.setFrame_no("F5");
        rollStiffness6.setFrame_no("F6");
        rollStiffness7.setFrame_no("F7");
        rollStiffness8.setFrame_no("R1");
        rollStiffness9.setFrame_no("R2");

        //10行数据依次赋值
        v1.setF1(MathUtil.getAvgByList(vfram1));
        v2.setF1(MathUtil.getAvgByList(vfram2));
        v3.setF1(MathUtil.getAvgByList(vfram3));
        v4.setF1(MathUtil.getAvgByList(vfram4));
        v5.setF1(MathUtil.getAvgByList(vfram5));
        v6.setF1(MathUtil.getAvgByList(vfram6));
        v7.setF1(MathUtil.getAvgByList(vfram7));
        v8.setF1(MathUtil.getAvgByList(vfram8));
        v9.setF1(MathUtil.getAvgByList(vfram9));

        if(vfram1.size() > 0) {
        	rollStiffness1.setRetention(MathUtil.getAvgByList(vfram1));
        }
        if(vfram2.size() > 0) {
        	rollStiffness1.setLeveling(MathUtil.getAvgByList(vfram2));
        }
        if(vfram3.size() > 0) {
        	rollStiffness1.setBothsides_stiffness(MathUtil.getAvgByList(vfram3));
        }
        if(vfram4.size() > 0) {
        	rollStiffness1.setBothposition_stiffness(MathUtil.getAvgByList(vfram4));
        }
        if(vfram5.size() > 0) {
        	rollStiffness1.setOsposition(MathUtil.getAvgByList(vfram5));
        }
        if(vfram6.size() > 0) {
        	rollStiffness1.setDsposition(MathUtil.getAvgByList(vfram6));
        }
        if(vfram7.size() > 0) {
        	rollStiffness1.setRolling_force(MathUtil.getAvgByList(vfram7));
        }
        if(vfram8.size() > 0) {
        	rollStiffness1.setOsrollgap(MathUtil.getAvgByList(vfram8));
        }
        if(vfram9.size() > 0) {
        	rollStiffness1.setDsrollgap(MathUtil.getAvgByList(vfram9));
        }

        RollStiffness r1 = score2(rollStiffness1);
        v1.setF1_score(r1.getRetention_score());
        v2.setF1_score(r1.getLeveling_score());
        v3.setF1_score(r1.getBothsides_stiffness_score());
        v4.setF1_score(r1.getBothposition_stiffness_score());
        v5.setF1_score(r1.getOsposition_score());
        v6.setF1_score(r1.getDsposition_score());
        v7.setF1_score(r1.getRolling_force_score());
        v8.setF1_score(r1.getOsrollgap_score());
        v9.setF1_score(r1.getDsrollgap_score());

        v1.setF2(MathUtil.getAvgByList(v2vfram1));
        v2.setF2(MathUtil.getAvgByList(v2vfram2));
        v3.setF2(MathUtil.getAvgByList(v2vfram3));
        v4.setF2(MathUtil.getAvgByList(v2vfram4));
        v5.setF2(MathUtil.getAvgByList(v2vfram5));
        v6.setF2(MathUtil.getAvgByList(v2vfram6));
        v7.setF2(MathUtil.getAvgByList(v2vfram7));
        v8.setF2(MathUtil.getAvgByList(v2vfram8));
        v9.setF2(MathUtil.getAvgByList(v2vfram9));

        if(v2vfram1.size() > 0) {
        	rollStiffness2.setRetention(MathUtil.getAvgByList(v2vfram1));
        }
        if(v2vfram2.size() > 0) {
        	rollStiffness2.setLeveling(MathUtil.getAvgByList(v2vfram2));
        }
        if(v2vfram3.size() > 0) {
        	rollStiffness2.setBothsides_stiffness(MathUtil.getAvgByList(v2vfram3));
        }
        if(v2vfram4.size() > 0) {
        	rollStiffness2.setBothposition_stiffness(MathUtil.getAvgByList(v2vfram4));
        }
        if(v2vfram5.size() > 0) {
        	rollStiffness2.setOsposition(MathUtil.getAvgByList(v2vfram5));
        }
        if(v2vfram6.size() > 0) {
        	rollStiffness2.setDsposition(MathUtil.getAvgByList(v2vfram6));
        }
        if(v2vfram7.size() > 0) {
        	rollStiffness2.setRolling_force(MathUtil.getAvgByList(v2vfram7));
        }
        if(v2vfram8.size() > 0) {
        	rollStiffness2.setOsrollgap(MathUtil.getAvgByList(v2vfram8));
        }
        if(v2vfram9.size() > 0) {
        	rollStiffness2.setDsrollgap(MathUtil.getAvgByList(v2vfram9));
        }

        RollStiffness r2 = score2(rollStiffness2);
        v1.setF2_score(r2.getRetention_score());
        v2.setF2_score(r2.getLeveling_score());
        v3.setF2_score(r2.getBothsides_stiffness_score());
        v4.setF2_score(r2.getBothposition_stiffness_score());
        v5.setF2_score(r2.getOsposition_score());
        v6.setF2_score(r2.getDsposition_score());
        v7.setF2_score(r2.getRolling_force_score());
        v8.setF2_score(r2.getOsrollgap_score());
        v9.setF2_score(r2.getDsrollgap_score());

        v1.setF3(MathUtil.getAvgByList(v3vfram1));
        v2.setF3(MathUtil.getAvgByList(v3vfram2));
        v3.setF3(MathUtil.getAvgByList(v3vfram3));
        v4.setF3(MathUtil.getAvgByList(v3vfram4));
        v5.setF3(MathUtil.getAvgByList(v3vfram5));
        v6.setF3(MathUtil.getAvgByList(v3vfram6));
        v7.setF3(MathUtil.getAvgByList(v3vfram7));
        v8.setF3(MathUtil.getAvgByList(v3vfram8));
        v9.setF3(MathUtil.getAvgByList(v3vfram9));

        if(v3vfram1.size() > 0) {
        	rollStiffness3.setRetention(MathUtil.getAvgByList(v3vfram1));
        }
        if(v3vfram2.size() > 0) {
        	rollStiffness3.setLeveling(MathUtil.getAvgByList(v3vfram2));
        }
        if(v3vfram3.size() > 0) {
        	rollStiffness3.setBothsides_stiffness(MathUtil.getAvgByList(v3vfram3));
        }
        if(v3vfram4.size() > 0) {
        	rollStiffness3.setBothposition_stiffness(MathUtil.getAvgByList(v3vfram4));
        }
        if(v3vfram5.size() > 0) {
        	rollStiffness3.setOsposition(MathUtil.getAvgByList(v3vfram5));
        }
        if(v3vfram6.size() > 0) {
        	rollStiffness3.setDsposition(MathUtil.getAvgByList(v3vfram6));
        }
        if(v3vfram7.size() > 0) {
        	rollStiffness3.setRolling_force(MathUtil.getAvgByList(v3vfram7));
        }
        if(v3vfram8.size() > 0) {
        	rollStiffness3.setOsrollgap(MathUtil.getAvgByList(v3vfram8));
        }
        if(v3vfram9.size() > 0) {
        	rollStiffness3.setDsrollgap(MathUtil.getAvgByList(v3vfram9));
        }

        RollStiffness r3 = score2(rollStiffness3);
        v1.setF3_score(r3.getRetention_score());
        v2.setF3_score(r3.getLeveling_score());
        v3.setF3_score(r3.getBothsides_stiffness_score());
        v4.setF3_score(r3.getBothposition_stiffness_score());
        v5.setF3_score(r3.getOsposition_score());
        v6.setF3_score(r3.getDsposition_score());
        v7.setF3_score(r3.getRolling_force_score());
        v8.setF3_score(r3.getOsrollgap_score());
        v9.setF3_score(r3.getDsrollgap_score());

        v1.setF4(MathUtil.getAvgByList(v4vfram1));
        v2.setF4(MathUtil.getAvgByList(v4vfram2));
        v3.setF4(MathUtil.getAvgByList(v4vfram3));
        v4.setF4(MathUtil.getAvgByList(v4vfram4));
        v5.setF4(MathUtil.getAvgByList(v4vfram5));
        v6.setF4(MathUtil.getAvgByList(v4vfram6));
        v7.setF4(MathUtil.getAvgByList(v4vfram7));
        v8.setF4(MathUtil.getAvgByList(v4vfram8));
        v9.setF4(MathUtil.getAvgByList(v4vfram9));

        if(v4vfram1.size() > 0) {
        	rollStiffness4.setRetention(MathUtil.getAvgByList(v4vfram1));
        }
        if(v4vfram2.size() > 0) {
        	rollStiffness4.setLeveling(MathUtil.getAvgByList(v4vfram2));
        }
        if(v4vfram3.size() > 0) {
        	rollStiffness4.setBothsides_stiffness(MathUtil.getAvgByList(v4vfram3));
        }
        if(v4vfram4.size() > 0) {
        	rollStiffness4.setBothposition_stiffness(MathUtil.getAvgByList(v4vfram4));
        }
        if(v4vfram5.size() > 0) {
        	rollStiffness4.setOsposition(MathUtil.getAvgByList(v4vfram5));
        }
        if(v4vfram6.size() > 0) {
        	rollStiffness4.setDsposition(MathUtil.getAvgByList(v4vfram6));
        }
        if(v4vfram7.size() > 0) {
        	rollStiffness4.setRolling_force(MathUtil.getAvgByList(v4vfram7));
        }
        if(v4vfram8.size() > 0) {
        	rollStiffness4.setOsrollgap(MathUtil.getAvgByList(v4vfram8));
        }
        if(v4vfram9.size() > 0) {
        	rollStiffness4.setDsrollgap(MathUtil.getAvgByList(v4vfram9));
        }
        
        RollStiffness r4 = score2(rollStiffness4);
        v1.setF4_score(r4.getRetention_score());
        v2.setF4_score(r4.getLeveling_score());
        v3.setF4_score(r4.getBothsides_stiffness_score());
        v4.setF4_score(r4.getBothposition_stiffness_score());
        v5.setF4_score(r4.getOsposition_score());
        v6.setF4_score(r4.getDsposition_score());
        v7.setF4_score(r4.getRolling_force_score());
        v8.setF4_score(r4.getOsrollgap_score());
        v9.setF4_score(r4.getDsrollgap_score());

        v1.setF5(MathUtil.getAvgByList(v5vfram1));
        v2.setF5(MathUtil.getAvgByList(v5vfram2));
        v3.setF5(MathUtil.getAvgByList(v5vfram3));
        v4.setF5(MathUtil.getAvgByList(v5vfram4));
        v5.setF5(MathUtil.getAvgByList(v5vfram5));
        v6.setF5(MathUtil.getAvgByList(v5vfram6));
        v7.setF5(MathUtil.getAvgByList(v5vfram7));
        v8.setF5(MathUtil.getAvgByList(v5vfram8));
        v9.setF5(MathUtil.getAvgByList(v5vfram9));

        if(v5vfram1.size() > 0) {
        	 rollStiffness5.setRetention(MathUtil.getAvgByList(v5vfram1));
        }
        if(v5vfram2.size() > 0) {
        	 rollStiffness5.setLeveling(MathUtil.getAvgByList(v5vfram2));
        }
        if(v5vfram3.size() > 0) {
        	rollStiffness5.setBothsides_stiffness(MathUtil.getAvgByList(v5vfram3));
        }
        if(v5vfram4.size() > 0) {
        	rollStiffness5.setBothposition_stiffness(MathUtil.getAvgByList(v5vfram4));
        }
        if(v5vfram5.size() > 0) {
        	rollStiffness5.setOsposition(MathUtil.getAvgByList(v5vfram5));
        }
        if(v5vfram6.size() > 0) {
        	rollStiffness5.setDsposition(MathUtil.getAvgByList(v5vfram6));
        }
        if(v5vfram7.size() > 0) {
        	rollStiffness5.setRolling_force(MathUtil.getAvgByList(v5vfram7));
        }
        if(v5vfram8.size() > 0) {
        	rollStiffness5.setOsrollgap(MathUtil.getAvgByList(v5vfram8));
        }
        if(v5vfram9.size() > 0) {
        	rollStiffness5.setDsrollgap(MathUtil.getAvgByList(v5vfram9));
        }

        RollStiffness r5 = score2(rollStiffness5);
        v1.setF5_score(r5.getRetention_score());
        v2.setF5_score(r5.getLeveling_score());
        v3.setF5_score(r5.getBothsides_stiffness_score());
        v4.setF5_score(r5.getBothposition_stiffness_score());
        v5.setF5_score(r5.getOsposition_score());
        v6.setF5_score(r5.getDsposition_score());
        v7.setF5_score(r5.getRolling_force_score());
        v8.setF5_score(r5.getOsrollgap_score());
        v9.setF5_score(r5.getDsrollgap_score());

        v1.setF6(MathUtil.getAvgByList(v6vfram1));
        v2.setF6(MathUtil.getAvgByList(v6vfram2));
        v3.setF6(MathUtil.getAvgByList(v6vfram3));
        v4.setF6(MathUtil.getAvgByList(v6vfram4));
        v5.setF6(MathUtil.getAvgByList(v6vfram5));
        v6.setF6(MathUtil.getAvgByList(v6vfram6));
        v7.setF6(MathUtil.getAvgByList(v6vfram7));
        v8.setF6(MathUtil.getAvgByList(v6vfram8));
        v9.setF6(MathUtil.getAvgByList(v6vfram9));

        
       if(v6vfram1.size() > 0) {
    	   rollStiffness6.setRetention(MathUtil.getAvgByList(v6vfram1));
       }
       if(v6vfram2.size() > 0) {
    	   rollStiffness6.setLeveling(MathUtil.getAvgByList(v6vfram2));
       }
       if(v6vfram3.size() > 0) {
    	   rollStiffness6.setBothsides_stiffness(MathUtil.getAvgByList(v6vfram3));
       }
       if(v6vfram4.size() > 0) {
    	   rollStiffness6.setBothposition_stiffness(MathUtil.getAvgByList(v6vfram4));
       }
       if(v6vfram5.size() > 0) {
    	   rollStiffness6.setOsposition(MathUtil.getAvgByList(v6vfram5));
       }
       if(v6vfram6.size() > 0) {
    	   rollStiffness6.setDsposition(MathUtil.getAvgByList(v6vfram6));
       }
       if(v6vfram7.size() > 0) {
    	   rollStiffness6.setRolling_force(MathUtil.getAvgByList(v6vfram7));
       }
       if(v6vfram8.size() > 0) {
    	   rollStiffness6.setOsrollgap(MathUtil.getAvgByList(v6vfram8));
       }
       if(v6vfram9.size() > 0) {
    	   rollStiffness6.setDsrollgap(MathUtil.getAvgByList(v6vfram9));
       }

        RollStiffness r6 = score2(rollStiffness6);
        v1.setF6_score(r6.getRetention_score());
        v2.setF6_score(r6.getLeveling_score());
        v3.setF6_score(r6.getBothsides_stiffness_score());
        v4.setF6_score(r6.getBothposition_stiffness_score());
        v5.setF6_score(r6.getOsposition_score());
        v6.setF6_score(r6.getDsposition_score());
        v7.setF6_score(r6.getRolling_force_score());
        v8.setF6_score(r6.getOsrollgap_score());
        v9.setF6_score(r6.getDsrollgap_score());

        v1.setF7(MathUtil.getAvgByList(v7vfram1));
        v2.setF7(MathUtil.getAvgByList(v7vfram2));
        v3.setF7(MathUtil.getAvgByList(v7vfram3));
        v4.setF7(MathUtil.getAvgByList(v7vfram4));
        v5.setF7(MathUtil.getAvgByList(v7vfram5));
        v6.setF7(MathUtil.getAvgByList(v7vfram6));
        v7.setF7(MathUtil.getAvgByList(v7vfram7));
        v8.setF7(MathUtil.getAvgByList(v7vfram8));
        v9.setF7(MathUtil.getAvgByList(v7vfram9));
        
        if(v7vfram1.size() > 0) {
        	rollStiffness7.setRetention(MathUtil.getAvgByList(v7vfram1));
        }
        if(v7vfram2.size() > 0) {
        	rollStiffness7.setLeveling(MathUtil.getAvgByList(v7vfram2));
        }
        if(v7vfram3.size() > 0) {
        	rollStiffness7.setBothsides_stiffness(MathUtil.getAvgByList(v7vfram3));
        }
        if(v7vfram4.size() > 0) {
        	rollStiffness7.setBothposition_stiffness(MathUtil.getAvgByList(v7vfram4));
        }
        if(v7vfram5.size() > 0) {
        	rollStiffness7.setOsposition(MathUtil.getAvgByList(v7vfram5));
        }
        if(v7vfram6.size() > 0) {
        	rollStiffness7.setDsposition(MathUtil.getAvgByList(v7vfram6));
        }
        if(v7vfram7.size() > 0) {
        	rollStiffness7.setRolling_force(MathUtil.getAvgByList(v7vfram7));
        }
        if(v7vfram8.size() > 0) {
        	rollStiffness7.setOsrollgap(MathUtil.getAvgByList(v7vfram8));
        }
        if(v7vfram9.size() > 0) {
        	rollStiffness7.setDsrollgap(MathUtil.getAvgByList(v7vfram9));
        }

        RollStiffness r7 = score2(rollStiffness7);
        v1.setF7_score(r7.getRetention_score());
        v2.setF7_score(r7.getLeveling_score());
        v3.setF7_score(r7.getBothsides_stiffness_score());
        v4.setF7_score(r7.getBothposition_stiffness_score());
        v5.setF7_score(r7.getOsposition_score());
        v6.setF7_score(r7.getDsposition_score());
        v7.setF7_score(r7.getRolling_force_score());
        v8.setF7_score(r7.getOsrollgap_score());
        v9.setF7_score(r7.getDsrollgap_score());

        v1.setR1(MathUtil.getAvgByList(v8vfram1));
        v2.setR1(MathUtil.getAvgByList(v8vfram2));
        v3.setR1(MathUtil.getAvgByList(v8vfram3));
        v4.setR1(MathUtil.getAvgByList(v8vfram4));
        v5.setR1(MathUtil.getAvgByList(v8vfram5));
        v6.setR1(MathUtil.getAvgByList(v8vfram6));
        v7.setR1(MathUtil.getAvgByList(v8vfram7));
        v8.setR1(MathUtil.getAvgByList(v8vfram8));
        v9.setR1(MathUtil.getAvgByList(v8vfram9));

        if(v8vfram1.size() > 0) {
        	rollStiffness8.setRetention(MathUtil.getAvgByList(v8vfram1));
        }
        if(v8vfram2.size() > 0) {
        	rollStiffness8.setLeveling(MathUtil.getAvgByList(v8vfram2));
        }
        if(v8vfram3.size() > 0) {
        	rollStiffness8.setBothsides_stiffness(MathUtil.getAvgByList(v8vfram3));
        }
        if(v8vfram4.size() > 0) {
        	rollStiffness8.setBothposition_stiffness(MathUtil.getAvgByList(v8vfram4));
        }
        if(v8vfram5.size() > 0) {
        	rollStiffness8.setOsposition(MathUtil.getAvgByList(v8vfram5));
        }
        if(v8vfram6.size() > 0) {
        	rollStiffness8.setDsposition(MathUtil.getAvgByList(v8vfram6));
        }
        if(v8vfram7.size() > 0) {
        	rollStiffness8.setRolling_force(MathUtil.getAvgByList(v8vfram7));
        }
        if(v8vfram8.size() > 0) {
            rollStiffness8.setOsrollgap(MathUtil.getAvgByList(v8vfram8));
        }
        if(v8vfram9.size() > 0) {
        	rollStiffness8.setDsrollgap(MathUtil.getAvgByList(v8vfram9));
        }

        RollStiffness r8 = score2(rollStiffness8);
        v1.setR1_score(r8.getRetention_score());
        v2.setR1_score(r8.getLeveling_score());
        v3.setR1_score(r8.getBothsides_stiffness_score());
        v4.setR1_score(r8.getBothposition_stiffness_score());
        v5.setR1_score(r8.getOsposition_score());
        v6.setR1_score(r8.getDsposition_score());
        v7.setR1_score(r8.getRolling_force_score());
        v8.setR1_score(r8.getOsrollgap_score());
        v9.setR1_score(r8.getDsrollgap_score());

        v1.setR2(MathUtil.getAvgByList(v9vfram1));
        v2.setR2(MathUtil.getAvgByList(v9vfram2));
        v3.setR2(MathUtil.getAvgByList(v9vfram3));
        v4.setR2(MathUtil.getAvgByList(v9vfram4));
        v5.setR2(MathUtil.getAvgByList(v9vfram5));
        v6.setR2(MathUtil.getAvgByList(v9vfram6));
        v7.setR2(MathUtil.getAvgByList(v9vfram7));
        v8.setR2(MathUtil.getAvgByList(v9vfram8));
        v9.setR2(MathUtil.getAvgByList(v9vfram9));

        if(v9vfram1.size() > 0) {
        	rollStiffness9.setRetention(MathUtil.getAvgByList(v9vfram1));
        }
        if(v9vfram2.size() > 0) {
        	 rollStiffness9.setLeveling(MathUtil.getAvgByList(v9vfram2));
        }
        if(v9vfram3.size() > 0) {
        	rollStiffness9.setBothsides_stiffness(MathUtil.getAvgByList(v9vfram3));
        }
        if(v9vfram4.size() > 0) {
        	 rollStiffness9.setBothposition_stiffness(MathUtil.getAvgByList(v9vfram4));
        }
        if(v9vfram5.size() > 0) {
        	rollStiffness9.setOsposition(MathUtil.getAvgByList(v9vfram5));
        }
        if(v9vfram6.size() > 0) {
            rollStiffness9.setDsposition(MathUtil.getAvgByList(v9vfram6));
        }
        if(v9vfram7.size() > 0) {
        	rollStiffness9.setRolling_force(MathUtil.getAvgByList(v9vfram7));
        }
        if(v9vfram8.size() > 0) {
        	rollStiffness9.setOsrollgap(MathUtil.getAvgByList(v9vfram8));
        }
        if(v9vfram9.size() > 0) {
        	rollStiffness9.setDsrollgap(MathUtil.getAvgByList(v9vfram9));
        }

        RollStiffness r9 = score2(rollStiffness9);
        v1.setR2_score(r9.getRetention_score());
        v2.setR2_score(r9.getLeveling_score());
        v3.setR2_score(r9.getBothsides_stiffness_score());
        v4.setR2_score(r9.getBothposition_stiffness_score());
        v5.setR2_score(r9.getOsposition_score());
        v6.setR2_score(r9.getDsposition_score());
        v7.setR2_score(r9.getRolling_force_score());
        v8.setR2_score(r9.getOsrollgap_score());
        v9.setR2_score(r9.getDsrollgap_score());
        
        Double t1 = MathUtil.getSumByList(v1.getF1_score(),v2.getF1_score(),v3.getF1_score(),v4.getF1_score(),v5.getF1_score(),v6.getF1_score(),v7.getF1_score(),v8.getF1_score(),v9.getF1_score());
        Double t2 = MathUtil.getSumByList(v1.getF2_score(),v2.getF2_score(),v3.getF2_score(),v4.getF2_score(),v5.getF2_score(),v6.getF2_score(),v7.getF2_score(),v8.getF2_score(),v9.getF2_score());
        Double t3 = MathUtil.getSumByList(v1.getF3_score(),v2.getF3_score(),v3.getF3_score(),v4.getF3_score(),v5.getF3_score(),v6.getF3_score(),v7.getF3_score(),v8.getF3_score(),v9.getF3_score());
        Double t4 = MathUtil.getSumByList(v1.getF4_score(),v2.getF4_score(),v3.getF4_score(),v4.getF4_score(),v5.getF4_score(),v6.getF4_score(),v7.getF4_score(),v8.getF4_score(),v9.getF4_score());
        Double t5 = MathUtil.getSumByList(v1.getF5_score(),v2.getF5_score(),v3.getF5_score(),v4.getF5_score(),v5.getF5_score(),v6.getF5_score(),v7.getF5_score(),v8.getF5_score(),v9.getF5_score());
        Double t6 = MathUtil.getSumByList(v1.getF6_score(),v2.getF6_score(),v3.getF6_score(),v4.getF6_score(),v5.getF6_score(),v6.getF6_score(),v7.getF6_score(),v8.getF6_score(),v9.getF6_score());
        Double t7 = MathUtil.getSumByList(v1.getF7_score(),v2.getF7_score(),v3.getF7_score(),v4.getF7_score(),v5.getF7_score(),v6.getF7_score(),v7.getF7_score(),v8.getF7_score(),v9.getF7_score());
        Double t8 = MathUtil.getSumByList(v1.getR1_score(),v2.getR1_score(),v3.getR1_score(),v4.getR1_score(),v5.getR1_score(),v6.getR1_score(),v7.getR1_score(),v8.getR1_score(),v9.getR1_score());
        Double t9 = MathUtil.getSumByList(v1.getR2_score(),v2.getR2_score(),v3.getR2_score(),v4.getR2_score(),v5.getR2_score(),v6.getR2_score(),v7.getR2_score(),v8.getR2_score(),v9.getR2_score());
        
        v10.setF1_score(t1);
        v10.setF2_score(t2);
        v10.setF3_score(t3);
        v10.setF4_score(t4);
        v10.setF5_score(t5);
        v10.setF6_score(t6);
        v10.setF7_score(t7);
        v10.setR1_score(t8);
        v10.setR2_score(t9);

        List<VStiffness> totle_list = new ArrayList<VStiffness>();
        totle_list.add(v1);
        totle_list.add(v2);
        totle_list.add(v3);
        totle_list.add(v4);
        totle_list.add(v5);
        totle_list.add(v6);
        totle_list.add(v7);
        totle_list.add(v8);
        totle_list.add(v9);
        totle_list.add(v10);
        return totle_list;
    }

    @Override
    public String excel(HSSFWorkbook workbook, String recordtime, String production_line_id, String dbegin, String dend) {

        //开始时间
        String starttime = null;
        if (!StringUtils.isEmpty(dbegin)) {
            starttime = dbegin;
        }

        //结束时间
        String endtime = null;
        if (!StringUtils.isEmpty(dend)) {
            endtime = dend;
        }

        //产线
        String cx = null;
        if (!StringUtils.isEmpty(production_line_id)) {
            cx = production_line_id;
        }

        String filename = "轧机刚度评价";

        String title = null;
        if (!StringUtils.isEmpty(recordtime)) {
            title = recordtime;
            filename = recordtime + filename;
        }
        List<RollStiffness> list = new ArrayList<RollStiffness>();

        if (!StringUtils.isEmpty(recordtime)) {
            list = rollStiffnessDao.findTableExcelone(starttime, endtime, cx, recordtime.substring(0, 13));
        } else {
            list = rollStiffnessDao.findTable(starttime, endtime, cx);
        }


        //按照excel中从上到下的顺序依次排序，所以下面的注解按照这个思路就行
        List<VStiffness> totle_list = getVStiffnessList(list);

        HSSFSheet sheet = workbook.createSheet("sheet1");
        getExcel(totle_list, sheet, workbook, production_line_id);
        return filename;
    }

    private void getExcel(List<VStiffness> list, HSSFSheet sheet, HSSFWorkbook workbook, String production_line_id) {
        DecimalFormat df = new DecimalFormat("#0.00");
        // 设置表头样式 居中 加粗 宋体 20
        HSSFCellStyle cellStyleHead = workbook.createCellStyle();
        cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
        HSSFFont redFontHead = workbook.createFont();
        redFontHead.setFontHeightInPoints((short) 12);//设置字体大小
        redFontHead.setFontName("宋体");//字体型号
        cellStyleHead.setFont(redFontHead);


        HSSFRow rowhead = sheet.createRow(0);   //创建第一行头

        HSSFCell cell_00 = rowhead.createCell(0);   //创建第一行第一列
        cell_00.setCellStyle(cellStyleHead);
        if (production_line_id.equals("1")) {
            cell_00.setCellValue("2250轧机刚度评价");
        } else if (production_line_id.equals("2")) {
            cell_00.setCellValue("1580轧机刚度评价");
        }

        HSSFRow row1 = sheet.createRow(1);   //创建第二行
        HSSFCell cell_2_1 = row1.createCell(0);   //创建第二行第一列
        cell_2_1.setCellValue("编号");
        HSSFCell cell_2_2 = row1.createCell(1);   //创建第二行第二列
        cell_2_2.setCellValue("项目");
        HSSFCell cell_2_3 = row1.createCell(2);   //创建第二行第三列
        cell_2_3.setCellValue("单位");
        HSSFCell cell_2_4 = row1.createCell(3);   //创建第二行第四列
        cell_2_4.setCellValue("权重");
        HSSFCell cell_2_5 = row1.createCell(4);   //创建第二行第五列
        cell_2_5.setCellValue("F1机架");
        HSSFCell cell_2_6 = row1.createCell(5);   //创建第二行第六列
        cell_2_6.setCellValue("得分");
        HSSFCell cell_2_7 = row1.createCell(6);   //创建第二行第七列
        cell_2_7.setCellValue("F2机架");
        HSSFCell cell_2_8 = row1.createCell(7);   //创建第二行第八列
        cell_2_8.setCellValue("得分");
        HSSFCell cell_2_9 = row1.createCell(8);   //创建第二行第九列
        cell_2_9.setCellValue("F3机架");
        HSSFCell cell_2_10 = row1.createCell(9);   //创建第二行第十列
        cell_2_10.setCellValue("得分");
        HSSFCell cell_2_11 = row1.createCell(10);   //创建第二行第十一列
        cell_2_11.setCellValue("F4机架");
        HSSFCell cell_2_12 = row1.createCell(11);   //创建第二行第十二列
        cell_2_12.setCellValue("得分");
        HSSFCell cell_2_13 = row1.createCell(12);   //创建第二行第十三列
        cell_2_13.setCellValue("F5机架");
        HSSFCell cell_2_14 = row1.createCell(13);   //创建第二行第十四列
        cell_2_14.setCellValue("得分");
        HSSFCell cell_2_15 = row1.createCell(14);   //创建第二行第十五列
        cell_2_15.setCellValue("F6机架");
        HSSFCell cell_2_16 = row1.createCell(15);   //创建第二行第十六列
        cell_2_16.setCellValue("得分");
        HSSFCell cell_2_17 = row1.createCell(16);   //创建第二行第十七列
        cell_2_17.setCellValue("F7机架");
        HSSFCell cell_2_18 = row1.createCell(17);   //创建第二行第十八列
        cell_2_18.setCellValue("得分");
        HSSFCell cell_2_19 = row1.createCell(18);   //创建第二行第十八列
        cell_2_19.setCellValue("R1F3机架");
        HSSFCell cell_2_20 = row1.createCell(19);   //创建第二行第十八列
        cell_2_20.setCellValue("得分");
        HSSFCell cell_2_21 = row1.createCell(20);   //创建第二行第十八列
        cell_2_21.setCellValue("R2机架");
        HSSFCell cell_2_22 = row1.createCell(21);   //创建第二行第十八列
        cell_2_22.setCellValue("得分");


        //从第三行开始(包含第三行)
        for (int i = 0; i < list.size(); i++) {
            VStiffness v = list.get(i);
            HSSFRow rown = sheet.createRow(i + 2);   //创建第三行
            HSSFCell cell_3_1 = rown.createCell(0);   //创建第三行第一列,编号
            cell_3_1.setCellValue(i + 1);
            HSSFCell cell_3_2 = rown.createCell(1);   //创建第三行第二列,项目
            cell_3_2.setCellValue(v.getXm());
            HSSFCell cell_3_3 = rown.createCell(2);   //创建第三行第三列,单位
            cell_3_3.setCellValue(v.getUnit());
            HSSFCell cell_3_4 = rown.createCell(3);   //创建第三行第四列,权重
            cell_3_4.setCellValue(v.getQz());
            HSSFCell cell_3_5 = rown.createCell(4);   //创建第三行第五列,F1机架
            if (!StringUtils.isEmpty(v.getF1())) {
                cell_3_5.setCellValue(df.format(v.getF1()));
            }
            HSSFCell cell_3_6 = rown.createCell(5);   //创建第三行第六列,F1得分
            if (!StringUtils.isEmpty(v.getF1_score())) {
                cell_3_6.setCellValue(df.format(v.getF1_score()));
            }
            HSSFCell cell_3_7 = rown.createCell(6);   //创建第三行第七列,F2机架
            if (!StringUtils.isEmpty(v.getF2())) {
                cell_3_7.setCellValue(df.format(v.getF2()));
            }
            HSSFCell cell_3_8 = rown.createCell(7);   //创建第三行第八列,F2得分
            if (!StringUtils.isEmpty(v.getF2_score())) {
                cell_3_8.setCellValue(df.format(v.getF2_score()));
            }
            HSSFCell cell_3_9 = rown.createCell(8);   //创建第三行第九列,F3机架
            if (!StringUtils.isEmpty(v.getF3())) {
                cell_3_9.setCellValue(df.format(v.getF3()));
            }
            HSSFCell cell_3_10 = rown.createCell(9);   //创建第三行第十列,F3得分
            if (!StringUtils.isEmpty(v.getF3_score())) {
                cell_3_10.setCellValue(df.format(v.getF3_score()));
            }
            HSSFCell cell_3_11 = rown.createCell(10);   //创建第三行第十一列,F4机架
            if (!StringUtils.isEmpty(v.getF4())) {
                cell_3_11.setCellValue(df.format(v.getF4()));
            }
            HSSFCell cell_3_12 = rown.createCell(11);   //创建第三行第十二列,F4得分
            if (!StringUtils.isEmpty(v.getF4_score())) {
                cell_3_12.setCellValue(df.format(v.getF4_score()));
            }
            HSSFCell cell_3_13 = rown.createCell(12);   //创建第三行第十三列,F5机架
            if (!StringUtils.isEmpty(v.getF5())) {
                cell_3_13.setCellValue(df.format(v.getF5()));
            }
            HSSFCell cell_3_14 = rown.createCell(13);   //创建第三行第十四列,F5得分
            if (!StringUtils.isEmpty(v.getF5_score())) {
                cell_3_14.setCellValue(df.format(v.getF5_score()));
            }
            HSSFCell cell_3_15 = rown.createCell(14);   //创建第三行第十五列,F6机架
            if (!StringUtils.isEmpty(v.getF6())) {
                cell_3_15.setCellValue(df.format(v.getF6()));
            }
            HSSFCell cell_3_16 = rown.createCell(15);   //创建第三行第十六列,F6得分
            if (!StringUtils.isEmpty(v.getF6_score())) {
                cell_3_16.setCellValue(df.format(v.getF6_score()));
            }
            HSSFCell cell_3_17 = rown.createCell(16);   //创建第三行第十七列,F7机架
            if (!StringUtils.isEmpty(v.getF7())) {
                cell_3_17.setCellValue(df.format(v.getF7()));
            }
            HSSFCell cell_3_18 = rown.createCell(17);   //创建第三行第十八列,F7得分
            if (!StringUtils.isEmpty(v.getF7_score())) {
                cell_3_18.setCellValue(df.format(v.getF7_score()));
            }
            HSSFCell cell_3_19 = rown.createCell(18);   //创建第三行第十九列,R1机架
            if (!StringUtils.isEmpty(v.getR1())) {
                cell_3_19.setCellValue(df.format(v.getR1()));
            }
            HSSFCell cell_3_20 = rown.createCell(19);   //创建第三行第二十列,R1得分
            if (!StringUtils.isEmpty(v.getR1_score())) {
                cell_3_20.setCellValue(df.format(v.getR1_score()));
            }
            HSSFCell cell_3_21 = rown.createCell(20);   //创建第三行第二十一列,R2机架
            if (!StringUtils.isEmpty(v.getR2())) {
                cell_3_21.setCellValue(df.format(v.getR2()));
            }
            HSSFCell cell_3_22 = rown.createCell(21);   //创建第三行第二十二列,R2得分
            if (!StringUtils.isEmpty(v.getR2_score())) {
                cell_3_22.setCellValue(df.format(v.getR2_score()));
            }
        }
        CellRangeAddress region = new CellRangeAddress(0, 0, 0, 21);
        sheet.addMergedRegion(region);
    }

    public List<DpdEntity> findlist(String data) {
        JRollStiffness jrollStiffness = JSON.parseObject(data, JRollStiffness.class);
        JSONObject jsonObject = null;

        if (null != jrollStiffness.getCondition()) {
            jsonObject = JSON.parseObject(jrollStiffness.getCondition().toString());
        }

        //开始时间
        String dbegin = null;
        if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
            dbegin = jsonObject.get("dbegin").toString();
        }

        //结束时间
        String dend = null;
        if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
            dend = jsonObject.get("dend").toString();
        }

        //产线id
        String production_line_id = null;
        if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
            production_line_id = jsonObject.get("production_line_id").toString();
        }

        List<RollStiffness> time = rollStiffnessDao.findlist(dbegin, dend, production_line_id);//根据时间区间查询集合数据
        List<DpdEntity> list = new ArrayList<>();
        for (RollStiffness entity : time) {
            DpdEntity dpd = new DpdEntity();
            dpd.setKey(String.valueOf(entity.getIndocno()));
            dpd.setValue(entity.getRecordtime());
            list.add(dpd);
        }
        return list;
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findByThird(String data) {
        try {
            JRollStiffness jrollStiffness = JSON.parseObject(data, JRollStiffness.class);
            JSONObject jsonObject = null;

            if (null != jrollStiffness.getCondition()) {
                jsonObject = JSON.parseObject(jrollStiffness.getCondition().toString());
            }

            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("startTime"))) {
                dbegin = jsonObject.get("startTime").toString();
            }

            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("endTime"))) {
                dend = jsonObject.get("endTime").toString();
            }

            Long frame_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
                frame_noid = Long.valueOf(jsonObject.get("frame_noid").toString());
            }

            //产线id
            Long production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
            }

            String w_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("w_no"))) {
                w_no = jsonObject.get("w_no").toString();
            }

            String b_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("b_no"))) {
                b_no = jsonObject.get("b_no").toString();
            }
            List<RollStiffness> list = rollStiffnessDao.findByThird(dbegin, dend, frame_noid, w_no, b_no, production_line_id);
            Integer count = rollStiffnessDao.findDataRollStiffnessByPageSize(dbegin, dend, frame_noid, w_no, b_no, production_line_id);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

//    /**
//     * 单项评分查询
//     *
//     * @param production_line_id 产线
//     * @param frame_no           机架
//     * @param ids                评分项类指
//     * @param math               评分的数值
//     * @return
//     */
//    @Override
//    public Double findForSimpleScore(Long production_line_id, String frame_no, Long ids, Double math) {
//        RollStiffness rollStiffness = new RollStiffness();
//        rollStiffness.setProduction_line_id(production_line_id);
//        rollStiffness.setFrame_no(frame_no);
//        RollStiffness rollStiffness1 = new RollStiffness();
//        Double score = 0.00;
//        if (ids == 1L) {
//            //刚度保持率(%)
//            rollStiffness.setRetention(math);
//            rollStiffness1 = score2(rollStiffness);
//            score = rollStiffness1.getRetention_score();
//        } else if (ids == 2L) {
//            //调平值(mm)
//            rollStiffness.setLeveling(math);
//            rollStiffness1 = score2(rollStiffness);
//            score = rollStiffness1.getLeveling_score();
//        } else if (ids == 3L) {
//            //两侧刚度偏差(KN/mm)
//            rollStiffness.setBothsides_stiffness(math);
//            rollStiffness1 = score2(rollStiffness);
//            score = rollStiffness1.getBothsides_stiffness_score();
//        } else if (ids == 4L) {
//            //两侧位置偏差(mm)
//            rollStiffness.setBothposition_stiffness(math);
//            rollStiffness1 = score2(rollStiffness);
//            score = rollStiffness1.getBothposition_stiffness_score();
//        } else if (ids == 5L) {
//            //OS位置偏差(mm)
//            rollStiffness.setOsposition(math);
//            rollStiffness1 = score2(rollStiffness);
//            score = rollStiffness1.getOsposition_score();
//        } else if (ids == 6L) {
//            //DS位置偏差(mm)
//            rollStiffness.setDsposition(math);
//            rollStiffness1 = score2(rollStiffness);
//            score = rollStiffness1.getDsposition_score();
//        } else if (ids == 7L) {
//            //轧制力偏差(KN)
//            rollStiffness.setRolling_force(math);
//            rollStiffness1 = score2(rollStiffness);
//            score = rollStiffness1.getRolling_force_score();
//        } else if (ids == 8L) {
//            //OS辊缝偏差(mm)
//            rollStiffness.setOsrollgap(math);
//            rollStiffness1 = score2(rollStiffness);
//            score = rollStiffness1.getOsrollgap_score();
//        } else if (ids == 9L) {
//            //DS辊缝偏差(mm)
//            rollStiffness.setDsrollgap(math);
//            rollStiffness1 = score2(rollStiffness);
//            score = rollStiffness1.getDsrollgap_score();
//        }
//
//        return score;
//    }

    public RollStiffness score2(RollStiffness rollStiffness) {
        //获取机架号
        String frame_no = rollStiffness.getFrame_no();

        //反查——获取机架号id
        Long frame_noid = Long.valueOf(dictionaryDao.findMapBynameV1("frameteam", frame_no));

        //评分
        //根据机架号、产线查询标准主表主键
        Long inmain = stiffnessStandardDao.findIndocno(frame_noid, rollStiffness.getProduction_line_id());

        //根据主表主键查子表项目列表
        List<StiffnessProject> listp = stiffnessProjectDao.findDataStiffnessProjectByIlinkno(inmain);

        for (StiffnessProject sp : listp) {
            //根据指标主键查询子表得分
            List<StiffnessScore> listsc = stiffnessScoreDao.findDataStiffnessScoreByIlinkno(sp.getIndocno());
            for (StiffnessScore entity : listsc) {
                //min max 有 无
                if (!StringUtils.isEmpty(entity.getMin()) && StringUtils.isEmpty(entity.getMax())) {
                    //判断是哪个指标
                    if (sp.getField_name().equals("retention")) {
                        //对比数据大小
                        if (!StringUtils.isEmpty(rollStiffness.getRetention())) {
                            if (rollStiffness.getRetention() >= entity.getMin()) {
                                rollStiffness.setRetention_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setRetention_score(null);
                        }
                    } else if (sp.getField_name().equals("leveling")) {
                        if (!StringUtils.isEmpty(rollStiffness.getLeveling())) {
                            if (Math.abs(rollStiffness.getLeveling()) >= entity.getMin()) {
                                rollStiffness.setLeveling_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setLeveling_score(null);
                        }
                    } else if (sp.getField_name().equals("bothsides_stiffness")) {
                        if (!StringUtils.isEmpty(rollStiffness.getBothsides_stiffness())) {
                            if (Math.abs(rollStiffness.getBothsides_stiffness()) >= entity.getMin()) {
                                rollStiffness.setBothsides_stiffness_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setBothsides_stiffness_score(null);
                        }
                    } else if (sp.getField_name().equals("bothposition_stiffness")) {
                        if (!StringUtils.isEmpty(rollStiffness.getBothposition_stiffness())) {
                            if (Math.abs(rollStiffness.getBothposition_stiffness()) >= entity.getMin()) {
                                rollStiffness.setBothposition_stiffness_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setBothposition_stiffness_score(null);
                        }
                    } else if (sp.getField_name().equals("osposition")) {
                        if (!StringUtils.isEmpty(rollStiffness.getOsposition())) {
                            if (Math.abs(rollStiffness.getOsposition()) >= entity.getMin()) {
                                rollStiffness.setOsposition_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setOsposition_score(null);
                        }
                    } else if (sp.getField_name().equals("dsposition")) {
                        if (!StringUtils.isEmpty(rollStiffness.getDsposition())) {
                            if (Math.abs(rollStiffness.getDsposition()) >= entity.getMin()) {
                                rollStiffness.setDsposition_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setDsposition_score(null);
                        }
                    } else if (sp.getField_name().equals("rolling_force")) {
                        if (!StringUtils.isEmpty(rollStiffness.getRolling_force())) {
                            if (Math.abs(rollStiffness.getRolling_force()) >= entity.getMin()) {
                                rollStiffness.setRolling_force_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setRolling_force_score(null);
                        }
                    } else if (sp.getField_name().equals("osrollgap")) {
                        if (!StringUtils.isEmpty(rollStiffness.getOsrollgap())) {
                            if (Math.abs(rollStiffness.getOsrollgap()) >= entity.getMin()) {
                                rollStiffness.setOsrollgap_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setOsrollgap_score(null);
                        }
                    } else if (sp.getField_name().equals("dsrollgap")) {
                        if (!StringUtils.isEmpty(rollStiffness.getDsrollgap())) {
                            if (Math.abs(rollStiffness.getDsrollgap()) >= entity.getMin()) {
                                rollStiffness.setDsrollgap_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setDsrollgap_score(null);
                        }
                    }
                }
                //无 有
                else if (StringUtils.isEmpty(entity.getMin()) && !StringUtils.isEmpty(entity.getMax())) {
                    //判断是哪个指标
                    if (sp.getField_name().equals("retention")) {
                        //对比数据大小
                        if (!StringUtils.isEmpty(rollStiffness.getRetention())) {
                            if (rollStiffness.getRetention() < entity.getMax()) {
                                rollStiffness.setRetention_score(entity.getScore());
                                System.out.println(rollStiffness.getRetention_score());
                            }
                        } else {
                            rollStiffness.setRetention_score(null);
                        }
                    } else if (sp.getField_name().equals("leveling")) {
                        if (!StringUtils.isEmpty(rollStiffness.getLeveling())) {
                            if (Math.abs(rollStiffness.getLeveling()) < entity.getMax()) {
                                rollStiffness.setLeveling_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setLeveling_score(null);
                        }
                    } else if (sp.getField_name().equals("bothsides_stiffness")) {
                        if (!StringUtils.isEmpty(rollStiffness.getBothsides_stiffness())) {
                            if (Math.abs(rollStiffness.getBothsides_stiffness()) < entity.getMax()) {
                                rollStiffness.setBothsides_stiffness_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setBothsides_stiffness_score(null);
                        }
                    } else if (sp.getField_name().equals("bothposition_stiffness")) {
                        if (!StringUtils.isEmpty(rollStiffness.getBothposition_stiffness())) {
                            if (Math.abs(rollStiffness.getBothposition_stiffness()) < entity.getMax()) {
                                rollStiffness.setBothposition_stiffness_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setBothposition_stiffness_score(null);
                        }
                    } else if (sp.getField_name().equals("osposition")) {
                        if (!StringUtils.isEmpty(rollStiffness.getOsposition())) {
                            if (Math.abs(rollStiffness.getOsposition()) < entity.getMax()) {
                                rollStiffness.setOsposition_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setOsposition_score(null);
                        }
                    } else if (sp.getField_name().equals("dsposition")) {
                        if (!StringUtils.isEmpty(rollStiffness.getDsposition())) {
                            if (Math.abs(rollStiffness.getDsposition()) < entity.getMax()) {
                                rollStiffness.setDsposition_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setDsposition_score(null);
                        }
                    } else if (sp.getField_name().equals("rolling_force")) {
                        if (!StringUtils.isEmpty(rollStiffness.getRolling_force())) {
                            if (Math.abs(rollStiffness.getRolling_force()) < entity.getMax()) {
                                rollStiffness.setRolling_force_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setRolling_force_score(null);
                        }
                    } else if (sp.getField_name().equals("osrollgap")) {
                        if (!StringUtils.isEmpty(rollStiffness.getOsrollgap())) {
                            if (Math.abs(rollStiffness.getOsrollgap()) < entity.getMax()) {
                                rollStiffness.setOsrollgap_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setOsrollgap_score(null);
                        }
                    } else if (sp.getField_name().equals("dsrollgap")) {
                        if (!StringUtils.isEmpty(rollStiffness.getDsrollgap())) {
                            if (Math.abs(rollStiffness.getDsrollgap()) < entity.getMax()) {
                                rollStiffness.setDsrollgap_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setDsrollgap_score(null);
                        }
                    }
                }
                //有 有
                else if (!StringUtils.isEmpty(entity.getMin()) && !StringUtils.isEmpty(entity.getMax())) {
                    //判断是哪个指标
                    if (sp.getField_name().equals("retention")) {
                        //对比数据大小
                        if (!StringUtils.isEmpty(rollStiffness.getRetention())) {
                            if (rollStiffness.getRetention() < entity.getMax() && rollStiffness.getRetention() >= entity.getMin()) {
                                rollStiffness.setRetention_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setRetention_score(null);
                        }
                    } else if (sp.getField_name().equals("leveling")) {
                        if (!StringUtils.isEmpty(rollStiffness.getLeveling())) {
                            if (Math.abs(rollStiffness.getLeveling()) < entity.getMax() && Math.abs(rollStiffness.getLeveling()) >= entity.getMin()) {
                                rollStiffness.setLeveling_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setLeveling_score(null);
                        }
                    } else if (sp.getField_name().equals("bothsides_stiffness")) {
                        if (!StringUtils.isEmpty(rollStiffness.getBothsides_stiffness())) {
                            if (Math.abs(rollStiffness.getBothsides_stiffness()) < entity.getMax() && Math.abs(rollStiffness.getBothsides_stiffness()) >= entity.getMin()) {
                                rollStiffness.setBothsides_stiffness_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setBothsides_stiffness_score(null);
                        }
                    } else if (sp.getField_name().equals("bothposition_stiffness")) {
                        if (!StringUtils.isEmpty(rollStiffness.getBothposition_stiffness())) {
                            if (Math.abs(rollStiffness.getBothposition_stiffness()) < entity.getMax() && Math.abs(rollStiffness.getBothposition_stiffness()) >= entity.getMin()) {
                                rollStiffness.setBothposition_stiffness_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setBothposition_stiffness_score(null);
                        }
                    } else if (sp.getField_name().equals("osposition")) {
                        if (!StringUtils.isEmpty(rollStiffness.getOsposition())) {
                            if (Math.abs(rollStiffness.getOsposition()) < entity.getMax() && Math.abs(rollStiffness.getOsposition()) >= entity.getMin()) {
                                rollStiffness.setOsposition_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setOsposition_score(null);
                        }
                    } else if (sp.getField_name().equals("dsposition")) {
                        if (!StringUtils.isEmpty(rollStiffness.getDsposition())) {
                            if (Math.abs(rollStiffness.getDsposition()) < entity.getMax() && Math.abs(rollStiffness.getDsposition()) >= entity.getMin()) {
                                rollStiffness.setDsposition_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setDsposition_score(null);
                        }
                    } else if (sp.getField_name().equals("rolling_force")) {
                        if (!StringUtils.isEmpty(rollStiffness.getRolling_force())) {
                            if (Math.abs(rollStiffness.getRolling_force()) < entity.getMax() && Math.abs(rollStiffness.getRolling_force()) >= entity.getMin()) {
                                rollStiffness.setRolling_force_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setRolling_force_score(null);
                        }
                    } else if (sp.getField_name().equals("osrollgap")) {
                        if (!StringUtils.isEmpty(rollStiffness.getOsrollgap())) {
                            if (Math.abs(rollStiffness.getOsrollgap()) < entity.getMax() && Math.abs(rollStiffness.getOsrollgap()) >= entity.getMin()) {
                                rollStiffness.setOsrollgap_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setOsrollgap_score(null);
                        }
                    } else if (sp.getField_name().equals("dsrollgap")) {
                        if (!StringUtils.isEmpty(rollStiffness.getDsrollgap())) {
                            if (Math.abs(rollStiffness.getDsrollgap()) < entity.getMax() && Math.abs(rollStiffness.getDsrollgap()) >= entity.getMin()) {
                                rollStiffness.setDsrollgap_score(entity.getScore());
                            }
                        } else {
                            rollStiffness.setDsrollgap_score(null);
                        }
                    }

                }
                //无 无
                else if (StringUtils.isEmpty(entity.getMin()) && StringUtils.isEmpty(entity.getMax())) {

                }
                //其他
                else {
                    switch (sp.getField_name()) {
                        case "retention":
                            rollStiffness.setRetention_score(0.00);
                            break;
                        case "leveling":
                            rollStiffness.setLeveling_score(0.00);
                            break;
                        case "bothsides_stiffness":
                            rollStiffness.setBothsides_stiffness_score(0.00);
                            break;
                        case "bothposition_stiffness":
                            rollStiffness.setBothposition_stiffness_score(0.00);
                            break;
                        case "osposition":
                            rollStiffness.setOsposition_score(0.00);
                            break;
                        case "dsposition":
                            rollStiffness.setDsposition_score(0.00);
                            break;
                        case "rolling_force":
                            rollStiffness.setRolling_force_score(0.00);
                            break;
                        case "osrollgap":
                            rollStiffness.setOsrollgap_score(0.00);
                            break;
                        case "dsrollgap":
                            rollStiffness.setDsrollgap_score(0.00);
                            break;
                    }
                }
            }
        }

        //基本信息赋值
        rollStiffness.setFrame_noid(frame_noid);
        Double Retention_score = 0.00;
        Double Leveling_score = 0.00;
        Double Bothsides_stiffness_score = 0.00;
        Double Bothposition_stiffness_score = 0.00;
        Double Osposition_score = 0.00;
        Double Dsposition_score = 0.00;
        Double Rolling_force_score = 0.00;
        Double Osrollgap_score = 0.00;
        Double Dsrollgap_score = 0.00;
        if (!StringUtils.isEmpty(rollStiffness.getRetention_score())) {
            Retention_score = rollStiffness.getRetention_score();
        }
        if (!StringUtils.isEmpty(rollStiffness.getLeveling_score())) {
            Leveling_score = rollStiffness.getLeveling_score();
        }
        if (!StringUtils.isEmpty(rollStiffness.getBothsides_stiffness_score())) {
            Bothsides_stiffness_score = rollStiffness.getBothsides_stiffness_score();
        }
        if (!StringUtils.isEmpty(rollStiffness.getBothposition_stiffness_score())) {
            Bothposition_stiffness_score = rollStiffness.getBothposition_stiffness_score();
        }
        if (!StringUtils.isEmpty(rollStiffness.getOsposition_score())) {
            Osposition_score = rollStiffness.getOsposition_score();
        }
        if (!StringUtils.isEmpty(rollStiffness.getDsposition_score())) {
            Dsposition_score = rollStiffness.getDsposition_score();
        }
        if (!StringUtils.isEmpty(rollStiffness.getRolling_force_score())) {
            Rolling_force_score = rollStiffness.getRolling_force_score();
        }
        if (!StringUtils.isEmpty(rollStiffness.getOsrollgap_score())) {
            Osrollgap_score = rollStiffness.getOsrollgap_score();
        }
        if (!StringUtils.isEmpty(rollStiffness.getDsrollgap_score())) {
            Dsrollgap_score = rollStiffness.getDsrollgap_score();
        }

        return rollStiffness;
    }
}
