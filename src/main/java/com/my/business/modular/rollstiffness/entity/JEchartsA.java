package com.my.business.modular.rollstiffness.entity;


import java.util.List;

/**
 * 轧辊多变异分析图形化——支撑辊实体类
 *
 * @author 生成器生成
 * @date 2020-09-08 11:24:47
 */
public class JEchartsA {

    private List<String> catalog;
    private List<xy> series;

    public List<String> getCatalog() {
        return catalog;
    }

    public void setCatalog(List<String> catalog) {
        this.catalog = catalog;
    }

    public List<xy> getSeries() {
        return series;
    }

    public void setSeries(List<xy> series) {
        this.series = series;
    }

    public static class xy {
        String name;
        List<String> xst;
        List<Double> yst;
        List<Double> yst2;
        List<Double> yst3;
        List<Double> yst4;
        List<Double> yst5;
        List<Double> yst6;
        List<Double> yst7;
        List<Double> yst8;
        List<Double> yst9;
        List<Double> yst10;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<String> getXst() {
            return xst;
        }

        public void setXst(List<String> xst) {
            this.xst = xst;
        }

        public List<Double> getYst() {
            return yst;
        }

        public void setYst(List<Double> yst) {
            this.yst = yst;
        }

        public List<Double> getYst2() {
            return yst2;
        }

        public void setYst2(List<Double> yst2) {
            this.yst2 = yst2;
        }

        public List<Double> getYst3() {
            return yst3;
        }

        public void setYst3(List<Double> yst3) {
            this.yst3 = yst3;
        }

        public List<Double> getYst4() {
            return yst4;
        }

        public void setYst4(List<Double> yst4) {
            this.yst4 = yst4;
        }

        public List<Double> getYst5() {
            return yst5;
        }

        public void setYst5(List<Double> yst5) {
            this.yst5 = yst5;
        }

        public List<Double> getYst6() {
            return yst6;
        }

        public void setYst6(List<Double> yst6) {
            this.yst6 = yst6;
        }

        public List<Double> getYst7() {
            return yst7;
        }

        public void setYst7(List<Double> yst7) {
            this.yst7 = yst7;
        }

        public List<Double> getYst8() {
            return yst8;
        }

        public void setYst8(List<Double> yst8) {
            this.yst8 = yst8;
        }

        public List<Double> getYst9() {
            return yst9;
        }

        public void setYst9(List<Double> yst9) {
            this.yst9 = yst9;
        }

        public List<Double> getYst10() {
            return yst10;
        }

        public void setYst10(List<Double> yst10) {
            this.yst10 = yst10;
        }
    }
}