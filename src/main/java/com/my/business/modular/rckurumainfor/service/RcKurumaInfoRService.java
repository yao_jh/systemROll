package com.my.business.modular.rckurumainfor.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.modular.rckurumainfor.entity.RcKurumaInfoR;
import com.my.business.sys.dict.entity.DpdEntity;

import java.util.List;

/**
* 换辊小车表——支撑辊类接口服务类
* @author  生成器生成
* @date 2020-10-10 11:51:26
*/
public interface RcKurumaInfoRService {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRcKurumaInfoR(String data, Long userId, String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRcKurumaInfoROne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRcKurumaInfoRMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataRcKurumaInfoR(String data, Long userId, String sname);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRcKurumaInfoRByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataRcKurumaInfoRByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<RcKurumaInfoR> findDataRcKurumaInfoR();

	/**
	 * 备辊小车现役辊
	 *
	 * @param data 分页参数字符串
	 */
	List<DpdEntity> findlist();
}
