package com.my.business.modular.rckurumainfor.controller;

import java.util.List;

import com.my.business.sys.dict.entity.DpdEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.modular.rckurumainfor.entity.RcKurumaInfoR;
import com.my.business.modular.rckurumainfor.entity.JRcKurumaInfoR;
import com.my.business.modular.rckurumainfor.service.RcKurumaInfoRService;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import com.my.business.sys.common.entity.ResultData;
import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.my.business.util.CodiUtil;
import org.springframework.web.bind.annotation.RequestHeader;


/**
* 换辊小车表——支撑辊类控制器层
* @author  生成器生成
* @date 2020-10-10 11:51:26
*/
@RestController
@RequestMapping("/rcKurumaInfoR")
public class RcKurumaInfoRController {

	@Autowired
	private RcKurumaInfoRService rcKurumaInfoRService;
	
	/**
	 * 添加记录
	 * @param data userId 用户id
     * @param data sname 用户姓名
	 * */
	@CrossOrigin
	@RequestMapping(value={"/insert"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData insertDataRcKurumaInfoR(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return rcKurumaInfoRService.insertDataRcKurumaInfoR(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
	 * 根据主键删除对象
	 * @param data json字符串
	 * */
	@CrossOrigin
	@RequestMapping(value={"/deleteOne"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData deleteDataRcKurumaInfoROne(@RequestBody String data){
		try{
    		JRcKurumaInfoR jrcKurumaInfoR = JSON.parseObject(data,JRcKurumaInfoR.class);
    		return rcKurumaInfoRService.deleteDataRcKurumaInfoROne(jrcKurumaInfoR.getIndocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
    	try{
    		JRcKurumaInfoR jrcKurumaInfoR = JSON.parseObject(data,JRcKurumaInfoR.class);
    		return rcKurumaInfoRService.deleteDataRcKurumaInfoRMany(jrcKurumaInfoR.getStr_indocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
    }
	
	/**
     * 修改记录
     * @param data json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
	@RequestMapping(value={"/update"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData updateDataRcKurumaInfoR(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return rcKurumaInfoRService.updateDataRcKurumaInfoR(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
     * 分页查看记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRcKurumaInfoRByPage(@RequestBody String data) {
        return rcKurumaInfoRService.findDataRcKurumaInfoRByPage(data);
    }
    
    /**
     * 根据主键查询单条记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRcKurumaInfoRByIndocno(@RequestBody String data) {
        return rcKurumaInfoRService.findDataRcKurumaInfoRByIndocno(data);
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	@CrossOrigin
	@RequestMapping(value={"/findAll"}, method=RequestMethod.POST)
	public List<RcKurumaInfoR> findDataRcKurumaInfoR(){
		return rcKurumaInfoRService.findDataRcKurumaInfoR();
	};

	/**
	 * 表格查询接口
	 */
	@CrossOrigin
	@RequestMapping(value = {"/findlist"}, method = RequestMethod.POST)
	@ResponseBody
	public List<DpdEntity> findlist() {
		return rcKurumaInfoRService.findlist();
	}
}
