package com.my.business.modular.rckurumainfor.dao;

import com.my.business.modular.rckurumainfor.entity.RcKurumaInfoR;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 换辊小车表——支撑辊类dao接口
 *
 * @author 生成器生成
 * @date 2020-10-10 11:51:26
 */
@Mapper
public interface RcKurumaInfoRDao {

    /**
     * 添加记录
     *
     * @param rcKurumaInfoR 对象实体
     */
	void insertDataRcKurumaInfoR(RcKurumaInfoR rcKurumaInfoR);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
	void deleteDataRcKurumaInfoROne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
	void deleteDataRcKurumaInfoRMany(String value);

    /**
     * 修改记录
     *
     * @param rcKurumaInfoR 对象实体
     */
	void updateDataRcKurumaInfoR(RcKurumaInfoR rcKurumaInfoR);

    /**
     * 修改记录
     *
     * @param rcKurumaInfoR 对象实体
     */
    void updateDataRcKurumaInfoRByPre(RcKurumaInfoR rcKurumaInfoR);

    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
	List<RcKurumaInfoR> findDataRcKurumaInfoRByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
	Integer findDataRcKurumaInfoRByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
	RcKurumaInfoR findDataRcKurumaInfoRByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
	List<RcKurumaInfoR> findDataRcKurumaInfoR();


    /**
     * 查看所有机架号
     *
     * @return 对象数据集合
     */
    List<RcKurumaInfoR> findDataRcKurumaInfoROnlyStandno();

    /**
     * 查询单条信息
     *
     * @param standno       机架号
     * @param roll_position 上机位置
     * @return 集合
     */
    RcKurumaInfoR findDataRcKurumaInfoRByStandno(@Param("standno") String standno, @Param("roll_position") String roll_position);
}
