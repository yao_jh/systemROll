package com.my.business.modular.rckurumainfor.service.impl;

import com.my.business.modular.rckurumainfo.entity.RcKurumaInfo;
import com.my.business.sys.dict.entity.DpdEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.rckurumainfor.entity.RcKurumaInfoR;
import com.my.business.modular.rckurumainfor.entity.JRcKurumaInfoR;
import com.my.business.modular.rckurumainfor.dao.RcKurumaInfoRDao;
import com.my.business.modular.rckurumainfor.service.RcKurumaInfoRService;
import com.my.business.sys.common.entity.ResultData;
import org.springframework.util.StringUtils;

/**
* 换辊小车表——支撑辊类接口具体实现类
* @author  生成器生成
* @date 2020-10-10 11:51:26
*/
@Service
public class RcKurumaInfoRServiceImpl implements RcKurumaInfoRService {
	
	@Autowired
	private RcKurumaInfoRDao rcKurumaInfoRDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRcKurumaInfoR(String data,Long userId,String sname){
		try{
			JRcKurumaInfoR jrcKurumaInfoR = JSON.parseObject(data,JRcKurumaInfoR.class);
    		RcKurumaInfoR rcKurumaInfoR = jrcKurumaInfoR.getRcKurumaInfoR();
    		CodiUtil.newRecord(userId,sname,rcKurumaInfoR);
            rcKurumaInfoRDao.insertDataRcKurumaInfoR(rcKurumaInfoR);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRcKurumaInfoROne(Long indocno){
		try {
            rcKurumaInfoRDao.deleteDataRcKurumaInfoROne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRcKurumaInfoRMany(String str_id) {
        try {
        	String sql = "delete rc_kuruma_info_r where indocno in(" + str_id +")";
            rcKurumaInfoRDao.deleteDataRcKurumaInfoRMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param data sysUser 对象实体
     */
	public ResultData updateDataRcKurumaInfoR(String data,Long userId,String sname){
		try {
			JRcKurumaInfoR jrcKurumaInfoR = JSON.parseObject(data,JRcKurumaInfoR.class);
    		RcKurumaInfoR rcKurumaInfoR = jrcKurumaInfoR.getRcKurumaInfoR();
        	CodiUtil.editRecord(userId,sname,rcKurumaInfoR);
            rcKurumaInfoRDao.updateDataRcKurumaInfoR(rcKurumaInfoR);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRcKurumaInfoRByPage(String data) {
        try {
        	JRcKurumaInfoR jrcKurumaInfoR = JSON.parseObject(data, JRcKurumaInfoR.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jrcKurumaInfoR.getPageIndex();
        	Integer pageSize = jrcKurumaInfoR.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jrcKurumaInfoR.getCondition()){
    			jsonObject  = JSON.parseObject(jrcKurumaInfoR.getCondition().toString());
    		}
    
    		List<RcKurumaInfoR> list = rcKurumaInfoRDao.findDataRcKurumaInfoRByPage((pageIndex-1)*pageSize, pageSize);
    		Integer count = rcKurumaInfoRDao.findDataRcKurumaInfoRByPageSize();
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRcKurumaInfoRByIndocno(String data) {
        try {
        	JRcKurumaInfoR jrcKurumaInfoR = JSON.parseObject(data, JRcKurumaInfoR.class); 
        	Long indocno = jrcKurumaInfoR.getIndocno();
        	
    		RcKurumaInfoR rcKurumaInfoR = rcKurumaInfoRDao.findDataRcKurumaInfoRByIndocno(indocno);
    		return ResultData.ResultDataSuccess(rcKurumaInfoR);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<RcKurumaInfoR> findDataRcKurumaInfoR(){
		List<RcKurumaInfoR> list = rcKurumaInfoRDao.findDataRcKurumaInfoR();
		return list;
	};

	/**
	 * 查看备辊小车现役备辊
	 */
	public List<DpdEntity> findlist() {
		List<RcKurumaInfoR> list = rcKurumaInfoRDao.findDataRcKurumaInfoROnlyStandno();
		List<DpdEntity> dpd = new ArrayList<>();
		for (RcKurumaInfoR entity:list){
			String top = "TOP";
			String bot = "BOT";
			RcKurumaInfoR rcT = rcKurumaInfoRDao.findDataRcKurumaInfoRByStandno(entity.getStandno(),top);
			RcKurumaInfoR rcB = rcKurumaInfoRDao.findDataRcKurumaInfoRByStandno(entity.getStandno(),bot);
			String standno  = rcT.getRoll_id() + "/" + rcB.getRoll_id();
			if (StringUtils.isEmpty(rcT.getRoll_id()) && StringUtils.isEmpty(rcB.getRoll_id())){
				standno = "";
			}
			DpdEntity d = new DpdEntity();
			d.setKey(entity.getStandno());
			d.setValue(standno);
			dpd.add(d);
		}
		return dpd;
	}
}
