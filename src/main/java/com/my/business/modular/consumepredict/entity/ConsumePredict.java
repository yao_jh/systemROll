package com.my.business.modular.consumepredict.entity;

import java.util.Date;

/**
* FSP磨削预测接口实体类
* @author  生成器生成
* @date 2020-11-25 14:21:58
*/
public class ConsumePredict{
		
	private Long indocno;  //主键
	private Date curtime;  //时间
	private String act_coilid;  //带钢号
	private String roll_no;  //辊号
	private String roll_type;  //轧辊类型
	private String roll_position;  //轧辊位置
	private String frame_no;  //机架号
	private Double mmconsume;  //mm消耗量预测
	private Double moneyconsume;  //费用消耗预测
	private Double mmconskgt;  //kg/t消耗预测
    
    
	public void setIndocno(Long indocno){
	    this.indocno = indocno;
	}
	public Long getIndocno(){
	    return this.indocno;
	}
	public void setCurtime(Date curtime){
	    this.curtime = curtime;
	}
	public Date getCurtime(){
		return this.curtime;
	}
	public void setAct_coilid(String act_coilid){
	    this.act_coilid = act_coilid;
	}
	public String getAct_coilid(){
	    return this.act_coilid;
	}
	public void setRoll_no(String roll_no){
	    this.roll_no = roll_no;
	}
	public String getRoll_no(){
	    return this.roll_no;
	}
	public void setRoll_type(String roll_type){
	    this.roll_type = roll_type;
	}
	public String getRoll_type(){
	    return this.roll_type;
	}
	public void setRoll_position(String roll_position){
	    this.roll_position = roll_position;
	}
	public String getRoll_position(){
	    return this.roll_position;
	}
	public void setFrame_no(String frame_no){
	    this.frame_no = frame_no;
	}
	public String getFrame_no(){
	    return this.frame_no;
	}
	public void setMmconsume(Double mmconsume){
	    this.mmconsume = mmconsume;
	}
	public Double getMmconsume(){
	    return this.mmconsume;
	}
	public void setMoneyconsume(Double moneyconsume){
	    this.moneyconsume = moneyconsume;
	}
	public Double getMoneyconsume(){
	    return this.moneyconsume;
	}
	public void setMmconskgt(Double mmconskgt){
	    this.mmconskgt = mmconskgt;
	}
	public Double getMmconskgt(){
	    return this.mmconskgt;
	}

    
}