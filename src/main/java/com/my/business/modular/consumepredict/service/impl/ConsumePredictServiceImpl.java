package com.my.business.modular.consumepredict.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.consumepredict.entity.ConsumePredict;
import com.my.business.modular.consumepredict.entity.JConsumePredict;
import com.my.business.modular.consumepredict.dao.ConsumePredictDao;
import com.my.business.modular.consumepredict.service.ConsumePredictService;
import com.my.business.sys.common.entity.ResultData;

/**
* FSP磨削预测接口接口具体实现类
* @author  生成器生成
* @date 2020-11-25 14:21:59
*/
@Service
public class ConsumePredictServiceImpl implements ConsumePredictService {
	
	@Autowired
	private ConsumePredictDao consumePredictDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataConsumePredict(String data,Long userId,String sname){
		try{
			JConsumePredict jconsumePredict = JSON.parseObject(data,JConsumePredict.class);
    		ConsumePredict consumePredict = jconsumePredict.getConsumePredict();
            consumePredictDao.insertDataConsumePredict(consumePredict);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataConsumePredictOne(Long indocno){
		try {
            consumePredictDao.deleteDataConsumePredictOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataConsumePredictMany(String str_id) {
        try {
        	String sql = "delete consume_predict where indocno in(" + str_id +")";
            consumePredictDao.deleteDataConsumePredictMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataConsumePredict(String data,Long userId,String sname){
		try {
			JConsumePredict jconsumePredict = JSON.parseObject(data,JConsumePredict.class);
    		ConsumePredict consumePredict = jconsumePredict.getConsumePredict();
            consumePredictDao.updateDataConsumePredict(consumePredict);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataConsumePredictByPage(String data) {
        try {
        	JConsumePredict jconsumePredict = JSON.parseObject(data, JConsumePredict.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jconsumePredict.getPageIndex();
        	Integer pageSize = jconsumePredict.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jconsumePredict.getCondition()){
    			jsonObject  = JSON.parseObject(jconsumePredict.getCondition().toString());
    		}
    
    		List<ConsumePredict> list = consumePredictDao.findDataConsumePredictByPage((pageIndex-1)*pageSize, pageSize);
    		Integer count = consumePredictDao.findDataConsumePredictByPageSize();
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataConsumePredictByIndocno(String data) {
        try {
        	JConsumePredict jconsumePredict = JSON.parseObject(data, JConsumePredict.class); 
        	Long indocno = jconsumePredict.getIndocno();
        	
    		ConsumePredict consumePredict = consumePredictDao.findDataConsumePredictByIndocno(indocno);
    		return ResultData.ResultDataSuccess(consumePredict);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<ConsumePredict> findDataConsumePredict(){
		List<ConsumePredict> list = consumePredictDao.findDataConsumePredict();
		return list;
	};
}
