package com.my.business.modular.consumepredict.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.modular.consumepredict.entity.ConsumePredict;
import java.util.List;

/**
* FSP磨削预测接口接口服务类
* @author  生成器生成
* @date 2020-11-25 14:21:59
*/
public interface ConsumePredictService {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataConsumePredict(String data,Long userId,String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataConsumePredictOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataConsumePredictMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataConsumePredict(String data,Long userId,String sname);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataConsumePredictByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataConsumePredictByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<ConsumePredict> findDataConsumePredict();
}
