package com.my.business.modular.consumepredict.entity;

/**
 * FSP磨削预测接口实体类
 * 
 * @author 生成器生成
 * @date 2020-11-25 14:21:58
 */
public class ConsumePredictYc {

	private String act_coilid; // 卷号
	private Double w_mmconskgt; // 工作辊预测消耗
	private Double w_moneyconsume; // 工作辊预测费用
	private Double r_mmconskgt; // 支撑辊预测消耗
	private Double r_moneyconsume; // 支撑辊预测费用
	
	private String GAPsender;
	private String GAPreceiver;
	
	public String getGAPsender() {
		return GAPsender;
	}

	public void setGAPsender(String gAPsender) {
		GAPsender = gAPsender;
	}

	public String getGAPreceiver() {
		return GAPreceiver;
	}

	public void setGAPreceiver(String gAPreceiver) {
		GAPreceiver = gAPreceiver;
	}

	public String getAct_coilid() {
		return act_coilid;
	}

	public void setAct_coilid(String act_coilid) {
		this.act_coilid = act_coilid;
	}

	public Double getW_mmconskgt() {
		return w_mmconskgt;
	}

	public void setW_mmconskgt(Double w_mmconskgt) {
		this.w_mmconskgt = w_mmconskgt;
	}

	public Double getW_moneyconsume() {
		return w_moneyconsume;
	}

	public void setW_moneyconsume(Double w_moneyconsume) {
		this.w_moneyconsume = w_moneyconsume;
	}

	public Double getR_mmconskgt() {
		return r_mmconskgt;
	}

	public void setR_mmconskgt(Double r_mmconskgt) {
		this.r_mmconskgt = r_mmconskgt;
	}

	public Double getR_moneyconsume() {
		return r_moneyconsume;
	}

	public void setR_moneyconsume(Double r_moneyconsume) {
		this.r_moneyconsume = r_moneyconsume;
	}

}