package com.my.business.modular.consumepredict.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.consumepredict.entity.ConsumePredict;
import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
 * FSP磨削预测接口dao接口
 * @author  生成器生成
 * @date 2020-11-25 14:21:59
 * */
@Mapper
public interface ConsumePredictDao {

	/**
	 * 添加记录
	 * @param consumePredict  对象实体
	 * */
	public void insertDataConsumePredict(ConsumePredict consumePredict);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataConsumePredictOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataConsumePredictMany(String value);
	
	/**
	 * 修改记录
	 * @param consumePredict  对象实体
	 * */
	public void updateDataConsumePredict(ConsumePredict consumePredict);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<ConsumePredict> findDataConsumePredictByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataConsumePredictByPageSize();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public ConsumePredict findDataConsumePredictByIndocno(@Param("indocno") Long indocno);
    
    /***
     * 根据钢卷号询信息
     * @param indocno 用户id
     * @return
     */
    public List<ConsumePredict> findDataConsumePredictByact_coilid(@Param("act_coilid") String act_coilid);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<ConsumePredict> findDataConsumePredict();
	
	/**
	 * 根据时间去重查询出钢卷号集合
	 * @return 对象数据集合
	 * */
	public List<ConsumePredict>  findAct_coilid(@Param("curtime") String curtime);
//	public Map<String,String>  findAct_coilid(@Param("curtime") String curtime);
	
}
