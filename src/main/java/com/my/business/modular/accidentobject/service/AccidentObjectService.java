package com.my.business.modular.accidentobject.service;

import com.my.business.modular.accidentobject.entity.AccidentObject;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 *  生产事故原因管理
 */
public abstract interface AccidentObjectService
{
     ResultData insertDataAccidentObject(String paramString1, Long paramLong, String paramString2);

    ResultData deleteDataAccidentObjectOne(Long paramLong);

    ResultData deleteDataAccidentObjectMany(String paramString);

     ResultData updateDataAccidentObject(String paramString1, Long paramLong, String paramString2);

    ResultData findDataAccidentObjectByPage(String paramString);

     ResultData findDataAccidentObjectByIndocno(String paramString);

     ResultData findDataDpd(String paramString);

     List<AccidentObject> findDataAccidentObject();
}
