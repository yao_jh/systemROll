package com.my.business.modular.accidentobject.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.accidentobject.entity.AccidentObject;
import com.my.business.modular.accidentobject.entity.JAccidentObject;
import com.my.business.modular.accidentobject.service.AccidentObjectService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *  生产事故原因管理
 */
@RestController
@RequestMapping({"/accidentObject"})
public class AccidentObjectController
{

    @Autowired
    private AccidentObjectService accidentObjectService;

    @CrossOrigin
    @RequestMapping(value={"/insert"}, method={RequestMethod.POST})
    @ResponseBody
    public ResultData insertDataAccidentObject(@RequestBody String data, @RequestHeader("loginToken") String loginToken)
    {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return this.accidentObjectService.insertDataAccidentObject(data, userId, CodiUtil.returnLm(sname));
    }

    @CrossOrigin
    @RequestMapping(value={"/deleteOne"}, method={RequestMethod.POST})
    @ResponseBody
    public ResultData deleteDataAccidentObjectOne(@RequestBody String data)
    {
        try
        {
            JAccidentObject jaccidentObject = JSON.parseObject(data, JAccidentObject.class);
            return this.accidentObjectService.deleteDataAccidentObjectOne(jaccidentObject.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    @CrossOrigin
    @RequestMapping(value={"/deleteMany"}, method={RequestMethod.POST})
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data)
    {
        try
        {
            JAccidentObject jaccidentObject = JSON.parseObject(data, JAccidentObject.class);
            return this.accidentObjectService.deleteDataAccidentObjectMany(jaccidentObject.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    @CrossOrigin
    @RequestMapping(value={"/update"}, method={RequestMethod.POST})
    @ResponseBody
    public ResultData updateDataAccidentObject(@RequestBody String data, @RequestHeader("loginToken") String loginToken)
    {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return this.accidentObjectService.updateDataAccidentObject(data, userId, CodiUtil.returnLm(sname));
    }

    @CrossOrigin
    @RequestMapping(value={"/findByPage"}, method={RequestMethod.POST})
    @ResponseBody
    public ResultData findDataAccidentObjectByPage(@RequestBody String data)
    {
        return this.accidentObjectService.findDataAccidentObjectByPage(data);
    }

    @CrossOrigin
    @RequestMapping(value={"/findByIndocno"}, method={RequestMethod.POST})
    @ResponseBody
    public ResultData findDataAccidentObjectByIndocno(@RequestBody String data)
    {
        return this.accidentObjectService.findDataAccidentObjectByIndocno(data);
    }

    @CrossOrigin
    @RequestMapping(value={"/findAll"}, method={RequestMethod.POST})
    public List<AccidentObject> findDataAccidentObject()
    {
        return this.accidentObjectService.findDataAccidentObject();
    }

    @CrossOrigin
    @RequestMapping(value={"/findDpd"}, method={RequestMethod.POST})
    @ResponseBody
    public ResultData findDpd(@RequestBody String data)
    {
        return this.accidentObjectService.findDataDpd(data);
    }
}