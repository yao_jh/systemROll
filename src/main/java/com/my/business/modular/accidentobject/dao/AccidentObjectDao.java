package com.my.business.modular.accidentobject.dao;
/**
 *  生产事故原因管理
 */
import com.my.business.modular.accidentobject.entity.AccidentObject;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public abstract interface AccidentObjectDao
{
    public abstract void insertDataAccidentObject(AccidentObject paramAccidentObject);

    public abstract void deleteDataAccidentObjectOne(@Param("indocno") Long paramLong);

    public abstract void deleteDataAccidentObjectMany(String paramString);

    public abstract void updateDataAccidentObject(AccidentObject paramAccidentObject);

    public abstract List<AccidentObject> findDataAccidentObjectByPage(@Param("pageIndex") Integer paramInteger1, @Param("pageSize") Integer paramInteger2, @Param("accident_type_id") String paramString1, @Param("accident_reason") String paramString2);

    public abstract Integer findDataAccidentObjectByPageSize(@Param("accident_type_id") String paramString1, @Param("accident_reason") String paramString2);

    public abstract AccidentObject findDataAccidentObjectByIndocno(@Param("indocno") Long paramLong);

    public abstract List<AccidentObject> findDataAccidentObject();
}
