package com.my.business.modular.accidentobject.entity;
/**
 *  生产事故原因管理
 */
import com.my.business.sys.common.entity.JCommon;

public class JAccidentObject extends JCommon
{
    private Integer pageIndex;
    private Integer pageSize;
    private Object condition;
    private AccidentObject accidentObject;
    private Long indocno;
    private String str_indocno;
    private String order_no;
    private String nodeid;
    private String perform_no;

    public Integer getPageIndex()
    {
        return this.pageIndex;
    }
    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }
    public Integer getPageSize() {
        return this.pageSize;
    }
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
    public Object getCondition() {
        return this.condition;
    }
    public void setCondition(Object condition) {
        this.condition = condition;
    }
    public AccidentObject getAccidentObject() {
        return this.accidentObject;
    }
    public void setAccidentObject(AccidentObject accidentObject) {
        this.accidentObject = accidentObject;
    }
    public Long getIndocno() {
        return this.indocno;
    }
    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }
    public String getStr_indocno() {
        return this.str_indocno;
    }
    public void setStr_indocno(String str_indocno) {
        this.str_indocno = str_indocno;
    }

    public String getOrder_no() {
        return this.order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getNodeid() {
        return this.nodeid;
    }

    public void setNodeid(String nodeid) {
        this.nodeid = nodeid;
    }

    public String getPerform_no() {
        return this.perform_no;
    }

    public void setPerform_no(String perform_no) {
        this.perform_no = perform_no;
    }
}
