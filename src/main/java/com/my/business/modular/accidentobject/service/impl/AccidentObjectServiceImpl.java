package com.my.business.modular.accidentobject.service.impl;
/**
 *  生产事故原因管理
 */

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.accidentobject.dao.AccidentObjectDao;
import com.my.business.modular.accidentobject.entity.AccidentObject;
import com.my.business.modular.accidentobject.entity.JAccidentObject;
import com.my.business.modular.accidentobject.service.AccidentObjectService;
import com.my.business.sys.common.dao.CommonDao;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class AccidentObjectServiceImpl
        implements AccidentObjectService
{

    @Autowired
    private AccidentObjectDao accidentObjectDao;

    @Autowired
    private CommonDao commonDao;

    public ResultData insertDataAccidentObject(String data, Long userId, String sname)
    {
        try
        {
            JAccidentObject jaccidentObject = JSON.parseObject(data, JAccidentObject.class);
            AccidentObject accidentObject = jaccidentObject.getAccidentObject();
            CodiUtil.newRecord(userId, sname, accidentObject);
            this.accidentObjectDao.insertDataAccidentObject(accidentObject);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    public ResultData deleteDataAccidentObjectOne(Long indocno)
    {
        try
        {
            this.accidentObjectDao.deleteDataAccidentObjectOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    public ResultData deleteDataAccidentObjectMany(String str_id)
    {
        try
        {
            String sql = "delete from accident_object where indocno in(" + str_id + ")";
            this.accidentObjectDao.deleteDataAccidentObjectMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    public ResultData updateDataAccidentObject(String data, Long userId, String sname)
    {
        try
        {
            JAccidentObject jaccidentObject = JSON.parseObject(data, JAccidentObject.class);
            AccidentObject accidentObject = jaccidentObject.getAccidentObject();
            CodiUtil.editRecord(userId, sname, accidentObject);
            this.accidentObjectDao.updateDataAccidentObject(accidentObject);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    public ResultData findDataAccidentObjectByPage(String data)
    {
        try
        {
            JAccidentObject jaccidentObject = JSON.parseObject(data, JAccidentObject.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jaccidentObject.getPageIndex();
            Integer pageSize = jaccidentObject.getPageSize();

            if ((pageIndex == null) || (pageSize == null)) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (jaccidentObject.getCondition() != null) {
                jsonObject = JSON.parseObject(jaccidentObject.getCondition().toString());
            }

            String accident_type_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("accident_type_id"))) {
                accident_type_id = jsonObject.get("accident_type_id").toString();
            }

            String accident_reason = null;
            if (!StringUtils.isEmpty(jsonObject.get("accident_reason"))) {
                accident_reason = jsonObject.get("accident_reason").toString();
            }

            List list = this.accidentObjectDao.findDataAccidentObjectByPage(Integer.valueOf((pageIndex.intValue() - 1) * pageSize.intValue()), pageSize, accident_type_id, accident_reason);
            Integer count = this.accidentObjectDao.findDataAccidentObjectByPageSize(accident_type_id, accident_reason);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public ResultData findDataAccidentObjectByIndocno(String data)
    {
        try
        {
            JAccidentObject jaccidentObject = JSON.parseObject(data, JAccidentObject.class);
            Long indocno = jaccidentObject.getIndocno();

            AccidentObject accidentObject = this.accidentObjectDao.findDataAccidentObjectByIndocno(indocno);
            return ResultData.ResultDataSuccess(accidentObject);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public List<AccidentObject> findDataAccidentObject()
    {
        List list = this.accidentObjectDao.findDataAccidentObject();
        return list;
    }

    public ResultData findDataDpd(String data)
    {
        try {
            AccidentObject accidentObject = JSON.parseObject(data, AccidentObject.class);
            String sql = "select a.indocno as 'key',a.accident_reason as 'value' from accident_object a where a.accident_type_id = " + accidentObject.getAccident_type_id();
            List list = this.commonDao.findMap(sql);
            return ResultData.ResultDataSuccess(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }
}