package com.my.business.modular.accidentobject.entity;
/**
 *  生产事故原因管理
 */
import com.my.business.sys.common.entity.BaseEntity;
public class AccidentObject extends BaseEntity
{
    private Long indocno;
    private Long accident_type_id;
    private String accident_type;
    private String accident_reason;

    public void setIndocno(Long indocno)
    {
        this.indocno = indocno;
    }
    public Long getIndocno() {
        return this.indocno;
    }
    public void setAccident_type_id(Long accident_type_id) {
        this.accident_type_id = accident_type_id;
    }
    public Long getAccident_type_id() {
        return this.accident_type_id;
    }
    public void setAccident_type(String accident_type) {
        this.accident_type = accident_type;
    }
    public String getAccident_type() {
        return this.accident_type;
    }
    public void setAccident_reason(String accident_reason) {
        this.accident_reason = accident_reason;
    }
    public String getAccident_reason() {
        return this.accident_reason;
    }
}
