package com.my.business.modular.productionaccidents.dao;

import com.my.business.modular.productionaccidents.entity.ProductionAccidents;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 生产事故管理dao接口
 *
 * @author 生成器生成
 * @date 2020-08-12 11:32:47
 */
@Mapper
public interface ProductionAccidentsDao {

    /**
     * 添加记录
     *
     * @param productionAccidents 对象实体
     */
    void insertDataProductionAccidents(ProductionAccidents productionAccidents);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataProductionAccidentsOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataProductionAccidentsMany(String value);

    /**
     * 修改记录
     *
     * @param productionAccidents 对象实体
     */
    void updateDataProductionAccidents(ProductionAccidents productionAccidents);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<ProductionAccidents> findDataProductionAccidentsByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("eq_typeid") Long eq_typeid, @Param("accident_typeid") Long accident_typeid, @Param("accident_reasonid") Long accident_reasonid, @Param("dstart") String dstart, @Param("dend") String dend, @Param("accident_analysis") String accident_analysis, @Param("solving_process") String solving_process);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataProductionAccidentsByPageSize(@Param("eq_typeid") Long eq_typeid, @Param("accident_typeid") Long accident_typeid, @Param("accident_reasonid") Long accident_reasonid, @Param("dstart") String dstart, @Param("dend") String dend, @Param("accident_analysis") String accident_analysis, @Param("solving_process") String solving_process);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    ProductionAccidents findDataProductionAccidentsByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<ProductionAccidents> findDataProductionAccidents();

    /**
     * 修改文件名称
     *
     * @param filename 文件名称
     * @param indocno  记录主键
     */
    void updateDataFileName(@Param("filename") String filename, @Param("indocno") Long indocno);


}
