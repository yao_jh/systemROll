package com.my.business.modular.productionaccidents.service;

import com.my.business.modular.productionaccidents.entity.ProductionAccidents;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 生产事故管理接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-12 11:32:47
 */
public interface ProductionAccidentsService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataProductionAccidents(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataProductionAccidentsOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataProductionAccidentsMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataProductionAccidents(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataProductionAccidentsByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataProductionAccidentsByIndocno(String data);

    /**
     * 查看记录
     */
    List<ProductionAccidents> findDataProductionAccidents();

    /**
     * 修改记录文件名称
     *
     * @param filename 文件名称
     * @param indocno  主键
     */
    ResultData updateDataFileName(String filename, Long indocno);
}
