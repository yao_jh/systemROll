package com.my.business.modular.productionaccidents.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.productionaccidents.entity.JProductionAccidents;
import com.my.business.modular.productionaccidents.entity.ProductionAccidents;
import com.my.business.modular.productionaccidents.service.ProductionAccidentsService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.util.ExcelUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;


/**
 * 生产事故管理控制器层
 *
 * @author 生成器生成
 * @date 2020-08-12 11:32:47
 */
@RestController
@RequestMapping("/productionAccidents")
public class ProductionAccidentsController {

    @Value("${upload_url}")
    private String file_url;  //从配置文件中获取文件路径

    @Autowired
    private ProductionAccidentsService productionAccidentsService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataProductionAccidents(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return productionAccidentsService.insertDataProductionAccidents(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataProductionAccidentsOne(@RequestBody String data) {
        try {
            JProductionAccidents jproductionAccidents = JSON.parseObject(data, JProductionAccidents.class);
            return productionAccidentsService.deleteDataProductionAccidentsOne(jproductionAccidents.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JProductionAccidents jproductionAccidents = JSON.parseObject(data, JProductionAccidents.class);
            return productionAccidentsService.deleteDataProductionAccidentsMany(jproductionAccidents.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataProductionAccidents(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return productionAccidentsService.updateDataProductionAccidents(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataProductionAccidentsByPage(@RequestBody String data) {
        return productionAccidentsService.findDataProductionAccidentsByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataProductionAccidentsByIndocno(@RequestBody String data) {
        return productionAccidentsService.findDataProductionAccidentsByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<ProductionAccidents> findDataProductionAccidents() {
        return productionAccidentsService.findDataProductionAccidents();
    }

    /***
     * 单文件上传
     * @param file
     * @return
     */
    @CrossOrigin
    @PostMapping("singleupload")
    @ResponseBody
    public ResultData singleupload(Long indocno, MultipartFile file) {
        if (null != file) {
            String filename = file.getOriginalFilename();// 文件原名称
            String path = file_url + "/" + filename;
            File localFile = new File(path);

            try {
                file.transferTo(localFile);
                productionAccidentsService.updateDataFileName(filename, indocno);
                List A = ExcelUtils.readExcel(localFile);
                return ResultData.ResultDataSuccessSelf("上传成功", A);
            } catch (Exception e) {
                e.printStackTrace();
                return ResultData.ResultDataFaultSelf("上传失败", null);
            }
        } else {
            return ResultData.ResultDataFaultSelf("上传失败,文件为空", null);
        }
    }
}
