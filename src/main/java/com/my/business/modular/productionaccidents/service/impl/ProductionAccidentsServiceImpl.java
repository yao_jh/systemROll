package com.my.business.modular.productionaccidents.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.productionaccidents.dao.ProductionAccidentsDao;
import com.my.business.modular.productionaccidents.entity.JProductionAccidents;
import com.my.business.modular.productionaccidents.entity.ProductionAccidents;
import com.my.business.modular.productionaccidents.service.ProductionAccidentsService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 生产事故管理接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-12 11:32:47
 */
@Service
public class ProductionAccidentsServiceImpl implements ProductionAccidentsService {

    @Autowired
    private ProductionAccidentsDao productionAccidentsDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataProductionAccidents(String data, Long userId, String sname) {
        try {
            JProductionAccidents jproductionAccidents = JSON.parseObject(data, JProductionAccidents.class);
            ProductionAccidents productionAccidents = jproductionAccidents.getProductionAccidents();
            CodiUtil.newRecord(userId, sname, productionAccidents);
            productionAccidentsDao.insertDataProductionAccidents(productionAccidents);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataProductionAccidentsOne(Long indocno) {
        try {
            productionAccidentsDao.deleteDataProductionAccidentsOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataProductionAccidentsMany(String str_id) {
        try {
            String sql = "delete production_accidents where indocno in(" + str_id + ")";
            productionAccidentsDao.deleteDataProductionAccidentsMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataProductionAccidents(String data, Long userId, String sname) {
        try {
            JProductionAccidents jproductionAccidents = JSON.parseObject(data, JProductionAccidents.class);
            ProductionAccidents productionAccidents = jproductionAccidents.getProductionAccidents();
            CodiUtil.editRecord(userId, sname, productionAccidents);
            productionAccidentsDao.updateDataProductionAccidents(productionAccidents);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataProductionAccidentsByPage(String data) {
        try {
            JProductionAccidents jproductionAccidents = JSON.parseObject(data, JProductionAccidents.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jproductionAccidents.getPageIndex();
            Integer pageSize = jproductionAccidents.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jproductionAccidents.getCondition()) {
                jsonObject = JSON.parseObject(jproductionAccidents.getCondition().toString());
            }

            Long eq_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("eq_typeid"))) {
                eq_typeid = Long.valueOf(jsonObject.get("eq_typeid").toString());
            }

            Long accident_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("accident_typeid"))) {
                accident_typeid = Long.valueOf(jsonObject.get("accident_typeid").toString());
            }

            Long accident_reasonid = null;
            if (!StringUtils.isEmpty(jsonObject.get("accident_reasonid"))) {
                accident_reasonid = Long.valueOf(jsonObject.get("accident_reasonid").toString());
            }

            String dstart = null;
            if (!StringUtils.isEmpty(jsonObject.get("dstart"))) {
                dstart = jsonObject.get("dstart").toString();
            }

            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            String accident_analysis = null;
            if (!StringUtils.isEmpty(jsonObject.get("accident_analysis"))) {
                accident_analysis = jsonObject.get("accident_analysis").toString();
            }

            String solving_process = null;
            if (!StringUtils.isEmpty(jsonObject.get("solving_process"))) {
                solving_process = jsonObject.get("solving_process").toString();
            }

            List<ProductionAccidents> list = productionAccidentsDao.findDataProductionAccidentsByPage((pageIndex - 1)*pageSize, pageSize, eq_typeid, accident_typeid, accident_reasonid, dstart, dend, accident_analysis, solving_process);
            Integer count = productionAccidentsDao.findDataProductionAccidentsByPageSize(eq_typeid, accident_typeid, accident_reasonid, dstart, dend, accident_analysis, solving_process);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataProductionAccidentsByIndocno(String data) {
        try {
            JProductionAccidents jproductionAccidents = JSON.parseObject(data, JProductionAccidents.class);
            Long indocno = jproductionAccidents.getIndocno();

            ProductionAccidents productionAccidents = productionAccidentsDao.findDataProductionAccidentsByIndocno(indocno);
            return ResultData.ResultDataSuccess(productionAccidents);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<ProductionAccidents> findDataProductionAccidents() {
        List<ProductionAccidents> list = productionAccidentsDao.findDataProductionAccidents();
        return list;
    }

    /**
     * 修改记录文件名称
     *
     * @param filename 文件名称
     * @param indocno  主键
     */
    public ResultData updateDataFileName(String filename, Long indocno) {
        try {
            productionAccidentsDao.updateDataFileName(filename, indocno);
            return ResultData.ResultDataSuccessSelf("上传成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("上传失败,失败信息为" + e.getMessage(), null);
        }
    }
}
