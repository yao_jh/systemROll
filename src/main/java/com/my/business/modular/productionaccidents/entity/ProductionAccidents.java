package com.my.business.modular.productionaccidents.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 生产事故管理实体类
 *
 * @author 生成器生成
 * @date 2020-08-12 11:32:46
 */
public class ProductionAccidents extends BaseEntity {

    private Long indocno;  //主键
    private Long eq_typeid;  //设备类型id
    private String eq_type;  //设备类型
    private Long accident_typeid;  //事故类型id
    private String accident_type;  //事故类型
    private String accident_time;  //事故时间
    private Long accident_reasonid;  //事故原因id
    private String accident_reason;  //事故原因
    private Long ibz;  //班组id
    private String bz;  //班组
    private Long duty_userid;  //当班责任人id
    private String duty_user;  //当班责任人
    private Long connection_userid;  //事故联络人id
    private String connection_user;  //事故联络人
    private String roll_no;  //辊号
    private String chock_no;  //轴承座号
    private String grinding_time;  //磨削时间定位
    private String uplinetime;  //上机时间定位
    private String lowlinetime;  //下机时间定位
    private String pairedtime;  //装配时间定位
    private String accident_report;  //事故报告
    private String accident_analysis;  //事故分析
    private String solving_process;  //解决过程

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getEq_typeid() {
        return this.eq_typeid;
    }

    public void setEq_typeid(Long eq_typeid) {
        this.eq_typeid = eq_typeid;
    }

    public String getEq_type() {
        return this.eq_type;
    }

    public void setEq_type(String eq_type) {
        this.eq_type = eq_type;
    }

    public Long getAccident_typeid() {
        return this.accident_typeid;
    }

    public void setAccident_typeid(Long accident_typeid) {
        this.accident_typeid = accident_typeid;
    }

    public String getAccident_type() {
        return this.accident_type;
    }

    public void setAccident_type(String accident_type) {
        this.accident_type = accident_type;
    }

    public String getAccident_time() {
        return this.accident_time;
    }

    public void setAccident_time(String accident_time) {
        this.accident_time = accident_time;
    }

    public Long getAccident_reasonid() {
        return this.accident_reasonid;
    }

    public void setAccident_reasonid(Long accident_reasonid) {
        this.accident_reasonid = accident_reasonid;
    }

    public String getAccident_reason() {
        return this.accident_reason;
    }

    public void setAccident_reason(String accident_reason) {
        this.accident_reason = accident_reason;
    }

    public Long getIbz() {
        return this.ibz;
    }

    public void setIbz(Long ibz) {
        this.ibz = ibz;
    }

    public String getBz() {
        return this.bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public Long getDuty_userid() {
        return this.duty_userid;
    }

    public void setDuty_userid(Long duty_userid) {
        this.duty_userid = duty_userid;
    }

    public String getDuty_user() {
        return this.duty_user;
    }

    public void setDuty_user(String duty_user) {
        this.duty_user = duty_user;
    }

    public Long getConnection_userid() {
        return this.connection_userid;
    }

    public void setConnection_userid(Long connection_userid) {
        this.connection_userid = connection_userid;
    }

    public String getConnection_user() {
        return this.connection_user;
    }

    public void setConnection_user(String connection_user) {
        this.connection_user = connection_user;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getChock_no() {
        return this.chock_no;
    }

    public void setChock_no(String chock_no) {
        this.chock_no = chock_no;
    }

    public String getGrinding_time() {
        return this.grinding_time;
    }

    public void setGrinding_time(String grinding_time) {
        this.grinding_time = grinding_time;
    }

    public String getUplinetime() {
        return this.uplinetime;
    }

    public void setUplinetime(String uplinetime) {
        this.uplinetime = uplinetime;
    }

    public String getLowlinetime() {
        return this.lowlinetime;
    }

    public void setLowlinetime(String lowlinetime) {
        this.lowlinetime = lowlinetime;
    }

    public String getPairedtime() {
        return this.pairedtime;
    }

    public void setPairedtime(String pairedtime) {
        this.pairedtime = pairedtime;
    }

    public String getAccident_report() {
        return this.accident_report;
    }

    public void setAccident_report(String accident_report) {
        this.accident_report = accident_report;
    }

    public String getAccident_analysis() {
        return this.accident_analysis;
    }

    public void setAccident_analysis(String accident_analysis) {
        this.accident_analysis = accident_analysis;
    }

    public String getSolving_process() {
        return this.solving_process;
    }

    public void setSolving_process(String solving_process) {
        this.solving_process = solving_process;
    }


}