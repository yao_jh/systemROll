package com.my.business.modular.rolldevicescheckpart.service;

import com.my.business.modular.rolldevicescheckpart.entity.RollDevicescheckpart;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 点检部位表接口服务类
 *
 * @author 生成器生成
 * @date 2020-07-22 13:45:31
 */
public interface RollDevicesCheckPartService {

    /**
     * 查看一条数据信息
     *
     * @param deviceId 设备id
     */
    ResultData findDataRollDevicescheckpartByDeviceId(String deviceId);

    ResultData findDataRollDevicescheckpartByIndoc(String data);

    List<RollDevicescheckpart> findDataRollDevicescheckpartByDeviceIdAndDevicePartName(List<Long> deviceIds,
                                                                                       String devicePartName);
}
