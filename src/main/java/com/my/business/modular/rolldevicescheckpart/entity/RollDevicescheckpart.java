package com.my.business.modular.rolldevicescheckpart.entity;

import com.my.business.sys.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 点检部位表实体类
 *
 * @author 生成器生成
 * @date 2020-07-22 13:45:29
 */
@Data
@EqualsAndHashCode
public class RollDevicescheckpart extends BaseEntity {

    private Long indocno;  //点检部位主键
    private Long device_id;  //设备主键
    private String device_name;    //设备名称
    private String device_part_name;  //点检部位名称
    private Long priority;  //优先级
    private String snote;  //备注
    private int check_type;//对应的点检操作工角色：0电气/1机械

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getDevice_id() {
        return device_id;
    }

    public void setDevice_id(Long device_id) {
        this.device_id = device_id;
    }

    public String getDevice_name() {
        return device_name;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }

    public String getDevice_part_name() {
        return device_part_name;
    }

    public void setDevice_part_name(String device_part_name) {
        this.device_part_name = device_part_name;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public String getSnote() {
        return snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public int getCheck_type() {
        return check_type;
    }

    public void setCheck_type(int check_type) {
        this.check_type = check_type;
    }
}