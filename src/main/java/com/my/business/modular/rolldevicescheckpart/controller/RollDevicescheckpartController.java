package com.my.business.modular.rolldevicescheckpart.controller;

import com.my.business.modular.rolldevicescheckpart.service.RollDevicesCheckPartService;
import com.my.business.sys.common.entity.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 点检部位表控制器层
 *
 * @author 生成器生成
 * @date 2020-07-22 13:45:32
 */
@RestController
@RequestMapping("/rollDevicescheckpart")
public class RollDevicescheckpartController {

    @Autowired
    private RollDevicesCheckPartService rollDevicescheckpartService;

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByDeviceId"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDevicescheckpartByIndocno(@RequestBody String data) {
        return rollDevicescheckpartService.findDataRollDevicescheckpartByDeviceId(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDevicescheckdictByIndocno(@RequestBody String data) {
        return rollDevicescheckpartService.findDataRollDevicescheckpartByIndoc(data);
    }

}
