package com.my.business.modular.rolldevicescheckpart.dao;

import com.my.business.modular.rolldevicescheckpart.entity.RollDevicescheckpart;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 点检部位表dao接口
 *
 * @author 生成器生成
 * @date 2020-07-22 13:45:31
 */
@Mapper
public interface RollDevicescheckpartDao {

    /***
     * 根据主键查询信息
     * @param deviceId 设备id
     * @return
     */
    List<RollDevicescheckpart> findDataRollDevicescheckpartByDeviceId(@Param("deviceId") Long deviceId);

    /**
     * @param indocno
     * @return
     */
    RollDevicescheckpart findDataRollDevicescheckPartByIndocno(@Param("indocno") Long indocno);

    /**
     * 根据设备名称和设备点检部位名称双重查询
     *
     * @param deviceIds      设备id
     * @param devicePartName 点检部位名称
     */
    List<RollDevicescheckpart> findDataRollDevicescheckpartByDeviceIdAndDevicePartName(@Param("deviceIds") List<Long> deviceIds,
                                                                                       @Param("device_part_Name") String devicePartName);

}
