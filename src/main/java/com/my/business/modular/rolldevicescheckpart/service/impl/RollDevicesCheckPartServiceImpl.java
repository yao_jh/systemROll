package com.my.business.modular.rolldevicescheckpart.service.impl;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rolldevicescheckpart.dao.RollDevicescheckpartDao;
import com.my.business.modular.rolldevicescheckpart.entity.JRollDevicescheckpart;
import com.my.business.modular.rolldevicescheckpart.entity.RollDevicescheckpart;
import com.my.business.modular.rolldevicescheckpart.service.RollDevicesCheckPartService;
import com.my.business.sys.common.entity.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 点检部位表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-07-22 13:45:32
 */
@Service
public class RollDevicesCheckPartServiceImpl implements RollDevicesCheckPartService {

    @Autowired
    private RollDevicescheckpartDao rollDevicescheckpartDao;

    /**
     * 根据主键查询单条记录
     *
     * @param data 对象类型参数
     */
    public ResultData findDataRollDevicescheckpartByDeviceId(String data) {
        try {
            JRollDevicescheckpart jRollDevicescheckpart = JSON.parseObject(data, JRollDevicescheckpart.class);
            Long indocno = jRollDevicescheckpart.getIndocno();
            List<RollDevicescheckpart> rollDevicescheckpart = rollDevicescheckpartDao.findDataRollDevicescheckpartByDeviceId(indocno);
            return ResultData.ResultDataSuccess(rollDevicescheckpart);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    @Override
    public ResultData findDataRollDevicescheckpartByIndoc(String data) {
        try {
            JRollDevicescheckpart jRollDevicescheckpart = JSON.parseObject(data, JRollDevicescheckpart.class);
            Long indocno = jRollDevicescheckpart.getIndocno();
            RollDevicescheckpart rollDevicescheckpart = rollDevicescheckpartDao.findDataRollDevicescheckPartByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollDevicescheckpart);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }


    @Override
    public List<RollDevicescheckpart> findDataRollDevicescheckpartByDeviceIdAndDevicePartName(List<Long> deviceIds, String devicePartName) {
        List<RollDevicescheckpart> rollDevicescheckpart = rollDevicescheckpartDao.findDataRollDevicescheckpartByDeviceIdAndDevicePartName(deviceIds, devicePartName);
        return rollDevicescheckpart;
    }

}
