package com.my.business.modular.action.actionflow.dao;

import com.my.business.modular.action.actionflow.entity.ActionFlow;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 执行流程表dao接口
 *
 * @author 生成器生成
 * @date 2020-05-22 13:37:16
 */
@Mapper
public interface ActionFlowDao {

    /**
     * 添加记录
     *
     * @param actionFlow 对象实体
     */
    void insertDataActionFlow(ActionFlow actionFlow);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataActionFlowOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataActionFlowMany(String value);

    /**
     * 修改记录
     *
     * @param actionFlow 对象实体
     */
    void updateDataActionFlow(ActionFlow actionFlow);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<ActionFlow> findDataActionFlowByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataActionFlowByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    ActionFlow findDataActionFlowByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<ActionFlow> findDataActionFlow();

}
