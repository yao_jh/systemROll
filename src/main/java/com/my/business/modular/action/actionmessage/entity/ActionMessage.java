package com.my.business.modular.action.actionmessage.entity;

import java.util.Date;

/**
 * 执行信息表实体类
 *
 * @author 生成器生成
 * @date 2020-06-03 08:59:39
 */
public class ActionMessage {

    private Long indocno;  //主键
    private String work_id;  //流程编码
    private String step_no;  //步骤编码
    private Long act_userid;  //操作人id
    private String act_user;  //操作人姓名
    private String common;  //操作事项
    private Date createtime; // 创建时间
    private String createname; // 创建人姓名
    private Long createid; // 创建人ID
    private Long istate;  //状态

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getWork_id() {
        return this.work_id;
    }

    public void setWork_id(String work_id) {
        this.work_id = work_id;
    }

    public String getStep_no() {
        return this.step_no;
    }

    public void setStep_no(String step_no) {
        this.step_no = step_no;
    }

    public Long getAct_userid() {
        return this.act_userid;
    }

    public void setAct_userid(Long act_userid) {
        this.act_userid = act_userid;
    }

    public String getAct_user() {
        return this.act_user;
    }

    public void setAct_user(String act_user) {
        this.act_user = act_user;
    }

    public String getCommon() {
        return this.common;
    }

    public void setCommon(String common) {
        this.common = common;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getCreatename() {
        return createname;
    }

    public void setCreatename(String createname) {
        this.createname = createname;
    }

    public Long getCreateid() {
        return createid;
    }

    public void setCreateid(Long createid) {
        this.createid = createid;
    }

    public Long getIstate() {
        return istate;
    }

    public void setIstate(Long istate) {
        this.istate = istate;
    }

}