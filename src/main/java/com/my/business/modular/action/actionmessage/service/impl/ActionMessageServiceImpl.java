package com.my.business.modular.action.actionmessage.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.action.actionmessage.dao.ActionMessageDao;
import com.my.business.modular.action.actionmessage.entity.ActionMessage;
import com.my.business.modular.action.actionmessage.entity.JActionMessage;
import com.my.business.modular.action.actionmessage.service.ActionMessageService;
import com.my.business.sys.common.entity.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 执行信息表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-06-03 08:59:40
 */
@Service
public class ActionMessageServiceImpl implements ActionMessageService {

    @Autowired
    private ActionMessageDao actionMessageDao;

    /***
     * 创建新纪录时给共用字段赋值（用于只有创建3个字段）
     * @param userId
     * @param sname
     * @param entity
     */
    public static void newRecordCreate(Long userId, String sname, ActionMessage entity) {
        entity.setCreateid(userId);
        entity.setCreatename(sname);
        entity.setCreatetime(new Date());
    }

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataActionMessage(String data, Long userId, String sname) {
        try {
            JActionMessage jactionMessage = JSON.parseObject(data, JActionMessage.class);
            ActionMessage actionMessage = jactionMessage.getActionMessage();
            newRecordCreate(userId, sname, actionMessage);
            actionMessageDao.insertDataActionMessage(actionMessage);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataActionMessageOne(Long indocno) {
        try {
            actionMessageDao.deleteDataActionMessageOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataActionMessageMany(String str_id) {
        try {
            String sql = "delete action_message where indocno in(" + str_id + ")";
            actionMessageDao.deleteDataActionMessageMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData updateDataActionMessage(String data, Long userId, String sname) {
        try {
            JActionMessage jactionMessage = JSON.parseObject(data, JActionMessage.class);
            ActionMessage actionMessage = jactionMessage.getActionMessage();
            actionMessageDao.updateDataActionMessage(actionMessage);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataActionMessageByPage(String data) {
        try {
            JActionMessage jactionMessage = JSON.parseObject(data, JActionMessage.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jactionMessage.getPageIndex();
            Integer pageSize = jactionMessage.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jactionMessage.getCondition()) {
                jsonObject = JSON.parseObject(jactionMessage.getCondition().toString());
            }

            List<ActionMessage> list = actionMessageDao.findDataActionMessageByPage(pageIndex, pageSize);
            Integer count = actionMessageDao.findDataActionMessageByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataActionMessageByIndocno(String data) {
        try {
            JActionMessage jactionMessage = JSON.parseObject(data, JActionMessage.class);
            Long indocno = jactionMessage.getIndocno();

            ActionMessage actionMessage = actionMessageDao.findDataActionMessageByIndocno(indocno);
            return ResultData.ResultDataSuccess(actionMessage);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<ActionMessage> findDataActionMessage() {
        List<ActionMessage> list = actionMessageDao.findDataActionMessage();
        return list;
    }

}
