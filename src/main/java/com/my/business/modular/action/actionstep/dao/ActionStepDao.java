package com.my.business.modular.action.actionstep.dao;

import com.my.business.modular.action.actionstep.entity.ActionStep;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 执行步骤表dao接口
 *
 * @author 生成器生成
 * @date 2020-05-22 13:34:52
 */
@Mapper
public interface ActionStepDao {

    /**
     * 添加记录
     *
     * @param actionStep 对象实体
     */
    void insertDataActionStep(ActionStep actionStep);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataActionStepOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataActionStepMany(String value);

    /**
     * 修改记录
     *
     * @param actionStep 对象实体
     */
    void updateDataActionStep(ActionStep actionStep);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<ActionStep> findDataActionStepByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataActionStepByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno id
     * @return
     */
    ActionStep findDataActionStepByIndocno(@Param("indocno") Long indocno);

    /***
     * 根据编码查询信息
     * @param step_no 步骤编码
     * @return
     */
    ActionStep findDataActionStepByNo(@Param("step_no") String step_no);

    /***
     * 根据父节点查询信息
     * @param iparent 父级编码
     * @return
     */
    List<ActionStep> findDataActionStepByIparent(@Param("iparent") String iparent);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<ActionStep> findDataActionStep();

}
