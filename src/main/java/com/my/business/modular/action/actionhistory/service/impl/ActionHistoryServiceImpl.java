package com.my.business.modular.action.actionhistory.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.action.actionhistory.dao.ActionHistoryDao;
import com.my.business.modular.action.actionhistory.entity.ActionHistory;
import com.my.business.modular.action.actionhistory.entity.JActionHistory;
import com.my.business.modular.action.actionhistory.service.ActionHistoryService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 历史表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:38:58
 */
@Service
public class ActionHistoryServiceImpl implements ActionHistoryService {

    @Autowired
    private ActionHistoryDao actionHistoryDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataActionHistory(String data, Long userId, String sname) {
        try {
            JActionHistory jactionHistory = JSON.parseObject(data, JActionHistory.class);
            ActionHistory actionHistory = jactionHistory.getActionHistory();
            CodiUtil.newRecord(userId, sname, actionHistory);
            actionHistoryDao.insertDataActionHistory(actionHistory);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataActionHistoryOne(Long indocno) {
        try {
            actionHistoryDao.deleteDataActionHistoryOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataActionHistoryMany(String str_id) {
        try {
            String sql = "delete action_history where indocno in(" + str_id + ")";
            actionHistoryDao.deleteDataActionHistoryMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataActionHistory(String data, Long userId, String sname) {
        try {
            JActionHistory jactionHistory = JSON.parseObject(data, JActionHistory.class);
            ActionHistory actionHistory = jactionHistory.getActionHistory();
            CodiUtil.editRecord(userId, sname, actionHistory);
            actionHistoryDao.updateDataActionHistory(actionHistory);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataActionHistoryByPage(String data) {
        try {
            JActionHistory jactionHistory = JSON.parseObject(data, JActionHistory.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jactionHistory.getPageIndex();
            Integer pageSize = jactionHistory.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jactionHistory.getCondition()) {
                jsonObject = JSON.parseObject(jactionHistory.getCondition().toString());
            }

            List<ActionHistory> list = actionHistoryDao.findDataActionHistoryByPage(pageIndex, pageSize);
            Integer count = actionHistoryDao.findDataActionHistoryByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataActionHistoryByIndocno(String data) {
        try {
            JActionHistory jactionHistory = JSON.parseObject(data, JActionHistory.class);
            Long indocno = jactionHistory.getIndocno();

            ActionHistory actionHistory = actionHistoryDao.findDataActionHistoryByIndocno(indocno);
            return ResultData.ResultDataSuccess(actionHistory);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<ActionHistory> findDataActionHistory() {
        List<ActionHistory> list = actionHistoryDao.findDataActionHistory();
        return list;
    }

}
