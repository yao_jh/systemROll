package com.my.business.modular.action.actionflow.service;

import com.my.business.modular.action.actionflow.entity.ActionFlow;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 执行流程表接口服务类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:37:16
 */
public interface ActionFlowService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataActionFlow(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataActionFlowOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataActionFlowMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataActionFlow(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataActionFlowByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataActionFlowByIndocno(String data);

    /**
     * 查看记录
     */
    List<ActionFlow> findDataActionFlow();
}
