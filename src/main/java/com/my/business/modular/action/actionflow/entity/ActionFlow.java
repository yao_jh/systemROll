package com.my.business.modular.action.actionflow.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 执行流程表实体类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:37:15
 */
public class ActionFlow extends BaseEntity {

    private Long indocno;  //主键
    private Long ilinkno;  //外键
    private String work_name;  //工作流名称
    private String work_no;  //工作流编码
    private String snote;  //说明
    private String iparent;   //父级节点编码
    private Long ifirst;   //是否开始标识

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getIlinkno() {
        return this.ilinkno;
    }

    public void setIlinkno(Long ilinkno) {
        this.ilinkno = ilinkno;
    }

    public String getWork_name() {
        return this.work_name;
    }

    public void setWork_name(String work_name) {
        this.work_name = work_name;
    }

    public String getWork_no() {
        return this.work_no;
    }

    public void setWork_no(String work_no) {
        this.work_no = work_no;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public String getIparent() {
        return iparent;
    }

    public void setIparent(String iparent) {
        this.iparent = iparent;
    }

    public Long getIfirst() {
        return ifirst;
    }

    public void setIfirst(Long ifirst) {
        this.ifirst = ifirst;
    }
}