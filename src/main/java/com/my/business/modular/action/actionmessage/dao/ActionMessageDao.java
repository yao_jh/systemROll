package com.my.business.modular.action.actionmessage.dao;

import com.my.business.modular.action.actionmessage.entity.ActionMessage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 执行信息表dao接口
 *
 * @author 生成器生成
 * @date 2020-06-03 08:59:40
 */
@Mapper
public interface ActionMessageDao {

    /**
     * 添加记录
     *
     * @param actionMessage 对象实体
     */
    void insertDataActionMessage(ActionMessage actionMessage);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataActionMessageOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataActionMessageMany(String value);

    /**
     * 修改记录
     *
     * @param actionMessage 对象实体
     */
    void updateDataActionMessage(ActionMessage actionMessage);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<ActionMessage> findDataActionMessageByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataActionMessageByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    ActionMessage findDataActionMessageByIndocno(@Param("indocno") Long indocno);

    /***
     * 根据步骤编码查询信息
     * @param step_no 步骤编码
     * @return
     */
    ActionMessage findDataActionMessageByStepNo(@Param("step_no") String step_no);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<ActionMessage> findDataActionMessage();

}
