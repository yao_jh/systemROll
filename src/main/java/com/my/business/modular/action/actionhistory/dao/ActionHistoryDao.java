package com.my.business.modular.action.actionhistory.dao;

import com.my.business.modular.action.actionhistory.entity.ActionHistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 历史表dao接口
 *
 * @author 生成器生成
 * @date 2020-05-22 13:38:58
 */
@Mapper
public interface ActionHistoryDao {

    /**
     * 添加记录
     *
     * @param actionHistory 对象实体
     */
    void insertDataActionHistory(ActionHistory actionHistory);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataActionHistoryOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataActionHistoryMany(String value);

    /**
     * 修改记录
     *
     * @param actionHistory 对象实体
     */
    void updateDataActionHistory(ActionHistory actionHistory);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<ActionHistory> findDataActionHistoryByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataActionHistoryByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    ActionHistory findDataActionHistoryByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<ActionHistory> findDataActionHistory();

}
