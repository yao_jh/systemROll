package com.my.business.modular.action.actionhistory.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 历史表实体类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:38:57
 */
public class ActionHistory extends BaseEntity {

    private Long indocno;  //
    private String ilinkno;  //流程编码
    private String step_name;  //步骤名称
    private String step_no;  //步骤编码
    private String act_user;  //办理人姓名
    private Long act_userid;  //办理人id
    private String act_role;  //办理角色名称
    private Long act_roleid;  //办理角色id
    private String action_common;  //步骤类型（普通步骤还是分支选择器）
    private String snote;  //说明

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getIlinkno() {
        return this.ilinkno;
    }

    public void setIlinkno(String ilinkno) {
        this.ilinkno = ilinkno;
    }

    public String getStep_name() {
        return this.step_name;
    }

    public void setStep_name(String step_name) {
        this.step_name = step_name;
    }

    public String getStep_no() {
        return this.step_no;
    }

    public void setStep_no(String step_no) {
        this.step_no = step_no;
    }

    public String getAct_user() {
        return this.act_user;
    }

    public void setAct_user(String act_user) {
        this.act_user = act_user;
    }

    public Long getAct_userid() {
        return this.act_userid;
    }

    public void setAct_userid(Long act_userid) {
        this.act_userid = act_userid;
    }

    public String getAct_role() {
        return this.act_role;
    }

    public void setAct_role(String act_role) {
        this.act_role = act_role;
    }

    public Long getAct_roleid() {
        return this.act_roleid;
    }

    public void setAct_roleid(Long act_roleid) {
        this.act_roleid = act_roleid;
    }

    public String getAction_common() {
        return this.action_common;
    }

    public void setAction_common(String action_common) {
        this.action_common = action_common;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }


}