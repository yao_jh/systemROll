package com.my.business.modular.action.actionstep.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 执行步骤表实体类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:34:52
 */
public class ActionStep extends BaseEntity {

    private Long indocno;  //主键
    private Long ilinkno;  //流程id
    private String step_name;  //步骤名称
    private String step_no;  //步骤编码
    private String act_user;  //办理人姓名
    private Long act_userid;  //办理人id
    private String act_role;  //办理角色名称
    private Long act_roleid;  //办理角色id
    private Long step_type;  //步骤类型（普通步骤还是分支选择器）
    private Long step_state;  //步骤状态（完成还是没完成）
    private String step_common;  //执行情况（需要做的事情）
    private String snote;  //说明
    private String iparent;  //父节点
    private Long ifirst;    //是否顶级节点

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getIlinkno() {
        return this.ilinkno;
    }

    public void setIlinkno(Long ilinkno) {
        this.ilinkno = ilinkno;
    }

    public String getStep_name() {
        return this.step_name;
    }

    public void setStep_name(String step_name) {
        this.step_name = step_name;
    }

    public String getStep_no() {
        return this.step_no;
    }

    public void setStep_no(String step_no) {
        this.step_no = step_no;
    }

    public String getAct_user() {
        return this.act_user;
    }

    public void setAct_user(String act_user) {
        this.act_user = act_user;
    }

    public Long getAct_userid() {
        return this.act_userid;
    }

    public void setAct_userid(Long act_userid) {
        this.act_userid = act_userid;
    }

    public String getAct_role() {
        return this.act_role;
    }

    public void setAct_role(String act_role) {
        this.act_role = act_role;
    }

    public Long getAct_roleid() {
        return this.act_roleid;
    }

    public void setAct_roleid(Long act_roleid) {
        this.act_roleid = act_roleid;
    }

    public Long getStep_type() {
        return this.step_type;
    }

    public void setStep_type(Long step_type) {
        this.step_type = step_type;
    }

    public Long getStep_state() {
        return this.step_state;
    }

    public void setStep_state(Long step_state) {
        this.step_state = step_state;
    }

    public String getStep_common() {
        return this.step_common;
    }

    public void setStep_common(String step_common) {
        this.step_common = step_common;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public String getIparent() {
        return iparent;
    }

    public void setIparent(String iparent) {
        this.iparent = iparent;
    }

    public Long getIfirst() {
        return ifirst;
    }

    public void setIfirst(Long ifirst) {
        this.ifirst = ifirst;
    }
}