package com.my.business.modular.action.actionstep.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.action.actionstep.dao.ActionStepDao;
import com.my.business.modular.action.actionstep.entity.ActionStep;
import com.my.business.modular.action.actionstep.entity.JActionStep;
import com.my.business.modular.action.actionstep.service.ActionStepService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 执行步骤表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:34:52
 */
@Service
public class ActionStepServiceImpl implements ActionStepService {

    @Autowired
    private ActionStepDao actionStepDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataActionStep(String data, Long userId, String sname) {
        try {
            JActionStep jactionStep = JSON.parseObject(data, JActionStep.class);
            ActionStep actionStep = jactionStep.getActionStep();
            CodiUtil.newRecord(userId, sname, actionStep);
            actionStepDao.insertDataActionStep(actionStep);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataActionStepOne(Long indocno) {
        try {
            actionStepDao.deleteDataActionStepOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataActionStepMany(String str_id) {
        try {
            String sql = "delete action_step where indocno in(" + str_id + ")";
            actionStepDao.deleteDataActionStepMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataActionStep(String data, Long userId, String sname) {
        try {
            JActionStep jactionStep = JSON.parseObject(data, JActionStep.class);
            ActionStep actionStep = jactionStep.getActionStep();
            CodiUtil.editRecord(userId, sname, actionStep);
            actionStepDao.updateDataActionStep(actionStep);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataActionStepByPage(String data) {
        try {
            JActionStep jactionStep = JSON.parseObject(data, JActionStep.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jactionStep.getPageIndex();
            Integer pageSize = jactionStep.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jactionStep.getCondition()) {
                jsonObject = JSON.parseObject(jactionStep.getCondition().toString());
            }

            List<ActionStep> list = actionStepDao.findDataActionStepByPage(pageIndex, pageSize);
            Integer count = actionStepDao.findDataActionStepByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataActionStepByIndocno(String data) {
        try {
            JActionStep jactionStep = JSON.parseObject(data, JActionStep.class);
            Long indocno = jactionStep.getIndocno();

            ActionStep actionStep = actionStepDao.findDataActionStepByIndocno(indocno);
            return ResultData.ResultDataSuccess(actionStep);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<ActionStep> findDataActionStep() {
        List<ActionStep> list = actionStepDao.findDataActionStep();
        return list;
    }

}
