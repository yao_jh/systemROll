package com.my.business.modular.action.actionflow.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.action.actionflow.dao.ActionFlowDao;
import com.my.business.modular.action.actionflow.entity.ActionFlow;
import com.my.business.modular.action.actionflow.entity.JActionFlow;
import com.my.business.modular.action.actionflow.service.ActionFlowService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 执行流程表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-05-22 13:37:16
 */
@Service
public class ActionFlowServiceImpl implements ActionFlowService {

    @Autowired
    private ActionFlowDao actionFlowDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataActionFlow(String data, Long userId, String sname) {
        try {
            JActionFlow jactionFlow = JSON.parseObject(data, JActionFlow.class);
            ActionFlow actionFlow = jactionFlow.getActionFlow();
            CodiUtil.newRecord(userId, sname, actionFlow);
            actionFlowDao.insertDataActionFlow(actionFlow);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataActionFlowOne(Long indocno) {
        try {
            actionFlowDao.deleteDataActionFlowOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataActionFlowMany(String str_id) {
        try {
            String sql = "delete action_flow where indocno in(" + str_id + ")";
            actionFlowDao.deleteDataActionFlowMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataActionFlow(String data, Long userId, String sname) {
        try {
            JActionFlow jactionFlow = JSON.parseObject(data, JActionFlow.class);
            ActionFlow actionFlow = jactionFlow.getActionFlow();
            CodiUtil.editRecord(userId, sname, actionFlow);
            actionFlowDao.updateDataActionFlow(actionFlow);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataActionFlowByPage(String data) {
        try {
            JActionFlow jactionFlow = JSON.parseObject(data, JActionFlow.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jactionFlow.getPageIndex();
            Integer pageSize = jactionFlow.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jactionFlow.getCondition()) {
                jsonObject = JSON.parseObject(jactionFlow.getCondition().toString());
            }

            List<ActionFlow> list = actionFlowDao.findDataActionFlowByPage(pageIndex, pageSize);
            Integer count = actionFlowDao.findDataActionFlowByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataActionFlowByIndocno(String data) {
        try {
            JActionFlow jactionFlow = JSON.parseObject(data, JActionFlow.class);
            Long indocno = jactionFlow.getIndocno();

            ActionFlow actionFlow = actionFlowDao.findDataActionFlowByIndocno(indocno);
            return ResultData.ResultDataSuccess(actionFlow);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<ActionFlow> findDataActionFlow() {
        List<ActionFlow> list = actionFlowDao.findDataActionFlow();
        return list;
    }

}
