package com.my.business.modular.chockoil.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轴承座注油记录实体类
 *
 * @author 生成器生成
 * @date 2020-09-23 11:32:31
 */
public class ChockOil extends BaseEntity {

    private Long indocno;  //主键
    private String roll_no;  //辊号
    private Long framerangeid;  //机架范围id
    private String framerange;  //机架范围
    private String oil_time;  //注油时间
    private String odiname;  //操作人名称
    private Long odiid;  //操作人id
    private Long ibz;  //操作班组id
    private String sbz;  //操作班组
    private String snote;  //备注

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public Long getFramerangeid() {
        return framerangeid;
    }

    public void setFramerangeid(Long framerangeid) {
        this.framerangeid = framerangeid;
    }

    public String getFramerange() {
        return framerange;
    }

    public void setFramerange(String framerange) {
        this.framerange = framerange;
    }

    public String getOil_time() {
        return oil_time;
    }

    public void setOil_time(String oil_time) {
        this.oil_time = oil_time;
    }

    public String getOdiname() {
        return odiname;
    }

    public void setOdiname(String odiname) {
        this.odiname = odiname;
    }

    public Long getOdiid() {
        return odiid;
    }

    public void setOdiid(Long odiid) {
        this.odiid = odiid;
    }

    public Long getIbz() {
        return ibz;
    }

    public void setIbz(Long ibz) {
        this.ibz = ibz;
    }

    public String getSbz() {
        return sbz;
    }

    public void setSbz(String sbz) {
        this.sbz = sbz;
    }

    public String getSnote() {
        return snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }
}