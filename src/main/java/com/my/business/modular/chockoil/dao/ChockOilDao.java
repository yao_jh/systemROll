package com.my.business.modular.chockoil.dao;

import com.my.business.modular.chockoil.entity.ChockOil;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轴承座注油记录dao接口
 *
 * @author 生成器生成
 * @date 2020-09-23 11:32:31
 */
@Mapper
public interface ChockOilDao {

    /**
     * 添加记录
     *
     * @param chockOil 对象实体
     */
    void insertDataChockOil(ChockOil chockOil);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataChockOilOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataChockOilMany(String value);

    /**
     * 修改记录
     *
     * @param chockOil 对象实体
     */
    void updateDataChockOil(ChockOil chockOil);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<ChockOil> findDataChockOilByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("roll_no") String roll_no,@Param("framerangeid") Long framerangeid,@Param("dbegin") String dbegin, @Param("dend") String dend,@Param("ibz") Long ibz);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataChockOilByPageSize(@Param("roll_no") String roll_no,@Param("framerangeid") Long framerangeid,@Param("dbegin") String dbegin, @Param("dend") String dend,@Param("ibz") Long ibz);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    ChockOil findDataChockOilByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<ChockOil> findDataChockOil();

}
