package com.my.business.modular.chockoil.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.chockoil.dao.ChockOilDao;
import com.my.business.modular.chockoil.entity.ChockOil;
import com.my.business.modular.chockoil.entity.JChockOil;
import com.my.business.modular.chockoil.service.ChockOilService;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 轴承座注油记录接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-23 11:32:31
 */
@Service
public class ChockOilServiceImpl implements ChockOilService {

    @Autowired
    private ChockOilDao chockOilDao;
    @Autowired
    private RollInformationDao rollInformationDao;
    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataChockOil(String data, Long userId, String sname) {
        try {
            JChockOil jchockOil = JSON.parseObject(data, JChockOil.class);
            ChockOil chockOil = jchockOil.getChockOil();
            chockOil.setOdiid(userId);
            chockOil.setOdiname(sname);
            CodiUtil.newRecord(userId, sname, chockOil);
            chockOilDao.insertDataChockOil(chockOil);
            rollInformationDao.updateOil(chockOil.getRoll_no());
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataChockOilOne(Long indocno) {
        try {
            chockOilDao.deleteDataChockOilOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataChockOilMany(String str_id) {
        try {
            String sql = "delete chock_oil where indocno in(" + str_id + ")";
            chockOilDao.deleteDataChockOilMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataChockOil(String data, Long userId, String sname) {
        try {
            JChockOil jchockOil = JSON.parseObject(data, JChockOil.class);
            ChockOil chockOil = jchockOil.getChockOil();
            CodiUtil.editRecord(userId, sname, chockOil);
            chockOilDao.updateDataChockOil(chockOil);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataChockOilByPage(String data) {
        try {
            JChockOil jchockOil = JSON.parseObject(data, JChockOil.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jchockOil.getPageIndex();
            Integer pageSize = jchockOil.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jchockOil.getCondition()) {
                jsonObject = JSON.parseObject(jchockOil.getCondition().toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

            Long framerangeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("framerangeid"))) {
                framerangeid = Long.valueOf(jsonObject.get("framerangeid").toString());
            }

            //开始时间
            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            //结束时间
            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            Long ibz = null;
            if (!StringUtils.isEmpty(jsonObject.get("ibz"))) {
                ibz = Long.valueOf(jsonObject.get("ibz").toString());
            }

            List<ChockOil> list = chockOilDao.findDataChockOilByPage((pageIndex - 1) * pageSize, pageSize, roll_no, framerangeid, dbegin, dend, ibz);
            Integer count = chockOilDao.findDataChockOilByPageSize(roll_no, framerangeid, dbegin, dend, ibz);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataChockOilByIndocno(String data) {
        try {
            JChockOil jchockOil = JSON.parseObject(data, JChockOil.class);
            Long indocno = jchockOil.getIndocno();

            ChockOil chockOil = chockOilDao.findDataChockOilByIndocno(indocno);
            return ResultData.ResultDataSuccess(chockOil);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<ChockOil> findDataChockOil() {
        List<ChockOil> list = chockOilDao.findDataChockOil();
        return list;
    }


}
