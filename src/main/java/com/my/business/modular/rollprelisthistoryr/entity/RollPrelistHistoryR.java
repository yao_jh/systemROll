package com.my.business.modular.rollprelisthistoryr.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 备辊操作历史R表实体类
 *
 * @author 生成器生成
 * @date 2020-11-21 12:20:03
 */
public class RollPrelistHistoryR extends BaseEntity {

    private Long indocno;  //主键
    private Long frame_noid;  //机架id
    private String frame_no;  //机架
    private String roll_no;  //辊号
    private Long idotype;  //操作状态id
    private String sdotype;  //操作状态
    private String reason;  //驳回原因
    private String pretime;  //备辊时间
    private String roll_type;  //轧辊类型

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getFrame_noid() {
        return this.frame_noid;
    }

    public void setFrame_noid(Long frame_noid) {
        this.frame_noid = frame_noid;
    }

    public String getFrame_no() {
        return this.frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public Long getIdotype() {
        return this.idotype;
    }

    public void setIdotype(Long idotype) {
        this.idotype = idotype;
    }

    public String getSdotype() {
        return this.sdotype;
    }

    public void setSdotype(String sdotype) {
        this.sdotype = sdotype;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getPretime() {
        return this.pretime;
    }

    public void setPretime(String pretime) {
        this.pretime = pretime;
    }

    public String getRoll_type() {
        return roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }
}