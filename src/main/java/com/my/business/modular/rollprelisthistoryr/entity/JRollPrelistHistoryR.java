package com.my.business.modular.rollprelisthistoryr.entity;

import com.my.business.sys.common.entity.JCommon;

/**
 * 备辊操作历史R表json的实体类
 *
 * @author 生成器生成
 * @date 2020-11-21 12:20:03
 */
public class JRollPrelistHistoryR extends JCommon {

    private Integer pageIndex; // 第几页
    private Integer pageSize; // 每页多少数据
    private Object condition; // 查询条件
    private RollPrelistHistoryR rollPrelistHistoryR;   //对应模块的实体类
    private Long indocno;
    private String str_indocno;
    private Long ifcm;  //是否重磨

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Object getCondition() {
        return condition;
    }

    public void setCondition(Object condition) {
        this.condition = condition;
    }

    public RollPrelistHistoryR getRollPrelistHistoryR() {
        return rollPrelistHistoryR;
    }

    public void setRollPrelistHistoryR(RollPrelistHistoryR rollPrelistHistoryR) {
        this.rollPrelistHistoryR = rollPrelistHistoryR;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getStr_indocno() {
        return str_indocno;
    }

    public void setStr_indocno(String str_indocno) {
        this.str_indocno = str_indocno;
    }

    public Long getIfcm() {
        return ifcm;
    }

    public void setIfcm(Long ifcm) {
        this.ifcm = ifcm;
    }
}