package com.my.business.modular.rollprelisthistoryr.dao;

import com.my.business.modular.rollprelisthistoryr.entity.RollPrelistHistoryR;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 备辊操作历史R表dao接口
 *
 * @author 生成器生成
 * @date 2020-11-21 10:40:53
 */
@Mapper
public interface RollPrelistHistoryRDao {

    /**
     * 添加记录
     *
     * @param rollPrelistHistoryR 对象实体
     */
    void insertDataRollPrelistHistoryR(RollPrelistHistoryR rollPrelistHistoryR);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollPrelistHistoryROne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollPrelistHistoryRMany(String value);

    /**
     * 修改记录
     *
     * @param rollPrelistHistoryR 对象实体
     */
    void updateDataRollPrelistHistoryR(RollPrelistHistoryR rollPrelistHistoryR);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollPrelistHistoryR> findDataRollPrelistHistoryRByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("roll_no") String roll_no);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollPrelistHistoryRByPageSize(@Param("roll_no") String roll_no);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollPrelistHistoryR findDataRollPrelistHistoryRByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollPrelistHistoryR> findDataRollPrelistHistoryR();

    /**
     * 查询当前最大主键
     *
     * @return 主键
     */
    Long findIndocnoMax();

    /**
     * 查询同一个机架最新一条备辊
     *
     * @param frame_noid 机架号
     * @return
     */
    RollPrelistHistoryR findNew(@Param("frame_noid") Long frame_noid,@Param("sdotype") String sdotype);

    /**
     * 放在备辊车上时，状态改成-1
     *
     * @param roll_no 辊号
     */
    void updateidotype(@Param("roll_no") String roll_no);
}
