package com.my.business.modular.rollprelisthistoryr.service;

import com.my.business.modular.rollprelisthistoryr.entity.RollPrelistHistoryR;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 备辊操作历史R表接口服务类
 *
 * @author 生成器生成
 * @date 2020-11-21 10:40:53
 */
public interface RollPrelistHistoryRService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollPrelistHistoryR(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollPrelistHistoryROne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollPrelistHistoryRMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollPrelistHistoryR(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPrelistHistoryRByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPrelistHistoryRByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollPrelistHistoryR> findDataRollPrelistHistoryR();

    /**
     * 查找最新一条备辊信息
     *
     * @param data
     * @return
     */
    ResultData findNew(String data);
}
