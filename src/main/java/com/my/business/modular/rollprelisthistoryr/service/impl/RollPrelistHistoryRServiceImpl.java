package com.my.business.modular.rollprelisthistoryr.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rckurumainfo.dao.RcKurumaInfoDao;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollpaired.dao.RollPairedDao;
import com.my.business.modular.rollprelisthistoryr.dao.RollPrelistHistoryRDao;
import com.my.business.modular.rollprelisthistoryr.entity.JRollPrelistHistoryR;
import com.my.business.modular.rollprelisthistoryr.entity.RollPrelistHistoryR;
import com.my.business.modular.rollprelisthistoryr.service.RollPrelistHistoryRService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 备辊操作历史R表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-11-21 10:40:53
 */
@Service
public class RollPrelistHistoryRServiceImpl implements RollPrelistHistoryRService {

    @Autowired
    private RollPrelistHistoryRDao rollPrelistHistoryRDao;
    @Autowired
    private RollInformationDao rollInformationDao;
    @Autowired
    private RcKurumaInfoDao rcKurumaInfoDao;
    @Autowired
    private RollPairedDao rollPairedDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollPrelistHistoryR(String data, Long userId, String sname) {
        try {
            JRollPrelistHistoryR jrollPrelistHistoryR = JSON.parseObject(data, JRollPrelistHistoryR.class);
            RollPrelistHistoryR rollPrelistHistoryR = jrollPrelistHistoryR.getRollPrelistHistoryR();
            CodiUtil.newRecord(userId, sname, rollPrelistHistoryR);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            rollPrelistHistoryR.setPretime(df.format(new Date()));
            rollPrelistHistoryRDao.insertDataRollPrelistHistoryR(rollPrelistHistoryR);
            Long indocnoOld = rollPrelistHistoryRDao.findIndocnoMax();
            return ResultData.ResultDataSuccessSelf("添加数据成功", indocnoOld);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollPrelistHistoryROne(Long indocno) {
        try {
            rollPrelistHistoryRDao.deleteDataRollPrelistHistoryROne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollPrelistHistoryRMany(String str_id) {
        try {
            String sql = "delete roll_prelist_history_r where indocno in(" + str_id + ")";
            rollPrelistHistoryRDao.deleteDataRollPrelistHistoryRMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollPrelistHistoryR(String data, Long userId, String sname) {
        try {
            JRollPrelistHistoryR jrollPrelistHistoryR = JSON.parseObject(data, JRollPrelistHistoryR.class);
            RollPrelistHistoryR rollPrelistHistoryR = jrollPrelistHistoryR.getRollPrelistHistoryR();
            CodiUtil.editRecord(userId, sname, rollPrelistHistoryR);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            rollPrelistHistoryR.setPretime(df.format(new Date()));
            rollPrelistHistoryRDao.updateDataRollPrelistHistoryR(rollPrelistHistoryR);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPrelistHistoryRByPage(String data) {
        try {
            JRollPrelistHistoryR jrollPrelistHistoryR = JSON.parseObject(data, JRollPrelistHistoryR.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollPrelistHistoryR.getPageIndex();
            Integer pageSize = jrollPrelistHistoryR.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollPrelistHistoryR.getCondition()) {
                jsonObject = JSON.parseObject(jrollPrelistHistoryR.getCondition().toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

            List<RollPrelistHistoryR> list = rollPrelistHistoryRDao.findDataRollPrelistHistoryRByPage((pageIndex - 1) * pageSize, pageSize,roll_no);
            Integer count = rollPrelistHistoryRDao.findDataRollPrelistHistoryRByPageSize(roll_no);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPrelistHistoryRByIndocno(String data) {
        try {
            JRollPrelistHistoryR jrollPrelistHistoryR = JSON.parseObject(data, JRollPrelistHistoryR.class);
            Long indocno = jrollPrelistHistoryR.getIndocno();

            RollPrelistHistoryR rollPrelistHistoryR = rollPrelistHistoryRDao.findDataRollPrelistHistoryRByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollPrelistHistoryR);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollPrelistHistoryR> findDataRollPrelistHistoryR() {
        List<RollPrelistHistoryR> list = rollPrelistHistoryRDao.findDataRollPrelistHistoryR();
        return list;
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findNew(String data) {
        try {
            JRollPrelistHistoryR jrollPrelistHistoryR = JSON.parseObject(data, JRollPrelistHistoryR.class);
            JSONObject jsonObject = null;

            if (null != jrollPrelistHistoryR.getCondition()) {
                jsonObject = JSON.parseObject(jrollPrelistHistoryR.getCondition().toString());
            }

            //机架号id
            Long frame_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
                frame_noid = Long.valueOf(jsonObject.get("frame_noid").toString());
            }

            String sdotype = null;
            if (!StringUtils.isEmpty(jsonObject.get("sdotype"))) {
                sdotype = jsonObject.get("sdotype").toString();
            }

            RollPrelistHistoryR r = rollPrelistHistoryRDao.findNew(frame_noid,sdotype);
            return ResultData.ResultDataSuccess(r);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }
}
