package com.my.business.modular.dictionary.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.dictionary.dao.DictionaryDao;
import com.my.business.modular.dictionary.entity.DicEntity;
import com.my.business.modular.dictionary.entity.Dictionary;
import com.my.business.modular.dictionary.entity.JDictionary;
import com.my.business.modular.dictionary.service.DictionaryService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.dict.entity.DpdEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据字典接口具体实现类
 *
 * @author 生成器生成
 * @date 2019-11-06 21:21:19
 */
@Service
public class DictionaryServiceImpl implements DictionaryService {

    @Autowired
    private DictionaryDao dictionaryDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataDictionary(String data, Long userId, String sname) {
        try {
            JDictionary jdictionary = JSON.parseObject(data, JDictionary.class);
            Dictionary dictionary = jdictionary.getDictionary();
            if (!StringUtils.isEmpty(dictionary.getIndocno())) {
                dictionaryDao.updateDataDictionary(dictionary);
            } else  {
                dictionaryDao.insertDataDictionary(dictionary);
            }
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataDictionaryOne(Long indocno) {
        try {
            dictionaryDao.deleteDataDictionaryOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataDictionaryMany(String str_id) {
        try {
            String sql = "delete dictionary where indocno in(" + str_id + ")";
            dictionaryDao.deleteDataDictionaryMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataDictionary(String data, Long userId, String sname) {
        try {
            JDictionary jdictionary = JSON.parseObject(data, JDictionary.class);
            Dictionary dictionary = jdictionary.getDictionary();
//        	CodiUtil.editRecord(userId,sname,dictionary);
            dictionaryDao.updateDataDictionary(dictionary);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataDictionaryByPage(String data) {
        try {
            JDictionary jdictionary = JSON.parseObject(data, JDictionary.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jdictionary.getPageIndex();
            Integer pageSize = jdictionary.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jdictionary.getCondition()) {
                jsonObject = JSON.parseObject(jdictionary.getCondition().toString());
            }

            List<Dictionary> list = dictionaryDao.findDataDictionaryByPage(pageIndex, pageSize);
            Integer count = dictionaryDao.findDataDictionaryByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataDictionaryByIndocno(String data) {
        try {
            JDictionary jdictionary = JSON.parseObject(data, JDictionary.class);
            Long indocno = jdictionary.getIndocno();

            Dictionary dictionary = dictionaryDao.findDataDictionaryByIndocno(indocno);
            return ResultData.ResultDataSuccess(dictionary);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public ResultData findDataDictionary() {
        try {
            List<Dictionary> last = new ArrayList<>();
            List<Dictionary> list = dictionaryDao.findDataDictionary(1L,null);
            for (Dictionary entity : list){//循环1级节点
                List<Dictionary> listN = dictionaryDao.findDataDictionary(2L,entity.getIndocno());
                for (Dictionary ent : listN){//循环2级节点
                    List<Dictionary> listL = dictionaryDao.findDataDictionary(3L,ent.getIndocno());
                    ent.setDetail(listL);
                }
                entity.setDetail(listN);
                last.add(entity);
            }

            return ResultData.ResultDataSuccess(last);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 数据字典通用查询，下拉模式
     *
     * @param data 数据字典编码
     * @return
     */
    public ResultData findMapV(String data, Long v) {
        try {
            DicEntity entity = JSON.parseObject(data, DicEntity.class);
            String key = null;
            if (v == 1L) {
                key = "v1";
            } else {
                key = "v2";
            }
            String dicno = entity.getDicno();
            Long level = entity.getLevel();
            String sql = "select a." + key + " as 'key',a.sname as 'value' from dictionary a where a.ilevel = 3 and a.dicno like '" + dicno + "%' order by " +
                    " FIELD(sname,'F1','F2','F3','F4','F5','F6','F7','F8','R1','R2','S1','S2','R1E','R2E','F1E') asc,a.indocno asc, indocno asc";

            if (!StringUtils.isEmpty(level)) {
                sql = "select a." + key + " as 'key',a.sname as 'value' from dictionary a where a.ilevel = " + level + " and a.dicno like '" + dicno + "%' order by indocno asc";
            }

            List<DpdEntity> list = dictionaryDao.findMap(sql);
            return ResultData.ResultDataSuccessSelf("获取成功", list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("获取失败" + e.getMessage(), null);
        }
    }

    /**
     * 查看节点下包含的子表记录数量
     * @return list 对象集合返回
     * */
    public ResultData findDataTreeNum(String data){
        try {
            JDictionary jdictionary = JSON.parseObject(data, JDictionary.class);
            Long indocno = jdictionary.getIndocno();

            Integer count = 1;
//			Integer count = equipInfoDao.findDataEquipInfoEchartsNum(indocno);
            return ResultData.ResultDataSuccess(count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 数据字典级联下拉(用于vue的组件)
     *
     * @param data 数据字典编码
     * @return
     */
	@Override
	public ResultData findMapCascader(String data) {
		try {
			DicEntity entity = JSON.parseObject(data, DicEntity.class);
			String sql = "select a.v1 as 'v1',a.sname as 'sname' from dictionary a where a.ilevel = 3 and a.dicno like '" + entity.getDicno() + "%' order by indocno asc";
            List<Dictionary> list = dictionaryDao.findDataDictionaryByNo(sql);
            
            List<DpdEntity> list_all = new ArrayList<DpdEntity>();
            
            for(Dictionary entity_dic : list) {
            	DpdEntity d = new DpdEntity();
            	String sql_child = "select a.v1 as 'key',a.sname as 'value' from dictionary a where a.ilevel = 3 and a.dicno like '" + entity_dic.getV1() + "%' order by indocno asc";
            	List<DpdEntity> list_child = dictionaryDao.findMap(sql_child);
            	d.setKey(entity_dic.getV1());
            	d.setValue(entity_dic.getSname());
            	if(list_child != null && list_child.size()>0) {
            		d.setChildren(list_child);
            	}
            	list_all.add(d);
            }
            return ResultData.ResultDataSuccess(list_all);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
	}

	@Override
	public ResultData findDataByDicno(String data) {
		try {
            DicEntity entity = JSON.parseObject(data, DicEntity.class);
            String key = null;
            String[] dicno = entity.getDicno().split(",");
            String value = "";
            for(String v : dicno) {
            	value = value + "'" + v + "',";
            }
            value = value.substring(0, value.length()-1);
            String sql = "select a.v1 as 'key',a.sname as 'value' from dictionary a where a.dicno in(" + value  +")";

            List<DpdEntity> list = dictionaryDao.findMap(sql);
            return ResultData.ResultDataSuccessSelf("获取成功", list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("获取失败" + e.getMessage(), null);
        }
	}

	@Override
	public String findDataByV1(String dicno,Long v,Long value) {
		try {
            String key = null;
            if (v == 1L) {
                key = "v1";
            } else {
                key = "v2";
            }
            String sql = "select a." + key + " as 'key',a.sname as 'value' from dictionary a where a.ilevel = 3 and a.dicno like '" + dicno + "%' order by indocno asc";
            List<DpdEntity> list = dictionaryDao.findMap(sql);
            String t_value = "";
            for(DpdEntity e : list) {
            	if(value == Long.valueOf(e.getKey())) {
            		t_value = e.getValue();
            	}
            }
            return t_value;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
	};
}
