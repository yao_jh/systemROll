package com.my.business.modular.dictionary.dao;

import com.my.business.modular.dictionary.entity.Dictionary;
import com.my.business.sys.dict.entity.DpdEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 数据字典dao接口
 *
 * @author 生成器生成
 * @date 2019-11-06 21:21:18
 */
@Mapper
public interface DictionaryDao {

    /**
     * 添加记录
     *
     * @param dictionary 对象实体
     */
    void insertDataDictionary(Dictionary dictionary);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataDictionaryOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataDictionaryMany(String value);

    /**
     * 修改记录
     *
     * @param dictionary 对象实体
     */
    void updateDataDictionary(Dictionary dictionary);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<Dictionary> findDataDictionaryByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataDictionaryByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    Dictionary findDataDictionaryByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     * @param
     */
    List<Dictionary> findDataDictionary(@Param("ilevel") Long ilevel,@Param("iparent") Long iparent);
    
    /**
     * 根据编码查看记录
     *
     * @return 对象数据集合
     * @param
     */
    List<Dictionary> findDataDictionaryByNo(String sql);
    

    /***
     * 根据参数获取map类型的数据，一般用于下拉,适用于任何只需要key value的数据
     * @param sql
     * @return
     */
    List<DpdEntity> findMap(String sql);

    /**
     * 反查id V1
     *
     * @return v1
     */
    String findMapBynameV1(@Param("dicno") String dicno, @Param("sname") String sname);

    /**
     * 反查id V2
     *
     * @return v2
     */
    String findMapBynameV2(@Param("dicno") String dicno, @Param("sname") String sname);

    /**
     *
     * @param dicno 数据字典关键字编码
     * @param v1 值1
     * @return 名称
     */
    String findMapByForNameByV1(@Param("dicno") String dicno,@Param("v1") Long v1);

    /**
     *
     * @param dicno 数据字典关键字编码
     * @param v2 值2
     * @return 名称
     */
    String findMapByForNameByV2(@Param("dicno") String dicno,@Param("v2") Long v2);

    Dictionary findDataBaseEquipmentBySparentId(@Param("equipment_id") Long equipment_id);

    List<Dictionary> findDataDictionaryByDicno(@Param("ilevel") Long ilevel,@Param("dicno") String dicno);
}
