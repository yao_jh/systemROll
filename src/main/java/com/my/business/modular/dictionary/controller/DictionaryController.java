package com.my.business.modular.dictionary.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.dictionary.entity.JDictionary;
import com.my.business.modular.dictionary.service.DictionaryService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 数据字典控制器层
 *
 * @author 生成器生成
 * @date 2019-11-06 21:21:19
 */
@RestController
@RequestMapping("/dictionary")
public class DictionaryController {

    @Autowired
    private DictionaryService dictionaryService;

    /**
     * 添加记录
     *
     * @param loginToken userId 用户id sname  用户姓名
     * @param
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataDictionary(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return dictionaryService.insertDataDictionary(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataDictionaryOne(@RequestBody String data) {
        try {
            JDictionary jdictionary = JSON.parseObject(data, JDictionary.class);
            return dictionaryService.deleteDataDictionaryOne(jdictionary.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JDictionary jdictionary = JSON.parseObject(data, JDictionary.class);
            return dictionaryService.deleteDataDictionaryMany(jdictionary.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataDictionary(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return dictionaryService.updateDataDictionary(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataDictionaryByPage(@RequestBody String data) {
        return dictionaryService.findDataDictionaryByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataDictionaryByIndocno(@RequestBody String data) {
        return dictionaryService.findDataDictionaryByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public ResultData findDataDictionary() {
        return dictionaryService.findDataDictionary();
    }

    /**
     * 数据字典通用查询，下拉模式
     *
     * @param data 数据字典编码
     * @return
     */
    @CrossOrigin
    @RequestMapping(value = {"/findMapV1"}, method = RequestMethod.POST)
    public ResultData findDataV1(@RequestBody String data) {
        Long v = 1L;
        return dictionaryService.findMapV(data, v);
    }

    /**
     * 数据字典通用查询，下拉模式
     *
     * @param data 数据字典编码
     * @return
     */
    @CrossOrigin
    @RequestMapping(value = {"/findMapV2"}, method = RequestMethod.POST)
    public ResultData findDataV2(@RequestBody String data) {
        Long v = 2L;
        return dictionaryService.findMapV(data, v);
    }
    
    /**
     * 数据字典根据具体编码集合查询指定集合
     * @param data 数据字典编码
     * @return
     */
    @CrossOrigin
    @RequestMapping(value = {"/finddicno"}, method = RequestMethod.POST)
    public ResultData findDataByDicno(@RequestBody String data) {
        return dictionaryService.findDataByDicno(data);
    }
    
    /**
     * 数据字典级联下拉（vue用）
     *
     * @param data 数据字典编码
     * @return
     */
    @CrossOrigin
    @RequestMapping(value = {"/findCascader"}, method = RequestMethod.POST)
    public ResultData findMapCascader(@RequestBody String data) {
        return dictionaryService.findMapCascader(data);
    }

    /**
     * 查看节点下包含的子表记录数量
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findTreeNum"}, method = RequestMethod.POST)
    public ResultData findDataTreeNum(@RequestBody String data) {
        return dictionaryService.findDataTreeNum(data);
    }
}
