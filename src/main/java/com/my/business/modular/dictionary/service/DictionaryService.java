package com.my.business.modular.dictionary.service;

import com.my.business.sys.common.entity.ResultData;

/**
 * 数据字典接口服务类
 *
 * @author 生成器生成
 * @date 2019-11-06 21:21:19
 */
public interface DictionaryService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataDictionary(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataDictionaryOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataDictionaryMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataDictionary(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataDictionaryByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataDictionaryByIndocno(String data);

    /**
     * 查看记录
     */
    ResultData findDataDictionary();

    /**
     * 数据字典通用查询，下拉模式
     *
     * @param data 数据字典编码
     * @return
     */
    ResultData findMapV(String data, Long v);
    
    /**
     * 数据字典根据具体编码集合查询指定集合
     *
     * @param data 数据字典编码
     * @return
     */
    ResultData findDataByDicno(String data);
    
    /**
     * 数据字典级联下拉(用于vue的组件)
     *
     * @param data 数据字典编码
     * @return
     */
    ResultData findMapCascader(String data);
    
    /**
     * 节点下包含子表信息的数量
     *
     * @param data 节点主键
     * @return
     */
    public ResultData findDataTreeNum(String data);
    
    /**
     * 根据二级节点编码查询对应的值
     *
     * @param data 节点主键
     * @return
     */
    public String findDataByV1(String dicno,Long v,Long value);
}
