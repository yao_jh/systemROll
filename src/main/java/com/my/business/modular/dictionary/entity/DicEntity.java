package com.my.business.modular.dictionary.entity;

/***
 * 类似于map的json类
 * @author chenchao
 *
 */

public class DicEntity {
    private String dicno;
    private Long level;

    public String getDicno() {
        return dicno;
    }

    public void setDicno(String dicno) {
        this.dicno = dicno;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }
}
