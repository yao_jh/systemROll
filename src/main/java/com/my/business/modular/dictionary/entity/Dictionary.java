package com.my.business.modular.dictionary.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.util.List;

/**
 * 数据字典实体类
 *
 * @author 生成器生成
 * @date 2019-11-06 21:21:18
 */
public class Dictionary extends BaseEntity {

    private Long indocno;  //主键
    private String sname;  //名称
    private Long iparent;  //父节点ID
    private String sparent;  //父节点名称
    private String dicno;  //编码
    private Long ilevel;  //等级
    private String v1;  //实际值1
    private String v2;  //实际值2
    private List<Dictionary> detail;

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getSname() {
        return this.sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Long getIparent() {
        return this.iparent;
    }

    public void setIparent(Long iparent) {
        this.iparent = iparent;
    }

    public String getSparent() {
        return this.sparent;
    }

    public void setSparent(String sparent) {
        this.sparent = sparent;
    }

    public String getDicno() {
        return this.dicno;
    }

    public void setDicno(String dicno) {
        this.dicno = dicno;
    }

    public Long getIlevel() {
        return this.ilevel;
    }

    public void setIlevel(Long ilevel) {
        this.ilevel = ilevel;
    }

    public String getV1() {
        return this.v1;
    }

    public void setV1(String v1) {
        this.v1 = v1;
    }

    public String getV2() {
        return this.v2;
    }

    public void setV2(String v2) {
        this.v2 = v2;
    }

    public List<Dictionary> getDetail() {
        return detail;
    }

    public void setDetail(List<Dictionary> detail) {
        this.detail = detail;
    }
}