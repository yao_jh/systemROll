package com.my.business.modular.chockrepairscrap.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.modular.chockrepairscrap.entity.ChockRepairScrap;
import java.util.List;

/**
* 轴承座修复与报废管理接口服务类
* @author  生成器生成
* @date 2020-09-23 11:31:53
*/
public interface ChockRepairScrapService {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataChockRepairScrap(String data, Long userId, String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataChockRepairScrapOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataChockRepairScrapMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataChockRepairScrap(String data, Long userId, String sname);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataChockRepairScrapByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataChockRepairScrapByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<ChockRepairScrap> findDataChockRepairScrap();
}
