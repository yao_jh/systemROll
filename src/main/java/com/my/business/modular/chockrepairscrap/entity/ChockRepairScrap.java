package com.my.business.modular.chockrepairscrap.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轴承座修复与报废管理实体类
 *
 * @author 生成器生成
 * @date 2020-09-23 11:31:53
 */
public class ChockRepairScrap extends BaseEntity {

    private Long indocno;  //主键
    private String spare_parts_no;  //部件编号
    private Long spare_parts_type_id;  //部件类型id
    private String spare_parts_type;  //部件类型
    private String chock_no;  //轴承座编号
    private String roll_no;  //辊号
    private String scrap_reason;  //处理原因
    private String scrap_time;  //处理时间
    private Long scrap_p_id;  //报废人员id
    private String scrap_p;  //报废人员
    private Long results;  //处理结果（0 报废 1 修复）
    private String snote;  //备注

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getSpare_parts_no() {
        return this.spare_parts_no;
    }

    public void setSpare_parts_no(String spare_parts_no) {
        this.spare_parts_no = spare_parts_no;
    }

    public Long getSpare_parts_type_id() {
        return this.spare_parts_type_id;
    }

    public void setSpare_parts_type_id(Long spare_parts_type_id) {
        this.spare_parts_type_id = spare_parts_type_id;
    }

    public String getSpare_parts_type() {
        return this.spare_parts_type;
    }

    public void setSpare_parts_type(String spare_parts_type) {
        this.spare_parts_type = spare_parts_type;
    }

    public String getChock_no() {
        return this.chock_no;
    }

    public void setChock_no(String chock_no) {
        this.chock_no = chock_no;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getScrap_reason() {
        return this.scrap_reason;
    }

    public void setScrap_reason(String scrap_reason) {
        this.scrap_reason = scrap_reason;
    }

    public String getScrap_time() {
        return this.scrap_time;
    }

    public void setScrap_time(String scrap_time) {
        this.scrap_time = scrap_time;
    }

    public Long getScrap_p_id() {
        return this.scrap_p_id;
    }

    public void setScrap_p_id(Long scrap_p_id) {
        this.scrap_p_id = scrap_p_id;
    }

    public String getScrap_p() {
        return this.scrap_p;
    }

    public void setScrap_p(String scrap_p) {
        this.scrap_p = scrap_p;
    }

    public Long getResults() {
        return this.results;
    }

    public void setResults(Long results) {
        this.results = results;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }


}