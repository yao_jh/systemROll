package com.my.business.modular.chockrepairscrap.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.chockrepairscrap.entity.ChockRepairScrap;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 轴承座修复与报废管理dao接口
 * @author  生成器生成
 * @date 2020-09-23 11:31:53
 * */
@Mapper
public interface ChockRepairScrapDao {

	/**
	 * 添加记录
	 * @param chockRepairScrap  对象实体
	 * */
	public void insertDataChockRepairScrap(ChockRepairScrap chockRepairScrap);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataChockRepairScrapOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataChockRepairScrapMany(String value);
	
	/**
	 * 修改记录
	 * @param chockRepairScrap  对象实体
	 * */
	public void updateDataChockRepairScrap(ChockRepairScrap chockRepairScrap);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<ChockRepairScrap> findDataChockRepairScrapByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("spare_parts_no") String spare_parts_no,@Param("spare_parts_type_id") Long spare_parts_type_id,@Param("chock_no") String chock_no);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataChockRepairScrapByPageSize(@Param("spare_parts_no") String spare_parts_no,@Param("spare_parts_type_id") Long spare_parts_type_id,@Param("chock_no") String chock_no);
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public ChockRepairScrap findDataChockRepairScrapByIndocno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<ChockRepairScrap> findDataChockRepairScrap();
	
}
