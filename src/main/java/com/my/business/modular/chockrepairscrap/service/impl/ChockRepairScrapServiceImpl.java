package com.my.business.modular.chockrepairscrap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.chockrepairscrap.entity.ChockRepairScrap;
import com.my.business.modular.chockrepairscrap.entity.JChockRepairScrap;
import com.my.business.modular.chockrepairscrap.dao.ChockRepairScrapDao;
import com.my.business.modular.chockrepairscrap.service.ChockRepairScrapService;
import com.my.business.sys.common.entity.ResultData;
import org.springframework.util.StringUtils;

/**
* 轴承座修复与报废管理接口具体实现类
* @author  生成器生成
* @date 2020-09-23 11:31:54
*/
@Service
public class ChockRepairScrapServiceImpl implements ChockRepairScrapService {
	
	@Autowired
	private ChockRepairScrapDao chockRepairScrapDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataChockRepairScrap(String data,Long userId,String sname){
		try{
			JChockRepairScrap jchockRepairScrap = JSON.parseObject(data,JChockRepairScrap.class);
    		ChockRepairScrap chockRepairScrap = jchockRepairScrap.getChockRepairScrap();
    		CodiUtil.newRecord(userId,sname,chockRepairScrap);
            chockRepairScrapDao.insertDataChockRepairScrap(chockRepairScrap);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataChockRepairScrapOne(Long indocno){
		try {
            chockRepairScrapDao.deleteDataChockRepairScrapOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataChockRepairScrapMany(String str_id) {
        try {
        	String sql = "delete chock_repair_scrap where indocno in(" + str_id +")";
            chockRepairScrapDao.deleteDataChockRepairScrapMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param data sysUser 对象实体
     */
	public ResultData updateDataChockRepairScrap(String data,Long userId,String sname){
		try {
			JChockRepairScrap jchockRepairScrap = JSON.parseObject(data,JChockRepairScrap.class);
    		ChockRepairScrap chockRepairScrap = jchockRepairScrap.getChockRepairScrap();
        	CodiUtil.editRecord(userId,sname,chockRepairScrap);
            chockRepairScrapDao.updateDataChockRepairScrap(chockRepairScrap);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataChockRepairScrapByPage(String data) {
        try {
        	JChockRepairScrap jchockRepairScrap = JSON.parseObject(data, JChockRepairScrap.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jchockRepairScrap.getPageIndex();
        	Integer pageSize = jchockRepairScrap.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jchockRepairScrap.getCondition()){
    			jsonObject  = JSON.parseObject(jchockRepairScrap.getCondition().toString());
    		}

			String spare_parts_no = null;
			if (!StringUtils.isEmpty(jsonObject.get("spare_parts_no"))) {
				spare_parts_no = jsonObject.get("spare_parts_no").toString();
			}

			Long spare_parts_type_id = null;
			if (!StringUtils.isEmpty(jsonObject.get("spare_parts_type_id"))) {
				spare_parts_type_id = Long.valueOf(jsonObject.get("spare_parts_type_id").toString());
			}

			String chock_no = null;
			if (!StringUtils.isEmpty(jsonObject.get("chock_no"))) {
				chock_no = jsonObject.get("chock_no").toString();
			}

    		List<ChockRepairScrap> list = chockRepairScrapDao.findDataChockRepairScrapByPage((pageIndex-1)*pageSize, pageSize,spare_parts_no,spare_parts_type_id,chock_no);
    		Integer count = chockRepairScrapDao.findDataChockRepairScrapByPageSize(spare_parts_no,spare_parts_type_id,chock_no);
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataChockRepairScrapByIndocno(String data) {
        try {
        	JChockRepairScrap jchockRepairScrap = JSON.parseObject(data, JChockRepairScrap.class); 
        	Long indocno = jchockRepairScrap.getIndocno();
        	
    		ChockRepairScrap chockRepairScrap = chockRepairScrapDao.findDataChockRepairScrapByIndocno(indocno);
    		return ResultData.ResultDataSuccess(chockRepairScrap);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<ChockRepairScrap> findDataChockRepairScrap(){
		List<ChockRepairScrap> list = chockRepairScrapDao.findDataChockRepairScrap();
		return list;
	};
}
