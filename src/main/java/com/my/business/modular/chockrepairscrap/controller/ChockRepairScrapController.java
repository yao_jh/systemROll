package com.my.business.modular.chockrepairscrap.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.modular.chockrepairscrap.entity.ChockRepairScrap;
import com.my.business.modular.chockrepairscrap.entity.JChockRepairScrap;
import com.my.business.modular.chockrepairscrap.service.ChockRepairScrapService;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import com.my.business.sys.common.entity.ResultData;
import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.my.business.util.CodiUtil;
import org.springframework.web.bind.annotation.RequestHeader;


/**
* 轴承座修复与报废管理控制器层
* @author  生成器生成
* @date 2020-09-23 11:31:54
*/
@RestController
@RequestMapping("/chockRepairScrap")
public class ChockRepairScrapController {

	@Autowired
	private ChockRepairScrapService chockRepairScrapService;
	
	/**
	 * 添加记录
	 * @param data userId 用户id
     * @param data sname 用户姓名
	 * */
	@CrossOrigin
	@RequestMapping(value={"/insert"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData insertDataChockRepairScrap(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return chockRepairScrapService.insertDataChockRepairScrap(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
	 * 根据主键删除对象
	 * @param data json字符串
	 * */
	@CrossOrigin
	@RequestMapping(value={"/deleteOne"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData deleteDataChockRepairScrapOne(@RequestBody String data){
		try{
    		JChockRepairScrap jchockRepairScrap = JSON.parseObject(data,JChockRepairScrap.class);
    		return chockRepairScrapService.deleteDataChockRepairScrapOne(jchockRepairScrap.getIndocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
    	try{
    		JChockRepairScrap jchockRepairScrap = JSON.parseObject(data,JChockRepairScrap.class);
    		return chockRepairScrapService.deleteDataChockRepairScrapMany(jchockRepairScrap.getStr_indocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
    }
	
	/**
     * 修改记录
     * @param data json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
	@RequestMapping(value={"/update"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData updateDataChockRepairScrap(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return chockRepairScrapService.updateDataChockRepairScrap(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
     * 分页查看记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataChockRepairScrapByPage(@RequestBody String data) {
        return chockRepairScrapService.findDataChockRepairScrapByPage(data);
    }
    
    /**
     * 根据主键查询单条记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataChockRepairScrapByIndocno(@RequestBody String data) {
        return chockRepairScrapService.findDataChockRepairScrapByIndocno(data);
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	@CrossOrigin
	@RequestMapping(value={"/findAll"}, method=RequestMethod.POST)
	public List<ChockRepairScrap> findDataChockRepairScrap(){
		return chockRepairScrapService.findDataChockRepairScrap();
	};
}
