package com.my.business.modular.rolldevicecheckschedule.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class CheckTreeInfo {

    // 默认的集合
    private List<CheckDeviceInfo> defaultList;

    // 默认集合的补集
    private List<CheckDeviceInfo> remainList;

    public List<CheckDeviceInfo> getDefaultList() {
        return defaultList;
    }

    public void setDefaultList(List<CheckDeviceInfo> defaultList) {
        this.defaultList = defaultList;
    }

    public List<CheckDeviceInfo> getRemainList() {
        return remainList;
    }

    public void setRemainList(List<CheckDeviceInfo> remainList) {
        this.remainList = remainList;
    }

}