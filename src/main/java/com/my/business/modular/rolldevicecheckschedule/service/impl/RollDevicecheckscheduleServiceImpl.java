package com.my.business.modular.rolldevicecheckschedule.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rolldevicecheckhistory.entity.RollDevicecheckhistory;
import com.my.business.modular.rolldevicecheckhistory.service.RollDevicecheckhistoryService;
import com.my.business.modular.rolldevicecheckmodel.dao.RollDevicecheckmodelDao;
import com.my.business.modular.rolldevicecheckmodel.entity.ModelItem;
import com.my.business.modular.rolldevicecheckmodel.entity.RollDeviceCheckModel;
import com.my.business.modular.rolldevicecheckmodel.service.RollDevicecheckmodelService;
import com.my.business.modular.rolldevicecheckschedule.dao.RollDevicecheckscheduleDao;
import com.my.business.modular.rolldevicecheckschedule.entity.CheckTreeInfo;
import com.my.business.modular.rolldevicecheckschedule.entity.JRollDevicecheckschedule;
import com.my.business.modular.rolldevicecheckschedule.entity.RollDevicecheckschedule;
import com.my.business.modular.rolldevicecheckschedule.service.RollDevicecheckscheduleService;
import com.my.business.modular.rolldevicescheck.dao.RollDevicescheckDao;
import com.my.business.modular.rolldevicescheckdict.dao.RollDevicescheckdictDao;
import com.my.business.modular.rolldevicescheckdict.entity.RollDevicescheckdict;
import com.my.business.modular.rolldevicescheckdict.service.RollDevicesCheckDictService;
import com.my.business.modular.rolldevicescheckpart.dao.RollDevicescheckpartDao;
import com.my.business.modular.rolldevicescheckpart.entity.RollDevicescheckpart;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.util.MathUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 点检计划分配表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-07-14 16:25:03
 */
@Service
@EnableScheduling
public class RollDevicecheckscheduleServiceImpl implements RollDevicecheckscheduleService {

    // 检查类型
    private static final int DAILY_CHECK = 0;// 日检
    private static final int DEVICE_STOP_CHECK = 1;// 停机检
    private static final int YEAR_CHECK = 2;// 年检

    // 模板自动添加
    private static final Long USERID = 1L;// 操作人id
    private static final String SNAME = "管理员";//操作人名称

    // 检查周期类型
    private static final String CHECK_INTERNAL_UNIT_DAY = "D";// 日
    private static final String CHECK_INTERNAL_UNIT_WEEK = "W";// 周
    private static final String CHECK_INTERNAL_UNIT_MONTH = "M";// 月
    private static final String CHECK_INTERNAL_UNIT_YEAR = "Y";//年

    // 检查状态
    private static final Integer CHECK_PASS = 0;// 通过
    private static final Integer CHECK_NOPASS = 1;// 未通过
    private static final Integer CHECK_LATER = 2;// 以后再测

    // DATE 和 字符串转换格式
    private static final String PATTERN = "yyyy-MM-dd";

    // 设备点检类型
    private static final String CHECK_TYPE_DAILY = "日检";//日检
    private static final String CHECK_TYPE_STOP = "停机检";//停机检
    private static final String CHECK_TYPE_YEAR = "年检";//年检

    @Autowired
    private RollDevicecheckscheduleDao rollDevicecheckscheduleDao;
    @Autowired
    private RollDevicecheckmodelDao rollDevicecheckmodelDao;
    @Autowired
    private RollDevicescheckdictDao rollDevicescheckdictDao;
    @Autowired
    private RollDevicescheckpartDao rollDevicescheckpartDao;
    @Autowired
    private RollDevicescheckDao rollDevicescheckDao;
    @Autowired
    private RollDevicecheckmodelService rollDevicecheckmodelService;
    @Autowired
    private RollDevicesCheckDictService rollDevicesCheckDictService;
    @Autowired
    private RollDevicecheckhistoryService rollDevicecheckhistoryService;

    private static List<RollDevicescheckpart> removeDupliById(List<RollDevicescheckpart> checkParts) {
        Set<RollDevicescheckpart> checkPartSet = new TreeSet<>((o1, o2) -> o1.getIndocno().compareTo(o2.getIndocno()));
        checkPartSet.addAll(checkParts);
        return new ArrayList<>(checkPartSet);
    }

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollDevicecheckschedule(String data, Long userId, String sname) {
        try {
            JRollDevicecheckschedule jrollDevicecheckschedule = JSON.parseObject(data, JRollDevicecheckschedule.class);
            RollDevicecheckschedule rollDevicecheckschedule = jrollDevicecheckschedule.getRollDevicecheckschedule();
            CodiUtil.newRecord(userId, sname, rollDevicecheckschedule);
            rollDevicecheckscheduleDao.insertDataRollDevicecheckschedule(rollDevicecheckschedule);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 定时任务添加 每天凌晨5点执行任务
     * 日检项目每天凌晨五点  0 0 5 * * ?
     *
     * @return
     */
    @Scheduled(cron = "0 0 9 * * ?")
    public void insertDailyCheckSchedule() {
        try {
            RollDeviceCheckModel rollDeviceCheckModel = getDefaultModel(DAILY_CHECK);
            insertCheckSchedule(rollDeviceCheckModel, new Date());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 年检项目每年第一天早上8点 0 0 8 1 1 ?
     */
    @Scheduled(cron = "0 0 8 1 1 ?")
    public void insertYearCheckSchedule() {
        try {
            RollDeviceCheckModel rollDeviceCheckModel = getDefaultModel(YEAR_CHECK);
            insertCheckSchedule(rollDeviceCheckModel, new Date());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 停机项手动添加
     */
    public void insertDeviceStopCheckSchedule(String checkTime) {
        try {
            RollDeviceCheckModel rollDeviceCheckModel = getDefaultModel(DEVICE_STOP_CHECK);
            insertCheckSchedule(rollDeviceCheckModel, MathUtil.convertString2Date(checkTime, PATTERN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param data
     * @return
     */
    @Override
    public ResultData findDataRollDeviceCheckScheduleByScheduleId(String data) {
        try {
            JRollDevicecheckschedule jRollDevicecheckschedule = JSON.parseObject(data, JRollDevicecheckschedule.class);
            Long indocno = jRollDevicecheckschedule.getIndocno();
            RollDevicecheckschedule rollDevicecheckschedule = rollDevicecheckscheduleDao.findDataRollDevicecheckscheduleByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollDevicecheckschedule);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }



    /**
     * 点检计划添加流程
     */
    private void insertCheckSchedule(RollDeviceCheckModel rollDeviceCheckModel, Date checkTime) {
        RollDevicecheckschedule rollDevicecheckschedule = new RollDevicecheckschedule();
        rollDevicecheckschedule.setCheck_type(DAILY_CHECK);// 检查类型(0/1/2，日检/停机检/年检）
        rollDevicecheckschedule.setCheck_time(checkTime);// 点检计划时间:当天日期
        ResultData resultData = rollDevicecheckmodelService.insertDataRollDevicecheckmodel(rollDeviceCheckModel, 7l, "王英璨");
        // 创建新的模板 并提供新模板的id
        rollDevicecheckschedule.setCheck_model_id(Long.parseLong(resultData.getData().toString()));// 点检模板id
        CodiUtil.newRecord(USERID, SNAME, rollDevicecheckschedule);
        rollDevicecheckscheduleDao.insertDataRollDevicecheckschedule(rollDevicecheckschedule);
        Long newScheduleId = rollDevicecheckschedule.getIndocno();
        // 同步添加到历史数据中
        RollDevicecheckhistory rollDevicecheckhistory = new RollDevicecheckhistory();
        rollDevicecheckhistory.setCheck_schedule_id(newScheduleId); // 点检计划id
        rollDevicecheckhistory.setCheck_complete_time(new Date());// 点检完成时间
        rollDevicecheckhistoryService.insertDataRollDevicecheckhistory(rollDevicecheckhistory, 7l, "王英璨");
    }

    /**
     * 根据不同的周
     *
     * @return
     */
    private RollDeviceCheckModel getDefaultModel(Integer checkType) {
        RollDeviceCheckModel rollDeviceCheckModel = new RollDeviceCheckModel();
        String[] checkIntervalUnit = new String[2];
        // 是否改变dict 添加点检类型字段 插入的时候判断是日检 还是 停机检 还是年检
        switch (checkType) {
            case DAILY_CHECK:
                checkIntervalUnit[0] = CHECK_INTERNAL_UNIT_DAY;
                break;
            case DEVICE_STOP_CHECK:
                checkIntervalUnit[0] = CHECK_INTERNAL_UNIT_WEEK;
                checkIntervalUnit[1] = CHECK_INTERNAL_UNIT_MONTH;
                break;
            case YEAR_CHECK:
                checkIntervalUnit[0] = CHECK_INTERNAL_UNIT_YEAR;
                break;
            default:
                break;
        }
        List<RollDevicescheckdict> rollDevicescheckdicts = rollDevicesCheckDictService.findDataRollDevicesCheckDictByCheckType(checkIntervalUnit);
        List<Long> deviceCheckIds = rollDevicescheckdicts.stream().map(RollDevicescheckdict::getIndocno).collect(Collectors.toList());

        List<ModelItem> modelItems = new ArrayList<ModelItem>();
        for (Long checkId : deviceCheckIds) {
            ModelItem items = new ModelItem();
            items.setKey(checkId);
            items.setValue("");
            modelItems.add(items);
        }
        String modelJson = JSON.toJSONString(modelItems);
        // 根据不同的checkType 获取对应的默认检查项目
        rollDeviceCheckModel.setCheck_items(modelJson);//设备检查项目列表，key为字典表id,value为检查状态（0通过，1未通过，2以后再测）
        rollDeviceCheckModel.setCheck_type(checkType);//检查类型(0/1/2，日检/停机检/年检）
        // 分成不同的角色 每个厂都只有一个负责人 但是有两个厂 1580 和 2250 这个做到权限里面
        // TODO : 此处先写死 等后面有改动再说
        rollDeviceCheckModel.setCheck_manager_name("王英璨");//检测人
        rollDeviceCheckModel.setCheck_manager_id(7l);//检测人id
        return rollDeviceCheckModel;
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollDevicecheckscheduleOne(Long indocno) {
        try {
            rollDevicecheckscheduleDao.deleteDataRollDevicecheckscheduleOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollDevicecheckscheduleMany(String str_id) {
        try {
            String sql = "delete roll_devicecheckschedule where indocno in(" + str_id + ")";
            rollDevicecheckscheduleDao.deleteDataRollDevicecheckscheduleMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   对象实体
     */
    public ResultData updateDataRollDevicecheckschedule(String data, Long userId, String sname) {
        try {
            JRollDevicecheckschedule jrollDevicecheckschedule = JSON.parseObject(data, JRollDevicecheckschedule.class);
            RollDevicecheckschedule rollDevicecheckschedule = jrollDevicecheckschedule.getRollDevicecheckschedule();
            // 判断是否是今日点检
            CodiUtil.editRecord(userId, sname, rollDevicecheckschedule);
            rollDevicecheckscheduleDao.updateDataRollDevicecheckschedule(rollDevicecheckschedule);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollDevicecheckscheduleByPage(String data) {
        try {
            JRollDevicecheckschedule jrollDevicecheckschedule = JSON.parseObject(data, JRollDevicecheckschedule.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollDevicecheckschedule.getPageIndex();
            Integer pageSize = jrollDevicecheckschedule.getPageSize();
            pageIndex = (pageIndex-1) * pageSize;

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollDevicecheckschedule.getCondition()) {
                jsonObject = JSON.parseObject(jrollDevicecheckschedule.getCondition().toString());
            }

            // 点检负责人
            String checkManagerNameCondition = null;
            if (!StringUtils.isEmpty(jsonObject.get("check_manager_condition"))) {
                checkManagerNameCondition = jsonObject.get("check_manager_condition").toString();
            }

            // 点检类型
            String checkTypeStr = null;
            Integer checkType = 0;
            if (!StringUtils.isEmpty(jsonObject.get("check_type_condition"))) {
                checkTypeStr = jsonObject.get("check_type_condition").toString();
                checkType = Integer.parseInt(checkTypeStr);
            }

            // 点检计划时间开始
            String arrivalDateStart = null;// 到货日期开始时间
            if (!StringUtils.isEmpty(jsonObject.get("check_date_start_condition"))) {
                arrivalDateStart = jsonObject.get("check_date_start_condition").toString();
            }

            // 点检计划时间结束
            String arrivalDateEnd = null;// 到货日期开始时间
            if (!StringUtils.isEmpty(jsonObject.get("check_date_end_condition"))) {
                arrivalDateEnd = jsonObject.get("check_date_end_condition").toString();
            }

            List<RollDevicecheckschedule> list = rollDevicecheckscheduleDao.findDataRollDevicecheckscheduleByPage(pageIndex, pageSize, checkManagerNameCondition, checkType, arrivalDateStart, arrivalDateEnd);
            for (RollDevicecheckschedule rollDevicecheckschedule : list) {
                Date checkDate = rollDevicecheckschedule.getCheck_time();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
                String checkDateStr = sdf.format(checkDate);
                String checkName = checkDateStr + getCheckTypeStr(rollDevicecheckschedule.getCheck_type());
                rollDevicecheckschedule.setCheck_name(checkName);

                // 负责人
                Long checkModelId = rollDevicecheckschedule.getCheck_model_id();
                RollDeviceCheckModel rollDeviceCheckModel = rollDevicecheckmodelDao.findDataRollDevicecheckmodelByIndocno(checkModelId);
                String checkManagerName = rollDeviceCheckModel.getCheck_manager_name();
                rollDevicecheckschedule.setCheck_manager_name(checkManagerName);
            }
            Integer count = rollDevicecheckscheduleDao.findDataRollDevicecheckscheduleByPageSize(checkManagerNameCondition, checkType, arrivalDateStart, arrivalDateEnd);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    private String getCheckTypeStr(int checkType) {
        String checkTypeStr = null;
        switch (checkType) {
            case 0:
                checkTypeStr = CHECK_TYPE_DAILY;
                break;
            case 1:
                checkTypeStr = CHECK_TYPE_STOP;
                break;
            case 2:
                checkTypeStr = CHECK_TYPE_YEAR;
                break;
            default:
                break;
        }
        return checkTypeStr;
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollDevicecheckscheduleByIndocno(String data) {
        try {
            JRollDevicecheckschedule jrollDevicecheckschedule = JSON.parseObject(data, JRollDevicecheckschedule.class);
            Long indocno = jrollDevicecheckschedule.getIndocno();

            RollDevicecheckschedule rollDevicecheckschedule = rollDevicecheckscheduleDao.findDataRollDevicecheckscheduleByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollDevicecheckschedule);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    @Override
    public ResultData findTreeInfoByIndocno(String data) {
        JRollDevicecheckschedule jrollDevicecheckschedule = JSON.parseObject(data, JRollDevicecheckschedule.class);
        Long indocno = jrollDevicecheckschedule.getIndocno();

        RollDevicecheckschedule rollDevicecheckschedule = rollDevicecheckscheduleDao.findDataRollDevicecheckscheduleByIndocno(indocno);
        Long checkModelId = rollDevicecheckschedule.getCheck_model_id();
        CheckTreeInfo checkTreeInfo = rollDevicecheckmodelService.findTreeInfoByIndocno(checkModelId + "");
        return ResultData.ResultDataSuccess(checkTreeInfo);
    }

    @Override
    public RollDevicecheckschedule findDataRollDevicecheckscheduleByModelId(Long checkModelId) {
        try {
            RollDevicecheckschedule rollDevicecheckschedule = rollDevicecheckscheduleDao.findDataRollDevicecheckscheduleByModelId(checkModelId);
            return rollDevicecheckschedule;
        } catch (Exception e) {
            e.printStackTrace();
            return null;// TODO
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollDevicecheckschedule> findDataRollDevicecheckschedule() {
        List<RollDevicecheckschedule> list = rollDevicecheckscheduleDao.findDataRollDevicecheckschedule();
        return list;
    }
}
