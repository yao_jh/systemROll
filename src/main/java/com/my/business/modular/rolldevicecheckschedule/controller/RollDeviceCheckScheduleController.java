package com.my.business.modular.rolldevicecheckschedule.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rolldevicecheckschedule.entity.JRollDevicecheckschedule;
import com.my.business.modular.rolldevicecheckschedule.entity.RollDevicecheckschedule;
import com.my.business.modular.rolldevicecheckschedule.service.RollDevicecheckscheduleService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 点检计划分配表控制器层
 *
 * @author 生成器生成
 * @date 2020-07-14 16:25:03
 */
@RestController
@RequestMapping("/rollDevicecheckschedule")
public class RollDeviceCheckScheduleController {

    @Autowired
    private RollDevicecheckscheduleService rollDevicecheckscheduleService;

    /**
     * 添加记录
     *
     * @param loginToken 用户token
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollDevicecheckschedule(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollDevicecheckscheduleService.insertDataRollDevicecheckschedule(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollDevicecheckscheduleOne(@RequestBody String data) {
        try {
            JRollDevicecheckschedule jrollDevicecheckschedule = JSON.parseObject(data, JRollDevicecheckschedule.class);
            return rollDevicecheckscheduleService.deleteDataRollDevicecheckscheduleOne(jrollDevicecheckschedule.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollDevicecheckschedule jrollDevicecheckschedule = JSON.parseObject(data, JRollDevicecheckschedule.class);
            return rollDevicecheckscheduleService.deleteDataRollDevicecheckscheduleMany(jrollDevicecheckschedule.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollDevicecheckschedule(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollDevicecheckscheduleService.updateDataRollDevicecheckschedule(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 输出默认的格式和所有的点检项目
     */
    @CrossOrigin
    @RequestMapping(value = {"/findDataByScheduleId"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDevicecheckscheduleByScheduleId(@RequestBody String data) {
        return rollDevicecheckscheduleService.findDataRollDeviceCheckScheduleByScheduleId(data);
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDevicecheckscheduleByPage(@RequestBody String data) {
        return rollDevicecheckscheduleService.findDataRollDevicecheckscheduleByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDevicecheckscheduleByIndocno(@RequestBody String data) {
        return rollDevicecheckscheduleService.findDataRollDevicecheckscheduleByIndocno(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findTreeInfoByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findTreeInfoByIndocno(@RequestBody String data) {
        return rollDevicecheckscheduleService.findTreeInfoByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollDevicecheckschedule> findDataRollDevicecheckschedule() {
        return rollDevicecheckscheduleService.findDataRollDevicecheckschedule();
    }
}
