package com.my.business.modular.rolldevicecheckschedule.service;

import com.my.business.modular.rolldevicecheckschedule.entity.RollDevicecheckschedule;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 点检计划分配表接口服务类
 *
 * @author 生成器生成
 * @date 2020-07-14 16:25:03
 */
public interface RollDevicecheckscheduleService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollDevicecheckschedule(String data, Long userId, String sname);

    /**
     * 日检任务添加 每天早晨5点执行任务
     */
    void insertDailyCheckSchedule();

    /**
     * 停机检任务手动添加
     */
    void insertDeviceStopCheckSchedule(String checkTime);

    /**
     * 年检任务添加 每年第一天早上8点
     */
    void insertYearCheckSchedule();

    ResultData findDataRollDeviceCheckScheduleByScheduleId(String data);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollDevicecheckscheduleOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollDevicecheckscheduleMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollDevicecheckschedule(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollDevicecheckscheduleByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollDevicecheckscheduleByIndocno(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findTreeInfoByIndocno(String data);

    /**
     * 查看一条数据信息
     *
     * @param checkModelId 分页参数字符串
     */
    RollDevicecheckschedule findDataRollDevicecheckscheduleByModelId(Long checkModelId);

    /**
     * 查看记录
     */
    List<RollDevicecheckschedule> findDataRollDevicecheckschedule();
}
