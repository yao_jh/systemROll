package com.my.business.modular.rolldevicecheckschedule.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class CheckItemInfo {

    private String id;// 点检项id

    private String name;// 点检项目名称

    private String checkSchedule;// 点检标准

    private String pid;// 树形结构的级别 对应上一层id

    private Boolean isflag = true;// 是否是最底层

    private String checkResult;// 点检结果 0不通过/1通过

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCheckSchedule() {
        return checkSchedule;
    }

    public void setCheckSchedule(String checkSchedule) {
        this.checkSchedule = checkSchedule;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Boolean getIsflag() {
        return isflag;
    }

    public void setIsflag(Boolean isflag) {
        this.isflag = isflag;
    }

    public String getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult;
    }

}
