package com.my.business.modular.rolldevicecheckschedule.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

// 点检项目穿梭框使用的设备信息
@Data
@EqualsAndHashCode
public class CheckDeviceInfo {

    private Long id;// 设备id

    private String name;// 设备名称 + （运行/停机）

    private String description; // 点检备注

    private String operatorName = "王英璨";// 操作员名称

    private Integer pid = 0;// 树形结构的级别 对应上一层id

    private Boolean isflag = false;// 是否是最底层

//    private TreeMap<Long ,CheckPartInfo> CheckPartInfoMap;// 点检部位

    private List<CheckPartInfo> children;// 点检部位list

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Boolean getIsflag() {
        return isflag;
    }

    public void setIsflag(Boolean isflag) {
        this.isflag = isflag;
    }

    public List<CheckPartInfo> getChildren() {
        return children;
    }

    public void setChildren(List<CheckPartInfo> children) {
        this.children = children;
    }

}
