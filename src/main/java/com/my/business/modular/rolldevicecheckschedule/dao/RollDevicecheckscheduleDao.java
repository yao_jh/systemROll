package com.my.business.modular.rolldevicecheckschedule.dao;

import com.my.business.modular.rolldevicecheckschedule.entity.RollDevicecheckschedule;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 点检计划分配表dao接口
 *
 * @author 生成器生成
 * @date 2020-07-14 16:25:03
 */
@Mapper
public interface RollDevicecheckscheduleDao {

    /**
     * 添加记录
     *
     * @param rollDevicecheckschedule 对象实体
     */
    void insertDataRollDevicecheckschedule(RollDevicecheckschedule rollDevicecheckschedule);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollDevicecheckscheduleOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollDevicecheckscheduleMany(String value);

    /**
     * 修改记录
     *
     * @param rollDevicecheckschedule 对象实体
     */
    void updateDataRollDevicecheckschedule(RollDevicecheckschedule rollDevicecheckschedule);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollDevicecheckschedule> findDataRollDevicecheckscheduleByPage(@Param("pageIndex") Integer pageIndex,
                                                                        @Param("pageSize") Integer pageSize,
                                                                        @Param("check_manager_condition") String checkManagerCondition,
                                                                        @Param("check_type_condition") Integer checkType,
                                                                        @Param("check_date_start_condition") String checkDateStart,
                                                                        @Param("check_date_end_condition") String checkDateEnd);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollDevicecheckscheduleByPageSize(@Param("check_manager_condition") String checkManagerCondition,
                                                      @Param("check_type_condition") Integer checkType,
                                                      @Param("check_date_start_condition") String checkDateStart,
                                                      @Param("check_date_end_condition") String checkDateEnd);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollDevicecheckschedule findDataRollDevicecheckscheduleByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollDevicecheckschedule> findDataRollDevicecheckschedule();

    /**
     * 查看记录
     *
     * @param modelId
     */
    RollDevicecheckschedule findDataRollDevicecheckscheduleByModelId(@Param("modelId") Long modelId);

}
