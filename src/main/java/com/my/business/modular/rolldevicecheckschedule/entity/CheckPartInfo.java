package com.my.business.modular.rolldevicecheckschedule.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class CheckPartInfo {

    private String id;// 点检设备部位id

    private String name;// 点检设备部位名称

    private Long pid;// 树形结构的级别 对应上一层id

    private Boolean isflag = false;// 是否是最底层

    private List<CheckItemInfo> children;// 点检项目

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Boolean getIsflag() {
        return isflag;
    }

    public void setIsflag(Boolean isflag) {
        this.isflag = isflag;
    }

    public List<CheckItemInfo> getChildren() {
        return children;
    }

    public void setChildren(List<CheckItemInfo> children) {
        this.children = children;
    }
}
