package com.my.business.modular.rolldevicecheckschedule.entity;

import com.my.business.sys.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 点检计划分配表实体类
 *
 * @author 生成器生成
 * @date 2020-07-14 16:25:02
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RollDevicecheckschedule extends BaseEntity {

    private Long indocno;  //主键
    private Date check_time;  //点检计划时间
    private Integer check_type;  //检查类型(0/1/2，日检/停机检/年检）
    private Long check_model_id;  //点检模板id
    private Integer check_complete_ornot = 1;  //是否点检（0/1/2，今日不点检/安排点检但是目前未提交/已点检完成）
    private String check_incomplete_reason;  //不点检原因
    private String check_comments;  //备注

    private String check_name;// 点检名称
    private String check_manager_name;// 负责人

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Date getCheck_time() {
        return check_time;
    }

    public void setCheck_time(Date check_time) {
        this.check_time = check_time;
    }

    public Integer getCheck_type() {
        return check_type;
    }

    public void setCheck_type(Integer check_type) {
        this.check_type = check_type;
    }

    public Long getCheck_model_id() {
        return check_model_id;
    }

    public void setCheck_model_id(Long check_model_id) {
        this.check_model_id = check_model_id;
    }

    public Integer getCheck_complete_ornot() {
        return check_complete_ornot;
    }

    public void setCheck_complete_ornot(Integer check_complete_ornot) {
        this.check_complete_ornot = check_complete_ornot;
    }

    public String getCheck_incomplete_reason() {
        return check_incomplete_reason;
    }

    public void setCheck_incomplete_reason(String check_incomplete_reason) {
        this.check_incomplete_reason = check_incomplete_reason;
    }

    public String getCheck_comments() {
        return check_comments;
    }

    public void setCheck_comments(String check_comments) {
        this.check_comments = check_comments;
    }

    public String getCheck_name() {
        return check_name;
    }

    public void setCheck_name(String check_name) {
        this.check_name = check_name;
    }

    public String getCheck_manager_name() {
        return check_manager_name;
    }

    public void setCheck_manager_name(String check_manager_name) {
        this.check_manager_name = check_manager_name;
    }
}