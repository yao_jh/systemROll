package com.my.business.modular.rckurumainfo.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rckurumainfo.entity.JRcKurumaInfo;
import com.my.business.modular.rckurumainfo.entity.RcKurumaInfo;
import com.my.business.modular.rckurumainfo.service.RcKurumaInfoService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.dict.entity.DpdEntity;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 换辊小车表——工作辊类控制器层
 *
 * @author 生成器生成
 * @date 2020-10-05 16:33:29
 */
@RestController
@RequestMapping("/rcKurumaInfo")
public class RcKurumaInfoController {

    @Autowired
    private RcKurumaInfoService rcKurumaInfoService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRcKurumaInfo(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rcKurumaInfoService.insertDataRcKurumaInfo(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRcKurumaInfoOne(@RequestBody String data) {
        try {
            JRcKurumaInfo jrcKurumaInfo = JSON.parseObject(data, JRcKurumaInfo.class);
            return rcKurumaInfoService.deleteDataRcKurumaInfoOne(jrcKurumaInfo.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRcKurumaInfo jrcKurumaInfo = JSON.parseObject(data, JRcKurumaInfo.class);
            return rcKurumaInfoService.deleteDataRcKurumaInfoMany(jrcKurumaInfo.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRcKurumaInfo(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rcKurumaInfoService.updateDataRcKurumaInfo(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRcKurumaInfoByPage(@RequestBody String data) {
        return rcKurumaInfoService.findDataRcKurumaInfoByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRcKurumaInfoByIndocno(@RequestBody String data) {
        return rcKurumaInfoService.findDataRcKurumaInfoByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RcKurumaInfo> findDataRcKurumaInfo() {
        return rcKurumaInfoService.findDataRcKurumaInfo();
    }

    /**
     * 表格查询接口
     */
    @CrossOrigin
    @RequestMapping(value = {"/findlist"}, method = RequestMethod.POST)
    @ResponseBody
    public List<DpdEntity> findlist(@RequestBody String data) {
        return rcKurumaInfoService.findlist(data);
    }
}
