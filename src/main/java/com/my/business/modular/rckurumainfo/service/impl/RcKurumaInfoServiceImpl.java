package com.my.business.modular.rckurumainfo.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rckurumainfo.dao.RcKurumaInfoDao;
import com.my.business.modular.rckurumainfo.entity.JRcKurumaInfo;
import com.my.business.modular.rckurumainfo.entity.RcKurumaInfo;
import com.my.business.modular.rckurumainfo.service.RcKurumaInfoService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.dict.entity.DpdEntity;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 换辊小车表——工作辊类接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-10-05 16:33:29
 */
@Service
public class RcKurumaInfoServiceImpl implements RcKurumaInfoService {

    @Autowired
    private RcKurumaInfoDao rcKurumaInfoDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRcKurumaInfo(String data, Long userId, String sname) {
        try {
            JRcKurumaInfo jrcKurumaInfo = JSON.parseObject(data, JRcKurumaInfo.class);
            RcKurumaInfo rcKurumaInfo = jrcKurumaInfo.getRcKurumaInfo();
            CodiUtil.newRecord(userId, sname, rcKurumaInfo);
            rcKurumaInfoDao.insertDataRcKurumaInfo(rcKurumaInfo);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRcKurumaInfoOne(Long indocno) {
        try {
            rcKurumaInfoDao.deleteDataRcKurumaInfoOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRcKurumaInfoMany(String str_id) {
        try {
            String sql = "delete from rc_kuruma_info where indocno in(" + str_id + ")";
            rcKurumaInfoDao.deleteDataRcKurumaInfoMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRcKurumaInfo(String data, Long userId, String sname) {
        try {
            JRcKurumaInfo jrcKurumaInfo = JSON.parseObject(data, JRcKurumaInfo.class);
            RcKurumaInfo rcKurumaInfo = jrcKurumaInfo.getRcKurumaInfo();
            CodiUtil.editRecord(userId, sname, rcKurumaInfo);
            rcKurumaInfoDao.updateDataRcKurumaInfo(rcKurumaInfo);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRcKurumaInfoByPage(String data) {
        try {
            JRcKurumaInfo jrcKurumaInfo = JSON.parseObject(data, JRcKurumaInfo.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrcKurumaInfo.getPageIndex();
            Integer pageSize = jrcKurumaInfo.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrcKurumaInfo.getCondition()) {
                jsonObject = JSON.parseObject(jrcKurumaInfo.getCondition().toString());
            }

            List<RcKurumaInfo> list = rcKurumaInfoDao.findDataRcKurumaInfoByPage((pageIndex - 1) * pageSize, pageSize);
            Integer count = rcKurumaInfoDao.findDataRcKurumaInfoByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRcKurumaInfoByIndocno(String data) {
        try {
            JRcKurumaInfo jrcKurumaInfo = JSON.parseObject(data, JRcKurumaInfo.class);
            Long indocno = jrcKurumaInfo.getIndocno();

            RcKurumaInfo rcKurumaInfo = rcKurumaInfoDao.findDataRcKurumaInfoByIndocno(indocno);
            return ResultData.ResultDataSuccess(rcKurumaInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RcKurumaInfo> findDataRcKurumaInfo() {
        List<RcKurumaInfo> list = rcKurumaInfoDao.findDataRcKurumaInfo();
        return list;
    }

    /**
     * 查看备辊小车现役备辊
     */
    public List<DpdEntity> findlist(String data) {
        JRcKurumaInfo jrcKurumaInfo = JSON.parseObject(data, JRcKurumaInfo.class);
        JSONObject jsonObject = null;

        if (null != jrcKurumaInfo.getCondition()) {
            jsonObject = JSON.parseObject(jrcKurumaInfo.getCondition().toString());
        }

        Long mut = null;
        if (!StringUtils.isEmpty(jsonObject.get("mut"))) {
            mut = Long.valueOf(jsonObject.get("mut").toString());
        }

        List<RcKurumaInfo> list = new ArrayList<>();
        if (mut == 0L) {
            list = rcKurumaInfoDao.findDataRcKurumaInfoOnlyStandno("精轧工作辊");
        } else {
            list = rcKurumaInfoDao.findDataRcKurumaInfoOnlyStandno2("精轧工作辊");
        }
        List<DpdEntity> dpd = new ArrayList<>();
        for (RcKurumaInfo entity:list){
            String top = "TOP";
            String bot = "BOT";
            if(entity.getStandno().contains("立辊") || entity.getStandno().contains("锤头")){
                top = "OS";
                bot = "DS";
            }

            RcKurumaInfo rcT = new RcKurumaInfo();
            RcKurumaInfo rcB = new RcKurumaInfo();
            if (mut == 0L){
                rcT = rcKurumaInfoDao.findDataRcKurumaInfoByStandno(entity.getStandno(),top,"精轧工作辊");
                rcB = rcKurumaInfoDao.findDataRcKurumaInfoByStandno(entity.getStandno(),bot,"精轧工作辊");
            }else{
                rcT = rcKurumaInfoDao.findDataRcKurumaInfoByStandno2(entity.getStandno(),top,"精轧工作辊");
                rcB = rcKurumaInfoDao.findDataRcKurumaInfoByStandno2(entity.getStandno(),bot,"精轧工作辊");
            }
            String standno  = null;
            String standnoName  = null;
            if (!StringUtils.isEmpty(rcT) && !StringUtils.isEmpty(rcB)){
                standno  = rcT.getRoll_id() + "/" + rcB.getRoll_id();
                standnoName  ="上辊：" +rcT.getRoll_id() + "   " + "下辊："+rcB.getRoll_id();
                if (StringUtils.isEmpty(rcT.getRoll_id()) && StringUtils.isEmpty(rcB.getRoll_id())){
                    standno = null;
                    standnoName=null;
                }
            }
            DpdEntity d = new DpdEntity();
            d.setValueName(standnoName);
            d.setKey(entity.getStandno());
            d.setValue(standno);
            dpd.add(d);
        }
        return dpd;
    }
}
