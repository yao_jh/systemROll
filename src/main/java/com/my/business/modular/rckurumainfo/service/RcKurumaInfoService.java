package com.my.business.modular.rckurumainfo.service;

import com.my.business.modular.rckurumainfo.entity.RcKurumaInfo;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.dict.entity.DpdEntity;

import java.util.List;

/**
 * 换辊小车表——工作辊类接口服务类
 *
 * @author 生成器生成
 * @date 2020-10-05 16:33:29
 */
public interface RcKurumaInfoService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRcKurumaInfo(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRcKurumaInfoOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRcKurumaInfoMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRcKurumaInfo(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRcKurumaInfoByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRcKurumaInfoByIndocno(String data);

    /**
     * 查看记录
     */
    List<RcKurumaInfo> findDataRcKurumaInfo();

    /**
     * 备辊小车现役辊
     *
     * @param data 分页参数字符串
     */
    List<DpdEntity> findlist(String data);
}
