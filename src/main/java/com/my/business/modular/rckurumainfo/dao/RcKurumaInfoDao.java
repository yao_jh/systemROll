package com.my.business.modular.rckurumainfo.dao;

import com.my.business.modular.rckurumainfo.entity.RcKurumaInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 换辊小车表——工作辊类dao接口
 *
 * @author 生成器生成
 * @date 2020-10-05 16:33:29
 */
@Mapper
public interface RcKurumaInfoDao {

    /**
     * 添加记录
     *
     * @param rcKurumaInfo 对象实体
     */
    void insertDataRcKurumaInfo(RcKurumaInfo rcKurumaInfo);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRcKurumaInfoOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRcKurumaInfoMany(String value);

    /**
     * 修改记录
     *
     * @param rcKurumaInfo 对象实体
     */
    void updateDataRcKurumaInfo(RcKurumaInfo rcKurumaInfo);

    /**
     * 修改记录
     *
     * @param rcKurumaInfo 对象实体
     */
    void updateDataRcKurumaInfoByPre(RcKurumaInfo rcKurumaInfo);

    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RcKurumaInfo> findDataRcKurumaInfoByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRcKurumaInfoByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RcKurumaInfo findDataRcKurumaInfoByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RcKurumaInfo> findDataRcKurumaInfo();

    /**
     * 查看所有机架号
     *
     * @return 对象数据集合
     */
    List<RcKurumaInfo> findDataRcKurumaInfoOnlyStandno(@Param("type") String type);

    /**
     * 查看所有机架号
     *
     * @return 对象数据集合
     */
    List<RcKurumaInfo> findDataRcKurumaInfoOnlyStandno2(@Param("type") String type);

    /**
     * 查询单条信息
     * @param standno 机架号
     * @param roll_position 上机位置
     * @return 集合
     */
    RcKurumaInfo findDataRcKurumaInfoByStandno(@Param("standno") String standno,@Param("roll_position") String roll_position,@Param("type") String type);

    /**
     * 查询单条信息
     * @param standno 机架号
     * @param roll_position 上机位置
     * @return 集合
     */
    RcKurumaInfo findDataRcKurumaInfoByStandno2(@Param("standno") String standno,@Param("roll_position") String roll_position,@Param("type") String type);

    void deleteDataRcKurumaInfoByrollNo(@Param("standno")String standno, @Param("roll_position")String roll_position, @Param("roll_type")String roll_type);

    RcKurumaInfo findDataRcKurumaInfoByrollId(@Param("roll_no")String roll_no);
}
