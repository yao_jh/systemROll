package com.my.business.modular.rckurumainfo.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 换辊小车表——工作辊类实体类
 *
 * @author 生成器生成
 * @date 2020-10-05 16:33:29
 */
public class RcKurumaInfo extends BaseEntity {

    private Long indocno;  //
    private String standno;  //机架
    private String roll_id;  //辊号
    private String chock_os;  //座号OS
    private String chock_ds;  //座号DS
    private String roll_type;  //轧辊类型
    private String roll_position;  //上机位置
    private Float roll_diam;  //轧辊直径
    private Float ini_crown_max;  //最大凸度
    private Float ini_crown_min;  //最小凸度
    private String roll_mat;  //轧辊材质
    private Long hssflag;  //HSS标记位
    private Long hssusedcounter;  //HSS上线次数
    private Float hssofflinetime_tot;  //HSS上次下机到本次上机间隔时间
    private String spare;  //备用
    private Long flag;  //上下线标志位

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getStandno() {
        return this.standno;
    }

    public void setStandno(String standno) {
        this.standno = standno;
    }

    public String getRoll_id() {
        return this.roll_id;
    }

    public void setRoll_id(String roll_id) {
        this.roll_id = roll_id;
    }

    public String getChock_os() {
        return this.chock_os;
    }

    public void setChock_os(String chock_os) {
        this.chock_os = chock_os;
    }

    public String getChock_ds() {
        return this.chock_ds;
    }

    public void setChock_ds(String chock_ds) {
        this.chock_ds = chock_ds;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public String getRoll_position() {
        return this.roll_position;
    }

    public void setRoll_position(String roll_position) {
        this.roll_position = roll_position;
    }

    public Float getRoll_diam() {
        return this.roll_diam;
    }

    public void setRoll_diam(Float roll_diam) {
        this.roll_diam = roll_diam;
    }

    public Float getIni_crown_max() {
        return this.ini_crown_max;
    }

    public void setIni_crown_max(Float ini_crown_max) {
        this.ini_crown_max = ini_crown_max;
    }

    public Float getIni_crown_min() {
        return this.ini_crown_min;
    }

    public void setIni_crown_min(Float ini_crown_min) {
        this.ini_crown_min = ini_crown_min;
    }

    public String getRoll_mat() {
        return this.roll_mat;
    }

    public void setRoll_mat(String roll_mat) {
        this.roll_mat = roll_mat;
    }

    public Long getHssflag() {
        return this.hssflag;
    }

    public void setHssflag(Long hssflag) {
        this.hssflag = hssflag;
    }

    public Long getHssusedcounter() {
        return this.hssusedcounter;
    }

    public void setHssusedcounter(Long hssusedcounter) {
        this.hssusedcounter = hssusedcounter;
    }

    public Float getHssofflinetime_tot() {
        return this.hssofflinetime_tot;
    }

    public void setHssofflinetime_tot(Float hssofflinetime_tot) {
        this.hssofflinetime_tot = hssofflinetime_tot;
    }

    public String getSpare() {
        return this.spare;
    }

    public void setSpare(String spare) {
        this.spare = spare;
    }

    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }
}