package com.my.business.modular.rollgrindingannex.service.impl;

import com.my.business.modular.historyrollground.dao.HistoryRollGroundDao;
import com.my.business.modular.historyrollground.entity.HistoryRollGround;
import com.my.business.modular.historyrollground.entity.JHistoryRollGround;
import com.my.business.modular.rollgrinding.dao.RollGrindingBFDao;
import com.my.business.modular.rollgrinding.entity.RollGrindingBF;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.RollInformation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import lombok.extern.apachecommons.CommonsLog;

import com.my.business.modular.rollgrindingannex.entity.RollGrindingAnnex;
import com.my.business.modular.rollgrindingannex.entity.JRollGrindingAnnex;
import com.my.business.modular.rollgrinding.dao.RollGrindingDao;
import com.my.business.modular.rollgrinding.entity.RollGrinding;
import com.my.business.modular.rollgrindingannex.dao.RollGrindingAnnexDao;
import com.my.business.modular.rollgrindingannex.service.RollGrindingAnnexService;
import com.my.business.sys.common.entity.ResultData;

import javax.sound.midi.MidiChannel;

/**
* 磨削附件表接口具体实现类
* @author  生成器生成
* @date 2020-10-27 16:29:18
*/
@CommonsLog
@Service
public class RollGrindingAnnexServiceImpl implements RollGrindingAnnexService {
	
	private final static Log logger = LogFactory.getLog(RollGrindingAnnexServiceImpl.class);
	
	@Autowired
	private RollGrindingAnnexDao rollGrindingAnnexDao;
	
	@Autowired
	private RollGrindingBFDao rollGrindingBFDao;
	@Autowired
	private RollInformationDao rollInformationDao;
	@Autowired
	private HistoryRollGroundDao historyRollGroundDao;
	
	/**
     * 复制磨削实绩数据到磨削附件表
     * @param rollGring 磨削实绩对象
     */
	public ResultData copyRollGrindingBF(RollGrindingBF rollGringBF){
		try{
			JSONObject request = new JSONObject();
			request.put("body", rollGringBF);
			logger.debug("传递过来的数据为" + request.toJSONString());
			
			
			RollGrindingAnnex r = rollGrindingAnnexDao.findDataRollGrindingAnnexNull(rollGringBF.getRoll_no());
			if(r != null) {
				JSONObject request2 = new JSONObject();
				request2.put("body", r);
				logger.debug("根据辊号查出来的数据为" + request2.toJSONString());
				
				Long indocno = r.getIndocno();
				BeanUtils.copyProperties(rollGringBF, r);  //把前面属性赋值给后面对象
				r.setIndocno(indocno);
				r.setIstate(0L);
				JSONObject request3 = new JSONObject();
				request3.put("body", r);
				logger.debug("主键赋值后的的数据为" + request3.toJSONString());
				rollGrindingAnnexDao.updateDataRollGrindingAnnex(r);
			}else {
				logger.debug("没有该棍子的数据");
			}
            return ResultData.ResultDataSuccessSelf("复制数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("复制失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	public ResultData copydata(){
		try{
			List<RollGrindingBF> list = rollGrindingBFDao.findDataRollGrindingBF();
			for(RollGrindingBF entity : list) {
				RollGrindingAnnex r = new RollGrindingAnnex();
				BeanUtils.copyProperties(entity,r); 
				rollGrindingAnnexDao.insertDataRollGrindingAnnex(r);
			}
            return ResultData.ResultDataSuccessSelf("复制成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("复制失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollGrindingAnnex(String data,Long userId,String sname){
		try{
			JRollGrindingAnnex jrollGrindingAnnex = JSON.parseObject(data,JRollGrindingAnnex.class);
    		RollGrindingAnnex rollGrindingAnnex = jrollGrindingAnnex.getRollGrindingAnnex();
    		CodiUtil.newRecord(userId,sname,rollGrindingAnnex);
            rollGrindingAnnexDao.insertDataRollGrindingAnnex(rollGrindingAnnex);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollGrindingAnnexOne(Long indocno){
		try {
            rollGrindingAnnexDao.deleteDataRollGrindingAnnexOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollGrindingAnnexMany(String str_id) {
        try {
        	String sql = "delete roll_grinding_annex where indocno in(" + str_id +")";
            rollGrindingAnnexDao.deleteDataRollGrindingAnnexMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param data 对象实体
     */
	public ResultData updateDataRollGrindingAnnex(String data,Long userId,String sname){
		try {
			JRollGrindingAnnex jrollGrindingAnnex = JSON.parseObject(data,JRollGrindingAnnex.class);
    		RollGrindingAnnex rollGrindingAnnex = jrollGrindingAnnex.getRollGrindingAnnex();
        	CodiUtil.editRecord(userId,sname,rollGrindingAnnex);
            rollGrindingAnnexDao.updateDataRollGrindingAnnex(rollGrindingAnnex);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollGrindingAnnexByPage(String data) {
        try {
        	JRollGrindingAnnex jrollGrindingAnnex = JSON.parseObject(data, JRollGrindingAnnex.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jrollGrindingAnnex.getPageIndex();
        	Integer pageSize = jrollGrindingAnnex.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jrollGrindingAnnex.getCondition()){
    			jsonObject  = JSON.parseObject(jrollGrindingAnnex.getCondition().toString());
    		}
        	
        	 String roll_no = null;
             if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                 roll_no = jsonObject.get("roll_no").toString();
             }
             
             String roll_type = null;
             if (!StringUtils.isEmpty(jsonObject.get("roll_type"))) {
            	 roll_type = jsonObject.get("roll_type").toString();
             }

             Long roll_typeid = null;
             if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                 roll_typeid = Long.valueOf(jsonObject.get("roll_typeid").toString());
             }

             String machine_no = null;
             if (!StringUtils.isEmpty(jsonObject.get("machine_no"))) {
                 machine_no = jsonObject.get("machine_no").toString();
             }

             String grind_starttime = null;
             if (!StringUtils.isEmpty(jsonObject.get("grind_starttime"))) {
                 grind_starttime = jsonObject.get("grind_starttime").toString();
             }

             String dbegin = null;
             if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                 dbegin = jsonObject.get("dbegin").toString();
             }

             String dend = null;
             if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                 dend = jsonObject.get("dend").toString();
             }

             Long production_line_id = null;
             if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                 production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
             }
             
             String istate = null;
             if (!StringUtils.isEmpty(jsonObject.get("istate"))) {
            	 istate = jsonObject.get("istate").toString();
             }
    
    		List<RollGrindingAnnex> list = rollGrindingAnnexDao.findDataRollGrindingAnnexByPage((pageIndex - 1)*pageSize, pageSize, istate,roll_no, roll_typeid, dbegin, dend, machine_no, grind_starttime,production_line_id,roll_type);
    		Integer count = rollGrindingAnnexDao.findDataRollGrindingAnnexByPageSize(istate,roll_no, roll_typeid, dbegin, dend, machine_no, grind_starttime,production_line_id,roll_type);
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollGrindingAnnexByIndocno(String data) {
        try {
        	JRollGrindingAnnex jrollGrindingAnnex = JSON.parseObject(data, JRollGrindingAnnex.class); 
        	Long indocno = jrollGrindingAnnex.getIndocno();
        	
    		RollGrindingAnnex rollGrindingAnnex = rollGrindingAnnexDao.findDataRollGrindingAnnexByIndocno(indocno);
    		return ResultData.ResultDataSuccess(rollGrindingAnnex);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<RollGrindingAnnex> findDataRollGrindingAnnex(){
		List<RollGrindingAnnex> list = rollGrindingAnnexDao.findDataRollGrindingAnnex();
		return list;
	}

	/**
	 * 修改状态——待检测
	 * @param data 选中的数据
	 * @return
	 */
	@Override
	public ResultData updateState(String data) {
		try {
			JRollGrindingAnnex jrollGrindingAnnex = JSON.parseObject(data,JRollGrindingAnnex.class);
			RollGrindingAnnex rollGrindingAnnex = jrollGrindingAnnex.getRollGrindingAnnex();
			Long indocno = jrollGrindingAnnex.getIndocno();
			//查询基本信息
			RollInformation rollInformation = rollInformationDao.findDataRollInformationByRollNo(rollGrindingAnnex.getRoll_no());
			if (!StringUtils.isEmpty(rollInformation)){
				//更改基本信息表的状态
				rollInformationDao.updateDataRollInformationByRevoleve(rollGrindingAnnex.getRoll_no(),5L);
				//改变磨削附件表的状态
				RollGrindingAnnex rgax = rollGrindingAnnexDao.findDataRollGrindingAnnexByIndocno(indocno);
				rgax.setIstate(2L);
				rollGrindingAnnexDao.updateDataRollGrindingAnnex(rgax);
				return ResultData.ResultDataSuccessSelf("更新成功", null);
			}else {
				return ResultData.ResultDataFaultSelf("更新失败,基本信息表中没有次辊数据，辊号为:"+rollGrindingAnnex.getRoll_no(), null);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.ResultDataFaultSelf("更新失败,失败信息为" + e.getMessage(), null);
		}
	}

	/**
	 * 修改状态——待上机
	 * @param data 选中的数据
	 * @return
	 */
	@Override
	public ResultData updateA(String data) {
		try {
			JHistoryRollGround jHistoryRollGround = JSON.parseObject(data,JHistoryRollGround.class);
			HistoryRollGround historyRollGround = jHistoryRollGround.getHistoryRollGround();
			if (!StringUtils.isEmpty(historyRollGround)) {
				if (!StringUtils.isEmpty(historyRollGround.getIndocno())){
					//弹窗中选中的磨削实际HistoryRollGround的主键
					Long hindocno = historyRollGround.getIndocno();
					//选中的待上机的磨削附件表数据的主键，附带在这个实体类里而已
					Long gindocno = jHistoryRollGround.getIndocno();
					//查找磨削实绩数据
					HistoryRollGround hs = historyRollGroundDao.findDataHistoryRollGroundByIndocno(hindocno);
					//查找磨削附件数据
					RollGrindingAnnex rgax = rollGrindingAnnexDao.findDataRollGrindingAnnexByIndocno(gindocno);
					//将磨削实绩数据赋值给磨削附件顺带更改状态
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					rgax.setRoll_no(hs.getRoll_nb());
					rgax.setMaterial(hs.getRoll_material());
//					rgax.setRoll_type(hs.getRoll_type());
					if (hs.getStart_date_time() != null) {
						rgax.setGrind_starttime(sdf.format(hs.getStart_date_time()));
					}
					if (hs.getEnd_date_time() != null) {
						rgax.setGrind_endtime(sdf.format(hs.getEnd_date_time()));
					} else {
						rgax.setGrind_endtime(null);
					}
					rgax.setMachine_no(String.valueOf(hs.getGrinder_no()));
					rgax.setBefore_diameter(Double.valueOf(String.valueOf(hs.getDiam_before_mid())));
					rgax.setAfter_diameter(Double.valueOf(String.valueOf(hs.getPresent_diam_mid())));
					rgax.setDeviation(Double.valueOf(String.valueOf(hs.getProfile())));
					rgax.setDiametermax(Double.valueOf(String.valueOf(hs.getPresent_diam_max())));
					rgax.setDiametermin(Double.valueOf(String.valueOf(hs.getPresent_diam_min())));
					rgax.setTaper(Double.valueOf(String.valueOf(hs.getTaper())));
					rgax.setRoundness(Double.valueOf(String.valueOf(hs.getRoundness())));
					if (hs.getCrack() != null) {
						rgax.setCrack(Double.valueOf(String.valueOf(hs.getCrack())));
					}
					if (hs.getBruise() != null) {
						rgax.setHidden_flaws((double) hs.getBruise());
					}
					rgax.setProduction_line_id(1L);
					rgax.setProduction_line("2250");
					rgax.setIstate(0L);
					//更新磨削附件数据
					rollGrindingAnnexDao.updateDataRollGrindingAnnex(rgax);
					//更新基本信息表状态
					rollInformationDao.updateDataRollInformationByRevoleve(rgax.getRoll_no(),4L);
					return ResultData.ResultDataSuccessSelf("更新成功", null);
				}else {
					return ResultData.ResultDataFaultSelf("更新失败,未选择实绩数据", null);
				}
			}else {
				return ResultData.ResultDataFaultSelf("更新失败,未选择实绩数据", null);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.ResultDataFaultSelf("更新失败,失败信息为" + e.getMessage(), null);
		}
	}

	;
}
