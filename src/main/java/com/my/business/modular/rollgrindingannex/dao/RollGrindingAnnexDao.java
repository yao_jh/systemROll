package com.my.business.modular.rollgrindingannex.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.rollgrindingannex.entity.RollGrindingAnnex;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 磨削附件表dao接口
 * @author  生成器生成
 * @date 2020-10-27 16:29:18
 * */
@Mapper
public interface RollGrindingAnnexDao {

	/**
	 * 添加记录
	 * @param rollGrindingAnnex  对象实体
	 * */
	public void insertDataRollGrindingAnnex(RollGrindingAnnex rollGrindingAnnex);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataRollGrindingAnnexOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataRollGrindingAnnexMany(String value);
	
	/**
	 * 修改记录
	 * @param rollGrindingAnnex  对象实体
	 * */
	public void updateDataRollGrindingAnnex(RollGrindingAnnex rollGrindingAnnex);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<RollGrindingAnnex> findDataRollGrindingAnnexByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("istate") String istate,@Param("roll_no") String roll_no, @Param("roll_typeid") Long roll_typeid, @Param("dbegin") String dbegin, @Param("dend") String dend, @Param("machine_no") String machine_no, @Param("grind_starttime") String grind_starttime, @Param("production_line_id") Long production_line_id,@Param("roll_type") String roll_type);
    
    /**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public RollGrindingAnnex findDataRollGrindingAnnexNull(@Param("roll_no") String roll_no);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataRollGrindingAnnexByPageSize(@Param("istate") String istate,@Param("roll_no") String roll_no, @Param("roll_typeid") Long roll_typeid, @Param("dbegin") String dbegin, @Param("dend") String dend, @Param("machine_no") String machine_no, @Param("grind_starttime") String grind_starttime, @Param("production_line_id") Long production_line_id,@Param("roll_type") String roll_type);
    
    /**
     * 查询最大主键值
     * @return 对象数据集合
     */
    public Long findMaxIndocno();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public RollGrindingAnnex findDataRollGrindingAnnexByIndocno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<RollGrindingAnnex> findDataRollGrindingAnnex();
	
}
