package com.my.business.modular.rollgrindingannex.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
* 磨削附件表实体类
* @author  生成器生成
* @date 2020-10-27 16:29:18
*/
public class RollGrindingAnnex extends BaseEntity{
		
	private Long indocno;  //主键
	private String roll_no;  //辊号
    private Long factory_id;
    private String factory;  //生产厂家
    private Long material_id;
    private String material;  //材质
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private Long frame_noid;  //机架号id
    private String frame_no;  //机架号
	private String grind_starttime;  //磨削开始时间
	private String grind_endtime;  //磨削结束时间
	private String machine_no;  //磨床号
	private Double before_diameter;  //磨前直径
	private Double after_diameter;  //磨后直径
	private Double deviation;  //辊形偏差
	private Double diametermax;  //最大直径
	private Double diametermin;  //最小直径
	private Double taper;  //锥度
	private Double roundness;  //圆度
	private Double crack;  //裂纹
	private Double hidden_flaws;  //暗伤
	private Double qualifiednum;  //合格点数
	private String sclass;  //班
	private String sgroup;  //班组
	private String operat_user;  //操作人姓名
	private Long operat_userid;  //操作人id
	private String snote;  //备注
	private String production_line;  //产线(1-2250,2-1580)
	private Long production_line_id;  //
	private Long if_wave;  //是否超声波检测
	private Long if_hardness;  //是否硬度检测
    
    
	public void setIndocno(Long indocno){
	    this.indocno = indocno;
	}
	public Long getIndocno(){
	    return this.indocno;
	}
	public void setRoll_no(String roll_no){
	    this.roll_no = roll_no;
	}
	public String getRoll_no(){
	    return this.roll_no;
	}
	public void setFactory(String factory){
	    this.factory = factory;
	}
	public String getFactory(){
	    return this.factory;
	}
	public void setMaterial(String material){
	    this.material = material;
	}
	public String getMaterial(){
	    return this.material;
	}
	public void setRoll_typeid(Long roll_typeid){
	    this.roll_typeid = roll_typeid;
	}
	public Long getRoll_typeid(){
	    return this.roll_typeid;
	}
	public void setRoll_type(String roll_type){
	    this.roll_type = roll_type;
	}
	public String getRoll_type(){
	    return this.roll_type;
	}
	public void setGrind_starttime(String grind_starttime){
	    this.grind_starttime = grind_starttime;
	}
	public String getGrind_starttime(){
	    return this.grind_starttime;
	}
	public void setGrind_endtime(String grind_endtime){
	    this.grind_endtime = grind_endtime;
	}
	public String getGrind_endtime(){
	    return this.grind_endtime;
	}
	public void setMachine_no(String machine_no){
	    this.machine_no = machine_no;
	}
	public String getMachine_no(){
	    return this.machine_no;
	}
	public void setBefore_diameter(Double before_diameter){
	    this.before_diameter = before_diameter;
	}
	public Double getBefore_diameter(){
	    return this.before_diameter;
	}
	public void setAfter_diameter(Double after_diameter){
	    this.after_diameter = after_diameter;
	}
	public Double getAfter_diameter(){
	    return this.after_diameter;
	}
	public void setDeviation(Double deviation){
	    this.deviation = deviation;
	}
	public Double getDeviation(){
	    return this.deviation;
	}
	public void setDiametermax(Double diametermax){
	    this.diametermax = diametermax;
	}
	public Double getDiametermax(){
	    return this.diametermax;
	}
	public void setDiametermin(Double diametermin){
	    this.diametermin = diametermin;
	}
	public Double getDiametermin(){
	    return this.diametermin;
	}
	public void setTaper(Double taper){
	    this.taper = taper;
	}
	public Double getTaper(){
	    return this.taper;
	}
	public void setRoundness(Double roundness){
	    this.roundness = roundness;
	}
	public Double getRoundness(){
	    return this.roundness;
	}
	public void setCrack(Double crack){
	    this.crack = crack;
	}
	public Double getCrack(){
	    return this.crack;
	}
	public void setHidden_flaws(Double hidden_flaws){
	    this.hidden_flaws = hidden_flaws;
	}
	public Double getHidden_flaws(){
	    return this.hidden_flaws;
	}
	public void setQualifiednum(Double qualifiednum){
	    this.qualifiednum = qualifiednum;
	}
	public Double getQualifiednum(){
	    return this.qualifiednum;
	}
	public void setSclass(String sclass){
	    this.sclass = sclass;
	}
	public String getSclass(){
	    return this.sclass;
	}
	public void setSgroup(String sgroup){
	    this.sgroup = sgroup;
	}
	public String getSgroup(){
	    return this.sgroup;
	}
	public void setOperat_user(String operat_user){
	    this.operat_user = operat_user;
	}
	public String getOperat_user(){
	    return this.operat_user;
	}
	public void setOperat_userid(Long operat_userid){
	    this.operat_userid = operat_userid;
	}
	public Long getOperat_userid(){
	    return this.operat_userid;
	}
	public void setSnote(String snote){
	    this.snote = snote;
	}
	public String getSnote(){
	    return this.snote;
	}
	public void setProduction_line(String production_line){
	    this.production_line = production_line;
	}
	public String getProduction_line(){
	    return this.production_line;
	}
	public void setProduction_line_id(Long production_line_id){
	    this.production_line_id = production_line_id;
	}
	public Long getProduction_line_id(){
	    return this.production_line_id;
	}
	public void setIf_wave(Long if_wave){
	    this.if_wave = if_wave;
	}
	public Long getIf_wave(){
	    return this.if_wave;
	}
	public void setIf_hardness(Long if_hardness){
	    this.if_hardness = if_hardness;
	}
	public Long getIf_hardness(){
	    return this.if_hardness;
	}
	public Long getFactory_id() {
		return factory_id;
	}
	public void setFactory_id(Long factory_id) {
		this.factory_id = factory_id;
	}
	public Long getMaterial_id() {
		return material_id;
	}
	public void setMaterial_id(Long material_id) {
		this.material_id = material_id;
	}
	public Long getFrame_noid() {
		return frame_noid;
	}
	public void setFrame_noid(Long frame_noid) {
		this.frame_noid = frame_noid;
	}
	public String getFrame_no() {
		return frame_no;
	}
	public void setFrame_no(String frame_no) {
		this.frame_no = frame_no;
	}
}