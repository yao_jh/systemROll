package com.my.business.modular.rollgrindingannex.service;

import com.my.business.modular.rollgrinding.entity.RollGrindingBF;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.modular.rollgrinding.entity.RollGrinding;
import com.my.business.modular.rollgrindingannex.entity.RollGrindingAnnex;
import java.util.List;

/**
* 磨削附件表接口服务类
* @author  生成器生成
* @date 2020-10-27 16:29:18
*/
public interface RollGrindingAnnexService {
	
	/**
     * 复制磨削实绩数据到磨削附件表
     * @param rollGring 磨削实绩对象
     */
	public ResultData copyRollGrindingBF(RollGrindingBF rollGringBF);
	
	/**
     *全部复制磨削实际数据到附件表
     */
	public ResultData copydata();

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollGrindingAnnex(String data,Long userId,String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollGrindingAnnexOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollGrindingAnnexMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataRollGrindingAnnex(String data,Long userId,String sname);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollGrindingAnnexByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataRollGrindingAnnexByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<RollGrindingAnnex> findDataRollGrindingAnnex();

	/**
	 * 修改状态——待检测
	 * @param data
	 * @return
	 */
    ResultData updateState(String data);

	/**
	 * 修改状态——待上机
	 * @param data json字符串
	 */
	ResultData updateA(String data);
}
