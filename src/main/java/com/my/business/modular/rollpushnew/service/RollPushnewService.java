package com.my.business.modular.rollpushnew.service;

import com.my.business.modular.rollpushnew.entity.RollPushnew;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 消息推送接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-03 16:52:14
 */
public interface RollPushnewService {
	
	/**
     * 磨削取消按钮
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData cancel(String data, Long userId, String sname);
	
	
	/**
     * 自定义添加记录
     *
     * @param modular  模块编码
     * @param rollPushnew
     */
    void insertDataRollPushnewSelf(String modular,RollPushnew rollPushnew);

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollPushnew(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollPushnewOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollPushnewMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollPushnew(String data, Long userId, String sname);
    
    /**
     * 修改状态记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollPushnewFinish(String data);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPushnewByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPushnewByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollPushnew> findDataRollPushnew();
}
