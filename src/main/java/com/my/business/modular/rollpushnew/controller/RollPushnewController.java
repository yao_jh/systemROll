package com.my.business.modular.rollpushnew.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollpushnew.entity.JRollPushnew;
import com.my.business.modular.rollpushnew.entity.RollPushnew;
import com.my.business.modular.rollpushnew.service.RollPushnewService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 消息推送控制器层
 *
 * @author 生成器生成
 * @date 2020-08-03 16:52:14
 */
@RestController
@RequestMapping("/rollPushnew")
public class RollPushnewController {

    @Autowired
    private RollPushnewService rollPushnewService;
    
    
    /**
     * 磨削取消按钮接口
     *
     * @param userId 用户id
     * @param sname  用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/cancel"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData cancel(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollPushnewService.cancel(data, userId, sname);
    }
    

    /**
     * 添加记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollPushnew(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollPushnewService.insertDataRollPushnew(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollPushnewOne(@RequestBody String data) {
        try {
            JRollPushnew jrollPushnew = JSON.parseObject(data, JRollPushnew.class);
            return rollPushnewService.deleteDataRollPushnewOne(jrollPushnew.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollPushnew jrollPushnew = JSON.parseObject(data, JRollPushnew.class);
            return rollPushnewService.deleteDataRollPushnewMany(jrollPushnew.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollPushnew(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollPushnewService.updateDataRollPushnew(data, userId, CodiUtil.returnLm(sname));
    }
    
    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/updatefinish"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollPushnewFinishi(@RequestBody String data) {
        return rollPushnewService.updateDataRollPushnewFinish(data);
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollPushnewByPage(@RequestBody String data) {
        return rollPushnewService.findDataRollPushnewByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollPushnewByIndocno(@RequestBody String data) {
        return rollPushnewService.findDataRollPushnewByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollPushnew> findDataRollPushnew() {
        return rollPushnewService.findDataRollPushnew();
    }

}
