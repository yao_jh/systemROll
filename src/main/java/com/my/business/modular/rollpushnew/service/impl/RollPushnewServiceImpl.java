package com.my.business.modular.rollpushnew.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollcancel.dao.RollCancelDao;
import com.my.business.modular.rollcancel.entity.RollCancel;
import com.my.business.modular.rollgrindingannex.dao.RollGrindingAnnexDao;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollpushnew.dao.RollPushnewDao;
import com.my.business.modular.rollpushnew.entity.JRollPushnew;
import com.my.business.modular.rollpushnew.entity.RollPushnew;
import com.my.business.modular.rollpushnew.service.RollPushnewService;
import com.my.business.sys.common.dao.CommonDao;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.workflow.wokrupdatetable.dao.WokrUpdatetableDao;
import com.my.business.workflow.wokrupdatetable.entity.WokrUpdatetable;
import com.my.business.workflow.workflowdetail.dao.WorkFlowdetailDao;
import com.my.business.workflow.workflowdetail.entity.WorkFlowdetail;
import com.my.business.workflow.workflowdetailperform.dao.WorkFlowdetailPerformDao;
import com.my.business.workflow.workflowdetailperform.entity.WorkFlowdetailPerform;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 消息推送接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-03 16:52:14
 */
@Service
public class RollPushnewServiceImpl implements RollPushnewService {

    @Autowired
    private RollPushnewDao rollPushnewDao;
    
    @Autowired
    private RollGrindingAnnexDao rollGrindingAnnexDao;
    
    @Autowired
	private WorkFlowdetailDao workFlowdetailDao;
    
    @Autowired
	private CommonDao commonDao;
    
    @Autowired
	private WokrUpdatetableDao wokrUpdatetableDao;
    
    @Autowired
    private RollInformationDao rollInformationDao;
    
    @Autowired
	private RollCancelDao rollCancelDao;
    
    /**
     * 磨削取消按钮
     *
     * @param modular  模块编码
     * @param rollPushnew
     */
    public ResultData cancel(String data, Long userId, String sname) {
        try {
        	JRollPushnew jrollPushnew = JSON.parseObject(data, JRollPushnew.class);
            RollPushnew rollPushnew = jrollPushnew.getRollPushnew();
            RollPushnew rollPushnew_find = rollPushnewDao.findDataRollPushnewByPushTimeMax(rollPushnew.getRoll_no(), String.valueOf(rollPushnew.getMachine_no()));
            if(rollPushnew_find != null) {
            	rollPushnewDao.deleteDataRollPushnewOne(rollPushnew_find.getIndocno());
            	if(null != rollPushnew_find.getEntity_id()) {
            		rollGrindingAnnexDao.deleteDataRollGrindingAnnexOne(rollPushnew_find.getEntity_id());
            	}
            	
            	RollCancel rollCancel = new RollCancel();
            	rollCancel.setItype(1L);
            	rollCancel.setBack_reason(jrollPushnew.getBack_reason());
        		CodiUtil.newRecord(userId,sname,rollCancel);
                rollCancelDao.insertDataRollCancel(rollCancel);
            	
            	rollInformationDao.updateDataRollInformationByRevoleve(rollPushnew.getRoll_no(), 6L);
            }else {
            	 return ResultData.ResultDataFaultSelf("该记录辊号和磨床号匹配不正常,或者该记录已经磨削完成,请确认无误后再取消", null);
            }
            
            return ResultData.ResultDataSuccessSelf("取消成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("取消失败，错误信息为" + e.getMessage(), null);
        }
    }
    
    /**
     * 自定义添加记录
     *
     * @param modular  模块编码
     * @param rollPushnew
     */
    public void insertDataRollPushnewSelf(String modular,RollPushnew rollPushnew) {
        try {
        	rollPushnew.setModular_no(modular);
            rollPushnewDao.insertDataRollPushnew(rollPushnew);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollPushnew(String data, Long userId, String sname) {
        try {
            JRollPushnew jrollPushnew = JSON.parseObject(data, JRollPushnew.class);
            RollPushnew rollPushnew = jrollPushnew.getRollPushnew();
            CodiUtil.newRecord(userId, sname, rollPushnew);
            rollPushnewDao.insertDataRollPushnew(rollPushnew);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollPushnewOne(Long indocno) {
        try {
            rollPushnewDao.deleteDataRollPushnewOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollPushnewMany(String str_id) {
        try {
            String sql = "delete roll_pushnew where indocno in(" + str_id + ")";
            rollPushnewDao.deleteDataRollPushnewMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataRollPushnew(String data, Long userId, String sname) {
        try {
            JRollPushnew jrollPushnew = JSON.parseObject(data, JRollPushnew.class);
            RollPushnew rollPushnew = jrollPushnew.getRollPushnew();
            CodiUtil.editRecord(userId, sname, rollPushnew);
            rollPushnewDao.updateDataRollPushnew(rollPushnew);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }
    
    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataRollPushnewFinish(String data) {
        try {
            JRollPushnew jrollPushnew = JSON.parseObject(data, JRollPushnew.class);
            Long indocno = jrollPushnew.getIndocno();
            rollPushnewDao.updateDataRollPushnewFinish(indocno);
            
            RollPushnew pushnew = rollPushnewDao.findDataRollPushnewByIndocno(indocno);
            
            WorkFlowdetail wts = workFlowdetailDao.findDataWorkFlowdetailByNodeid(pushnew.getNodeid());
            if(wts != null) {
            	updateTable(wts.getIndocno(),pushnew.getRoll_no());
            }
            
            return ResultData.ResultDataSuccessSelf("修改状态成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改状态失败,失败信息为" + e.getMessage(), null);
        }
    }
    
    /***
	 * 根据主键和轧辊号更新主键关联的表的状态
	 * @param indocno  步骤id
	 * @param roll_no
	 */
	public void updateTable(Long indocno,String roll_no) {
		List<WokrUpdatetable> list = wokrUpdatetableDao.findDataWokrUpdatetableByIlinkno(indocno);
		for(WokrUpdatetable w : list) {
			String sql = "update " + w.getTable_name() + " set " + w.getField_no() + " = " + w.getUpdate_value() + " where roll_no = '" + roll_no  + "'";
			commonDao.findOneData(sql);
		}
	}

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPushnewByPage(String data) {
        try {
            JRollPushnew jrollPushnew = JSON.parseObject(data, JRollPushnew.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollPushnew.getPageIndex();
            Integer pageSize = jrollPushnew.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollPushnew.getCondition()) {
                jsonObject = JSON.parseObject(jrollPushnew.getCondition().toString());
            }

            List<RollPushnew> list = rollPushnewDao.findDataRollPushnewByPage((pageIndex - 1) * pageSize, pageSize);
            Integer count = rollPushnewDao.findDataRollPushnewByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPushnewByIndocno(String data) {
        try {
            JRollPushnew jrollPushnew = JSON.parseObject(data, JRollPushnew.class);
            Long indocno = jrollPushnew.getIndocno();

            RollPushnew rollPushnew = rollPushnewDao.findDataRollPushnewByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollPushnew);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollPushnew> findDataRollPushnew() {
        List<RollPushnew> list = rollPushnewDao.findDataRollPushnew();
        return list;
    }

}
