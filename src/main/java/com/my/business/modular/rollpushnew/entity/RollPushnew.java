package com.my.business.modular.rollpushnew.entity;

import com.my.business.modular.rolldimensionorder.entity.RollDimensionorder;
import com.my.business.modular.rollorderconfig.entity.RollOrderconfig;
import com.my.business.modular.rollorderconfigdetail.entity.RollOrderconfigdetail;
import com.my.business.sys.common.entity.BaseEntity;

import java.util.List;
import java.util.Map;

/**
 * 消息推送实体类
 *
 * @author 生成器生成
 * @date 2020-08-03 16:52:14
 */
public class RollPushnew extends BaseEntity {

    private Long indocno;  //主键
    private String production_line;  //产线
    private Long production_line_id; //产线id
    private String roll_no;  //辊号
    private Long factory_id; //生产商id
    private String factory;  //生产厂家
    private Long material_id;  //材质id
    private String material;  //材质
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private Long frame_noid;  //机架号id
    private String frame_no;  //机架号
    private Long push_userid;  //推送人id
    private String push_user;  //推送人
    private String pushtime;  //推送时间
    private String pushmodular;  //推送模块编码
    private Long get_userid;  //接收人id
    private String get_user;  //推送人
    private Long get_groupid; //推送组id
    private String get_group; //推送组名称
    private String modular_no;  //模块编码
    private Long area_id;  //地点id
    private String area_name;  //地点名称
    private String new_note;  //消息
    private Long ifinish;  //是否完成
    private String starttime;  //开始执行时间
    private String endtime;  //结束执行时间
    private Long if_delay;  //是否延期
    private String order_no;   //对应工单表中的数据编码
    private String method_url;   //方法接口地址
    private String order_entity;  //工单对应的实体类
    private String tourl; //跳转url
    private String nodeid;   //步骤id
    private String perform_no; //执行步骤表中定位步骤的组号
    private Long flowid;  //对应工作流主键
    private Long entity_id;  //关联实体id
    private Long machine_no;  //磨床号
    
    private String maxtime;


    //以下属性为工单查询使用
    private Long order_indocno;  //对应工单配置主表主键
    private RollOrderconfig rollOrderconfig;  //工单
    private List<RollOrderconfigdetail> order_detailList;
    
    private Map<String,String> workparam_map;

    //以下属性为填写工单使用
    private RollDimensionorder rollDimensionorder;

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public Long getRoll_typeid() {
        return this.roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public Long getPush_userid() {
        return this.push_userid;
    }

    public void setPush_userid(Long push_userid) {
        this.push_userid = push_userid;
    }

    public String getPush_user() {
        return this.push_user;
    }

    public void setPush_user(String push_user) {
        this.push_user = push_user;
    }

    public String getPushtime() {
        return this.pushtime;
    }

    public void setPushtime(String pushtime) {
        this.pushtime = pushtime;
    }

    public String getPushmodular() {
        return this.pushmodular;
    }

    public void setPushmodular(String pushmodular) {
        this.pushmodular = pushmodular;
    }

    public Long getGet_userid() {
        return this.get_userid;
    }

    public void setGet_userid(Long get_userid) {
        this.get_userid = get_userid;
    }

    public String getGet_user() {
        return this.get_user;
    }

    public void setGet_user(String get_user) {
        this.get_user = get_user;
    }

    public String getModular_no() {
        return this.modular_no;
    }

    public void setModular_no(String modular_no) {
        this.modular_no = modular_no;
    }

    public Long getArea_id() {
        return this.area_id;
    }

    public void setArea_id(Long area_id) {
        this.area_id = area_id;
    }

    public String getArea_name() {
        return this.area_name;
    }

    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }

    public String getNew_note() {
        return this.new_note;
    }

    public void setNew_note(String new_note) {
        this.new_note = new_note;
    }

    public Long getIfinish() {
        return this.ifinish;
    }

    public void setIfinish(Long ifinish) {
        this.ifinish = ifinish;
    }

    public String getStarttime() {
        return this.starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return this.endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public Long getIf_delay() {
        return this.if_delay;
    }

    public void setIf_delay(Long if_delay) {
        this.if_delay = if_delay;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getMethod_url() {
        return method_url;
    }

    public void setMethod_url(String method_url) {
        this.method_url = method_url;
    }

    public String getOrder_entity() {
        return order_entity;
    }

    public void setOrder_entity(String order_entity) {
        this.order_entity = order_entity;
    }

    public List<RollOrderconfigdetail> getOrder_detailList() {
        return order_detailList;
    }

    public void setOrder_detailList(List<RollOrderconfigdetail> order_detailList) {
        this.order_detailList = order_detailList;
    }

    public RollDimensionorder getRollDimensionorder() {
        return rollDimensionorder;
    }

    public void setRollDimensionorder(RollDimensionorder rollDimensionorder) {
        this.rollDimensionorder = rollDimensionorder;
    }

    public RollOrderconfig getRollOrderconfig() {
        return rollOrderconfig;
    }

    public void setRollOrderconfig(RollOrderconfig rollOrderconfig) {
        this.rollOrderconfig = rollOrderconfig;
    }

    public Long getOrder_indocno() {
        return order_indocno;
    }

    public void setOrder_indocno(Long order_indocno) {
        this.order_indocno = order_indocno;
    }

    public Long getGet_groupid() {
        return get_groupid;
    }

    public void setGet_groupid(Long get_groupid) {
        this.get_groupid = get_groupid;
    }

    public String getGet_group() {
        return get_group;
    }

    public void setGet_group(String get_group) {
        this.get_group = get_group;
    }

    public String getTourl() {
        return tourl;
    }

    public void setTourl(String tourl) {
        this.tourl = tourl;
    }

    public String getNodeid() {
        return nodeid;
    }

    public void setNodeid(String nodeid) {
        this.nodeid = nodeid;
    }

	public String getPerform_no() {
		return perform_no;
	}

	public void setPerform_no(String perform_no) {
		this.perform_no = perform_no;
	}

	public Long getFlowid() {
		return flowid;
	}

	public void setFlowid(Long flowid) {
		this.flowid = flowid;
	}

	public Map<String, String> getWorkparam_map() {
		return workparam_map;
	}

	public void setWorkparam_map(Map<String, String> workparam_map) {
		this.workparam_map = workparam_map;
	}

	public String getProduction_line() {
		return production_line;
	}

	public void setProduction_line(String production_line) {
		this.production_line = production_line;
	}

	public Long getProduction_line_id() {
		return production_line_id;
	}

	public void setProduction_line_id(Long production_line_id) {
		this.production_line_id = production_line_id;
	}

	public String getFactory() {
		return factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getRoll_type() {
		return roll_type;
	}

	public void setRoll_type(String roll_type) {
		this.roll_type = roll_type;
	}

	public Long getFrame_noid() {
		return frame_noid;
	}

	public void setFrame_noid(Long frame_noid) {
		this.frame_noid = frame_noid;
	}

	public String getFrame_no() {
		return frame_no;
	}

	public void setFrame_no(String frame_no) {
		this.frame_no = frame_no;
	}

	public Long getEntity_id() {
		return entity_id;
	}

	public void setEntity_id(Long entity_id) {
		this.entity_id = entity_id;
	}

	public Long getFactory_id() {
		return factory_id;
	}

	public void setFactory_id(Long factory_id) {
		this.factory_id = factory_id;
	}

	public Long getMaterial_id() {
		return material_id;
	}

	public void setMaterial_id(Long material_id) {
		this.material_id = material_id;
	}

	public Long getMachine_no() {
		return machine_no;
	}

	public void setMachine_no(Long machine_no) {
		this.machine_no = machine_no;
	}

	public String getMaxtime() {
		return maxtime;
	}

	public void setMaxtime(String maxtime) {
		this.maxtime = maxtime;
	}
}