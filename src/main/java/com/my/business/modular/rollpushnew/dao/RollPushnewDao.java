package com.my.business.modular.rollpushnew.dao;

import com.my.business.modular.rollpushnew.entity.RollPushnew;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 消息推送dao接口
 *
 * @author 生成器生成
 * @date 2020-08-03 16:52:14
 */
@Mapper
public interface RollPushnewDao {

    /**
     * 添加记录
     *
     * @param rollPushnew 对象实体
     */
    void insertDataRollPushnew(RollPushnew rollPushnew);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollPushnewOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollPushnewMany(String value);

    /**
     * 修改记录
     *
     * @param rollPushnew 对象实体
     */
    void updateDataRollPushnew(RollPushnew rollPushnew);
    
    /**
     * 修改状态记录
     *
     * @param rollPushnew 对象实体
     */
    void updateDataRollPushnewFinish(@Param("indocno") Long indocno);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollPushnew> findDataRollPushnewByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollPushnewByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollPushnew findDataRollPushnewByIndocno(@Param("indocno") Long indocno);
    
    /***
     * 根据辊号,磨床号查询当前最大推送时间的数据
     * @param indocno 用户id
     * @return
     */
    RollPushnew findDataRollPushnewByPushTimeMax(@Param("roll_no") String roll_no,@Param("machine_no") String machine_no);

    /***
     * 根据接收人id查询所有信息
     * @param indocno 用户id
     * @return
     */
    List<RollPushnew> findDataRollPushnewByUserId(@Param("getUserid") Long getUserid);

    /***
     * 根据接收人id查询未完成的信息
     * @param indocno 用户id
     * @return
     */
    List<RollPushnew> findNotFinishRollPushnewByUserId(@Param("getUserid") Long getUserid);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollPushnew> findDataRollPushnew();

    /**
     * 根据模块编码和是否完成查询记录
     * @param modular_no 模块编码
     * @param ifinish 是否已完成
     * @param machine_no 磨床号
     * @param getUserid 接收人id
     * @return 数据集合
     */
    List<RollPushnew> findDataRollPushnewByModularNo(@Param("modular_no")String modular_no,@Param("ifinish") Long ifinish,@Param("machine_no") Long machine_no,@Param("getUserid") Long getUserid);

    /***
     * 同上，但是只显示万能磨床
     * @param modular_no
     * @param ifinish
     * @param machine_no
     * @param getUserid
     * @return
     */
    List<RollPushnew> findDataRollPushnewByModularNoWn(@Param("modular_no")String modular_no,@Param("ifinish") Long ifinish,@Param("roleid") Long roleid);
    
    /***
     * 同上，但是只显示3,4,5,6号磨床
     * @param modular_no
     * @param ifinish
     * @param machine_no
     * @param getUserid
     * @return
     */
    List<RollPushnew> findDataRollPushnewByModularNoOnetoFour(@Param("modular_no")String modular_no,@Param("ifinish") Long ifinish,@Param("roleid") Long roleid);

}
