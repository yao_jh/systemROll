package com.my.business.modular.rollpushnew.entity;

import com.my.business.sys.common.entity.JCommon;

/**
 * 消息推送json的实体类
 *
 * @author 生成器生成
 * @date 2020-08-03 16:52:14
 */
public class JRollPushnew extends JCommon {

    private Integer pageIndex; // 第几页
    private Integer pageSize; // 每页多少数据
    private Object condition; // 查询条件
    private RollPushnew rollPushnew;   //对应模块的实体类
    private Long indocno;
    private String str_indocno;
    private String role_ids;
    
    private String back_reason;  //取消原因
    
    public String getBack_reason() {
		return back_reason;
	}

	public void setBack_reason(String back_reason) {
		this.back_reason = back_reason;
	}

	public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Object getCondition() {
        return condition;
    }

    public void setCondition(Object condition) {
        this.condition = condition;
    }

    public RollPushnew getRollPushnew() {
        return rollPushnew;
    }

    public void setRollPushnew(RollPushnew rollPushnew) {
        this.rollPushnew = rollPushnew;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getStr_indocno() {
        return str_indocno;
    }

    public void setStr_indocno(String str_indocno) {
        this.str_indocno = str_indocno;
    }

    public String getRole_ids() {
        return role_ids;
    }

    public void setRole_ids(String role_ids) {
        this.role_ids = role_ids;
    }
}