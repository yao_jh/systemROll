package com.my.business.modular.rollcoolorder.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.rollcoolorder.entity.RollCoolorder;
import com.my.business.modular.rollcoolorder.entity.RollCoolorderList;
import com.my.business.modular.rollcoolorder.entity.JRollCoolorder;
import com.my.business.modular.rollcooling.dao.RollCoolingDao;
import com.my.business.modular.rollcooling.entity.RollCooling;
import com.my.business.modular.rollcoolorder.dao.RollCoolorderDao;
import com.my.business.modular.rollcoolorder.service.RollCoolorderService;
import com.my.business.sys.common.entity.ResultData;

/**
* 冷却工单表接口具体实现类
* @author  生成器生成
* @date 2020-10-26 11:29:08
*/
@Service
public class RollCoolorderServiceImpl implements RollCoolorderService {
	
	@Autowired
	private RollCoolorderDao rollCoolorderDao;
	
	@Autowired
	private RollCoolingDao rollCoolingDao;
	
	/**
     * 赋值记录到冷却工单
     * @param data json字符串
     */
	public ResultData copydata(String data,Long userId,String sname){
		try{
    		RollCoolorder rollCoolorder = JSON.parseObject(data,RollCoolorder.class);
    		
    		RollCoolorder r_order = rollCoolorderDao.findDataRollCoolorderByEndNo(rollCoolorder.getRoll_no(),rollCoolorder.getField2());
    		
    		RollCooling r = new RollCooling();
    		r.setRoll_no(r_order.getRoll_no());
    		r.setProduction_line(r_order.getProduction_line());
    		r.setProduction_line_id(r_order.getProduction_line_id());
    		r.setFactory(r_order.getFactory());
    		r.setFactory_id(r_order.getFactory_id());
    		r.setFrame_no(r_order.getFrame_no());
    		r.setFrame_noid(r_order.getFrame_noid());
    		r.setMaterial(r_order.getMaterial());
    		r.setMaterial_id(r_order.getMaterial_id());
    		r.setRoll_type(r_order.getRoll_type());
    		r.setRoll_typeid(r_order.getRoll_typeid());
    		r.setCool_starttime(r_order.getField1());
    		r.setCool_endtime(r_order.getField2());
    		
    		CodiUtil.newRecord(userId,sname,r);
    		rollCoolingDao.insertDataRollCooling(r);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollCoolorder(String data,Long userId,String sname){
		try{
			JRollCoolorder jrollCoolorder = JSON.parseObject(data,JRollCoolorder.class);
    		RollCoolorder rollCoolorder = jrollCoolorder.getRollCoolorder();
    		CodiUtil.newRecord(userId,sname,rollCoolorder);
            rollCoolorderDao.insertDataRollCoolorder(rollCoolorder);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollCoolorderList(String data,Long userId,String sname){
		try{
			RollCoolorderList roll_list = JSON.parseObject(data,RollCoolorderList.class);
			List<RollCoolorder> list = roll_list.getList();
			for(RollCoolorder rollCoolorder : list) {
				CodiUtil.newRecord(userId,sname,rollCoolorder);
	            rollCoolorderDao.insertDataRollCoolorder(rollCoolorder);
			}
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollCoolorderOne(Long indocno){
		try {
            rollCoolorderDao.deleteDataRollCoolorderOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollCoolorderMany(String str_id) {
        try {
        	String sql = "delete roll_coolorder where indocno in(" + str_id +")";
            rollCoolorderDao.deleteDataRollCoolorderMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataRollCoolorder(String data,Long userId,String sname){
		try {
			JRollCoolorder jrollCoolorder = JSON.parseObject(data,JRollCoolorder.class);
    		RollCoolorder rollCoolorder = jrollCoolorder.getRollCoolorder();
        	CodiUtil.editRecord(userId,sname,rollCoolorder);
            rollCoolorderDao.updateDataRollCoolorder(rollCoolorder);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updatestate(String data){
		try {
    		RollCoolorder rollCoolorder = JSON.parseObject(data,RollCoolorder.class);
    		rollCoolorderDao.finishiData(rollCoolorder.getIndocno(), rollCoolorder.getIfinish());
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollCoolorderByPage(String data) {
        try {
        	JRollCoolorder jrollCoolorder = JSON.parseObject(data, JRollCoolorder.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jrollCoolorder.getPageIndex();
        	Integer pageSize = jrollCoolorder.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jrollCoolorder.getCondition()){
    			jsonObject  = JSON.parseObject(jrollCoolorder.getCondition().toString());
    		}
        	
        	
        	 String roll_no = null;
             String cool_starttime = null;  //冷却开始时间
             String cool_endtime = null;  //冷却结束时间
             String ifinish = null;
             if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                 roll_no = jsonObject.get("roll_no").toString();
             }
             if (!StringUtils.isEmpty(jsonObject.get("cool_starttime"))) {
             	cool_starttime = jsonObject.get("cool_starttime").toString();
             }
             if (!StringUtils.isEmpty(jsonObject.get("cool_endtime"))) {
             	cool_endtime = jsonObject.get("cool_endtime").toString();
             }
             if (!StringUtils.isEmpty(jsonObject.get("ifinish"))) {
             	ifinish = jsonObject.get("ifinish").toString();
             }

    		List<RollCoolorder> list = rollCoolorderDao.findDataRollCoolorderByPage((pageIndex-1)*pageSize, pageSize,roll_no,cool_starttime,cool_endtime,ifinish);
    		Integer count = rollCoolorderDao.findDataRollCoolorderByPageSize(roll_no,cool_starttime,cool_endtime,ifinish);
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollCoolorderByIndocno(String data) {
        try {
        	JRollCoolorder jrollCoolorder = JSON.parseObject(data, JRollCoolorder.class); 
        	Long indocno = jrollCoolorder.getIndocno();
        	
    		RollCoolorder rollCoolorder = rollCoolorderDao.findDataRollCoolorderByIndocno(indocno);
    		return ResultData.ResultDataSuccess(rollCoolorder);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<RollCoolorder> findDataRollCoolorder(){
		List<RollCoolorder> list = rollCoolorderDao.findDataRollCoolorder();
		return list;
	};
}
