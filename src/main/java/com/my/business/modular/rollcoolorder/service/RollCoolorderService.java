package com.my.business.modular.rollcoolorder.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.modular.rollcoolorder.entity.RollCoolorder;
import java.util.List;

/**
* 冷却工单表接口服务类
* @author  生成器生成
* @date 2020-10-26 11:29:08
*/
public interface RollCoolorderService {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollCoolorder(String data,Long userId,String sname);
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollCoolorderList(String data,Long userId,String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollCoolorderOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollCoolorderMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataRollCoolorder(String data,Long userId,String sname);
	
	/**
     * 修改ifinish
     */
	public ResultData updatestate(String data);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollCoolorderByPage(String data);
    
    /**
     * 复制到冷却历史中
     * @param data 分页参数字符串
     */
    public ResultData copydata(String data,Long userId,String sname);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataRollCoolorderByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<RollCoolorder> findDataRollCoolorder();
}
