package com.my.business.modular.rollcoolorder.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.rollcoolorder.entity.RollCoolorder;

import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 冷却工单表dao接口
 * @author  生成器生成
 * @date 2020-10-26 11:29:07
 * */
@Mapper
public interface RollCoolorderDao {

	/**
	 * 添加记录
	 * @param rollCoolorder  对象实体
	 * */
	public void insertDataRollCoolorder(RollCoolorder rollCoolorder);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataRollCoolorderOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataRollCoolorderMany(String value);
	
	/**
	 * 修改记录
	 * @param rollCoolorder  对象实体
	 * */
	public void updateDataRollCoolorder(RollCoolorder rollCoolorder);
	
	/**
	 * 修改记录
	 * @param rollCoolorder  对象实体
	 * */
	public void finishiData(@Param("indocno") Long indocno,@Param("ifinish") Long ifinish);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<RollCoolorder> findDataRollCoolorderByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize,@Param("roll_no") String roll_no,@Param("cool_starttime") String cool_starttime,@Param("cool_endtime") String cool_endtime,@Param("ifinish") String ifinish);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataRollCoolorderByPageSize(@Param("roll_no") String roll_no,@Param("cool_starttime") String cool_starttime,@Param("cool_endtime") String cool_endtime,@Param("ifinish") String ifinish);
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public RollCoolorder findDataRollCoolorderByIndocno(@Param("indocno") Long indocno);
    
    /***
     * 根据辊号和结束时间查询数据
     * @param indocno 用户id
     * @return
     */
    public RollCoolorder findDataRollCoolorderByEndNo(@Param("roll_no") String roll_no,@Param("f2") String f2);
    
    /**
	 * 根据结束冷却时间查看记录
	 * @return 对象数据集合
	 * */
	public List<RollCoolorder> findDataRollCoolorderByEndTime();
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<RollCoolorder> findDataRollCoolorder();
	
}
