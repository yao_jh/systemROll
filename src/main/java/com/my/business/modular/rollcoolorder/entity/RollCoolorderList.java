package com.my.business.modular.rollcoolorder.entity;

import java.util.List;


/**
* 冷却工单表json的实体类
* @author  生成器生成
* @date 2020-10-26 11:29:07
*/
public class RollCoolorderList{

	List<RollCoolorder> list;

	public List<RollCoolorder> getList() {
		return list;
	}

	public void setList(List<RollCoolorder> list) {
		this.list = list;
	}
}