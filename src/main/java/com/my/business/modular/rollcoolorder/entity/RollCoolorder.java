package com.my.business.modular.rollcoolorder.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
* 冷却工单表实体类
* @author  生成器生成
* @date 2020-10-26 11:29:06
*/
public class RollCoolorder extends BaseEntity{
		
	private Long indocno;  //主键
	private String production_line;  //产线(1-2250,2-1580)
	private Long production_line_id;  //
	private String roll_no;  //辊号
	private Long factory_id;  //厂商id
	private String factory;  //生产厂家
	private Long material_id;  //材质id
	private String material;  //材质
	private Long roll_typeid;  //轧辊类型id
	private String roll_type;  //轧辊类型
	private Long frame_noid;  //机架号id
	private String frame_no;  //机架号
	private String field1;  //
	private String field2;  //
	private String field3;  //
	private String field4;  //
	private String field5;  //
	private String field6;  //
	private String field7;  //
	private String field8;  //
	private String field9;  //
	private String field10;  //
	private String field11;  //
	private String field12;  //
	private Long ifinish;  //是否完成(0 未完成  1完成)
    
    
	public void setIndocno(Long indocno){
	    this.indocno = indocno;
	}
	public Long getIndocno(){
	    return this.indocno;
	}
	public void setProduction_line(String production_line){
	    this.production_line = production_line;
	}
	public String getProduction_line(){
	    return this.production_line;
	}
	public void setProduction_line_id(Long production_line_id){
	    this.production_line_id = production_line_id;
	}
	public Long getProduction_line_id(){
	    return this.production_line_id;
	}
	public void setRoll_no(String roll_no){
	    this.roll_no = roll_no;
	}
	public String getRoll_no(){
	    return this.roll_no;
	}
	public void setFactory_id(Long factory_id){
	    this.factory_id = factory_id;
	}
	public Long getFactory_id(){
	    return this.factory_id;
	}
	public void setFactory(String factory){
	    this.factory = factory;
	}
	public String getFactory(){
	    return this.factory;
	}
	public void setMaterial_id(Long material_id){
	    this.material_id = material_id;
	}
	public Long getMaterial_id(){
	    return this.material_id;
	}
	public void setMaterial(String material){
	    this.material = material;
	}
	public String getMaterial(){
	    return this.material;
	}
	public void setRoll_typeid(Long roll_typeid){
	    this.roll_typeid = roll_typeid;
	}
	public Long getRoll_typeid(){
	    return this.roll_typeid;
	}
	public void setRoll_type(String roll_type){
	    this.roll_type = roll_type;
	}
	public String getRoll_type(){
	    return this.roll_type;
	}
	public void setFrame_noid(Long frame_noid){
	    this.frame_noid = frame_noid;
	}
	public Long getFrame_noid(){
	    return this.frame_noid;
	}
	public void setFrame_no(String frame_no){
	    this.frame_no = frame_no;
	}
	public String getFrame_no(){
	    return this.frame_no;
	}
	public void setIfinish(Long ifinish){
	    this.ifinish = ifinish;
	}
	public Long getIfinish(){
	    return this.ifinish;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	public String getField11() {
		return field11;
	}
	public void setField11(String field11) {
		this.field11 = field11;
	}
	public String getField12() {
		return field12;
	}
	public void setField12(String field12) {
		this.field12 = field12;
	}
}