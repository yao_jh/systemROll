package com.my.business.modular.rollmxhard.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.rollmxhard.entity.RollMxHard;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 磨削硬度表dao接口
 * @author  生成器生成
 * @date 2020-10-31 15:55:21
 * */
@Mapper
public interface RollMxHardDao {

	/**
	 * 添加记录
	 * @param rollMxHard  对象实体
	 * */
	public void insertDataRollMxHard(RollMxHard rollMxHard);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataRollMxHardOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataRollMxHardMany(String value);
	
	/**
	 * 修改记录
	 * @param rollMxHard  对象实体
	 * */
	public void updateDataRollMxHard(RollMxHard rollMxHard);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<RollMxHard> findDataRollMxHardByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataRollMxHardByPageSize();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public RollMxHard findDataRollMxHardByIndocno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<RollMxHard> findDataRollMxHard();
	
}
