package com.my.business.modular.rollmxhard.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.rollmxhard.entity.RollMxHard;
import com.my.business.modular.rollmxhard.entity.JRollMxHard;
import com.my.business.modular.rollmxhard.dao.RollMxHardDao;
import com.my.business.modular.rollmxhard.service.RollMxHardService;
import com.my.business.sys.common.entity.ResultData;

/**
* 磨削硬度表接口具体实现类
* @author  生成器生成
* @date 2020-10-31 15:55:21
*/
@Service
public class RollMxHardServiceImpl implements RollMxHardService {
	
	@Autowired
	private RollMxHardDao rollMxHardDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollMxHard(String data,Long userId,String sname){
		try{
			JRollMxHard jrollMxHard = JSON.parseObject(data,JRollMxHard.class);
    		RollMxHard rollMxHard = jrollMxHard.getRollMxHard();
    		CodiUtil.newRecord(userId,sname,rollMxHard);
            rollMxHardDao.insertDataRollMxHard(rollMxHard);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollMxHardOne(Long indocno){
		try {
            rollMxHardDao.deleteDataRollMxHardOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollMxHardMany(String str_id) {
        try {
        	String sql = "delete roll_mx_hard where indocno in(" + str_id +")";
            rollMxHardDao.deleteDataRollMxHardMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataRollMxHard(String data,Long userId,String sname){
		try {
			JRollMxHard jrollMxHard = JSON.parseObject(data,JRollMxHard.class);
    		RollMxHard rollMxHard = jrollMxHard.getRollMxHard();
        	CodiUtil.editRecord(userId,sname,rollMxHard);
            rollMxHardDao.updateDataRollMxHard(rollMxHard);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollMxHardByPage(String data) {
        try {
        	JRollMxHard jrollMxHard = JSON.parseObject(data, JRollMxHard.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jrollMxHard.getPageIndex();
        	Integer pageSize = jrollMxHard.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jrollMxHard.getCondition()){
    			jsonObject  = JSON.parseObject(jrollMxHard.getCondition().toString());
    		}
    
    		List<RollMxHard> list = rollMxHardDao.findDataRollMxHardByPage((pageIndex-1)*pageSize, pageSize);
    		Integer count = rollMxHardDao.findDataRollMxHardByPageSize();
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollMxHardByIndocno(String data) {
        try {
        	JRollMxHard jrollMxHard = JSON.parseObject(data, JRollMxHard.class); 
        	Long indocno = jrollMxHard.getIndocno();
        	
    		RollMxHard rollMxHard = rollMxHardDao.findDataRollMxHardByIndocno(indocno);
    		return ResultData.ResultDataSuccess(rollMxHard);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<RollMxHard> findDataRollMxHard(){
		List<RollMxHard> list = rollMxHardDao.findDataRollMxHard();
		return list;
	};
}
