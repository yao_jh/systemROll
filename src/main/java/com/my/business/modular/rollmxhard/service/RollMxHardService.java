package com.my.business.modular.rollmxhard.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.modular.rollmxhard.entity.RollMxHard;
import java.util.List;

/**
* 磨削硬度表接口服务类
* @author  生成器生成
* @date 2020-10-31 15:55:21
*/
public interface RollMxHardService {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollMxHard(String data,Long userId,String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollMxHardOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollMxHardMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataRollMxHard(String data,Long userId,String sname);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollMxHardByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataRollMxHardByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<RollMxHard> findDataRollMxHard();
}
