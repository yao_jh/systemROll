package com.my.business.modular.rollstandard.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollstandard.entity.JRollStandard;
import com.my.business.modular.rollstandard.entity.RollStandard;
import com.my.business.modular.rollstandard.service.RollStandardService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 配置标准表控制器层
 *
 * @author 生成器生成
 * @date 2020-08-08 15:49:16
 */
@RestController
@RequestMapping("/rollStandard")
public class RollStandardController {

    @Autowired
    private RollStandardService rollStandardService;

    /**
     * 添加记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollStandard(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollStandardService.insertDataRollStandard(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollStandardOne(@RequestBody String data) {
        try {
            JRollStandard jrollStandard = JSON.parseObject(data, JRollStandard.class);
            return rollStandardService.deleteDataRollStandardOne(jrollStandard.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollStandard jrollStandard = JSON.parseObject(data, JRollStandard.class);
            return rollStandardService.deleteDataRollStandardMany(jrollStandard.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollStandard(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollStandardService.updateDataRollStandard(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollStandardByPage(@RequestBody String data) {
        return rollStandardService.findDataRollStandardByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollStandardByIndocno(@RequestBody String data) {
        return rollStandardService.findDataRollStandardByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollStandard> findDataRollStandard() {
        return rollStandardService.findDataRollStandard();
    }

    /**
     * 标准下拉
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findDpd"}, method = RequestMethod.POST)
    public ResultData findDpdStandard() {
        return rollStandardService.findDpdStandard();
    }

}
