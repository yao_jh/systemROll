package com.my.business.modular.rollstandard.dao;

import com.my.business.modular.rollstandard.entity.DpdStandard;
import com.my.business.modular.rollstandard.entity.RollStandard;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 配置标准表dao接口
 *
 * @author 生成器生成
 * @date 2020-08-08 15:49:16
 */
@Mapper
public interface RollStandardDao {

    /**
     * 添加记录
     *
     * @param rollStandard 对象实体
     */
    void insertDataRollStandard(RollStandard rollStandard);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollStandardOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollStandardMany(String value);

    /**
     * 修改记录
     *
     * @param rollStandard 对象实体
     */
    void updateDataRollStandard(RollStandard rollStandard);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollStandard> findDataRollStandardByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("modular_no") String modular_no, @Param("modular_name") String modular_name, @Param("roll_typeid") String roll_typeid);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollStandardByPageSize(@Param("modular_no") String modular_no, @Param("modular_name") String modular_name, @Param("roll_typeid") String roll_typeid);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollStandard findDataRollStandardByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollStandard> findDataRollStandard();

    /**
     * 标准下拉
     *
     * @return 对象数据集合
     */
    List<DpdStandard> findDpdStandard();

}
