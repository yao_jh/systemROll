package com.my.business.modular.rollstandard.entity;

/***
 * 标准下拉框实体类
 *
 * @author cc
 *
 */
public class DpdStandard {
    private String modular_name; // 模块名称
    private String modular_no; // 模块编码

    public String getModular_name() {
        return modular_name;
    }

    public void setModular_name(String modular_name) {
        this.modular_name = modular_name;
    }

    public String getModular_no() {
        return modular_no;
    }

    public void setModular_no(String modular_no) {
        this.modular_no = modular_no;
    }

}
