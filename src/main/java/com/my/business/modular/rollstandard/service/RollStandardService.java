package com.my.business.modular.rollstandard.service;

import com.my.business.modular.rollstandard.entity.RollStandard;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 配置标准表接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-08 15:49:16
 */
public interface RollStandardService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollStandard(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollStandardOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollStandardMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollStandard(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollStandardByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollStandardByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollStandard> findDataRollStandard();

    /**
     * 标准下拉框
     */
    ResultData findDpdStandard();
}
