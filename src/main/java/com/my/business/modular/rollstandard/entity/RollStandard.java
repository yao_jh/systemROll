package com.my.business.modular.rollstandard.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 配置标准表实体类
 *
 * @author 生成器生成
 * @date 2020-08-08 15:49:15
 */
public class RollStandard extends BaseEntity {

    private Long indocno;  //主键
    private String production_line;  //产线
    private Long production_line_id; //产线id
    private String modular_name;  //模块名称
    private String modular_no;  //模块编码
    private Long filed_typeid;  //字段类型id
    private String field_type;  //字段类型
    private String field_value;  //是或者否的文字
    private String field_unit; //字段单位
    private Double field_max;  //上区间
    private Double field_min;  //下区间
    private String field_name;  //字段名称
    private String field_key;  //对应工单的字段
    private String snote;  //备注
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private Long standard_type;   //标准类型

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }
    
    public String getProduction_line() {
		return production_line;
	}

	public void setProduction_line(String production_line) {
		this.production_line = production_line;
	}

	public Long getProduction_line_id() {
		return production_line_id;
	}

	public void setProduction_line_id(Long production_line_id) {
		this.production_line_id = production_line_id;
	}

	public String getModular_name() {
        return this.modular_name;
    }

    public void setModular_name(String modular_name) {
        this.modular_name = modular_name;
    }

    public String getModular_no() {
        return this.modular_no;
    }

    public void setModular_no(String modular_no) {
        this.modular_no = modular_no;
    }

    public Long getFiled_typeid() {
        return this.filed_typeid;
    }

    public void setFiled_typeid(Long filed_typeid) {
        this.filed_typeid = filed_typeid;
    }

    public String getField_type() {
        return this.field_type;
    }

    public void setField_type(String field_type) {
        this.field_type = field_type;
    }

    public Double getField_max() {
        return this.field_max;
    }

    public void setField_max(Double field_max) {
        this.field_max = field_max;
    }

    public Double getField_min() {
        return this.field_min;
    }

    public void setField_min(Double field_min) {
        this.field_min = field_min;
    }

    public String getField_name() {
        return this.field_name;
    }

    public void setField_name(String field_name) {
        this.field_name = field_name;
    }

    public String getField_key() {
        return this.field_key;
    }

    public void setField_key(String field_key) {
        this.field_key = field_key;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public Long getRoll_typeid() {
        return roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public Long getStandard_type() {
        return standard_type;
    }

    public void setStandard_type(Long standard_type) {
        this.standard_type = standard_type;
    }

    public String getField_unit() {
        return field_unit;
    }

    public void setField_unit(String field_unit) {
        this.field_unit = field_unit;
    }

    public String getField_value() {
        return field_value;
    }

    public void setField_value(String field_value) {
        this.field_value = field_value;
    }
}