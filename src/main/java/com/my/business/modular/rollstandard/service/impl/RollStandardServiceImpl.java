package com.my.business.modular.rollstandard.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollstandard.dao.RollStandardDao;
import com.my.business.modular.rollstandard.entity.DpdStandard;
import com.my.business.modular.rollstandard.entity.JRollStandard;
import com.my.business.modular.rollstandard.entity.RollStandard;
import com.my.business.modular.rollstandard.service.RollStandardService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 配置标准表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-08 15:49:16
 */
@Service
public class RollStandardServiceImpl implements RollStandardService {

    @Autowired
    private RollStandardDao rollStandardDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollStandard(String data, Long userId, String sname) {
        try {
            JRollStandard jrollStandard = JSON.parseObject(data, JRollStandard.class);
            RollStandard rollStandard = jrollStandard.getRollStandard();
            CodiUtil.newRecord(userId, sname, rollStandard);
            rollStandardDao.insertDataRollStandard(rollStandard);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollStandardOne(Long indocno) {
        try {
            rollStandardDao.deleteDataRollStandardOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollStandardMany(String str_id) {
        try {
            String sql = "delete roll_standard where indocno in(" + str_id + ")";
            rollStandardDao.deleteDataRollStandardMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataRollStandard(String data, Long userId, String sname) {
        try {
            JRollStandard jrollStandard = JSON.parseObject(data, JRollStandard.class);
            RollStandard rollStandard = jrollStandard.getRollStandard();
            CodiUtil.editRecord(userId, sname, rollStandard);
            rollStandardDao.updateDataRollStandard(rollStandard);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollStandardByPage(String data) {
        try {
            JRollStandard jrollStandard = JSON.parseObject(data, JRollStandard.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollStandard.getPageIndex();
            Integer pageSize = jrollStandard.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollStandard.getCondition()) {
                jsonObject = JSON.parseObject(jrollStandard.getCondition().toString());
            }

            String modular_no = null;
            String modular_name = null;
            String roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("modular_no"))) {
                modular_no = jsonObject.get("modular_no").toString();
            }
            if (!StringUtils.isEmpty(jsonObject.get("modular_name"))) {
                modular_name = jsonObject.get("modular_name").toString();
            }
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = jsonObject.get("roll_typeid").toString();
            }

            List<RollStandard> list = rollStandardDao.findDataRollStandardByPage((pageIndex - 1) * pageSize, pageSize, modular_no, modular_name, roll_typeid);
            Integer count = rollStandardDao.findDataRollStandardByPageSize(modular_no, modular_name, roll_typeid);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollStandardByIndocno(String data) {
        try {
            JRollStandard jrollStandard = JSON.parseObject(data, JRollStandard.class);
            Long indocno = jrollStandard.getIndocno();

            RollStandard rollStandard = rollStandardDao.findDataRollStandardByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollStandard);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollStandard> findDataRollStandard() {
        List<RollStandard> list = rollStandardDao.findDataRollStandard();
        return list;
    }

    /**
     * 标准下拉框
     */
    @Override
    public ResultData findDpdStandard() {
        try {
            List<DpdStandard> list = rollStandardDao.findDpdStandard();
            return ResultData.ResultDataSuccessSelf("获取成功", list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("获取失败" + e.getMessage(), null);
        }
    }

}
