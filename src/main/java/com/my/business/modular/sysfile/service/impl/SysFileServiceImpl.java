package com.my.business.modular.sysfile.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.common.OperationFile;
import com.my.business.modular.sysfile.dao.SysFileDao;
import com.my.business.modular.sysfile.entity.JSysFile;
import com.my.business.modular.sysfile.entity.SysFile;
import com.my.business.modular.sysfile.service.SysFileService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 文件上传表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-06-10 09:42:24
 */
@Service
public class SysFileServiceImpl implements SysFileService {

    @Autowired
    private SysFileDao sysFileDao;

    /**
     * 添加记录
     *
     * @param fileName 文件名称
     * @param fileUrl  文件上传路径
     * @param userId   用户id
     * @param sname    用户姓名
     */
    public ResultData insertDataSysFile(String fileName, String fileUrl, Long userId, String sname) {
        try {
            //JSysFile jsysFile = JSON.parseObject(data,JSysFile.class);
//			JSysFile jsysFile = new JSysFile();
//			jsysFile.setSysFile(sysFile);
            SysFile sysFile = new SysFile();
            sysFile.setFile_name(fileName);
            sysFile.setFile_url(fileUrl);

            CodiUtil.newRecord(userId, sname, sysFile);
            sysFileDao.insertDataSysFile(sysFile);
            return ResultData.ResultDataSuccessSelf("文件上传成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            OperationFile.removeFile(fileUrl);//删除文件
            return ResultData.ResultDataFaultSelf("上传失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataSysFileOne(Long indocno) {
        try {
            SysFile sysFile = sysFileDao.findDataSysFileByIndocno(indocno);
            String fileUrl = sysFile.getFile_url();
            sysFileDao.deleteDataSysFileOne(indocno);

            OperationFile.removeFile(fileUrl);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataSysFileMany(String str_id) {
        try {
            String sql = "delete sys_file where indocno in(" + str_id + ")";
            sysFileDao.deleteDataSysFileMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataSysFile(String data, Long userId, String sname) {
        try {
            JSysFile jsysFile = JSON.parseObject(data, JSysFile.class);
            SysFile sysFile = jsysFile.getSysFile();
            CodiUtil.editRecord(userId, sname, sysFile);
            sysFileDao.updateDataSysFile(sysFile);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysFileByPage(String data) {
        try {
            JSysFile jsysFile = JSON.parseObject(data, JSysFile.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jsysFile.getPageIndex();
            Integer pageSize = jsysFile.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jsysFile.getCondition()) {
                jsonObject = JSON.parseObject(jsysFile.getCondition().toString());
            }

            String file_name = null;
            String createname = null;
            String createtimes = null;
            String createtimee = null;

            if (!StringUtils.isEmpty(jsonObject.get("file_name"))) {
                file_name = jsonObject.get("file_name").toString();
            }
            if (!StringUtils.isEmpty(jsonObject.get("createname"))) {
                createname = jsonObject.get("createname").toString();
            }
            if (!StringUtils.isEmpty(jsonObject.get("createtimes"))) {
                createtimes = jsonObject.get("createtimes").toString();
            }
            if (!StringUtils.isEmpty(jsonObject.get("createtimee"))) {
                createtimee = jsonObject.get("createtimee").toString();
            }

            List<SysFile> list = sysFileDao.findDataSysFileByPage(file_name, createname, createtimes, createtimee, pageIndex, pageSize);
            Integer count = sysFileDao.findDataSysFileByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataSysFileByIndocno(String data) {
        try {
            JSysFile jsysFile = JSON.parseObject(data, JSysFile.class);
            Long indocno = jsysFile.getIndocno();

            SysFile sysFile = sysFileDao.findDataSysFileByIndocno(indocno);
            return ResultData.ResultDataSuccess(sysFile);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<SysFile> findDataSysFile() {
        List<SysFile> list = sysFileDao.findDataSysFile();
        return list;
    }

}
