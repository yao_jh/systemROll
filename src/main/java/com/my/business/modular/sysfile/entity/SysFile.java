package com.my.business.modular.sysfile.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 文件上传表实体类
 *
 * @author 生成器生成
 * @date 2020-06-10 09:42:23
 */
public class SysFile extends BaseEntity {

    private Long indocno;  //主键
    private String file_name;  //文件名称
    private String file_url;  //文件地址
    private String snote;  //说明

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getFile_name() {
        return this.file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getFile_url() {
        return this.file_url;
    }

    public void setFile_url(String file_url) {
        this.file_url = file_url;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }


}