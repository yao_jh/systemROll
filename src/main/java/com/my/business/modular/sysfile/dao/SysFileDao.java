package com.my.business.modular.sysfile.dao;

import com.my.business.modular.sysfile.entity.SysFile;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 文件上传表dao接口
 *
 * @author 生成器生成
 * @date 2020-06-10 09:42:24
 */
@Mapper
public interface SysFileDao {

    /**
     * 添加记录
     *
     * @param sysFile 对象实体
     */
    void insertDataSysFile(SysFile sysFile);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataSysFileOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataSysFileMany(String value);

    /**
     * 修改记录
     *
     * @param sysFile 对象实体
     */
    void updateDataSysFile(SysFile sysFile);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<SysFile> findDataSysFileByPage(@Param("file_name") String file_name, @Param("createname") String createname, @Param("createtimes") String createtimes, @Param("createtimee") String createtimee, @Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataSysFileByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    SysFile findDataSysFileByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<SysFile> findDataSysFile();

}
