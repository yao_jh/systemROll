package com.my.business.modular.sysfile.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.sysfile.entity.JSysFile;
import com.my.business.modular.sysfile.entity.SysFile;
import com.my.business.modular.sysfile.service.SysFileService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.List;


/**
 * 文件上传表控制器层
 *
 * @author 生成器生成
 * @date 2020-06-10 09:42:24
 */
@RestController
@RequestMapping("/sysFile")
public class SysFileController {

    @Value("${upload_url}")
    private String file_url;  //从配置文件中获取文件路径

    @Autowired
    private SysFileService sysFileService;

    /**
     * 添加记录
     * @param userId 用户id
     * @param sname 用户姓名
     * */
//	@CrossOrigin
//	@RequestMapping(value={"/insert"}, method=RequestMethod.POST)
//	@ResponseBody
//	public ResultData insertDataSysFile(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
//		JCommon common = JSON.parseObject(loginToken,JCommon.class);
//    	String sname = common.getSname();
//    	Long userId = common.getUserId();
//		return sysFileService.insertDataSysFile(data,userId,CodiUtil.returnLm(sname));
//	};

    /***
     * 单文件上传
     * @param file
     * @return
     */
    @CrossOrigin
    @PostMapping("singleupload")
    @ResponseBody
    public ResultData singleUpload(MultipartFile file, @RequestHeader(value = "loginToken") String loginToken) {
        if (null != file) {
            String filename = file.getOriginalFilename();// 文件原名称
            String path = file_url + "/" + filename;
            File localFile = new File(path);
            try {
                file.transferTo(localFile);
                JCommon common = JSON.parseObject(loginToken, JCommon.class);
                String sname = common.getSname();
                Long userId = common.getUserId();
                return sysFileService.insertDataSysFile(filename, path, userId, CodiUtil.returnLm(sname));
                //return ResultData.ResultDataSuccessSelf("上传成功", null);
            } catch (Exception e) {
                e.printStackTrace();
                return ResultData.ResultDataFaultSelf("文件有误，上传失败", null);
            }
        } else {
            return ResultData.ResultDataFaultSelf("上传失败,文件为空", null);
        }
    }

    /***
     * 文件下载
     * @param  HttpServletResponse response
     * @return
     */
    @CrossOrigin
    @GetMapping("singledownload")
    @ResponseBody
    public String singledownload(String file_name, HttpServletResponse request, HttpServletResponse response) throws Exception {
        //String file_name = "CentalDb表结构.sql";// 文件名
        System.out.println(file_name);
        if (file_name != null) {
            //设置文件路径
            //    String pathname = "D:\\fileupload\\1.jpg";
//			File path=new File(ResourceUtils.getURL("classpath:").getPath());
//			File targetFile=new File(path.getAbsolutePath(),"static/imgupload/");//jar包下面的target目录
            File file = new File(file_url + "/" + file_name);
            if (file.exists()) {
                response.setContentType("application/force-download");// 设置强制下载不打开
                response.addHeader("Content-Disposition", "attachment;fileName=" + file_name);// 设置文件名
                byte[] buffer = new byte[1024];
                FileInputStream fis = null;
                BufferedInputStream bis = null;
                try {
                    fis = new FileInputStream(file);
                    bis = new BufferedInputStream(fis);
                    OutputStream os = response.getOutputStream();
                    int i = bis.read(buffer);
                    while (i != -1) {
                        os.write(buffer, 0, i);
                        i = bis.read(buffer);
                    }
                    return "下载成功";
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    bis.close();
                    fis.close();
                }
            } else {
                return "文件不存在";
            }
        }
        return "下载失败";
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysFileOne(@RequestBody String data) {
        try {
            JSysFile jsysFile = JSON.parseObject(data, JSysFile.class);
            return sysFileService.deleteDataSysFileOne(jsysFile.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JSysFile jsysFile = JSON.parseObject(data, JSysFile.class);
            return sysFileService.deleteDataSysFileMany(jsysFile.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataSysFile(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return sysFileService.updateDataSysFile(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysFileByPage(@RequestBody String data) {
        return sysFileService.findDataSysFileByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataSysFileByIndocno(@RequestBody String data) {
        return sysFileService.findDataSysFileByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<SysFile> findDataSysFile() {
        return sysFileService.findDataSysFile();
    }

}
