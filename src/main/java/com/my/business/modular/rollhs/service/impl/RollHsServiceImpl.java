package com.my.business.modular.rollhs.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollhs.dao.RollHsDao;
import com.my.business.modular.rollhs.entity.JRollHs;
import com.my.business.modular.rollhs.entity.RollHs;
import com.my.business.modular.rollhs.service.RollHsService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 辊票查询接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-12-05 15:31:27
 */
@Service
public class RollHsServiceImpl implements RollHsService {

    @Autowired
    private RollHsDao rollHsDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollHs(String data, Long userId, String sname) {
        try {
            JRollHs jrollHs = JSON.parseObject(data, JRollHs.class);
            RollHs rollHs = jrollHs.getRollHs();
            CodiUtil.newRecord(userId, sname, rollHs);
            rollHsDao.insertDataRollHs(rollHs);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollHsOne(Long indocno) {
        try {
            rollHsDao.deleteDataRollHsOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollHsMany(String str_id) {
        try {
            String sql = "delete roll_hs where indocno in(" + str_id + ")";
            rollHsDao.deleteDataRollHsMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollHs(String data, Long userId, String sname) {
        try {
            JRollHs jrollHs = JSON.parseObject(data, JRollHs.class);
            RollHs rollHs = jrollHs.getRollHs();
            CodiUtil.editRecord(userId, sname, rollHs);
            rollHsDao.updateDataRollHs(rollHs);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollHsByPage(String data) {
        try {
            JRollHs jrollHs = JSON.parseObject(data, JRollHs.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollHs.getPageIndex();
            Integer pageSize = jrollHs.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollHs.getCondition()) {
                jsonObject = JSON.parseObject(jrollHs.getCondition().toString());
            }

            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            List<RollHs> list = rollHsDao.findDataRollHsByPage((pageIndex - 1) * pageSize, pageSize, dbegin, dend);
            Integer count = rollHsDao.findDataRollHsByPageSize(dbegin, dend);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollHsByPage2(String data) {
        try {
            JRollHs jrollHs = JSON.parseObject(data, JRollHs.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollHs.getPageIndex();
            Integer pageSize = jrollHs.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollHs.getCondition()) {
                jsonObject = JSON.parseObject(jrollHs.getCondition().toString());
            }

            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            Long istate = null;
            if (!StringUtils.isEmpty(jsonObject.get("istate"))) {
                istate = Long.valueOf(jsonObject.get("istate").toString());
            }

            List<RollHs> list2 = rollHsDao.findDataRollHsByColl((pageIndex - 1) * pageSize, pageSize, dbegin, dend,istate);
            Integer count = rollHsDao.findDataRollHsByCollPage((pageIndex - 1) * pageSize, pageSize, dbegin, dend,istate);
            return ResultData.ResultDataSuccess(list2, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    @Override
    public ResultData changestate(String data, long l) {
        try {
            JRollHs jrollHs = JSON.parseObject(data, JRollHs.class);
            RollHs rollHs = jrollHs.getRollHs();
            if(l == 1L){
                rollHsDao.updateState(rollHs.getTime_1(),null,1L);
            }else {
                rollHsDao.updateState(rollHs.getTime_1(),rollHs.getSnote(),2L);
            }
            return ResultData.ResultDataSuccessSelf("状态变更成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("状态变更失败" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollHsByIndocno(String data) {
        try {
            JRollHs jrollHs = JSON.parseObject(data, JRollHs.class);
            Long indocno = jrollHs.getIndocno();

            RollHs rollHs = rollHsDao.findDataRollHsByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollHs);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollHs> findDataRollHs() {
        List<RollHs> list = rollHsDao.findDataRollHs();
        return list;
    }

}
