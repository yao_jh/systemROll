package com.my.business.modular.rollhs.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollhs.entity.JRollHs;
import com.my.business.modular.rollhs.entity.RollHs;
import com.my.business.modular.rollhs.service.RollHsService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 辊票查询控制器层
 *
 * @author 生成器生成
 * @date 2020-12-05 15:31:27
 */
@RestController
@RequestMapping("/rollHs")
public class RollHsController {

    @Autowired
    private RollHsService rollHsService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollHs(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollHsService.insertDataRollHs(data, userId, CodiUtil.returnLm(sname));
    }

	/**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollHsOne(@RequestBody String data) {
        try {
            JRollHs jrollHs = JSON.parseObject(data, JRollHs.class);
            return rollHsService.deleteDataRollHsOne(jrollHs.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollHs jrollHs = JSON.parseObject(data, JRollHs.class);
            return rollHsService.deleteDataRollHsMany(jrollHs.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollHs(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollHsService.updateDataRollHs(data, userId, CodiUtil.returnLm(sname));
    }

	/**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollHsByPage(@RequestBody String data) {
        return rollHsService.findDataRollHsByPage(data);
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage2"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollHsByPage2(@RequestBody String data) {
        return rollHsService.findDataRollHsByPage2(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollHsByIndocno(@RequestBody String data) {
        return rollHsService.findDataRollHsByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollHs> findDataRollHs() {
        return rollHsService.findDataRollHs();
    }

    /**
     * 更新状态——确认
     */
    @CrossOrigin
    @RequestMapping(value = {"/changestate"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData changestate(@RequestBody String data) {
        return rollHsService.changestate(data,1L);
    }

    /**
     * 更新状态——驳回
     */
    @CrossOrigin
    @RequestMapping(value = {"/changestate2"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData changestate2(@RequestBody String data) {
        return rollHsService.changestate(data,2L);
    }
}
