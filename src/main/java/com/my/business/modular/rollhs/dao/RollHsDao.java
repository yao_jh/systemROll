package com.my.business.modular.rollhs.dao;

import com.my.business.modular.rollhs.entity.RollHs;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 辊票查询dao接口
 *
 * @author 生成器生成
 * @date 2020-12-05 15:31:27
 */
@Mapper
public interface RollHsDao {

    /**
     * 添加记录
     *
     * @param rollHs 对象实体
     */
	void insertDataRollHs(RollHs rollHs);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
	void deleteDataRollHsOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
	void deleteDataRollHsMany(String value);

    /**
     * 修改记录
     *
     * @param rollHs 对象实体
     */
	void updateDataRollHs(RollHs rollHs);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
	List<RollHs> findDataRollHsByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("dbegin") String dbegin, @Param("dend") String dend);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
	Integer findDataRollHsByPageSize(@Param("dbegin") String dbegin, @Param("dend") String dend);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
	RollHs findDataRollHsByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
	List<RollHs> findDataRollHs();

    /**
     * 根据时间查找时间和班组班次
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @param istate
     * @return
     */
    List<RollHs> findDataRollHsByColl(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("dbegin") String dbegin, @Param("dend") String dend,@Param("istate") Long istate);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollHsByCollPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("dbegin") String dbegin, @Param("dend") String dend,@Param("istate") Long istate);

    /**
     * 更新状态
     * @param time_1 时间
     * @param snote 备注
     */
    void updateState(@Param("time_1") String time_1, @Param("snote") String snote, @Param("istate") Long istate);
}
