package com.my.business.modular.rollhs.service;

import com.my.business.modular.rollhs.entity.RollHs;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 辊票查询接口服务类
 *
 * @author 生成器生成
 * @date 2020-12-05 15:31:27
 */
public interface RollHsService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
	ResultData insertDataRollHs(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
	ResultData deleteDataRollHsOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
	ResultData deleteDataRollHsMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
	ResultData updateDataRollHs(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
	ResultData findDataRollHsByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
	ResultData findDataRollHsByIndocno(String data);

    /**
     * 查看记录
     */
	List<RollHs> findDataRollHs();

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollHsByPage2(String data);

    /**
     * 更新状态
     * @param data
     * @param l
     * @return
     */
    ResultData changestate(String data, long l);
}
