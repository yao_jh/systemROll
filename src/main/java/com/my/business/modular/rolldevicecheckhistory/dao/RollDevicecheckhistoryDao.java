package com.my.business.modular.rolldevicecheckhistory.dao;

import com.my.business.modular.rolldevicecheckhistory.entity.RollDevicecheckhistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 点检历史记录表dao接口
 *
 * @author 生成器生成
 * @date 2020-07-14 16:28:13
 */
@Mapper
public interface RollDevicecheckhistoryDao {

    /**
     * 添加记录
     *
     * @param rollDevicecheckhistory 对象实体
     */
    void insertDataRollDevicecheckhistory(RollDevicecheckhistory rollDevicecheckhistory);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollDevicecheckhistoryOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollDevicecheckhistoryMany(String value);

    /**
     * 修改记录
     *
     * @param rollDevicecheckhistory 对象实体
     */
    void updateDataRollDevicecheckhistory(RollDevicecheckhistory rollDevicecheckhistory);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollDevicecheckhistory> findDataRollDevicecheckhistoryByPage(@Param("pageIndex") Integer pageIndex,
                                                                      @Param("pageSize") Integer pageSize,
                                                                      @Param("check_manager_condition") String checkManagerCondition,
                                                                      @Param("check_type_condition") Integer checkType,
                                                                      @Param("check_date_start_condition") String checkDateStart,
                                                                      @Param("check_date_end_condition") String checkDateEnd);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollDevicecheckhistoryByPageSize(@Param("check_manager_condition") String checkManagerCondition,
                                                     @Param("check_type_condition") Integer checkType,
                                                     @Param("check_date_start_condition") String checkDateStart,
                                                     @Param("check_date_end_condition") String checkDateEnd);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollDevicecheckhistory findDataRollDevicecheckhistoryByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollDevicecheckhistory> findDataRollDevicecheckhistory();

}
