package com.my.business.modular.rolldevicecheckhistory.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rolldevicecheckhistory.entity.JRollDevicecheckhistory;
import com.my.business.modular.rolldevicecheckhistory.entity.RollDevicecheckhistory;
import com.my.business.modular.rolldevicecheckhistory.service.RollDevicecheckhistoryService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 点检历史记录表控制器层
 *
 * @author 生成器生成
 * @date 2020-07-14 16:28:14
 */
@RestController
@RequestMapping("/rollDevicecheckhistory")
public class RollDevicecheckhistoryController {

    @Autowired
    private RollDevicecheckhistoryService rollDevicecheckhistoryService;

    /**
     * 添加记录
     *
     * @param loginToken 用户token
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollDevicecheckhistory(@RequestBody RollDevicecheckhistory rollDevicecheckhistory, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollDevicecheckhistoryService.insertDataRollDevicecheckhistory(rollDevicecheckhistory, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollDevicecheckhistoryOne(@RequestBody String data) {
        try {
            JRollDevicecheckhistory jrollDevicecheckhistory = JSON.parseObject(data, JRollDevicecheckhistory.class);
            return rollDevicecheckhistoryService.deleteDataRollDevicecheckhistoryOne(jrollDevicecheckhistory.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollDevicecheckhistory jrollDevicecheckhistory = JSON.parseObject(data, JRollDevicecheckhistory.class);
            return rollDevicecheckhistoryService.deleteDataRollDevicecheckhistoryMany(jrollDevicecheckhistory.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollDevicecheckhistory(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollDevicecheckhistoryService.updateDataRollDevicecheckhistory(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDevicecheckhistoryByPage(@RequestBody String data) {
        return rollDevicecheckhistoryService.findDataRollDevicecheckhistoryByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDevicecheckhistoryByIndocno(@RequestBody String data) {
        return rollDevicecheckhistoryService.findDataRollDevicecheckhistoryByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollDevicecheckhistory> findDataRollDevicecheckhistory() {
        return rollDevicecheckhistoryService.findDataRollDevicecheckhistory();
    }
}
