package com.my.business.modular.rolldevicecheckhistory.service;

import com.my.business.modular.rolldevicecheckhistory.entity.RollDevicecheckhistory;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 点检历史记录表接口服务类
 *
 * @author 生成器生成
 * @date 2020-07-14 16:28:14
 */
public interface RollDevicecheckhistoryService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollDevicecheckhistory(RollDevicecheckhistory rollDevicecheckhistory, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollDevicecheckhistoryOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollDevicecheckhistoryMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollDevicecheckhistory(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollDevicecheckhistoryByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollDevicecheckhistoryByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollDevicecheckhistory> findDataRollDevicecheckhistory();
}
