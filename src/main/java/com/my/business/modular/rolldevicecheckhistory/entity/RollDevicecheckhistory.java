package com.my.business.modular.rolldevicecheckhistory.entity;

import com.my.business.sys.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 点检历史记录表实体类
 *
 * @author 生成器生成
 * @date 2020-07-14 16:28:13
 */
@Data
@EqualsAndHashCode
public class RollDevicecheckhistory extends BaseEntity {

    private Long indocno;  //主键
    private Long check_schedule_id;  //点检计划id
    private String check_content;  //修改内容
    private Date check_complete_time;  //点检完成时间
    private String snote;  //备注

    private Integer check_type;  //检查类型(0/1/2，日检/停机检/年检）
    private Integer check_complete_ornot;//检查是否完成 今日不点检/安排点检但是目前未提交/已点检完成）
    private String check_manager_name;  //检测人
    private String broken_infomation; //故障信息
    private String checkCompleteTimeStr; //点检时间string 类型

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getCheck_schedule_id() {
        return check_schedule_id;
    }

    public void setCheck_schedule_id(Long check_schedule_id) {
        this.check_schedule_id = check_schedule_id;
    }

    public String getCheck_content() {
        return check_content;
    }

    public void setCheck_content(String check_content) {
        this.check_content = check_content;
    }

    public Date getCheck_complete_time() {
        return check_complete_time;
    }

    public void setCheck_complete_time(Date check_complete_time) {
        this.check_complete_time = check_complete_time;
    }

    public String getSnote() {
        return snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public Integer getCheck_type() {
        return check_type;
    }

    public void setCheck_type(Integer check_type) {
        this.check_type = check_type;
    }

    public Integer getCheck_complete_ornot() {
        return check_complete_ornot;
    }

    public void setCheck_complete_ornot(Integer check_complete_ornot) {
        this.check_complete_ornot = check_complete_ornot;
    }

    public String getCheck_manager_name() {
        return check_manager_name;
    }

    public void setCheck_manager_name(String check_manager_name) {
        this.check_manager_name = check_manager_name;
    }

    public String getBroken_infomation() {
        return broken_infomation;
    }

    public void setBroken_infomation(String broken_infomation) {
        this.broken_infomation = broken_infomation;
    }

    public String getCheckCompleteTimeStr() {
        return checkCompleteTimeStr;
    }

    public void setCheckCompleteTimeStr(String checkCompleteTimeStr) {
        this.checkCompleteTimeStr = checkCompleteTimeStr;
    }
}