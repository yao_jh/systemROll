package com.my.business.modular.rolldevicecheckhistory.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rolldevicecheckhistory.dao.RollDevicecheckhistoryDao;
import com.my.business.modular.rolldevicecheckhistory.entity.JRollDevicecheckhistory;
import com.my.business.modular.rolldevicecheckhistory.entity.RollDevicecheckhistory;
import com.my.business.modular.rolldevicecheckhistory.service.RollDevicecheckhistoryService;
import com.my.business.modular.rolldevicecheckmodel.dao.RollDevicecheckmodelDao;
import com.my.business.modular.rolldevicecheckmodel.entity.RollDeviceCheckModel;
import com.my.business.modular.rolldevicecheckschedule.dao.RollDevicecheckscheduleDao;
import com.my.business.modular.rolldevicecheckschedule.entity.RollDevicecheckschedule;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 点检历史记录表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-07-14 16:28:14
 */
@Service
public class RollDevicecheckhistoryServiceImpl implements RollDevicecheckhistoryService {

    @Autowired
    private RollDevicecheckhistoryDao rollDevicecheckhistoryDao;

    @Autowired
    private RollDevicecheckscheduleDao rollDevicecheckscheduleDao;

    @Autowired
    private RollDevicecheckmodelDao rollDevicecheckmodelDao;

    /**
     * 添加记录
     *
     * @param rollDevicecheckhistory json字符串
     * @param userId                 用户id
     * @param sname                  用户姓名
     */
    public ResultData insertDataRollDevicecheckhistory(RollDevicecheckhistory rollDevicecheckhistory, Long userId, String sname) {
        try {
            CodiUtil.newRecord(userId, sname, rollDevicecheckhistory);
            rollDevicecheckhistoryDao.insertDataRollDevicecheckhistory(rollDevicecheckhistory);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollDevicecheckhistoryOne(Long indocno) {
        try {
            rollDevicecheckhistoryDao.deleteDataRollDevicecheckhistoryOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollDevicecheckhistoryMany(String str_id) {
        try {
            String sql = "delete roll_devicecheckhistory where indocno in(" + str_id + ")";
            rollDevicecheckhistoryDao.deleteDataRollDevicecheckhistoryMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   对象实体
     */
    public ResultData updateDataRollDevicecheckhistory(String data, Long userId, String sname) {
        try {
            JRollDevicecheckhistory jrollDevicecheckhistory = JSON.parseObject(data, JRollDevicecheckhistory.class);
            RollDevicecheckhistory rollDevicecheckhistory = jrollDevicecheckhistory.getRollDevicecheckhistory();
            CodiUtil.editRecord(userId, sname, rollDevicecheckhistory);
            rollDevicecheckhistoryDao.updateDataRollDevicecheckhistory(rollDevicecheckhistory);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollDevicecheckhistoryByPage(String data) {
        try {
            JRollDevicecheckhistory jrollDevicecheckhistory = JSON.parseObject(data, JRollDevicecheckhistory.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollDevicecheckhistory.getPageIndex();
            Integer pageSize = jrollDevicecheckhistory.getPageSize();
            pageIndex = (pageIndex - 1) * pageSize;

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollDevicecheckhistory.getCondition()) {
                jsonObject = JSON.parseObject(jrollDevicecheckhistory.getCondition().toString());
            }

            // 点检负责人
            String checkManagerNameCondition = null;
            if (!StringUtils.isEmpty(jsonObject.get("check_manager_condition"))) {
                checkManagerNameCondition = jsonObject.get("check_manager_condition").toString();
            }

            // 点检类型
            String checkTypeStr = null;
            Integer checkType = 0;
            if (!StringUtils.isEmpty(jsonObject.get("check_type_condition"))) {
                checkTypeStr = jsonObject.get("check_type_condition").toString();
                checkType = Integer.parseInt(checkTypeStr);
            }

            // 点检计划时间
            String arrivalDateStart = null;// 到货日期开始时间
            if (!StringUtils.isEmpty(jsonObject.get("check_date_start_condition"))) {
                arrivalDateStart = jsonObject.get("check_date_start_condition").toString();
            }

            // 点检计划时间结束
            String arrivalDateEnd = null;// 到货日期开始时间
            if (!StringUtils.isEmpty(jsonObject.get("check_date_end_condition"))) {
                arrivalDateEnd = jsonObject.get("check_date_end_condition").toString();
            }
            List<RollDevicecheckhistory> list = rollDevicecheckhistoryDao.findDataRollDevicecheckhistoryByPage(pageIndex, pageSize, checkManagerNameCondition, checkType, arrivalDateStart, arrivalDateEnd);
            // 重新装配
            for (RollDevicecheckhistory rollDevicecheckhistory : list) {
                Long scheduleId = rollDevicecheckhistory.getCheck_schedule_id();
                Date checkCompleteTime = rollDevicecheckhistory.getCheck_complete_time();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
                String checkCompleteTimeStr = sdf.format(checkCompleteTime);
                rollDevicecheckhistory.setCheckCompleteTimeStr(checkCompleteTimeStr);
                //塞入点检类型（check_type）0/1/2，日检/停机检/年检
                RollDevicecheckschedule rollDevicecheckschedule = rollDevicecheckscheduleDao.findDataRollDevicecheckscheduleByIndocno(scheduleId);
                rollDevicecheckhistory.setCheck_type(rollDevicecheckschedule.getCheck_type());
                //塞入检测是否完成check_complete_ornot
                rollDevicecheckhistory.setCheck_complete_ornot(rollDevicecheckschedule.getCheck_complete_ornot());
                //塞入检测人
                Long modelId = rollDevicecheckschedule.getCheck_model_id();
                RollDeviceCheckModel rollDevicecheckmodel = rollDevicecheckmodelDao.findDataRollDevicecheckmodelByIndocno(modelId);
                rollDevicecheckhistory.setCheck_manager_name(rollDevicecheckmodel.getCheck_manager_name());
                //塞入故障信息
                rollDevicecheckhistory.setBroken_infomation(rollDevicecheckmodel.getSnote());
            }
            Integer count = rollDevicecheckhistoryDao.findDataRollDevicecheckhistoryByPageSize(checkManagerNameCondition, checkType, arrivalDateStart, arrivalDateEnd);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollDevicecheckhistoryByIndocno(String data) {
        try {
            JRollDevicecheckhistory jrollDevicecheckhistory = JSON.parseObject(data, JRollDevicecheckhistory.class);
            Long indocno = jrollDevicecheckhistory.getIndocno();

            RollDevicecheckhistory rollDevicecheckhistory = rollDevicecheckhistoryDao.findDataRollDevicecheckhistoryByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollDevicecheckhistory);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollDevicecheckhistory> findDataRollDevicecheckhistory() {
        List<RollDevicecheckhistory> list = rollDevicecheckhistoryDao.findDataRollDevicecheckhistory();
        return list;
    }

}
