package com.my.business.modular.rollcheckitemusehistory.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollcheckitemusehistory.entity.JRollCheckitemusehistory;
import com.my.business.modular.rollcheckitemusehistory.entity.RollCheckitemusehistory;
import com.my.business.modular.rollcheckitemusehistory.service.RollCheckitemusehistoryService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 点检项使用历史表控制器层
 *
 * @author 生成器生成
 * @date 2020-07-14 16:30:46
 */
@RestController
@RequestMapping("/rollCheckitemusehistory")
public class RollCheckitemusehistoryController {

    @Autowired
    private RollCheckitemusehistoryService rollCheckitemusehistoryService;

    /**
     * 添加记录
     *
     * @param loginToken 用户token
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollCheckitemusehistory(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollCheckitemusehistoryService.insertDataRollCheckitemusehistory(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollCheckitemusehistoryOne(@RequestBody String data) {
        try {
            JRollCheckitemusehistory jrollCheckitemusehistory = JSON.parseObject(data, JRollCheckitemusehistory.class);
            return rollCheckitemusehistoryService.deleteDataRollCheckitemusehistoryOne(jrollCheckitemusehistory.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollCheckitemusehistory jrollCheckitemusehistory = JSON.parseObject(data, JRollCheckitemusehistory.class);
            return rollCheckitemusehistoryService.deleteDataRollCheckitemusehistoryMany(jrollCheckitemusehistory.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollCheckitemusehistory(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollCheckitemusehistoryService.updateDataRollCheckitemusehistory(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollCheckitemusehistoryByPage(@RequestBody String data) {
        return rollCheckitemusehistoryService.findDataRollCheckitemusehistoryByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollCheckitemusehistoryByIndocno(@RequestBody String data) {
        return rollCheckitemusehistoryService.findDataRollCheckitemusehistoryByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollCheckitemusehistory> findDataRollCheckitemusehistory() {
        return rollCheckitemusehistoryService.findDataRollCheckitemusehistory();
    }

}
