package com.my.business.modular.rollcheckitemusehistory.dao;

import com.my.business.modular.rollcheckitemusehistory.entity.RollCheckitemusehistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 点检项使用历史表dao接口
 *
 * @author 生成器生成
 * @date 2020-07-14 16:30:46
 */
@Mapper
public interface RollCheckitemusehistoryDao {

    /**
     * 添加记录
     *
     * @param rollCheckitemusehistory 对象实体
     */
    void insertDataRollCheckitemusehistory(RollCheckitemusehistory rollCheckitemusehistory);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollCheckitemusehistoryOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollCheckitemusehistoryMany(String value);

    /**
     * 修改记录
     *
     * @param rollCheckitemusehistory 对象实体
     */
    void updateDataRollCheckitemusehistory(RollCheckitemusehistory rollCheckitemusehistory);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollCheckitemusehistory> findDataRollCheckitemusehistoryByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollCheckitemusehistoryByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollCheckitemusehistory findDataRollCheckitemusehistoryByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollCheckitemusehistory> findDataRollCheckitemusehistory();

}
