package com.my.business.modular.rollcheckitemusehistory.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.util.Date;

/**
 * 点检项使用历史表实体类
 *
 * @author 生成器生成
 * @date 2020-07-14 16:30:45
 */
public class RollCheckitemusehistory extends BaseEntity {

    private Long indocno;  //主键
    private Long check_dict_id;  //点检项id
    private String check_manager_name;  //点检人
    private Long check_manager_id;  //点检人id
    private Long check_schedule_id;  //点检计划id
    private Date check_latest_time;  //最近点检时间

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getCheck_dict_id() {
        return this.check_dict_id;
    }

    public void setCheck_dict_id(Long check_dict_id) {
        this.check_dict_id = check_dict_id;
    }

    public String getCheck_manager_name() {
        return this.check_manager_name;
    }

    public void setCheck_manager_name(String check_manager_name) {
        this.check_manager_name = check_manager_name;
    }

    public Long getCheck_manager_id() {
        return this.check_manager_id;
    }

    public void setCheck_manager_id(Long check_manager_id) {
        this.check_manager_id = check_manager_id;
    }

    public Long getCheck_schedule_id() {
        return this.check_schedule_id;
    }

    public void setCheck_schedule_id(Long check_schedule_id) {
        this.check_schedule_id = check_schedule_id;
    }

    public Date getCheck_latest_time() {
        return this.check_latest_time;
    }

    public void setCheck_latest_time(Date check_latest_time) {
        this.check_latest_time = check_latest_time;
    }


}