package com.my.business.modular.rollcheckitemusehistory.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollcheckitemusehistory.dao.RollCheckitemusehistoryDao;
import com.my.business.modular.rollcheckitemusehistory.entity.JRollCheckitemusehistory;
import com.my.business.modular.rollcheckitemusehistory.entity.RollCheckitemusehistory;
import com.my.business.modular.rollcheckitemusehistory.service.RollCheckitemusehistoryService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 点检项使用历史表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-07-14 16:30:46
 */
@Service
public class RollCheckitemusehistoryServiceImpl implements RollCheckitemusehistoryService {

    @Autowired
    private RollCheckitemusehistoryDao rollCheckitemusehistoryDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollCheckitemusehistory(String data, Long userId, String sname) {
        try {
            JRollCheckitemusehistory jrollCheckitemusehistory = JSON.parseObject(data, JRollCheckitemusehistory.class);
            RollCheckitemusehistory rollCheckitemusehistory = jrollCheckitemusehistory.getRollCheckitemusehistory();
            CodiUtil.newRecord(userId, sname, rollCheckitemusehistory);
            rollCheckitemusehistoryDao.insertDataRollCheckitemusehistory(rollCheckitemusehistory);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollCheckitemusehistoryOne(Long indocno) {
        try {
            rollCheckitemusehistoryDao.deleteDataRollCheckitemusehistoryOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollCheckitemusehistoryMany(String str_id) {
        try {
            String sql = "delete roll_checkitemusehistory where indocno in(" + str_id + ")";
            rollCheckitemusehistoryDao.deleteDataRollCheckitemusehistoryMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   对象实体
     */
    public ResultData updateDataRollCheckitemusehistory(String data, Long userId, String sname) {
        try {
            JRollCheckitemusehistory jrollCheckitemusehistory = JSON.parseObject(data, JRollCheckitemusehistory.class);
            RollCheckitemusehistory rollCheckitemusehistory = jrollCheckitemusehistory.getRollCheckitemusehistory();
            CodiUtil.editRecord(userId, sname, rollCheckitemusehistory);
            rollCheckitemusehistoryDao.updateDataRollCheckitemusehistory(rollCheckitemusehistory);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollCheckitemusehistoryByPage(String data) {
        try {
            JRollCheckitemusehistory jrollCheckitemusehistory = JSON.parseObject(data, JRollCheckitemusehistory.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollCheckitemusehistory.getPageIndex();
            Integer pageSize = jrollCheckitemusehistory.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollCheckitemusehistory.getCondition()) {
                jsonObject = JSON.parseObject(jrollCheckitemusehistory.getCondition().toString());
            }

            List<RollCheckitemusehistory> list = rollCheckitemusehistoryDao.findDataRollCheckitemusehistoryByPage(pageIndex, pageSize);
            Integer count = rollCheckitemusehistoryDao.findDataRollCheckitemusehistoryByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollCheckitemusehistoryByIndocno(String data) {
        try {
            JRollCheckitemusehistory jrollCheckitemusehistory = JSON.parseObject(data, JRollCheckitemusehistory.class);
            Long indocno = jrollCheckitemusehistory.getIndocno();

            RollCheckitemusehistory rollCheckitemusehistory = rollCheckitemusehistoryDao.findDataRollCheckitemusehistoryByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollCheckitemusehistory);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollCheckitemusehistory> findDataRollCheckitemusehistory() {
        List<RollCheckitemusehistory> list = rollCheckitemusehistoryDao.findDataRollCheckitemusehistory();
        return list;
    }
}
