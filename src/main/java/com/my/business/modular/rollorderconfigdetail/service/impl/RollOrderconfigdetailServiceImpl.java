package com.my.business.modular.rollorderconfigdetail.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollorderconfigdetail.dao.RollOrderconfigdetailDao;
import com.my.business.modular.rollorderconfigdetail.entity.JRollOrderconfigdetail;
import com.my.business.modular.rollorderconfigdetail.entity.RollOrderconfigdetail;
import com.my.business.modular.rollorderconfigdetail.service.RollOrderconfigdetailService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 工单配置子表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-08 15:59:39
 */
@Service
public class RollOrderconfigdetailServiceImpl implements RollOrderconfigdetailService {

    @Autowired
    private RollOrderconfigdetailDao rollOrderconfigdetailDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollOrderconfigdetail(String data, Long userId, String sname) {
        try {
            JRollOrderconfigdetail jrollOrderconfigdetail = JSON.parseObject(data, JRollOrderconfigdetail.class);
            RollOrderconfigdetail rollOrderconfigdetail = jrollOrderconfigdetail.getRollOrderconfigdetail();
            CodiUtil.newRecord(userId, sname, rollOrderconfigdetail);
            rollOrderconfigdetailDao.insertDataRollOrderconfigdetail(rollOrderconfigdetail);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollOrderconfigdetailOne(Long indocno) {
        try {
            rollOrderconfigdetailDao.deleteDataRollOrderconfigdetailOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollOrderconfigdetailMany(String str_id) {
        try {
            String sql = "delete roll_orderconfigdetail where indocno in(" + str_id + ")";
            rollOrderconfigdetailDao.deleteDataRollOrderconfigdetailMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataRollOrderconfigdetail(String data, Long userId, String sname) {
        try {
            JRollOrderconfigdetail jrollOrderconfigdetail = JSON.parseObject(data, JRollOrderconfigdetail.class);
            RollOrderconfigdetail rollOrderconfigdetail = jrollOrderconfigdetail.getRollOrderconfigdetail();
            CodiUtil.editRecord(userId, sname, rollOrderconfigdetail);
            rollOrderconfigdetailDao.updateDataRollOrderconfigdetail(rollOrderconfigdetail);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollOrderconfigdetailByPage(String data) {
        try {
            JRollOrderconfigdetail jrollOrderconfigdetail = JSON.parseObject(data, JRollOrderconfigdetail.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollOrderconfigdetail.getPageIndex();
            Integer pageSize = jrollOrderconfigdetail.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollOrderconfigdetail.getCondition()) {
                jsonObject = JSON.parseObject(jrollOrderconfigdetail.getCondition().toString());
            }
            
            String ilinkno = null;
            if (!StringUtils.isEmpty(jsonObject.get("ilinkno"))) {
            	ilinkno = jsonObject.get("ilinkno").toString();
            }

            List<RollOrderconfigdetail> list = rollOrderconfigdetailDao.findDataRollOrderconfigdetailByPage((pageIndex - 1) * pageSize, pageSize,ilinkno);
            Integer count = rollOrderconfigdetailDao.findDataRollOrderconfigdetailByPageSize(ilinkno);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollOrderconfigdetailByIndocno(String data) {
        try {
            JRollOrderconfigdetail jrollOrderconfigdetail = JSON.parseObject(data, JRollOrderconfigdetail.class);
            Long indocno = jrollOrderconfigdetail.getIndocno();

            RollOrderconfigdetail rollOrderconfigdetail = rollOrderconfigdetailDao.findDataRollOrderconfigdetailByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollOrderconfigdetail);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollOrderconfigdetail> findDataRollOrderconfigdetail() {
        List<RollOrderconfigdetail> list = rollOrderconfigdetailDao.findDataRollOrderconfigdetail();
        return list;
    }

	@Override
	public ResultData findDataRollOrderconfigdetailByIlinkno(String data) {
		try {
			RollOrderconfigdetail rollOrderconfigdetail = JSON.parseObject(data, RollOrderconfigdetail.class);
			List<RollOrderconfigdetail> list = rollOrderconfigdetailDao.findDataRollOrderconfigdetailByIlinkno(rollOrderconfigdetail.getIlinkno());
            return ResultData.ResultDataSuccess(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
	}

}
