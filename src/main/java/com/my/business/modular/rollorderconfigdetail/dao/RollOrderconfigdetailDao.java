package com.my.business.modular.rollorderconfigdetail.dao;

import com.my.business.modular.rollorderconfigdetail.entity.RollOrderconfigdetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 工单配置子表dao接口
 *
 * @author 生成器生成
 * @date 2020-08-08 15:59:39
 */
@Mapper
public interface RollOrderconfigdetailDao {

    /**
     * 添加记录
     *
     * @param rollOrderconfigdetail 对象实体
     */
    void insertDataRollOrderconfigdetail(RollOrderconfigdetail rollOrderconfigdetail);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollOrderconfigdetailOne(@Param("indocno") Long indocno);

    /**
     * 根据外键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollOrderconfigdetailByIlinkno(@Param("ilinkno") String ilinkno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollOrderconfigdetailMany(String value);

    /**
     * 修改记录
     *
     * @param rollOrderconfigdetail 对象实体
     */
    void updateDataRollOrderconfigdetail(RollOrderconfigdetail rollOrderconfigdetail);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollOrderconfigdetail> findDataRollOrderconfigdetailByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("ilinkno") String ilinkno);
    
    /**
     * 根据工单编码查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollOrderconfigdetail> findDataRollOrderconfigdetailByOrderNo(@Param("order_no") String order_no);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollOrderconfigdetailByPageSize(@Param("ilinkno") String ilinkno);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollOrderconfigdetail findDataRollOrderconfigdetailByIndocno(@Param("indocno") Long indocno);
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    List<RollOrderconfigdetail> findDataRollOrderconfigdetailByIlinkno(@Param("ilinkno") String ilinkno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollOrderconfigdetail> findDataRollOrderconfigdetail();
    
    List<RollOrderconfigdetail> findtbdatadetail();

}
