package com.my.business.modular.rollorderconfigdetail.service;

import com.my.business.modular.rollorderconfigdetail.entity.RollOrderconfigdetail;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 工单配置子表接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-08 15:59:39
 */
public interface RollOrderconfigdetailService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollOrderconfigdetail(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollOrderconfigdetailOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollOrderconfigdetailMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollOrderconfigdetail(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollOrderconfigdetailByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollOrderconfigdetailByIndocno(String data);
    
    /**
     * 根据外键查询子表
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollOrderconfigdetailByIlinkno(String data);

    /**
     * 查看记录
     */
    List<RollOrderconfigdetail> findDataRollOrderconfigdetail();
}
