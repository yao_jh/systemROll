package com.my.business.modular.rollorderconfigdetail.entity;

import com.my.business.modular.rollstandard.entity.RollStandard;
import com.my.business.sys.common.entity.BaseEntity;

/**
 * 工单配置子表实体类
 *
 * @author 生成器生成
 * @date 2020-08-08 15:59:39
 */
public class RollOrderconfigdetail extends BaseEntity {

    private Long indocno;  //主键
    private String ilinkno;  //外键
    private Long indexs;  //排序号
    private String field_name;  //字段label
    private String field_id;  //对应工单字段名
    private Long field_typeid;  //字段类型id
    private String field_type;  //字段类型
    private String dic_no;  //数据字典编码
    private Long line;  //行
    private Long columns;  //列
    private Long iconfig;  //外键(关联标准配置主键)
    private String snote;  //备注

    private RollStandard rollStandard;

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getIlinkno() {
        return this.ilinkno;
    }

    public void setIlinkno(String ilinkno) {
        this.ilinkno = ilinkno;
    }

    public String getField_id() {
        return field_id;
    }

    public void setField_id(String field_id) {
        this.field_id = field_id;
    }

    public String getField_name() {
        return this.field_name;
    }

    public void setField_name(String field_name) {
        this.field_name = field_name;
    }

    public Long getField_typeid() {
        return this.field_typeid;
    }

    public void setField_typeid(Long field_typeid) {
        this.field_typeid = field_typeid;
    }

    public String getField_type() {
        return this.field_type;
    }

    public void setField_type(String field_type) {
        this.field_type = field_type;
    }

    public String getDic_no() {
        return this.dic_no;
    }

    public void setDic_no(String dic_no) {
        this.dic_no = dic_no;
    }

    public Long getLine() {
        return this.line;
    }

    public void setLine(Long line) {
        this.line = line;
    }

    public Long getIndexs() {
        return indexs;
    }

    public void setIndexs(Long indexs) {
        this.indexs = indexs;
    }

    public Long getColumns() {
        return columns;
    }

    public void setColumns(Long columns) {
        this.columns = columns;
    }

    public Long getIconfig() {
        return this.iconfig;
    }

    public void setIconfig(Long iconfig) {
        this.iconfig = iconfig;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public RollStandard getRollStandard() {
        return rollStandard;
    }

    public void setRollStandard(RollStandard rollStandard) {
        this.rollStandard = rollStandard;
    }

}