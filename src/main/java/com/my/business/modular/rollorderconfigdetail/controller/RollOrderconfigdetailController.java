package com.my.business.modular.rollorderconfigdetail.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollorderconfigdetail.entity.JRollOrderconfigdetail;
import com.my.business.modular.rollorderconfigdetail.entity.RollOrderconfigdetail;
import com.my.business.modular.rollorderconfigdetail.service.RollOrderconfigdetailService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 工单配置子表控制器层
 *
 * @author 生成器生成
 * @date 2020-08-08 15:59:39
 */
@RestController
@RequestMapping("/rollOrderconfigdetail")
public class RollOrderconfigdetailController {

    @Autowired
    private RollOrderconfigdetailService rollOrderconfigdetailService;

    /**
     * 添加记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollOrderconfigdetail(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollOrderconfigdetailService.insertDataRollOrderconfigdetail(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollOrderconfigdetailOne(@RequestBody String data) {
        try {
            JRollOrderconfigdetail jrollOrderconfigdetail = JSON.parseObject(data, JRollOrderconfigdetail.class);
            return rollOrderconfigdetailService.deleteDataRollOrderconfigdetailOne(jrollOrderconfigdetail.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollOrderconfigdetail jrollOrderconfigdetail = JSON.parseObject(data, JRollOrderconfigdetail.class);
            return rollOrderconfigdetailService.deleteDataRollOrderconfigdetailMany(jrollOrderconfigdetail.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollOrderconfigdetail(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollOrderconfigdetailService.updateDataRollOrderconfigdetail(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollOrderconfigdetailByPage(@RequestBody String data) {
        return rollOrderconfigdetailService.findDataRollOrderconfigdetailByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIlinkno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollOrderconfigdetailByIlinkno(@RequestBody String data) {
        return rollOrderconfigdetailService.findDataRollOrderconfigdetailByIlinkno(data);
    }
    
    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollOrderconfigdetailByIndocno(@RequestBody String data) {
        return rollOrderconfigdetailService.findDataRollOrderconfigdetailByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollOrderconfigdetail> findDataRollOrderconfigdetail() {
        return rollOrderconfigdetailService.findDataRollOrderconfigdetail();
    }

}
