package com.my.business.modular.rollpartsdetail.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollparts.dao.RollPartsDao;
import com.my.business.modular.rollpartsdetail.dao.RollPartsDetailDao;
import com.my.business.modular.rollpartsdetail.entity.JRollPartsDetail;
import com.my.business.modular.rollpartsdetail.entity.RollPartsDetail;
import com.my.business.modular.rollpartsdetail.service.RollPartsDetailService;
import com.my.business.modular.rollpartshistory.dao.RollPartsHistoryDao;
import com.my.business.modular.rollpartshistory.entity.RollPartsHistory;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 设备备件管理详情接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-10-13 16:32:45
 */
@Service
public class RollPartsDetailServiceImpl implements RollPartsDetailService {

    @Autowired
    private RollPartsDetailDao rollPartsDetailDao;
    @Autowired
    private RollPartsDao rollPartsDao;
    @Autowired
    private RollPartsHistoryDao rollPartsHistoryDao;

    /**
     * 添加记录(入库)
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollPartsDetail(String data, Long userId, String sname) {
        try {
            JRollPartsDetail jrollPartsDetail = JSON.parseObject(data, JRollPartsDetail.class);
            RollPartsDetail rollPartsDetail = jrollPartsDetail.getRollPartsDetail();

            //如果是重要设备，所有子表入库，都是新增，并且数量为1
            if (rollPartsDetail.getIf_import() == 1) {
                rollPartsDetail.setEq_count(1L);
                CodiUtil.newRecord(userId, sname, rollPartsDetail);
                rollPartsDetailDao.insertDataRollPartsDetail(rollPartsDetail);

                //插入历史表
                RollPartsHistory rh = new RollPartsHistory();
                BeanUtils.copyProperties(rollPartsDetail, rh);
                rollPartsHistoryDao.insertDataRollPartsHistory(rh);

                //更新主表信息
                Long s = rollPartsDetail.getIstatus_in();  //获取入库状态
                Long ilinkno = rollPartsDetail.getIlinkno();//获取子表外键，也就是主表主键
                Long num = 1L; //入库数量
                if (s == 1L) {
                    rollPartsDao.updateDataRollPartsA(ilinkno, num, 1L, null, null, null, null, null); //入库状态为新备件时，新备件数量+1
                } else if (s == 2L) {
                    rollPartsDao.updateDataRollPartsA(ilinkno, num, null, 1L, null, null, null, 1L); //入库状态为旧备件时，旧备件数量+1，在机备件-1
                } else if (s == 3L) {
                    rollPartsDao.updateDataRollPartsA(ilinkno, num, null, null, 1L, null, 1L, null); //入库状态为修复备件时，旧备件数量+1，送修备件-1
                } else {
                    rollPartsDao.updateDataRollPartsA(ilinkno, num, null, 1L, null, null, null, 1L); //入库状态为损坏备件时，损坏备件数量+1，在机备件-1
                }
            } else {
                //如果不是重要设备，所有子表入库，查询是否有相同数据
                Long i = rollPartsDetailDao.findDataRollPartsDetailByName(rollPartsDetail.getEq_name(), rollPartsDetail.getEq_code(), rollPartsDetail.getIlinkno());
                if (i == 0L) {
                    //查询没有相同数据，则插入新数据
                    rollPartsDetail.setEq_count(rollPartsDetail.getEq_in()); //入库数量写入备件数量
                    CodiUtil.newRecord(userId, sname, rollPartsDetail);
                    rollPartsDetailDao.insertDataRollPartsDetail(rollPartsDetail);

                    //更新主表信息
                    Long s = rollPartsDetail.getIstatus_in();  //获取入库状态
                    Long ilinkno = rollPartsDetail.getIlinkno();//获取子表外键，也就是主表主键
                    Long num = rollPartsDetail.getEq_in(); //入库数量
                    if (s == 1L) {
                        //新备件
                        rollPartsDao.updateDataRollPartsA(ilinkno, num, 1L, null, null, null, null, null); //入库状态为新备件时
                    } else if (s == 2L) {
                        //旧备件
                        rollPartsDao.updateDataRollPartsA(ilinkno, num, null, 1L, null, null, null, 1L); //入库状态为旧备件时
                    } else if (s == 3L) {
                        //维修备件
                        rollPartsDao.updateDataRollPartsA(ilinkno, num, null, 1L, null, null, 1L, null); //入库状态为修复备件时
                    } else {
                        //损坏备件
                        rollPartsDao.updateDataRollPartsA(ilinkno, num, null, 1L, null, null, null, 1L); //入库状态为损坏备件时
                    }
                } else if (i == 1L) {
                    //查询有且只有一条相同数据，则更新数量
                    //获取旧记录
                    RollPartsDetail r = rollPartsDetailDao.findDataRollPartsDetailByR(rollPartsDetail.getEq_name(), rollPartsDetail.getEq_code(), rollPartsDetail.getIlinkno());
                    //计算新数量
                    Long num = r.getEq_count() + rollPartsDetail.getEq_in();
                    //赋值
                    rollPartsDetail.setEq_count(num);
                    rollPartsDetail.setIndocno(r.getIndocno());
                    CodiUtil.editRecord(userId, sname, rollPartsDetail);
                    //更新数量
                    rollPartsDetailDao.updateDataRollPartsDetailForNum(rollPartsDetail);
                } else {
                    return ResultData.ResultDataSuccessSelf("列表包含重复数据", "备件编号为：" + rollPartsDetail.getEq_code());
                }
            }

            return ResultData.ResultDataSuccessSelf("入库操作完成", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("入库失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollPartsDetailOne(Long indocno) {
        try {
            rollPartsDetailDao.deleteDataRollPartsDetailOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollPartsDetailMany(String str_id) {
        try {
            String sql = "delete roll_parts_detail where indocno in(" + str_id + ")";
            rollPartsDetailDao.deleteDataRollPartsDetailMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollPartsDetail(String data, Long userId, String sname) {
        try {
            JRollPartsDetail jrollPartsDetail = JSON.parseObject(data, JRollPartsDetail.class);
            RollPartsDetail rollPartsDetail = jrollPartsDetail.getRollPartsDetail();
            CodiUtil.editRecord(userId, sname, rollPartsDetail);
            rollPartsDetailDao.updateDataRollPartsDetail(rollPartsDetail);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPartsDetailByPage(String data) {
        try {
            JRollPartsDetail jrollPartsDetail = JSON.parseObject(data, JRollPartsDetail.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollPartsDetail.getPageIndex();
            Integer pageSize = jrollPartsDetail.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollPartsDetail.getCondition()) {
                jsonObject = JSON.parseObject(jrollPartsDetail.getCondition().toString());
            }

            Long ilinkno = null;
            if (!StringUtils.isEmpty(jsonObject.get("ilinkno"))) {
                ilinkno = Long.valueOf(jsonObject.get("ilinkno").toString());
            }

            String eq_name = null;
            if (!StringUtils.isEmpty(jsonObject.get("eq_name"))) {
                eq_name = jsonObject.get("eq_name").toString();
            }

            String eq_code = null;
            if (!StringUtils.isEmpty(jsonObject.get("eq_code"))) {
                eq_code = jsonObject.get("eq_code").toString();
            }

            String main_name = null;
            if (!StringUtils.isEmpty(jsonObject.get("main_name"))) {
                main_name = jsonObject.get("main_name").toString();
            }

            String main_code = null;
            if (!StringUtils.isEmpty(jsonObject.get("main_code"))) {
                main_code = jsonObject.get("main_code").toString();
            }

            List<RollPartsDetail> list = rollPartsDetailDao.findDataRollPartsDetailByPage((pageIndex - 1) * pageSize, pageSize, eq_name, ilinkno, eq_code, main_name, main_code);
            Integer count = rollPartsDetailDao.findDataRollPartsDetailByPageSize(eq_name, ilinkno, eq_code, main_name, main_code);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPartsDetailByIndocno(String data) {
        try {
            JRollPartsDetail jrollPartsDetail = JSON.parseObject(data, JRollPartsDetail.class);
            Long indocno = jrollPartsDetail.getIndocno();

            RollPartsDetail rollPartsDetail = rollPartsDetailDao.findDataRollPartsDetailByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollPartsDetail);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollPartsDetail> findDataRollPartsDetail() {
        List<RollPartsDetail> list = rollPartsDetailDao.findDataRollPartsDetail();
        return list;
    }

    /**
     * 出库
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData OutDataRollPartsDetail(String data, Long userId, String sname) {
        try {
            JRollPartsDetail jrollPartsDetail = JSON.parseObject(data, JRollPartsDetail.class);
            RollPartsDetail rollPartsDetail = jrollPartsDetail.getRollPartsDetail();

            Long indocno = rollPartsDetail.getIndocno(); //获取主键
            RollPartsDetail r = rollPartsDetailDao.findDataRollPartsDetailByIndocno(indocno);  //查询单条记录（当前行的旧记录）

            Long num_old = r.getEq_count();  //原备件数量
            Long num_new = rollPartsDetail.getEq_out();  //出库数量

            //判断库存数量是否为0
            //数量不为0
            if (r.getEq_count() != 0L) {
                //判断出库后数量是否为负数
                Long num = num_old - num_new;
                if (num >= 0L) {
                    //赋值，计算后的库存数量
                    rollPartsDetail.setEq_count(num);
                    SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String date = sDateFormat.format(new Date());
                    rollPartsDetail.setOuttime(date);
                    CodiUtil.editRecord(userId, sname, rollPartsDetail);
                    rollPartsDetailDao.updateDataRollPartsDetail(rollPartsDetail);
                    //插入历史表
                    RollPartsHistory rh = new RollPartsHistory();
                    BeanUtils.copyProperties(rollPartsDetail, rh);
                    rollPartsHistoryDao.insertDataRollPartsHistory(rh);

                    //更新主表信息
                    //获取出库去向
                    Long out = rollPartsDetail.getIstatus_out();
                    //获取主表主键
                    Long ilnk = rollPartsDetail.getIlinkno();
                    //获取原数据入库状态
                    Long in = r.getIstatus_in();
                    //出库计算
                    if (out == 1L) {
                        //送修
                        //原入库状态判断
                        if (in == 1L) {
                            //新备件
                            rollPartsDao.updateDataRollPartsM(ilnk, num_new, 1L, null, null, null, 1L, null);
                        } else if (in == 2L) {
                            //旧备件
                            rollPartsDao.updateDataRollPartsM(ilnk, num_new, null, 1L, null, null, 1L, null);
                        } else if (in == 3L) {
                            //修复备件
                            rollPartsDao.updateDataRollPartsM(ilnk, num_new, null, 1L, null, null, 1L, null);
                        } else {
                            //损坏备件
                            rollPartsDao.updateDataRollPartsM(ilnk, num_new, null, null, 1L, null, 1L, null);
                        }
                    } else if (out == 2L) {
                        //报废
                        //原入库状态判断
                        if (in == 1L) {
                            //新备件
                            rollPartsDao.updateDataRollPartsM(ilnk, num_new, 1L, null, null, 1L, null, null);
                        } else if (in == 2L) {
                            //旧备件
                            rollPartsDao.updateDataRollPartsM(ilnk, num_new, null, 1L, null, 1L, null, null);
                        } else if (in == 3L) {
                            //修复备件
                            rollPartsDao.updateDataRollPartsM(ilnk, num_new, null, 1L, null, 1L, null, null);
                        } else {
                            //损坏备件
                            rollPartsDao.updateDataRollPartsM(ilnk, num_new, null, null, 1L, 1L, null, null);
                        }
                    } else if (out == 3L) {
                        //上机
                        //原入库状态判断
                        if (in == 1L) {
                            //新备件
                            rollPartsDao.updateDataRollPartsM(ilnk, num_new, 1L, null, null, null, null, 1L);
                        } else if (in == 2L) {
                            //旧备件
                            rollPartsDao.updateDataRollPartsM(ilnk, num_new, null, 1L, null, null, null, 1L);
                        } else if (in == 3L) {
                            //修复备件
                            rollPartsDao.updateDataRollPartsM(ilnk, num_new, null, 1L, null, null, null, 1L);
                        } else {
                            //损坏备件
                            rollPartsDao.updateDataRollPartsM(ilnk, num_new, null, null, 1L, null, null, 1L);
                        }
                    }

                    return ResultData.ResultDataSuccessSelf("出库完成", null);
                } else {
                    return ResultData.ResultDataSuccessSelf("数量不足，不能出库", null);
                }
            } else {
                //数量为0
                return ResultData.ResultDataSuccessSelf("数量不足，不能出库", null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据轴承座号查看记录
     *
     * @param data json字符串
     */
    public List<RollPartsDetail> findDataRollPartsDetailByChock(String data) {
        JRollPartsDetail jrollPartsDetail = JSON.parseObject(data, JRollPartsDetail.class);
        JSONObject jsonObject = null;

        if (null != jrollPartsDetail.getCondition()) {
            jsonObject = JSON.parseObject(jrollPartsDetail.getCondition().toString());
        }

        String main_code = null;
        if (!StringUtils.isEmpty(jsonObject.get("main_code"))) {
            main_code = jsonObject.get("main_code").toString();
        }

        List<RollPartsDetail> list = rollPartsDetailDao.findDataRollPartsDetailByChock(main_code);
        return list;
    }
}
