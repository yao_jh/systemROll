package com.my.business.modular.rollpartsdetail.dao;

import com.my.business.modular.rollpartsdetail.entity.RollPartsDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 设备备件管理详情dao接口
 *
 * @author 生成器生成
 * @date 2020-10-13 16:32:45
 */
@Mapper
public interface RollPartsDetailDao {

    /**
     * 添加记录
     *
     * @param rollPartsDetail 对象实体
     */
    void insertDataRollPartsDetail(RollPartsDetail rollPartsDetail);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollPartsDetailOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollPartsDetailMany(String value);

    /**
     * 修改记录
     *
     * @param rollPartsDetail 对象实体
     */
    void updateDataRollPartsDetail(RollPartsDetail rollPartsDetail);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollPartsDetail> findDataRollPartsDetailByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("eq_name") String eq_name, @Param("ilinkno") Long ilinkno, @Param("eq_code") String eq_code, @Param("main_name") String main_name, @Param("main_code") String main_code);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollPartsDetailByPageSize(@Param("eq_name") String eq_name, @Param("ilinkno") Long ilinkno, @Param("eq_code") String eq_code, @Param("main_name") String main_name, @Param("main_code") String main_code);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollPartsDetail findDataRollPartsDetailByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollPartsDetail> findDataRollPartsDetail();

    /**
     * 入库时查找有无旧记录
     *
     * @param eq_name 备件名称
     * @param eq_code 备件编码
     * @param ilinkno 主表主键
     * @return
     */
    Long findDataRollPartsDetailByName(@Param("eq_name") String eq_name, @Param("eq_code") String eq_code, @Param("ilinkno") Long ilinkno);

    /**
     * 入库时查找旧记录信息
     *
     * @param eq_name 备件名称
     * @param eq_code 备件编码
     * @param ilinkno 主表主键
     * @return
     */
    RollPartsDetail findDataRollPartsDetailByR(@Param("eq_name") String eq_name, @Param("eq_code") String eq_code, @Param("ilinkno") Long ilinkno);

    /**
     * 修改记录
     *
     * @param rollPartsDetail 对象实体
     */
    void updateDataRollPartsDetailForNum(RollPartsDetail rollPartsDetail);

    /**
     * 根据轴承座号查看记录
     *
     * @param main_code json字符串
     */
    List<RollPartsDetail> findDataRollPartsDetailByChock(@Param("main_code") String main_code);
}
