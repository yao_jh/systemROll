package com.my.business.modular.rollpartsdetail.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollpartsdetail.entity.JRollPartsDetail;
import com.my.business.modular.rollpartsdetail.entity.RollPartsDetail;
import com.my.business.modular.rollpartsdetail.service.RollPartsDetailService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 设备备件管理详情控制器层
 *
 * @author 生成器生成
 * @date 2020-10-13 16:32:45
 */
@RestController
@RequestMapping("/rollPartsDetail")
public class RollPartsDetailController {

    @Autowired
    private RollPartsDetailService rollPartsDetailService;

    /**
     * 添加记录(入库)
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollPartsDetail(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollPartsDetailService.insertDataRollPartsDetail(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollPartsDetailOne(@RequestBody String data) {
        try {
            JRollPartsDetail jrollPartsDetail = JSON.parseObject(data, JRollPartsDetail.class);
            return rollPartsDetailService.deleteDataRollPartsDetailOne(jrollPartsDetail.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollPartsDetail jrollPartsDetail = JSON.parseObject(data, JRollPartsDetail.class);
            return rollPartsDetailService.deleteDataRollPartsDetailMany(jrollPartsDetail.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollPartsDetail(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollPartsDetailService.updateDataRollPartsDetail(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollPartsDetailByPage(@RequestBody String data) {
        return rollPartsDetailService.findDataRollPartsDetailByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollPartsDetailByIndocno(@RequestBody String data) {
        return rollPartsDetailService.findDataRollPartsDetailByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollPartsDetail> findDataRollPartsDetail() {
        return rollPartsDetailService.findDataRollPartsDetail();
    }

    /**
     * 出库
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/out"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData OutDataRollPartsDetail(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollPartsDetailService.OutDataRollPartsDetail(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据轴承座号查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByChock"}, method = RequestMethod.POST)
    @ResponseBody
    public List<RollPartsDetail> findDataRollPartsDetailByChock(@RequestBody String data) {
        return rollPartsDetailService.findDataRollPartsDetailByChock(data);
    }
}
