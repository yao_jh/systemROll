package com.my.business.modular.stiffnessstandard.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.stiffnessproject.dao.StiffnessProjectDao;
import com.my.business.modular.stiffnessproject.entity.StiffnessProject;
import com.my.business.modular.stiffnessscore.dao.StiffnessScoreDao;
import com.my.business.modular.stiffnessstandard.dao.StiffnessStandardDao;
import com.my.business.modular.stiffnessstandard.entity.JStiffnessStandard;
import com.my.business.modular.stiffnessstandard.entity.StiffnessStandard;
import com.my.business.modular.stiffnessstandard.service.StiffnessStandardService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 轧辊刚度评价标准接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-08 10:44:57
 */
@Service
public class StiffnessStandardServiceImpl implements StiffnessStandardService {

    @Autowired
    private StiffnessStandardDao stiffnessStandardDao;
    @Autowired
    private StiffnessProjectDao stiffnessProjectDao;
    @Autowired
    private StiffnessScoreDao stiffnessScoreDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataStiffnessStandard(String data, Long userId, String sname) {
        try {
            JStiffnessStandard jstiffnessStandard = JSON.parseObject(data, JStiffnessStandard.class);
            StiffnessStandard stiffnessStandard = jstiffnessStandard.getStiffnessStandard();
            CodiUtil.newRecord(userId, sname, stiffnessStandard);
            stiffnessStandardDao.insertDataStiffnessStandard(stiffnessStandard);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataStiffnessStandardOne(Long indocno) {
        try {
            stiffnessStandardDao.deleteDataStiffnessStandardOne(indocno);//删除主表数据
            List<StiffnessProject> list = stiffnessProjectDao.findDataStiffnessProjectByIlinkno(indocno);//查询1号子表对应主键集合
            for (StiffnessProject entity : list) {//遍历集合，取出主键
                Long ilinkno = entity.getIndocno();
                stiffnessScoreDao.deleteDataStiffnessScoreByilinkno(ilinkno);//1号子表的主键对应2号子表的外键，关联删除2号子表数据
            }
            stiffnessProjectDao.deleteDataStiffnessProjectByilinkno(indocno);//删除1号子表数据
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataStiffnessStandardMany(String str_id) {
        try {
            String sql = "delete stiffness_standard where indocno in(" + str_id + ")";
            stiffnessStandardDao.deleteDataStiffnessStandardMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataStiffnessStandard(String data, Long userId, String sname) {
        try {
            JStiffnessStandard jstiffnessStandard = JSON.parseObject(data, JStiffnessStandard.class);
            StiffnessStandard stiffnessStandard = jstiffnessStandard.getStiffnessStandard();
            CodiUtil.editRecord(userId, sname, stiffnessStandard);
            stiffnessStandardDao.updateDataStiffnessStandard(stiffnessStandard);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataStiffnessStandardByPage(String data) {
        try {
            JStiffnessStandard jstiffnessStandard = JSON.parseObject(data, JStiffnessStandard.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jstiffnessStandard.getPageIndex();
            Integer pageSize = jstiffnessStandard.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jstiffnessStandard.getCondition()) {
                jsonObject = JSON.parseObject(jstiffnessStandard.getCondition().toString());
            }

            Long roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = Long.valueOf(jsonObject.get("roll_typeid").toString());
            }
            Long production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
            }
            Long frame_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
                frame_noid = Long.valueOf(jsonObject.get("frame_noid").toString());
            }
            List<StiffnessStandard> list = stiffnessStandardDao.findDataStiffnessStandardByPage((pageIndex - 1)*pageSize, pageSize, roll_typeid, production_line_id, frame_noid);
            Integer count = stiffnessStandardDao.findDataStiffnessStandardByPageSize(roll_typeid, production_line_id, frame_noid);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataStiffnessStandardByIndocno(String data) {
        try {
            JStiffnessStandard jstiffnessStandard = JSON.parseObject(data, JStiffnessStandard.class);
            Long indocno = jstiffnessStandard.getIndocno();

            StiffnessStandard stiffnessStandard = stiffnessStandardDao.findDataStiffnessStandardByIndocno(indocno);
            return ResultData.ResultDataSuccess(stiffnessStandard);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<StiffnessStandard> findDataStiffnessStandard() {
        List<StiffnessStandard> list = stiffnessStandardDao.findDataStiffnessStandard();
        return list;
    }

}
