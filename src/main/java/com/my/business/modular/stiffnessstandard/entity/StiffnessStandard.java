package com.my.business.modular.stiffnessstandard.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轧辊刚度评价标准实体类
 *
 * @author 生成器生成
 * @date 2020-09-08 10:44:56
 */
public class StiffnessStandard extends BaseEntity {

    private Long indocno;  //主键
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private Long production_line_id;  //产线id
    private String production_line;  //产线
    private Long frame_noid;  //机架号id
    private String frame_no;  //机架号

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getRoll_typeid() {
        return this.roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public Long getProduction_line_id() {
        return this.production_line_id;
    }

    public void setProduction_line_id(Long production_line_id) {
        this.production_line_id = production_line_id;
    }

    public String getProduction_line() {
        return this.production_line;
    }

    public void setProduction_line(String production_line) {
        this.production_line = production_line;
    }

    public Long getFrame_noid() {
        return this.frame_noid;
    }

    public void setFrame_noid(Long frame_noid) {
        this.frame_noid = frame_noid;
    }

    public String getFrame_no() {
        return this.frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }


}