package com.my.business.modular.stiffnessstandard.service;

import com.my.business.modular.stiffnessstandard.entity.StiffnessStandard;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 轧辊刚度评价标准接口服务类
 *
 * @author 生成器生成
 * @date 2020-09-08 10:44:57
 */
public interface StiffnessStandardService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataStiffnessStandard(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataStiffnessStandardOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataStiffnessStandardMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataStiffnessStandard(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataStiffnessStandardByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataStiffnessStandardByIndocno(String data);

    /**
     * 查看记录
     */
    List<StiffnessStandard> findDataStiffnessStandard();
}
