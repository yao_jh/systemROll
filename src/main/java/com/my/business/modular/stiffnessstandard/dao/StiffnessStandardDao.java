package com.my.business.modular.stiffnessstandard.dao;

import com.my.business.modular.stiffnessstandard.entity.StiffnessStandard;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轧辊刚度评价标准dao接口
 *
 * @author 生成器生成
 * @date 2020-09-08 10:44:57
 */
@Mapper
public interface StiffnessStandardDao {

    /**
     * 添加记录
     *
     * @param stiffnessStandard 对象实体
     */
    void insertDataStiffnessStandard(StiffnessStandard stiffnessStandard);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataStiffnessStandardOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataStiffnessStandardMany(String value);

    /**
     * 修改记录
     *
     * @param stiffnessStandard 对象实体
     */
    void updateDataStiffnessStandard(StiffnessStandard stiffnessStandard);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex          第几页
     * @param pageSize           每页总数
     * @param roll_typeid
     * @param production_line_id
     * @param frame_noid
     * @return 对象数据集合
     */
    List<StiffnessStandard> findDataStiffnessStandardByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("roll_typeid") Long roll_typeid, @Param("production_line_id") Long production_line_id, @Param("frame_noid") Long frame_noid);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataStiffnessStandardByPageSize(@Param("roll_typeid") Long roll_typeid, @Param("production_line_id") Long production_line_id, @Param("frame_noid") Long frame_noid);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    StiffnessStandard findDataStiffnessStandardByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<StiffnessStandard> findDataStiffnessStandard();

    /**
     * 根据机架号、产线查询标准主表主键
     *
     * @param frame_noid         机架号
     * @param production_line_id 产线
     * @return indocno
     */
    Long findIndocno(@Param("frame_noid") Long frame_noid, @Param("production_line_id") Long production_line_id);
}
