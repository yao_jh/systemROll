package com.my.business.modular.vrollstock.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.vrollstock.dao.VRollStockDao;
import com.my.business.modular.vrollstock.entity.JVRollStock;
import com.my.business.modular.vrollstock.entity.VRollStock;
import com.my.business.modular.vrollstock.service.VRollStockService;
import com.my.business.sys.common.entity.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 轧辊仓库管理接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-14 10:16:39
 */
@Service
public class VRollStockServiceImpl implements VRollStockService {

    @Autowired
    private VRollStockDao vRollStockDao;

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataVRollStockByPage(String data) {
        try {
            JVRollStock jvRollStock = JSON.parseObject(data, JVRollStock.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jvRollStock.getPageIndex();
            Integer pageSize = jvRollStock.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jvRollStock.getCondition()) {
                jsonObject = JSON.parseObject(jvRollStock.getCondition().toString());
            }
            List<VRollStock> list = vRollStockDao.findDataVRollStockByPage((pageIndex - 1) * pageSize, pageSize);
            List<VRollStock> lists = vRollStockDao.findDataVRollStock();
            VRollStock total = new VRollStock();
            VRollStock stotal = new VRollStock();
            Long f1tof4_work_roll = 0L;
            Long f1tof4_backup_roll = 0L;
            Long f5tof7_work_roll = 0L;
            Long f5tof7_backup_roll = 0L;
            Long r1_r2_work_roll = 0L;
            Long r1_r2_backup_roll = 0L;
            Long temper_mill_work_roll = 0L;
            Long temper_mill_backup_roll = 0L;
            Long F_vertical_roll = 0L;
            Long R_vertical_roll = 0L;
            Long hammer = 0L;
            for (VRollStock s : lists) {
                f1tof4_work_roll = f1tof4_work_roll + s.getF1tof4_work_roll();
                f1tof4_backup_roll = f1tof4_backup_roll + s.getF1tof4_backup_roll();
                f5tof7_work_roll = f5tof7_work_roll + s.getF5tof7_work_roll();
                f5tof7_backup_roll = f5tof7_backup_roll + s.getF5tof7_backup_roll();
                r1_r2_work_roll = r1_r2_work_roll + s.getR1_r2_work_roll();
                r1_r2_backup_roll = r1_r2_backup_roll + s.getR1_r2_backup_roll();
                temper_mill_work_roll = temper_mill_work_roll + s.getTemper_mill_work_roll();
                temper_mill_backup_roll = temper_mill_backup_roll + s.getTemper_mill_backup_roll();
                F_vertical_roll = F_vertical_roll + s.getF_vertical_roll();
                R_vertical_roll = R_vertical_roll + s.getR_vertical_roll();
                hammer = hammer + s.getHammer();
            }
            total.setIndocno(98L);
            total.setS_roll_state("库存量");
            total.setF1tof4_work_roll(f1tof4_work_roll);
            total.setF1tof4_backup_roll(f1tof4_backup_roll);
            total.setF5tof7_work_roll(f5tof7_work_roll);
            total.setF5tof7_backup_roll(f5tof7_backup_roll);
            total.setR1_r2_work_roll(r1_r2_work_roll);
            total.setR1_r2_backup_roll(r1_r2_backup_roll);
            total.setTemper_mill_work_roll(temper_mill_work_roll);
            total.setTemper_mill_backup_roll(temper_mill_backup_roll);
            total.setF_vertical_roll(F_vertical_roll);
            total.setR_vertical_roll(R_vertical_roll);
            total.setHammer(hammer);
            list.add(total);

            Long sf1tof4_work_roll = 0L;
            Long sf1tof4_backup_roll = 0L;
            Long sf5tof7_work_roll = 0L;
            Long sf5tof7_backup_roll = 0L;
            Long sr1_r2_work_roll = 0L;
            Long sr1_r2_backup_roll = 0L;
            Long stemper_mill_work_roll = 0L;
            Long stemper_mill_backup_roll = 0L;
            Long sf_vertical_roll = 0L;
            Long sr_vertical_roll = 0L;
            Long shammer = 0L;
            for (VRollStock s : list) {
                sf1tof4_work_roll = sf1tof4_work_roll + s.getF1tof4_work_roll();
                sf1tof4_backup_roll = sf1tof4_backup_roll + s.getF1tof4_backup_roll();
                sf5tof7_work_roll = sf5tof7_work_roll + s.getF5tof7_work_roll();
                sf5tof7_backup_roll = sf5tof7_backup_roll + s.getF5tof7_backup_roll();
                sr1_r2_work_roll = sr1_r2_work_roll + s.getR1_r2_work_roll();
                sr1_r2_backup_roll = sr1_r2_backup_roll + s.getR1_r2_backup_roll();
                stemper_mill_work_roll = stemper_mill_work_roll + s.getTemper_mill_work_roll();
                stemper_mill_backup_roll = stemper_mill_backup_roll + s.getTemper_mill_backup_roll();
                sf_vertical_roll = sf_vertical_roll + s.getF_vertical_roll();
                sr_vertical_roll = sr_vertical_roll + s.getR_vertical_roll();
                shammer = shammer + s.getHammer();
            }
            stotal.setIndocno(99L);
            stotal.setS_roll_state("总计");
            stotal.setF1tof4_work_roll(sf1tof4_work_roll);
            stotal.setF1tof4_backup_roll(sf1tof4_backup_roll);
            stotal.setF5tof7_work_roll(sf5tof7_work_roll);
            stotal.setF5tof7_backup_roll(sf5tof7_backup_roll);
            stotal.setR1_r2_work_roll(sr1_r2_work_roll);
            stotal.setR1_r2_backup_roll(sr1_r2_backup_roll);
            stotal.setTemper_mill_work_roll(stemper_mill_work_roll);
            stotal.setTemper_mill_backup_roll(stemper_mill_backup_roll);
            stotal.setF_vertical_roll(sf_vertical_roll);
            stotal.setR_vertical_roll(sr_vertical_roll);
            stotal.setHammer(shammer);
            list.add(stotal);
            Integer count = vRollStockDao.findDataVRollStockByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataVRollStockByIndocno(String data) {
        try {
            JVRollStock jvRollStock = JSON.parseObject(data, JVRollStock.class);
            Long indocno = jvRollStock.getIndocno();

            VRollStock vRollStock = vRollStockDao.findDataVRollStockByIndocno(indocno);
            return ResultData.ResultDataSuccess(vRollStock);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<VRollStock> findDataVRollStock() {
        List<VRollStock> list = vRollStockDao.findDataVRollStock();
        return list;
    }

}
