package com.my.business.modular.vrollstock.controller;

import com.my.business.modular.vrollstock.entity.VRollStock;
import com.my.business.modular.vrollstock.service.VRollStockService;
import com.my.business.sys.common.entity.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 轧辊仓库管理控制器层
 *
 * @author 生成器生成
 * @date 2020-08-14 10:16:39
 */
@RestController
@RequestMapping("/vRollStock")
public class VRollStockController {

    @Autowired
    private VRollStockService vRollStockService;


    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataVRollStockByPage(@RequestBody String data) {
        return vRollStockService.findDataVRollStockByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataVRollStockByIndocno(@RequestBody String data) {
        return vRollStockService.findDataVRollStockByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<VRollStock> findDataVRollStock() {
        return vRollStockService.findDataVRollStock();
    }

}
