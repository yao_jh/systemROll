package com.my.business.modular.vrollstock.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轧辊仓库管理实体类
 *
 * @author 生成器生成
 * @date 2020-08-14 10:16:38
 */
public class VRollStock extends BaseEntity {

    private Long indocno;  //主键
    private String s_roll_state;  //轧辊状态
    private Long f1tof4_work_roll;  //F1~F4工作辊
    private Long f1tof4_backup_roll;  //F1~F4支撑辊
    private Long f5tof7_work_roll;  //F5~F8工作辊
    private Long f5tof7_backup_roll;  //F5~F8支撑辊
    private Long r1_r2_work_roll;  //粗轧工作辊
    private Long r1_r2_backup_roll;  //粗轧支撑辊
    private Long temper_mill_work_roll;  //平整工作辊
    private Long temper_mill_backup_roll;  //平整支撑辊
    private Long F_vertical_roll;  //精轧立辊
    private Long R_vertical_roll;  //粗轧立辊
    private Long hammer;  //锤头

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getS_roll_state() {
        return this.s_roll_state;
    }

    public void setS_roll_state(String s_roll_state) {
        this.s_roll_state = s_roll_state;
    }

    public Long getF1tof4_work_roll() {
        return this.f1tof4_work_roll;
    }

    public void setF1tof4_work_roll(Long f1tof4_work_roll) {
        this.f1tof4_work_roll = f1tof4_work_roll;
    }

    public Long getF1tof4_backup_roll() {
        return this.f1tof4_backup_roll;
    }

    public void setF1tof4_backup_roll(Long f1tof4_backup_roll) {
        this.f1tof4_backup_roll = f1tof4_backup_roll;
    }

    public Long getF5tof7_work_roll() {
        return this.f5tof7_work_roll;
    }

    public void setF5tof7_work_roll(Long f5tof7_work_roll) {
        this.f5tof7_work_roll = f5tof7_work_roll;
    }

    public Long getF5tof7_backup_roll() {
        return this.f5tof7_backup_roll;
    }

    public void setF5tof7_backup_roll(Long f5tof7_backup_roll) {
        this.f5tof7_backup_roll = f5tof7_backup_roll;
    }

    public Long getTemper_mill_work_roll() {
        return this.temper_mill_work_roll;
    }

    public void setTemper_mill_work_roll(Long temper_mill_work_roll) {
        this.temper_mill_work_roll = temper_mill_work_roll;
    }

    public Long getTemper_mill_backup_roll() {
        return this.temper_mill_backup_roll;
    }

    public void setTemper_mill_backup_roll(Long temper_mill_backup_roll) {
        this.temper_mill_backup_roll = temper_mill_backup_roll;
    }

    public Long getHammer() {
        return this.hammer;
    }

    public void setHammer(Long hammer) {
        this.hammer = hammer;
    }

    public Long getR1_r2_work_roll() {
        return r1_r2_work_roll;
    }

    public void setR1_r2_work_roll(Long r1_r2_work_roll) {
        this.r1_r2_work_roll = r1_r2_work_roll;
    }

    public Long getR1_r2_backup_roll() {
        return r1_r2_backup_roll;
    }

    public void setR1_r2_backup_roll(Long r1_r2_backup_roll) {
        this.r1_r2_backup_roll = r1_r2_backup_roll;
    }

    public Long getF_vertical_roll() {
        return F_vertical_roll;
    }

    public void setF_vertical_roll(Long f_vertical_roll) {
        F_vertical_roll = f_vertical_roll;
    }

    public Long getR_vertical_roll() {
        return R_vertical_roll;
    }

    public void setR_vertical_roll(Long r_vertical_roll) {
        R_vertical_roll = r_vertical_roll;
    }
}