package com.my.business.modular.vrollstock.service;

import com.my.business.modular.vrollstock.entity.VRollStock;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 轧辊仓库管理接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-14 10:16:39
 */
public interface VRollStockService {

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataVRollStockByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataVRollStockByIndocno(String data);

    /**
     * 查看记录
     */
    List<VRollStock> findDataVRollStock();
}
