package com.my.business.modular.vrollstock.dao;

import com.my.business.modular.vrollstock.entity.VRollStock;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轧辊仓库管理dao接口
 *
 * @author 生成器生成
 * @date 2020-08-14 10:16:39
 */
@Mapper
public interface VRollStockDao {


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<VRollStock> findDataVRollStockByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataVRollStockByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    VRollStock findDataVRollStockByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<VRollStock> findDataVRollStock();

}
