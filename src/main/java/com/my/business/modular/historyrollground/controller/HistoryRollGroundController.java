package com.my.business.modular.historyrollground.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.historyrollground.entity.HistoryRollGround;
import com.my.business.modular.historyrollground.entity.JHistoryRollGround;
import com.my.business.modular.historyrollground.service.HistoryRollGroundService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 轧辊磨削历史控制器层
 *
 * @author 生成器生成
 * @date 2020-08-26 11:11:08
 */
@RestController
@RequestMapping("/historyRollGround")
public class HistoryRollGroundController {

    @Autowired
    private HistoryRollGroundService historyRollGroundService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataHistoryRollGround(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return historyRollGroundService.insertDataHistoryRollGround(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataHistoryRollGroundOne(@RequestBody String data) {
        try {
            JHistoryRollGround jhistoryRollGround = JSON.parseObject(data, JHistoryRollGround.class);
            return historyRollGroundService.deleteDataHistoryRollGroundOne(jhistoryRollGround.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JHistoryRollGround jhistoryRollGround = JSON.parseObject(data, JHistoryRollGround.class);
            return historyRollGroundService.deleteDataHistoryRollGroundMany(jhistoryRollGround.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataHistoryRollGround(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return historyRollGroundService.updateDataHistoryRollGround(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataHistoryRollGroundByPage(@RequestBody String data) {
        return historyRollGroundService.findDataHistoryRollGroundByPage(data);
    }
    
    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPageNew"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataHistoryRollGroundByPageNew(@RequestBody String data) {
        return historyRollGroundService.findDataHistoryRollGroundByPageNew(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataHistoryRollGroundByIndocno(@RequestBody String data) {
        return historyRollGroundService.findDataHistoryRollGroundByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<HistoryRollGround> findDataHistoryRollGround() {
        return historyRollGroundService.findDataHistoryRollGround();
    }

}
