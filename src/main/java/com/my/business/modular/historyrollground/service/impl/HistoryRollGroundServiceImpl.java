package com.my.business.modular.historyrollground.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.historyrollground.dao.HistoryRollGroundDao;
import com.my.business.modular.historyrollground.entity.HistoryRollGround;
import com.my.business.modular.historyrollground.entity.JHistoryRollGround;
import com.my.business.modular.historyrollground.service.HistoryRollGroundService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * 轧辊磨削历史接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-26 11:11:07
 */
@Service
public class HistoryRollGroundServiceImpl implements HistoryRollGroundService {

    @Autowired
    private HistoryRollGroundDao historyRollGroundDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataHistoryRollGround(String data, Long userId, String sname) {
        try {
            JHistoryRollGround jhistoryRollGround = JSON.parseObject(data, JHistoryRollGround.class);
            HistoryRollGround historyRollGround = jhistoryRollGround.getHistoryRollGround();
            CodiUtil.newRecord(userId, sname, historyRollGround);
            historyRollGroundDao.insertDataHistoryRollGround(historyRollGround);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataHistoryRollGroundOne(Long indocno) {
        try {
            historyRollGroundDao.deleteDataHistoryRollGroundOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataHistoryRollGroundMany(String str_id) {
        try {
            String sql = "delete history_roll_ground where indocno in(" + str_id + ")";
            historyRollGroundDao.deleteDataHistoryRollGroundMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataHistoryRollGround(String data, Long userId, String sname) {
        try {
            JHistoryRollGround jhistoryRollGround = JSON.parseObject(data, JHistoryRollGround.class);
            HistoryRollGround historyRollGround = jhistoryRollGround.getHistoryRollGround();
            CodiUtil.editRecord(userId, sname, historyRollGround);
            historyRollGroundDao.updateDataHistoryRollGround(historyRollGround);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataHistoryRollGroundByPage(String data) {
        try {
            JHistoryRollGround jhistoryRollGround = JSON.parseObject(data, JHistoryRollGround.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jhistoryRollGround.getPageIndex();
            Integer pageSize = jhistoryRollGround.getPageSize();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jhistoryRollGround.getCondition()) {
                jsonObject = JSON.parseObject(jhistoryRollGround.getCondition().toString());
            }

            String start_date_time = null;
            if (!StringUtils.isEmpty(jsonObject.get("start_date_time"))) {
                start_date_time = jsonObject.get("start_date_time").toString();
            }

            String roll_nb = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_nb"))) {
                roll_nb = jsonObject.get("roll_nb").toString();
            }

            Long grinder_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("grinder_no"))) {
                grinder_no = Long.valueOf(jsonObject.get("grinder_no").toString());
            }

            List<HistoryRollGround> list = historyRollGroundDao.findDataHistoryRollGroundByPage((pageIndex - 1)*pageSize, pageSize, start_date_time,grinder_no,roll_nb);
            Integer count = historyRollGroundDao.findDataHistoryRollGroundByPageSize(start_date_time,grinder_no,roll_nb);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }
    
    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataHistoryRollGroundByPageNew(String data) {
        try {
            JHistoryRollGround jhistoryRollGround = JSON.parseObject(data, JHistoryRollGround.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jhistoryRollGround.getPageIndex();
            Integer pageSize = jhistoryRollGround.getPageSize();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jhistoryRollGround.getCondition()) {
                jsonObject = JSON.parseObject(jhistoryRollGround.getCondition().toString());
            }

            String start_date_time = null;
            if (!StringUtils.isEmpty(jsonObject.get("start_date_time"))) {
                start_date_time = jsonObject.get("start_date_time").toString();
            }

            String roll_nb = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_nb"))) {
            	roll_nb = jsonObject.get("roll_nb").toString();
            }

            List<HistoryRollGround> list = historyRollGroundDao.findDataHistoryRollGroundByPageNew((pageIndex - 1)*pageSize, pageSize, start_date_time,roll_nb);
            Integer count = historyRollGroundDao.findDataHistoryRollGroundByPageSizeNew(start_date_time,roll_nb);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataHistoryRollGroundByIndocno(String data) {
        try {
            JHistoryRollGround jhistoryRollGround = JSON.parseObject(data, JHistoryRollGround.class);
            Long indocno = jhistoryRollGround.getIndocno();

            HistoryRollGround historyRollGround = historyRollGroundDao.findDataHistoryRollGroundByIndocno(indocno);
            return ResultData.ResultDataSuccess(historyRollGround);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<HistoryRollGround> findDataHistoryRollGround() {
        List<HistoryRollGround> list = historyRollGroundDao.findDataHistoryRollGround();
        return list;
    }
}
