package com.my.business.modular.historyrollground.dao;

import com.my.business.modular.historyrollground.entity.HistoryRollGround;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 轧辊磨削历史dao接口
 *
 * @author 生成器生成
 * @date 2020-08-26 11:11:07
 */
@Mapper
public interface HistoryRollGroundDao {

    /**
     * 添加记录
     *
     * @param historyRollGround 对象实体
     */
    void insertDataHistoryRollGround(HistoryRollGround historyRollGround);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataHistoryRollGroundOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataHistoryRollGroundMany(String value);

    /**
     * 修改记录
     *
     * @param historyRollGround 对象实体
     */
    void updateDataHistoryRollGround(HistoryRollGround historyRollGround);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<HistoryRollGround> findDataHistoryRollGroundByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("start_date_time") String start_date_time,@Param("grinder_no") Long grinder_no,@Param("roll_nb") String roll_nb);

    
    List<HistoryRollGround> findDataHistoryRollGroundByPageNew(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("start_date_time") String start_date_time,@Param("roll_nb") String roll_nb);

    Integer findDataHistoryRollGroundByPageSizeNew(@Param("start_date_time") String start_date_time,@Param("roll_nb") String roll_nb);
    
    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataHistoryRollGroundByPageSize(@Param("start_date_time") String start_date_time,@Param("grinder_no") Long grinder_no,@Param("roll_nb") String roll_nb);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    HistoryRollGround findDataHistoryRollGroundByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<HistoryRollGround> findDataHistoryRollGround();

    /**
     * 查找重复数量
     *
     * @return
     */
    Integer findDataHave(@Param("value") Date value,@Param("grinder_no") Long grinder_no);

    /**
     * 查找最新的日期时间
     *
     * @return datetime
     */
    Date findTime(@Param("grinder_no") Long grinder_no);

    /**
     * 清除PRESET
     */
    void clear();
}
