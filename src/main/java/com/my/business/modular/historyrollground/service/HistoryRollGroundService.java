package com.my.business.modular.historyrollground.service;

import com.my.business.modular.historyrollground.entity.HistoryRollGround;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 轧辊磨削历史接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-26 11:11:07
 */
public interface HistoryRollGroundService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataHistoryRollGround(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataHistoryRollGroundOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataHistoryRollGroundMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataHistoryRollGround(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataHistoryRollGroundByPage(String data);
    
    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataHistoryRollGroundByPageNew(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataHistoryRollGroundByIndocno(String data);

    /**
     * 查看记录
     */
    List<HistoryRollGround> findDataHistoryRollGround();
}
