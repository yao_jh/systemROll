package com.my.business.modular.rolldatafiles.dao;

import com.my.business.modular.rolldatafiles.entity.RollDataFiles;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 轧辊数据文件dao接口
 *
 * @author 生成器生成
 * @date 2020-08-26 11:11:39
 */
@Mapper
public interface RollDataFilesDao {

    /**
     * 添加记录
     *
     * @param rollDataFiles 对象实体
     */
    void insertDataRollDataFiles(RollDataFiles rollDataFiles);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollDataFilesOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollDataFilesMany(String value);

    /**
     * 修改记录
     *
     * @param rollDataFiles 对象实体
     */
    void updateDataRollDataFiles(RollDataFiles rollDataFiles);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollDataFiles> findDataRollDataFilesByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollDataFilesByPageSize();

    /***
     * 根据主键查询信息
     * @param roll_no 磨床号
     * @param initial_date_time 开始时间
     * @return
     */
    RollDataFiles findDataRollDataFilesEcharts(@Param("roll_no") String roll_no, @Param("initial_date_time") String initial_date_time);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollDataFiles> findDataRollDataFiles();

    /**
     * 查找重复数量
     *
     * @return
     */
    Integer findDataHave(@Param("value") Date value,@Param("grinder") Long grinder);

    /**
     * 查找最新的日期时间
     *
     * @return datetime
     */
    Date findTime(@Param("grinder") Long grinder);

    /**
     * 清除PRESET
     */
    void clear();

    void deleteDuplicateDataRollDataFilesMessage();
}
