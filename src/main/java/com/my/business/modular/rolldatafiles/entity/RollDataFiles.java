package com.my.business.modular.rolldatafiles.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.util.Date;

/**
 * 轧辊数据文件实体类
 *
 * @author 生成器生成
 * @date 2020-08-26 11:11:38
 */
public class RollDataFiles extends BaseEntity {

    private Long indocno;  //主键
    private Long grinder;  //磨床号
    private String rolling_mill;  //机架
    private String roll_type;  //轧辊类型
    private String roll_material;  //材质
    private String roll_nb;  //辊号
    private String initial_report;  //初始报告
    private Date initial_date_time;  //开始时间

    private String initial_diameter;  //磨前辊径
    private Date initial_diam_date_time;  //

    private String first_theopro;  //磨前标准曲线
    private Date first_theopro_date_time;  //

    private String first_realpro;  //磨前辊形曲线
    private Date first_realpro_date_time;  //

    private String next_diameter;  //
    private Date next_diam_date_time;  //

    private String crack;  //
    private Date crack_date_time;  //

    private String bruise;  //
    private Date bruise_date_time;  //

    private String final_theopro;  //磨后标准曲线
    private Date final_theopro_date_time;  //

    private String final_realpro;  //磨后辊形曲线
    private Date final_realpro_date_time;  //

    private String roundness;  //圆度曲线
    private Date roundness_date_time;  //

    private String endcrack;  //
    private Date endcrack_date_time;  //

    private String endbruise;  //
    private Date endbruise_date_time;  //

    private String inspekt;  //探伤图
    private Date inspekt_dt;  //

    private String endinspekt;  //结束探伤图
    private Date endinspekt_dt;  //

    private String structure;  //
    private Date structure_date_time;  //

    private String ultrasound;  //超声波探伤图
    private Date ultrasound_dt;  //

    private String ultrasoundend;  //
    private Date ultrasoundend_dt;  //

    private String roughness;  //粗糙度
    private Date roughness_dt;  //

    private String final_report;  //最终报告
    private Date final_report_date_time;  //

    private String validate_discard;  //
    private String history_grind;  //
    private String history_pprograms;  //
    private String hardness;  //硬度
    private Date hardness_dt;  //

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    private String pos;//位置 点

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getGrinder() {
        return this.grinder;
    }

    public void setGrinder(Long grinder) {
        this.grinder = grinder;
    }

    public String getRolling_mill() {
        return this.rolling_mill;
    }

    public void setRolling_mill(String rolling_mill) {
        this.rolling_mill = rolling_mill;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public String getRoll_material() {
        return this.roll_material;
    }

    public void setRoll_material(String roll_material) {
        this.roll_material = roll_material;
    }

    public String getRoll_nb() {
        return this.roll_nb;
    }

    public void setRoll_nb(String roll_nb) {
        this.roll_nb = roll_nb;
    }

    public String getInitial_report() {
        return this.initial_report;
    }

    public void setInitial_report(String initial_report) {
        this.initial_report = initial_report;
    }

    public Date getInitial_date_time() {
        return this.initial_date_time;
    }

    public void setInitial_date_time(Date initial_date_time) {
        this.initial_date_time = initial_date_time;
    }

    public String getInitial_diameter() {
        return this.initial_diameter;
    }

    public void setInitial_diameter(String initial_diameter) {
        this.initial_diameter = initial_diameter;
    }

    public Date getInitial_diam_date_time() {
        return this.initial_diam_date_time;
    }

    public void setInitial_diam_date_time(Date initial_diam_date_time) {
        this.initial_diam_date_time = initial_diam_date_time;
    }

    public String getFirst_theopro() {
        return this.first_theopro;
    }

    public void setFirst_theopro(String first_theopro) {
        this.first_theopro = first_theopro;
    }

    public Date getFirst_theopro_date_time() {
        return this.first_theopro_date_time;
    }

    public void setFirst_theopro_date_time(Date first_theopro_date_time) {
        this.first_theopro_date_time = first_theopro_date_time;
    }

    public String getFirst_realpro() {
        return this.first_realpro;
    }

    public void setFirst_realpro(String first_realpro) {
        this.first_realpro = first_realpro;
    }

    public Date getFirst_realpro_date_time() {
        return this.first_realpro_date_time;
    }

    public void setFirst_realpro_date_time(Date first_realpro_date_time) {
        this.first_realpro_date_time = first_realpro_date_time;
    }

    public String getNext_diameter() {
        return this.next_diameter;
    }

    public void setNext_diameter(String next_diameter) {
        this.next_diameter = next_diameter;
    }

    public Date getNext_diam_date_time() {
        return this.next_diam_date_time;
    }

    public void setNext_diam_date_time(Date next_diam_date_time) {
        this.next_diam_date_time = next_diam_date_time;
    }

    public String getCrack() {
        return this.crack;
    }

    public void setCrack(String crack) {
        this.crack = crack;
    }

    public Date getCrack_date_time() {
        return this.crack_date_time;
    }

    public void setCrack_date_time(Date crack_date_time) {
        this.crack_date_time = crack_date_time;
    }

    public String getBruise() {
        return this.bruise;
    }

    public void setBruise(String bruise) {
        this.bruise = bruise;
    }

    public Date getBruise_date_time() {
        return this.bruise_date_time;
    }

    public void setBruise_date_time(Date bruise_date_time) {
        this.bruise_date_time = bruise_date_time;
    }

    public String getFinal_theopro() {
        return this.final_theopro;
    }

    public void setFinal_theopro(String final_theopro) {
        this.final_theopro = final_theopro;
    }

    public Date getFinal_theopro_date_time() {
        return this.final_theopro_date_time;
    }

    public void setFinal_theopro_date_time(Date final_theopro_date_time) {
        this.final_theopro_date_time = final_theopro_date_time;
    }

    public String getFinal_realpro() {
        return this.final_realpro;
    }

    public void setFinal_realpro(String final_realpro) {
        this.final_realpro = final_realpro;
    }

    public Date getFinal_realpro_date_time() {
        return this.final_realpro_date_time;
    }

    public void setFinal_realpro_date_time(Date final_realpro_date_time) {
        this.final_realpro_date_time = final_realpro_date_time;
    }

    public String getRoundness() {
        return this.roundness;
    }

    public void setRoundness(String roundness) {
        this.roundness = roundness;
    }

    public Date getRoundness_date_time() {
        return this.roundness_date_time;
    }

    public void setRoundness_date_time(Date roundness_date_time) {
        this.roundness_date_time = roundness_date_time;
    }

    public String getEndcrack() {
        return this.endcrack;
    }

    public void setEndcrack(String endcrack) {
        this.endcrack = endcrack;
    }

    public Date getEndcrack_date_time() {
        return this.endcrack_date_time;
    }

    public void setEndcrack_date_time(Date endcrack_date_time) {
        this.endcrack_date_time = endcrack_date_time;
    }

    public String getEndbruise() {
        return this.endbruise;
    }

    public void setEndbruise(String endbruise) {
        this.endbruise = endbruise;
    }

    public Date getEndbruise_date_time() {
        return this.endbruise_date_time;
    }

    public void setEndbruise_date_time(Date endbruise_date_time) {
        this.endbruise_date_time = endbruise_date_time;
    }

    public String getInspekt() {
        return this.inspekt;
    }

    public void setInspekt(String inspekt) {
        this.inspekt = inspekt;
    }

    public Date getInspekt_dt() {
        return this.inspekt_dt;
    }

    public void setInspekt_dt(Date inspekt_dt) {
        this.inspekt_dt = inspekt_dt;
    }

    public String getEndinspekt() {
        return this.endinspekt;
    }

    public void setEndinspekt(String endinspekt) {
        this.endinspekt = endinspekt;
    }

    public Date getEndinspekt_dt() {
        return this.endinspekt_dt;
    }

    public void setEndinspekt_dt(Date endinspekt_dt) {
        this.endinspekt_dt = endinspekt_dt;
    }

    public String getStructure() {
        return this.structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public Date getStructure_date_time() {
        return this.structure_date_time;
    }

    public void setStructure_date_time(Date structure_date_time) {
        this.structure_date_time = structure_date_time;
    }

    public String getUltrasound() {
        return this.ultrasound;
    }

    public void setUltrasound(String ultrasound) {
        this.ultrasound = ultrasound;
    }

    public Date getUltrasound_dt() {
        return this.ultrasound_dt;
    }

    public void setUltrasound_dt(Date ultrasound_dt) {
        this.ultrasound_dt = ultrasound_dt;
    }

    public String getUltrasoundend() {
        return this.ultrasoundend;
    }

    public void setUltrasoundend(String ultrasoundend) {
        this.ultrasoundend = ultrasoundend;
    }

    public Date getUltrasoundend_dt() {
        return this.ultrasoundend_dt;
    }

    public void setUltrasoundend_dt(Date ultrasoundend_dt) {
        this.ultrasoundend_dt = ultrasoundend_dt;
    }

    public String getRoughness() {
        return this.roughness;
    }

    public void setRoughness(String roughness) {
        this.roughness = roughness;
    }

    public Date getRoughness_dt() {
        return this.roughness_dt;
    }

    public void setRoughness_dt(Date roughness_dt) {
        this.roughness_dt = roughness_dt;
    }

    public String getFinal_report() {
        return this.final_report;
    }

    public void setFinal_report(String final_report) {
        this.final_report = final_report;
    }

    public Date getFinal_report_date_time() {
        return this.final_report_date_time;
    }

    public void setFinal_report_date_time(Date final_report_date_time) {
        this.final_report_date_time = final_report_date_time;
    }

    public String getValidate_discard() {
        return this.validate_discard;
    }

    public void setValidate_discard(String validate_discard) {
        this.validate_discard = validate_discard;
    }

    public String getHistory_grind() {
        return this.history_grind;
    }

    public void setHistory_grind(String history_grind) {
        this.history_grind = history_grind;
    }

    public String getHistory_pprograms() {
        return this.history_pprograms;
    }

    public void setHistory_pprograms(String history_pprograms) {
        this.history_pprograms = history_pprograms;
    }

    public String getHardness() {
        return this.hardness;
    }

    public void setHardness(String hardness) {
        this.hardness = hardness;
    }

    public Date getHardness_dt() {
        return this.hardness_dt;
    }

    public void setHardness_dt(Date hardness_dt) {
        this.hardness_dt = hardness_dt;
    }


}