package com.my.business.modular.rolldatafiles.service;

import com.my.business.modular.rolldatafiles.entity.RollDataFiles;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;
import java.util.Map;

/**
 * 轧辊数据文件接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-26 11:11:39
 */
public interface RollDataFilesService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollDataFiles(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollDataFilesOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollDataFilesMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollDataFiles(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollDataFilesByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollDataFilesEcharts(String data);

    /**
     * 查看记录
     */
    List<RollDataFiles> findDataRollDataFiles();

    /**
     * 多项式拟合曲线差值计算
     * @param data json字符串
     * @return json字符串
     */
    ResultData findDateForMath3(String data);

    /**
     * 计算曲线
     * @param roll_no 磨床号
     * @param grind_starttime 磨削开始时间
     * @return
     */
    Map<String, String> AnalysisDifference(String  roll_no, String grind_starttime);
}
