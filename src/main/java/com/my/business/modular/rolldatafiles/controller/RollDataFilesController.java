package com.my.business.modular.rolldatafiles.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rolldatafiles.entity.JRollDataFiles;
import com.my.business.modular.rolldatafiles.entity.RollDataFiles;
import com.my.business.modular.rolldatafiles.service.RollDataFilesService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 轧辊数据文件控制器层
 *
 * @author 生成器生成
 * @date 2020-08-26 11:11:39
 */
@RestController
@RequestMapping("/rollDataFiles")
public class RollDataFilesController {

    @Autowired
    private RollDataFilesService rollDataFilesService;

    /**
     * 添加 记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollDataFiles(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollDataFilesService.insertDataRollDataFiles(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollDataFilesOne(@RequestBody String data) {
        try {
            JRollDataFiles jrollDataFiles = JSON.parseObject(data, JRollDataFiles.class);
            return rollDataFilesService.deleteDataRollDataFilesOne(jrollDataFiles.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollDataFiles jrollDataFiles = JSON.parseObject(data, JRollDataFiles.class);
            return rollDataFilesService.deleteDataRollDataFilesMany(jrollDataFiles.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollDataFiles(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollDataFilesService.updateDataRollDataFiles(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDataFilesByPage(@RequestBody String data) {
        return rollDataFilesService.findDataRollDataFilesByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findecharts"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDataFilesEcharts(@RequestBody String data) {
        return rollDataFilesService.findDataRollDataFilesEcharts(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollDataFiles> findDataRollDataFiles() {
        return rollDataFilesService.findDataRollDataFiles();
    }

    /**
     * 多项式拟合曲线差值计算
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findmath3"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findmath3(@RequestBody String data) {
        return rollDataFilesService.findDateForMath3(data);
    }
}
