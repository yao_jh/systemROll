package com.my.business.modular.rolldatafiles.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rolldatafiles.dao.RollDataFilesDao;
import com.my.business.modular.rolldatafiles.entity.JRollDataFiles;
import com.my.business.modular.rolldatafiles.entity.RollDataFiles;
import com.my.business.modular.rolldatafiles.service.RollDataFilesService;
import com.my.business.modular.rollgrinding.dao.RollGrindingBFDao;
import com.my.business.modular.rollgrinding.dao.RollGrindingDao;
import com.my.business.modular.rollgrinding.entity.RollGrinding;
import com.my.business.modular.rollgrinding.entity.RollGrindingBF;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.util.MathUtil;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.*;

import static java.lang.Double.parseDouble;

/**
 * 轧辊数据文件接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-26 11:11:39
 */
@CommonsLog
@Service
public class RollDataFilesServiceImpl implements RollDataFilesService {

    private final static Log logger = LogFactory.getLog(RollDataFilesServiceImpl.class);
    @Autowired
    private RollDataFilesDao rollDataFilesDao;
    @Autowired
    private RollGrindingBFDao rollGrindingBFDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollDataFiles(String data, Long userId, String sname) {
        try {
            JRollDataFiles jrollDataFiles = JSON.parseObject(data, JRollDataFiles.class);
            RollDataFiles rollDataFiles = jrollDataFiles.getRollDataFiles();
            CodiUtil.newRecord(userId, sname, rollDataFiles);
            rollDataFilesDao.insertDataRollDataFiles(rollDataFiles);


            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollDataFilesOne(Long indocno) {
        try {
            rollDataFilesDao.deleteDataRollDataFilesOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollDataFilesMany(String str_id) {
        try {
            String sql = "delete from roll_data_files where indocno in(" + str_id + ")";
            rollDataFilesDao.deleteDataRollDataFilesMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollDataFiles(String data, Long userId, String sname) {
        try {
            JRollDataFiles jrollDataFiles = JSON.parseObject(data, JRollDataFiles.class);
            RollDataFiles rollDataFiles = jrollDataFiles.getRollDataFiles();
            CodiUtil.editRecord(userId, sname, rollDataFiles);
            rollDataFilesDao.updateDataRollDataFiles(rollDataFiles);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollDataFilesByPage(String data) {
        try {
            JRollDataFiles jrollDataFiles = JSON.parseObject(data, JRollDataFiles.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollDataFiles.getPageIndex();
            Integer pageSize = jrollDataFiles.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollDataFiles.getCondition()) {
                jsonObject = JSON.parseObject(jrollDataFiles.getCondition().toString());
            }

            List<RollDataFiles> list = rollDataFilesDao.findDataRollDataFilesByPage((pageIndex - 1) * pageSize, pageSize);
            Integer count = rollDataFilesDao.findDataRollDataFilesByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 图形化
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollDataFilesEcharts(String data) {
        try {
            JRollDataFiles jrollDataFiles = JSON.parseObject(data, JRollDataFiles.class);
            JSONObject jsonObject = null;
            if (null != jrollDataFiles.getCondition()) {
                jsonObject = JSON.parseObject(jrollDataFiles.getCondition().toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no =jsonObject.get("roll_no").toString();
            }

            String initial_date_time = null;
            if (!StringUtils.isEmpty(jsonObject.get("initial_date_time"))) {
                initial_date_time = jsonObject.get("initial_date_time").toString();
            }

            RollDataFiles rollDataFiles = rollDataFilesDao.findDataRollDataFilesEcharts(roll_no, initial_date_time);

            String first_theopro = rollDataFiles.getFirst_theopro();
            String first_realpro = rollDataFiles.getFirst_realpro();
            String final_theopro = rollDataFiles.getFinal_theopro();
            String final_realpro = rollDataFiles.getFinal_realpro();
            String roundness = rollDataFiles.getRoundness();
            String pos = rollDataFiles.getPos();

            LinkedHashMap first_theopro_map = new LinkedHashMap();
            LinkedHashMap first_realpro_map = new LinkedHashMap();
            LinkedHashMap final_theopro_map = new LinkedHashMap();
            LinkedHashMap final_realpro_map = new LinkedHashMap();
            LinkedHashMap roundness_map = new LinkedHashMap();
            LinkedHashMap pos_map = new LinkedHashMap();
            if (!StringUtils.isEmpty(first_theopro) && !first_theopro.contains("null")) {
                //解析图形化——磨前标准曲线图
                //String Start_first_theopro = first_theopro.substring(first_theopro.indexOf("Start*") + 10, first_theopro.indexOf("Profile:") - 2).replace("\r\n", ",");
                //String Profile_first_theopro = first_theopro.substring(first_theopro.indexOf("Profile:") + 10, first_theopro.indexOf("Map:") - 2).replace("\r\n", ",");
                //String Map_first_theopro = first_theopro.split("Map:")[1].substring(2, first_theopro.split("Map:")[1].indexOf("Profile:") - 2).replace("\r\n", ",");
                String Map_first_theopro = first_theopro.replace("\n", ",");
                //String Profile2_first_theopro = first_theopro.split("Map:")[1].substring(first_theopro.split("Map:")[1].indexOf("Profile:") + 10, first_theopro.split("Map:")[1].indexOf("End.") - 2).replace("\r\n", ",");

                first_theopro_map.put("title", "磨前标准曲线图");
               // first_theopro_map.put("Msg", first_theopro.substring(first_theopro.indexOf("Msg:") + 4, first_theopro.indexOf("Ver:")).replace("\r\n", ""));
                //first_theopro_map.put("Ver", first_theopro.substring(first_theopro.indexOf("Ver:") + 4, first_theopro.indexOf("Time:")).replace("\r\n", ""));
                //first_theopro_map.put("Time", first_theopro.substring(first_theopro.indexOf("Time:") + 5, first_theopro.indexOf("Unit:")).replace("\r\n", ""));
                //first_theopro_map.put("Unit", first_theopro.substring(first_theopro.indexOf("Unit:") + 5, first_theopro.indexOf("Index:")).replace("\r\n", ""));
                //first_theopro_map.put("Index", first_theopro.substring(first_theopro.indexOf("Index:") + 6, first_theopro.indexOf("Start*")).replace("\r\n", ""));
                //first_theopro_map.put("Start", Start_first_theopro);
                //first_theopro_map.put("Profile", Profile_first_theopro);
                first_theopro_map.put("Map", Map_first_theopro);
                //first_theopro_map.put("Profile2", Profile2_first_theopro);
            } else {
                first_theopro_map.put("first_theopro", null);
            }


            if (!StringUtils.isEmpty(first_realpro) && !first_realpro.contains("null")) {
                //解析图形化——磨前辊形曲线图
                //String Start_first_realpro = first_realpro.substring(first_realpro.indexOf("Start*") + 8, first_realpro.indexOf("Profile:") - 2).replace("\r\n", ",");
                //String Profile_first_realpro = first_realpro.substring(first_realpro.indexOf("Profile:") + 10, first_realpro.indexOf("Map:") - 2).replace("\r\n", ",");
                //String Map_first_realpro = first_realpro.split("Map:")[1].substring(2, first_realpro.split("Map:")[1].indexOf("Profile:") - 2).replace("\r\n", ",");
                String Map_first_realpro = first_realpro.replace("\n", ",");
                first_realpro_map.put("title", "磨前辊形曲线图");
                //first_realpro_map.put("Msg", first_realpro.substring(first_realpro.indexOf("Msg:") + 4, first_realpro.indexOf("Ver:")).replace("\r\n", ""));
                //first_realpro_map.put("Ver:", first_realpro.substring(first_realpro.indexOf("Ver:") + 4, first_realpro.indexOf("Time:")).replace("\r\n", ""));
                //first_realpro_map.put("Time", first_realpro.substring(first_realpro.indexOf("Time:") + 5, first_realpro.indexOf("Unit:")).replace("\r\n", ""));
                //first_realpro_map.put("Unit", first_realpro.substring(first_realpro.indexOf("Unit:") + 5, first_realpro.indexOf("Index:")).replace("\r\n", ""));
                //first_realpro_map.put("Index", first_realpro.substring(first_realpro.indexOf("Index:") + 6, first_realpro.indexOf("Start*")).replace("\r\n", ""));
                //first_realpro_map.put("Start", Start_first_realpro);
                //first_realpro_map.put("Profile", Profile_first_realpro);
                first_realpro_map.put("Map", Map_first_realpro);
            } else {
                first_realpro_map.put("first_realpro", null);
            }

            if (!StringUtils.isEmpty(final_theopro) && !final_theopro.contains("null")) {
                //解析图形化——磨后标准曲线图
                //String Start_final_theopro = final_theopro.substring(final_theopro.indexOf("Start*") + 8, final_theopro.indexOf("Profile:") - 2).replace("\r\n", ",");
                //String Profile_final_theopro = final_theopro.substring(final_theopro.indexOf("Profile:") + 10, final_theopro.indexOf("Map:") - 2).replace("\r\n", ",");
                //String Map_final_theopro = final_theopro.split("Map:")[1].substring(2, final_theopro.split("Map:")[1].indexOf("Profile:") - 2).replace("\r\n", ",");
                String Map_final_theopro = final_theopro.replace("\n", ",");
                //String Profile2_final_theopro = final_theopro.split("Map:")[1].substring(final_theopro.split("Map:")[1].indexOf("Profile:") + 10, final_theopro.split("Map:")[1].indexOf("End.") - 2).replace("\r\n", ",");

                final_theopro_map.put("title", "磨后标准曲线图");
                //final_theopro_map.put("Msg", final_theopro.substring(final_theopro.indexOf("Msg:") + 4, final_theopro.indexOf("Ver:")).replace("\r\n", ""));
                //final_theopro_map.put("Ver", final_theopro.substring(final_theopro.indexOf("Ver:") + 4, final_theopro.indexOf("Time:")).replace("\r\n", ""));
                //final_theopro_map.put("Time", final_theopro.substring(final_theopro.indexOf("Time:") + 5, final_theopro.indexOf("Unit:")).replace("\r\n", ""));
                //final_theopro_map.put("Unit", final_theopro.substring(final_theopro.indexOf("Unit:") + 5, final_theopro.indexOf("Index:")).replace("\r\n", ""));
                //final_theopro_map.put("Index", final_theopro.substring(final_theopro.indexOf("Index:") + 6, final_theopro.indexOf("Start*")).replace("\r\n", ""));
                //final_theopro_map.put("Start", Start_final_theopro);
                //final_theopro_map.put("Profile", Profile_final_theopro);
                final_theopro_map.put("Map", Map_final_theopro);
                //final_theopro_map.put("Profile2", Profile2_final_theopro);
            } else {
                final_theopro_map.put("final_theopro", null);
            }

            if (!StringUtils.isEmpty(final_realpro) && !final_realpro.contains("null")) {
                //解析图形化——磨后辊形曲线图
                //String Start_final_realpro = final_realpro.substring(final_realpro.indexOf("Start*") + 8, final_realpro.indexOf("Profile:") - 2).replace("\r\n", ",");
                //String Profile_final_realpro = final_realpro.substring(final_realpro.indexOf("Profile:") + 10, final_realpro.indexOf("Map:") - 2).replace("\r\n", ",");
                //String Map_final_realpro = final_realpro.split("Map:")[1].substring(2, final_realpro.split("Map:")[1].indexOf("Profile:") - 2).replace("\r\n", ",");
                String Map_final_realpro = final_realpro.replace("\n", ",");

                final_realpro_map.put("title", "磨后辊形曲线图");
                //final_realpro_map.put("Msg", final_realpro.substring(final_realpro.indexOf("Msg:") + 4, final_realpro.indexOf("Ver:")).replace("\r\n", ""));
                //final_realpro_map.put("Ver:", final_realpro.substring(final_realpro.indexOf("Ver:") + 4, final_realpro.indexOf("Time:")).replace("\r\n", ""));
                //final_realpro_map.put("Time", final_realpro.substring(final_realpro.indexOf("Time:") + 5, final_realpro.indexOf("Unit:")).replace("\r\n", ""));
                //final_realpro_map.put("Unit", final_realpro.substring(final_realpro.indexOf("Unit:") + 5, final_realpro.indexOf("Index:")).replace("\r\n", ""));
                //final_realpro_map.put("Index", final_realpro.substring(final_realpro.indexOf("Index:") + 6, final_realpro.indexOf("Start*")).replace("\r\n", ""));
                //final_realpro_map.put("Start", Start_final_realpro);
                //final_realpro_map.put("Profile", Profile_final_realpro);
                final_realpro_map.put("Map", Map_final_realpro);
            } else {
                final_realpro_map.put("final_theopro", null);
            }

            if (!StringUtils.isEmpty(roundness) && !roundness.contains("null")) {
                //解析图形化——圆度
                roundness_map.put("title", "圆度曲线图");
                String   Map_roundness  = roundness.replace("\n", ",");
                roundness_map.put("roundness", Map_roundness);
                //roundness_map.put("Msg", roundness.substring(roundness.indexOf("Msg:") + 4, roundness.indexOf("Ver:")).replace("\r\n", ""));
                //roundness_map.put("Ver", roundness.substring(roundness.indexOf("Ver:") + 4, roundness.indexOf("Time:")).replace("\r\n", ""));
                //roundness_map.put("Time", roundness.substring(roundness.indexOf("Time:") + 5, roundness.indexOf("Unit:")).replace("\r\n", ""));
                //roundness_map.put("Unit", roundness.substring(roundness.indexOf("Unit:") + 5, roundness.indexOf("Index:")).replace("\r\n", ""));
                //roundness_map.put("Index", roundness.substring(roundness.indexOf("Index:") + 6, roundness.indexOf("Start*")).replace("\r\n", ""));
                //roundness_map.put("Start", roundness.substring(roundness.indexOf("Start*") + 8, roundness.indexOf("Position:") - 2).replace("\r\n", ","));

                //String roundness0 = roundness.substring(roundness.indexOf("Position:\r\n0"), roundness.indexOf("Position:\r\n1") - 2);
                //roundness_map.put("Position0", roundness0.substring(roundness0.indexOf("Position:") + 11, roundness0.indexOf("1UpperArm:") - 2).replace("\r\n", ","));
                //roundness_map.put("UpperArm0", roundness0.substring(roundness0.indexOf("1UpperArm:") + 12, roundness0.indexOf("LowerArm:") - 2).replace("\r\n", ","));
                //roundness_map.put("LowerArm0", roundness0.substring(roundness0.indexOf("LowerArm:") + 11, roundness0.indexOf("BothArms:") - 2).replace("\r\n", ","));
                //roundness_map.put("BothArms0", roundness0.substring(roundness0.indexOf("BothArms:") + 11).replace("\r\n", ","));
                //String roundness1 = roundness.substring(roundness.indexOf("Position:\r\n1"), roundness.indexOf("Position:\r\n2") - 2);
                //roundness_map.put("Position1", roundness1.substring(roundness1.indexOf("Position:") + 11, roundness1.indexOf("1UpperArm:") - 2).replace("\r\n", ","));
                //roundness_map.put("UpperArm1", roundness1.substring(roundness1.indexOf("1UpperArm:") + 12, roundness1.indexOf("LowerArm:") - 2).replace("\r\n", ","));
                //roundness_map.put("LowerArm1", roundness1.substring(roundness1.indexOf("LowerArm:") + 11, roundness1.indexOf("BothArms:") - 2).replace("\r\n", ","));
                //roundness_map.put("BothArms1", roundness1.substring(roundness1.indexOf("BothArms:") + 11).replace("\r\n", ","));
                //String roundness2 = roundness.substring(roundness.indexOf("Position:\r\n2"), roundness.indexOf("End.") - 2);
                //roundness_map.put("Position2", roundness2.substring(roundness2.indexOf("Position:") + 11, roundness2.indexOf("1UpperArm:") - 2).replace("\r\n", ","));
                //roundness_map.put("UpperArm2", roundness2.substring(roundness2.indexOf("1UpperArm:") + 12, roundness2.indexOf("LowerArm:") - 2).replace("\r\n", ","));
                //roundness_map.put("LowerArm2", roundness2.substring(roundness2.indexOf("LowerArm:") + 11, roundness2.indexOf("BothArms:") - 2).replace("\r\n", ","));
                //roundness_map.put("BothArms2", roundness2.substring(roundness1.indexOf("BothArms:") + 22).replace("\r\n", ","));
            } else {
                roundness_map.put("roundness", null);
            }

            if (!StringUtils.isEmpty(pos) && !pos.contains("null")) {
                pos_map.put("title", "位置");
                String Map_pos = pos.replace("\n", ",");
                pos_map.put("pos", Map_pos);
            }else{
                pos_map.put("pos", null);
            }


            //转换成json
            JSONObject json_first_theopro = new JSONObject(first_theopro_map);
            JSONObject json_first_realpro = new JSONObject(first_realpro_map);
            JSONObject json_final_theopro = new JSONObject(final_theopro_map);
            JSONObject json_final_realpro = new JSONObject(final_realpro_map);
            JSONObject json_roundness = new JSONObject(roundness_map);
            JSONObject json_pos = new JSONObject(pos_map);

            List<JSONObject> list = new ArrayList<>();

            list.add(0, json_final_theopro);
            list.add(1, json_final_realpro);
            list.add(2, json_roundness);
            list.add(3, json_first_theopro);
            list.add(4, json_first_realpro);
            list.add(5, json_pos);

            return ResultData.ResultDataSuccess(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollDataFiles> findDataRollDataFiles() {
        List<RollDataFiles> list = rollDataFilesDao.findDataRollDataFiles();
        return list;
    }

    /**
     * 多项式拟合曲线差值计算
     *
     * @param data json字符串
     * @return json字符串
     */
    @Override
    public ResultData findDateForMath3(String data) {
        try {
            JRollDataFiles jrollDataFiles = JSON.parseObject(data, JRollDataFiles.class);
            JSONObject jsonObject = null;
            if (null != jrollDataFiles.getCondition()) {
                jsonObject = JSON.parseObject(jrollDataFiles.getCondition().toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

            String initial_date_time = null;
            if (!StringUtils.isEmpty(jsonObject.get("initial_date_time"))) {
                initial_date_time = jsonObject.get("initial_date_time").toString();
            }

            Map<String, String> map = AnalysisDifference(roll_no, initial_date_time);
            Map<String, String> mapS = new HashMap<>();
            mapS.put("x", map.get("实测曲线X轴坐标"));
            mapS.put("y", map.get("差值曲线y轴坐标"));
            return ResultData.ResultDataSuccess(mapS);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }


    public Map<String, String> AnalysisDifference(String roll_no, String initial_date_time) {
        RollDataFiles rollDataFiles = rollDataFilesDao.findDataRollDataFilesEcharts(roll_no, initial_date_time);
        RollGrindingBF rollGrindingBF = rollGrindingBFDao.findDataRollGrindingBFforOne(roll_no,initial_date_time);
        Map<String, String> map = new HashMap<String, String>();
        if (!StringUtils.isEmpty(rollDataFiles)) {
            if (!StringUtils.isEmpty(rollDataFiles.getFinal_theopro()) && !StringUtils.isEmpty(rollDataFiles.getFinal_realpro())) {
                String final_theopro = rollDataFiles.getFinal_theopro();
                String final_realpro = rollDataFiles.getFinal_realpro();

                String Start_final_theopro = null;
                String Profile_final_theopro = null;
                String Map_final_theopro = null;
                String Profile_final_realpro = null;
                String Map_final_realpro = null;
                PolynomialCurveFitter Fitter;


                if (!StringUtils.isEmpty(final_theopro) && !final_theopro.contains("null") && !StringUtils.isEmpty(final_realpro) && !final_realpro.contains("null")) {
                    //解析图形化——磨后标准曲线图
                    Start_final_theopro = final_theopro.substring(final_theopro.indexOf("Start*") + 8, final_theopro.indexOf("Profile:") - 2).replace("\r\n", ",");
                    Profile_final_theopro = final_theopro.substring(final_theopro.indexOf("Profile:") + 10, final_theopro.indexOf("Map:") - 2).replace("\r\n", ",");
                    Map_final_theopro = final_theopro.split("Map:")[1].substring(2, final_theopro.split("Map:")[1].indexOf("Profile:") - 2).replace("\r\n", ",");

                    String[] standard_s = Start_final_theopro.split(",");
                    String[] x_s = Profile_final_theopro.split(",");
                    String[] y_s = Map_final_theopro.split(",");

                    int num = Integer.parseInt(x_s[0]); // num 区间数
                    double x_min = parseDouble(x_s[1]); // x轴最小值
                    double x_max = parseDouble(x_s[2]); // x轴最大值


                    double[] x = new double[num + 1]; // 定义标准曲线X轴坐标集合
                    double[] y = new double[num + 1]; // 定义标准曲线y轴坐标集合

                    double x_d = (x_max - x_min) / num; //相邻x轴坐标之间的差值
                    //循环计算每个x轴的坐标并加入集合
                    for (int i = 0; i < num + 1; i++) {
                        x[i] = MathUtil.tofixed(x_min + x_d * i, 3);
                    }

                    //循环取值每个y轴的坐标并加入集合
                    for (int i = 0; i < num + 1; i++) {
                        y[i] = MathUtil.tofixed(parseDouble(y_s[i]), 3);
                    }

                    Fitter = PolynomialCurveFitter.create(6);//创建拟合
                    WeightedObservedPoints points = new WeightedObservedPoints();
                    for (int i = 0; i < num + 1; i++) {
                        points.add(x[i], y[i]);
                    }
                    double[] result = Fitter.fit(points.toList());//常数项集合 顺序从最低次幂的常数到最高的常数项
                    System.out.println("拟合曲线初始化完成");

                    //解析图形化——磨后辊形曲线图
                    Profile_final_realpro = final_realpro.substring(final_realpro.indexOf("Profile:") + 10, final_realpro.indexOf("Map:") - 2).replace("\r\n", ",");
                    Map_final_realpro = final_realpro.split("Map:")[1].substring(2, final_realpro.split("Map:")[1].indexOf("Profile:") - 2).replace("\r\n", ",");

                    String[] x2_s = Profile_final_realpro.split(",");
                    String[] y2_s = Map_final_realpro.split(",");

                    int num2 = Integer.parseInt(x2_s[0]); // num2 实测数据区间数
                    double x2_min = parseDouble(x2_s[1]); //实测数据——x轴最小值
                    double x2_max = parseDouble(x2_s[2]); //实测数据——x轴最大值


                    double[] x2 = new double[num2 + 1]; // 实测数据——定义标准曲线X轴坐标集合
                    double[] y2 = new double[num2 + 1]; // 实测数据——定义标准曲线y轴坐标集合

                    double x2_d = (x2_max - x2_min) / num2; //实测数据——相邻x轴坐标之间的差值
                    //循环计算每个x轴的坐标并加入集合
                    for (int i = 0; i < num2 + 1; i++) {
                        x2[i] = MathUtil.tofixed(x2_min + x2_d * i, 3);
                    }
                    
                    //循环取值每个y轴的坐标并加入集合
                    for (int i = 0; i < num2 + 1; i++) {
                        y2[i] = MathUtil.tofixed(parseDouble(y2_s[i]), 3);
                    }

                    double erta = 0.00;
                    if (!StringUtils.isEmpty(rollGrindingBF.getTaper())){
                        erta = Math.tan((rollGrindingBF.getTaper() * Math.PI / 180));
                    }


                    //根据实测图X轴坐标，寻找标准中理应得到的Y轴数据，并计算差值，并记录合格数量
                    double[] ch = new double[num2 + 1];
                    List<Double> listys = new ArrayList<>();
                    int count = 0;
                    for (int i = 0; i < num2 + 1; i++) {
                        //yjs = 函数f（x）,计算出实测x轴对应的标准中的y轴参考值
                        double yjs = result[0] + result[1] * x2[i] + result[2] * Math.pow(x2[i], 2) + result[3] * Math.pow(x2[i], 3) + result[4] * Math.pow(x2[i], 4) + result[5] * Math.pow(x2[i], 5) + result[6] * Math.pow(x2[i], 6);
                        //修正后的真实y值
                        double ys = y2[i] + (num2 - i) * erta;
                        listys.add(ys);
                        ch[i] = Math.abs(yjs - ys);
                        if (ch[i] - parseDouble(standard_s[0]) < 0) {
                            count++;
                        }
                    }

                    map.put("合格点数", String.valueOf(count));
                    map.put("标准曲线X轴坐标", Arrays.toString(x));
                    map.put("标准曲线y轴坐标", Arrays.toString(y));
                    map.put("实测曲线X轴坐标", Arrays.toString(x2));
                    map.put("实测曲线y轴坐标", Arrays.toString(y2));
                    map.put("差值曲线y轴坐标", Arrays.toString(ch));
                    map.put("修正后y轴坐标", listys.toString());

                    JSONObject request = new JSONObject();
                    request.put("合格点数", count);
                    request.put("标准曲线X轴坐标", Arrays.toString(x));
                    request.put("标准曲线y轴坐标", Arrays.toString(y));
                    request.put("实测曲线X轴坐标", Arrays.toString(x2));
                    request.put("实测曲线y轴坐标", Arrays.toString(y2));
                    request.put("差值曲线y轴坐标", Arrays.toString(ch));
                    request.put("参数",Arrays.toString(result));
                    request.put("修正后y轴坐标", listys.toString());
                    request.put("A", erta);
                    logger.debug("该辊号对应的上下线列表" + request.toJSONString());
                }
            }
        }
        return map;
    }
}