package com.my.business.modular.rollonoffline.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollonoffline.entity.JRollOnoffLine;
import com.my.business.modular.rollonoffline.entity.RollOnoffLine;
import com.my.business.modular.rollonoffline.service.RollOnoffLineService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;


/**
 * 轧辊上下线管理控制器层
 *
 * @author 生成器生成
 * @date 2020-08-17 15:45:05
 */
@RestController
@RequestMapping("/rollOnoffLine")
public class RollOnoffLineController {

    @Autowired
    private RollOnoffLineService rollOnoffLineService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollOnoffLine(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollOnoffLineService.insertDataRollOnoffLine(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollOnoffLineOne(@RequestBody String data) {
        try {
            JRollOnoffLine jrollOnoffLine = JSON.parseObject(data, JRollOnoffLine.class);
            return rollOnoffLineService.deleteDataRollOnoffLineOne(jrollOnoffLine.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollOnoffLine jrollOnoffLine = JSON.parseObject(data, JRollOnoffLine.class);
            return rollOnoffLineService.deleteDataRollOnoffLineMany(jrollOnoffLine.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollOnoffLine(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollOnoffLineService.updateDataRollOnoffLine(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollOnoffLineByPage(@RequestBody String data) {
        return rollOnoffLineService.findDataRollOnoffLineByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findRollOnoffLineByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findRollOnoffLineByIndocno(@RequestBody String data) {
        return rollOnoffLineService.findRollOnoffLineByIndocno(data);
    }

    /**
     * 钢种块数重量报表
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findRollinfoMesByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findRollinfoMesByPage(@RequestBody String data) {
        return rollOnoffLineService.findRollinfoMesByPage(data);
    }

    /**
     * 导出 钢种块数重量报表 表数据
     */
    @CrossOrigin
    @RequestMapping(value = {"/excelRollinfoMesByPage"}, method = RequestMethod.GET)
    @ResponseBody
    public void getExcelWheelAbrasion(
            @RequestParam ( "type" ) String type,
            @RequestParam ( "roll_no" ) String roll_no,
            @RequestParam ( "frame_noid" ) String frame_noid,
            @RequestParam ( "factory_id" ) String factory_id,
            @RequestParam ( "framerangeid" ) String framerangeid,
            @RequestParam ( "roll_typeid" ) String roll_typeid,
            @RequestParam ( "material_id" ) String material_id,
            @RequestParam ( "dbegin" ) String dbegin,
            @RequestParam ( "dend" ) String dend,
            HttpServletResponse response, HttpServletRequest request) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        String fileName = "";//设置要导出的文件的名字
        fileName = rollOnoffLineService.excelRollinfoMesByPage(workbook,
                type
                ,roll_no
                ,frame_noid
                ,factory_id
                ,framerangeid
                ,roll_typeid
                ,material_id
                ,dbegin
                ,dend
        );
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1) + ".xls");//URLEncoder.encode(fileName, "utf-8")
        workbook.write(response.getOutputStream());
    }


    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollOnoffLineByIndocno(@RequestBody String data) {
        return rollOnoffLineService.findDataRollOnoffLineByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollOnoffLine> findDataRollOnoffLine() {
        return rollOnoffLineService.findDataRollOnoffLine();
    }

}
