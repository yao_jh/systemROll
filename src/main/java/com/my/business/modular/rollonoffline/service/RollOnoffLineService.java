package com.my.business.modular.rollonoffline.service;

import com.my.business.modular.rollonoffline.entity.RollOnoffLine;
import com.my.business.sys.common.entity.ResultData;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.List;

/**
 * 轧辊上下线管理接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-17 15:45:05
 */
public interface RollOnoffLineService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollOnoffLine(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollOnoffLineOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollOnoffLineMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollOnoffLine(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollOnoffLineByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollOnoffLineByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollOnoffLine> findDataRollOnoffLine();

    ResultData findRollinfoMesByPage(String data);

    String excelRollinfoMesByPage(HSSFWorkbook workbook, String type, String roll_no, String frame_noid, String factory_id, String framerangeid, String roll_typeid, String material_id, String dbegin, String dend);

    ResultData findRollOnoffLineByIndocno(String data);
}
