package com.my.business.modular.rollonoffline.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.dictionary.service.DictionaryService;
import com.my.business.modular.rckurumainfo.dao.RcKurumaInfoDao;
import com.my.business.modular.rckurumainfo.entity.RcKurumaInfo;
import com.my.business.modular.rollgrinding.entity.RollGrindingBFReport;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.modular.rollonoffline.dao.RollOnoffLineDao;
import com.my.business.modular.rollonoffline.entity.JRollOnoffLine;
import com.my.business.modular.rollonoffline.entity.RollOnoffLine;
import com.my.business.modular.rollonoffline.entity.RollOnoffLineReport;
import com.my.business.modular.rollonoffline.entity.RollOnoffLineView;
import com.my.business.modular.rollonoffline.service.RollOnoffLineService;
import com.my.business.modular.rollonofflinedetail.dao.RollOnoffLineDetailDao;
import com.my.business.modular.rollonofflinedetail.entity.RollOnoffLineDetail;
import com.my.business.modular.rollwear.service.RollWearService;
import com.my.business.restful.GradingKafkaService;
import com.my.business.sys.common.entity.MapXYEntity;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.workflow.workflow.service.WorkFlowService;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 轧辊上下线管理接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-17 15:45:05
 */
@Service
public class RollOnoffLineServiceImpl implements RollOnoffLineService {

    @Autowired
    private GradingKafkaService gradingKafkaService;

    @Autowired
    private RollOnoffLineDao rollOnoffLineDao;
    @Autowired
    private RollOnoffLineDetailDao detailDao;
    @Autowired
    private WorkFlowService workFlowService;
    @Autowired
    private RcKurumaInfoDao rcKurumaInfoDao;
    @Autowired
    private RollInformationDao rollInformationDao;
    @Autowired
    private DictionaryService dictionaryService;
    @Autowired
    private RollWearService rollWearService;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollOnoffLine(String data, Long userId, String sname) {
        try {
            JRollOnoffLine jrollOnoffLine = JSON.parseObject(data, JRollOnoffLine.class);
            RollOnoffLine rollOnoffLine = jrollOnoffLine.getRollOnoffLine();
            Integer i = rollOnoffLineDao.findIndocnoMax();
            Long indocno;
            if (i == null) {
                indocno = 1L;
            } else {
                indocno = Long.valueOf(i) + 1;
            }
            rollOnoffLine.setIndocno(indocno);
            CodiUtil.newRecord(userId, sname, rollOnoffLine);
            rollOnoffLineDao.insertDataRollOnoffLine(rollOnoffLine);

            //调用kafka发送成本数据
            gradingKafkaService.sendonoffline(rollOnoffLine);

            //更新全生命周期
            rollWearService.changedata(rollOnoffLine.getRoll_no());

            //操作子表
            List<RollOnoffLineDetail> detail = jrollOnoffLine.getDetail();
            if (detail != null && detail.size() > 0) {
                for (RollOnoffLineDetail p : detail) {
                    p.setIlinkno(indocno);
                    CodiUtil.newRecord(userId, sname, p);
                    detailDao.insertDataRollOnoffLineDetail(p);
                }
            }

            //把备辊的辊子变成在机的辊子
            String frame_no = rollOnoffLine.getFrame_no();//机架
            String roll_type = rollOnoffLine.getRoll_type();//轧辊类型
            String roll_position = rollOnoffLine.getRoll_position();//上机位置
            RcKurumaInfo r = rcKurumaInfoDao.findDataRcKurumaInfoByStandno(frame_no, roll_position, roll_type);
            if (r != null) {
                RollInformation ri = rollInformationDao.findDataRollInformationByRollNo(r.getRoll_id());
                ri.setRoll_revolve(7L);
                rollInformationDao.updateDataRollInformation(ri);
            }

            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollOnoffLineOne(Long indocno) {
        try {
            rollOnoffLineDao.deleteDataRollOnoffLineOne(indocno);
            detailDao.deleteDataRollOnoffLineDetailManyByIlinkno(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollOnoffLineMany(String str_id) {
        try {
            String sql = "delete roll_onoff_line where indocno in(" + str_id + ")";
            rollOnoffLineDao.deleteDataRollOnoffLineMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollOnoffLine(String data, Long userId, String sname) {
        try {
            JRollOnoffLine jrollOnoffLine = JSON.parseObject(data, JRollOnoffLine.class);
            RollOnoffLine rollOnoffLine = jrollOnoffLine.getRollOnoffLine();
            CodiUtil.editRecord(userId, sname, rollOnoffLine);
            rollOnoffLineDao.updateDataRollOnoffLine(rollOnoffLine);
//            detailDao.deleteDataRollOnoffLineDetailManyByIlinkno(rollOnoffLine.getIndocno());
            //操作子表
            List<RollOnoffLineDetail> detail = jrollOnoffLine.getDetail();
            if (detail != null && detail.size() > 0) {
                for (RollOnoffLineDetail p : detail) {
                    p.setIlinkno(rollOnoffLine.getIndocno());
                    CodiUtil.newRecord(userId, sname, p);
                    detailDao.insertDataRollOnoffLineDetail(p);
                }
            }
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    /*public ResultData findDataRollOnoffLineByPageNew(String data) {
        try {
            JRollOnoffLine jrollOnoffLine = JSON.parseObject(data, JRollOnoffLine.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollOnoffLine.getPageIndex();
            Integer pageSize = jrollOnoffLine.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollOnoffLine.getCondition()) {
                jsonObject = JSON.parseObject(jrollOnoffLine.getCondition().toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

            Long frame_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
                frame_noid = Long.valueOf(jsonObject.get("frame_noid").toString());
            }

            String frame_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_no"))) {
                frame_no = jsonObject.get("frame_no").toString();
            }

            String dbeginon = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbeginon"))) {
                dbeginon = jsonObject.get("dbeginon").toString();
            }

            String dendon = null;
            if (!StringUtils.isEmpty(jsonObject.get("dendon"))) {
                dendon = jsonObject.get("dendon").toString();
            }

            String dbeginoff = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbeginoff"))) {
                dbeginoff = jsonObject.get("dbeginoff").toString();
            }

            String dendoff = null;
            if (!StringUtils.isEmpty(jsonObject.get("dendoff"))) {
                dendoff = jsonObject.get("dendoff").toString();
            }

            Long production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
            }

            List<RollOnoffLine> list = rollOnoffLineDao.findDataRollOnoffLineByPage((pageIndex - 1) * pageSize, pageSize, roll_no, frame_noid, frame_no, dbeginon, dendon, dbeginoff, dendoff, production_line_id,null,null,null,null);

            for (RollOnoffLine r : list) {
                if (!StringUtils.isEmpty(r.getOff_line_reason())) {
                    r.setOff_line_reason_value(dictionaryService.findDataByV1("offline_reason", 1L, Long.valueOf(r.getOff_line_reason())));

                }
                if (!StringUtils.isEmpty(r.getRollinfo())) {
                    String   rollinfo  = r.getRollinfo().replace("\n", ",");
                    r.setRollinfo(rollinfo);
                }
            }
            Integer count = rollOnoffLineDao.findDataRollOnoffLineByPageSize(roll_no, frame_noid, frame_no, dbeginon, dendon, dbeginoff, dendoff, production_line_id,null,null,null,null);

            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }*/

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollOnoffLineByPage(String data) {
        try {
            JRollOnoffLine jrollOnoffLine = JSON.parseObject(data, JRollOnoffLine.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollOnoffLine.getPageIndex();
            Integer pageSize = jrollOnoffLine.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollOnoffLine.getCondition()) {
                jsonObject = JSON.parseObject(jrollOnoffLine.getCondition().toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

            Long frame_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
                frame_noid = Long.valueOf(jsonObject.get("frame_noid").toString());
            }

            String frame_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_no"))) {
                frame_no = jsonObject.get("frame_no").toString();
            }

            String dbeginon = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbeginon"))) {
                dbeginon = jsonObject.get("dbeginon").toString();
            }

            String dendon = null;
            if (!StringUtils.isEmpty(jsonObject.get("dendon"))) {
                dendon = jsonObject.get("dendon").toString();
            }

            String dbeginoff = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbeginoff"))) {
                dbeginoff = jsonObject.get("dbeginoff").toString();
            }

            String dendoff = null;
            if (!StringUtils.isEmpty(jsonObject.get("dendoff"))) {
                dendoff = jsonObject.get("dendoff").toString();
            }

            Long production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
            }

            String factory_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("factory_id"))) {
                factory_id = jsonObject.get("factory_id").toString();
            }

            String absolutely = null;//百分百
            if (!StringUtils.isEmpty(jsonObject.get("absolutely"))) {
                absolutely = jsonObject.get("absolutely").toString();
            }else{
                absolutely = 0+"";
            }
            String material_id = null;
            String ks = null;
            String kd = null;
            List<RollOnoffLine> list = null;
            Integer count =0;
            if(!StringUtils.isEmpty(jsonObject.get("state"))&&Long.valueOf((String) jsonObject.get("state"))==1L&&!StringUtils.isEmpty(jrollOnoffLine.getIndocno())){
                RollOnoffLine bean = rollOnoffLineDao.findDataRollOnoffLineByIndocno(jrollOnoffLine.getIndocno());

                if(bean!=null&&bean.getRoll_no().length()>=4&&!StringUtils.isEmpty(bean.getRollinfo())){
                    RollInformation b = rollInformationDao.findDataRollInformationByRollNo(bean.getRoll_no());
                    if(b!=null){
                        material_id = b.getMaterial_id()+"";
                    }
                    roll_no = bean.getRoll_no().substring(0,2);
                    ks = bean.getRoll_coil_num()+"";
                    kd = bean.getRollinfo().substring(bean.getRollinfo().indexOf("\n")+1,bean.getRollinfo().indexOf("\n")+3);
                   if(!"0".equals(absolutely)){
                       absolutely = BigDecimal.valueOf(100L-Long.valueOf(absolutely)).divide(BigDecimal.valueOf(100),2,BigDecimal.ROUND_HALF_UP)+"";
                       absolutely= String.valueOf(absolutely);
                   }else{
                       absolutely = null;
                   }
                    list= rollOnoffLineDao.findDataRollOnoffLineByPage((pageIndex - 1) * pageSize, pageSize, roll_no, frame_noid, frame_no, dbeginon, dendon, dbeginoff, dendoff, production_line_id,ks,absolutely,factory_id,kd,material_id);
                    for (RollOnoffLine r : list) {
                        if (!StringUtils.isEmpty(r.getOff_line_reason())) {
                            r.setOff_line_reason_value(dictionaryService.findDataByV1("offline_reason", 1L, Long.valueOf(r.getOff_line_reason())));
                        }
                        if (!StringUtils.isEmpty(r.getRollinfo())) {
                            String   rollinfo  = r.getRollinfo().replace("\n", ",");
                            r.setRollinfo(rollinfo);
                        }
                    }
                    count= rollOnoffLineDao.findDataRollOnoffLineByPageSize(roll_no, frame_noid, frame_no, dbeginon, dendon, dbeginoff, dendoff, production_line_id,ks,absolutely,factory_id,kd,material_id);

                }
            }else{
                list= rollOnoffLineDao.findDataRollOnoffLineByPage((pageIndex - 1) * pageSize, pageSize, roll_no, frame_noid, frame_no, dbeginon, dendon, dbeginoff, dendoff, production_line_id,null,null,null,null,null);
                for (RollOnoffLine r : list) {
                    if (!StringUtils.isEmpty(r.getOff_line_reason())) {
                        r.setOff_line_reason_value(dictionaryService.findDataByV1("offline_reason", 1L, Long.valueOf(r.getOff_line_reason())));
                    }
                    if (!StringUtils.isEmpty(r.getRollinfo())) {
                        String   rollinfo  = r.getRollinfo().replace("\n", ",");
                        r.setRollinfo(rollinfo);
                    }
                }
                count= rollOnoffLineDao.findDataRollOnoffLineByPageSize(roll_no, frame_noid, frame_no, dbeginon, dendon, dbeginoff, dendoff, production_line_id,null,null,null,null,null);
            }
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollOnoffLineByIndocno(String data) {
        try {
            JRollOnoffLine jrollOnoffLine = JSON.parseObject(data, JRollOnoffLine.class);
            Long indocno = jrollOnoffLine.getIndocno();

            RollOnoffLine rollOnoffLine = rollOnoffLineDao.findDataRollOnoffLineByIndocno(indocno);
            List<RollOnoffLineDetail> detail = detailDao.findDataRollOnoffLineDetailByIlinkno(indocno);
            rollOnoffLine.setDetail(detail);
            return ResultData.ResultDataSuccess(rollOnoffLine);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollOnoffLine> findDataRollOnoffLine() {
        List<RollOnoffLine> list = rollOnoffLineDao.findDataRollOnoffLine();
        return list;
    }

    public ResultData findRollinfoMesByPage(String data) {
        try {
            JRollOnoffLine jrollOnoffLine = JSON.parseObject(data, JRollOnoffLine.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollOnoffLine.getPageIndex();
            Integer pageSize = jrollOnoffLine.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollOnoffLine.getCondition()) {
                jsonObject = JSON.parseObject(jrollOnoffLine.getCondition().toString());
            }

            // 类型 1 是 机架范围  非1 是机架
            String type = null;
            if (!StringUtils.isEmpty(jsonObject.get("type"))) {
                type = jsonObject.get("type").toString();
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

            String frame_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
                frame_noid = jsonObject.get("frame_noid").toString();
            }
            String factory_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("factory_id"))) {
                factory_id = jsonObject.get("factory_id").toString();
            }
            String framerangeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("framerangeid"))) {
                framerangeid = jsonObject.get("framerangeid").toString();
            }
            String roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = jsonObject.get("roll_typeid").toString();
            }

            String material_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("material_id"))) {
                material_id = jsonObject.get("material_id").toString();
            }

            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }
            List<RollOnoffLineReport> list = null;
            Integer count = null;
            if(!StringUtils.isEmpty(type)&&type.equals("1")){
                 list = rollOnoffLineDao.findRollinforFramerangeMesByPage((pageIndex - 1) * pageSize,pageSize
                        ,roll_no,factory_id,framerangeid,roll_typeid,material_id,dbegin,dend);
                 count = rollOnoffLineDao.findRollinforFramerangeMesByPageSize(roll_no,factory_id,framerangeid,roll_typeid,material_id,dbegin,dend);
            }else{
                 list = rollOnoffLineDao.findRollinforFrameNoMesByPage((pageIndex - 1) * pageSize,pageSize
                        ,roll_no,frame_noid,factory_id,framerangeid,roll_typeid,material_id,dbegin,dend);
                 count= rollOnoffLineDao.findRollinforFrameNoMesByPageSize(roll_no,frame_noid,factory_id,framerangeid,roll_typeid,material_id,dbegin,dend);
            }
            List<MapXYEntity> map = new ArrayList<MapXYEntity>();
            //Map map3 = new HashMap();
            for (RollOnoffLineReport bean:list) {
                MapXYEntity t = new MapXYEntity();
                if(!StringUtils.isEmpty(type)&&type.equals("1")){
                    if(!StringUtils.isEmpty(bean.getRoll_type()+" "+bean.getMaterial()+" "+bean.getFramerange()+" "+bean.getGz())){
                        t.setX(bean.getRoll_type()+" "+bean.getMaterial()+" "+bean.getFramerange()+" "+bean.getGz());
                    }else{
                        t.setX(0);
                    }
                    if(!StringUtils.isEmpty(bean.getKs())){
                        t.setY1(bean.getKs());
                    }else{
                        t.setY1(0);
                    }
                    if(!StringUtils.isEmpty(bean.getZl())){
                        t.setY2(bean.getZl());
                    }else{
                        t.setY2(0);
                    }
                }else{
                    if(!StringUtils.isEmpty(bean.getRoll_type()+" "+bean.getMaterial()+" "+bean.getFrame_no()+" "+bean.getGz())){
                        t.setX(bean.getRoll_type()+" "+bean.getMaterial()+" "+bean.getFrame_no()+" "+bean.getGz());
                    }else{
                        t.setX(0);
                    }
                    if(!StringUtils.isEmpty(bean.getKs())){
                        t.setY1(bean.getKs());
                    }else{
                        t.setY1(0);
                    }
                    if(!StringUtils.isEmpty(bean.getZl())){
                        t.setY2(bean.getZl());
                    }else{
                        t.setY2(0);
                    }
                }
                map.add(t);
            }

            return ResultData.ResultDataSuccess(list, count,null,map);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public String excelRollinfoMesByPage(HSSFWorkbook workbook,
                                         String type, String roll_no,
                                         String frame_noid, String factory_id,
                                         String framerangeid, String roll_typeid,
                                         String material_id,
                                         String dbegin, String dend) {
        String filename="钢种块数重量";

        if(StringUtils.isEmpty(roll_no)){
            roll_no = null;
        }
        if(StringUtils.isEmpty(frame_noid)){
            frame_noid = null;
        }
        if(StringUtils.isEmpty(factory_id)){
            factory_id = null;
        }
        if(StringUtils.isEmpty(framerangeid)){
            framerangeid = null;
        }
        if(StringUtils.isEmpty(roll_typeid)){
            roll_typeid = null;
        }
        if(StringUtils.isEmpty(material_id)){
            material_id = null;
        }
        if(StringUtils.isEmpty(dbegin)){
            dbegin = null;
        }
        if(StringUtils.isEmpty(dend)){
            dend = null;
        }
        List<RollOnoffLineReport> list = null;
        if(!StringUtils.isEmpty(type)&&type.equals("1")){
            list = rollOnoffLineDao.findRollinforFramerangeMesByPage(0,100
                    ,roll_no,factory_id,framerangeid,roll_typeid,material_id,dbegin,dend);
        }else{
            list = rollOnoffLineDao.findRollinforFrameNoMesByPage(0,100
                    ,roll_no,frame_noid,factory_id,framerangeid,roll_typeid,material_id,dbegin,dend);
        }
        HSSFSheet sheet = workbook.createSheet("sheet1");
        getExcelRollinfoMesByPage(list,sheet, workbook);
        return filename;
    }

    @Override
    public ResultData findRollOnoffLineByIndocno(String data) {
        try {
            JRollOnoffLine jrollOnoffLine = JSON.parseObject(data, JRollOnoffLine.class);
            Long indocno = jrollOnoffLine.getIndocno();

            List<RollOnoffLineView> list = rollOnoffLineDao.findRollOnoffLineByIndocno(indocno);
            return ResultData.ResultDataSuccess(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    private void getExcelRollinfoMesByPage(List<RollOnoffLineReport> list, HSSFSheet sheet, HSSFWorkbook workbook) {
        if (list != null && list.size() > 0) {
            // 设置表头样式 居中 加粗 宋体 20
            HSSFCellStyle cellStyleHead = workbook.createCellStyle();
            cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
            HSSFFont redFontHead = workbook.createFont();
            redFontHead.setFontHeightInPoints((short) 12);//设置字体大小
            redFontHead.setFontName("宋体");//字体型号
            cellStyleHead.setFont(redFontHead);
            HSSFRow rowhead = sheet.createRow(0);   //创建第一行头
            HSSFCell cell_00 = rowhead.createCell(0);   //创建第一行第一列
            cell_00.setCellStyle(cellStyleHead);
            cell_00.setCellValue("钢种块数重量");
            HSSFRow row1 = sheet.createRow(1);   //创建第二行
            HSSFCell cell_2_1 = row1.createCell(0);   //创建第二行第一列
            cell_2_1.setCellValue("序号");
            HSSFCell cell_2_2 = row1.createCell(1);   //创建第二行第九列
            cell_2_2.setCellValue("客户");
            HSSFCell cell_2_3 = row1.createCell(2);   //创建第二行第二列
            cell_2_3.setCellValue("钢种");
            HSSFCell cell_2_4 = row1.createCell(3);   //创建第二行第三列
            cell_2_4.setCellValue("块数");
            HSSFCell cell_2_5 = row1.createCell(4);   //创建第二行第三列
            cell_2_5.setCellValue("重量");
            //从第三行开始(包含第三行)
            for (int i = 0; i < list.size(); i++) {
                RollOnoffLineReport v = list.get(i);
                HSSFRow rown = sheet.createRow(i + 2);   //创建第三行
                HSSFCell cell_3_1 = rown.createCell(0);   // 1
                cell_3_1.setCellValue(i + 1);
                HSSFCell cell_3_2 = rown.createCell(1);   //2
                if (!StringUtils.isEmpty(v.getFactory())) {
                    cell_3_2.setCellValue(v.getFactory());
                }
                HSSFCell cell_3_3 = rown.createCell(2);   //3
                if (!StringUtils.isEmpty(v.getGz())) {
                    cell_3_3.setCellValue(v.getGz());
                }
                HSSFCell cell_3_4 = rown.createCell(3);   //4
                if (!StringUtils.isEmpty(v.getKs())) {
                    cell_3_4.setCellValue(v.getKs().toString());
                }
                HSSFCell cell_3_5 = rown.createCell(4);   //4
                if (!StringUtils.isEmpty(v.getZl())) {
                    cell_3_5.setCellValue(v.getZl().toString());
                }
            }
            CellRangeAddress region = new CellRangeAddress(0, 0, 0, 4);
            sheet.addMergedRegion(region);
        }
    }

}
