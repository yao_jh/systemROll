package com.my.business.modular.rollonoffline.entity;

import com.my.business.modular.rollonofflinedetail.entity.RollOnoffLineDetail;
import com.my.business.sys.common.entity.BaseEntity;

import java.math.BigDecimal;
import java.util.List;

/**
 * 轧辊上下线管理实体类
 *
 * @author 生成器生成
 * @date 2020-08-17 15:45:04
 */
public class RollOnoffLine extends BaseEntity {
	private String off_line_reason_value;  //下线原因

    private Long indocno;  //主键
    private String roll_no;  //辊号
    private Long production_line_id;  //产线id
    private String production_line;  //产线
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private String frame_noid;  //机架号id
    private String frame_no;  //机架号
    private Long roll_positionid;  //轧辊位置id
    private String roll_position;  //轧辊位置
    private String loomingposition;  //上机位置
    private Long rollshapeid;  //辊形id
    private String rollshape;  //辊形
    private String material;  //材质
    private String ds_chock_no;  //DS侧座号
    private String os_chock_no;  //OS侧座号
    private String onlinetime;  //上机时间
    private Long onlinecount;  //上机次数
    private String offlinetime;  //下机时间
    private Double rollkilometer;  //轧制公里数
    private Double rolltonnage;  //轧制吨数
    private String off_line_reason;  //下线原因
    private String dbeginon;  //上机时间从
    private String dendon;  //上机时间至
    private String dbeginoff;  //下机时间从
    private String dendoff;  //下机时间至
    private Long uplinecount;  //累积上线次数

    private Long  roll_coil_num;//轧制数量
    private Long rolling_time;//轧制时间
    private Long crown ;//轧制凸度
    private String rollinfo;  //轧制钢种、宽度、重量等信息

    private BigDecimal updiam;//上机直径

    public BigDecimal getUpdiam() {
        return updiam;
    }

    public void setUpdiam(BigDecimal updiam) {
        this.updiam = updiam;
    }

    public Long getRoll_coil_num() {
        return roll_coil_num;
    }

    public void setRoll_coil_num(Long roll_coil_num) {
        this.roll_coil_num = roll_coil_num;
    }

    public Long getRolling_time() {
        return rolling_time;
    }

    public void setRolling_time(Long rolling_time) {
        this.rolling_time = rolling_time;
    }

    public Long getCrown() {
        return crown;
    }

    public void setCrown(Long crown) {
        this.crown = crown;
    }

    public String getRollinfo() {
        return rollinfo;
    }

    public void setRollinfo(String rollinfo) {
        this.rollinfo = rollinfo;
    }

    private List<RollOnoffLineDetail> detail;

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public Long getRoll_typeid() {
        return this.roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public String getFrame_noid() {
        return this.frame_noid;
    }

    public void setFrame_noid(String frame_noid) {
        this.frame_noid = frame_noid;
    }

    public String getFrame_no() {
        return this.frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public String getLoomingposition() {
        return this.loomingposition;
    }

    public void setLoomingposition(String loomingposition) {
        this.loomingposition = loomingposition;
    }

    public Long getRollshapeid() {
        return this.rollshapeid;
    }

    public void setRollshapeid(Long rollshapeid) {
        this.rollshapeid = rollshapeid;
    }

    public String getRollshape() {
        return this.rollshape;
    }

    public void setRollshape(String rollshape) {
        this.rollshape = rollshape;
    }

    public String getMaterial() {
        return this.material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getDs_chock_no() {
        return this.ds_chock_no;
    }

    public void setDs_chock_no(String ds_chock_no) {
        this.ds_chock_no = ds_chock_no;
    }

    public String getOs_chock_no() {
        return this.os_chock_no;
    }

    public void setOs_chock_no(String os_chock_no) {
        this.os_chock_no = os_chock_no;
    }

    public String getOnlinetime() {
        return this.onlinetime;
    }

    public void setOnlinetime(String onlinetime) {
        this.onlinetime = onlinetime;
    }

    public String getOfflinetime() {
        return this.offlinetime;
    }

    public void setOfflinetime(String offlinetime) {
        this.offlinetime = offlinetime;
    }

    public Double getRollkilometer() {
        return this.rollkilometer;
    }

    public void setRollkilometer(Double rollkilometer) {
        this.rollkilometer = rollkilometer;
    }

    public Double getRolltonnage() {
        return this.rolltonnage;
    }

    public void setRolltonnage(Double rolltonnage) {
        this.rolltonnage = rolltonnage;
    }

    public String getOff_line_reason() {
        return this.off_line_reason;
    }

    public void setOff_line_reason(String off_line_reason) {
        this.off_line_reason = off_line_reason;
    }

    public String getDbeginon() {
        return dbeginon;
    }

    public void setDbeginon(String dbeginon) {
        this.dbeginon = dbeginon;
    }

    public String getDendon() {
        return dendon;
    }

    public void setDendon(String dendon) {
        this.dendon = dendon;
    }

    public String getDbeginoff() {
        return dbeginoff;
    }

    public void setDbeginoff(String dbeginoff) {
        this.dbeginoff = dbeginoff;
    }

    public String getDendoff() {
        return dendoff;
    }

    public void setDendoff(String dendoff) {
        this.dendoff = dendoff;
    }

    public List<RollOnoffLineDetail> getDetail() {
        return detail;
    }

    public void setDetail(List<RollOnoffLineDetail> detail) {
        this.detail = detail;
    }

    public Long getProduction_line_id() {
        return production_line_id;
    }

    public void setProduction_line_id(Long production_line_id) {
        this.production_line_id = production_line_id;
    }

    public String getProduction_line() {
        return production_line;
    }

    public void setProduction_line(String production_line) {
        this.production_line = production_line;
    }

	public Long getOnlinecount() {
		return onlinecount;
	}

	public void setOnlinecount(Long onlinecount) {
		this.onlinecount = onlinecount;
	}

    public Long getUplinecount() {
        return uplinecount;
    }

    public void setUplinecount(Long uplinecount) {
        this.uplinecount = uplinecount;
    }

	public Long getRoll_positionid() {
		return roll_positionid;
	}

	public void setRoll_positionid(Long roll_positionid) {
		this.roll_positionid = roll_positionid;
	}

	public String getRoll_position() {
		return roll_position;
	}

	public void setRoll_position(String roll_position) {
		this.roll_position = roll_position;
	}

	public String getOff_line_reason_value() {
		return off_line_reason_value;
	}

	public void setOff_line_reason_value(String off_line_reason_value) {
		this.off_line_reason_value = off_line_reason_value;
	}
	
}