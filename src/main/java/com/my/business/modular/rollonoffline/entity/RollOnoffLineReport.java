package com.my.business.modular.rollonoffline.entity;

import com.my.business.modular.rollonofflinedetail.entity.RollOnoffLineDetail;
import com.my.business.sys.common.entity.BaseEntity;

import java.math.BigDecimal;
import java.util.List;

/**
 *  钢种宽度块数报表 实体类
 *
 * @author 生成器生成
 * @date 2020-08-17 15:45:04
 */
public class RollOnoffLineReport extends BaseEntity {
    private Long indocno;  //主键
    private Long parent_id;  //主键
    private String roll_no;  //辊号
    private String factory;//客户
    private Long factory_id;  //客户id
    private String framerange;//机架范围
    private Long framerangeid;  //机架范围id
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private String frame_noid;  //机架号id
    private String frame_no;  //机架号
    private String material;  //材质
    private Long material_id;  //材质id
    private String onlinetime;  //上机时间
    private String offlinetime;  //下机时间
    private String grind_starttime;  //磨削时间
    private String grind_endtime;  //磨削时间
    private BigDecimal rollkilometer;  //轧制公里数
    private BigDecimal rolltonnage;  //轧制吨数
    private BigDecimal wear;//消耗
    private BigDecimal working_layer;//工作层重
    private String numvalue;//轧制钢种、宽度、重量等信息 的值
    private BigDecimal num;// 1钢种 2宽度 3块数 0重量

    private String gz;//钢种
    private BigDecimal kd;//宽度
    private BigDecimal ks;//块数
    private BigDecimal zl;//重量

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getParent_id() {
        return parent_id;
    }

    public void setParent_id(Long parent_id) {
        this.parent_id = parent_id;
    }

    public String getGz() {
        return gz;
    }

    public void setGz(String gz) {
        this.gz = gz;
    }

    public BigDecimal getKd() {
        return kd;
    }

    public void setKd(BigDecimal kd) {
        this.kd = kd;
    }

    public BigDecimal getKs() {
        return ks;
    }

    public void setKs(BigDecimal ks) {
        this.ks = ks;
    }

    public BigDecimal getZl() {
        return zl;
    }

    public void setZl(BigDecimal zl) {
        this.zl = zl;
    }

    public String getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getFactory() {
        return factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public Long getFactory_id() {
        return factory_id;
    }

    public void setFactory_id(Long factory_id) {
        this.factory_id = factory_id;
    }

    public String getFramerange() {
        return framerange;
    }

    public void setFramerange(String framerange) {
        this.framerange = framerange;
    }

    public Long getFramerangeid() {
        return framerangeid;
    }

    public void setFramerangeid(Long framerangeid) {
        this.framerangeid = framerangeid;
    }

    public Long getRoll_typeid() {
        return roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public String getFrame_noid() {
        return frame_noid;
    }

    public void setFrame_noid(String frame_noid) {
        this.frame_noid = frame_noid;
    }

    public String getFrame_no() {
        return frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public Long getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(Long material_id) {
        this.material_id = material_id;
    }

    public String getOnlinetime() {
        return onlinetime;
    }

    public void setOnlinetime(String onlinetime) {
        this.onlinetime = onlinetime;
    }

    public String getOfflinetime() {
        return offlinetime;
    }

    public void setOfflinetime(String offlinetime) {
        this.offlinetime = offlinetime;
    }

    public String getGrind_starttime() {
        return grind_starttime;
    }

    public void setGrind_starttime(String grind_starttime) {
        this.grind_starttime = grind_starttime;
    }

    public String getGrind_endtime() {
        return grind_endtime;
    }

    public void setGrind_endtime(String grind_endtime) {
        this.grind_endtime = grind_endtime;
    }

    public BigDecimal getRollkilometer() {
        return rollkilometer;
    }

    public void setRollkilometer(BigDecimal rollkilometer) {
        this.rollkilometer = rollkilometer;
    }

    public BigDecimal getRolltonnage() {
        return rolltonnage;
    }

    public void setRolltonnage(BigDecimal rolltonnage) {
        this.rolltonnage = rolltonnage;
    }

    public BigDecimal getWear() {
        return wear;
    }

    public void setWear(BigDecimal wear) {
        this.wear = wear;
    }

    public BigDecimal getWorking_layer() {
        return working_layer;
    }

    public void setWorking_layer(BigDecimal working_layer) {
        this.working_layer = working_layer;
    }

    public String getNumvalue() {
        return numvalue;
    }

    public void setNumvalue(String numvalue) {
        this.numvalue = numvalue;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }
}