package com.my.business.modular.rollonoffline.entity;

/**
 * 轧辊上下线管理实体发成本
 *
 * @author 生成器生成
 * @date 2020-08-17 15:45:04
 */
public class RollOnoffLineBc {

	private String GAPsender;
	private String GAPreceiver;
	private String roll_no; // 辊号
	private String onlinetime; // 上机时间
	private String offlinetime; // 下机时间
	
	public String getGAPsender() {
		return GAPsender;
	}

	public void setGAPsender(String gAPsender) {
		GAPsender = gAPsender;
	}

	public String getGAPreceiver() {
		return GAPreceiver;
	}

	public void setGAPreceiver(String gAPreceiver) {
		GAPreceiver = gAPreceiver;
	}

	public String getRoll_no() {
		return roll_no;
	}

	public void setRoll_no(String roll_no) {
		this.roll_no = roll_no;
	}

	public String getOnlinetime() {
		return onlinetime;
	}

	public void setOnlinetime(String onlinetime) {
		this.onlinetime = onlinetime;
	}

	public String getOfflinetime() {
		return offlinetime;
	}

	public void setOfflinetime(String offlinetime) {
		this.offlinetime = offlinetime;
	}

}