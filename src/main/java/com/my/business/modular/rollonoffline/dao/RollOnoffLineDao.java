package com.my.business.modular.rollonoffline.dao;

import com.my.business.modular.rollonoffline.entity.RollOnoffLine;
import com.my.business.modular.rollonoffline.entity.RollOnoffLineReport;
import com.my.business.modular.rollonoffline.entity.RollOnoffLineView;
import com.my.business.restful.entity.Roll_performent;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轧辊上下线管理dao接口
 *
 * @author 生成器生成
 * @date 2020-08-17 15:45:05
 */
@Mapper
public interface RollOnoffLineDao {

    /**
     * 添加记录
     *
     * @param rollOnoffLine 对象实体
     */
    void insertDataRollOnoffLine(RollOnoffLine rollOnoffLine);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollOnoffLineOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollOnoffLineMany(String value);

    /**
     * 修改记录
     *
     * @param rollOnoffLine 对象实体
     */
    void updateDataRollOnoffLine(RollOnoffLine rollOnoffLine);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollOnoffLine> findDataRollOnoffLineByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("roll_no") String roll_no, @Param("frame_noid") Long frame_noid, @Param("frame_no") String frame_no, @Param("dbeginon") String dbeginon, @Param("dendon") String dendon, @Param("dbeginoff") String dbeginoff, @Param("dendoff") String dendoff, @Param("production_line_id") Long production_line_id
            ,@Param("ks") String ks
            ,@Param("absolutely") String absolutely
            ,@Param("factory_id") String factory_id
            ,@Param("kd") String kd
                                                    ,@Param("material_id") String material_id
    );

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollOnoffLineByPageSize(@Param("roll_no") String roll_no, @Param("frame_noid") Long frame_noid, @Param("frame_no") String frame_no, @Param("dbeginon") String dbeginon, @Param("dendon") String dendon, @Param("dbeginoff") String dbeginoff, @Param("dendoff") String dendoff, @Param("production_line_id") Long production_line_id
            ,@Param("ks") String ks
            ,@Param("absolutely") String absolutely
            ,@Param("factory_id") String factory_id
            ,@Param("kd") String kd
            ,@Param("material_id") String material_id
    );

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollOnoffLine findDataRollOnoffLineByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollOnoffLine> findDataRollOnoffLine();
    
    /**
     * 查看istate = 0的记录
     *
     * @return 对象数据集合
     */
    List<RollOnoffLine> findDataRollOnoffLineByIstate();
    
    /**
     * 查看最大主键数
     *
     * @return 对象数据集合
     */
    Integer findIndocnoMax();
    
    /***
     * 根据时间和机架号查询数据
     * @param out_stand_start
     * @param out_stand_end
     * @param frame_no
     * @return
     */
    List<Roll_performent> findRest(@Param("out_stand_start") String out_stand_start,@Param("out_stand_end") String out_stand_end,@Param("frame_no") String frame_no);


    /**
     * 根据辊号和产线，进行分页查询，按照时间倒序排序
     * @param roll_no 辊号
     * @param production_line_id 产线
     * @return
     */
    List<RollOnoffLine> findDataRollOnoffLineByPageOrderByTime(@Param("roll_no") String roll_no,@Param("production_line_id") Long production_line_id);

    /**
     * 查找上一次上下线的记录
     * @param roll_no 辊号
     * @param onlinetime 本次上线时间
     * @return
     */
    RollOnoffLine findLastTime(@Param("roll_no") String roll_no,@Param("onlinetime") String onlinetime);

    List<RollOnoffLineReport> findRollinforFramerangeMesByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,
                                                               @Param("roll_no")String roll_no,
                                                               @Param("factory_id")String factory_id, @Param("framerangeid")String framerangeid,
                                                               @Param("roll_typeid")String roll_typeid, @Param("material_id")String material_id,
                                                               @Param("dbegin")String dbegin, @Param("dend")String dend);
    Integer findRollinforFramerangeMesByPageSize(@Param("roll_no")String roll_no,
                                                 @Param("factory_id")String factory_id, @Param("framerangeid")String framerangeid,
                                                 @Param("roll_typeid")String roll_typeid, @Param("material_id")String material_id,
                                                 @Param("dbegin")String dbegin, @Param("dend")String dend);
    List<RollOnoffLineReport> findRollinforFrameNoMesByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,
                                                            @Param("roll_no")String roll_no, @Param("frame_noid")String frame_noid,
                                                            @Param("factory_id")String factory_id, @Param("framerangeid")String framerangeid,
                                                            @Param("roll_typeid")String roll_typeid, @Param("material_id")String material_id,
                                                            @Param("dbegin")String dbegin, @Param("dend")String dend);
    Integer findRollinforFrameNoMesByPageSize(@Param("roll_no")String roll_no, @Param("frame_noid")String frame_noid,
                                              @Param("factory_id")String factory_id, @Param("framerangeid")String framerangeid,
                                              @Param("roll_typeid")String roll_typeid, @Param("material_id")String material_id,
                                              @Param("dbegin")String dbegin, @Param("dend")String dend);

    void deleteDataRollOnoffLineView(@Param("indocno") Long indocno);

    void insertDataRollOnoffLineView(RollOnoffLineView bean);

    void deleteDuplicateDataRollOnoffLineDaoMessage();

    List<RollOnoffLineView> findRollOnoffLineByIndocno(@Param("indocno")Long indocno);


}
