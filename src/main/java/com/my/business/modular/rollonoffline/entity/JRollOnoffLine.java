package com.my.business.modular.rollonoffline.entity;

import com.my.business.modular.rollonofflinedetail.entity.RollOnoffLineDetail;
import com.my.business.sys.common.entity.JCommon;

import java.util.List;

/**
 * 轧辊上下线管理json的实体类
 *
 * @author 生成器生成
 * @date 2020-08-17 15:45:05
 */
public class JRollOnoffLine extends JCommon {

    private Integer pageIndex; // 第几页
    private Integer pageSize; // 每页多少数据
    private Object condition; // 查询条件
    private RollOnoffLine rollOnoffLine;   //对应模块的实体类
    private Long indocno;
    private String str_indocno;
    private List<RollOnoffLineDetail> detail;

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Object getCondition() {
        return condition;
    }

    public void setCondition(Object condition) {
        this.condition = condition;
    }

    public RollOnoffLine getRollOnoffLine() {
        return rollOnoffLine;
    }

    public void setRollOnoffLine(RollOnoffLine rollOnoffLine) {
        this.rollOnoffLine = rollOnoffLine;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getStr_indocno() {
        return str_indocno;
    }

    public void setStr_indocno(String str_indocno) {
        this.str_indocno = str_indocno;
    }

    public List<RollOnoffLineDetail> getDetail() {
        return detail;
    }

    public void setDetail(List<RollOnoffLineDetail> detail) {
        this.detail = detail;
    }
}