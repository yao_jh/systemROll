package com.my.business.modular.monthreport.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.modular.monthreport.entity.MonthReport;
import com.my.business.modular.monthreport.dao.MonthReportDao;
import com.my.business.modular.monthreport.entity.JMonthReport;
import com.my.business.modular.monthreport.service.MonthReportService;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import com.my.business.sys.common.entity.ResultData;
import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.my.business.util.CodiUtil;
import org.springframework.web.bind.annotation.RequestHeader;


/**
* 月报表控制器层
* @author  生成器生成
* @date 2020-11-23 14:12:05
*/
@RestController
@RequestMapping("/monthReport")
public class MonthReportController {

	@Autowired
	private MonthReportService monthReportService;
	
	/**
	 * 添加记录
	 * @param userId 用户id
     * @param sname 用户姓名
	 * */
	@CrossOrigin
	@RequestMapping(value={"/insert"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData insertDataMonthReport(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return monthReportService.insertDataMonthReport(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
	 * 根据主键删除对象
	 * @param data json字符串
	 * */
	@CrossOrigin
	@RequestMapping(value={"/deleteOne"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData deleteDataMonthReportOne(@RequestBody String data){
		try{
    		JMonthReport jmonthReport = JSON.parseObject(data,JMonthReport.class);
    		return monthReportService.deleteDataMonthReportOne(jmonthReport.getIndocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
    	try{
    		JMonthReport jmonthReport = JSON.parseObject(data,JMonthReport.class);
    		return monthReportService.deleteDataMonthReportMany(jmonthReport.getStr_indocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
    }
	
	/**
     * 修改记录
     * @param data json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
	@RequestMapping(value={"/update"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData updateDataMonthReport(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return monthReportService.updateDataMonthReport(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
     * 分页查看记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataMonthReportByPage(@RequestBody String data) {
        return monthReportService.findDataMonthReportByPage(data);
    }
    
    /**
     * 根据主键查询单条记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataMonthReportByIndocno(@RequestBody String data) {
        return monthReportService.findDataMonthReportByIndocno(data);
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	@CrossOrigin
	@RequestMapping(value={"/findAll"}, method=RequestMethod.POST)
	public List<MonthReport> findDataMonthReport(){
		return monthReportService.findDataMonthReport();
	};
}
