package com.my.business.modular.monthreport.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.monthreport.entity.MonthReport;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * 月报表dao接口
 * @author  生成器生成
 * @date 2020-11-23 14:12:05
 * */
@Mapper
public interface MonthReportDao {

	/**
	 * 添加记录
	 * @param monthReport  对象实体
	 * */
	public void insertDataMonthReport(MonthReport monthReport);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataMonthReportOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataMonthReportMany(String value);
	
	/**
	 * 修改记录
	 * @param monthReport  对象实体
	 * */
	public void updateDataMonthReport(MonthReport monthReport);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<MonthReport> findDataMonthReportByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataMonthReportByPageSize();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public MonthReport findDataMonthReportByIndocno(@Param("indocno") Long indocno);
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public Map<String,Object> findDataToMap(@Param("month") String month);
    
    /***
     * 根据年月查询数据
     * @param indocno 用户id
     * @return
     */
    public MonthReport findDataMonthReportByMonth(@Param("month") String month);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<MonthReport> findDataMonthReport();
	
}
