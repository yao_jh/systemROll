package com.my.business.modular.monthreport.entity;

/**
* 月报表实体类
* @author  生成器生成
* @date 2020-11-23 14:12:04
*/
public class MonthReport{
		
	private String month;  //年月
	private String sbz; //班组
	private Double rollsum_2250;  //2250轧辊辊耗
	private Double f1f3fb_roll_week; //F1-F3支撑辊轧制周期
	private Double f4f7fb_roll_week; //F4-F7支撑辊轧制周期
	private Double r1r2rb_roll_week; //R1R2支撑辊轧制周期
	private Double machine_rate_denominator;  //磨床可开动率分母
	private Double machine_rate_numerator;//磨床可开动率分子
	private String machine_rate;  //2250磨床可开动率
	private Double roll_grade_rate_denominator;  //工作辊磨削合格点数分母
	private Double roll_grade_rate_numerator;//工作辊磨削合格点数分子
	private String roll_grade_rate;  //工作辊磨削合格点数
	private Double roll_rw_week;  //支撑辊轧制周期（热轧支撑辊长周期服役技术）
	private Double total_rollconsum_permonth;  //班组月轧辊总消耗
	private Double grinder_failtimes_premonth;  //班组磨辊间磨床故障次数
	private Float total_grinding_pre_month;  //班组总磨削量
	private Float total_onwear_permonth;  //班组在线磨损总量
	private Double f1f4fw_totalconsum;  //F1-F4工作辊消耗总量
	private Double f5f7fw_totalconsum;  //F5-F7工作辊消耗总量
	private Double fb_totalconsum;  //精轧支撑辊消耗总量
	private Double fe_totalconsum;  //精轧立辊消耗总量
	private Double rw_totalconsum;  //粗轧工作辊消耗总量
	private Double rb_totalconsum;  //粗轧支撑辊消耗总量
	private Double re_totalconsum;  //粗轧立辊消耗总量
	private Float f1f4fw_total_costconsum;  //F1-F4工作辊消耗金额
	private Float f5f7fw_total_costconsum;  //F5-F7工作辊消耗总金额
	private Float fb_total_costconsum;  //精轧支撑辊消耗总金额
	private Float fe_total_costconsum;  //精轧立辊消耗总金额
	private Float rw_total_costconsum;  //粗轧工作辊消耗总金额
	private Float rb_total_costconsum;  //粗轧支撑辊消耗总金额
	private Float re_total_costconsum;  //粗轧立辊消耗总金额
	private Long f1f4fw_inservice_amount;  //F1-F4工作辊在役支数
	private Long f5f7fw_inservice_amount;  //F5-F7工作辊在役支数
	private Long fb_inservice_amount;  //精轧支撑辊在役支数
	private Long fe_inservice_amount;  //精轧立辊在役支数
	private Long rw_inservice_amount;  //粗轧工作辊在役支数
	private Long rb_inservice_amount;  //粗轧支撑辊在役支数
	private Long re_inservice_amount;  //粗轧立辊在役支数
	private Float f1f4fw_preton_consum;  //F1-F4工作辊吨钢消耗量
	private Float f5f7fw_preton_consum;  //F5-F7工作辊吨钢消耗量
	private Float fb_preton_consum;  //精轧支撑辊吨钢消耗量
	private Float fe_preton_consum;  //精轧立辊吨钢消耗量
	private Float rw_preton_consum;  //粗轧工作辊吨钢消耗量
	private Float rb_preton_consum;  //粗轧支撑辊吨钢消耗量
	private Float re_preton_consum;  //粗轧立辊吨钢消耗量
	private Float f1f4fw_preton_costconsum;  //F1-F4工作辊吨钢消耗金额
	private Float f5f7fw_preton_costconsum;  //F5-F7工作辊吨钢消耗金额
	private Float fb_preton_costconsum;  //精轧支撑辊吨钢消耗金额
	private Float fe_preton_costconsum;  //精轧立辊吨钢消耗金额
	private Float rw_preton_costconsum;  //粗轧工作辊吨钢消耗金额
	private Float rb_preton_costconsum;  //粗轧支撑辊吨钢消耗金额
	private Float re_preton_costconsum;  //粗轧立辊吨钢消耗金额
	private Double f1f4fw_tonnage; //F1-F4工作辊轧制吨位
	private Double f5f7fw_tonnage; //F5-F7工作辊轧制吨位
	private Double fb_tonnage; //精轧支撑辊轧制吨位
	private Double fe_tonnage; //精轧立辊轧制吨位
	private Double rw_tonnage; //粗轧工作辊轧制吨位
	private Double rb_tonnage; //粗轧支撑辊轧制吨位
	private Double re_tonnage; //粗轧立辊轧制吨位
	private Double f1f4fw_gradingsum; //F1-F4工作辊磨削总量
	private Double f5f7fw_gradingsum; //F5-F7工作辊磨削总量
	private Double fb_gradingsum; //精轧支撑辊磨削总量
	private Double fe_gradingsum; //精轧立辊磨削总量
	private Double rw_gradingsum; //粗轧工作辊磨削总量
	private Double rb_gradingsum; //粗轧支撑辊磨削总量
	private Double re_gradingsum; //粗轧立辊磨削总量
	
    
	public void setMonth(String month){
	    this.month = month;
	}
	public String getMonth(){
	    return this.month;
	}
	public Double getF1f3fb_roll_week() {
		return f1f3fb_roll_week;
	}
	public void setF1f3fb_roll_week(Double f1f3fb_roll_week) {
		this.f1f3fb_roll_week = f1f3fb_roll_week;
	}
	public Double getF4f7fb_roll_week() {
		return f4f7fb_roll_week;
	}
	public void setF4f7fb_roll_week(Double f4f7fb_roll_week) {
		this.f4f7fb_roll_week = f4f7fb_roll_week;
	}
	public Double getR1r2rb_roll_week() {
		return r1r2rb_roll_week;
	}
	public void setR1r2rb_roll_week(Double r1r2rb_roll_week) {
		this.r1r2rb_roll_week = r1r2rb_roll_week;
	}
	public void setRollsum_2250(Double rollsum_2250){
	    this.rollsum_2250 = rollsum_2250;
	}
	public Double getRollsum_2250(){
	    return this.rollsum_2250;
	}
	public void setMachine_rate(String machine_rate){
	    this.machine_rate = machine_rate;
	}
	public String getMachine_rate(){
	    return this.machine_rate;
	}
	public void setRoll_grade_rate(String roll_grade_rate){
	    this.roll_grade_rate = roll_grade_rate;
	}
	public String getRoll_grade_rate(){
	    return this.roll_grade_rate;
	}
	public void setRoll_rw_week(Double roll_rw_week){
	    this.roll_rw_week = roll_rw_week;
	}
	public Double getRoll_rw_week(){
	    return this.roll_rw_week;
	}
	public void setTotal_rollconsum_permonth(Double total_rollconsum_permonth){
	    this.total_rollconsum_permonth = total_rollconsum_permonth;
	}
	public Double getTotal_rollconsum_permonth(){
	    return this.total_rollconsum_permonth;
	}
	public void setGrinder_failtimes_premonth(Double grinder_failtimes_premonth){
	    this.grinder_failtimes_premonth = grinder_failtimes_premonth;
	}
	public Double getGrinder_failtimes_premonth(){
	    return this.grinder_failtimes_premonth;
	}
	public void setTotal_grinding_pre_month(Float total_grinding_pre_month){
	    this.total_grinding_pre_month = total_grinding_pre_month;
	}
	public Float getTotal_grinding_pre_month(){
	    return this.total_grinding_pre_month;
	}
	public void setTotal_onwear_permonth(Float total_onwear_permonth){
	    this.total_onwear_permonth = total_onwear_permonth;
	}
	public Float getTotal_onwear_permonth(){
	    return this.total_onwear_permonth;
	}
	public void setF1f4fw_totalconsum(Double f1f4fw_totalconsum){
	    this.f1f4fw_totalconsum = f1f4fw_totalconsum;
	}
	public Double getF1f4fw_totalconsum(){
	    return this.f1f4fw_totalconsum;
	}
	public void setF5f7fw_totalconsum(Double f5f7fw_totalconsum){
	    this.f5f7fw_totalconsum = f5f7fw_totalconsum;
	}
	public Double getF5f7fw_totalconsum(){
	    return this.f5f7fw_totalconsum;
	}
	public void setFb_totalconsum(Double fb_totalconsum){
	    this.fb_totalconsum = fb_totalconsum;
	}
	public Double getFb_totalconsum(){
	    return this.fb_totalconsum;
	}
	public void setFe_totalconsum(Double fe_totalconsum){
	    this.fe_totalconsum = fe_totalconsum;
	}
	public Double getFe_totalconsum(){
	    return this.fe_totalconsum;
	}
	public void setRw_totalconsum(Double rw_totalconsum){
	    this.rw_totalconsum = rw_totalconsum;
	}
	public Double getRw_totalconsum(){
	    return this.rw_totalconsum;
	}
	public void setRb_totalconsum(Double rb_totalconsum){
	    this.rb_totalconsum = rb_totalconsum;
	}
	public Double getRb_totalconsum(){
	    return this.rb_totalconsum;
	}
	public void setRe_totalconsum(Double re_totalconsum){
	    this.re_totalconsum = re_totalconsum;
	}
	public Double getRe_totalconsum(){
	    return this.re_totalconsum;
	}
	public void setF1f4fw_total_costconsum(Float f1f4fw_total_costconsum){
	    this.f1f4fw_total_costconsum = f1f4fw_total_costconsum;
	}
	public Float getF1f4fw_total_costconsum(){
	    return this.f1f4fw_total_costconsum;
	}
	public void setF5f7fw_total_costconsum(Float f5f7fw_total_costconsum){
	    this.f5f7fw_total_costconsum = f5f7fw_total_costconsum;
	}
	public Float getF5f7fw_total_costconsum(){
	    return this.f5f7fw_total_costconsum;
	}
	public void setFb_total_costconsum(Float fb_total_costconsum){
	    this.fb_total_costconsum = fb_total_costconsum;
	}
	public Float getFb_total_costconsum(){
	    return this.fb_total_costconsum;
	}
	public void setFe_total_costconsum(Float fe_total_costconsum){
	    this.fe_total_costconsum = fe_total_costconsum;
	}
	public Float getFe_total_costconsum(){
	    return this.fe_total_costconsum;
	}
	public void setRw_total_costconsum(Float rw_total_costconsum){
	    this.rw_total_costconsum = rw_total_costconsum;
	}
	public Float getRw_total_costconsum(){
	    return this.rw_total_costconsum;
	}
	public void setRb_total_costconsum(Float rb_total_costconsum){
	    this.rb_total_costconsum = rb_total_costconsum;
	}
	public Float getRb_total_costconsum(){
	    return this.rb_total_costconsum;
	}
	public void setRe_total_costconsum(Float re_total_costconsum){
	    this.re_total_costconsum = re_total_costconsum;
	}
	public Float getRe_total_costconsum(){
	    return this.re_total_costconsum;
	}
	public void setF1f4fw_inservice_amount(Long f1f4fw_inservice_amount){
	    this.f1f4fw_inservice_amount = f1f4fw_inservice_amount;
	}
	public Long getF1f4fw_inservice_amount(){
	    return this.f1f4fw_inservice_amount;
	}
	public void setF5f7fw_inservice_amount(Long f5f7fw_inservice_amount){
	    this.f5f7fw_inservice_amount = f5f7fw_inservice_amount;
	}
	public Long getF5f7fw_inservice_amount(){
	    return this.f5f7fw_inservice_amount;
	}
	public void setFb_inservice_amount(Long fb_inservice_amount){
	    this.fb_inservice_amount = fb_inservice_amount;
	}
	public Long getFb_inservice_amount(){
	    return this.fb_inservice_amount;
	}
	public void setFe_inservice_amount(Long fe_inservice_amount){
	    this.fe_inservice_amount = fe_inservice_amount;
	}
	public Long getFe_inservice_amount(){
	    return this.fe_inservice_amount;
	}
	public void setRw_inservice_amount(Long rw_inservice_amount){
	    this.rw_inservice_amount = rw_inservice_amount;
	}
	public Long getRw_inservice_amount(){
	    return this.rw_inservice_amount;
	}
	public void setRb_inservice_amount(Long rb_inservice_amount){
	    this.rb_inservice_amount = rb_inservice_amount;
	}
	public Long getRb_inservice_amount(){
	    return this.rb_inservice_amount;
	}
	public void setRe_inservice_amount(Long re_inservice_amount){
	    this.re_inservice_amount = re_inservice_amount;
	}
	public Long getRe_inservice_amount(){
	    return this.re_inservice_amount;
	}
	public void setF1f4fw_preton_consum(Float f1f4fw_preton_consum){
	    this.f1f4fw_preton_consum = f1f4fw_preton_consum;
	}
	public Float getF1f4fw_preton_consum(){
	    return this.f1f4fw_preton_consum;
	}
	public void setF5f7fw_preton_consum(Float f5f7fw_preton_consum){
	    this.f5f7fw_preton_consum = f5f7fw_preton_consum;
	}
	public Float getF5f7fw_preton_consum(){
	    return this.f5f7fw_preton_consum;
	}
	public void setFb_preton_consum(Float fb_preton_consum){
	    this.fb_preton_consum = fb_preton_consum;
	}
	public Float getFb_preton_consum(){
	    return this.fb_preton_consum;
	}
	public void setFe_preton_consum(Float fe_preton_consum){
	    this.fe_preton_consum = fe_preton_consum;
	}
	public Float getFe_preton_consum(){
	    return this.fe_preton_consum;
	}
	public void setRw_preton_consum(Float rw_preton_consum){
	    this.rw_preton_consum = rw_preton_consum;
	}
	public Float getRw_preton_consum(){
	    return this.rw_preton_consum;
	}
	public void setRb_preton_consum(Float rb_preton_consum){
	    this.rb_preton_consum = rb_preton_consum;
	}
	public Float getRb_preton_consum(){
	    return this.rb_preton_consum;
	}
	public void setRe_preton_consum(Float re_preton_consum){
	    this.re_preton_consum = re_preton_consum;
	}
	public Float getRe_preton_consum(){
	    return this.re_preton_consum;
	}
	public void setF1f4fw_preton_costconsum(Float f1f4fw_preton_costconsum){
	    this.f1f4fw_preton_costconsum = f1f4fw_preton_costconsum;
	}
	public Float getF1f4fw_preton_costconsum(){
	    return this.f1f4fw_preton_costconsum;
	}
	public void setF5f7fw_preton_costconsum(Float f5f7fw_preton_costconsum){
	    this.f5f7fw_preton_costconsum = f5f7fw_preton_costconsum;
	}
	public Float getF5f7fw_preton_costconsum(){
	    return this.f5f7fw_preton_costconsum;
	}
	public void setFb_preton_costconsum(Float fb_preton_costconsum){
	    this.fb_preton_costconsum = fb_preton_costconsum;
	}
	public Float getFb_preton_costconsum(){
	    return this.fb_preton_costconsum;
	}
	public void setFe_preton_costconsum(Float fe_preton_costconsum){
	    this.fe_preton_costconsum = fe_preton_costconsum;
	}
	public Float getFe_preton_costconsum(){
	    return this.fe_preton_costconsum;
	}
	public void setRw_preton_costconsum(Float rw_preton_costconsum){
	    this.rw_preton_costconsum = rw_preton_costconsum;
	}
	public Float getRw_preton_costconsum(){
	    return this.rw_preton_costconsum;
	}
	public void setRb_preton_costconsum(Float rb_preton_costconsum){
	    this.rb_preton_costconsum = rb_preton_costconsum;
	}
	public Float getRb_preton_costconsum(){
	    return this.rb_preton_costconsum;
	}
	public void setRe_preton_costconsum(Float re_preton_costconsum){
	    this.re_preton_costconsum = re_preton_costconsum;
	}
	public Float getRe_preton_costconsum(){
	    return this.re_preton_costconsum;
	}
	public Double getF1f4fw_tonnage() {
		return f1f4fw_tonnage;
	}
	public void setF1f4fw_tonnage(Double f1f4fw_tonnage) {
		this.f1f4fw_tonnage = f1f4fw_tonnage;
	}
	public Double getF5f7fw_tonnage() {
		return f5f7fw_tonnage;
	}
	public void setF5f7fw_tonnage(Double f5f7fw_tonnage) {
		this.f5f7fw_tonnage = f5f7fw_tonnage;
	}
	public Double getFb_tonnage() {
		return fb_tonnage;
	}
	public void setFb_tonnage(Double fb_tonnage) {
		this.fb_tonnage = fb_tonnage;
	}
	public Double getFe_tonnage() {
		return fe_tonnage;
	}
	public void setFe_tonnage(Double fe_tonnage) {
		this.fe_tonnage = fe_tonnage;
	}
	public Double getRw_tonnage() {
		return rw_tonnage;
	}
	public void setRw_tonnage(Double rw_tonnage) {
		this.rw_tonnage = rw_tonnage;
	}
	public Double getRb_tonnage() {
		return rb_tonnage;
	}
	public void setRb_tonnage(Double rb_tonnage) {
		this.rb_tonnage = rb_tonnage;
	}
	public Double getRe_tonnage() {
		return re_tonnage;
	}
	public void setRe_tonnage(Double re_tonnage) {
		this.re_tonnage = re_tonnage;
	}
	public Double getF1f4fw_gradingsum() {
		return f1f4fw_gradingsum;
	}
	public void setF1f4fw_gradingsum(Double f1f4fw_gradingsum) {
		this.f1f4fw_gradingsum = f1f4fw_gradingsum;
	}
	public Double getF5f7fw_gradingsum() {
		return f5f7fw_gradingsum;
	}
	public void setF5f7fw_gradingsum(Double f5f7fw_gradingsum) {
		this.f5f7fw_gradingsum = f5f7fw_gradingsum;
	}
	public Double getFb_gradingsum() {
		return fb_gradingsum;
	}
	public void setFb_gradingsum(Double fb_gradingsum) {
		this.fb_gradingsum = fb_gradingsum;
	}
	public Double getFe_gradingsum() {
		return fe_gradingsum;
	}
	public void setFe_gradingsum(Double fe_gradingsum) {
		this.fe_gradingsum = fe_gradingsum;
	}
	public Double getRw_gradingsum() {
		return rw_gradingsum;
	}
	public void setRw_gradingsum(Double rw_gradingsum) {
		this.rw_gradingsum = rw_gradingsum;
	}
	public Double getRb_gradingsum() {
		return rb_gradingsum;
	}
	public void setRb_gradingsum(Double rb_gradingsum) {
		this.rb_gradingsum = rb_gradingsum;
	}
	public Double getRe_gradingsum() {
		return re_gradingsum;
	}
	public void setRe_gradingsum(Double re_gradingsum) {
		this.re_gradingsum = re_gradingsum;
	}
	public String getSbz() {
		return sbz;
	}
	public void setSbz(String sbz) {
		this.sbz = sbz;
	}
	public Double getMachine_rate_denominator() {
		return machine_rate_denominator;
	}
	public void setMachine_rate_denominator(Double machine_rate_denominator) {
		this.machine_rate_denominator = machine_rate_denominator;
	}
	public Double getMachine_rate_numerator() {
		return machine_rate_numerator;
	}
	public void setMachine_rate_numerator(Double machine_rate_numerator) {
		this.machine_rate_numerator = machine_rate_numerator;
	}
	public Double getRoll_grade_rate_denominator() {
		return roll_grade_rate_denominator;
	}
	public void setRoll_grade_rate_denominator(Double roll_grade_rate_denominator) {
		this.roll_grade_rate_denominator = roll_grade_rate_denominator;
	}
	public Double getRoll_grade_rate_numerator() {
		return roll_grade_rate_numerator;
	}
	public void setRoll_grade_rate_numerator(Double roll_grade_rate_numerator) {
		this.roll_grade_rate_numerator = roll_grade_rate_numerator;
	}
}