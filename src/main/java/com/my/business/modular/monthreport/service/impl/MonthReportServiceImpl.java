package com.my.business.modular.monthreport.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.monthreport.entity.MonthReport;
import com.my.business.modular.monthreport.entity.JMonthReport;
import com.my.business.modular.monthreport.dao.MonthReportDao;
import com.my.business.modular.monthreport.service.MonthReportService;
import com.my.business.sys.common.entity.ResultData;

/**
* 月报表接口具体实现类
* @author  生成器生成
* @date 2020-11-23 14:12:05
*/
@Service
public class MonthReportServiceImpl implements MonthReportService {
	
	@Autowired
	private MonthReportDao monthReportDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataMonthReport(String data,Long userId,String sname){
		try{
			JMonthReport jmonthReport = JSON.parseObject(data,JMonthReport.class);
    		MonthReport monthReport = jmonthReport.getMonthReport();
            monthReportDao.insertDataMonthReport(monthReport);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataMonthReportOne(Long indocno){
		try {
            monthReportDao.deleteDataMonthReportOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataMonthReportMany(String str_id) {
        try {
        	String sql = "delete month_report where indocno in(" + str_id +")";
            monthReportDao.deleteDataMonthReportMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataMonthReport(String data,Long userId,String sname){
		try {
			JMonthReport jmonthReport = JSON.parseObject(data,JMonthReport.class);
    		MonthReport monthReport = jmonthReport.getMonthReport();
            monthReportDao.updateDataMonthReport(monthReport);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataMonthReportByPage(String data) {
        try {
        	JMonthReport jmonthReport = JSON.parseObject(data, JMonthReport.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jmonthReport.getPageIndex();
        	Integer pageSize = jmonthReport.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jmonthReport.getCondition()){
    			jsonObject  = JSON.parseObject(jmonthReport.getCondition().toString());
    		}
    
    		List<MonthReport> list = monthReportDao.findDataMonthReportByPage((pageIndex-1)*pageSize, pageSize);
    		Integer count = monthReportDao.findDataMonthReportByPageSize();
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataMonthReportByIndocno(String data) {
        try {
        	JMonthReport jmonthReport = JSON.parseObject(data, JMonthReport.class); 
        	Long indocno = jmonthReport.getIndocno();
        	
    		MonthReport monthReport = monthReportDao.findDataMonthReportByIndocno(indocno);
    		return ResultData.ResultDataSuccess(monthReport);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<MonthReport> findDataMonthReport(){
		List<MonthReport> list = monthReportDao.findDataMonthReport();
		return list;
	};
}
