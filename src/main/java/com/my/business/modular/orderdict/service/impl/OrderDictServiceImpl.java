package com.my.business.modular.orderdict.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.dictionary.entity.DicEntity;
import com.my.business.modular.orderdict.dao.OrderDictDao;
import com.my.business.modular.orderdict.entity.JOrderDict;
import com.my.business.modular.orderdict.entity.OrderDict;
import com.my.business.modular.orderdict.service.OrderDictService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.dict.entity.DpdEntity;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 文件上传表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-06-16 09:24:19
 */
@Service
public class OrderDictServiceImpl implements OrderDictService {

    @Autowired
    private OrderDictDao orderDictDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataOrderDict(String data, Long userId, String sname) {
        try {
            JOrderDict jorderDict = JSON.parseObject(data, JOrderDict.class);
            OrderDict orderDict = jorderDict.getOrderDict();
            CodiUtil.newRecord(userId, sname, orderDict);
            orderDictDao.insertDataOrderDict(orderDict);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataOrderDictOne(Long indocno) {
        try {
            orderDictDao.deleteDataOrderDictOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataOrderDictMany(String str_id) {
        try {
            String sql = "delete from order_dict where indocno in(" + str_id + ")";
            orderDictDao.deleteDataOrderDictMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataOrderDict(String data, Long userId, String sname) {
        try {
            JOrderDict jorderDict = JSON.parseObject(data, JOrderDict.class);
            OrderDict orderDict = jorderDict.getOrderDict();
            CodiUtil.editRecord(userId, sname, orderDict);
            orderDictDao.updateDataOrderDict(orderDict);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataOrderDictByPage(String data) {
        try {
            JOrderDict jorderDict = JSON.parseObject(data, JOrderDict.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jorderDict.getPageIndex();
            Integer pageSize = jorderDict.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jorderDict.getCondition()) {
                jsonObject = JSON.parseObject(jorderDict.getCondition().toString());
            }

            List<OrderDict> list = orderDictDao.findDataOrderDictByPage(pageIndex, pageSize);
            Integer count = orderDictDao.findDataOrderDictByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataOrderDictByIndocno(String data) {
        try {
            JOrderDict jorderDict = JSON.parseObject(data, JOrderDict.class);
            Long indocno = jorderDict.getIndocno();

            OrderDict orderDict = orderDictDao.findDataOrderDictByIndocno(indocno);
            return ResultData.ResultDataSuccess(orderDict);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public ResultData findDataOrderDict() {
        try {
            List<OrderDict> list = orderDictDao.findDataOrderDict();
            return ResultData.ResultDataSuccess(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 数据字典通用查询，下拉模式
     *
     * @param data 数据字典编码
     * @return
     */
    public ResultData findWorkDicV(String data) {
        try {
            DicEntity entity = JSON.parseObject(data, DicEntity.class);
            String dicno = entity.getDicno();
            Long level = entity.getLevel();
            String sql = null;
            if (!StringUtils.isEmpty(level)) {
                sql = "select a.order_no as 'key',a.sname as 'value' from order_dic a where a.ilevel = " + level + " and a.order_no like '" + dicno + "%' order by indocno asc";
            } else {
                sql = "select a.order_no as 'key',a.sname as 'value' from order_dic a where a.ilevel = 2 and a.order_no like '" + dicno + "%' order by indocno asc";
            }

            List<DpdEntity> list = orderDictDao.findWorkDicV(sql);
            return ResultData.ResultDataSuccessSelf("获取成功", list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("获取失败" + e.getMessage(), null);
        }
    }
}
