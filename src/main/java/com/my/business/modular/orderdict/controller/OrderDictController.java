package com.my.business.modular.orderdict.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.orderdict.entity.JOrderDict;
import com.my.business.modular.orderdict.service.OrderDictService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 文件上传表控制器层
 *
 * @author 生成器生成
 * @date 2020-06-16 09:24:19
 */
@RestController
@RequestMapping("/orderDict")
public class OrderDictController {

    @Autowired
    private OrderDictService orderDictService;

    /**
     * 添加记录
     *
     * @param data userId 用户id sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataOrderDict(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return orderDictService.insertDataOrderDict(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataOrderDictOne(@RequestBody String data) {
        try {
            JOrderDict jorderDict = JSON.parseObject(data, JOrderDict.class);
            return orderDictService.deleteDataOrderDictOne(jorderDict.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JOrderDict jorderDict = JSON.parseObject(data, JOrderDict.class);
            return orderDictService.deleteDataOrderDictMany(jorderDict.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataOrderDict(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return orderDictService.updateDataOrderDict(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataOrderDictByPage(@RequestBody String data) {
        return orderDictService.findDataOrderDictByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataOrderDictByIndocno(@RequestBody String data) {
        return orderDictService.findDataOrderDictByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public ResultData findDataOrderDict() {
        return orderDictService.findDataOrderDict();
    }

    /**
     * 工单字典通用查询，下拉模式
     *
     * @param data 工单字典编码
     * @return
     */
    @CrossOrigin
    @RequestMapping(value = {"/findWorkDic"}, method = RequestMethod.POST)
    public ResultData findDataV1(@RequestBody String data) {
        return orderDictService.findWorkDicV(data);
    }
}
