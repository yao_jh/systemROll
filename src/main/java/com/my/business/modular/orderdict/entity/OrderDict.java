package com.my.business.modular.orderdict.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 文件上传表实体类
 *
 * @author 生成器生成
 * @date 2020-06-16 09:24:18
 */
public class OrderDict extends BaseEntity {

    private Long indocno;  //主键
    private String sname;  //工单类型名称
    private String order_table;  //工单表
    private String order_no;  //编码
    private Long iparent;  //父节点id
    private String sparent;  //父节点名称
    private Long ilevel;  //等级
    private String order_filed;  //工单字段
    private String order_label;  //工单label
    private String snote;  //说明

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getSname() {
        return this.sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getOrder_table() {
        return this.order_table;
    }

    public void setOrder_table(String order_table) {
        this.order_table = order_table;
    }

    public String getOrder_no() {
        return this.order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public Long getIparent() {
        return this.iparent;
    }

    public void setIparent(Long iparent) {
        this.iparent = iparent;
    }

    public String getSparent() {
        return this.sparent;
    }

    public void setSparent(String sparent) {
        this.sparent = sparent;
    }

    public Long getIlevel() {
        return this.ilevel;
    }

    public void setIlevel(Long ilevel) {
        this.ilevel = ilevel;
    }

    public String getOrder_filed() {
        return this.order_filed;
    }

    public void setOrder_filed(String order_filed) {
        this.order_filed = order_filed;
    }

    public String getOrder_label() {
        return this.order_label;
    }

    public void setOrder_label(String order_label) {
        this.order_label = order_label;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }


}