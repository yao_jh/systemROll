package com.my.business.modular.orderdict.service;

import com.my.business.sys.common.entity.ResultData;

/**
 * 文件上传表接口服务类
 *
 * @author 生成器生成
 * @date 2020-06-16 09:24:18
 */
public interface OrderDictService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataOrderDict(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataOrderDictOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataOrderDictMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataOrderDict(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataOrderDictByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataOrderDictByIndocno(String data);

    /**
     * 查看记录
     */
    ResultData findDataOrderDict();

    /**
     * 工单字典通用查询，下拉模式
     *
     * @param data 工单字典编码
     * @return
     */
    ResultData findWorkDicV(String data);
}
