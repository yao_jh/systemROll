package com.my.business.modular.orderdict.dao;

import com.my.business.modular.orderdict.entity.OrderDict;
import com.my.business.sys.dict.entity.DpdEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 文件上传表dao接口
 *
 * @author 生成器生成
 * @date 2020-06-16 09:24:18
 */
@Mapper
public interface OrderDictDao {

    /**
     * 添加记录
     *
     * @param orderDict 对象实体
     */
    void insertDataOrderDict(OrderDict orderDict);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataOrderDictOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataOrderDictMany(String value);

    /**
     * 修改记录
     *
     * @param orderDict 对象实体
     */
    void updateDataOrderDict(OrderDict orderDict);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<OrderDict> findDataOrderDictByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataOrderDictByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    OrderDict findDataOrderDictByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<OrderDict> findDataOrderDict();

    /***
     * 根据参数获取map类型的数据，一般用于下拉,适用于任何只需要key value的数据
     * @param sql
     * @return
     */
    List<DpdEntity> findWorkDicV(String sql);
}
