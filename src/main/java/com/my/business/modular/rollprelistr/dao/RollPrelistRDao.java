package com.my.business.modular.rollprelistr.dao;

import com.my.business.modular.rollprelistr.entity.RollPrelistR;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 备辊列表——支撑辊类dao接口
 *
 * @author 生成器生成
 * @date 2020-10-10 12:10:30
 */
@Mapper
public interface RollPrelistRDao {

    /**
     * 添加记录
     *
     * @param rollPrelistR 对象实体
     */
    void insertDataRollPrelistR(RollPrelistR rollPrelistR);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollPrelistROne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollPrelistRMany(String value);

    /**
     * 修改记录
     *
     * @param rollPrelistR 对象实体
     */
    void updateDataRollPrelistR(RollPrelistR rollPrelistR);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollPrelistR> findDataRollPrelistRByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("frame_noid") Long frame_noid);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollPrelistRByPageSize(@Param("frame_noid") Long frame_noid);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollPrelistR findDataRollPrelistRByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollPrelistR> findDataRollPrelistR();

    List<RollPrelistR> findDataRollPrelistRByPage2(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("frame_noid") Long frame_noid);

    Integer findDataRollPrelistRByPageSize2(@Param("frame_noid") Long frame_noid);
}
