package com.my.business.modular.rollprelistr.service.impl;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.modular.rollpaired.dao.RollPairedDao;
import com.my.business.modular.rollpaired.entity.RollPaired;
import com.my.business.modular.rollprelistr.dao.RollPrelistRDao;
import com.my.business.modular.rollprelistr.entity.DpdEntityEx2;
import com.my.business.modular.rollprelistr.entity.JRollPrelistR;
import com.my.business.modular.rollprelistr.entity.RollPrelistR;
import com.my.business.modular.rollprelistr.service.RollPrelistRService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 备辊列表——支撑辊类接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-10-10 12:10:30
 */
@Service
public class RollPrelistRServiceImpl implements RollPrelistRService {

    @Autowired
    private RollPrelistRDao rollPrelistRDao;
    @Autowired
    private RollPairedDao rollPairedDao;
    @Autowired
    private RollInformationDao rollInformationDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollPrelistR(String data, Long userId, String sname) {
        try {
            JRollPrelistR jrollPrelistR = JSON.parseObject(data, JRollPrelistR.class);
            RollPrelistR rollPrelistR = jrollPrelistR.getRollPrelistR();
            CodiUtil.newRecord(userId, sname, rollPrelistR);
            rollPrelistRDao.insertDataRollPrelistR(rollPrelistR);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollPrelistROne(Long indocno) {
        try {
            rollPrelistRDao.deleteDataRollPrelistROne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollPrelistRMany(String str_id) {
        try {
            String sql = "delete roll_prelist_r where indocno in(" + str_id + ")";
            rollPrelistRDao.deleteDataRollPrelistRMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollPrelistR(String data, Long userId, String sname) {
        try {
            JRollPrelistR jrollPrelistR = JSON.parseObject(data, JRollPrelistR.class);
            RollPrelistR rollPrelistR = jrollPrelistR.getRollPrelistR();
            CodiUtil.editRecord(userId, sname, rollPrelistR);
            rollPrelistRDao.updateDataRollPrelistR(rollPrelistR);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPrelistRByPage(String data) {
        try {
            List<DpdEntityEx2> listprime = new ArrayList<>();
            Long production_line_id = 1L;
            //F1支撑辊
            //List<RollPaired> list1 = rollPairedDao.findDataRollPairedB(null, production_line_id, 2L, 1L);
            List<RollInformation> list1  = rollPairedDao.findDataRollInformationByMes(2L);
            if (list1.size() != 0 && !StringUtils.isEmpty(list1)) {
                for (int i = 0; i < list1.size(); i++) {
                    DpdEntityEx2 r = new DpdEntityEx2();
                    r.setFrame_noid(1L);
                    r.setFrame_no("F1支撑辊");
                    r.setIndocno(1L);
                    r.setKey("F1支撑辊" + (i + 1));
                    //r.setValue1(findRollNo(list1.get(i).getRoll_no()));
                    //r.setValue2(findRollNo(list1.get(i).getRoll_no()));
                    r.setValue1(list1.get(i).getRoll_no());
                    r.setValue2(list1.get(i).getRoll_no());
                    listprime.add(r);
                }
            }
            //F2支撑辊
            //List<RollPaired> list2 = rollPairedDao.findDataRollPairedB(null, production_line_id, 2L, 2L);
            List<RollInformation> list2  = rollPairedDao.findDataRollInformationByMes(2L);
            if (list2.size() != 0 && !StringUtils.isEmpty(list2)) {
                for (int i = 0; i < list2.size(); i++) {
                    DpdEntityEx2 r2 = new DpdEntityEx2();
                    r2.setFrame_noid(2L);
                    r2.setFrame_no("F2支撑辊");
                    r2.setIndocno(2L);
                    r2.setKey("F2支撑辊" + (i + 1));
                    r2.setValue1(list2.get(i).getRoll_no());
                    r2.setValue2(list2.get(i).getRoll_no());
                    listprime.add(r2);
                }
            }
            //F3支撑辊
           // List<RollPaired> list3 = rollPairedDao.findDataRollPairedB(null, production_line_id, 2L, 3L);
            List<RollInformation> list3  = rollPairedDao.findDataRollInformationByMes(2L);

            if (list3.size() != 0 && !StringUtils.isEmpty(list3)) {
                for (int i = 0; i < list3.size(); i++) {
                    DpdEntityEx2 r3 = new DpdEntityEx2();
                    r3.setFrame_noid(3L);
                    r3.setFrame_no("F3支撑辊");
                    r3.setIndocno(3L);
                    r3.setKey("F3支撑辊" + (i + 1));
                    r3.setValue1(list3.get(i).getRoll_no());
                    r3.setValue2(list3.get(i).getRoll_no());
                    listprime.add(r3);
                }
            }
            //F4支撑辊
            //List<RollPaired> list4 = rollPairedDao.findDataRollPairedB(null, production_line_id, 2L, 4L);
            List<RollInformation> list4  = rollPairedDao.findDataRollInformationByMes(2L);

            if (list4.size() != 0 && !StringUtils.isEmpty(list4)) {
                for (int i = 0; i < list4.size(); i++) {
                    DpdEntityEx2 r4 = new DpdEntityEx2();
                    r4.setFrame_noid(4L);
                    r4.setFrame_no("F4支撑辊");
                    r4.setIndocno(4L);
                    r4.setKey("F4支撑辊" + (i + 1));
                    r4.setValue1(list4.get(i).getRoll_no());
                    r4.setValue2(list4.get(i).getRoll_no());
                    listprime.add(r4);
                }
            }
            //F5支撑辊
            //List<RollPaired> list5 = rollPairedDao.findDataRollPairedB(null, production_line_id, 2L, 5L);
            List<RollInformation> list5  = rollPairedDao.findDataRollInformationByMes(2L);

            if (list5.size() != 0 && !StringUtils.isEmpty(list5)) {
                for (int i = 0; i < list5.size(); i++) {
                    DpdEntityEx2 r5 = new DpdEntityEx2();
                    r5.setFrame_noid(5L);
                    r5.setFrame_no("F5支撑辊");
                    r5.setIndocno(5L);
                    r5.setKey("F5支撑辊" + (i + 1));
                    r5.setValue1(list5.get(i).getRoll_no());
                    r5.setValue2(list5.get(i).getRoll_no());
                    listprime.add(r5);
                }
            }
            //F6支撑辊
            //List<RollPaired> list6 = rollPairedDao.findDataRollPairedB(null, production_line_id, 2L, 6L);
            List<RollInformation> list6  = rollPairedDao.findDataRollInformationByMes(2L);

            if (list6.size() != 0 && !StringUtils.isEmpty(list6)) {
                for (int i = 0; i < list6.size(); i++) {
                    DpdEntityEx2 r6 = new DpdEntityEx2();
                    r6.setFrame_noid(6L);
                    r6.setFrame_no("F6支撑辊");
                    r6.setIndocno(6L);
                    r6.setKey("F6支撑辊" + (i + 1));
                    r6.setValue1(list6.get(i).getRoll_no());
                    r6.setValue2(list6.get(i).getRoll_no());
                    listprime.add(r6);
                }

            }
            //F7支撑辊
            //List<RollPaired> list7 = rollPairedDao.findDataRollPairedB(null, production_line_id, 2L, 7L);
            List<RollInformation> list7  = rollPairedDao.findDataRollInformationByMes(2L);

            if (list7.size() != 0 && !StringUtils.isEmpty(list7)) {
                for (int i = 0; i < list7.size(); i++) {
                    DpdEntityEx2 r7 = new DpdEntityEx2();
                    r7.setFrame_noid(7L);
                    r7.setFrame_no("F7支撑辊");
                    r7.setIndocno(7L);
                    r7.setKey("F7支撑辊" + (i + 1));
                    r7.setValue1(list7.get(i).getRoll_no());
                    r7.setValue2(list7.get(i).getRoll_no());
                    listprime.add(r7);
                }
            }

            //F8支撑辊
            //List<RollPaired> list8 = rollPairedDao.findDataRollPairedB(null, production_line_id, 2L, 15L);
            List<RollInformation> list8  = rollPairedDao.findDataRollInformationByMes(2L);

            if (list8.size() != 0 && !StringUtils.isEmpty(list8)) {
                for (int i = 0; i < list8.size(); i++) {
                    DpdEntityEx2 r8 = new DpdEntityEx2();
                    r8.setFrame_noid(15L);
                    r8.setFrame_no("F8支撑辊");
                    r8.setIndocno(8L);
                    r8.setKey("F8支撑辊" + (i + 1));
                    r8.setValue1(list8.get(i).getRoll_no());
                    r8.setValue2(list8.get(i).getRoll_no());
                    listprime.add(r8);
                }
            }


            //R1工作辊
            //List<RollPaired> list9 = rollPairedDao.findDataRollPairedB(null, production_line_id, 3L, 8L);
            List<RollInformation> list9  = rollPairedDao.findDataRollInformationByMes(3L);

            if (list9.size() != 0 && !StringUtils.isEmpty(list9)) {
                for (int i = 0; i < list9.size(); i++) {
                    DpdEntityEx2 r9 = new DpdEntityEx2();
                    r9.setFrame_noid(8L);
                    r9.setFrame_no("R1工作辊");
                    r9.setIndocno(9L);
                    r9.setKey("R1工作辊" + (i + 1));
                    r9.setValue1(list9.get(i).getRoll_no());
                    r9.setValue2(list9.get(i).getRoll_no());
                    listprime.add(r9);
                }
            }
            //R1支撑辊
            //List<RollPaired> list10 = rollPairedDao.findDataRollPairedB(null, production_line_id, 4L, 8L);
            List<RollInformation> list10  = rollPairedDao.findDataRollInformationByMes(4L);

            if (list10.size() != 0 && !StringUtils.isEmpty(list10)) {
                for (int i = 0; i < list10.size(); i++) {
                    DpdEntityEx2 r10 = new DpdEntityEx2();
                    r10.setFrame_noid(8L);
                    r10.setFrame_no("R1支撑辊");
                    r10.setIndocno(10L);
                    r10.setKey("R1支撑辊" + (i + 1));
                    r10.setValue1(list10.get(i).getRoll_no());
                    r10.setValue2(list10.get(i).getRoll_no());
                    listprime.add(r10);
                }
            }
            //R2支撑辊
           /* List<RollPaired> list11 = rollPairedDao.findDataRollPairedB(null, production_line_id, 4L, 9L);
            if (list11.size() != 0 && !StringUtils.isEmpty(list11)) {
                for (int i = 0; i < list11.size(); i++) {
                    DpdEntityEx2 r11 = new DpdEntityEx2();
                    r11.setFrame_noid(9L);
                    r11.setFrame_no("R2支撑辊");
                    r11.setIndocno(11L);
                    r11.setKey("R2支撑辊" + (i + 1));
                    r11.setValue1(findRollNo(list11.get(i).getRoll_no_up()));
                    r11.setValue2(findRollNo(list11.get(i).getRoll_no_down()));
                    listprime.add(r11);
                }
            }*/
            //FE立辊
           // List<RollPaired> list12 = rollPairedDao.findDataRollPairedB(null, production_line_id, 5L, 14L);
            List<RollInformation> list12  = rollPairedDao.findDataRollInformationByMes(5L);

            if (list12.size() != 0 && !StringUtils.isEmpty(list12)) {
                for (int i = 0; i < list12.size(); i++) {
                    DpdEntityEx2 r12 = new DpdEntityEx2();
                    r12.setFrame_noid(14L);
                    r12.setFrame_no("FE立辊");
                    r12.setIndocno(11L);
                    r12.setKey("FE立辊" + (i + 1));
                    r12.setValue1(list12.get(i).getRoll_no());
                    r12.setValue2(list12.get(i).getRoll_no());
                    listprime.add(r12);
                }
            }
            //E1立辊
            //List<RollPaired> list13 = rollPairedDao.findDataRollPairedB(null, production_line_id, 6L, 12L);
            List<RollInformation> list13  = rollPairedDao.findDataRollInformationByMes(6L);

            if (list13.size() != 0 && !StringUtils.isEmpty(list13)) {
                for (int i = 0; i < list13.size(); i++) {
                    DpdEntityEx2 r13 = new DpdEntityEx2();
                    r13.setFrame_noid(12L);
                    r13.setFrame_no("E1立辊");
                    r13.setIndocno(12L);
                    r13.setKey("E1立辊" + (i + 1));
                    r13.setValue1(list13.get(i).getRoll_no());
                    r13.setValue2(list13.get(i).getRoll_no());
                    listprime.add(r13);
                }
            }
            //R2E立辊
           /* List<RollPaired> list14 = rollPairedDao.findDataRollPairedB(null, production_line_id, 6L, 13L);
            if (list14.size() != 0 && !StringUtils.isEmpty(list14)) {
                for (int i = 0; i < list14.size(); i++) {
                    DpdEntityEx2 r14 = new DpdEntityEx2();
                    r14.setFrame_noid(13L);
                    r14.setFrame_no("R2E立辊");
                    r14.setIndocno(14L);
                    r14.setKey("R2E立辊" + (i + 1));
                    r14.setValue1(findRollNo(list14.get(i).getRoll_no_up()));
                    r14.setValue2(findRollNo(list14.get(i).getRoll_no_down()));
                    listprime.add(r14);
                }
            }*/
            //SSP锤头
/*            List<RollPaired> list15 = rollPairedDao.findDataRollPairedB(null, production_line_id, 10L, 15L);
            if (list15.size() != 0 && !StringUtils.isEmpty(list15)) {
                for (int i = 0; i < list15.size(); i++) {
                    DpdEntityEx2 r15 = new DpdEntityEx2();
                    r15.setFrame_noid(15L);
                    r15.setFrame_no("SSP锤头");
                    r15.setIndocno(15L);
                    r15.setKey("SSP锤头" + (i + 1));
                    r15.setValue1(findRollNo(list15.get(i).getRoll_no_up()));
                    r15.setValue2(findRollNo(list15.get(i).getRoll_no_down()));
                    listprime.add(r15);
                }
            }*/
            return ResultData.ResultDataSuccess(listprime, null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPrelistRByIndocno(String data) {
        try {
            JRollPrelistR jrollPrelistR = JSON.parseObject(data, JRollPrelistR.class);
            Long indocno = jrollPrelistR.getIndocno();

            RollPrelistR rollPrelistR = rollPrelistRDao.findDataRollPrelistRByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollPrelistR);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollPrelistR> findDataRollPrelistR() {
        List<RollPrelistR> list = rollPrelistRDao.findDataRollPrelistR();
        return list;
    }

    private String findRollNo(String roll_no) {
        String roll_nos = null;
        if (!StringUtils.isEmpty(roll_no)) {
            RollInformation Information1 = rollInformationDao.findDataRollInformationByRollNo(roll_no);
            if (!StringUtils.isEmpty(Information1) && !StringUtils.isEmpty(Information1.getRoll_revolve())) {
                if (Information1.getRoll_revolve() == 4L) {
                    roll_nos = roll_no;
                }else {
                    roll_nos = null;
                }
            } else {
                roll_nos = null;
            }
        }
        return roll_nos;
    }

}
