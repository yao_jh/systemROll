package com.my.business.modular.rollprelistr.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 备辊列表——支撑辊类实体类
 *
 * @author 生成器生成
 * @date 2020-10-10 12:10:30
 */
public class RollPrelistR extends BaseEntity {

    private Long indocno;  //主键
    private Long frame_noid;  //机架id
    private String frame_no;  //机架
    private String preroll1;  //备辊1
    private String preroll2;  //备辊2
    private String preroll3;  //备辊3
    private String preroll4;  //备辊4
    private String preroll5;  //备辊5
    private String preroll6;  //备辊6
    private String preroll7;  //备辊7
    private Long mut;

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getFrame_noid() {
        return this.frame_noid;
    }

    public void setFrame_noid(Long frame_noid) {
        this.frame_noid = frame_noid;
    }

    public String getFrame_no() {
        return this.frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public String getPreroll1() {
        return this.preroll1;
    }

    public void setPreroll1(String preroll1) {
        this.preroll1 = preroll1;
    }

    public String getPreroll2() {
        return this.preroll2;
    }

    public void setPreroll2(String preroll2) {
        this.preroll2 = preroll2;
    }

    public String getPreroll3() {
        return this.preroll3;
    }

    public void setPreroll3(String preroll3) {
        this.preroll3 = preroll3;
    }

    public String getPreroll4() {
        return this.preroll4;
    }

    public void setPreroll4(String preroll4) {
        this.preroll4 = preroll4;
    }

    public String getPreroll5() {
        return this.preroll5;
    }

    public void setPreroll5(String preroll5) {
        this.preroll5 = preroll5;
    }

    public String getPreroll6() {
        return preroll6;
    }

    public void setPreroll6(String preroll6) {
        this.preroll6 = preroll6;
    }

    public String getPreroll7() {
        return preroll7;
    }

    public void setPreroll7(String preroll7) {
        this.preroll7 = preroll7;
    }

    public Long getMut() {
        return mut;
    }

    public void setMut(Long mut) {
        this.mut = mut;
    }
}