package com.my.business.modular.rollobjection.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.rollobjection.entity.RollObjection;
import com.my.business.modular.rollobjection.entity.JRollObjection;
import com.my.business.modular.rollobjection.dao.RollObjectionDao;
import com.my.business.modular.rollobjection.service.RollObjectionService;
import com.my.business.sys.common.entity.ResultData;

/**
* 质量异议历史表接口具体实现类
* @author  生成器生成
* @date 2020-11-30 15:46:37
*/
@Service
public class RollObjectionServiceImpl implements RollObjectionService {
	
	@Autowired
	private RollObjectionDao rollObjectionDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollObjection(String data,Long userId,String sname){
		try{
			JRollObjection jrollObjection = JSON.parseObject(data,JRollObjection.class);
    		RollObjection rollObjection = jrollObjection.getRollObjection();
    		CodiUtil.newRecord(userId,sname,rollObjection);
            rollObjectionDao.insertDataRollObjection(rollObjection);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollObjectionOne(Long indocno){
		try {
            rollObjectionDao.deleteDataRollObjectionOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollObjectionMany(String str_id) {
        try {
        	String sql = "delete roll_objection where indocno in(" + str_id +")";
            rollObjectionDao.deleteDataRollObjectionMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataRollObjection(String data,Long userId,String sname){
		try {
			JRollObjection jrollObjection = JSON.parseObject(data,JRollObjection.class);
    		RollObjection rollObjection = jrollObjection.getRollObjection();
        	CodiUtil.editRecord(userId,sname,rollObjection);
            rollObjectionDao.updateDataRollObjection(rollObjection);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollObjectionByPage(String data) {
        try {
        	JRollObjection jrollObjection = JSON.parseObject(data, JRollObjection.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jrollObjection.getPageIndex();
        	Integer pageSize = jrollObjection.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jrollObjection.getCondition()){
    			jsonObject  = JSON.parseObject(jrollObjection.getCondition().toString());
    		}
        	
        	 String roll_no = null;
             if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
            	 roll_no = jsonObject.get("roll_no").toString();
             }
    
    		List<RollObjection> list = rollObjectionDao.findDataRollObjectionByPage((pageIndex-1)*pageSize, pageSize,roll_no);
    		Integer count = rollObjectionDao.findDataRollObjectionByPageSize(roll_no);
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollObjectionByIndocno(String data) {
        try {
        	JRollObjection jrollObjection = JSON.parseObject(data, JRollObjection.class); 
        	Long indocno = jrollObjection.getIndocno();
        	
    		RollObjection rollObjection = rollObjectionDao.findDataRollObjectionByIndocno(indocno);
    		return ResultData.ResultDataSuccess(rollObjection);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<RollObjection> findDataRollObjection(){
		List<RollObjection> list = rollObjectionDao.findDataRollObjection();
		return list;
	};
}
