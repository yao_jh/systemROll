package com.my.business.modular.rollobjection.entity;

import java.util.Date;
import com.my.business.sys.common.entity.BaseEntity;

/**
* 质量异议历史表实体类
* @author  生成器生成
* @date 2020-11-30 15:46:36
*/
public class RollObjection extends BaseEntity{
		
	private Long indocno;  //主键
	private String roll_no;  //辊号
	private String quality;  //质量异议
	private Long quality_id;  //质量异议id
	private String factory;  //供货商
	private Long factory_id;  //供货商id
	private String contract_no;  //合同号
	private String material;  //材质
	private Long material_id;  //材质id
	private String qualityreason;  //质量异议原因
    
    
	public void setIndocno(Long indocno){
	    this.indocno = indocno;
	}
	public Long getIndocno(){
	    return this.indocno;
	}
	public void setRoll_no(String roll_no){
	    this.roll_no = roll_no;
	}
	public String getRoll_no(){
	    return this.roll_no;
	}
	public void setQuality(String quality){
	    this.quality = quality;
	}
	public String getQuality(){
	    return this.quality;
	}
	public void setQuality_id(Long quality_id){
	    this.quality_id = quality_id;
	}
	public Long getQuality_id(){
	    return this.quality_id;
	}
	public void setFactory(String factory){
	    this.factory = factory;
	}
	public String getFactory(){
	    return this.factory;
	}
	public void setFactory_id(Long factory_id){
	    this.factory_id = factory_id;
	}
	public Long getFactory_id(){
	    return this.factory_id;
	}
	public void setContract_no(String contract_no){
	    this.contract_no = contract_no;
	}
	public String getContract_no(){
	    return this.contract_no;
	}
	public void setMaterial(String material){
	    this.material = material;
	}
	public String getMaterial(){
	    return this.material;
	}
	public void setMaterial_id(Long material_id){
	    this.material_id = material_id;
	}
	public Long getMaterial_id(){
	    return this.material_id;
	}
	public void setQualityreason(String qualityreason){
	    this.qualityreason = qualityreason;
	}
	public String getQualityreason(){
	    return this.qualityreason;
	}

    
}