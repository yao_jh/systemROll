package com.my.business.modular.rollobjection.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.modular.rollobjection.entity.RollObjection;
import java.util.List;

/**
* 质量异议历史表接口服务类
* @author  生成器生成
* @date 2020-11-30 15:46:37
*/
public interface RollObjectionService {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollObjection(String data,Long userId,String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollObjectionOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollObjectionMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataRollObjection(String data,Long userId,String sname);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollObjectionByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataRollObjectionByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<RollObjection> findDataRollObjection();
}
