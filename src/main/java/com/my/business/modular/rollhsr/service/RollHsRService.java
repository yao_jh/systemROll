package com.my.business.modular.rollhsr.service;

import com.my.business.modular.rollhsr.entity.RollHsR;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 辊票查询——非精轧工作辊接口服务类
 *
 * @author 生成器生成
 * @date 2020-12-05 16:00:20
 */
public interface RollHsRService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollHsR(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollHsROne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollHsRMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollHsR(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollHsRByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollHsRByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollHsR> findDataRollHsR();

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollHsRByPage2(String data);

    /**
     * 更新状态
     * @param data
     * @param l
     * @return
     */
    ResultData changestate(String data, long l);
}
