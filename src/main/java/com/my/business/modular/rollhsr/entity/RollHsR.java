package com.my.business.modular.rollhsr.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.math.BigDecimal;

/**
 * 辊票查询——非精轧工作辊实体类
 *
 * @author 生成器生成
 * @date 2020-12-05 16:00:20
 */
public class RollHsR extends BaseEntity {

    private Long indocno;  //主键
    private String roll_no;  //辊号
    private String roll_type;  //轧辊类型
    private String frame_no;  //机架号
    private String material;  //材质
    private String roll_position;  //轧辊位置
    private Double currentdiameter;  //当前辊径
    private String rollshape;  //辊形
    private String obearing_chock;  //OS座号
    private String dbearing_chock;  //DS座号
    private String oxide_evaluation;  //表面评级
    private Long uplinecount;  //HSS上机次数
    private Double ini_crown_max;  //最大凸度
    private Double ini_crown_min;  //最小凸度
    private String group1;  //班次
    private String group2;  //班组
    private String time_1;  //备辊时间
    private String dbegin;  //备辊时间从
    private String dend;  //备辊时间至
    private String snote;

    private Double roll_diameter;  //辊径差

    public Double getRoll_diameter() {
        return roll_diameter;
    }

    public void setRoll_diameter(Double roll_diameter) {
        this.roll_diameter = roll_diameter;
    }

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public String getFrame_no() {
        return this.frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public String getMaterial() {
        return this.material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getRoll_position() {
        return this.roll_position;
    }

    public void setRoll_position(String roll_position) {
        this.roll_position = roll_position;
    }

    public Double getCurrentdiameter() {
        return this.currentdiameter;
    }

    public void setCurrentdiameter(Double currentdiameter) {
        this.currentdiameter = currentdiameter;
    }

    public String getRollshape() {
        return this.rollshape;
    }

    public void setRollshape(String rollshape) {
        this.rollshape = rollshape;
    }

    public String getObearing_chock() {
        return this.obearing_chock;
    }

    public void setObearing_chock(String obearing_chock) {
        this.obearing_chock = obearing_chock;
    }

    public String getDbearing_chock() {
        return this.dbearing_chock;
    }

    public void setDbearing_chock(String dbearing_chock) {
        this.dbearing_chock = dbearing_chock;
    }

    public String getOxide_evaluation() {
        return this.oxide_evaluation;
    }

    public void setOxide_evaluation(String oxide_evaluation) {
        this.oxide_evaluation = oxide_evaluation;
    }

    public Long getUplinecount() {
        return this.uplinecount;
    }

    public void setUplinecount(Long uplinecount) {
        this.uplinecount = uplinecount;
    }

    public Double getIni_crown_max() {
        return this.ini_crown_max;
    }

    public void setIni_crown_max(Double ini_crown_max) {
        this.ini_crown_max = ini_crown_max;
    }

    public Double getIni_crown_min() {
        return this.ini_crown_min;
    }

    public void setIni_crown_min(Double ini_crown_min) {
        this.ini_crown_min = ini_crown_min;
    }

    public String getGroup1() {
        return this.group1;
    }

    public void setGroup1(String group1) {
        this.group1 = group1;
    }

    public String getGroup2() {
        return this.group2;
    }

    public void setGroup2(String group2) {
        this.group2 = group2;
    }

    public String getTime_1() {
        return this.time_1;
    }

    public void setTime_1(String time_1) {
        this.time_1 = time_1;
    }

    public String getDbegin() {
        return dbegin;
    }

    public void setDbegin(String dbegin) {
        this.dbegin = dbegin;
    }

    public String getDend() {
        return dend;
    }

    public void setDend(String dend) {
        this.dend = dend;
    }

    public String getSnote() {
        return snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }
}