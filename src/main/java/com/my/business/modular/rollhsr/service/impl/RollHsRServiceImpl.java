package com.my.business.modular.rollhsr.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollhs.entity.JRollHs;
import com.my.business.modular.rollhs.entity.RollHs;
import com.my.business.modular.rollhsr.dao.RollHsRDao;
import com.my.business.modular.rollhsr.entity.JRollHsR;
import com.my.business.modular.rollhsr.entity.RollHsR;
import com.my.business.modular.rollhsr.service.RollHsRService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 辊票查询——非精轧工作辊接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-12-05 16:00:20
 */
@Service
public class RollHsRServiceImpl implements RollHsRService {

    @Autowired
    private RollHsRDao rollHsRDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollHsR(String data, Long userId, String sname) {
        try {
            JRollHsR jrollHsR = JSON.parseObject(data, JRollHsR.class);
            RollHsR rollHsR = jrollHsR.getRollHsR();
            CodiUtil.newRecord(userId, sname, rollHsR);
            rollHsRDao.insertDataRollHsR(rollHsR);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollHsROne(Long indocno) {
        try {
            rollHsRDao.deleteDataRollHsROne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollHsRMany(String str_id) {
        try {
            String sql = "delete roll_hs_r where indocno in(" + str_id + ")";
            rollHsRDao.deleteDataRollHsRMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollHsR(String data, Long userId, String sname) {
        try {
            JRollHsR jrollHsR = JSON.parseObject(data, JRollHsR.class);
            RollHsR rollHsR = jrollHsR.getRollHsR();
            CodiUtil.editRecord(userId, sname, rollHsR);
            rollHsRDao.updateDataRollHsR(rollHsR);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollHsRByPage(String data) {
        try {
            JRollHsR jrollHsR = JSON.parseObject(data, JRollHsR.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollHsR.getPageIndex();
            Integer pageSize = jrollHsR.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollHsR.getCondition()) {
                jsonObject = JSON.parseObject(jrollHsR.getCondition().toString());
            }

            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }
            List<RollHsR> list = rollHsRDao.findDataRollHsRByPage((pageIndex - 1) * pageSize, pageSize,dbegin,dend);
            Integer count = rollHsRDao.findDataRollHsRByPageSize(dbegin,dend);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollHsRByPage2(String data) {
        try {
            JRollHsR jrollHsR = JSON.parseObject(data, JRollHsR.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollHsR.getPageIndex();
            Integer pageSize = jrollHsR.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollHsR.getCondition()) {
                jsonObject = JSON.parseObject(jrollHsR.getCondition().toString());
            }

            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            Long istate = null;
            if (!StringUtils.isEmpty(jsonObject.get("istate"))) {
                istate = Long.valueOf(jsonObject.get("istate").toString());
            }

            List<RollHsR> list2 = rollHsRDao.findDataRollHsRByColl((pageIndex - 1) * pageSize, pageSize,dbegin,dend,istate);
            Integer count = rollHsRDao.findDataRollHsRByCollPage((pageIndex - 1) * pageSize,pageSize,dbegin,dend,istate);
            return ResultData.ResultDataSuccess(list2, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollHsRByIndocno(String data) {
        try {
            JRollHsR jrollHsR = JSON.parseObject(data, JRollHsR.class);
            Long indocno = jrollHsR.getIndocno();

            RollHsR rollHsR = rollHsRDao.findDataRollHsRByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollHsR);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollHsR> findDataRollHsR() {
        List<RollHsR> list = rollHsRDao.findDataRollHsR();
        return list;
    }

    @Override
    public ResultData changestate(String data, long l) {
        try {
            JRollHsR jrollHsR = JSON.parseObject(data, JRollHsR.class);
            RollHsR rollHsR = jrollHsR.getRollHsR();
            if(l == 1L){
                rollHsRDao.updateState(rollHsR.getTime_1(),null,1L);
            }else {
                rollHsRDao.updateState(rollHsR.getTime_1(),rollHsR.getSnote(),2L);
            }
            return ResultData.ResultDataSuccessSelf("状态变更成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("状态变更失败" + e.getMessage(), null);
        }
    }
}
