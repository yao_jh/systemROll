package com.my.business.modular.rollgrinding.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollgrinding.dao.RollGrindingDao;
import com.my.business.modular.rollgrinding.entity.JRollGrinding;
import com.my.business.modular.rollgrinding.entity.RollGrinding;
import com.my.business.modular.rollgrinding.service.RollGrindingService;
import com.my.business.modular.rollgrindingannex.service.RollGrindingAnnexService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.sysbz.dao.SysBzDao;
import com.my.business.sys.sysbz.entity.SysBz;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 磨削实绩接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-24 17:28:05
 */
@Service
public class RollGrindingServiceImpl implements RollGrindingService {

/*    @Autowired
    private RollGrindingDao rollGrindingDao;
    @Autowired
    private RollGrindingAnnexService rollGrindingAnnexService;
    @Autowired
    private SysBzDao sysBzDao;*/
    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
/*    public ResultData insertDataRollGrinding(String data, Long userId, String sname) {
        try {
            JRollGrinding jrollGrinding = JSON.parseObject(data, JRollGrinding.class);
            RollGrinding rollGrinding = jrollGrinding.getRollGrinding();
            Map<String,String> map = findbz(rollGrinding);
            rollGrinding.setSgroup(map.get("bz"));
            rollGrinding.setSclass(map.get("bc"));
            CodiUtil.newRecord(userId, sname, rollGrinding);
            rollGrindingDao.insertDataRollGrinding(rollGrinding);
//            rollGrindingAnnexService.copyRollGrinding(rollGrinding);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }*/

    /**
     * 计算班组
     * 1.取磨削开始时间减去2020-01-01，再除以8，取余数，加1可得班组排序主键
     * 2.根据磨削开始时间后半截时间，形成班次排序
     * 3.综合可得班组和班次
     */
/*    public Map<String, String> findbz(RollGrinding rollGrinding) throws ParseException {
        String st1 = "2020-01-01 00:00:00";
        String st2 = rollGrinding.getGrind_starttime();

        //计算班组
        String from1 = st1.substring(0,10);
        String to1 = st2.substring(0,10);
        SimpleDateFormat simpleFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = simpleFormat1.parse(from1);
        Date date2 = simpleFormat1.parse(to1);
        int a = (int) ((date2.getTime() - date1.getTime()) / (1000*3600*24));
        int yu = (a + 1) % 8; //取余数
        if (yu == 0){
            yu = 8;
        }
        SysBz sysBz = sysBzDao.findDataSysBzByIndocno((long) yu);
        System.out.println("a = "+a+" ,yu = " + yu + " , 辊号：" + rollGrinding.getRoll_no());
        //计算班次及对应班组
        String bcs = st2.substring(11,13);
        String bc = null;
        int i = 0;
        if (Long.parseLong("08") <= Long.parseLong(bcs) && Long.parseLong("16") > Long.parseLong(bcs)){
            i = 1;
        }else if (Long.parseLong("16") <= Long.parseLong(bcs) && Long.parseLong("23") >= Long.parseLong(bcs)){
            i = 2;
        }else if (Long.parseLong("00") <= Long.parseLong(bcs) && Long.parseLong("08") > Long.parseLong(bcs)){
            i = 3;
        }
        String bz = null;
        if (i == 1){
            bz = sysBz.getFirstbz();
            bc = "白班";
        }else if (i == 2){
            bz = sysBz.getSecondbz();
            bc = "小夜班";
        }else if (i == 3){
            bz = sysBz.getThirdbz();
            bc = "大夜班";
        }else {
            bz = null;
            bc = null;
        }
        Map<String,String> map = new HashMap<>();
        map.put("bz",bz);
        map.put("bc",bc);
        return map;
    }*/

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
/*    public ResultData deleteDataRollGrindingOne(Long indocno) {
        try {
            rollGrindingDao.deleteDataRollGrindingOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }*/

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
/*    public ResultData deleteDataRollGrindingMany(String str_id) {
        try {
            String sql = "delete roll_grinding where indocno in(" + str_id + ")";
            rollGrindingDao.deleteDataRollGrindingMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }*/

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
/*    public ResultData updateDataRollGrinding(String data, Long userId, String sname) {
        try {
            JRollGrinding jrollGrinding = JSON.parseObject(data, JRollGrinding.class);
            RollGrinding rollGrinding = jrollGrinding.getRollGrinding();
            Map<String,String> map = findbz(rollGrinding);
            rollGrinding.setSgroup(map.get("bz"));
            rollGrinding.setSclass(map.get("bc"));
            CodiUtil.editRecord(userId, sname, rollGrinding);
            rollGrindingDao.updateDataRollGrinding(rollGrinding);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }*/

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
/*    public ResultData findDataRollGrindingByPage(String data) {
        try {
            JRollGrinding jrollGrinding = JSON.parseObject(data, JRollGrinding.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollGrinding.getPageIndex();
            Integer pageSize = jrollGrinding.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollGrinding.getCondition()) {
                jsonObject = JSON.parseObject(jrollGrinding.getCondition().toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

            Long roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = Long.valueOf(jsonObject.get("roll_typeid").toString());
            }

            String machine_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("machine_no"))) {
                machine_no = jsonObject.get("machine_no").toString();
            }

            String grind_starttime = null;
            if (!StringUtils.isEmpty(jsonObject.get("grind_starttime"))) {
                grind_starttime = jsonObject.get("grind_starttime").toString();
            }

            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            Long production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
            }

            List<RollGrinding> list = rollGrindingDao.findDataRollGrindingByPage((pageIndex - 1)*pageSize, pageSize, roll_no, roll_typeid, dbegin, dend, machine_no, grind_starttime,production_line_id);
            Integer count = rollGrindingDao.findDataRollGrindingByPageSize(roll_no, roll_typeid, dbegin, dend, machine_no, grind_starttime,production_line_id);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }*/

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
/*    public ResultData findDataRollGrindingByIndocno(String data) {
        try {
            JRollGrinding jrollGrinding = JSON.parseObject(data, JRollGrinding.class);
            Long indocno = jrollGrinding.getIndocno();

            RollGrinding rollGrinding = rollGrindingDao.findDataRollGrindingByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollGrinding);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }*/

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
/*    public ResultData findDataRollGrindingByNo(String data) {
        try {
            JRollGrinding jrollGrinding = JSON.parseObject(data, JRollGrinding.class);
            String roll_no = jrollGrinding.getRollGrinding().getRoll_no();
            Integer pageIndex = jrollGrinding.getPageIndex();
            Integer pageSize = jrollGrinding.getPageSize();

            List<RollGrinding> rollGrinding = rollGrindingDao.findDataRollGrindingByNo((pageIndex - 1) * pageSize, pageSize, roll_no);
            Integer count = rollGrindingDao.findDataRollGrindingByNoSize(roll_no);
            return ResultData.ResultDataSuccess(rollGrinding, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }*/

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    /*public List<RollGrinding> findDataRollGrinding() {
        List<RollGrinding> list = rollGrindingDao.findDataRollGrinding();
        return list;
    }*/
}
