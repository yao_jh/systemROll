package com.my.business.modular.rollgrinding.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rolldatafiles.service.RollDataFilesService;
import com.my.business.modular.rollgrinding.dao.RollGrindingDao;
import com.my.business.modular.rollgrinding.entity.JRollGrinding;
import com.my.business.modular.rollgrinding.entity.RollGrinding;
import com.my.business.modular.rollgrinding.service.RollGrindingService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * 磨削实绩控制器层
 *
 * @author 生成器生成
 * @date 2020-08-24 17:28:05
 */
@RestController
@RequestMapping("/rollGrinding")
public class RollGrindingController {

    //@Autowired
    //private RollGrindingService rollGrindingService;
    //@Autowired
    //private RollDataFilesService rollDataFilesService;
    //@Autowired
    //private RollGrindingDao rollGrindingDao;
    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    /*@CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollGrinding(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollGrindingService.insertDataRollGrinding(data, userId, CodiUtil.returnLm(sname));
    }*/

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    /*@CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollGrindingOne(@RequestBody String data) {
        try {
            JRollGrinding jrollGrinding = JSON.parseObject(data, JRollGrinding.class);
            return rollGrindingService.deleteDataRollGrindingOne(jrollGrinding.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }*/

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    /*@CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollGrinding jrollGrinding = JSON.parseObject(data, JRollGrinding.class);
            return rollGrindingService.deleteDataRollGrindingMany(jrollGrinding.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }*/

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    /*@CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollGrinding(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollGrindingService.updateDataRollGrinding(data, userId, CodiUtil.returnLm(sname));
    }*/

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    /*@CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollGrindingByPage(@RequestBody String data) {
        return rollGrindingService.findDataRollGrindingByPage(data);
    }*/

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    /*@CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollGrindingByIndocno(@RequestBody String data) {
        return rollGrindingService.findDataRollGrindingByIndocno(data);
    }*/

    /**
     * 根据辊号查询记录
     *
     * @param data json字符串
     */
    /*@CrossOrigin
    @RequestMapping(value = {"/findByNo"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollGrindingByNo(@RequestBody String data) {
        return rollGrindingService.findDataRollGrindingByNo(data);
    }*/

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
   /* @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollGrinding> findDataRollGrinding() {
        return rollGrindingService.findDataRollGrinding();
    }
*/
    /**
     *
     */
    /*@CrossOrigin
    @RequestMapping(value = {"/test"}, method = RequestMethod.POST)
    @ResponseBody
    public void test() {
        List<RollGrinding> rollGrinding = rollGrindingService.findDataRollGrinding();
        for (RollGrinding entity: rollGrinding){
            Map<String, String> map = rollDataFilesService.AnalysisDifference(entity.getRoll_no(),entity.getGrind_starttime());
            if (!StringUtils.isEmpty(map) && !StringUtils.isEmpty(map.get("合格点数"))) {
                Double count = Double.valueOf(map.get("合格点数"));
                entity.setQualifiednum(count);
                rollGrindingDao.updateDataRollGrinding(entity);
            }
        }

    }*/
}
