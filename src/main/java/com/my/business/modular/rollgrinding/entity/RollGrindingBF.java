package com.my.business.modular.rollgrinding.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 磨削实绩实体类
 *
 * @author 生成器生成
 * @date 2020-08-24 17:28:04
 */
public class RollGrindingBF extends BaseEntity {

    private BigDecimal grinding_time;//磨削时长

    private BigDecimal online_wear ; //在机磨损

    private Long indocno;  //主键
    private String roll_no;  //辊号
    private String curvetype;//曲线类型
    private String programname;//程序名
    private String grind_starttime;  //磨削开始时间
    private String grind_endtime;  //磨削结束时间
    private String machineno;  //磨床号
    private String standno;//磨床名
    private Double before_diameter;  //磨前中部直径
    private Double after_diameter;  //磨后直径
    private Double curvetolerance;//曲线误差
    private Double coaxality;//同心度
    private Double roundness;  //圆度
    //private Double crack;  //裂纹
    //private Double hidden_flaws;  //暗伤
    private Double qualifiednum;  //合格点数
    private String sclass;  //班
    private String sgroup;  //班组
    private String operator;  //操作人姓名
    private String snote;  //备注

    private Date createtime;//创建时间
    private String wheelno;//砂轮编号
    private Double wheel_dia_start; //砂轮开始直径
    private Double wheel_dia_end; //砂轮结束直径
    private String dbegin;
    private String dend;

    private BigDecimal crackvalue;//裂纹值最大值
    private BigDecimal crackmax_x;//裂纹最大值X坐标
    private BigDecimal crackmax_y;//裂纹最大值Y坐标
    private BigDecimal sopotvalue;//软点值最大值
    private BigDecimal sopotmax_x;//软点最大值X坐标
    private BigDecimal sopotmax_y;//软点最大值Y坐标

    private String ettime;//探伤时间

    public String getEttime() {
        return ettime;
    }

    public void setEttime(String ettime) {
        this.ettime = ettime;
    }

    public BigDecimal getCrackvalue() {
        return crackvalue;
    }

    public void setCrackvalue(BigDecimal crackvalue) {
        this.crackvalue = crackvalue;
    }

    public BigDecimal getCrackmax_x() {
        return crackmax_x;
    }

    public void setCrackmax_x(BigDecimal crackmax_x) {
        this.crackmax_x = crackmax_x;
    }

    public BigDecimal getCrackmax_y() {
        return crackmax_y;
    }

    public void setCrackmax_y(BigDecimal crackmax_y) {
        this.crackmax_y = crackmax_y;
    }

    public BigDecimal getSopotvalue() {
        return sopotvalue;
    }

    public void setSopotvalue(BigDecimal sopotvalue) {
        this.sopotvalue = sopotvalue;
    }

    public BigDecimal getSopotmax_x() {
        return sopotmax_x;
    }

    public void setSopotmax_x(BigDecimal sopotmax_x) {
        this.sopotmax_x = sopotmax_x;
    }

    public BigDecimal getSopotmax_y() {
        return sopotmax_y;
    }

    public void setSopotmax_y(BigDecimal sopotmax_y) {
        this.sopotmax_y = sopotmax_y;
    }

    public BigDecimal getOnline_wear() {
        return online_wear;
    }

    public void setOnline_wear(BigDecimal online_wear) {
        this.online_wear = online_wear;
    }

    public BigDecimal getGrinding_time() {
        return grinding_time;
    }

    public void setGrinding_time(BigDecimal grinding_time) {
        this.grinding_time = grinding_time;
    }

    public String getDbegin() {
        return dbegin;
    }

    public void setDbegin(String dbegin) {
        this.dbegin = dbegin;
    }

    public String getDend() {
        return dend;
    }

    public void setDend(String dend) {
        this.dend = dend;
    }

    public Double getTaper() {
        return taper;
    }

    public void setTaper(Double taper) {
        this.taper = taper;
    }

    private Double taper;//锥度

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getCurvetype() {
        return curvetype;
    }

    public void setCurvetype(String curvetype) {
        this.curvetype = curvetype;
    }

    public String getProgramname() {
        return programname;
    }

    public void setProgramname(String programname) {
        this.programname = programname;
    }

    public String getGrind_starttime() {
        return grind_starttime;
    }

    public void setGrind_starttime(String grind_starttime) {
        this.grind_starttime = grind_starttime;
    }

    public String getGrind_endtime() {
        return grind_endtime;
    }

    public void setGrind_endtime(String grind_endtime) {
        this.grind_endtime = grind_endtime;
    }

    public String getMachineno() {
        return machineno;
    }

    public void setMachineno(String machineno) {
        this.machineno = machineno;
    }

    public String getStandno() {
        return standno;
    }

    public void setStandno(String standno) {
        this.standno = standno;
    }

    public Double getBefore_diameter() {
        return before_diameter;
    }

    public void setBefore_diameter(Double before_diameter) {
        this.before_diameter = before_diameter;
    }

    public Double getAfter_diameter() {
        return after_diameter;
    }

    public void setAfter_diameter(Double after_diameter) {
        this.after_diameter = after_diameter;
    }

    public Double getCurvetolerance() {
        return curvetolerance;
    }

    public void setCurvetolerance(Double curvetolerance) {
        this.curvetolerance = curvetolerance;
    }

    public Double getCoaxality() {
        return coaxality;
    }

    public void setCoaxality(Double coaxality) {
        this.coaxality = coaxality;
    }

    public Double getRoundness() {
        return roundness;
    }

    public void setRoundness(Double roundness) {
        this.roundness = roundness;
    }

    public String getSclass() {
        return sclass;
    }

    public void setSclass(String sclass) {
        this.sclass = sclass;
    }

    public String getSgroup() {
        return sgroup;
    }

    public void setSgroup(String sgroup) {
        this.sgroup = sgroup;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getSnote() {
        return snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }


    public String getWheelno() {
        return wheelno;
    }

    public void setWheelno(String wheelno) {
        this.wheelno = wheelno;
    }

    public Double getWheel_dia_start() {
        return wheel_dia_start;
    }

    public void setWheel_dia_start(Double wheel_dia_start) {
        this.wheel_dia_start = wheel_dia_start;
    }

    public Double getWheel_dia_end() {
        return wheel_dia_end;
    }

    public void setWheel_dia_end(Double wheel_dia_end) {
        this.wheel_dia_end = wheel_dia_end;
    }


    public Double getQualifiednum() {
        return qualifiednum;
    }

    public void setQualifiednum(Double qualifiednum) {
        this.qualifiednum = qualifiednum;
    }
    @Override
    public Date getCreatetime() {
        return createtime;
    }
    @Override
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
}