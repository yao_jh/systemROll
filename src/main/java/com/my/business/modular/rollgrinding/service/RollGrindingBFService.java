package com.my.business.modular.rollgrinding.service;

import com.my.business.modular.rollgrinding.entity.RollGrinding;
import com.my.business.modular.rollgrinding.entity.RollGrindingBF;
import com.my.business.sys.common.entity.ResultData;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.List;

/**
 * 磨削实绩接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-24 17:28:05
 */
public interface RollGrindingBFService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollGrindingBF(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollGrindingBFOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollGrindingBFMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollGrindingBF(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollGrindingBFByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollGrindingBFByIndocno(String data);

    /**
     * 根据辊号查看数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollGrindingBFByNo(String data);

    /**
     * 查看记录
     */
    List<RollGrindingBF> findDataRollGrindingBF();

    ResultData findWheelAbrasionByPage(String data);

    String excelWheelAbrasion(HSSFWorkbook workbook,
                              String machine_no
            ,String wheelname
            ,String dbegin
            ,String dend
    ,String factory_name);

    ResultData findGrindingRateByPage(String data);

    String excelGrindingRate(HSSFWorkbook workbook,
                             String  machine_no
            ,String operator
            ,String dbegin
            ,String dend
    );

    ResultData findGrindingBFForTotalnum(String data);

    String excelGrindingBFForTotalnum(HSSFWorkbook workbook, String machine_no,String dbegin,String dend);

    ResultData findGrindingRateABigScreenByPage(String data);
}
