package com.my.business.modular.rollgrinding.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollgrinding.dao.RollGrindingBFDao;
import com.my.business.modular.rollgrinding.dao.RollGrindingDao;
import com.my.business.modular.rollgrinding.entity.*;
import com.my.business.modular.rollgrinding.service.RollGrindingBFService;
import com.my.business.modular.rollgrinding.service.RollGrindingService;
import com.my.business.modular.rollgrindingannex.service.RollGrindingAnnexService;
import com.my.business.modular.rollstiffness.entity.VStiffness;
import com.my.business.sys.common.entity.MapXYEntity;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.sysbz.dao.SysBzDao;
import com.my.business.sys.sysbz.entity.SysBz;
import com.my.business.util.CodiUtil;
import com.my.business.util.DateUtil;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 磨削实绩接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-24 17:28:05
 */
@Service
public class RollGrindingBFServiceImpl implements RollGrindingBFService {

    @Autowired
    private RollGrindingBFDao rollGrindingBFDao;
    @Autowired
    private RollGrindingAnnexService rollGrindingAnnexService;
    @Autowired
    private SysBzDao sysBzDao;
    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollGrindingBF(String data, Long userId, String sname) {
        try {
            JRollGrindingBF jrollGrindingBF = JSON.parseObject(data, JRollGrindingBF.class);
            RollGrindingBF rollGrindingBF = jrollGrindingBF.getRollGrindingBF();
            Map<String,String> map = findbz(rollGrindingBF);
            rollGrindingBF.setSgroup(map.get("bz"));
            rollGrindingBF.setSclass(map.get("bc"));
            CodiUtil.newRecord(userId, sname, rollGrindingBF);
            rollGrindingBFDao.insertDataRollGrindingBF(rollGrindingBF);
//            rollGrindingAnnexService.copyRollGrinding(rollGrinding);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 计算班组
     * 1.取磨削开始时间减去2020-01-01，再除以8，取余数，加1可得班组排序主键
     * 2.根据磨削开始时间后半截时间，形成班次排序
     * 3.综合可得班组和班次
     */
    public Map<String, String> findbz(RollGrindingBF rollGrindingBF) throws ParseException {
        String st1 = "2020-01-01 00:00:00";
        String st2 = rollGrindingBF.getGrind_starttime();

        //计算班组
        String from1 = st1.substring(0,10);
        String to1 = st2.substring(0,10);
        SimpleDateFormat simpleFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = simpleFormat1.parse(from1);
        Date date2 = simpleFormat1.parse(to1);
        int a = (int) ((date2.getTime() - date1.getTime()) / (1000*3600*24));
        int yu = (a + 1) % 8; //取余数
        if (yu == 0){
            yu = 8;
        }
        SysBz sysBz = sysBzDao.findDataSysBzByIndocno((long) yu);
        System.out.println("a = "+a+" ,yu = " + yu + " , 辊号：" + rollGrindingBF.getRoll_no());
        //计算班次及对应班组
        String bcs = st2.substring(11,13);
        String bc = null;
        int i = 0;
        if (Long.parseLong("08") <= Long.parseLong(bcs) && Long.parseLong("16") > Long.parseLong(bcs)){
            i = 1;
        }else if (Long.parseLong("16") <= Long.parseLong(bcs) && Long.parseLong("23") >= Long.parseLong(bcs)){
            i = 2;
        }else if (Long.parseLong("00") <= Long.parseLong(bcs) && Long.parseLong("08") > Long.parseLong(bcs)){
            i = 3;
        }
        String bz = null;
        if (i == 1){
            bz = sysBz.getFirstbz();
            bc = "白班";
        }else if (i == 2){
            bz = sysBz.getSecondbz();
            bc = "小夜班";
        }else if (i == 3){
            bz = sysBz.getThirdbz();
            bc = "大夜班";
        }else {
            bz = null;
            bc = null;
        }
        Map<String,String> map = new HashMap<>();
        map.put("bz",bz);
        map.put("bc",bc);
        return map;
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollGrindingBFOne(Long indocno) {
        try {
            rollGrindingBFDao.deleteDataRollGrindingBFOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollGrindingBFMany(String str_id) {
        try {
            String sql = "delete roll_grinding_bf where indocno in(" + str_id + ")";
            rollGrindingBFDao.deleteDataRollGrindingBFMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollGrindingBF(String data, Long userId, String sname) {
        try {
            JRollGrindingBF jrollGrindingBF = JSON.parseObject(data, JRollGrindingBF.class);
            RollGrindingBF rollGrindingBF = jrollGrindingBF.getRollGrindingBF();
            Map<String,String> map = findbz(rollGrindingBF);
            rollGrindingBF.setSgroup(map.get("bz"));
            rollGrindingBF.setSclass(map.get("bc"));
            CodiUtil.editRecord(userId, sname, rollGrindingBF);
            rollGrindingBFDao.updateDataRollGrindingBF(rollGrindingBF);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollGrindingBFByPage(String data) {
        try {
            JRollGrindingBF jrollGrindingBF = JSON.parseObject(data, JRollGrindingBF.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollGrindingBF.getPageIndex();
            Integer pageSize = jrollGrindingBF.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollGrindingBF.getCondition()) {
                jsonObject = JSON.parseObject(jrollGrindingBF.getCondition().toString());
            }else{
                jsonObject = new JSONObject();
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }


            String machine_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("machine_no"))) {
                machine_no = jsonObject.get("machine_no").toString();
            }

            String grind_starttime = null;
            if (!StringUtils.isEmpty(jsonObject.get("grind_starttime"))) {
                grind_starttime = jsonObject.get("grind_starttime").toString();
            }

            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            String wheelno = null;
            if (!StringUtils.isEmpty(jsonObject.get("wheelno"))) {
                wheelno = jsonObject.get("wheelno").toString();
            }

            List<RollGrindingBF> list = rollGrindingBFDao.findDataRollGrindingBFByPage((pageIndex - 1)*pageSize, pageSize, roll_no, dbegin, dend, machine_no, grind_starttime,wheelno);
            Integer count = rollGrindingBFDao.findDataRollGrindingBFByPageSize(roll_no, dbegin, dend, machine_no, grind_starttime,wheelno);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollGrindingBFByIndocno(String data) {
        try {
            JRollGrindingBF jrollGrindingBF = JSON.parseObject(data, JRollGrindingBF.class);
            Long indocno = jrollGrindingBF.getIndocno();

            RollGrindingBF rollGrindingBF = rollGrindingBFDao.findDataRollGrindingBFByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollGrindingBF);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollGrindingBFByNo(String data) {
        try {
            JRollGrindingBF jrollGrindingBF = JSON.parseObject(data, JRollGrindingBF.class);
            String roll_no = jrollGrindingBF.getRollGrindingBF().getRoll_no();
            Integer pageIndex = jrollGrindingBF.getPageIndex();
            Integer pageSize = jrollGrindingBF.getPageSize();

            List<RollGrindingBF> rollGrindingBF = rollGrindingBFDao.findDataRollGrindingBFByNo((pageIndex - 1) * pageSize, pageSize, roll_no);
            Integer count = rollGrindingBFDao.findDataRollGrindingBFByNoSize(roll_no);
            return ResultData.ResultDataSuccess(rollGrindingBF, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollGrindingBF> findDataRollGrindingBF() {
        List<RollGrindingBF> list = rollGrindingBFDao.findDataRollGrindingBF();
        return list;
    }

    @Override
    public ResultData findWheelAbrasionByPage(String data) {
        try {
            JRollGrindingBFReport jRollGrindingBFReport = JSON.parseObject(data, JRollGrindingBFReport.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jRollGrindingBFReport.getPageIndex();
            Integer pageSize = jRollGrindingBFReport.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jRollGrindingBFReport.getCondition()) {
                jsonObject = JSON.parseObject(jRollGrindingBFReport.getCondition().toString());
            }else{
                jsonObject = new JSONObject();
            }

            String machine_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("machine_no"))) {
                machine_no = jsonObject.get("machine_no").toString();
            }
            String wheelname = null;
            if (!StringUtils.isEmpty(jsonObject.get("wheelname"))) {
                wheelname = jsonObject.get("wheelname").toString();
            }

            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }
            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            String factory_name = null;
            if (!StringUtils.isEmpty(jsonObject.get("factory_name"))) {
                factory_name = jsonObject.get("factory_name").toString();
            }

            List<RollGrindingBFReport> list = rollGrindingBFDao.findWheelAbrasionByPage((pageIndex - 1)*pageSize, pageSize, dbegin, dend, machine_no,wheelname,factory_name);
            Integer count = rollGrindingBFDao.findWheelAbrasionByPageSize(dbegin, dend, machine_no,wheelname,factory_name);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public String excelWheelAbrasion(HSSFWorkbook workbook,  String machine_no
            ,String wheelname
            ,String dbegin
            ,String dend
            ,String factory_name) {
        String filename="砂轮磨耗统计";
        if(StringUtils.isEmpty(machine_no)){
            machine_no = null;
        }
        if(StringUtils.isEmpty(wheelname)){
            wheelname = null;
        }
        if(StringUtils.isEmpty(dbegin)){
            dbegin = null;
        }
        if(StringUtils.isEmpty(dend)){
            dend = null;
        }
        if(StringUtils.isEmpty(factory_name)){
            factory_name = null;
        }
        List<RollGrindingBFReport> list = rollGrindingBFDao.findWheelAbrasionByPage(0, 1000, dbegin, dend, machine_no,wheelname,factory_name);
        HSSFSheet sheet = workbook.createSheet("sheet1");
        getExcelWheelAbrasion(list, sheet, workbook);
        return filename;
    }

    public ResultData findGrindingRateByPage(String data) {
        try {
            JRollGrindingBFReport jRollGrindingBFReport = JSON.parseObject(data, JRollGrindingBFReport.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jRollGrindingBFReport.getPageIndex();
            Integer pageSize = jRollGrindingBFReport.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jRollGrindingBFReport.getCondition()) {
                jsonObject = JSON.parseObject(jRollGrindingBFReport.getCondition().toString());
            }else{
                jsonObject = new JSONObject();
            }

            String machine_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("machine_no"))) {
                machine_no = jsonObject.get("machine_no").toString();
            }
            String operator = null;
            if (!StringUtils.isEmpty(jsonObject.get("operator"))) {
                operator = jsonObject.get("operator").toString();
            }

            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }
            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            List<RollGrindingBFReport> list = rollGrindingBFDao.findGrindingRateByPage((pageIndex - 1)*pageSize, pageSize, dbegin, machine_no,operator,dend);
            Integer count = rollGrindingBFDao.findGrindingRateByPageSize(dbegin, machine_no,operator,dend);
            RollGrindingBFReport bean = rollGrindingBFDao.findGrindingRateForRollGrindingBFReport(dbegin, machine_no,operator,dend);
            if(bean!=null){
                bean.setMaxtotalnum(rollGrindingBFDao.getfindGrindingRateForMaxOrMin(dbegin,"z.totalnum","max(y.totalnum)",machine_no,dend));
                bean.setMintotalnum(rollGrindingBFDao.getfindGrindingRateForMaxOrMin(dbegin,"z.totalnum","min(y.totalnum)",machine_no,dend));
                bean.setMaxgrinding_timetotalnum(rollGrindingBFDao.getfindGrindingRateForMaxOrMin(dbegin,"z.grinding_timetotalnum","max(y.grinding_timetotalnum)",machine_no,dend));
                bean.setMingrinding_timetotalnum(rollGrindingBFDao.getfindGrindingRateForMaxOrMin(dbegin,"z.grinding_timetotalnum","min(y.grinding_timetotalnum)",machine_no,dend));
                bean.setMaxrates(rollGrindingBFDao.getfindGrindingRateForMaxOrMin(dbegin,"z.rates","max(y.rates)",machine_no,dend));
                bean.setMinrates(rollGrindingBFDao.getfindGrindingRateForMaxOrMin(dbegin,"z.rates","min(y.rates)",machine_no,dend));
                bean.setMaxtotalavgnum(rollGrindingBFDao.getfindGrindingRateForMaxOrMin(dbegin,"z.totalavgnum","max(y.totalavgnum)",machine_no,dend));
                bean.setMintotalavgnum(rollGrindingBFDao.getfindGrindingRateForMaxOrMin(dbegin,"z.totalavgnum","min(y.totalavgnum)",machine_no,dend));
            }
            //人员合计
            List<RollGrindingBFReport> listAll = rollGrindingBFDao.findGrindingAllRateByPageSize(dbegin, machine_no,dend);

            return ResultData.ResultDataSuccess(list,count,bean,listAll);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public String excelGrindingRate(HSSFWorkbook workbook,String  machine_no
            ,String operator
            ,String dbegin
            ,String dend) {
        String filename="磨床稼动率";
        if(StringUtils.isEmpty(machine_no)){
            machine_no = null;
        }
        if(StringUtils.isEmpty(operator)){
            operator = null;
        }
        if(StringUtils.isEmpty(dbegin)){
            dbegin = null;
        }
        if(StringUtils.isEmpty(dend)){
            dend = null;
        }
            List<RollGrindingBFReport> list = rollGrindingBFDao.findGrindingRateByPage(0, 1000, dbegin, machine_no,operator,dend);
            RollGrindingBFReport bean = rollGrindingBFDao.findGrindingRateForRollGrindingBFReport(dbegin, machine_no,operator,dend);
            HSSFSheet sheet = workbook.createSheet("sheet1");
            getExcelGrindingRate(list,bean, sheet, workbook);
            return filename;
    }

    public ResultData findGrindingBFForTotalnum(String data) {

    try{
        JRollGrindingBFReport jRollGrindingBFReport = JSON.parseObject(data, JRollGrindingBFReport.class);
        JSONObject jsonObject = null;

        if (null != jRollGrindingBFReport.getCondition()) {
            jsonObject = JSON.parseObject(jRollGrindingBFReport.getCondition().toString());
        }else{
            jsonObject = new JSONObject();
        }

        String machine_no = null;
        if (!StringUtils.isEmpty(jsonObject.get("machine_no"))) {
            machine_no = jsonObject.get("machine_no").toString();
        }

        String dbegin = null;
        if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
            dbegin = jsonObject.get("dbegin").toString();
        }

        String dend = null;
        if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
            dend = jsonObject.get("dend").toString();
        }
        List<RollGrindingBFReport> list = rollGrindingBFDao.findGrindingBFForTotalnum(0, 30,dend,dbegin, null);
        Integer count = rollGrindingBFDao.findGrindingBFForTotalnumSize(dend,dbegin, null);

        List<RollGrindingBFReport> countList = rollGrindingBFDao.findGrindingBFForAllTotalnum(dend,dbegin, machine_no);

        return ResultData.ResultDataSuccess(list,count,null,countList);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }

    }

    public String excelGrindingBFForTotalnum(HSSFWorkbook workbook,
                                             String machine_no,String dbegin,String dend) {
            String filename="整台磨床的使用累积时间";
        if(StringUtils.isEmpty(machine_no)){
            machine_no = null;
        }
        if(StringUtils.isEmpty(dbegin)){
            dbegin = null;
        }
        if(StringUtils.isEmpty(dend)){
            dend = null;
        }
            List<RollGrindingBFReport> list = rollGrindingBFDao.findGrindingBFForTotalnum(0, 30,dend,dbegin, machine_no);
            HSSFSheet sheet = workbook.createSheet("sheet1");
            getExcelGrindingBFForTotalnum(list,sheet, workbook);
            return filename;
    }

    public ResultData findGrindingRateABigScreenByPage(String data) {
        try {
            JRollGrindingBFReport jRollGrindingBFReport = JSON.parseObject(data, JRollGrindingBFReport.class);
            JSONObject jsonObject = null;

            if (null != jRollGrindingBFReport.getCondition()) {
                jsonObject = JSON.parseObject(jRollGrindingBFReport.getCondition().toString());
            }else{
                jsonObject = new JSONObject();
            }
                // 如果
            String dend = null;
            String dbegin = null;
            dbegin = DateUtil.getTimeStr(1);
            dend = DateUtil.getTimeStr(2);

            List<RollGrindingBFReport> list = rollGrindingBFDao.findGrindingRateABigScreenByPage(dbegin, dend);
            List<MapXYEntity> map = new ArrayList<MapXYEntity>();
            if(list!=null&&list.size()>0){
                for (RollGrindingBFReport bean:list) {
                    MapXYEntity t = new MapXYEntity();
                    if(!StringUtils.isEmpty(bean.getSclass())){
                        t.setX(bean.getSclass());
                    }else{
                        t.setX(0);
                    }
                    if(!StringUtils.isEmpty(bean.getTotalnum())){
                        t.setY1(bean.getTotalnum());
                    }else{
                        t.setY1(0);
                    }
                    if(!StringUtils.isEmpty(bean.getGrinding_timetotalnum())){
                        t.setY2(bean.getGrinding_timetotalnum());
                    }else{
                        t.setY2(0);
                    }
                    if(!StringUtils.isEmpty(bean.getRolldiameter_reduce())){
                        t.setY3(bean.getRolldiameter_reduce());
                    }else{
                        t.setY3(0);
                    }
                    if(!StringUtils.isEmpty(bean.getSanddiameter_reduce())){
                        t.setY4(bean.getSanddiameter_reduce());
                    }else{
                        t.setY4(0);
                    }
                    if(!StringUtils.isEmpty(bean.getGrinding_timeavgnum())){
                        t.setY5(bean.getGrinding_timeavgnum());
                    }else{
                        t.setY5(0);
                    }
                    map.add(t);
                }
            }

            return ResultData.ResultDataSuccess(list,0,null,map);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    private void getExcelGrindingBFForTotalnum(List<RollGrindingBFReport> list, HSSFSheet sheet, HSSFWorkbook workbook) {
        if (list != null && list.size() > 0) {
            // 设置表头样式 居中 加粗 宋体 20
            HSSFCellStyle cellStyleHead = workbook.createCellStyle();
            cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
            HSSFFont redFontHead = workbook.createFont();
            redFontHead.setFontHeightInPoints((short) 12);//设置字体大小
            redFontHead.setFontName("宋体");//字体型号
            cellStyleHead.setFont(redFontHead);
            HSSFRow rowhead = sheet.createRow(0);   //创建第一行头
            HSSFCell cell_00 = rowhead.createCell(0);   //创建第一行第一列
            cell_00.setCellStyle(cellStyleHead);
            cell_00.setCellValue("整台磨床的使用累积时间");
            HSSFRow row1 = sheet.createRow(1);   //创建第二行
            HSSFCell cell_2_1 = row1.createCell(0);   //创建第二行第一列
            cell_2_1.setCellValue("序号");
            HSSFCell cell_2_2 = row1.createCell(1);   //创建第二行第九列
            cell_2_2.setCellValue("磨床号");
            HSSFCell cell_2_3 = row1.createCell(2);   //创建第二行第二列
            cell_2_3.setCellValue("累计时间(min)");
            HSSFCell cell_2_4 = row1.createCell(3);   //创建第二行第三列
            cell_2_4.setCellValue("累计条数");

            //从第三行开始(包含第三行)
            for (int i = 0; i < list.size(); i++) {
                RollGrindingBFReport v = list.get(i);
                HSSFRow rown = sheet.createRow(i + 2);   //创建第三行
                HSSFCell cell_3_1 = rown.createCell(0);   // 1
                cell_3_1.setCellValue(i + 1);
                HSSFCell cell_3_2 = rown.createCell(1);   //2
                if (!StringUtils.isEmpty(v.getMachineno())) {
                    cell_3_2.setCellValue(v.getMachineno());
                }
                HSSFCell cell_3_3 = rown.createCell(2);   //3
                if (!StringUtils.isEmpty(v.getGrinding_timetotalnum())) {
                    cell_3_3.setCellValue(v.getGrinding_timetotalnum().toString());
                }
                HSSFCell cell_3_4 = rown.createCell(3);   //4
                if (!StringUtils.isEmpty(v.getTotalnum())) {
                    cell_3_4.setCellValue(v.getTotalnum().toString());
                }
            }
            CellRangeAddress region = new CellRangeAddress(0, 0, 0, 3);
            sheet.addMergedRegion(region);
        }
    }
    private void getExcelGrindingRate(List<RollGrindingBFReport> list, RollGrindingBFReport bean, HSSFSheet sheet, HSSFWorkbook workbook) {
        if(list!=null && list.size()>0) {
            // 设置表头样式 居中 加粗 宋体 20
            HSSFCellStyle cellStyleHead = workbook.createCellStyle();
            cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
            HSSFFont redFontHead = workbook.createFont();
            redFontHead.setFontHeightInPoints((short) 12);//设置字体大小
            redFontHead.setFontName("宋体");//字体型号
            cellStyleHead.setFont(redFontHead);
            HSSFRow rowhead = sheet.createRow(0);   //创建第一行头
            HSSFCell cell_00 = rowhead.createCell(0);   //创建第一行第一列
            cell_00.setCellStyle(cellStyleHead);
            cell_00.setCellValue("磨床稼动率");
            HSSFRow row1 = sheet.createRow(1);   //创建第二行
            HSSFCell cell_2_1 = row1.createCell(0);   //创建第二行第一列
            cell_2_1.setCellValue("序号");
            HSSFCell cell_2_2 = row1.createCell(1);   //创建第二行第九列
            cell_2_2.setCellValue("日期");
            HSSFCell cell_2_3 = row1.createCell(2);   //创建第二行第二列
            cell_2_3.setCellValue("人员");
            HSSFCell cell_2_4 = row1.createCell(3);   //创建第二行第三列
            cell_2_4.setCellValue("磨床号");
            HSSFCell cell_2_5 = row1.createCell(4);   //创建第二行第四列
            cell_2_5.setCellValue("支撑辊");
            HSSFCell cell_2_6 = row1.createCell(5);   //创建第二行第五列
            cell_2_6.setCellValue("粗轧工作辊");
            HSSFCell cell_2_7 = row1.createCell(6);   //创建第二行第六列
            cell_2_7.setCellValue("F1-4");
            HSSFCell cell_2_8 = row1.createCell(7);   //创建第二行第七列
            cell_2_8.setCellValue("F5-8");
            HSSFCell cell_2_9 = row1.createCell(8);   //创建第二行第八列
            cell_2_9.setCellValue("其他");
            HSSFCell cell_2_10 = row1.createCell(9);   //创建第二行第10列
            cell_2_10.setCellValue("磨辊总时间（min）");
            HSSFCell cell_2_11 = row1.createCell(10);   //创建第二行第11列
            cell_2_11.setCellValue("设备稼动率");

            //从第三行开始(包含第三行)
            for (int i = 0; i < list.size(); i++) {
                RollGrindingBFReport v = list.get(i);
                HSSFRow rown = sheet.createRow(i + 2);   //创建第三行
                HSSFCell cell_3_1 = rown.createCell(0);   // 1
                cell_3_1.setCellValue(i + 1);
                HSSFCell cell_3_2 = rown.createCell(1);   //2
                if (!StringUtils.isEmpty(v.getOperator())) {
                    cell_3_2.setCellValue(v.getOperator());
                }
                HSSFCell cell_3_3 = rown.createCell(2);   //3
                if (!StringUtils.isEmpty(v.getGrind_starttime())) {
                    cell_3_3.setCellValue(v.getGrind_starttime());
                }
                HSSFCell cell_3_4 = rown.createCell(3);   //4
                if (!StringUtils.isEmpty(v.getMachineno())) {
                    cell_3_4.setCellValue(v.getMachineno());
                }
                HSSFCell cell_3_5 = rown.createCell(4);   //5
                if (!StringUtils.isEmpty(v.getRnum())) {
                    cell_3_5.setCellValue(v.getRnum().toString());
                }
                HSSFCell cell_3_6 = rown.createCell(5);   //6
                if (!StringUtils.isEmpty(v.getRwnum())) {
                    cell_3_6.setCellValue(v.getRwnum().toString());
                }
                HSSFCell cell_3_7 = rown.createCell(6);   //7
                if (!StringUtils.isEmpty(v.getFwnum())) {
                    cell_3_7.setCellValue(v.getFwnum().toString());
                }
                HSSFCell cell_3_8 = rown.createCell(7);   //8
                if (!StringUtils.isEmpty(v.getGwnum())) {
                    cell_3_8.setCellValue(v.getGwnum().toString());
                }
                HSSFCell cell_3_9 = rown.createCell(8);   //9
                if (!StringUtils.isEmpty(v.getOthernum())) {
                    cell_3_9.setCellValue(v.getOthernum().toString());
                }
                HSSFCell cell_3_10 = rown.createCell(9);   //10
                if (!StringUtils.isEmpty(v.getGrinding_time())) {
                    cell_3_10.setCellValue(v.getGrinding_time().toString());
                }
                HSSFCell cell_3_11 = rown.createCell(10);   //11
                if (!StringUtils.isEmpty(v.getRates())) {
                    cell_3_11.setCellValue(v.getRates().toString());
                }
            }

            HSSFRow rownhj = sheet.createRow(list.size() + 1);   //创建 合计行
            HSSFCell cell_hj_1 = rownhj.createCell(0);   // 1
            cell_hj_1.setCellValue("");
            HSSFCell cell_hj_2 = rownhj.createCell(1);   // 2
            cell_hj_2.setCellValue("总计");
            HSSFCell cell_hj_3 = rownhj.createCell(2);   // 3
            cell_hj_3.setCellValue("");
            HSSFCell cell_hj_4 = rownhj.createCell(3);   //4
            cell_hj_4.setCellValue("");
            HSSFCell cell_hj_5 = rownhj.createCell(4);   //5
            if (!StringUtils.isEmpty(bean.getRtotalnum())) {
                cell_hj_5.setCellValue(bean.getRtotalnum().toString());
            }
            HSSFCell cell_hj_6 = rownhj.createCell(5);   //6
            if (!StringUtils.isEmpty(bean.getRwtotalnum())) {
                cell_hj_6.setCellValue(bean.getRwtotalnum().toString());
            }
            HSSFCell cell_hj_7 = rownhj.createCell(6);   //7
            if (!StringUtils.isEmpty(bean.getFwtotalnum())) {
                cell_hj_7.setCellValue(bean.getFwtotalnum().toString());
            }
            HSSFCell cell_hj_8 = rownhj.createCell(7);   //8
            if (!StringUtils.isEmpty(bean.getGwtotalnum())) {
                cell_hj_8.setCellValue(bean.getGwtotalnum().toString());
            }
            HSSFCell cell_hj_9 = rownhj.createCell(8);   //9
            if (!StringUtils.isEmpty(bean.getOthertotalnum())) {
                cell_hj_9.setCellValue(bean.getOthertotalnum().toString());
            }
            HSSFCell cell_hj_10 = rownhj.createCell(9);   //10
            if (!StringUtils.isEmpty(bean.getGrinding_timetotalnum())) {
                cell_hj_10.setCellValue(bean.getGrinding_timetotalnum().toString());
            }
            HSSFCell cell_hj_11 = rownhj.createCell(10);   //11
            if (!StringUtils.isEmpty(bean.getRates())) {
                cell_hj_11.setCellValue(bean.getRates().toString());
            }

            HSSFRow rownpj = sheet.createRow(list.size() + 2);   //创建 平均行
            HSSFCell cell_pj_1 = rownpj.createCell(0);   // 1
            cell_pj_1.setCellValue("");
            HSSFCell cell_pj_2 = rownpj.createCell(1);   // 2
            cell_pj_2.setCellValue("每班平均");
            HSSFCell cell_pj_3 = rownpj.createCell(2);   // 3
            cell_pj_3.setCellValue("");
            HSSFCell cell_pj_4 = rownpj.createCell(3);   //4
            cell_pj_4.setCellValue("");
            HSSFCell cell_pj_5 = rownpj.createCell(4);   //5
            if (!StringUtils.isEmpty(bean.getRavgnum())) {
                cell_pj_5.setCellValue(bean.getRavgnum().toString());
            }
            HSSFCell cell_pj_6 = rownpj.createCell(5);   //6
            if (!StringUtils.isEmpty(bean.getRwavgnum())) {
                cell_pj_6.setCellValue(bean.getRwavgnum().toString());
            }
            HSSFCell cell_pj_7 = rownpj.createCell(6);   //7
            if (!StringUtils.isEmpty(bean.getFwavgnum())) {
                cell_pj_7.setCellValue(bean.getFwavgnum().toString());
            }
            HSSFCell cell_pj_8 = rownpj.createCell(7);   //8
            if (!StringUtils.isEmpty(bean.getGwavgnum())) {
                cell_pj_8.setCellValue(bean.getGwavgnum().toString());
            }
            HSSFCell cell_pj_9 = rownpj.createCell(8);   //9
            if (!StringUtils.isEmpty(bean.getOtheravgnum())) {
                cell_pj_9.setCellValue(bean.getOtheravgnum().toString());
            }
            HSSFCell cell_pj_10 = rownpj.createCell(9);   //10
            if (!StringUtils.isEmpty(bean.getGrinding_timeavgnum())) {
                cell_pj_10.setCellValue(bean.getGrinding_timeavgnum().toString());
            }
            HSSFCell cell_pj_11 = rownpj.createCell(10);   //11
            if (!StringUtils.isEmpty(bean.getRates())) {
                cell_pj_11.setCellValue(bean.getRates().toString());
            }

            HSSFRow rownpjs = sheet.createRow(list.size() + 3);   //创建 平均行
            HSSFCell cell_pjs_1 = rownpjs.createCell(0);   // 1
            cell_pjs_1.setCellValue("");
            HSSFCell cell_pjs_2 = rownpjs.createCell(1);   // 2
            cell_pjs_2.setCellValue("平均每支辊用时");
            HSSFCell cell_pjs_3 = rownpjs.createCell(2);   // 3
            if (!StringUtils.isEmpty(bean.getGrinding_timetotalnum())&&!StringUtils.isEmpty(bean.getTotalnum())) {
                cell_pjs_3.setCellValue(bean.getGrinding_timetotalnum().divide(bean.getTotalnum(),2,BigDecimal.ROUND_HALF_UP).toString());
            }

            CellRangeAddress region = new CellRangeAddress(0, 0, 0, 10);
            sheet.addMergedRegion(region);
        }
    }

    private void getExcelWheelAbrasion(List<RollGrindingBFReport> list, HSSFSheet sheet, HSSFWorkbook workbook) {

        if(list!=null && list.size()>0){
            // 设置表头样式 居中 加粗 宋体 20
            HSSFCellStyle cellStyleHead = workbook.createCellStyle();
            cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
            HSSFFont redFontHead = workbook.createFont();
            redFontHead.setFontHeightInPoints((short) 12);//设置字体大小
            redFontHead.setFontName("宋体");//字体型号
            cellStyleHead.setFont(redFontHead);
            HSSFRow rowhead = sheet.createRow(0);   //创建第一行头
            HSSFCell cell_00 = rowhead.createCell(0);   //创建第一行第一列
            cell_00.setCellStyle(cellStyleHead);
            cell_00.setCellValue("砂轮磨耗统计");
            HSSFRow row1 = sheet.createRow(1);   //创建第二行
            HSSFCell cell_2_1 = row1.createCell(0);   //创建第二行第一列
            cell_2_1.setCellValue("序号");
            HSSFCell cell_2_2 = row1.createCell(1);   //创建第二行第二列
            cell_2_2.setCellValue("砂轮号");
            HSSFCell cell_2_3 = row1.createCell(2);   //创建第二行第三列
            cell_2_3.setCellValue("磨床号");
            HSSFCell cell_2_4 = row1.createCell(3);   //创建第二行第四列
            cell_2_4.setCellValue("砂轮粒度");
            HSSFCell cell_2_5 = row1.createCell(4);   //创建第二行第五列
            cell_2_5.setCellValue("日期");
            HSSFCell cell_2_6 = row1.createCell(5);   //创建第二行第六列
            cell_2_6.setCellValue("站别");
            HSSFCell cell_2_7 = row1.createCell(6);   //创建第二行第七列
            cell_2_7.setCellValue("辊号");
            HSSFCell cell_2_8 = row1.createCell(7);   //创建第二行第八列
            cell_2_8.setCellValue("磨前辊径");
            HSSFCell cell_2_9 = row1.createCell(8);   //创建第二行第九列
            cell_2_9.setCellValue("磨后辊径");

            HSSFCell cell_2_10 = row1.createCell(9);   //创建第二行第10列
            cell_2_10.setCellValue("磨前砂径");
            HSSFCell cell_2_11 = row1.createCell(10);   //创建第二行第11列
            cell_2_11.setCellValue("磨后砂径");

            HSSFCell cell_2_12 = row1.createCell(11);   //创建第二行第12列
            cell_2_12.setCellValue("磨削时间(min)");
            HSSFCell cell_2_13 = row1.createCell(12);   //创建第二行第13列
            cell_2_13.setCellValue("辊长");
            HSSFCell cell_2_14 = row1.createCell(13);   //创建第二行第14列
            cell_2_14.setCellValue("砂厚");
            HSSFCell cell_2_15 = row1.createCell(14);   //创建第二行第15列
            cell_2_15.setCellValue("砂辊径减少比");
            HSSFCell cell_2_16 = row1.createCell(15);   //创建第二行第16列
            cell_2_16.setCellValue("辊径减少");
            HSSFCell cell_2_17 = row1.createCell(16);   //创建第二行第17列
            cell_2_17.setCellValue("辊体减少（cm3)");
            HSSFCell cell_2_18 = row1.createCell(17);   //创建第二行第18列
            cell_2_18.setCellValue("砂径减少");
            HSSFCell cell_2_19 = row1.createCell(18);   //创建第二行第19列
            cell_2_19.setCellValue("砂体减少(cm3)");
            HSSFCell cell_2_20 = row1.createCell(19);   //创建第二行第20列
            cell_2_20.setCellValue("磨耗比(V/V)");
            HSSFCell cell_2_21 = row1.createCell(20);   //创建第二行第21列
            cell_2_21.setCellValue("效率(mm/min) ");
            HSSFCell cell_2_22 = row1.createCell(21);   //创建第二行第22列
            cell_2_22.setCellValue("备注");
            HSSFCell cell_2_23 = row1.createCell(22);   //创建第二行第23列
            cell_2_23.setCellValue("砂轮厂家名称");

            //从第三行开始(包含第三行)
            for (int i = 0; i < list.size(); i++) {
                RollGrindingBFReport v = list.get(i);
                HSSFRow rown = sheet.createRow(i + 2);   //创建第三行
                HSSFCell cell_3_1 = rown.createCell(0);   // 1
                cell_3_1.setCellValue(i + 1);
                HSSFCell cell_3_2 = rown.createCell(1);   //2
                if (!StringUtils.isEmpty(v.getWheelname())) {
                    cell_3_2.setCellValue(v.getWheelname());
                }
                HSSFCell cell_3_3 = rown.createCell(2);   //3
                if (!StringUtils.isEmpty(v.getMachineno())) {
                    cell_3_3.setCellValue(v.getMachineno());
                }
                HSSFCell cell_3_4 = rown.createCell(3);   //4
                if (!StringUtils.isEmpty(v.getSand_article())) {
                    cell_3_4.setCellValue(v.getSand_article().toString());
                }
                HSSFCell cell_3_5 = rown.createCell(4);   //5
                if (!StringUtils.isEmpty(v.getGrind_starttime())) {
                    cell_3_5.setCellValue(v.getGrind_starttime().substring(5,10));
                }
                HSSFCell cell_3_6 = rown.createCell(5);   //6
                if (!StringUtils.isEmpty(v.getRoll_type())) {
                    cell_3_6.setCellValue(v.getRoll_type());
                }
                HSSFCell cell_3_7 = rown.createCell(6);   //7
                if (!StringUtils.isEmpty(v.getRoll_no())) {
                    cell_3_7.setCellValue(v.getRoll_no());
                }
                HSSFCell cell_3_8 = rown.createCell(7);   //8
                if (!StringUtils.isEmpty(v.getBefore_diameter())) {
                    cell_3_8.setCellValue(v.getBefore_diameter());
                }
                HSSFCell cell_3_9 = rown.createCell(8);   //9
                if (!StringUtils.isEmpty(v.getAfter_diameter())) {
                    cell_3_9.setCellValue(v.getAfter_diameter());
                }
                HSSFCell cell_3_10 = rown.createCell(9);   //10
                if (!StringUtils.isEmpty(v.getWheel_dia_start())) {
                    cell_3_10.setCellValue(v.getWheel_dia_start());
                }
                HSSFCell cell_3_11 = rown.createCell(10);   //11
                if (!StringUtils.isEmpty(v.getWheel_dia_end())) {
                    cell_3_11.setCellValue(v.getWheel_dia_end());
                }
                HSSFCell cell_3_12 = rown.createCell(11);   //12
                if (!StringUtils.isEmpty(v.getGrinding_time())) {
                    cell_3_12.setCellValue(v.getGrinding_time().toString());
                }
                HSSFCell cell_3_13 = rown.createCell(12);   //13
                if (!StringUtils.isEmpty(v.getBody_length())) {
                    cell_3_13.setCellValue(v.getBody_length().toString());
                }
                HSSFCell cell_3_14 = rown.createCell(13);   //14
                if (!StringUtils.isEmpty(v.getSand_thickness())) {
                    cell_3_14.setCellValue(v.getSand_thickness().toString());
                }
                HSSFCell cell_3_15 = rown.createCell(14);   //15
                if (!StringUtils.isEmpty(v.getReduction_ratio())) {
                    cell_3_15.setCellValue(v.getReduction_ratio().toString());
                }
                HSSFCell cell_3_16 = rown.createCell(15);   //16
                if (!StringUtils.isEmpty(v.getRolldiameter_reduce())) {
                    cell_3_16.setCellValue(v.getRolldiameter_reduce().toString());
                }

                HSSFCell cell_3_17 = rown.createCell(16);   //22
                if (!StringUtils.isEmpty(v.getRollerbody_reduce())) {
                    cell_3_17.setCellValue(v.getRollerbody_reduce().toString());
                }

                HSSFCell cell_3_18 = rown.createCell(17);   //17
                if (!StringUtils.isEmpty(v.getSanddiameter_reduce())) {
                    cell_3_18.setCellValue(v.getSanddiameter_reduce().toString());
                }
                HSSFCell cell_3_19 = rown.createCell(18);   //18
                if (!StringUtils.isEmpty(v.getSandbody_reduce())) {
                    cell_3_19.setCellValue(v.getSandbody_reduce().toString());
                }
                HSSFCell cell_3_20 = rown.createCell(19);   //19
                if (!StringUtils.isEmpty(v.getAbrasion_than())) {
                    cell_3_20.setCellValue(v.getAbrasion_than().toString());
                }
                HSSFCell cell_3_21 = rown.createCell(20);   //20
                if (!StringUtils.isEmpty(v.getEfficiency())) {
                    cell_3_21.setCellValue(v.getEfficiency().toString());
                }
                HSSFCell cell_3_22 = rown.createCell(21);   //21
                if (!StringUtils.isEmpty(v.getSnote())) {
                    cell_3_22.setCellValue(v.getSnote());
                }
                HSSFCell cell_3_23 = rown.createCell(22);   //22
                if (!StringUtils.isEmpty(v.getFactory_name())) {
                    cell_3_23.setCellValue(v.getFactory_name());
                }

            }
            CellRangeAddress region = new CellRangeAddress(0, 0, 0, 22);
            sheet.addMergedRegion(region);

        }
    }
}
