package com.my.business.modular.rollgrinding.dao;

import com.my.business.modular.rollgrinding.entity.RollGrinding;
import com.my.business.modular.rollgrinding.entity.RollGrindingBF;
import com.my.business.modular.rollgrinding.entity.RollGrindingBFReport;
import com.my.business.restful.entity.Grinder_performent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 磨削实绩dao接口
 *
 * @author 生成器生成
 * @date 2020-08-24 17:28:05
 */
@Mapper
public interface RollGrindingBFDao {

    /**
     * 添加记录
     *
     * @param rollGrindingBF 对象实体
     */
    void insertDataRollGrindingBF(RollGrindingBF rollGrindingBF);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollGrindingBFOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollGrindingBFMany(String value);

    /**
     * 修改记录
     *
     * @param rollGrindingBF 对象实体
     */
    void updateDataRollGrindingBF(RollGrindingBF rollGrindingBF);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex       第几页
     * @param pageSize        每页总数
     * @param roll_no         辊号
     * @param grind_starttime 磨削开始时间
     * @param machine_no      磨床号
     * @return 对象数据集合
     */
    List<RollGrindingBF> findDataRollGrindingBFByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("roll_no") String roll_no,  @Param("dbegin") String dbegin, @Param("dend") String dend, @Param("machine_no") String machine_no, @Param("grind_starttime") String grind_starttime , @Param("wheelno") String wheelno);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollGrindingBFByPageSize(@Param("roll_no") String roll_no,  @Param("dbegin") String dbegin, @Param("dend") String dend, @Param("machine_no") String machine_no, @Param("grind_starttime") String grind_starttime,@Param("wheelno") String wheelno);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollGrindingBF findDataRollGrindingBFByIndocno(@Param("indocno") Long indocno);

    /***
     * 根据辊号查询最新一条信息
     * @param roll_no 用户id
     * @return
     */
    RollGrindingBF findDataRollGrindingBFByNoNew(@Param("roll_no") String roll_no);


    /***
     * 根据辊号查询信息
     * @return
     */
    List<RollGrindingBF> findDataRollGrindingBFByNo(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("roll_no") String roll_no);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollGrindingBFByNoSize(@Param("roll_no") String roll_no);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollGrindingBF> findDataRollGrindingBF();

    /**
     * 查找重复数量
     *
     * @param value
     * @return
     */
    Integer findDataHave(@Param("value") String value, @Param("machine_no") String machine_no);

    /**
     * 查找最新的日期时间
     *
     * @return datetime
     */
    String findTime(@Param("machine_no") String machine_no);
    
    /**
     * 根据辊号查看记录
     *
     * @return 对象数据集合
     */
    List<Grinder_performent> findRest(@Param("roll_no") String roll_no);

    /**
     * 根据辊号、时间查看查看记录
     *
     * @param roll_no         辊号
     * @param grind_starttime 磨削开始时间
     * @return 对象数据集合
     */
    RollGrindingBF findDataRollGrindingBFforOne(@Param("roll_no") String roll_no, @Param("grind_starttime") String grind_starttime);

    /**
     * 清除PRESET
     */
    void clear();

    /**
     * 查找上机前最新一条磨削记录
     * @param roll_no 辊号
     * @param onlinetime 上下线
     * @return 数据
     */
    RollGrindingBF findNewMax(@Param("roll_no") String roll_no, @Param("onlinetime") String onlinetime);

    List<RollGrindingBF> findDataRollGrindingByIstate();

    void updateDataRollGrindingBFForIstate(@Param("indocno") Long indocno,@Param("istate") Long istate);

    List<RollGrindingBFReport> findWheelAbrasionByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("dbegin")String dbegin, @Param("dend")String dend, @Param("machine_no")String machine_no, @Param("wheelname")String wheelname,@Param("factory_name")String factory_name);

    Integer findWheelAbrasionByPageSize(@Param("dbegin")String dbegin, @Param("dend")String dend, @Param("machine_no")String machine_no, @Param("wheelname")String wheelname,@Param("factory_name")String factory_name);

    List<RollGrindingBFReport> findGrindingRateByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("dbegin")String dbegin, @Param("machine_no")String machine_no, @Param("operator")String operator,@Param("dend")String dend);

    Integer findGrindingRateByPageSize(@Param("dbegin")String dbegin, @Param("machine_no")String machine_no, @Param("operator")String operator,@Param("dend")String dend );

    RollGrindingBFReport findGrindingRateForRollGrindingBFReport(@Param("dbegin")String dbegin, @Param("machine_no")String machine_no, @Param("operator")String operator,@Param("dend")String dend);

    String getfindGrindingRateForMaxOrMin(@Param("dbegin")String dbegin,@Param("str1")String str1,@Param("str2")String str2, @Param("machine_no")String machine_no,@Param("dend")String dend);

    void updateDataRollGrindingBFPersonal(RollGrindingBF rollGrindingBF);

    Integer findGrindingBFForTotalnumSize(@Param("dend")String dend, @Param("dbegin")String dbegin, @Param("machine_no")String machine_no);

    List<RollGrindingBFReport> findGrindingBFForTotalnum(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("dend")String dend, @Param("dbegin")String dbegin, @Param("machine_no")String machine_no);

    List<RollGrindingBFReport> findGrindingRateABigScreenByPage(@Param("dbegin") String dbegin,@Param("dend")  String dend);

    List<RollGrindingBFReport> findGrindingAllRateByPageSize(@Param("dbegin") String dbegin, @Param("machine_no") String machine_no, @Param("dend") String dend);

    List<RollGrindingBFReport> findGrindingBFForAllTotalnum(@Param("dend")String dend, @Param("dbegin")String dbegin, @Param("machine_no")String machine_no);
}
