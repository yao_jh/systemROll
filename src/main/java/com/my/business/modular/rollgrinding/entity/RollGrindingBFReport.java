package com.my.business.modular.rollgrinding.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 磨削实绩实体类
 *
 * @author 生成器生成
 * @date 2020-08-24 17:28:04
 */
public class RollGrindingBFReport{

    /*private Long indocno;  //主键
    private String curvetype;//曲线类型
    private String programname;//程序名
    private String grind_endtime;  //磨削结束时间

    private String standno;//磨床名

    private Double curvetolerance;//曲线误差
    private Double coaxality;//同心度
    private Double roundness;  //圆度
    //private Double crack;  //裂纹
    //private Double hidden_flaws;  //暗伤
    private Double qualifiednum;  //合格点数
    private String sclass;  //班
    private String sgroup;  //班组
    private String operator;  //操作人姓名

    private Date createtime;//创建时间
    private String wheelno;//砂轮编号

    private String dbegin;
    private String dend;*/

    private String  wheelname;//砂轮名称
    private String  factory_name;//客户名称
    private BigDecimal sand_article;//砂轮粒度
    private String roll_type;//站别
    private String roll_no;  //辊号
    private Double before_diameter;  //磨前中部直径
    private Double after_diameter;  //磨后直径
    private Double wheel_dia_start; //砂轮开始直径
    private Double wheel_dia_end; //砂轮结束直径
    private BigDecimal  body_length;//辊长
    private BigDecimal  sand_thickness;//砂厚
    private BigDecimal reduction_ratio;//砂辊径减少比
    private BigDecimal rolldiameter_reduce;//辊径减少
    private BigDecimal rollerbody_reduce;//辊体减少（cm3)
    private BigDecimal sanddiameter_reduce;//砂径减少
    private BigDecimal sandbody_reduce;//砂体减少(cm3)
    private BigDecimal abrasion_than;//磨耗比(V/V)
    private BigDecimal efficiency;//效率(mm/min)
    private String snote;  //备注

    private String machineno;  //磨床号
    private String grind_starttime;  //磨削开始时间
    private String sclass;  //班
    private String sgroup;  //班组
    private String operator;  //操作人姓名

    private BigDecimal totalnum;  //总数
    private BigDecimal gwnum;  //5-8工作辊
    private BigDecimal fwnum;  //1-4工作辊
    private BigDecimal rnum;  //支撑辊
    private BigDecimal rwnum;  //粗轧工作辊
    private BigDecimal othernum;  //其他数

    private BigDecimal rates;//稼动率
    private BigDecimal totalavgnum;  //平均数
    private BigDecimal gwavgnum;  //5-8工作辊平均
    private BigDecimal fwavgnum;  //1-4工作辊平均
    private BigDecimal ravgnum;  //支撑辊平均
    private BigDecimal rwavgnum;  //粗轧工作辊平均
    private BigDecimal otheravgnum;  //其他数平均

    private BigDecimal grinding_time;//磨削时长
    private BigDecimal grinding_timetotalnum;  //磨削时长合计
    private BigDecimal grinding_timeavgnum;//平均磨削时长

    private BigDecimal gwtotalnum;  //5-8工作辊合计
    private BigDecimal fwtotalnum;  //1-4工作辊合计
    private BigDecimal rtotalnum;  //支撑辊合计
    private BigDecimal rwtotalnum;  //粗轧工作辊合计
    private BigDecimal othertotalnum;  //其他数合计

    private String maxtotalnum;//磨辊总支数 最多
    private String mintotalnum;//磨辊总支数 最少

    private String maxgrinding_timetotalnum;//磨辊总时间 最多
    private String mingrinding_timetotalnum;//磨辊总时间 最少

    private String maxrates;//设备稼动率 最多
    private String minrates;//设备稼动率 最少

    private String maxtotalavgnum;//平均每支辊用时 最多
    private String mintotalavgnum;//平均每支辊用时 最少

    public String getFactory_name() {
        return factory_name;
    }

    public void setFactory_name(String factory_name) {
        this.factory_name = factory_name;
    }

    public String getMaxtotalnum() {
        return maxtotalnum;
    }

    public void setMaxtotalnum(String maxtotalnum) {
        this.maxtotalnum = maxtotalnum;
    }

    public String getMintotalnum() {
        return mintotalnum;
    }

    public void setMintotalnum(String mintotalnum) {
        this.mintotalnum = mintotalnum;
    }

    public String getMaxgrinding_timetotalnum() {
        return maxgrinding_timetotalnum;
    }

    public void setMaxgrinding_timetotalnum(String maxgrinding_timetotalnum) {
        this.maxgrinding_timetotalnum = maxgrinding_timetotalnum;
    }

    public String getMingrinding_timetotalnum() {
        return mingrinding_timetotalnum;
    }

    public void setMingrinding_timetotalnum(String mingrinding_timetotalnum) {
        this.mingrinding_timetotalnum = mingrinding_timetotalnum;
    }

    public String getMaxrates() {
        return maxrates;
    }

    public void setMaxrates(String maxrates) {
        this.maxrates = maxrates;
    }

    public String getMinrates() {
        return minrates;
    }

    public void setMinrates(String minrates) {
        this.minrates = minrates;
    }

    public String getMaxtotalavgnum() {
        return maxtotalavgnum;
    }

    public void setMaxtotalavgnum(String maxtotalavgnum) {
        this.maxtotalavgnum = maxtotalavgnum;
    }

    public String getMintotalavgnum() {
        return mintotalavgnum;
    }

    public void setMintotalavgnum(String mintotalavgnum) {
        this.mintotalavgnum = mintotalavgnum;
    }

    public BigDecimal getTotalavgnum() {
        return totalavgnum;
    }

    public void setTotalavgnum(BigDecimal totalavgnum) {
        this.totalavgnum = totalavgnum;
    }

    public BigDecimal getGwavgnum() {
        return gwavgnum;
    }

    public void setGwavgnum(BigDecimal gwavgnum) {
        this.gwavgnum = gwavgnum;
    }

    public BigDecimal getFwavgnum() {
        return fwavgnum;
    }

    public void setFwavgnum(BigDecimal fwavgnum) {
        this.fwavgnum = fwavgnum;
    }

    public BigDecimal getRavgnum() {
        return ravgnum;
    }

    public void setRavgnum(BigDecimal ravgnum) {
        this.ravgnum = ravgnum;
    }

    public BigDecimal getRwavgnum() {
        return rwavgnum;
    }

    public void setRwavgnum(BigDecimal rwavgnum) {
        this.rwavgnum = rwavgnum;
    }

    public BigDecimal getOtheravgnum() {
        return otheravgnum;
    }

    public void setOtheravgnum(BigDecimal otheravgnum) {
        this.otheravgnum = otheravgnum;
    }

    public BigDecimal getGrinding_timetotalnum() {
        return grinding_timetotalnum;
    }

    public void setGrinding_timetotalnum(BigDecimal grinding_timetotalnum) {
        this.grinding_timetotalnum = grinding_timetotalnum;
    }

    public BigDecimal getGrinding_timeavgnum() {
        return grinding_timeavgnum;
    }

    public void setGrinding_timeavgnum(BigDecimal grinding_timeavgnum) {
        this.grinding_timeavgnum = grinding_timeavgnum;
    }

    public BigDecimal getGwtotalnum() {
        return gwtotalnum;
    }

    public void setGwtotalnum(BigDecimal gwtotalnum) {
        this.gwtotalnum = gwtotalnum;
    }

    public BigDecimal getFwtotalnum() {
        return fwtotalnum;
    }

    public void setFwtotalnum(BigDecimal fwtotalnum) {
        this.fwtotalnum = fwtotalnum;
    }

    public BigDecimal getRtotalnum() {
        return rtotalnum;
    }

    public void setRtotalnum(BigDecimal rtotalnum) {
        this.rtotalnum = rtotalnum;
    }

    public BigDecimal getRwtotalnum() {
        return rwtotalnum;
    }

    public void setRwtotalnum(BigDecimal rwtotalnum) {
        this.rwtotalnum = rwtotalnum;
    }

    public BigDecimal getOthertotalnum() {
        return othertotalnum;
    }

    public void setOthertotalnum(BigDecimal othertotalnum) {
        this.othertotalnum = othertotalnum;
    }


    public BigDecimal getRates() {
        return rates;
    }

    public void setRates(BigDecimal rates) {
        this.rates = rates;
    }

    public String getSclass() {
        return sclass;
    }

    public void setSclass(String sclass) {
        this.sclass = sclass;
    }

    public String getSgroup() {
        return sgroup;
    }

    public void setSgroup(String sgroup) {
        this.sgroup = sgroup;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public BigDecimal getTotalnum() {
        return totalnum;
    }

    public void setTotalnum(BigDecimal totalnum) {
        this.totalnum = totalnum;
    }

    public BigDecimal getGwnum() {
        return gwnum;
    }

    public void setGwnum(BigDecimal gwnum) {
        this.gwnum = gwnum;
    }

    public BigDecimal getFwnum() {
        return fwnum;
    }

    public void setFwnum(BigDecimal fwnum) {
        this.fwnum = fwnum;
    }

    public BigDecimal getRnum() {
        return rnum;
    }

    public void setRnum(BigDecimal rnum) {
        this.rnum = rnum;
    }

    public BigDecimal getRwnum() {
        return rwnum;
    }

    public void setRwnum(BigDecimal rwnum) {
        this.rwnum = rwnum;
    }

    public BigDecimal getOthernum() {
        return othernum;
    }

    public void setOthernum(BigDecimal othernum) {
        this.othernum = othernum;
    }

    public String getMachineno() {
        return machineno;
    }

    public void setMachineno(String machineno) {
        this.machineno = machineno;
    }

    public String getGrind_starttime() {
        return grind_starttime;
    }

    public void setGrind_starttime(String grind_starttime) {
        this.grind_starttime = grind_starttime;
    }

    public String getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public Double getBefore_diameter() {
        return before_diameter;
    }

    public void setBefore_diameter(Double before_diameter) {
        this.before_diameter = before_diameter;
    }

    public Double getAfter_diameter() {
        return after_diameter;
    }

    public void setAfter_diameter(Double after_diameter) {
        this.after_diameter = after_diameter;
    }

    public Double getWheel_dia_start() {
        return wheel_dia_start;
    }

    public void setWheel_dia_start(Double wheel_dia_start) {
        this.wheel_dia_start = wheel_dia_start;
    }

    public Double getWheel_dia_end() {
        return wheel_dia_end;
    }

    public void setWheel_dia_end(Double wheel_dia_end) {
        this.wheel_dia_end = wheel_dia_end;
    }

    public String getSnote() {
        return snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public String getWheelname() {
        return wheelname;
    }

    public void setWheelname(String wheelname) {
        this.wheelname = wheelname;
    }

    public BigDecimal getSand_article() {
        return sand_article;
    }

    public void setSand_article(BigDecimal sand_article) {
        this.sand_article = sand_article;
    }

    public String getRoll_type() {
        return roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public BigDecimal getGrinding_time() {
        return grinding_time;
    }

    public void setGrinding_time(BigDecimal grinding_time) {
        this.grinding_time = grinding_time;
    }

    public BigDecimal getBody_length() {
        return body_length;
    }

    public void setBody_length(BigDecimal body_length) {
        this.body_length = body_length;
    }

    public BigDecimal getSand_thickness() {
        return sand_thickness;
    }

    public void setSand_thickness(BigDecimal sand_thickness) {
        this.sand_thickness = sand_thickness;
    }

    public BigDecimal getReduction_ratio() {
        return reduction_ratio;
    }

    public void setReduction_ratio(BigDecimal reduction_ratio) {
        this.reduction_ratio = reduction_ratio;
    }

    public BigDecimal getRolldiameter_reduce() {
        return rolldiameter_reduce;
    }

    public void setRolldiameter_reduce(BigDecimal rolldiameter_reduce) {
        this.rolldiameter_reduce = rolldiameter_reduce;
    }

    public BigDecimal getRollerbody_reduce() {
        return rollerbody_reduce;
    }

    public void setRollerbody_reduce(BigDecimal rollerbody_reduce) {
        this.rollerbody_reduce = rollerbody_reduce;
    }

    public BigDecimal getSanddiameter_reduce() {
        return sanddiameter_reduce;
    }

    public void setSanddiameter_reduce(BigDecimal sanddiameter_reduce) {
        this.sanddiameter_reduce = sanddiameter_reduce;
    }

    public BigDecimal getSandbody_reduce() {
        return sandbody_reduce;
    }

    public void setSandbody_reduce(BigDecimal sandbody_reduce) {
        this.sandbody_reduce = sandbody_reduce;
    }

    public BigDecimal getAbrasion_than() {
        return abrasion_than;
    }

    public void setAbrasion_than(BigDecimal abrasion_than) {
        this.abrasion_than = abrasion_than;
    }

    public BigDecimal getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(BigDecimal efficiency) {
        this.efficiency = efficiency;
    }

}