package com.my.business.modular.rollgrinding.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rolldatafiles.service.RollDataFilesService;
import com.my.business.modular.rollgrinding.dao.RollGrindingBFDao;
import com.my.business.modular.rollgrinding.dao.RollGrindingDao;
import com.my.business.modular.rollgrinding.entity.JRollGrinding;
import com.my.business.modular.rollgrinding.entity.JRollGrindingBF;
import com.my.business.modular.rollgrinding.entity.RollGrinding;
import com.my.business.modular.rollgrinding.entity.RollGrindingBF;
import com.my.business.modular.rollgrinding.service.RollGrindingBFService;
import com.my.business.modular.rollgrinding.service.RollGrindingService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;


/**
 * 磨削实绩控制器层
 *
 * @author 生成器生成
 * @date 2020-08-24 17:28:05
 */
@RestController
@RequestMapping("/rollGrindingBF")
public class RollGrindingBFController {

    @Autowired
    private RollGrindingBFService rollGrindingBFService;
    @Autowired
    private RollDataFilesService rollDataFilesService;
    @Autowired
    private RollGrindingBFDao rollGrindingBFDao;
    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollGrindingBF(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollGrindingBFService.insertDataRollGrindingBF(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollGrindingBFOne(@RequestBody String data) {
        try {
            JRollGrindingBF jrollGrindingBF = JSON.parseObject(data, JRollGrindingBF.class);
            return rollGrindingBFService.deleteDataRollGrindingBFOne(jrollGrindingBF.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollGrindingBF jrollGrindingBF = JSON.parseObject(data, JRollGrindingBF.class);
            return rollGrindingBFService.deleteDataRollGrindingBFMany(jrollGrindingBF.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollGrindingBF(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollGrindingBFService.updateDataRollGrindingBF(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollGrindingBFByPage(@RequestBody String data) {
        return rollGrindingBFService.findDataRollGrindingBFByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollGrindingBFByIndocno(@RequestBody String data) {
        return rollGrindingBFService.findDataRollGrindingBFByIndocno(data);
    }

    /**
     * 根据辊号查询记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByNo"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollGrindingBFByNo(@RequestBody String data) {
        return rollGrindingBFService.findDataRollGrindingBFByNo(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollGrindingBF> findDataRollGrindingBF() {
        return rollGrindingBFService.findDataRollGrindingBF();
    }

    /**
     *
     */
    @CrossOrigin
    @RequestMapping(value = {"/test"}, method = RequestMethod.POST)
    @ResponseBody
    public void test() {
        List<RollGrindingBF> rollGrindingBF = rollGrindingBFService.findDataRollGrindingBF();
        for (RollGrindingBF entity: rollGrindingBF){
            Map<String, String> map = rollDataFilesService.AnalysisDifference(entity.getRoll_no(),entity.getGrind_starttime());
            if (!StringUtils.isEmpty(map) && !StringUtils.isEmpty(map.get("合格点数"))) {
                Double count = Double.valueOf(map.get("合格点数"));
                entity.setQualifiednum(count);
                rollGrindingBFDao.updateDataRollGrindingBF(entity);
            }
        }

    }

    /**
     * 分页查看报表 砂轮磨耗统计
     */
    @CrossOrigin
    @RequestMapping(value = {"/findWheelAbrasionByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findWheelAbrasionByPage(@RequestBody String data) {
        return rollGrindingBFService.findWheelAbrasionByPage(data);
    }

    /**
     * 导出 砂轮磨耗统计 表数据
     */
    @CrossOrigin
    @RequestMapping(value = {"/excelWheelAbrasion"}, method = RequestMethod.GET)
    @ResponseBody
    public void getExcelWheelAbrasion(
            @RequestParam ( "machine_no" ) String machine_no,
            @RequestParam ( "wheelname" ) String wheelname,
            @RequestParam ( "dbegin" ) String dbegin,
            @RequestParam ( "dend" ) String dend,
             @RequestParam ( "factory_name" ) String factory_name,
            HttpServletResponse response, HttpServletRequest request) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        String fileName = "";//设置要导出的文件的名字
        fileName = rollGrindingBFService.excelWheelAbrasion(workbook,
                machine_no
        ,wheelname
                ,dbegin
                ,dend
        ,factory_name);
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1) + ".xls");//URLEncoder.encode(fileName, "utf-8")
        workbook.write(response.getOutputStream());
    }


    /**
     * 大屏幕  甲乙丙  KPI
     */
    @CrossOrigin
    @RequestMapping(value = {"/findGrindingRateABigScreenByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findGrindingRateABigScreenByPage(@RequestBody String data) {
        return rollGrindingBFService.findGrindingRateABigScreenByPage(data);
    }

    /**
     * 分页查看报表 磨床稼动率
     */
    @CrossOrigin
    @RequestMapping(value = {"/findGrindingRateByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findGrindingRateByPage(@RequestBody String data) {
        return rollGrindingBFService.findGrindingRateByPage(data);
    }
    /**
     * 导出 磨床稼动率 表数据
     */
    @CrossOrigin
    @RequestMapping(value = {"/excelGrindingRate"}, method = RequestMethod.GET)
    @ResponseBody
    public void getExcelGrindingRate(
            @RequestParam ( "machine_no" ) String machine_no,
            @RequestParam ( "operator" ) String operator,
            @RequestParam ( "dbegin" ) String dbegin,
            @RequestParam ( "dend" ) String dend,
            HttpServletResponse response, HttpServletRequest request) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        String fileName = "";//设置要导出的文件的名字
        fileName = rollGrindingBFService.excelGrindingRate(workbook,
                machine_no
                ,operator
                ,dbegin
                ,dend
                );
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1) + ".xls");//URLEncoder.encode(fileName, "utf-8")
        workbook.write(response.getOutputStream());
    }


    /**
     * 分页查看报表 整台磨床的使用累积时间
     */
    @CrossOrigin
    @RequestMapping(value = {"/findGrindingBFForTotalnum"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findGrindingBFForTotalnum(@RequestBody String data) {
        return rollGrindingBFService.findGrindingBFForTotalnum(data);
    }
    /**
     * 导出 整台磨床的使用累积时间 表数据
     */
    @CrossOrigin
    @RequestMapping(value = {"/excelGrindingBFForTotalnum"}, method = RequestMethod.GET)
    @ResponseBody
    public void getExcelGrindingBFForTotalnum(
            @RequestParam ( "machine_no" ) String machine_no,
            @RequestParam ( "dbegin" ) String dbegin,
            @RequestParam ( "dend" ) String dend,
            HttpServletResponse response, HttpServletRequest request) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        String fileName = "";//设置要导出的文件的名字
        fileName = rollGrindingBFService.excelGrindingBFForTotalnum(workbook, machine_no,dbegin,dend);
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1) + ".xls");//URLEncoder.encode(fileName, "utf-8")
        workbook.write(response.getOutputStream());
    }

}
