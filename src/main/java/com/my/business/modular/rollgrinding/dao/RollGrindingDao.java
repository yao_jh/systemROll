package com.my.business.modular.rollgrinding.dao;

import com.my.business.modular.rollgrinding.entity.RollGrinding;
import com.my.business.restful.entity.Grinder_performent;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 磨削实绩dao接口
 *
 * @author 生成器生成
 * @date 2020-08-24 17:28:05
 */
@Mapper
public interface RollGrindingDao {

    /**
     * 添加记录
     *
     * @param rollGrinding 对象实体
     */
    //void insertDataRollGrinding(RollGrinding rollGrinding);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    //void deleteDataRollGrindingOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    //void deleteDataRollGrindingMany(String value);

    /**
     * 修改记录
     *
     * @param rollGrinding 对象实体
     */
    //void updateDataRollGrinding(RollGrinding rollGrinding);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex       第几页
     * @param pageSize        每页总数
     * @param roll_no         辊号
     * @param roll_typeid     轧辊类型id
     * @param grind_starttime 磨削开始时间
     * @param machine_no      磨床号
     * @return 对象数据集合
     */
    //List<RollGrinding> findDataRollGrindingByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("roll_no") String roll_no, @Param("roll_typeid") Long roll_typeid, @Param("dbegin") String dbegin, @Param("dend") String dend, @Param("machine_no") String machine_no, @Param("grind_starttime") String grind_starttime, @Param("production_line_id") Long production_line_id);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    //Integer findDataRollGrindingByPageSize(@Param("roll_no") String roll_no, @Param("roll_typeid") Long roll_typeid, @Param("dbegin") String dbegin, @Param("dend") String dend, @Param("machine_no") String machine_no, @Param("grind_starttime") String grind_starttime, @Param("production_line_id") Long production_line_id);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    //RollGrinding findDataRollGrindingByIndocno(@Param("indocno") Long indocno);

    /***
     * 根据辊号查询最新一条信息
     * @param roll_no 用户id
     * @return
     */
    //RollGrinding findDataRollGrindingByNoNew(@Param("roll_no") String roll_no);


    /***
     * 根据辊号查询信息
     * @return
     */
    //List<RollGrinding> findDataRollGrindingByNo(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("roll_no") String roll_no);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    //Integer findDataRollGrindingByNoSize(@Param("roll_no") String roll_no);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    //List<RollGrinding> findDataRollGrinding();

    /**
     * 查找重复数量
     *
     * @param value
     * @return
     */
    //Integer findDataHave(@Param("value") String value,@Param("machine_no") String machine_no);

    /**
     * 查找最新的日期时间
     *
     * @return datetime
     */
    //String findTime(@Param("machine_no") String machine_no);
    
    /**
     * 根据辊号查看记录
     *
     * @return 对象数据集合
     */
    //List<Grinder_performent> findRest(@Param("roll_no") String roll_no);

    /**
     * 根据辊号、时间查看查看记录
     *
     * @param roll_no         辊号
     * @param grind_starttime 磨削开始时间
     * @return 对象数据集合
     */
    //RollGrinding findDataRollGrindingforOne(@Param("roll_no") String roll_no, @Param("grind_starttime") String grind_starttime, @Param("machine_no") String machine_no);

    /**
     * 清除PRESET
     */
    //void clear();

    /**
     * 查找上机前最新一条磨削记录
     * @param roll_no 辊号
     * @param onlinetime 上下线
     * @return 数据
     */
    //RollGrinding findNewMax(@Param("roll_no") String roll_no, @Param("onlinetime") String onlinetime);
}
