package com.my.business.modular.rollmxwave.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.rollmxwave.entity.RollMxWave;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 磨削超声波表dao接口
 * @author  生成器生成
 * @date 2020-10-31 15:54:59
 * */
@Mapper
public interface RollMxWaveDao {

	/**
	 * 添加记录
	 * @param rollMxWave  对象实体
	 * */
	public void insertDataRollMxWave(RollMxWave rollMxWave);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataRollMxWaveOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataRollMxWaveMany(String value);
	
	/**
	 * 修改记录
	 * @param rollMxWave  对象实体
	 * */
	public void updateDataRollMxWave(RollMxWave rollMxWave);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<RollMxWave> findDataRollMxWaveByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataRollMxWaveByPageSize();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public RollMxWave findDataRollMxWaveByIndocno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<RollMxWave> findDataRollMxWave();
	
}
