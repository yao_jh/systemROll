package com.my.business.modular.rollmxwave.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.rollmxwave.entity.RollMxWave;
import com.my.business.modular.rollmxwave.entity.JRollMxWave;
import com.my.business.modular.rollmxwave.dao.RollMxWaveDao;
import com.my.business.modular.rollmxwave.service.RollMxWaveService;
import com.my.business.sys.common.entity.ResultData;

/**
* 磨削超声波表接口具体实现类
* @author  生成器生成
* @date 2020-10-31 15:54:59
*/
@Service
public class RollMxWaveServiceImpl implements RollMxWaveService {
	
	@Autowired
	private RollMxWaveDao rollMxWaveDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollMxWave(String data,Long userId,String sname){
		try{
			JRollMxWave jrollMxWave = JSON.parseObject(data,JRollMxWave.class);
    		RollMxWave rollMxWave = jrollMxWave.getRollMxWave();
    		CodiUtil.newRecord(userId,sname,rollMxWave);
            rollMxWaveDao.insertDataRollMxWave(rollMxWave);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollMxWaveOne(Long indocno){
		try {
            rollMxWaveDao.deleteDataRollMxWaveOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollMxWaveMany(String str_id) {
        try {
        	String sql = "delete roll_mx_wave where indocno in(" + str_id +")";
            rollMxWaveDao.deleteDataRollMxWaveMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataRollMxWave(String data,Long userId,String sname){
		try {
			JRollMxWave jrollMxWave = JSON.parseObject(data,JRollMxWave.class);
    		RollMxWave rollMxWave = jrollMxWave.getRollMxWave();
        	CodiUtil.editRecord(userId,sname,rollMxWave);
            rollMxWaveDao.updateDataRollMxWave(rollMxWave);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollMxWaveByPage(String data) {
        try {
        	JRollMxWave jrollMxWave = JSON.parseObject(data, JRollMxWave.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jrollMxWave.getPageIndex();
        	Integer pageSize = jrollMxWave.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jrollMxWave.getCondition()){
    			jsonObject  = JSON.parseObject(jrollMxWave.getCondition().toString());
    		}
    
    		List<RollMxWave> list = rollMxWaveDao.findDataRollMxWaveByPage((pageIndex-1)*pageSize, pageSize);
    		Integer count = rollMxWaveDao.findDataRollMxWaveByPageSize();
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollMxWaveByIndocno(String data) {
        try {
        	JRollMxWave jrollMxWave = JSON.parseObject(data, JRollMxWave.class); 
        	Long indocno = jrollMxWave.getIndocno();
        	
    		RollMxWave rollMxWave = rollMxWaveDao.findDataRollMxWaveByIndocno(indocno);
    		return ResultData.ResultDataSuccess(rollMxWave);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<RollMxWave> findDataRollMxWave(){
		List<RollMxWave> list = rollMxWaveDao.findDataRollMxWave();
		return list;
	};
}
