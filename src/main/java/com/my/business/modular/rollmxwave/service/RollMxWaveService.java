package com.my.business.modular.rollmxwave.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.modular.rollmxwave.entity.RollMxWave;
import java.util.List;

/**
* 磨削超声波表接口服务类
* @author  生成器生成
* @date 2020-10-31 15:54:59
*/
public interface RollMxWaveService {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollMxWave(String data,Long userId,String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollMxWaveOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollMxWaveMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataRollMxWave(String data,Long userId,String sname);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollMxWaveByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataRollMxWaveByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<RollMxWave> findDataRollMxWave();
}
