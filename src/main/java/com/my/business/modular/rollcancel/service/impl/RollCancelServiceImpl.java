package com.my.business.modular.rollcancel.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.rollcancel.entity.RollCancel;
import com.my.business.modular.rollcancel.entity.JRollCancel;
import com.my.business.modular.rollcancel.dao.RollCancelDao;
import com.my.business.modular.rollcancel.service.RollCancelService;
import com.my.business.sys.common.entity.ResultData;

/**
* 取消表接口具体实现类
* @author  生成器生成
* @date 2020-11-23 10:00:26
*/
@Service
public class RollCancelServiceImpl implements RollCancelService {
	
	@Autowired
	private RollCancelDao rollCancelDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollCancel(String data,Long userId,String sname){
		try{
			JRollCancel jrollCancel = JSON.parseObject(data,JRollCancel.class);
    		RollCancel rollCancel = jrollCancel.getRollCancel();
    		CodiUtil.newRecord(userId,sname,rollCancel);
            rollCancelDao.insertDataRollCancel(rollCancel);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollCancelOne(Long indocno){
		try {
            rollCancelDao.deleteDataRollCancelOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollCancelMany(String str_id) {
        try {
        	String sql = "delete roll_cancel where indocno in(" + str_id +")";
            rollCancelDao.deleteDataRollCancelMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataRollCancel(String data,Long userId,String sname){
		try {
			JRollCancel jrollCancel = JSON.parseObject(data,JRollCancel.class);
    		RollCancel rollCancel = jrollCancel.getRollCancel();
        	CodiUtil.editRecord(userId,sname,rollCancel);
            rollCancelDao.updateDataRollCancel(rollCancel);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollCancelByPage(String data) {
        try {
        	JRollCancel jrollCancel = JSON.parseObject(data, JRollCancel.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jrollCancel.getPageIndex();
        	Integer pageSize = jrollCancel.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jrollCancel.getCondition()){
    			jsonObject  = JSON.parseObject(jrollCancel.getCondition().toString());
    		}
    
    		List<RollCancel> list = rollCancelDao.findDataRollCancelByPage((pageIndex-1)*pageSize, pageSize);
    		Integer count = rollCancelDao.findDataRollCancelByPageSize();
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollCancelByIndocno(String data) {
        try {
        	JRollCancel jrollCancel = JSON.parseObject(data, JRollCancel.class); 
        	Long indocno = jrollCancel.getIndocno();
        	
    		RollCancel rollCancel = rollCancelDao.findDataRollCancelByIndocno(indocno);
    		return ResultData.ResultDataSuccess(rollCancel);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<RollCancel> findDataRollCancel(){
		List<RollCancel> list = rollCancelDao.findDataRollCancel();
		return list;
	};
}
