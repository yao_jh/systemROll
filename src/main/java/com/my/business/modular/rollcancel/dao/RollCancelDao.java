package com.my.business.modular.rollcancel.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.rollcancel.entity.RollCancel;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 取消表dao接口
 * @author  生成器生成
 * @date 2020-11-23 10:00:26
 * */
@Mapper
public interface RollCancelDao {

	/**
	 * 添加记录
	 * @param rollCancel  对象实体
	 * */
	public void insertDataRollCancel(RollCancel rollCancel);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataRollCancelOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataRollCancelMany(String value);
	
	/**
	 * 修改记录
	 * @param rollCancel  对象实体
	 * */
	public void updateDataRollCancel(RollCancel rollCancel);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<RollCancel> findDataRollCancelByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataRollCancelByPageSize();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public RollCancel findDataRollCancelByIndocno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<RollCancel> findDataRollCancel();
	
}
