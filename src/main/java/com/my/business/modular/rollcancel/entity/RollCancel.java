package com.my.business.modular.rollcancel.entity;

import java.util.Date;
import com.my.business.sys.common.entity.BaseEntity;

/**
* 取消表实体类
* @author  生成器生成
* @date 2020-11-23 10:00:25
*/
public class RollCancel extends BaseEntity{
		
	private Long indocno;  //主键
	private Long itype;  //数据类型(1 磨削取消  2  磨床故障)
	private String back_reason;  //取消原因
	private String roll_no;  //辊号
    
    
	public void setIndocno(Long indocno){
	    this.indocno = indocno;
	}
	public Long getIndocno(){
	    return this.indocno;
	}
	public void setItype(Long itype){
	    this.itype = itype;
	}
	public Long getItype(){
	    return this.itype;
	}
	public void setBack_reason(String back_reason){
	    this.back_reason = back_reason;
	}
	public String getBack_reason(){
	    return this.back_reason;
	}
	public void setRoll_no(String roll_no){
	    this.roll_no = roll_no;
	}
	public String getRoll_no(){
	    return this.roll_no;
	}

    
}