package com.my.business.modular.rollinformation.service;

import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.modular.rollonoffline.entity.RollOnoffLine;
import com.my.business.sys.common.entity.ResultData;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.List;

/**
 * 轧辊基本信息接口服务类
 *
 * @author 生成器生成
 * @date 2020-07-28 16:12:14
 */
public interface RollInformationService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollInformation(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollInformationOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollInformationMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollInformation(String data, Long userId, String sname);
    
    /**
     * 根据辊号修改轧辊周转状态
     *
     * @param data   json字符串
     */
    ResultData updateDataRollInformationByRevoleve(String data);
    
    /**
     * 根据辊号批量修改轧辊周转状态
     *
     * @param data   json字符串
     */
    ResultData updateDataRollInformationByRevoleveAll(String data);
    
    /**
     * 根据辊号修改轧辊周转状态
     *
     * @param data   json字符串
     */
    ResultData updateDataRollInformationByRevoleveto6(String data);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollInformationByPage(String data);
    
    /**
     * 查看磨床记录
     *
     * @param data 分页参数字符串
     */
    ResultData findMachine(String data);
    
    /**
     * 分页查看已经配对的记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataPairedByPage(String data);
    
    /**
     * 推送消息配对的记录
     *
     * @param data 分页参数字符串
     */
    ResultData pushnewByPage(String data);
    
    /**
     * 查看上机已经配对的记录
     *
     *
     */
    ResultData findDataPairedOnline();

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollInformationByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollInformation> findDataRollInformation();

    /**
     * 备辊专用查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollInformationByPre(String data);
    
    /**
     * 磨削辊子优先级对换
     *
     * @param data 分页参数字符串
     */
    ResultData changeLevel(String data);

    /**
     * 轴承座补油专用查询
     * @param data 分页参数字符串
     * @return 集合
     */
    ResultData findDataByOil(String data);
    
    /**
     * 根据周转状态查询数据集合
     *
     * @param data 参数字符串
     */
    ResultData findDataByRelove(String data);
    
    /**
     * 根据轧辊类型和周转状态查询数据和数量
     *
     * @param data 参数字符串
     */
    ResultData findDataByReloveSize();

    ResultData findDataRollInformationHistoryByPage(String data);

    ResultData findRollDiameterByPage(String data);

    String excelRollDiameter(HSSFWorkbook workbook
            ,String roll_typeid,String framerangeid,String factory_id
            ,String material_id,String dbegin
                             );

    ResultData findRollInformationNum(String data);

    String excelRollInformationNum(HSSFWorkbook workbook);

    ResultData findRollDiameterABigScreenByPage(String data);
}
