package com.my.business.modular.rollinformation.dao;

import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.modular.rollinformation.entity.RollInformationHistory;
import com.my.business.modular.rollinformation.entity.RollInformationReport;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * 轧辊基本信息dao接口
 *
 * @author 生成器生成
 * @date 2020-07-28 16:12:14
 */
@Mapper
public interface RollInformationDao {

    /**
     * 添加记录
     *
     * @param rollInformation 对象实体
     */
    void insertDataRollInformation(RollInformation rollInformation);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollInformationOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollInformationMany(String value);

    /**
     * 修改记录
     *
     * @param rollInformation 对象实体
     */
    void updateDataRollInformation(RollInformation rollInformation);
    
    /**
     * 根据辊号修改轧辊周转状态
     *
     * @param roll_no 辊号
     * @param roll_revolve 状态
     */
    void updateDataRollInformationByRevoleve(@Param("roll_no") String roll_no,@Param("roll_revolve") Long roll_revolve);
    
    /**
     * 根据辊号修改轧辊特殊状态
     *
     */
    void updateDataRollInformationByroll_special(@Param("roll_no") String roll_no,@Param("roll_special") Long roll_special);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollInformation> findDataRollInformationByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("frame_scope_id") String frame_scope_id,@Param("frame_scope") String frame_scope,@Param("factory_id") String factory_id,@Param("material_id") String material_id,@Param("production_line") String production_line,@Param("production_line_id") String production_line_id, @Param("roll_revolve") String roll_revolve, @Param("roll_no") String roll_no, @Param("roll_state") String roll_state, @Param("roll_typeid") Long roll_typeid, @Param("factory") String factory, @Param("material") String material, @Param("inventory_stateid") Long inventory_stateid, @Param("framerangeid") Long framerangeid, @Param("ipaired") Long ipaired, @Param("frame_noid") Long frame_noid, @Param("contract_no") String contract_no, @Param("order_year") String order_year);

    
    /**
     * 根据各个轧辊状态查看轧辊数据(库存状态,周转状态,特殊状态)
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollInformation> findDataByState(@Param("roll_state") String roll_state,@Param("roll_revolve") String roll_revolve,@Param("roll_special") String roll_special);
    
    /**
     * 设备工艺精度用根据各个轧辊状态查看轧辊数据(库存状态,周转状态,特殊状态)
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollInformation> findDataByStateEquip(@Param("roll_state") String roll_state,@Param("roll_revolve") String roll_revolve,@Param("roll_special") String roll_special);
    
    
    
    /**
     * 查看上磨床记录
     *
     * @param roll_revolve 轧辊状态
     * @return 对象数据集合
     */
    List<RollInformation> findMachine(@Param("roll_revolve") String roll_revolve,@Param("machine_id") String machine_id);

    /**
     * 查看已上线的轧机
     *
     * @return 对象数据集合
     */
    List<RollInformation> findDataRollInformationOnline();

    
    
    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollInformationByPageSize(@Param("frame_scope_id") String frame_scope_id,@Param("frame_scope") String frame_scope,@Param("factory_id") String factory_id,@Param("material_id") String material_id,@Param("production_line") String production_line,@Param("production_line_id") String production_line_id,@Param("roll_revolve") String roll_revolve, @Param("roll_no") String roll_no, @Param("roll_state") String roll_state, @Param("roll_typeid") Long roll_typeid, @Param("factory") String factory, @Param("material") String material, @Param("inventory_stateid") Long inventory_stateid, @Param("framerangeid") Long framerangeid, @Param("ipaired") Long ipaired, @Param("frame_noid") Long frame_noid, @Param("contract_no") String contract_no, @Param("order_year") String order_year);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollInformation findDataRollInformationByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollInformation> findDataRollInformation();

    /**
     * 查看记录
     */
    void updatePaired(@Param("indocno") Long indocno);
    
    /**
     * 根据辊号修改信息
     */
    void updatePairedByNo(@Param("roll_no") String roll_no,@Param("ipaired") Long ipaired,@Param("roll_positionid") Long roll_positionid,@Param("roll_position") String roll_position);

    /***
     * 根据辊号查询
     * @param roll_no 辊号
     * @return
     */
    RollInformation findDataRollInformationByRollNo(@Param("roll_no") String roll_no);

    /**
     * 查看记录——刚度分析
     *
     * @return 对象数据集合
     */
    List<RollInformation> findDataRollInformationByMath(@Param("frame_noid") Long frame_noid, @Param("roll_revolve") Long roll_revolve, @Param("production_line_id") Long production_line_id);

    /**
     * 备辊专用查询
     * @param roll_no1 上辊号
     * @param roll_no2 下辊号
     * @return 集合
     */
    List<RollInformation> findDataRollInformationByPre(@Param("roll_no1") String roll_no1, @Param("roll_no2") String roll_no2);

    /**
     * 更新轴承座信息
     * @param os_no OS座号
     * @param ds_no DS座号
     * @param roll_no 辊号
     */
    void updateChockNo(@Param("os_no") String os_no,@Param("ds_no") String ds_no,@Param("bc_no") String bc_no,@Param("roll_no") String roll_no,@Param("sj_no") String sj_no,@Param("sjid_no") Long sjid_no);

    /**
     * 轴承座补油专用查询
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @param production_line_id 产线id
     * @param roll_no 辊号
     * @param framerangeid 机架范围id
     * @return 集合
     */
    List<RollInformation> findDataOilByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("production_line_id") String production_line_id, @Param("roll_no") String roll_no, @Param("framerangeid") Long framerangeid, @Param("workoilcount") Long workoilcount);

    /**
     * 轴承座补油专用查询
     * @param production_line_id 产线id
     * @param roll_no 辊号
     * @return
     */
    Integer findDataOilByPageSize( @Param("production_line_id") String production_line_id, @Param("roll_no") String roll_no, @Param("framerangeid") Long framerangeid, @Param("workoilcount") Long workoilcount);

    /**
     * 更新补油状态
     * @param roll_no 辊号
     */
    void updateOil(@Param("roll_no") String roll_no);
    
    /***
     * 根据周转状态查询轧辊信息
     * @param roll_revolve
     * @return
     */
    List<RollInformation> findDataByRevolve(@Param("roll_revolve") Long roll_revolve,@Param("frame_noid") Long frame_noid,@Param("roll_typeid") Long roll_typeid,@Param("roll_positionid") Long roll_positionid);
    
    /***
     * F1-F4精轧
     * @param roll_revolve
     * @param roll_typeid
     * @return
     */
    Integer findDataByRevolveSizeF1toF4(@Param("roll_revolve") Long roll_revolve,@Param("roll_typeid") Long roll_typeid);
    
    /***
     * F5-F7精轧
     * @param roll_revolve
     * @param roll_typeid
     * @return
     */
    Integer findDataByRevolveSizeF5toF7(@Param("roll_revolve") Long roll_revolve,@Param("roll_typeid") Long roll_typeid);
    
    /***
     * 根据轧辊类型读取数据
     * @param roll_revolve
     * @param roll_typeid
     * @return
     */
    Integer findDataByRevolveSize(@Param("roll_revolve") Long roll_revolve,@Param("roll_typeid") Long roll_typeid);


    RollInformation findSynchronousRollOnoffLineByRollno(@Param("roll_no")String roll_no);

    RollInformation findSynchronousRollGrindingBFByRollno(@Param("roll_no")String roll_no);

    void insertDataRollInformationHistory(RollInformationHistory bean);

    List<RollInformationHistory> findDataRollInformationHistoryByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("frame_scope_id") String frame_scope_id,@Param("frame_scope") String frame_scope,@Param("factory_id") String factory_id,@Param("material_id") String material_id,@Param("production_line") String production_line,@Param("production_line_id") String production_line_id, @Param("roll_revolve") String roll_revolve, @Param("roll_no") String roll_no, @Param("roll_state") String roll_state, @Param("roll_typeid") Long roll_typeid, @Param("factory") String factory, @Param("material") String material, @Param("inventory_stateid") Long inventory_stateid, @Param("framerangeid") Long framerangeid, @Param("ipaired") Long ipaired, @Param("frame_noid") Long frame_noid, @Param("contract_no") String contract_no, @Param("order_year") String order_year,@Param("dbegin") String dbegin,@Param("dend") String dend);

    Integer findDataRollInformationHistoryByPageSize(@Param("frame_scope_id") String frame_scope_id,@Param("frame_scope") String frame_scope,@Param("factory_id") String factory_id,@Param("material_id") String material_id,@Param("production_line") String production_line,@Param("production_line_id") String production_line_id,@Param("roll_revolve") String roll_revolve, @Param("roll_no") String roll_no, @Param("roll_state") String roll_state, @Param("roll_typeid") Long roll_typeid, @Param("factory") String factory, @Param("material") String material, @Param("inventory_stateid") Long inventory_stateid, @Param("framerangeid") Long framerangeid, @Param("ipaired") Long ipaired, @Param("frame_noid") Long frame_noid, @Param("contract_no") String contract_no, @Param("order_year") String order_year,@Param("dbegin") String dbegin,@Param("dend") String dend);

    List<RollInformation> findRollDiameterByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("roll_typeid") String roll_typeid, @Param("framerangeid") String framerangeid, @Param("factory_id") String factory_id, @Param("material_id") String material_id, @Param("dbegin") String dbegin);

    BigDecimal findRollDiameterByPageSize( @Param("roll_typeid") String roll_typeid, @Param("framerangeid") String framerangeid, @Param("factory_id") String factory_id, @Param("material_id") String material_id, @Param("dbegin") String dbegin);

    List<RollInformationReport> findRollInformationNum();

    List<RollInformation> findRollDiameterABigScreenByPage(@Param("roll_typeid") String roll_typeid, @Param("framerangeid") String framerangeid, @Param("factory_id") String factory_id, @Param("material_id") String material_id, @Param("dbegin") String dbegin,@Param("ifabnormal") String ifabnormal);
}
