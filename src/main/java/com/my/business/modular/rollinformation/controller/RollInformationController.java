package com.my.business.modular.rollinformation.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.aop.Log;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.JRollInformation;
import com.my.business.modular.rollinformation.entity.RollChange;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.modular.rollinformation.entity.Rolllist;
import com.my.business.modular.rollinformation.service.RollInformationService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;


/**
 * 轧辊基本信息控制器层
 *
 * @author 生成器生成
 * @date 2020-07-28 16:12:14
 */
@RestController
@RequestMapping("/rollInformation")
public class RollInformationController {

    @Autowired
    private RollInformationService rollInformationService;
    
    @Autowired
    private RollInformationDao  dao;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollInformation(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollInformationService.insertDataRollInformation(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollInformationOne(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        try {
            JRollInformation jrollInformation = JSON.parseObject(data, JRollInformation.class);
            return rollInformationService.deleteDataRollInformationOne(jrollInformation.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollInformation jrollInformation = JSON.parseObject(data, JRollInformation.class);
            return rollInformationService.deleteDataRollInformationMany(jrollInformation.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollInformation(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollInformationService.updateDataRollInformation(data, userId, CodiUtil.returnLm(sname));
    }
    
    /**
     * 根据辊号修改轧辊周转状态
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/updaterevoleve"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollInformationByRevoleve(@RequestBody String data) {
        return rollInformationService.updateDataRollInformationByRevoleve(data);
    }
    
    /**
     * 根据辊号批量修改轧辊周转状态
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/updaterevoleveAll"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollInformationByRevoleveAll(@RequestBody String data) {
        return rollInformationService.updateDataRollInformationByRevoleveAll(data);
    }
    
    /**
     * 根据辊号修改轧辊周转状态
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/updaterevoleveto6"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollInformationByRevoleveto6(@RequestBody String data) {
        return rollInformationService.updateDataRollInformationByRevoleveto6(data);
    }
    
    /**
     * 根据辊号修改轧辊周转状态
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/chen"}, method = RequestMethod.POST)
    @ResponseBody
    public void chen(@RequestBody String data) {
    	System.out.println(data);
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollInformationByPage(@RequestBody String data) {
        return rollInformationService.findDataRollInformationByPage(data);
    }

    /**
     * 查看上磨床记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findMachine"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findMachine(@RequestBody String data) {
        return rollInformationService.findMachine(data);
    }
    
    /**
     * 分页查看已配对记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findPairedByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findPairedByPage(@RequestBody String data) {
        return rollInformationService.findDataPairedByPage(data);
    }
    
    /**
     * 推送消息查看已配对记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/pushnewByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData pushnewByPage(@RequestBody String data) {
        return rollInformationService.pushnewByPage(data);
    }
    
    /**
     * 分页查看在线轧辊记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findOnline"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findPairedOnlie(@RequestBody String data) {
        return rollInformationService.findDataPairedOnline();
    }
    
    /**
     * 根据轧辊周转状态查询数据
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findRevolve"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findRevolve(@RequestBody String data) {
        return rollInformationService.findDataByRelove(data);
    }
    
    /**
     * 根据轧辊类型查询各个周转状态的数字
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findSizeByType"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findSizeByType(@RequestBody String data) {
        return rollInformationService.findDataByReloveSize();
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollInformationByIndocno(@RequestBody String data) {
        return rollInformationService.findDataRollInformationByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollInformation> findDataRollInformation() {
        return rollInformationService.findDataRollInformation();
    }

    /**
     * 备辊专用查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPre"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollInformationByPre(@RequestBody String data) {
        return rollInformationService.findDataRollInformationByPre(data);
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByOil"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataOilByPage(@RequestBody String data) {
        return rollInformationService.findDataByOil(data);
    }
    
    /**
     * 更换磨床优先级
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/changelevel"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData changelevel(@RequestBody String data) {
        return rollInformationService.changeLevel(data);
    }
    
    /**
     * 更换磨床优先级临时
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/changemc"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData changemc(@RequestBody String data) {
    	try {
    		Rolllist r = JSON.parseObject(data, Rolllist.class);
        	for(RollInformation ra : r.getRoll_list()) {
        		dao.updateDataRollInformation(ra);
        	}
            return ResultData.ResultDataSuccess("更换成功");
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("更换失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findHistoryByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollInformationHistoryByPage(@RequestBody String data) {
        return rollInformationService.findDataRollInformationHistoryByPage(data);
    }

    /**
     * 各月轧辊辊径表
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findRollDiameterByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findRollDiameterByPage(@RequestBody String data) {
        return rollInformationService.findRollDiameterByPage(data);
    }

    /**
     * 大屏幕 各月轧辊辊径表
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findRollDiameterABigScreenByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findRollDiameterABigScreenByPage(@RequestBody String data) {
        return rollInformationService.findRollDiameterABigScreenByPage(data);
    }

    /**
     * 导出 各月轧辊辊径表
     */
    @CrossOrigin
    @RequestMapping(value = {"/excelRollDiameter"}, method = RequestMethod.GET)
    @ResponseBody
    public void getExcelRollDiameter(
            @RequestParam ( "roll_typeid" ) String roll_typeid,
            @RequestParam ( "framerangeid" ) String framerangeid,
            @RequestParam ( "factory_id" ) String factory_id,
            @RequestParam ( "material_id" ) String material_id,
            @RequestParam ( "dbegin" ) String dbegin,
            HttpServletResponse response, HttpServletRequest request) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        String fileName = "";//设置要导出的文件的名字
        fileName = rollInformationService.excelRollDiameter(workbook
                , roll_typeid,framerangeid,factory_id
            ,material_id,dbegin
        );
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1) + ".xls");//URLEncoder.encode(fileName, "utf-8")
        workbook.write(response.getOutputStream());
    }

    /**
     * 轧辊数量统计
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findRollInformationNum"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findRollInformationNum(@RequestBody String data) {
        return rollInformationService.findRollInformationNum(data);
    }

    /**
     * 轧辊数量导出
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/excelRollInformationNum"}, method = RequestMethod.GET)
    @ResponseBody
    public void getExcelRollInformationNum(HttpServletResponse response, HttpServletRequest request) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        String fileName = "";//设置要导出的文件的名字
        fileName = rollInformationService.excelRollInformationNum(workbook);
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1) + ".xls");//URLEncoder.encode(fileName, "utf-8")
        workbook.write(response.getOutputStream());
    }

}
