package com.my.business.modular.rollinformation.entity;

import java.util.List;

public class Rolllist {
	List<RollInformation> roll_list;

	public List<RollInformation> getRoll_list() {
		return roll_list;
	}

	public void setRoll_list(List<RollInformation> roll_list) {
		this.roll_list = roll_list;
	}

}
