package com.my.business.modular.rollinformation.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.dictionary.service.DictionaryService;
import com.my.business.modular.rollgrinding.entity.RollGrindingBFReport;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.*;
import com.my.business.modular.rollinformation.service.RollInformationService;
import com.my.business.modular.rollonoffline.entity.RollOnoffLine;
import com.my.business.modular.rollpaired.dao.RollPairedDao;
import com.my.business.modular.rollpaired.entity.RollPaired;
import com.my.business.modular.rollwear.service.impl.RollWearServiceImpl;
import com.my.business.sys.common.entity.MapXYEntity;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.util.DateUtil;
import io.swagger.models.auth.In;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigDecimal;
import java.util.*;

/**
 * 轧辊基本信息接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-07-28 16:12:14
 */
@Service
public class RollInformationServiceImpl implements RollInformationService {
	
	private final static Log logger = LogFactory.getLog(RollInformationServiceImpl.class);

    @Autowired
    private RollInformationDao rollInformationDao;

    @Autowired
    private RollPairedDao rollPairedDao;
    
    @Autowired
    private DictionaryService dictionaryService;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollInformation(String data, Long userId, String sname) {
        try {
            JRollInformation jrollInformation = JSON.parseObject(data, JRollInformation.class);
            RollInformation rollInformation = jrollInformation.getRollInformation();
            CodiUtil.newRecord(userId, sname, rollInformation);
            if(!StringUtils.isEmpty(rollInformation.getRoll_no())&&rollInformationDao.findDataRollInformationByRollNo(rollInformation.getRoll_no())!=null) {
                return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + "重复辊号", null);
            }
            rollInformationDao.insertDataRollInformation(rollInformation);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollInformationOne(Long indocno) {
        try {
            rollInformationDao.deleteDataRollInformationOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollInformationMany(String str_id) {
        try {
            String sql = "delete from roll_information where indocno in(" + str_id + ")";
            rollInformationDao.deleteDataRollInformationMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollInformation(String data, Long userId, String sname) {
        try {
            JRollInformation jrollInformation = JSON.parseObject(data, JRollInformation.class);
            RollInformation rollInformation = jrollInformation.getRollInformation();
            CodiUtil.editRecord(userId, sname, rollInformation);
            //存储 报废轧辊 历史记录
            RollInformationHistory bean = new RollInformationHistory();
            BeanUtils.copyProperties(rollInformation,bean);
            if(rollInformation.getRoll_state()==6||rollInformation.getRoll_state()==6L){
                rollInformation.setScrapdate(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));
                bean.setOperate_time(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));
                bean.setOperate_name("报废");
                rollInformationDao.insertDataRollInformationHistory(bean);
            }else  if(rollInformation.getRoll_state()==7||rollInformation.getRoll_state()==7L){
                bean.setOperate_time(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));
                bean.setOperate_name("事故");
                rollInformationDao.insertDataRollInformationHistory(bean);
            }
            rollInformationDao.updateDataRollInformation(rollInformation);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollInformationByPage(String data) {
        try {
            JRollInformation jrollInformation = JSON.parseObject(data, JRollInformation.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollInformation.getPageIndex();
            Integer pageSize = jrollInformation.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollInformation.getCondition()) {
                jsonObject = JSON.parseObject(jrollInformation.getCondition().toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

            Long roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = Long.valueOf(jsonObject.get("roll_typeid").toString());
            }

            String roll_state = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_state"))) {
                roll_state = jsonObject.get("roll_state").toString();
            }

            String factory = null;
            if (!StringUtils.isEmpty(jsonObject.get("factory"))) {
                factory = jsonObject.get("factory").toString();
            }

            String material = null;
            if (!StringUtils.isEmpty(jsonObject.get("material"))) {
                material = jsonObject.get("material").toString();
            }

            Long inventory_stateid = null;
            if (!StringUtils.isEmpty(jsonObject.get("inventory_stateid"))) {
                inventory_stateid = Long.valueOf(jsonObject.get("inventory_stateid").toString());
            }

            Long framerangeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("framerangeid"))) {
                framerangeid = Long.valueOf(jsonObject.get("framerangeid").toString());
            }

            Long ipaired = null;
            if (!StringUtils.isEmpty(jsonObject.get("ipaired"))) {
                ipaired = Long.valueOf(jsonObject.get("ipaired").toString());
            }

            Long frame_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
                frame_noid = Long.valueOf(jsonObject.get("frame_noid").toString());
            }

            String frame_scope = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_scope"))) {
                frame_scope = jsonObject.get("frame_scope").toString();
            }

            String frame_scope_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_scope_id"))) {
                frame_scope_id = jsonObject.get("frame_scope_id").toString();
            }

            String roll_revolve = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_revolve"))) {
                roll_revolve = jsonObject.get("roll_revolve").toString();
            }

            String contract_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("contract_no"))) {
                contract_no = jsonObject.get("contract_no").toString();
            }

            String production_line = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line"))) {
                production_line = jsonObject.get("production_line").toString();
            }

            String production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = jsonObject.get("production_line_id").toString();
            }

            String order_year = null;
            if (!StringUtils.isEmpty(jsonObject.get("order_year"))) {
                order_year = jsonObject.get("order_year").toString();
            }

            String factory_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("factory_id"))) {
                factory_id = jsonObject.get("factory_id").toString();
            }

            String material_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("material_id"))) {
                material_id = jsonObject.get("material_id").toString();
            }

            List<RollInformation> list = rollInformationDao.findDataRollInformationByPage((pageIndex - 1) * pageSize, pageSize, frame_scope_id, frame_scope, factory_id, material_id, production_line, production_line_id, roll_revolve, roll_no, roll_state, roll_typeid, factory, material, inventory_stateid, framerangeid, ipaired, frame_noid, contract_no, order_year);
            for(RollInformation r : list) {
            	if(!StringUtils.isEmpty(r.getRoll_state())) {
            		r.setRoll_state_value(dictionaryService.findDataByV1("rollstate", 1L, r.getRoll_state()));
            	}
            	if(!StringUtils.isEmpty(r.getRoll_revolve())) {
            		r.setRoll_revolve_value(dictionaryService.findDataByV1("rollrevolve", 1L, r.getRoll_revolve()));
            	}
            	if(!StringUtils.isEmpty(r.getRoll_special())) {
            		r.setRoll_special_value(dictionaryService.findDataByV1("rollspecial", 1L, r.getRoll_special()));
            	}
            }
            Integer count = rollInformationDao.findDataRollInformationByPageSize(frame_scope_id, frame_scope, factory_id, material_id, production_line, production_line_id, roll_revolve, roll_no, roll_state, roll_typeid, factory, material, inventory_stateid, framerangeid, ipaired, frame_noid, contract_no, order_year);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看上磨床记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findMachine(String data) {
        try {
            JRollInformation jrollInformation = JSON.parseObject(data, JRollInformation.class);
            JSONObject jsonObject = null;
            if (null != jrollInformation.getCondition()) {
                jsonObject = JSON.parseObject(jrollInformation.getCondition().toString());
            }


            String roll_revolve = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_revolve"))) {
                roll_revolve = jsonObject.get("roll_revolve").toString();
            }
            String machine_id = jsonObject.get("machine_id").toString();

            List<RollInformation> list = rollInformationDao.findMachine(roll_revolve,machine_id);
            Integer count = 0;
            if (list != null && list.size() > 0) {
                count = list.size();
            }
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollInformationByIndocno(String data) {
        try {
            JRollInformation jrollInformation = JSON.parseObject(data, JRollInformation.class);
            Long indocno = jrollInformation.getIndocno();

            RollInformation rollInformation = rollInformationDao.findDataRollInformationByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollInformation);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollInformation> findDataRollInformation() {
        List<RollInformation> list = rollInformationDao.findDataRollInformation();
        return list;
    }

    /**
     * 分页查看已经配对的记录
     *
     * @param data 分页参数字符串
     */
    @Override
    public ResultData findDataPairedByPage(String data) {
        try {
            JRollInformation jrollInformation = JSON.parseObject(data, JRollInformation.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollInformation.getPageIndex();
            Integer pageSize = jrollInformation.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollInformation.getCondition()) {
                jsonObject = JSON.parseObject(jrollInformation.getCondition().toString());
            }else{
                jsonObject = new JSONObject();
            }

            String production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = jsonObject.get("production_line_id").toString();
            }
            String factory = null;
            if (!StringUtils.isEmpty(jsonObject.get("factory"))) {
                factory = jsonObject.get("factory").toString();
            }

            String material = null;
            if (!StringUtils.isEmpty(jsonObject.get("material"))) {
                material = jsonObject.get("material").toString();
            }
            String roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = jsonObject.get("roll_typeid").toString();
            }

            String roll_revolve = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_revolve"))) {
                roll_revolve = jsonObject.get("roll_revolve").toString();
            }
            
            String frame_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
            	frame_noid = jsonObject.get("frame_noid").toString();
            }
            
            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
            	roll_no = jsonObject.get("roll_no").toString();
            }
            
            List<RollPaired> pairedList = new ArrayList<RollPaired>();
            
            if(!StringUtils.isEmpty(roll_no)) {
            	/*RollPaired r = rollPairedDao.findDataRollPairedByRollUpOther(roll_no, roll_typeid, material,frame_noid);
            	RollPaired r_new = new RollPaired();
            	if(r != null) {
            		pairedList.add(r);
            	}else {
            		r_new = rollPairedDao.findDataRollPairedByRollDownOther(roll_no, roll_typeid, material,frame_noid);
            		pairedList.add(r_new);
            	}*/
                pairedList = rollPairedDao.findDataRollPairedByPage((pageIndex - 1) * pageSize, pageSize, roll_revolve, production_line_id, roll_typeid, factory, material,frame_noid,roll_no);

            }else {
            	pairedList = rollPairedDao.findDataRollPairedByPage((pageIndex - 1) * pageSize, pageSize, roll_revolve, production_line_id, roll_typeid, factory, material,frame_noid,null);
            }

            List<RollInformation> list = new ArrayList<RollInformation>();
            for (RollPaired entity : pairedList) {
            	RollInformation r_up = rollInformationDao.findDataRollInformationByIndocno(entity.getIlinkno_up());//上辊
            	RollInformation r_down = rollInformationDao.findDataRollInformationByIndocno(entity.getIlinkno_down()); //下辊
                Double num = 0d;
                //tofixed(after_diameter, 3)
                if(r_up.getCurrentdiameter()!=null){
                    num+=r_up.getCurrentdiameter();
                }
                if(r_down.getCurrentdiameter()!=null){
                    num-=r_down.getCurrentdiameter();
                }
                r_up.setNeck_diameter(RollWearServiceImpl.tofixed(num+"", 3));
                r_down.setNeck_diameter(RollWearServiceImpl.tofixed(num+"", 3));
                if(entity.getFrame_noid()!=null&&entity.getFrame_no()!=null){
                    r_up.setFrame_no(entity.getFrame_no());
                    r_down.setFrame_no(entity.getFrame_no());
                    r_up.setFrame_noid(entity.getFrame_noid());
                    r_down.setFrame_noid(entity.getFrame_noid());
                }

                if(r_up.getRoll_state() != 6) {  //报废辊过滤
            		list.add(r_up);
            	}
            	if(r_down.getRoll_state() != 6) {  //报废辊过滤
            		list.add(r_down);
            	}
//                list.add(rollInformationDao.findDataRollInformationByIndocno(entity.getIlinkno_up()));//上辊
//                list.add(rollInformationDao.findDataRollInformationByIndocno(entity.getIlinkno_down()));//下辊
            }
            
            for(RollInformation r : list) {
            	if(!StringUtils.isEmpty(r.getRoll_state())) {
            		r.setRoll_state_value(dictionaryService.findDataByV1("rollstate", 1L, r.getRoll_state()));
            	}
            	if(!StringUtils.isEmpty(r.getRoll_revolve())) {
            		r.setRoll_revolve_value(dictionaryService.findDataByV1("rollrevolve", 1L, r.getRoll_revolve()));
            	}
            	if(!StringUtils.isEmpty(r.getRoll_special())) {
            		r.setRoll_special_value(dictionaryService.findDataByV1("rollspecial", 1L, r.getRoll_special()));
            	}
            }
            int count = (rollPairedDao.findDataRollPaired(roll_revolve, production_line_id, roll_typeid, factory, material,frame_noid).size()) * 2;
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }


    /**
     * 推送消息配对的记录
     *
     * @param data 分页参数字符串
     */
    @Override
    public ResultData pushnewByPage(String data) {
        try {
        	RollInformation rollInformation = JSON.parseObject(data, RollInformation.class);
        	String[] roll_no_array = rollInformation.getRoll_no().split(",");
        	
        	List<RollPaired> pairedList = new ArrayList<RollPaired>();
        	for(String roll_nb : roll_no_array) {
        		RollPaired p = rollPairedDao.findDataRollPairedByRollUp(roll_nb);
        		if(p != null) {
        			pairedList.add(p);
        		}
        	}
            List<RollInformation> list = new ArrayList<RollInformation>();
            for (RollPaired entity : pairedList) {
                list.add(rollInformationDao.findDataRollInformationByIndocno(entity.getIlinkno_up()));//上辊
                list.add(rollInformationDao.findDataRollInformationByIndocno(entity.getIlinkno_down()));//下辊
            }
            return ResultData.ResultDataSuccess(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看已经配对的记录
     *
     * @param
     */
    @Override
    public ResultData findDataPairedOnline() {
        try {
            List<RollInformation> list = new ArrayList<RollInformation>();
            List<RollInformation> find_list = rollInformationDao.findDataRollInformationOnline();  //上机轧辊集合

            for (RollInformation entity : find_list) {
                //拿每一个数据先判断是适配对辊中的上辊还是下辊
                //上辊
                RollPaired uppair = rollPairedDao.findDataRollPairedByRollUp(entity.getRoll_no());   //根据辊号查询上辊
                RollPaired downpair = rollPairedDao.findDataRollPairedByRollDown(entity.getRoll_no());   //根据辊号查询下辊
                if (uppair != null) {   //如果上辊条件查询的结果不为空，则该辊是上辊，先加入list集合，然后根据上辊配对信息的下辊辊号查出轧辊信息加入list
                    RollInformation uproll = rollInformationDao.findDataRollInformationByRollNo(uppair.getRoll_no_up());
                    RollInformation downuproll = rollInformationDao.findDataRollInformationByRollNo(uppair.getRoll_no_down());
                    list.add(uproll);
                    list.add(downuproll);
                }
            }
            return ResultData.ResultDataSuccess(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }


    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollInformationByPre(String data) {
        try {
            JRollInformation jrollInformation = JSON.parseObject(data, JRollInformation.class);
            JSONObject jsonObject = null;

            if (null != jrollInformation.getCondition()) {
                jsonObject = JSON.parseObject(jrollInformation.getCondition().toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
                List<String> roll_nos = Arrays.asList(roll_no.split("/"));
                String roll_no1 = roll_nos.get(0);  //获取上辊辊号
                String roll_no2 = roll_nos.get(1);  //获取下辊辊号
                List<RollInformation> list = rollInformationDao.findDataRollInformationByPre(roll_no1, roll_no2);
                return ResultData.ResultDataSuccess(list);
            } else {
                return ResultData.ResultDataSuccess("暂无数据");
            }

        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    @Override
    public ResultData updateDataRollInformationByRevoleve(String data) {
        try {
            RollInformation rollInformation = JSON.parseObject(data, RollInformation.class);
            System.out.println("辊号=" + rollInformation.getRoll_no() + ",周状态=" + rollInformation.getRoll_revolve());
            logger.debug("辊号=" + rollInformation.getRoll_no() + ",周状态=" + rollInformation.getRoll_revolve());
            rollInformationDao.updateDataRollInformationByRevoleve(rollInformation.getRoll_no(), rollInformation.getRoll_revolve());
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }
    
    @Override
    public ResultData updateDataRollInformationByRevoleveAll(String data) {
        try {
        	JRollInformation jrollInformation = JSON.parseObject(data, JRollInformation.class);
            logger.debug("辊号集合=" + jrollInformation.getStr_indocno());
            String[] roll_no_array = jrollInformation.getStr_indocno().split(",");
            Long roll_revolve = jrollInformation.getIndocno();
            for(String roll_no : roll_no_array) {
            	rollInformationDao.updateDataRollInformationByRevoleve(roll_no,roll_revolve);
            }
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }
    
    @Override
    public ResultData updateDataRollInformationByRevoleveto6(String data) {
        try {
            RollInformation rollInformation = JSON.parseObject(data, RollInformation.class);
            logger.debug("6辊号=" + rollInformation.getRoll_no() + ",6周状态=" + rollInformation.getRoll_revolve());
            System.out.println("6辊号=" + rollInformation.getRoll_no() + ",6周状态=" + rollInformation.getRoll_revolve());
            rollInformationDao.updateDataRollInformationByRevoleve(rollInformation.getRoll_no(), 6L);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 轴承座补油专用查询
     * @param data 分页参数字符串
     * @return
     */
    public ResultData findDataByOil(String data) {
        try {
            JRollInformation jrollInformation = JSON.parseObject(data, JRollInformation.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollInformation.getPageIndex();
            Integer pageSize = jrollInformation.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollInformation.getCondition()) {
                jsonObject = JSON.parseObject(jrollInformation.getCondition().toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

            String production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = jsonObject.get("production_line_id").toString();
            }

            List<RollInformation> list = new ArrayList<>();
            List<RollInformation> list1 = rollInformationDao.findDataOilByPage((pageIndex - 1) * pageSize, pageSize, production_line_id, roll_no, 1L, 3L);
            List<RollInformation> list2 = rollInformationDao.findDataOilByPage((pageIndex - 1) * pageSize, pageSize, production_line_id, roll_no, 2L, 2L);
            list.addAll(list1);
            list.addAll(list2);
            Integer count1 = rollInformationDao.findDataOilByPageSize(production_line_id, roll_no, 1L, 3L);
            Integer count2 = rollInformationDao.findDataOilByPageSize(production_line_id, roll_no, 2L, 2L);
            Integer count = count1+count2;
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

	@Override
	public ResultData changeLevel(String data) {
		try {
            RollChange r = JSON.parseObject(data, RollChange.class);
            RollInformation a = rollInformationDao.findDataRollInformationByRollNo(r.getA_roll());
            RollInformation b = rollInformationDao.findDataRollInformationByRollNo(r.getB_roll());
            a.setIlevel(r.getB_level()); b.setIlevel(r.getA_level());
            
            rollInformationDao.updateDataRollInformation(a);
            rollInformationDao.updateDataRollInformation(b);
            return ResultData.ResultDataSuccess("更换成功");
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("更换失败,失败信息为" + e.getMessage(), null);
        }
	}

	@Override
	public ResultData findDataByRelove(String data) {
		try {
            RollInformation a = JSON.parseObject(data, RollInformation.class);
            
            List<RollInformation> list = rollInformationDao.findDataByRevolve(a.getRoll_revolve(),a.getFrame_noid(),a.getRoll_typeid(),a.getRoll_positionid());
            return ResultData.ResultDataSuccess(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
	}
	
	@Override
	public ResultData findDataByReloveSize() {
		try {
			List<RollBoard> board_list = new ArrayList<RollBoard>();
			//F1~F4精轧工作辊
			RollBoard r1 = new RollBoard();
			r1.setRoll_type("F1~F4精轧工作辊");
			r1.setZmx(rollInformationDao.findDataByRevolveSizeF1toF4(1L, 1L));
			r1.setDmx(rollInformationDao.findDataByRevolveSizeF1toF4(6L, 1L));
        	r1.setZlq(rollInformationDao.findDataByRevolveSizeF1toF4(2L, 1L));
        	r1.setZzp(rollInformationDao.findDataByRevolveSizeF1toF4(8L, 1L));
        	board_list.add(r1);
        	
        	//F5~F7精轧工作辊
			RollBoard r2 = new RollBoard();
			r2.setRoll_type("F5~F7精轧工作辊");
			r2.setZmx(rollInformationDao.findDataByRevolveSizeF5toF7(1L, 1L));
			r2.setDmx(rollInformationDao.findDataByRevolveSizeF5toF7(6L, 1L));
        	r2.setZlq(rollInformationDao.findDataByRevolveSizeF5toF7(2L, 1L));
        	r2.setZzp(rollInformationDao.findDataByRevolveSizeF5toF7(8L, 1L));
        	board_list.add(r2);
        	
        	//精轧支撑辊
			RollBoard r3 = new RollBoard();
			r3.setRoll_type("精轧支撑辊");
			r3.setZmx(rollInformationDao.findDataByRevolveSize(1L, 2L));
			r3.setDmx(rollInformationDao.findDataByRevolveSize(6L, 2L));
        	r3.setZlq(rollInformationDao.findDataByRevolveSize(2L, 2L));
        	r3.setZzp(rollInformationDao.findDataByRevolveSize(8L, 2L));
        	board_list.add(r3);
        	
        	//精轧立棍
			RollBoard r4 = new RollBoard();
			r4.setRoll_type("精轧立棍");
			r4.setZmx(rollInformationDao.findDataByRevolveSize(1L, 5L));
			r4.setDmx(rollInformationDao.findDataByRevolveSize(6L, 5L));
        	r4.setZlq(rollInformationDao.findDataByRevolveSize(2L, 5L));
        	r4.setZzp(rollInformationDao.findDataByRevolveSize(8L, 5L));
        	board_list.add(r4);
        	
        	//粗轧工作辊
			RollBoard r5 = new RollBoard();
			r5.setRoll_type("粗轧工作辊");
			r5.setZmx(rollInformationDao.findDataByRevolveSize(1L, 3L));
			r5.setDmx(rollInformationDao.findDataByRevolveSize(6L, 3L));
        	r5.setZlq(rollInformationDao.findDataByRevolveSize(2L, 3L));
        	r5.setZzp(rollInformationDao.findDataByRevolveSize(8L, 3L));
        	board_list.add(r5);
        	
        	//粗轧支撑辊
			RollBoard r6 = new RollBoard();
			r6.setRoll_type("粗轧支撑辊");
			r6.setZmx(rollInformationDao.findDataByRevolveSize(1L, 4L));
			r6.setDmx(rollInformationDao.findDataByRevolveSize(6L, 4L));
        	r6.setZlq(rollInformationDao.findDataByRevolveSize(2L, 4L));
        	r6.setZzp(rollInformationDao.findDataByRevolveSize(8L, 4L));
        	board_list.add(r6);
        	
        	//粗轧立棍
			RollBoard r7 = new RollBoard();
			r7.setRoll_type("粗轧立棍");
			r7.setZmx(rollInformationDao.findDataByRevolveSize(1L, 6L));
			r7.setDmx(rollInformationDao.findDataByRevolveSize(6L, 6L));
        	r7.setZlq(rollInformationDao.findDataByRevolveSize(2L, 6L));
        	r7.setZzp(rollInformationDao.findDataByRevolveSize(8L, 6L));
        	board_list.add(r7);
            
            return ResultData.ResultDataSuccess(board_list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
	}

    @Override
    public ResultData findDataRollInformationHistoryByPage(String data) {
        try {
            JRollInformation jrollInformation = JSON.parseObject(data, JRollInformation.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollInformation.getPageIndex();
            Integer pageSize = jrollInformation.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollInformation.getCondition()) {
                jsonObject = JSON.parseObject(jrollInformation.getCondition().toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

            Long roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = Long.valueOf(jsonObject.get("roll_typeid").toString());
            }

            String roll_state = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_state"))) {
                roll_state = jsonObject.get("roll_state").toString();
            }

            String factory = null;
            if (!StringUtils.isEmpty(jsonObject.get("factory"))) {
                factory = jsonObject.get("factory").toString();
            }

            String material = null;
            if (!StringUtils.isEmpty(jsonObject.get("material"))) {
                material = jsonObject.get("material").toString();
            }

            Long inventory_stateid = null;
            if (!StringUtils.isEmpty(jsonObject.get("inventory_stateid"))) {
                inventory_stateid = Long.valueOf(jsonObject.get("inventory_stateid").toString());
            }

            Long framerangeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("framerangeid"))) {
                framerangeid = Long.valueOf(jsonObject.get("framerangeid").toString());
            }

            Long ipaired = null;
            if (!StringUtils.isEmpty(jsonObject.get("ipaired"))) {
                ipaired = Long.valueOf(jsonObject.get("ipaired").toString());
            }

            Long frame_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
                frame_noid = Long.valueOf(jsonObject.get("frame_noid").toString());
            }

            String frame_scope = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_scope"))) {
                frame_scope = jsonObject.get("frame_scope").toString();
            }

            String frame_scope_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_scope_id"))) {
                frame_scope_id = jsonObject.get("frame_scope_id").toString();
            }

            String roll_revolve = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_revolve"))) {
                roll_revolve = jsonObject.get("roll_revolve").toString();
            }

            String contract_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("contract_no"))) {
                contract_no = jsonObject.get("contract_no").toString();
            }

            String production_line = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line"))) {
                production_line = jsonObject.get("production_line").toString();
            }

            String production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = jsonObject.get("production_line_id").toString();
            }

            String order_year = null;
            if (!StringUtils.isEmpty(jsonObject.get("order_year"))) {
                order_year = jsonObject.get("order_year").toString();
            }

            String factory_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("factory_id"))) {
                factory_id = jsonObject.get("factory_id").toString();
            }

            String material_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("material_id"))) {
                material_id = jsonObject.get("material_id").toString();
            }
            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }
            List<RollInformationHistory> list = rollInformationDao.findDataRollInformationHistoryByPage((pageIndex - 1) * pageSize, pageSize, frame_scope_id, frame_scope, factory_id, material_id, production_line, production_line_id, roll_revolve, roll_no, roll_state, roll_typeid, factory, material, inventory_stateid, framerangeid, ipaired, frame_noid, contract_no, order_year,dbegin,dend);
            for(RollInformationHistory r : list) {
                if(!StringUtils.isEmpty(r.getRoll_state())) {
                    r.setRoll_state_value(dictionaryService.findDataByV1("rollstate", 1L, r.getRoll_state()));
                }
                if(!StringUtils.isEmpty(r.getRoll_revolve())) {
                    r.setRoll_revolve_value(dictionaryService.findDataByV1("rollrevolve", 1L, r.getRoll_revolve()));
                }
                if(!StringUtils.isEmpty(r.getRoll_special())) {
                    r.setRoll_special_value(dictionaryService.findDataByV1("rollspecial", 1L, r.getRoll_special()));
                }
            }
            Integer count = rollInformationDao.findDataRollInformationHistoryByPageSize(frame_scope_id, frame_scope, factory_id, material_id, production_line, production_line_id, roll_revolve, roll_no, roll_state, roll_typeid, factory, material, inventory_stateid, framerangeid, ipaired, frame_noid, contract_no, order_year,dbegin,dend);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public ResultData findRollDiameterByPage(String data) {
        try {
            JRollInformation jrollInformation = JSON.parseObject(data, JRollInformation.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollInformation.getPageIndex();
            Integer pageSize = jrollInformation.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollInformation.getCondition()) {
                jsonObject = JSON.parseObject(jrollInformation.getCondition().toString());
            }


            String roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = jsonObject.get("roll_typeid").toString();
            }
            String framerangeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("framerangeid"))) {
                framerangeid = jsonObject.get("framerangeid").toString();
            }

            String factory_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("factory_id"))) {
                factory_id = jsonObject.get("factory_id").toString();
            }

            String material_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("material_id"))) {
                material_id = jsonObject.get("material_id").toString();
            }
            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }
            List<RollInformation> list = rollInformationDao.findRollDiameterByPage((pageIndex - 1) * pageSize, pageSize,roll_typeid,framerangeid,factory_id,material_id,dbegin);
            BigDecimal count = rollInformationDao.findRollDiameterByPageSize(roll_typeid,framerangeid,factory_id,material_id,dbegin);
            return ResultData.ResultDataSuccess(list,0, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }
    public String excelRollDiameter(HSSFWorkbook workbook, String roll_typeid,String framerangeid,String factory_id
            ,String material_id,String dbegin) {
        String filename="各月辊径统计";
        if(StringUtils.isEmpty(roll_typeid)){
            roll_typeid = null;
        }
        if(StringUtils.isEmpty(framerangeid)){
            framerangeid = null;
        }
        if(StringUtils.isEmpty(factory_id)){
            factory_id = null;
        }
        if(StringUtils.isEmpty(material_id)){
            material_id = null;
        }
        if(StringUtils.isEmpty(dbegin)){
            dbegin = null;
        }
        List<RollInformation> list = rollInformationDao.findRollDiameterByPage(0, 1000,roll_typeid,framerangeid,factory_id,material_id,dbegin);
        BigDecimal count = rollInformationDao.findRollDiameterByPageSize(roll_typeid,framerangeid,factory_id,material_id,dbegin);
        HSSFSheet sheet = workbook.createSheet("sheet1");
        getExcelRollDiameter(list,count, sheet, workbook);
        return filename;
    }

    public ResultData findRollInformationNum(String data) {
        try{
            List<RollInformationReport> list = rollInformationDao.findRollInformationNum();
            return ResultData.ResultDataSuccess(list,null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }


    public String excelRollInformationNum(HSSFWorkbook workbook) {
        String filename="轧辊数量统计";
        List<RollInformationReport> list = rollInformationDao.findRollInformationNum();
        HSSFSheet sheet = workbook.createSheet("sheet1");
        getExcelRollInformationNum(list,sheet,workbook);
        return filename;
    }

    public ResultData findRollDiameterABigScreenByPage(String data) {
        try {
            JRollInformation jrollInformation = JSON.parseObject(data, JRollInformation.class);
            JSONObject jsonObject = null;
            //Integer pageIndex = jrollInformation.getPageIndex();
            //Integer pageSize = jrollInformation.getPageSize();

            //if (null == pageIndex || null == pageSize) {
            //    return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            //}

            //if (null != jrollInformation.getCondition()) {
                jsonObject = JSON.parseObject(jrollInformation.getCondition().toString());
            //}


            String roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = jsonObject.get("roll_typeid").toString();
            }
            String framerangeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("framerangeid"))) {
                framerangeid = jsonObject.get("framerangeid").toString();
            }

            String factory_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("factory_id"))) {
                factory_id = jsonObject.get("factory_id").toString();
            }

            String material_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("material_id"))) {
                material_id = jsonObject.get("material_id").toString();
            }

            String ifabnormal = null;
            if (!StringUtils.isEmpty(jsonObject.get("ifabnormal"))) {
                ifabnormal = jsonObject.get("ifabnormal").toString();
            }


            String dbegin = DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss");
           /* if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }*/
            List<RollInformation> list = rollInformationDao.findRollDiameterABigScreenByPage(roll_typeid,framerangeid,factory_id,material_id,dbegin,ifabnormal);
            BigDecimal count =BigDecimal.ZERO;
            BigDecimal total =BigDecimal.ZERO;

            List<MapXYEntity> map = new ArrayList<MapXYEntity>();
            if(list!=null&&list.size()>0){
                for(RollInformation bean:list){
                    MapXYEntity t = new MapXYEntity();
                    if(!StringUtils.isEmpty(bean.getRoll_type()+" "+bean.getMaterial()+" "+bean.getFramerange())){
                        //t.setX(bean.getRoll_type()+" "+bean.getMaterial()+" "+bean.getFramerange());
                        String str="";
                        if(bean.getMaterial().equals("高速钢")){
                            str+="H";
                        }
                            str+=bean.getFramerange();
                        t.setX(str);
                    }else{
                        t.setX(0);
                    }
                    if(!StringUtils.isEmpty(bean.getCurrentdiameter())){
                        count.add(BigDecimal.valueOf(bean.getCurrentdiameter()));
                        t.setY1(bean.getCurrentdiameter());
                    }else{
                        t.setY1(0);
                    }
                    if(!StringUtils.isEmpty(bean.getBody_diameter())){
                        total.add(BigDecimal.valueOf(bean.getBody_diameter()));
                        t.setY2(bean.getBody_diameter());
                    }else{
                        t.setY2(0);
                    }
                    map.add(t);
                }
            }
            return ResultData.ResultDataSuccess(list,0, count,total,map);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    private void getExcelRollInformationNum(List<RollInformationReport> list, HSSFSheet sheet, HSSFWorkbook workbook) {
        if(list!=null && list.size()>0){
            // 设置表头样式 居中 加粗 宋体 20
            HSSFCellStyle cellStyleHead = workbook.createCellStyle();
            cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
            HSSFFont redFontHead = workbook.createFont();
            redFontHead.setFontHeightInPoints((short) 12);//设置字体大小
            redFontHead.setFontName("宋体");//字体型号
            cellStyleHead.setFont(redFontHead);
            HSSFRow rowhead = sheet.createRow(0);   //创建第一行头
            HSSFCell cell_00 = rowhead.createCell(0);   //创建第一行第一列
            cell_00.setCellStyle(cellStyleHead);
            cell_00.setCellValue("轧辊数量统计");

            HSSFRow row1 = sheet.createRow(1);   //创建第二行
            HSSFCell cell_2_1 = row1.createCell(0);   //创建第二行第一列
            cell_2_1.setCellValue("序号");
            HSSFCell cell_2_2 = row1.createCell(1);   //创建第二行第二列
            cell_2_2.setCellValue("料号");
            HSSFCell cell_2_3 = row1.createCell(2);   //创建第二行第三列
            cell_2_3.setCellValue("轧辊类型");
            HSSFCell cell_2_4 = row1.createCell(3);   //创建第二行第四列
            cell_2_4.setCellValue("轧辊材质");
            HSSFCell cell_2_5 = row1.createCell(4);   //创建第二行第五列
            cell_2_5.setCellValue("机架范围");
            HSSFCell cell_2_6 = row1.createCell(5);   //创建第二行第六列
            cell_2_6.setCellValue("规格");
            HSSFCell cell_2_7 = row1.createCell(6);   //创建第二行第七列
            cell_2_7.setCellValue("总数");
            HSSFCell cell_2_8 = row1.createCell(7);   //创建第二行第八列
            cell_2_8.setCellValue("报废");
            HSSFCell cell_2_9 = row1.createCell(8);   //创建第二行第9列
            cell_2_9.setCellValue("新辊");
            HSSFCell cell_2_10 = row1.createCell(9);   //创建第二行第10列
            cell_2_10.setCellValue("旧辊");

            //从第三行开始(包含第三行)
            for (int i = 0; i < list.size(); i++) {
                RollInformationReport v = list.get(i);
                HSSFRow rown = sheet.createRow(i + 2);   //创建第三行
                HSSFCell cell_3_1 = rown.createCell(0);   // 1
                cell_3_1.setCellValue(i + 1);
                HSSFCell cell_3_2 = rown.createCell(1);   //2
                if (!StringUtils.isEmpty(v.getMaterial_no())) {
                    cell_3_2.setCellValue(v.getMaterial_no());
                }
                HSSFCell cell_3_3 = rown.createCell(2);   //3
                if (!StringUtils.isEmpty(v.getRoll_type())) {
                    cell_3_3.setCellValue(v.getRoll_type());
                }
                HSSFCell cell_3_4 = rown.createCell(3);   //4
                if (!StringUtils.isEmpty(v.getMaterial())) {
                    cell_3_4.setCellValue(v.getMaterial());
                }
                HSSFCell cell_3_5 = rown.createCell(4);   //5
                if (!StringUtils.isEmpty(v.getFramerange())) {
                    cell_3_5.setCellValue(v.getFramerange());
                }
                HSSFCell cell_3_6 = rown.createCell(5);   //6
                if (!StringUtils.isEmpty(v.getSpecifications_no())) {
                    cell_3_6.setCellValue(v.getSpecifications_no());
                }
                HSSFCell cell_3_7 = rown.createCell(6);   //7
                if (!StringUtils.isEmpty(v.getAllnum())) {
                    cell_3_7.setCellValue(v.getAllnum().toString());
                }
                HSSFCell cell_3_8 = rown.createCell(7);   //8
                if (!StringUtils.isEmpty(v.getScrapnum())) {
                    cell_3_8.setCellValue(v.getScrapnum().toString());
                }
                HSSFCell cell_3_9 = rown.createCell(8);   //9
                if (!StringUtils.isEmpty(v.getNewnum())) {
                    cell_3_9.setCellValue(v.getNewnum().toString());
                }
                HSSFCell cell_3_10 = rown.createCell(9);   //10
                if (!StringUtils.isEmpty(v.getOldnum())) {
                    cell_3_10.setCellValue(v.getOldnum().toString());
                }
            }
            CellRangeAddress region = new CellRangeAddress(0, 0, 0, 7);
            sheet.addMergedRegion(region);
        }
    }

    private void getExcelRollDiameter(List<RollInformation> list, BigDecimal count, HSSFSheet sheet, HSSFWorkbook workbook) {
        if(list!=null && list.size()>0){
            // 设置表头样式 居中 加粗 宋体 20
            HSSFCellStyle cellStyleHead = workbook.createCellStyle();
            cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
            HSSFFont redFontHead = workbook.createFont();
            redFontHead.setFontHeightInPoints((short) 12);//设置字体大小
            redFontHead.setFontName("宋体");//字体型号
            cellStyleHead.setFont(redFontHead);
            HSSFRow rowhead = sheet.createRow(0);   //创建第一行头
            HSSFCell cell_00 = rowhead.createCell(0);   //创建第一行第一列
            cell_00.setCellStyle(cellStyleHead);
            cell_00.setCellValue("各月辊径统计"+"("+count+")");

            HSSFRow row1 = sheet.createRow(1);   //创建第二行
            HSSFCell cell_2_1 = row1.createCell(0);   //创建第二行第一列
            cell_2_1.setCellValue("序号");
            HSSFCell cell_2_2 = row1.createCell(1);   //创建第二行第二列
            cell_2_2.setCellValue("辊号");
            HSSFCell cell_2_3 = row1.createCell(2);   //创建第二行第三列
            cell_2_3.setCellValue("轧辊类型");
            HSSFCell cell_2_4 = row1.createCell(3);   //创建第二行第四列
            cell_2_4.setCellValue("轧辊材质");
            HSSFCell cell_2_5 = row1.createCell(4);   //创建第二行第五列
            cell_2_5.setCellValue("机架范围");
            HSSFCell cell_2_6 = row1.createCell(5);   //创建第二行第六列
            cell_2_6.setCellValue("客户");
            HSSFCell cell_2_7 = row1.createCell(6);   //创建第二行第七列
            cell_2_7.setCellValue("辊径");
            HSSFCell cell_2_8 = row1.createCell(7);   //创建第二行第八列
            cell_2_8.setCellValue("结存");

            //从第三行开始(包含第三行)
            for (int i = 0; i < list.size(); i++) {
                RollInformation v = list.get(i);
                HSSFRow rown = sheet.createRow(i + 2);   //创建第三行
                HSSFCell cell_3_1 = rown.createCell(0);   // 1
                cell_3_1.setCellValue(i + 1);
                HSSFCell cell_3_2 = rown.createCell(1);   //2
                if (!StringUtils.isEmpty(v.getRoll_no())) {
                    cell_3_2.setCellValue(v.getRoll_no());
                }
                HSSFCell cell_3_3 = rown.createCell(2);   //3
                if (!StringUtils.isEmpty(v.getRoll_type())) {
                    cell_3_3.setCellValue(v.getRoll_type());
                }
                HSSFCell cell_3_4 = rown.createCell(3);   //4
                if (!StringUtils.isEmpty(v.getMaterial())) {
                    cell_3_4.setCellValue(v.getMaterial());
                }
                HSSFCell cell_3_5 = rown.createCell(4);   //5
                if (!StringUtils.isEmpty(v.getFramerange())) {
                    cell_3_5.setCellValue(v.getFramerange());
                }
                HSSFCell cell_3_6 = rown.createCell(5);   //6
                if (!StringUtils.isEmpty(v.getFactory())) {
                    cell_3_6.setCellValue(v.getFactory());
                }
                HSSFCell cell_3_7 = rown.createCell(6);   //7
                if (!StringUtils.isEmpty(v.getCurrentdiameter())) {
                    cell_3_7.setCellValue(v.getCurrentdiameter().toString());
                }
                HSSFCell cell_3_8 = rown.createCell(7);   //8
                if (!StringUtils.isEmpty(v.getBody_diameter())) {
                    cell_3_8.setCellValue(v.getBody_diameter().toString());
                }
            }
            CellRangeAddress region = new CellRangeAddress(0, 0, 0, 7);
            sheet.addMergedRegion(region);
        }
    }

}
