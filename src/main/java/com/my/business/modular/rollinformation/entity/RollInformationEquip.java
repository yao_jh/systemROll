package com.my.business.modular.rollinformation.entity;

/**
 * 轧辊基本信息实体类
 *
 * @author 生成器生成
 * @date 2020-08-10 09:08:54
 */
public class RollInformationEquip {

	private String roll_no; // 辊号
	private String roll_type; // 轧辊类型
	private String frame_no; // 机架号
	private Double currentdiameter; // 当前辊径
	private String roll_position;  //轧辊位置
	private String onlinetime; //最新一次上线时间
	
	public String getOnlinetime() {
		return onlinetime;
	}

	public void setOnlinetime(String onlinetime) {
		this.onlinetime = onlinetime;
	}

	public String getRoll_no() {
		return roll_no;
	}

	public void setRoll_no(String roll_no) {
		this.roll_no = roll_no;
	}

	public String getRoll_type() {
		return roll_type;
	}

	public void setRoll_type(String roll_type) {
		this.roll_type = roll_type;
	}

	public String getFrame_no() {
		return frame_no;
	}

	public void setFrame_no(String frame_no) {
		this.frame_no = frame_no;
	}

	public Double getCurrentdiameter() {
		return currentdiameter;
	}

	public void setCurrentdiameter(Double currentdiameter) {
		this.currentdiameter = currentdiameter;
	}

	public String getRoll_position() {
		return roll_position;
	}

	public void setRoll_position(String roll_position) {
		this.roll_position = roll_position;
	}
}