package com.my.business.modular.rollinformation.entity;

/**
 * 轧辊基本信息实体类
 *
 * @author 生成器生成
 * @date 2020-08-10 09:08:54
 */
public class RollBoard{

	private Long roll_typeid; // 轧辊类型id
	private String roll_type; // 轧辊类型
	private Integer dmx;  //待磨削次数
	private Integer zlq;  //在冷却次数
	private Integer zmx;  //在磨削次数
	private Integer zzp;  //在装配次数

	public Long getRoll_typeid() {
		return roll_typeid;
	}

	public void setRoll_typeid(Long roll_typeid) {
		this.roll_typeid = roll_typeid;
	}

	public String getRoll_type() {
		return roll_type;
	}

	public void setRoll_type(String roll_type) {
		this.roll_type = roll_type;
	}

	public Integer getDmx() {
		return dmx;
	}

	public void setDmx(Integer dmx) {
		this.dmx = dmx;
	}

	public Integer getZlq() {
		return zlq;
	}

	public void setZlq(Integer zlq) {
		this.zlq = zlq;
	}

	public Integer getZmx() {
		return zmx;
	}

	public void setZmx(Integer zmx) {
		this.zmx = zmx;
	}

	public Integer getZzp() {
		return zzp;
	}

	public void setZzp(Integer zzp) {
		this.zzp = zzp;
	}
}