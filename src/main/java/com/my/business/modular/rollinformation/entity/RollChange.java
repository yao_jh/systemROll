package com.my.business.modular.rollinformation.entity;

public class RollChange {
	private String a_roll;
	private Long a_level;
	private String b_roll;
	private Long b_level;

	public String getA_roll() {
		return a_roll;
	}

	public void setA_roll(String a_roll) {
		this.a_roll = a_roll;
	}

	public Long getA_level() {
		return a_level;
	}

	public void setA_level(Long a_level) {
		this.a_level = a_level;
	}

	public String getB_roll() {
		return b_roll;
	}

	public void setB_roll(String b_roll) {
		this.b_roll = b_roll;
	}

	public Long getB_level() {
		return b_level;
	}

	public void setB_level(Long b_level) {
		this.b_level = b_level;
	}

}
