package com.my.business.modular.rollinformation.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轧辊基本信息实体类
 *
 * @author 生成器生成
 * @date 2020-08-10 09:08:54
 */
public class RollInformationReport extends BaseEntity {

    private Long material_noid;//料号id
    private String material_no;  //料号名称

    private Long specifications_noid;//规格id
    private String specifications_no;  //规格名称

    private Long roll_typeid;//轧辊类型id
    private String roll_type;  //轧辊类型名称

    private Long material_id;//材质id
    private String material;  //材质名称

    private Long framerangeid;//机架范围id
    private String framerange;  //机架范围名称

    private Long allnum;//所有辊
    private Long newnum;//新辊
    private Long scrapnum;//报废辊
    private Long oldnum;// 旧辊

    public Long getMaterial_noid() {
        return material_noid;
    }

    public void setMaterial_noid(Long material_noid) {
        this.material_noid = material_noid;
    }

    public String getMaterial_no() {
        return material_no;
    }

    public void setMaterial_no(String material_no) {
        this.material_no = material_no;
    }

    public Long getSpecifications_noid() {
        return specifications_noid;
    }

    public void setSpecifications_noid(Long specifications_noid) {
        this.specifications_noid = specifications_noid;
    }

    public String getSpecifications_no() {
        return specifications_no;
    }

    public void setSpecifications_no(String specifications_no) {
        this.specifications_no = specifications_no;
    }

    public Long getRoll_typeid() {
        return roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public Long getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(Long material_id) {
        this.material_id = material_id;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public Long getFramerangeid() {
        return framerangeid;
    }

    public void setFramerangeid(Long framerangeid) {
        this.framerangeid = framerangeid;
    }

    public String getFramerange() {
        return framerange;
    }

    public void setFramerange(String framerange) {
        this.framerange = framerange;
    }

    public Long getAllnum() {
        return allnum;
    }

    public void setAllnum(Long allnum) {
        this.allnum = allnum;
    }

    public Long getNewnum() {
        return newnum;
    }

    public void setNewnum(Long newnum) {
        this.newnum = newnum;
    }

    public Long getScrapnum() {
        return scrapnum;
    }

    public void setScrapnum(Long scrapnum) {
        this.scrapnum = scrapnum;
    }

    public Long getOldnum() {
        return oldnum;
    }

    public void setOldnum(Long oldnum) {
        this.oldnum = oldnum;
    }
}