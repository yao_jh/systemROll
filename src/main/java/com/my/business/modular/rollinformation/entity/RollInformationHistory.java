package com.my.business.modular.rollinformation.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轧辊基本信息实体类
 *
 * @author 生成器生成
 * @date 2020-08-10 09:08:54
 */
public class RollInformationHistory extends BaseEntity {
	private String roll_state_value;  //轧辊状态
    private String roll_revolve_value;  //周转状态
    private String roll_special_value;  //特殊状态

    private Long indocno;  //主键
    private String production_line;  //产线
    private Long production_line_id; //产线id
    private String roll_no;  //辊号
    private Long factory_id;
    private String factory;  //生产厂家
    private Long material_id;
    private String material;  //材质
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private Long frame_noid;  //机架号id
    private String frame_no;  //机架号
    private String frame_scope;  //机架范围
    private Long frame_scope_id;  //机架范围id
    private String order_year;  //订货年
    private String settlement_type;  //结算类型
    private Double price;  //单价
    private Long account;  //账目情况（0或者1）
    private Double mmprice;  //毫米单价
    private String productno;  //出厂制品号
    private Long roll_positionid;  //轧辊位置id
    private String roll_position;  //轧辊位置
    private Long roll_state;  //轧辊状态
    private Long roll_revolve;  //周转状态
    private Long roll_special;  //特殊状态
    private String contract_no;  //合同号
    private String arrivetime;  //到货日期
    private Long arriveuserid;  //到货责任人id
    private String arriveuser;  //到货责任人
    private Double arrivebodyhardnessmax;  //到货辊身硬度最大值
    private Double arrivebodyhardnessmin;  //到货辊身硬度最小值
    private Double arrivediameterhardnessmax;  //到货辊径硬度最大值
    private Double arrivediameterhardnessmin;  //到货辊径硬度最小值
    private Double body_length;  //辊身长度
    private Double body_diameter;  //辊身直径
    private Double shoulder_diameter;  //辊肩直径
    private Double neck_diameter;  //辊颈直径
    private Double flathead_length;  //扁头尺寸
    private Double scrap_diameter;  //报废直径
    private Long checkorderid;  //检测工单id
    private Long ifok;  //检测是否合格
    private String fchecktime;  //首次检测日期
    private String fusetime;  //首次使用时间
    private String business_scope;  //业务范围
    private Long assetsid;  //资产分类id
    private String assets;  //资产分类
    private String assetsno;  //资产编号
    private String assetsnote;  //资产描述
    private Double original;  //原值
    private Double networth;  //净值
    private Long inventory_stateid;  //库存状态id
    private String inventory_state;  //库存状态
    private String lastgrindingtime;  //最新一次磨削时间
    private String lastuplinetime;  //最新一次上线时间
    private String lastlowlinetime;  //最新一次下线时间
    private String dbegin;
    private String dend;

    public String getDbegin() {
        return dbegin;
    }

    public void setDbegin(String dbegin) {
        this.dbegin = dbegin;
    }

    public String getDend() {
        return dend;
    }

    public void setDend(String dend) {
        this.dend = dend;
    }

    public String getFirstuplinetime() {
        return firstuplinetime;
    }

    public void setFirstuplinetime(String firstuplinetime) {
        this.firstuplinetime = firstuplinetime;
    }

    private String firstuplinetime;  //第一次上线时间

    private Double lastgrindingdepth;  //最新一次磨削量
    private Long grindingcount;  //累积磨削次数
    private Double grindingavg;  //平均磨削量
    private Long uplinecount;  //累积上线次数
    private Double rollkilometer;  //累积轧制公里数
    private Double rolltonnagetotle;  //累计轧制吨位数
    private Double rolltonnagemm;  //每毫米轧制吨位
    private Double currentdiameter;  //当前辊径
    private Double theorydiameter;  //理论直径
    private String scrapdate;  //报废日期
    private Long scrapuserid;  //报废责任人id
    private String scrapuser;  //报废责任人
    private Double scrapdiameter;  //报废辊径
    private String scrapreason;  //报废原因
    private Long iaccident;  //事故辊标记
    private Long operateid;  //操作人id
    private String operatename;  //操作人
    private String operatetime;  //操作时间
    private Long confirmationid;  //确认人id
    private String confirmationname;  //确认人姓名
    private String confirmationtime;  //确认时间
    private Double residualdiameter;  //剩余可用直径
    private Long rollshapeid;  //辊形id
    private String rollshape;  //辊形
    private Double roundness;  //圆度
    private Double roughness;  //粗糙度
    private Double bodyhardness;  //辊身硬度
    private Double neckhardness;  //辊颈硬度
    private Double shoulderhardness;  //辊肩硬度
    private String inspectionresults;  //探伤结果
    private Long workoilcount;  //工作辊补油周期跟踪次数
    private Long framerangeid;  //机架范围id
    private String framerange;  //机架范围
    private String loomingposition;  //上机位置
    private Long qualityid;  //质量异议状态id
    private String quality;  //质量异议状态
    private String qualityreason;  //质量异议原因
    private Long ipaired;  //配对状态（0 未配对 1已配对）
    private String bearingchock_no; //工作辊轴承座号(OS-DS)
    private String work_layer;  //工作层
    private String leave_area;  //存放地点
    private String capital_time;  //资本化时间
    private String obearing_chock;  //轴承座操作侧os号
    private String dbearing_chock;  //轴承座传动侧ds号
    private Long zb_ifok;  //质保书是否合格
    private Long machine_id; //磨床id
    private Long ilevel;  //磨削优先级id
    private String slevel; //磨削优先级
    private Double ini_crown_max;  //凸度最大值
    private Double ini_crown_min;  //凸度最小值
    private Long special_state_count; //特殊状态跟踪次数
    private Double weight;

    private String operate_time;  //操作时间
    private String operate_name;  //操作模式(报废)

    private  Long  material_noid;//料号id
    private  String  material_no;//料号名称
    private  Long  specifications_noid;// 规格id
    private  String  specifications_no;// 规格名称

    public Long getMaterial_noid() {
        return material_noid;
    }

    public void setMaterial_noid(Long material_noid) {
        this.material_noid = material_noid;
    }

    public String getMaterial_no() {
        return material_no;
    }

    public void setMaterial_no(String material_no) {
        this.material_no = material_no;
    }

    public Long getSpecifications_noid() {
        return specifications_noid;
    }

    public void setSpecifications_noid(Long specifications_noid) {
        this.specifications_noid = specifications_noid;
    }

    public String getSpecifications_no() {
        return specifications_no;
    }

    public void setSpecifications_no(String specifications_no) {
        this.specifications_no = specifications_no;
    }

    public String getOperate_time() {
        return operate_time;
    }

    public void setOperate_time(String operate_time) {
        this.operate_time = operate_time;
    }

    public String getOperate_name() {
        return operate_name;
    }

    public void setOperate_name(String operate_name) {
        this.operate_name = operate_name;
    }

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getFactory() {
        return this.factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public String getMaterial() {
        return this.material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public Long getRoll_typeid() {
        return this.roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public Long getFrame_noid() {
        return this.frame_noid;
    }

    public void setFrame_noid(Long frame_noid) {
        this.frame_noid = frame_noid;
    }

    public String getFrame_no() {
        return this.frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public String getOrder_year() {
        return this.order_year;
    }

    public void setOrder_year(String order_year) {
        this.order_year = order_year;
    }

    public String getSettlement_type() {
        return this.settlement_type;
    }

    public void setSettlement_type(String settlement_type) {
        this.settlement_type = settlement_type;
    }

    public Double getPrice() {
        return this.price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getAccount() {
        return this.account;
    }

    public void setAccount(Long account) {
        this.account = account;
    }

    public Double getMmprice() {
        return this.mmprice;
    }

    public void setMmprice(Double mmprice) {
        this.mmprice = mmprice;
    }

    public String getProductno() {
        return this.productno;
    }

    public void setProductno(String productno) {
        this.productno = productno;
    }

    public Long getRoll_positionid() {
        return this.roll_positionid;
    }

    public void setRoll_positionid(Long roll_positionid) {
        this.roll_positionid = roll_positionid;
    }

    public String getRoll_position() {
        return this.roll_position;
    }

    public void setRoll_position(String roll_position) {
        this.roll_position = roll_position;
    }

    public Long getRoll_state() {
        return this.roll_state;
    }

    public void setRoll_state(Long roll_state) {
        this.roll_state = roll_state;
    }

    public Long getRoll_revolve() {
        return roll_revolve;
    }

    public void setRoll_revolve(Long roll_revolve) {
        this.roll_revolve = roll_revolve;
    }

    public Long getRoll_special() {
        return roll_special;
    }

    public void setRoll_special(Long roll_special) {
        this.roll_special = roll_special;
    }

    public String getContract_no() {
        return this.contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    public String getArrivetime() {
        return this.arrivetime;
    }

    public void setArrivetime(String arrivetime) {
        this.arrivetime = arrivetime;
    }

    public Long getArriveuserid() {
        return this.arriveuserid;
    }

    public void setArriveuserid(Long arriveuserid) {
        this.arriveuserid = arriveuserid;
    }

    public String getArriveuser() {
        return this.arriveuser;
    }

    public void setArriveuser(String arriveuser) {
        this.arriveuser = arriveuser;
    }

    public Double getArrivebodyhardnessmax() {
        return this.arrivebodyhardnessmax;
    }

    public void setArrivebodyhardnessmax(Double arrivebodyhardnessmax) {
        this.arrivebodyhardnessmax = arrivebodyhardnessmax;
    }

    public Double getArrivebodyhardnessmin() {
        return this.arrivebodyhardnessmin;
    }

    public void setArrivebodyhardnessmin(Double arrivebodyhardnessmin) {
        this.arrivebodyhardnessmin = arrivebodyhardnessmin;
    }

    public Double getArrivediameterhardnessmax() {
        return this.arrivediameterhardnessmax;
    }

    public void setArrivediameterhardnessmax(Double arrivediameterhardnessmax) {
        this.arrivediameterhardnessmax = arrivediameterhardnessmax;
    }

    public Double getArrivediameterhardnessmin() {
        return this.arrivediameterhardnessmin;
    }

    public void setArrivediameterhardnessmin(Double arrivediameterhardnessmin) {
        this.arrivediameterhardnessmin = arrivediameterhardnessmin;
    }

    public Double getBody_length() {
        return this.body_length;
    }

    public void setBody_length(Double body_length) {
        this.body_length = body_length;
    }

    public Double getBody_diameter() {
        return this.body_diameter;
    }

    public void setBody_diameter(Double body_diameter) {
        this.body_diameter = body_diameter;
    }

    public Double getShoulder_diameter() {
        return this.shoulder_diameter;
    }

    public void setShoulder_diameter(Double shoulder_diameter) {
        this.shoulder_diameter = shoulder_diameter;
    }

    public Double getNeck_diameter() {
        return this.neck_diameter;
    }

    public void setNeck_diameter(Double neck_diameter) {
        this.neck_diameter = neck_diameter;
    }

    public Double getFlathead_length() {
        return this.flathead_length;
    }

    public void setFlathead_length(Double flathead_length) {
        this.flathead_length = flathead_length;
    }

    public Double getScrap_diameter() {
        return this.scrap_diameter;
    }

    public void setScrap_diameter(Double scrap_diameter) {
        this.scrap_diameter = scrap_diameter;
    }

    public Long getCheckorderid() {
        return this.checkorderid;
    }

    public void setCheckorderid(Long checkorderid) {
        this.checkorderid = checkorderid;
    }

    public Long getIfok() {
        return this.ifok;
    }

    public void setIfok(Long ifok) {
        this.ifok = ifok;
    }

    public String getFchecktime() {
        return this.fchecktime;
    }

    public void setFchecktime(String fchecktime) {
        this.fchecktime = fchecktime;
    }

    public String getFusetime() {
        return this.fusetime;
    }

    public void setFusetime(String fusetime) {
        this.fusetime = fusetime;
    }

    public String getBusiness_scope() {
        return this.business_scope;
    }

    public void setBusiness_scope(String business_scope) {
        this.business_scope = business_scope;
    }

    public Long getAssetsid() {
        return this.assetsid;
    }

    public void setAssetsid(Long assetsid) {
        this.assetsid = assetsid;
    }

    public String getAssets() {
        return this.assets;
    }

    public void setAssets(String assets) {
        this.assets = assets;
    }

    public String getAssetsno() {
        return this.assetsno;
    }

    public void setAssetsno(String assetsno) {
        this.assetsno = assetsno;
    }

    public String getAssetsnote() {
        return this.assetsnote;
    }

    public void setAssetsnote(String assetsnote) {
        this.assetsnote = assetsnote;
    }

    public Double getOriginal() {
        return this.original;
    }

    public void setOriginal(Double original) {
        this.original = original;
    }

    public Double getNetworth() {
        return this.networth;
    }

    public void setNetworth(Double networth) {
        this.networth = networth;
    }

    public Long getInventory_stateid() {
        return this.inventory_stateid;
    }

    public void setInventory_stateid(Long inventory_stateid) {
        this.inventory_stateid = inventory_stateid;
    }

    public String getInventory_state() {
        return this.inventory_state;
    }

    public void setInventory_state(String inventory_state) {
        this.inventory_state = inventory_state;
    }

    public String getLastgrindingtime() {
        return this.lastgrindingtime;
    }

    public void setLastgrindingtime(String lastgrindingtime) {
        this.lastgrindingtime = lastgrindingtime;
    }

    public String getLastuplinetime() {
        return this.lastuplinetime;
    }

    public void setLastuplinetime(String lastuplinetime) {
        this.lastuplinetime = lastuplinetime;
    }

    public String getLastlowlinetime() {
        return this.lastlowlinetime;
    }

    public void setLastlowlinetime(String lastlowlinetime) {
        this.lastlowlinetime = lastlowlinetime;
    }

    public Double getLastgrindingdepth() {
        return this.lastgrindingdepth;
    }

    public void setLastgrindingdepth(Double lastgrindingdepth) {
        this.lastgrindingdepth = lastgrindingdepth;
    }

    public Long getGrindingcount() {
        return this.grindingcount;
    }

    public void setGrindingcount(Long grindingcount) {
        this.grindingcount = grindingcount;
    }

    public Double getGrindingavg() {
        return this.grindingavg;
    }

    public void setGrindingavg(Double grindingavg) {
        this.grindingavg = grindingavg;
    }

    public Long getUplinecount() {
        return this.uplinecount;
    }

    public void setUplinecount(Long uplinecount) {
        this.uplinecount = uplinecount;
    }

    public Double getRollkilometer() {
        return this.rollkilometer;
    }

    public void setRollkilometer(Double rollkilometer) {
        this.rollkilometer = rollkilometer;
    }

    public Double getRolltonnagetotle() {
        return this.rolltonnagetotle;
    }

    public void setRolltonnagetotle(Double rolltonnagetotle) {
        this.rolltonnagetotle = rolltonnagetotle;
    }

    public Double getRolltonnagemm() {
        return this.rolltonnagemm;
    }

    public void setRolltonnagemm(Double rolltonnagemm) {
        this.rolltonnagemm = rolltonnagemm;
    }

    public Double getCurrentdiameter() {
        return this.currentdiameter;
    }

    public void setCurrentdiameter(Double currentdiameter) {
        this.currentdiameter = currentdiameter;
    }

    public Double getTheorydiameter() {
        return this.theorydiameter;
    }

    public void setTheorydiameter(Double theorydiameter) {
        this.theorydiameter = theorydiameter;
    }

    public String getScrapdate() {
        return this.scrapdate;
    }

    public void setScrapdate(String scrapdate) {
        this.scrapdate = scrapdate;
    }

    public Long getScrapuserid() {
        return this.scrapuserid;
    }

    public void setScrapuserid(Long scrapuserid) {
        this.scrapuserid = scrapuserid;
    }

    public String getScrapuser() {
        return this.scrapuser;
    }

    public void setScrapuser(String scrapuser) {
        this.scrapuser = scrapuser;
    }

    public Double getScrapdiameter() {
        return this.scrapdiameter;
    }

    public void setScrapdiameter(Double scrapdiameter) {
        this.scrapdiameter = scrapdiameter;
    }

    public String getScrapreason() {
        return this.scrapreason;
    }

    public void setScrapreason(String scrapreason) {
        this.scrapreason = scrapreason;
    }

    public Long getIaccident() {
        return this.iaccident;
    }

    public void setIaccident(Long iaccident) {
        this.iaccident = iaccident;
    }

    public Long getOperateid() {
        return this.operateid;
    }

    public void setOperateid(Long operateid) {
        this.operateid = operateid;
    }

    public String getOperatename() {
        return this.operatename;
    }

    public void setOperatename(String operatename) {
        this.operatename = operatename;
    }

    public String getOperatetime() {
        return this.operatetime;
    }

    public void setOperatetime(String operatetime) {
        this.operatetime = operatetime;
    }

    public Long getConfirmationid() {
        return this.confirmationid;
    }

    public void setConfirmationid(Long confirmationid) {
        this.confirmationid = confirmationid;
    }

    public String getConfirmationname() {
        return this.confirmationname;
    }

    public void setConfirmationname(String confirmationname) {
        this.confirmationname = confirmationname;
    }

    public String getConfirmationtime() {
        return this.confirmationtime;
    }

    public void setConfirmationtime(String confirmationtime) {
        this.confirmationtime = confirmationtime;
    }

    public Double getResidualdiameter() {
        return this.residualdiameter;
    }

    public void setResidualdiameter(Double residualdiameter) {
        this.residualdiameter = residualdiameter;
    }

    public Long getRollshapeid() {
        return this.rollshapeid;
    }

    public void setRollshapeid(Long rollshapeid) {
        this.rollshapeid = rollshapeid;
    }

    public String getRollshape() {
        return this.rollshape;
    }

    public void setRollshape(String rollshape) {
        this.rollshape = rollshape;
    }

    public Double getRoundness() {
        return this.roundness;
    }

    public void setRoundness(Double roundness) {
        this.roundness = roundness;
    }

    public Double getRoughness() {
        return this.roughness;
    }

    public void setRoughness(Double roughness) {
        this.roughness = roughness;
    }

    public Double getBodyhardness() {
        return this.bodyhardness;
    }

    public void setBodyhardness(Double bodyhardness) {
        this.bodyhardness = bodyhardness;
    }

    public Double getNeckhardness() {
        return this.neckhardness;
    }

    public void setNeckhardness(Double neckhardness) {
        this.neckhardness = neckhardness;
    }

    public Double getShoulderhardness() {
        return this.shoulderhardness;
    }

    public void setShoulderhardness(Double shoulderhardness) {
        this.shoulderhardness = shoulderhardness;
    }

    public String getInspectionresults() {
        return this.inspectionresults;
    }

    public void setInspectionresults(String inspectionresults) {
        this.inspectionresults = inspectionresults;
    }

    public Long getWorkoilcount() {
        return this.workoilcount;
    }

    public void setWorkoilcount(Long workoilcount) {
        this.workoilcount = workoilcount;
    }

    public Long getFramerangeid() {
        return this.framerangeid;
    }

    public void setFramerangeid(Long framerangeid) {
        this.framerangeid = framerangeid;
    }

    public String getFramerange() {
        return this.framerange;
    }

    public void setFramerange(String framerange) {
        this.framerange = framerange;
    }

    public String getLoomingposition() {
        return this.loomingposition;
    }

    public void setLoomingposition(String loomingposition) {
        this.loomingposition = loomingposition;
    }

    public Long getQualityid() {
        return this.qualityid;
    }

    public void setQualityid(Long qualityid) {
        this.qualityid = qualityid;
    }

    public String getQuality() {
        return this.quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getQualityreason() {
        return this.qualityreason;
    }

    public void setQualityreason(String qualityreason) {
        this.qualityreason = qualityreason;
    }

    public Long getIpaired() {
        return ipaired;
    }

    public void setIpaired(Long ipaired) {
        this.ipaired = ipaired;
    }

    public String getBearingchock_no() {
        return bearingchock_no;
    }

    public void setBearingchock_no(String bearingchock_no) {
        this.bearingchock_no = bearingchock_no;
    }

	public String getProduction_line() {
		return production_line;
	}

	public void setProduction_line(String production_line) {
		this.production_line = production_line;
	}

	public Long getProduction_line_id() {
		return production_line_id;
	}

	public void setProduction_line_id(Long production_line_id) {
		this.production_line_id = production_line_id;
	}

	public String getWork_layer() {
		return work_layer;
	}

	public void setWork_layer(String work_layer) {
		this.work_layer = work_layer;
	}

	public String getLeave_area() {
		return leave_area;
	}

	public void setLeave_area(String leave_area) {
		this.leave_area = leave_area;
	}

	public String getCapital_time() {
		return capital_time;
	}

	public void setCapital_time(String capital_time) {
		this.capital_time = capital_time;
	}

	public String getObearing_chock() {
		return obearing_chock;
	}

	public void setObearing_chock(String obearing_chock) {
		this.obearing_chock = obearing_chock;
	}

	public String getDbearing_chock() {
		return dbearing_chock;
	}

	public void setDbearing_chock(String dbearing_chock) {
		this.dbearing_chock = dbearing_chock;
	}

	public Long getZb_ifok() {
		return zb_ifok;
	}

	public void setZb_ifok(Long zb_ifok) {
		this.zb_ifok = zb_ifok;
	}

	public Long getFactory_id() {
		return factory_id;
	}

	public void setFactory_id(Long factory_id) {
		this.factory_id = factory_id;
	}

	public Long getMaterial_id() {
		return material_id;
	}

	public void setMaterial_id(Long material_id) {
		this.material_id = material_id;
	}

	public Long getMachine_id() {
		return machine_id;
	}

	public void setMachine_id(Long machine_id) {
		this.machine_id = machine_id;
	}

	public Long getIlevel() {
		return ilevel;
	}

	public void setIlevel(Long ilevel) {
		this.ilevel = ilevel;
	}

	public String getSlevel() {
		return slevel;
	}

	public void setSlevel(String slevel) {
		this.slevel = slevel;
	}

	public String getFrame_scope() {
		return frame_scope;
	}

	public void setFrame_scope(String frame_scope) {
		this.frame_scope = frame_scope;
	}

	public Long getFrame_scope_id() {
		return frame_scope_id;
	}

	public void setFrame_scope_id(Long frame_scope_id) {
		this.frame_scope_id = frame_scope_id;
	}

	public Double getIni_crown_max() {
		return ini_crown_max;
	}

	public void setIni_crown_max(Double ini_crown_max) {
		this.ini_crown_max = ini_crown_max;
	}

	public Double getIni_crown_min() {
		return ini_crown_min;
	}

	public void setIni_crown_min(Double ini_crown_min) {
		this.ini_crown_min = ini_crown_min;
	}

	public Long getSpecial_state_count() {
		return special_state_count;
	}

	public void setSpecial_state_count(Long special_state_count) {
		this.special_state_count = special_state_count;
	}

	public String getRoll_state_value() {
		return roll_state_value;
	}

	public void setRoll_state_value(String roll_state_value) {
		this.roll_state_value = roll_state_value;
	}

	public String getRoll_revolve_value() {
		return roll_revolve_value;
	}

	public void setRoll_revolve_value(String roll_revolve_value) {
		this.roll_revolve_value = roll_revolve_value;
	}

	public String getRoll_special_value() {
		return roll_special_value;
	}

	public void setRoll_special_value(String roll_special_value) {
		this.roll_special_value = roll_special_value;
	}

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }
}