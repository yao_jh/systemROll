package com.my.business.modular.orderservice.entity;

import java.util.Date;
import java.util.List;

import com.my.business.modular.orderservicedetail.entity.OrderServiceDetail;
import com.my.business.sys.common.entity.BaseEntity;

/**
* 业务单据表实体类
* @author  生成器生成
* @date 2020-09-25 17:00:15
*/
public class OrderService extends BaseEntity{
		
	private Long indocno;  //主键
	private String service_name;  //服务名称
	private String method_url;   //方法接口地址
    private String order_entity;  //工单对应的实体类
	
	private List<OrderServiceDetail> detail;   //关联工单集合
    
    
	public void setIndocno(Long indocno){
	    this.indocno = indocno;
	}
	public Long getIndocno(){
	    return this.indocno;
	}
	public void setService_name(String service_name){
	    this.service_name = service_name;
	}
	public String getService_name(){
	    return this.service_name;
	}
	public List<OrderServiceDetail> getDetail() {
		return detail;
	}
	public void setDetail(List<OrderServiceDetail> detail) {
		this.detail = detail;
	}
	public String getMethod_url() {
		return method_url;
	}
	public void setMethod_url(String method_url) {
		this.method_url = method_url;
	}
	public String getOrder_entity() {
		return order_entity;
	}
	public void setOrder_entity(String order_entity) {
		this.order_entity = order_entity;
	}
}