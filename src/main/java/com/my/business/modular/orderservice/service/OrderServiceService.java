package com.my.business.modular.orderservice.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.modular.orderservice.entity.OrderService;
import java.util.List;

/**
* 业务单据表接口服务类
* @author  生成器生成
* @date 2020-09-25 17:00:15
*/
public interface OrderServiceService {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataOrderService(String data,Long userId,String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataOrderServiceOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataOrderServiceMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataOrderService(String data,Long userId,String sname);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataOrderServiceByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataOrderServiceByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<OrderService> findDataOrderService();
	
	/**
	 * 同步数据
	 * */
	public ResultData tbdata();
}
