package com.my.business.modular.orderservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.orderservice.entity.OrderService;
import com.my.business.modular.orderservice.entity.JOrderService;
import com.my.business.modular.orderservice.dao.OrderServiceDao;
import com.my.business.modular.orderservice.service.OrderServiceService;
import com.my.business.modular.orderservicedetail.dao.OrderServiceDetailDao;
import com.my.business.modular.orderservicedetail.entity.OrderServiceDetail;
import com.my.business.modular.rollorderconfig.dao.RollOrderconfigDao;
import com.my.business.modular.rollorderconfig.entity.RollOrderconfig;
import com.my.business.sys.common.entity.ResultData;

/**
* 业务单据表接口具体实现类
* @author  生成器生成
* @date 2020-09-25 17:00:15
*/
@Service
public class OrderServiceServiceImpl implements OrderServiceService {
	
	@Autowired
	private OrderServiceDao orderServiceDao;
	
	@Autowired
	private OrderServiceDetailDao orderServiceDetailDao;
	
	@Autowired
    private RollOrderconfigDao rollOrderconfigDao;
	
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataOrderService(String data,Long userId,String sname){
		try{
			JOrderService jorderService = JSON.parseObject(data,JOrderService.class);
    		OrderService orderService = jorderService.getOrderService();
    		Long indocno = orderServiceDao.findMaxIndecno();
    		if(indocno == null) {
    			indocno = 1L;
    		}else {
    			indocno ++;
    		}
    		
    		//主表保存
    		CodiUtil.newRecord(userId,sname,orderService);
    		orderService.setIndocno(indocno);
            orderServiceDao.insertDataOrderService(orderService);
            
            //子自表保存
            List<OrderServiceDetail> detail = orderService.getDetail();
            if(detail.size() > 0) {
            	for(OrderServiceDetail entity : detail) {
            		entity.setIlinkno(indocno);
            		CodiUtil.newRecord(userId,sname,entity);
            		orderServiceDetailDao.insertDataOrderServiceDetail(entity);
            	}
            }
            
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataOrderServiceOne(Long indocno){
		try {
            orderServiceDao.deleteDataOrderServiceOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataOrderServiceMany(String str_id) {
        try {
        	String sql = "delete order_service where indocno in(" + str_id +")";
            orderServiceDao.deleteDataOrderServiceMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataOrderService(String data,Long userId,String sname){
		try {
			JOrderService jorderService = JSON.parseObject(data,JOrderService.class);
    		OrderService orderService = jorderService.getOrderService();
    		
    		//主表修改保存
    		CodiUtil.editRecord(userId,sname,orderService);
            orderServiceDao.updateDataOrderService(orderService);
            
            //子表修改保存
            List<OrderServiceDetail> detail = orderService.getDetail();
            if(detail.size() > 0) {
            	for(OrderServiceDetail entity : detail) {
            		if(!StringUtils.isEmpty(entity.getIdel()) && entity.getIdel() == 0 ) {  ///表示该数据需要删除
            			orderServiceDetailDao.deleteDataOrderServiceDetailOne(entity.getIndocno());
            		}else {  //否则就是新增的子表数据
            			if(StringUtils.isEmpty(entity.getIndocno())) {
            				CodiUtil.newRecord(userId,sname,entity);
                    		entity.setIlinkno(orderService.getIndocno());
                    		orderServiceDetailDao.insertDataOrderServiceDetail(entity);
            			}
            		}
            	}
            }
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataOrderServiceByPage(String data) {
        try {
        	JOrderService jorderService = JSON.parseObject(data, JOrderService.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jorderService.getPageIndex();
        	Integer pageSize = jorderService.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jorderService.getCondition()){
    			jsonObject  = JSON.parseObject(jorderService.getCondition().toString());
    		}
        	
        	String service_name = null;
            if (!StringUtils.isEmpty(jsonObject.get("service_name"))) {
            	service_name = jsonObject.get("service_name").toString();
            }
    
    		List<OrderService> list = orderServiceDao.findDataOrderServiceByPage((pageIndex-1)*pageSize, pageSize,service_name);
    		Integer count = orderServiceDao.findDataOrderServiceByPageSize(service_name);
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataOrderServiceByIndocno(String data) {
        try {
        	JOrderService jorderService = JSON.parseObject(data, JOrderService.class); 
        	Long indocno = jorderService.getIndocno();
        	
    		OrderService orderService = orderServiceDao.findDataOrderServiceByIndocno(indocno);
    		return ResultData.ResultDataSuccess(orderService);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<OrderService> findDataOrderService(){
		List<OrderService> list = orderServiceDao.findDataOrderService();
		return list;
	}

	@Override
	public ResultData tbdata() {
		try {
			List<RollOrderconfig> list = rollOrderconfigDao.findtb();
			for(RollOrderconfig r : list) {
				System.out.println(r.getOrder_name());
				OrderServiceDetail o = new OrderServiceDetail();
				o.setIlinkno(1L);
				o.setOrder_name(r.getOrder_name());
				o.setOrder_id(r.getIndocno());
				o.setProduction_line(r.getProduction_line()); o.setProduction_line_id(r.getProduction_line_id());
				o.setRoll_type(r.getRoll_type()); o.setRoll_typeid(r.getRoll_typeid());
				o.setFactory(r.getFactory()); o.setFactory_id(r.getFactory_id());
				o.setMaterial(r.getMaterial()); o.setMaterial_id(r.getMaterial_id());
				o.setFrame_no(r.getFrame_no()); o.setFrame_noid(r.getFrame_noid());
				orderServiceDetailDao.insertDataOrderServiceDetail(o);
			}
    		return ResultData.ResultDataSuccess(null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
	};
}
