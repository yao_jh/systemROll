package com.my.business.modular.orderservice.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.orderservice.entity.OrderService;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 业务单据表dao接口
 * @author  生成器生成
 * @date 2020-09-25 17:00:15
 * */
@Mapper
public interface OrderServiceDao {

	/**
	 * 添加记录
	 * @param orderService  对象实体
	 * */
	public void insertDataOrderService(OrderService orderService);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataOrderServiceOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataOrderServiceMany(String value);
	
	/**
	 * 修改记录
	 * @param orderService  对象实体
	 * */
	public void updateDataOrderService(OrderService orderService);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<OrderService> findDataOrderServiceByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize,@Param("service_name") String service_name);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataOrderServiceByPageSize(@Param("service_name") String service_name);
    
    /**
     * 查看最大主键
     * @return 对象数据集合
     */
    public Long findMaxIndecno();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public OrderService findDataOrderServiceByIndocno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<OrderService> findDataOrderService();
	
}
