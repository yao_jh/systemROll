package com.my.business.modular.area.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.area.dao.AreaDao;
import com.my.business.modular.area.entity.Area;
import com.my.business.modular.area.entity.JArea;
import com.my.business.modular.area.service.AreaService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 作业区接口具体实现类
 *
 * @author 生成器生成
 * @date 2019-04-18 10:11:48
 */
@Service
public class AreaServiceImpl implements AreaService {

    @Autowired
    private AreaDao areaDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataArea(String data, Long userId, String sname) {
        try {
            JArea jarea = JSON.parseObject(data, JArea.class);
            Area area = jarea.getArea();
            CodiUtil.newRecord(userId, sname, area);
            areaDao.insertDataArea(area);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataAreaOne(Long indocno) {
        try {
            areaDao.deleteDataAreaOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataAreaMany(String str_id) {
        try {
            String sql = "delete area where indocno in(" + str_id + ")";
            areaDao.deleteDataAreaMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataArea(String data, Long userId, String sname) {
        try {
            JArea jarea = JSON.parseObject(data, JArea.class);
            Area area = jarea.getArea();
            CodiUtil.editRecord(userId, sname, area);
            areaDao.updateDataArea(area);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataAreaByPage(String data) {
        try {
            JArea jarea = JSON.parseObject(data, JArea.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jarea.getPageIndex();
            Integer pageSize = jarea.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jarea.getCondition()) {
                jsonObject = JSON.parseObject(jarea.getCondition().toString());
            }

            String areaname = null;
            if (!StringUtils.isEmpty(jsonObject.get("areaname"))) {
                areaname = jsonObject.get("areaname").toString();
            }

            List<Area> list = areaDao.findDataAreaByPage(pageIndex, pageSize, areaname);
            Integer count = areaDao.findDataAreaByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataAreaByIndocno(String data) {
        try {
            JArea jarea = JSON.parseObject(data, JArea.class);
            Long indocno = jarea.getIndocno();

            Area area = areaDao.findDataAreaByIndocno(indocno);
            return ResultData.ResultDataSuccess(area);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<Area> findDataArea() {
        List<Area> list = areaDao.findDataArea();
        return list;
    }

}
