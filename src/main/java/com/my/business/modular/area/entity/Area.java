package com.my.business.modular.area.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 作业区实体类
 *
 * @author 生成器生成
 * @date 2019-04-18 10:11:48
 */
public class Area extends BaseEntity {

    private Long indocno;  //主键
    private String areaname;  //工作区名称

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getAreaname() {
        return this.areaname;
    }

    public void setAreaname(String areaname) {
        this.areaname = areaname;
    }


}