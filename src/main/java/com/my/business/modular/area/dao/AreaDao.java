package com.my.business.modular.area.dao;

import com.my.business.modular.area.entity.Area;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 作业区dao接口
 *
 * @author 生成器生成
 * @date 2019-04-18 10:11:48
 */
@Mapper
public interface AreaDao {

    /**
     * 添加记录
     *
     * @param area 对象实体
     */
    void insertDataArea(Area area);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataAreaOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataAreaMany(String value);

    /**
     * 修改记录
     *
     * @param area 对象实体
     */
    void updateDataArea(Area area);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<Area> findDataAreaByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize
            , @Param("areaname") String areaname);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataAreaByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    Area findDataAreaByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<Area> findDataArea();

}
