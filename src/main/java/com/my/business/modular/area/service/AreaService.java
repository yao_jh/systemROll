package com.my.business.modular.area.service;

import com.my.business.modular.area.entity.Area;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 作业区接口服务类
 *
 * @author 生成器生成
 * @date 2019-04-18 10:11:48
 */
public interface AreaService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataArea(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataAreaOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataAreaMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataArea(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataAreaByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataAreaByIndocno(String data);

    /**
     * 查看记录
     */
    List<Area> findDataArea();
}
