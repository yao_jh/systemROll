package com.my.business.modular.tbroll.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.tbroll.dao.TbRollDao;
import com.my.business.modular.tbroll.entity.JTbRoll;
import com.my.business.modular.tbroll.entity.TbRoll;
import com.my.business.modular.tbroll.service.TbRollService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 轧辊基本信息接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-07-22 10:57:04
 */
@Service
public class TbRollServiceImpl implements TbRollService {

    @Autowired
    private TbRollDao tbRollDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataTbRoll(String data, Long userId, String sname) {
        try {
            JTbRoll jtbRoll = JSON.parseObject(data, JTbRoll.class);
            TbRoll tbRoll = jtbRoll.getTbRoll();
            CodiUtil.newRecord(userId, sname, tbRoll);
            tbRollDao.insertDataTbRoll(tbRoll);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataTbRollOne(Long indocno) {
        try {
            tbRollDao.deleteDataTbRollOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataTbRollMany(String str_id) {
        try {
            String sql = "delete tb_roll where indocno in(" + str_id + ")";
            tbRollDao.deleteDataTbRollMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataTbRoll(String data, Long userId, String sname) {
        try {
            JTbRoll jtbRoll = JSON.parseObject(data, JTbRoll.class);
            TbRoll tbRoll = jtbRoll.getTbRoll();
            CodiUtil.editRecord(userId, sname, tbRoll);
            tbRollDao.updateDataTbRoll(tbRoll);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataTbRollByPage(String data) {
        try {
            JTbRoll jtbRoll = JSON.parseObject(data, JTbRoll.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jtbRoll.getPageIndex();
            Integer pageSize = jtbRoll.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jtbRoll.getCondition()) {
                jsonObject = JSON.parseObject(jtbRoll.getCondition().toString());
            }

            String roll_mo = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_mo"))) {
                roll_mo = jsonObject.get("roll_mo").toString();
            }

            List<TbRoll> list = tbRollDao.findDataTbRollByPage((pageIndex - 1)*pageSize, pageSize, roll_mo);
            Integer count = tbRollDao.findDataTbRollByPageSize(roll_mo);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataTbRollByIndocno(String data) {
        try {
            JTbRoll jtbRoll = JSON.parseObject(data, JTbRoll.class);
            Long indocno = jtbRoll.getIndocno();

            TbRoll tbRoll = tbRollDao.findDataTbRollByIndocno(indocno);
            return ResultData.ResultDataSuccess(tbRoll);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<TbRoll> findDataTbRoll() {
        List<TbRoll> list = tbRollDao.findDataTbRoll();
        return list;
    }

}
