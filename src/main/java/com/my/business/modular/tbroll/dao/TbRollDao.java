package com.my.business.modular.tbroll.dao;

import com.my.business.modular.tbroll.entity.TbRoll;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轧辊基本信息dao接口
 *
 * @author 生成器生成
 * @date 2020-07-22 10:57:04
 */
@Mapper
public interface TbRollDao {

    /**
     * 添加记录
     *
     * @param tbRoll 对象实体
     */
    void insertDataTbRoll(TbRoll tbRoll);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataTbRollOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataTbRollMany(String value);

    /**
     * 修改记录
     *
     * @param tbRoll 对象实体
     */
    void updateDataTbRoll(TbRoll tbRoll);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<TbRoll> findDataTbRollByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("roll_mo") String roll_mo);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataTbRollByPageSize(@Param("roll_mo") String roll_mo);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    TbRoll findDataTbRollByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<TbRoll> findDataTbRoll();

}
