package com.my.business.modular.tbroll.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.util.Date;

/**
 * 轧辊基本信息实体类
 *
 * @author 生成器生成
 * @date 2020-07-22 11:03:03
 */
public class TbRoll extends BaseEntity {

    private Long indocno;  //主键
    private String roll_mo;  //辊号
    private String ghs;  //供货商
    private String jjd;  //机架段
    private String gzc;  //工作层
    private String address;  //存放地点
    private String rzt;  //状态
    private Double yszj;
    private Double gsmax;
    private Double gsmix;
    private Date tytime;  //投用日期
    private Date bftime;  //报废日期
    private Double bfzj;

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRoll_mo() {
        return this.roll_mo;
    }

    public void setRoll_mo(String roll_mo) {
        this.roll_mo = roll_mo;
    }

    public String getGhs() {
        return this.ghs;
    }

    public void setGhs(String ghs) {
        this.ghs = ghs;
    }

    public String getJjd() {
        return this.jjd;
    }

    public void setJjd(String jjd) {
        this.jjd = jjd;
    }

    public String getGzc() {
        return this.gzc;
    }

    public void setGzc(String gzc) {
        this.gzc = gzc;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRzt() {
        return this.rzt;
    }

    public void setRzt(String rzt) {
        this.rzt = rzt;
    }

    public Double getYszj() {
        return this.yszj;
    }

    public void setYszj(Double yszj) {
        this.yszj = yszj;
    }

    public Double getGsmax() {
        return this.gsmax;
    }

    public void setGsmax(Double gsmax) {
        this.gsmax = gsmax;
    }

    public Double getGsmix() {
        return this.gsmix;
    }

    public void setGsmix(Double gsmix) {
        this.gsmix = gsmix;
    }

    public Date getTytime() {
        return this.tytime;
    }

    public void setTytime(Date tytime) {
        this.tytime = tytime;
    }

    public Date getBftime() {
        return this.bftime;
    }

    public void setBftime(Date bftime) {
        this.bftime = bftime;
    }

    public Double getBfzj() {
        return this.bfzj;
    }

    public void setBfzj(Double bfzj) {
        this.bfzj = bfzj;
    }


}