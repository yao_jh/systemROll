package com.my.business.modular.tbroll.service;

import com.my.business.modular.tbroll.entity.TbRoll;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 轧辊基本信息接口服务类
 *
 * @author 生成器生成
 * @date 2020-07-22 10:57:04
 */
public interface TbRollService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataTbRoll(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataTbRollOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataTbRollMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataTbRoll(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataTbRollByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataTbRollByIndocno(String data);

    /**
     * 查看记录
     */
    List<TbRoll> findDataTbRoll();
}
