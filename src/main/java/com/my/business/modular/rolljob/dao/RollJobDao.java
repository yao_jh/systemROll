package com.my.business.modular.rolljob.dao;

import com.my.business.modular.rolljob.entity.RollJob;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 岗位信息dao接口
 *
 * @author 生成器生成
 * @date 2020-05-15 10:07:53
 */
@Mapper
public interface RollJobDao {

    /**
     * 添加记录
     *
     * @param rollJob 对象实体
     */
    void insertDataRollJob(RollJob rollJob);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollJobOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollJobMany(String value);

    /**
     * 修改记录
     *
     * @param rollJob 对象实体
     */
    void updateDataRollJob(RollJob rollJob);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollJob> findDataRollJobByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("job_name") String job_name);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollJobByPageSize(@Param("job_name") String job_name);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollJob findDataRollJobByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollJob> findDataRollJob();

}
