package com.my.business.modular.rolljob.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 岗位信息实体类
 *
 * @author 生成器生成
 * @date 2020-05-15 10:07:52
 */
public class RollJob extends BaseEntity {

    private Long indocno;  //主键
    private String job_name;  //岗位名称
    private String snotes;  //备注

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getJob_name() {
        return this.job_name;
    }

    public void setJob_name(String job_name) {
        this.job_name = job_name;
    }

    public String getSnotes() {
        return this.snotes;
    }

    public void setSnotes(String snotes) {
        this.snotes = snotes;
    }


}