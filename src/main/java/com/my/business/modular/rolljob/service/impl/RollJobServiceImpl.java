package com.my.business.modular.rolljob.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rolljob.dao.RollJobDao;
import com.my.business.modular.rolljob.entity.JRollJob;
import com.my.business.modular.rolljob.entity.RollJob;
import com.my.business.modular.rolljob.service.RollJobService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 岗位信息接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-05-15 10:07:53
 */
@Service
public class RollJobServiceImpl implements RollJobService {

    @Autowired
    private RollJobDao rollJobDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollJob(String data, Long userId, String sname) {
        try {
            JRollJob jrollJob = JSON.parseObject(data, JRollJob.class);
            RollJob rollJob = jrollJob.getRollJob();
            CodiUtil.newRecord(userId, sname, rollJob);
            rollJobDao.insertDataRollJob(rollJob);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollJobOne(Long indocno) {
        try {
            rollJobDao.deleteDataRollJobOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollJobMany(String str_id) {
        try {
            String sql = "delete roll_job where indocno in(" + str_id + ")";
            rollJobDao.deleteDataRollJobMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollJob(String data, Long userId, String sname) {
        try {
            JRollJob jrollJob = JSON.parseObject(data, JRollJob.class);
            RollJob rollJob = jrollJob.getRollJob();
            CodiUtil.editRecord(userId, sname, rollJob);
            rollJobDao.updateDataRollJob(rollJob);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollJobByPage(String data) {
        try {
            JRollJob jrollJob = JSON.parseObject(data, JRollJob.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollJob.getPageIndex();
            Integer pageSize = jrollJob.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollJob.getCondition()) {
                jsonObject = JSON.parseObject(jrollJob.getCondition().toString());
            }

            String job_name = null;
            if (null != jsonObject.get("job_name")) {
                job_name = jsonObject.get("job_name").toString();
            }

            List<RollJob> list = rollJobDao.findDataRollJobByPage(pageIndex, pageSize, job_name);
            Integer count = rollJobDao.findDataRollJobByPageSize(job_name);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollJobByIndocno(String data) {
        try {
            JRollJob jrollJob = JSON.parseObject(data, JRollJob.class);
            Long indocno = jrollJob.getIndocno();

            RollJob rollJob = rollJobDao.findDataRollJobByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollJob);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollJob> findDataRollJob() {
        List<RollJob> list = rollJobDao.findDataRollJob();
        return list;
    }

}
