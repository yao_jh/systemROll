package com.my.business.modular.company.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.company.dao.CompanyDao;
import com.my.business.modular.company.entity.Company;
import com.my.business.modular.company.entity.JCompany;
import com.my.business.modular.company.service.CompanyService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 厂区接口具体实现类
 *
 * @author 生成器生成
 * @date 2019-04-18 10:14:30
 */
@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyDao companyDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataCompany(String data, Long userId, String sname) {
        try {
            JCompany jcompany = JSON.parseObject(data, JCompany.class);
            Company company = jcompany.getCompany();
            CodiUtil.newRecord(userId, sname, company);
            companyDao.insertDataCompany(company);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataCompanyOne(Long indocno) {
        try {
            companyDao.deleteDataCompanyOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataCompanyMany(String str_id) {
        try {
            String sql = "delete from company where indocno in(" + str_id + ")";
            companyDao.deleteDataCompanyMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataCompany(String data, Long userId, String sname) {
        try {
            JCompany jcompany = JSON.parseObject(data, JCompany.class);
            Company company = jcompany.getCompany();
            CodiUtil.editRecord(userId, sname, company);
            companyDao.updateDataCompany(company);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataCompanyByPage(String data) {
        try {
            JCompany jcompany = JSON.parseObject(data, JCompany.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jcompany.getPageIndex();
            Integer pageSize = jcompany.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jcompany.getCondition()) {
                jsonObject = JSON.parseObject(jcompany.getCondition().toString());
            }

            String companyname = null;
            if (!StringUtils.isEmpty(jsonObject.get("companyname"))) {
                companyname = jsonObject.get("companyname").toString();
            }

            List<Company> list = companyDao.findDataCompanyByPage(pageIndex, pageSize, companyname);
            Integer count = companyDao.findDataCompanyByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataCompanyByIndocno(String data) {
        try {
            JCompany jcompany = JSON.parseObject(data, JCompany.class);
            Long indocno = jcompany.getIndocno();

            Company company = companyDao.findDataCompanyByIndocno(indocno);
            return ResultData.ResultDataSuccess(company);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<Company> findDataCompany() {
        List<Company> list = companyDao.findDataCompany();
        return list;
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<Company> findDataCompanyBetween(Integer minvalue, Integer maxvalue) {
        List<Company> list = companyDao.findDataCompanyBetween(minvalue, maxvalue);
        return list;
    }

}
