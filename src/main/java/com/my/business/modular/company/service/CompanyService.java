package com.my.business.modular.company.service;

import com.my.business.modular.company.entity.Company;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 厂区接口服务类
 *
 * @author 生成器生成
 * @date 2019-04-18 10:14:30
 */
public interface CompanyService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataCompany(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataCompanyOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataCompanyMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataCompany(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataCompanyByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataCompanyByIndocno(String data);

    /**
     * 查看记录
     */
    List<Company> findDataCompany();

    /**
     * 查看记录
     */
    List<Company> findDataCompanyBetween(Integer minvalue, Integer maxvalue);
}
