package com.my.business.modular.company.dao;

import com.my.business.modular.company.entity.Company;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 厂区dao接口
 *
 * @author 生成器生成
 * @date 2019-04-18 10:14:30
 */
@Mapper
public interface CompanyDao {

    /**
     * 添加记录
     *
     * @param company 对象实体
     */
    void insertDataCompany(Company company);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataCompanyOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataCompanyMany(String value);

    /**
     * 修改记录
     *
     * @param company 对象实体
     */
    void updateDataCompany(Company company);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<Company> findDataCompanyByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize
            , @Param("companyname") String companyname);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataCompanyByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    Company findDataCompanyByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<Company> findDataCompany();

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<Company> findDataCompanyBetween(@Param("minvalue") Integer minvalue, @Param("maxvalue") Integer maxvalue);

}
