package com.my.business.modular.company.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 厂区实体类
 *
 * @author 生成器生成
 * @date 2019-04-18 10:14:29
 */
public class Company extends BaseEntity {

    private Long indocno;  //主键
    private String companyname;  //厂区名称

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getCompanyname() {
        return this.companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }


}