package com.my.business.modular.rollpreparationstandard.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.rollpreparationstandard.entity.RollPreparationStandard;
import com.my.business.modular.rollpreparationstandard.entity.JRollPreparationStandard;
import com.my.business.modular.rollpreparationstandard.dao.RollPreparationStandardDao;
import com.my.business.modular.rollpreparationstandard.service.RollPreparationStandardService;
import com.my.business.sys.common.entity.ResultData;

/**
* 备辊配置表接口具体实现类
* @author  生成器生成
* @date 2020-10-21 17:43:05
*/
@Service
public class RollPreparationStandardServiceImpl implements RollPreparationStandardService {
	
	@Autowired
	private RollPreparationStandardDao rollPreparationStandardDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollPreparationStandard(String data,Long userId,String sname){
		try{
			JRollPreparationStandard jrollPreparationStandard = JSON.parseObject(data,JRollPreparationStandard.class);
    		RollPreparationStandard rollPreparationStandard = jrollPreparationStandard.getRollPreparationStandard();
    		CodiUtil.newRecord(userId,sname,rollPreparationStandard);
            rollPreparationStandardDao.insertDataRollPreparationStandard(rollPreparationStandard);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollPreparationStandardOne(Long indocno){
		try {
            rollPreparationStandardDao.deleteDataRollPreparationStandardOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollPreparationStandardMany(String str_id) {
        try {
        	String sql = "delete roll_preparation_standard where indocno in(" + str_id +")";
            rollPreparationStandardDao.deleteDataRollPreparationStandardMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataRollPreparationStandard(String data,Long userId,String sname){
		try {
			JRollPreparationStandard jrollPreparationStandard = JSON.parseObject(data,JRollPreparationStandard.class);
    		RollPreparationStandard rollPreparationStandard = jrollPreparationStandard.getRollPreparationStandard();
        	CodiUtil.editRecord(userId,sname,rollPreparationStandard);
            rollPreparationStandardDao.updateDataRollPreparationStandard(rollPreparationStandard);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPreparationStandardByPage(String data) {
        try {
        	JRollPreparationStandard jrollPreparationStandard = JSON.parseObject(data, JRollPreparationStandard.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jrollPreparationStandard.getPageIndex();
        	Integer pageSize = jrollPreparationStandard.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jrollPreparationStandard.getCondition()){
    			jsonObject  = JSON.parseObject(jrollPreparationStandard.getCondition().toString());
    		}
        	
        	String module_name = null;
        	String module_coding = null;
        	String sort_order = null;
            if (!StringUtils.isEmpty(jsonObject.get("module_name"))) {
            	module_name = jsonObject.get("module_name").toString();
            }
            if (!StringUtils.isEmpty(jsonObject.get("module_coding"))) {
            	module_coding = jsonObject.get("module_coding").toString();
            }
            if (!StringUtils.isEmpty(jsonObject.get("sort_order"))) {
            	sort_order = jsonObject.get("sort_order").toString();
            }
        	
    
    		List<RollPreparationStandard> list = rollPreparationStandardDao.findDataRollPreparationStandardByPage((pageIndex-1)*pageSize, pageSize,module_name,module_coding,sort_order);
    		Integer count = rollPreparationStandardDao.findDataRollPreparationStandardByPageSize(module_name,module_coding,sort_order);
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPreparationStandardByIndocno(String data) {
        try {
        	JRollPreparationStandard jrollPreparationStandard = JSON.parseObject(data, JRollPreparationStandard.class); 
        	Long indocno = jrollPreparationStandard.getIndocno();
        	
    		RollPreparationStandard rollPreparationStandard = rollPreparationStandardDao.findDataRollPreparationStandardByIndocno(indocno);
    		return ResultData.ResultDataSuccess(rollPreparationStandard);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<RollPreparationStandard> findDataRollPreparationStandard(){
		List<RollPreparationStandard> list = rollPreparationStandardDao.findDataRollPreparationStandard();
		return list;
	};
}
