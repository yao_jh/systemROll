package com.my.business.modular.rollpreparationstandard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.modular.rollpreparationstandard.entity.RollPreparationStandard;
import com.my.business.modular.rollpreparationstandard.entity.JRollPreparationStandard;
import com.my.business.modular.rollpreparationstandard.service.RollPreparationStandardService;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import com.my.business.sys.common.entity.ResultData;
import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.my.business.util.CodiUtil;
import org.springframework.web.bind.annotation.RequestHeader;


/**
* 备辊配置表控制器层
* @author  生成器生成
* @date 2020-10-21 17:43:05
*/
@RestController
@RequestMapping("/rollPreparationStandard")
public class RollPreparationStandardController {

	@Autowired
	private RollPreparationStandardService rollPreparationStandardService;
	
	/**
	 * 添加记录
	 * @param userId 用户id
     * @param sname 用户姓名
	 * */
	@CrossOrigin
	@RequestMapping(value={"/insert"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData insertDataRollPreparationStandard(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return rollPreparationStandardService.insertDataRollPreparationStandard(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
	 * 根据主键删除对象
	 * @param data json字符串
	 * */
	@CrossOrigin
	@RequestMapping(value={"/deleteOne"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData deleteDataRollPreparationStandardOne(@RequestBody String data){
		try{
    		JRollPreparationStandard jrollPreparationStandard = JSON.parseObject(data,JRollPreparationStandard.class);
    		return rollPreparationStandardService.deleteDataRollPreparationStandardOne(jrollPreparationStandard.getIndocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
    	try{
    		JRollPreparationStandard jrollPreparationStandard = JSON.parseObject(data,JRollPreparationStandard.class);
    		return rollPreparationStandardService.deleteDataRollPreparationStandardMany(jrollPreparationStandard.getStr_indocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
    }
	
	/**
     * 修改记录
     * @param data json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
	@RequestMapping(value={"/update"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData updateDataRollPreparationStandard(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return rollPreparationStandardService.updateDataRollPreparationStandard(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
     * 分页查看记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollPreparationStandardByPage(@RequestBody String data) {
        return rollPreparationStandardService.findDataRollPreparationStandardByPage(data);
    }
    
    /**
     * 根据主键查询单条记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollPreparationStandardByIndocno(@RequestBody String data) {
        return rollPreparationStandardService.findDataRollPreparationStandardByIndocno(data);
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	@CrossOrigin
	@RequestMapping(value={"/findAll"}, method=RequestMethod.POST)
	public List<RollPreparationStandard> findDataRollPreparationStandard(){
		return rollPreparationStandardService.findDataRollPreparationStandard();
	};
}
