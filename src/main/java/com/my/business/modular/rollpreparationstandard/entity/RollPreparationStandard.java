package com.my.business.modular.rollpreparationstandard.entity;

import java.util.Date;
import com.my.business.sys.common.entity.BaseEntity;

/**
* 备辊配置表实体类
* @author  生成器生成
* @date 2020-10-21 17:43:05
*/
public class RollPreparationStandard extends BaseEntity{
		
	private Long indocno;  //主键
	private String module_name;  //模块名称
	private String module_coding;  //模块编码
	private Long field_max;  //上区间
	private Long field_min;  //下区间
	private String field_name;  //字段名称
	private Long stand_id;  //机架范围id
	private String stand;  //机架范围
	private Long sort_order;  //备辊排列顺序
	private Long field1;  //
	private Long field2;  //
	private Long field3;  //
	private String snote;  //备注
    
    
	public void setIndocno(Long indocno){
	    this.indocno = indocno;
	}
	public Long getIndocno(){
	    return this.indocno;
	}
	public void setModule_name(String module_name){
	    this.module_name = module_name;
	}
	public String getModule_name(){
	    return this.module_name;
	}
	public void setModule_coding(String module_coding){
	    this.module_coding = module_coding;
	}
	public String getModule_coding(){
	    return this.module_coding;
	}
	public void setField_max(Long field_max){
	    this.field_max = field_max;
	}
	public Long getField_max(){
	    return this.field_max;
	}
	public void setField_min(Long field_min){
	    this.field_min = field_min;
	}
	public Long getField_min(){
	    return this.field_min;
	}
	public void setField_name(String field_name){
	    this.field_name = field_name;
	}
	public String getField_name(){
	    return this.field_name;
	}
	public void setStand_id(Long stand_id){
	    this.stand_id = stand_id;
	}
	public Long getStand_id(){
	    return this.stand_id;
	}
	public void setStand(String stand){
	    this.stand = stand;
	}
	public String getStand(){
	    return this.stand;
	}
	public void setSort_order(Long sort_order){
	    this.sort_order = sort_order;
	}
	public Long getSort_order(){
	    return this.sort_order;
	}
	public void setField1(Long field1){
	    this.field1 = field1;
	}
	public Long getField1(){
	    return this.field1;
	}
	public void setField2(Long field2){
	    this.field2 = field2;
	}
	public Long getField2(){
	    return this.field2;
	}
	public void setField3(Long field3){
	    this.field3 = field3;
	}
	public Long getField3(){
	    return this.field3;
	}
	public void setSnote(String snote){
	    this.snote = snote;
	}
	public String getSnote(){
	    return this.snote;
	}

    
}