package com.my.business.modular.rollpreparationstandard.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.rollpreparationstandard.entity.RollPreparationStandard;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 备辊配置表dao接口
 * @author  生成器生成
 * @date 2020-10-21 17:43:05
 * */
@Mapper
public interface RollPreparationStandardDao {

	/**
	 * 添加记录
	 * @param rollPreparationStandard  对象实体
	 * */
	public void insertDataRollPreparationStandard(RollPreparationStandard rollPreparationStandard);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataRollPreparationStandardOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataRollPreparationStandardMany(String value);
	
	/**
	 * 修改记录
	 * @param rollPreparationStandard  对象实体
	 * */
	public void updateDataRollPreparationStandard(RollPreparationStandard rollPreparationStandard);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<RollPreparationStandard> findDataRollPreparationStandardByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize,@Param("module_name") String module_name,@Param("module_coding") String module_coding,@Param("sort_order") String sort_order);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataRollPreparationStandardByPageSize(@Param("module_name") String module_name,@Param("module_coding") String module_coding,@Param("sort_order") String sort_order);
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public RollPreparationStandard findDataRollPreparationStandardByIndocno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<RollPreparationStandard> findDataRollPreparationStandard();
	
}
