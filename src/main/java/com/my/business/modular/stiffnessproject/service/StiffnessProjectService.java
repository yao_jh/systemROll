package com.my.business.modular.stiffnessproject.service;

import com.my.business.modular.stiffnessproject.entity.StiffnessProject;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 轧辊刚度评价标准——项目接口服务类
 *
 * @author 生成器生成
 * @date 2020-09-08 10:45:42
 */
public interface StiffnessProjectService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataStiffnessProject(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataStiffnessProjectOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataStiffnessProjectMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataStiffnessProject(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataStiffnessProjectByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataStiffnessProjectByIndocno(String data);

    /**
     * 查看记录
     */
    List<StiffnessProject> findDataStiffnessProject();

    /**
     * 根据外键查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataStiffnessProjectByIlinkno(String data);
}
