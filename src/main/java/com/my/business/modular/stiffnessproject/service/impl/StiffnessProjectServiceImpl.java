package com.my.business.modular.stiffnessproject.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.stiffnessproject.dao.StiffnessProjectDao;
import com.my.business.modular.stiffnessproject.entity.JStiffnessProject;
import com.my.business.modular.stiffnessproject.entity.StiffnessProject;
import com.my.business.modular.stiffnessproject.service.StiffnessProjectService;
import com.my.business.modular.stiffnessscore.dao.StiffnessScoreDao;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 轧辊刚度评价标准——项目接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-08 10:45:42
 */
@Service
public class StiffnessProjectServiceImpl implements StiffnessProjectService {

    @Autowired
    private StiffnessProjectDao stiffnessProjectDao;
    @Autowired
    private StiffnessScoreDao stiffnessScoreDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataStiffnessProject(String data, Long userId, String sname) {
        try {
            JStiffnessProject jstiffnessProject = JSON.parseObject(data, JStiffnessProject.class);
            StiffnessProject stiffnessProject = jstiffnessProject.getStiffnessProject();
            stiffnessProjectDao.insertDataStiffnessProject(stiffnessProject);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataStiffnessProjectOne(Long indocno) {
        try {
            stiffnessProjectDao.deleteDataStiffnessProjectOne(indocno);
            stiffnessScoreDao.deleteDataStiffnessScoreByilinkno(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataStiffnessProjectMany(String str_id) {
        try {
            String sql = "delete stiffness_project where indocno in(" + str_id + ")";
            stiffnessProjectDao.deleteDataStiffnessProjectMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataStiffnessProject(String data, Long userId, String sname) {
        try {
            JStiffnessProject jstiffnessProject = JSON.parseObject(data, JStiffnessProject.class);
            StiffnessProject stiffnessProject = jstiffnessProject.getStiffnessProject();
            CodiUtil.editRecord(userId, sname, stiffnessProject);
            stiffnessProjectDao.updateDataStiffnessProject(stiffnessProject);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataStiffnessProjectByPage(String data) {
        try {
            JStiffnessProject jstiffnessProject = JSON.parseObject(data, JStiffnessProject.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jstiffnessProject.getPageIndex();
            Integer pageSize = jstiffnessProject.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jstiffnessProject.getCondition()) {
                jsonObject = JSON.parseObject(jstiffnessProject.getCondition().toString());
            }

            List<StiffnessProject> list = stiffnessProjectDao.findDataStiffnessProjectByPage((pageIndex - 1) * pageSize, pageSize);
            Integer count = stiffnessProjectDao.findDataStiffnessProjectByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataStiffnessProjectByIndocno(String data) {
        try {
            JStiffnessProject jstiffnessProject = JSON.parseObject(data, JStiffnessProject.class);
            Long indocno = jstiffnessProject.getIndocno();

            StiffnessProject stiffnessProject = stiffnessProjectDao.findDataStiffnessProjectByIndocno(indocno);
            return ResultData.ResultDataSuccess(stiffnessProject);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<StiffnessProject> findDataStiffnessProject() {
        List<StiffnessProject> list = stiffnessProjectDao.findDataStiffnessProject();
        return list;
    }

    /**
     * 根据外键查询多条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataStiffnessProjectByIlinkno(String data) {
        try {
            JStiffnessProject jstiffnessProject = JSON.parseObject(data, JStiffnessProject.class);
            Long ilinkno = jstiffnessProject.getStiffnessProject().getIlinkno();
            List<StiffnessProject> list = stiffnessProjectDao.findDataStiffnessProjectByIlinkno(ilinkno);
            return ResultData.ResultDataSuccess(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }
}
