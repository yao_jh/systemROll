package com.my.business.modular.stiffnessproject.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.stiffnessproject.entity.JStiffnessProject;
import com.my.business.modular.stiffnessproject.entity.StiffnessProject;
import com.my.business.modular.stiffnessproject.service.StiffnessProjectService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 轧辊刚度评价标准——项目控制器层
 *
 * @author 生成器生成
 * @date 2020-09-08 10:45:42
 */
@RestController
@RequestMapping("/stiffnessProject")
public class StiffnessProjectController {

    @Autowired
    private StiffnessProjectService stiffnessProjectService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataStiffnessProject(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return stiffnessProjectService.insertDataStiffnessProject(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataStiffnessProjectOne(@RequestBody String data) {
        try {
            JStiffnessProject jstiffnessProject = JSON.parseObject(data, JStiffnessProject.class);
            return stiffnessProjectService.deleteDataStiffnessProjectOne(jstiffnessProject.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JStiffnessProject jstiffnessProject = JSON.parseObject(data, JStiffnessProject.class);
            return stiffnessProjectService.deleteDataStiffnessProjectMany(jstiffnessProject.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataStiffnessProject(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return stiffnessProjectService.updateDataStiffnessProject(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataStiffnessProjectByPage(@RequestBody String data) {
        return stiffnessProjectService.findDataStiffnessProjectByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataStiffnessProjectByIndocno(@RequestBody String data) {
        return stiffnessProjectService.findDataStiffnessProjectByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<StiffnessProject> findDataStiffnessProject() {
        return stiffnessProjectService.findDataStiffnessProject();
    }

    /**
     * 根据外键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIlinkno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataStiffnessProjectByIlinkno(@RequestBody String data) {
        return stiffnessProjectService.findDataStiffnessProjectByIlinkno(data);
    }
}
