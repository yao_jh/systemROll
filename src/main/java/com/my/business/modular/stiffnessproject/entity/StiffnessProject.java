package com.my.business.modular.stiffnessproject.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轧辊刚度评价标准——项目实体类
 *
 * @author 生成器生成
 * @date 2020-09-08 10:45:42
 */
public class StiffnessProject extends BaseEntity {

    private Long indocno;  //主键
    private Long ilinkno;  //外键
    private String project_name;  //项目名称
    private Long weight;  //权重
    private String standard;  //控制标准
    private String field_name;  //关联字段名称

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getIlinkno() {
        return this.ilinkno;
    }

    public void setIlinkno(Long ilinkno) {
        this.ilinkno = ilinkno;
    }

    public String getProject_name() {
        return this.project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public Long getWeight() {
        return this.weight;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }

    public String getStandard() {
        return this.standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public String getField_name() {
        return this.field_name;
    }

    public void setField_name(String field_name) {
        this.field_name = field_name;
    }


}