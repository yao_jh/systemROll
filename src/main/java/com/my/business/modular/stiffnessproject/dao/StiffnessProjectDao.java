package com.my.business.modular.stiffnessproject.dao;

import com.my.business.modular.stiffnessproject.entity.StiffnessProject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轧辊刚度评价标准——项目dao接口
 *
 * @author 生成器生成
 * @date 2020-09-08 10:45:42
 */
@Mapper
public interface StiffnessProjectDao {

    /**
     * 添加记录
     *
     * @param stiffnessProject 对象实体
     */
    void insertDataStiffnessProject(StiffnessProject stiffnessProject);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataStiffnessProjectOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataStiffnessProjectMany(String value);

    /**
     * 修改记录
     *
     * @param stiffnessProject 对象实体
     */
    void updateDataStiffnessProject(StiffnessProject stiffnessProject);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<StiffnessProject> findDataStiffnessProjectByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataStiffnessProjectByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    StiffnessProject findDataStiffnessProjectByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<StiffnessProject> findDataStiffnessProject();

    /**
     * 根据外键查看记录
     *
     * @return 对象数据集合
     */
    List<StiffnessProject> findDataStiffnessProjectByIlinkno(@Param("ilinkno") Long ilinkno);

    /**
     * 根据外键删除记录
     *
     * @return 对象数据集合
     */
    void deleteDataStiffnessProjectByilinkno(@Param("ilinkno") Long ilinkno);

    StiffnessProject findDataStiffnessProjectByIlinknoAndFieldName(@Param("ilinkno") Long ilinkno,@Param("field_name") String field);
}
