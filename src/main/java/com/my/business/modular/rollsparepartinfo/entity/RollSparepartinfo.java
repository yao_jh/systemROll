package com.my.business.modular.rollsparepartinfo.entity;

import com.my.business.sys.common.entity.BaseEntity;
import lombok.Data;

/**
 * 备件信息表实体类
 *
 * @author 生成器生成
 * @date 2020-08-03 15:15:40
 */
@Data
public class RollSparepartinfo extends BaseEntity {

    private Long indocno;  //主键
    private String produce_line;  //产线
    private String deposit_locale;  //存放地点
    private String assignments_section;  //所辖作业区
    private String spare_part_no;  //备件编码
    private String spare_part_name;  //备件名称
    private String drawing_no;  //图号
    private String device_type;  // 设备类型
    private String specifications;  //规格型号
    private Long deposit_number;  //库存数量
    private Long spare_number;  //新备件数量
    private Double spare_monovalent; // 新备件单价
    private Double spare_total_amount;  // 新备件总价
    private Long spare_repaired_number;  //已修复件数量
    private Double spare_repaire_monovalent;  // 修复单价
    private Double spare_repaire_total_amount;  // 修复总价
    private Long spare_need_repaire_number;  //待修复件数量
    private Long spare_broken_number;  //报废件数量
    private String snote;  //备注

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getProduce_line() {
        return produce_line;
    }

    public void setProduce_line(String produce_line) {
        this.produce_line = produce_line;
    }

    public String getDeposit_locale() {
        return deposit_locale;
    }

    public void setDeposit_locale(String deposit_locale) {
        this.deposit_locale = deposit_locale;
    }

    public String getAssignments_section() {
        return assignments_section;
    }

    public void setAssignments_section(String assignments_section) {
        this.assignments_section = assignments_section;
    }

    public String getSpare_part_no() {
        return spare_part_no;
    }

    public void setSpare_part_no(String spare_part_no) {
        this.spare_part_no = spare_part_no;
    }

    public String getSpare_part_name() {
        return spare_part_name;
    }

    public void setSpare_part_name(String spare_part_name) {
        this.spare_part_name = spare_part_name;
    }

    public String getDrawing_no() {
        return drawing_no;
    }

    public void setDrawing_no(String drawing_no) {
        this.drawing_no = drawing_no;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public Long getDeposit_number() {
        return deposit_number;
    }

    public void setDeposit_number(Long deposit_number) {
        this.deposit_number = deposit_number;
    }

    public Long getSpare_number() {
        return spare_number;
    }

    public void setSpare_number(Long spare_number) {
        this.spare_number = spare_number;
    }

    public Double getSpare_monovalent() {
        return spare_monovalent;
    }

    public void setSpare_monovalent(Double spare_monovalent) {
        this.spare_monovalent = spare_monovalent;
    }

    public Double getSpare_total_amount() {
        return spare_total_amount;
    }

    public void setSpare_total_amount(Double spare_total_amount) {
        this.spare_total_amount = spare_total_amount;
    }

    public Long getSpare_repaired_number() {
        return spare_repaired_number;
    }

    public void setSpare_repaired_number(Long spare_repaired_number) {
        this.spare_repaired_number = spare_repaired_number;
    }

    public Double getSpare_repaire_monovalent() {
        return spare_repaire_monovalent;
    }

    public void setSpare_repaire_monovalent(Double spare_repaire_monovalent) {
        this.spare_repaire_monovalent = spare_repaire_monovalent;
    }

    public Double getSpare_repaire_total_amount() {
        return spare_repaire_total_amount;
    }

    public void setSpare_repaire_total_amount(Double spare_repaire_total_amount) {
        this.spare_repaire_total_amount = spare_repaire_total_amount;
    }

    public Long getSpare_need_repaire_number() {
        return spare_need_repaire_number;
    }

    public void setSpare_need_repaire_number(Long spare_need_repaire_number) {
        this.spare_need_repaire_number = spare_need_repaire_number;
    }

    public Long getSpare_broken_number() {
        return spare_broken_number;
    }

    public void setSpare_broken_number(Long spare_broken_number) {
        this.spare_broken_number = spare_broken_number;
    }

    public String getSnote() {
        return snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }
}