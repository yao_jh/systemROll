package com.my.business.modular.rollsparepartinfo.dao;

import com.my.business.modular.rollsparepartinfo.entity.RollSparepartinfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 备件信息表dao接口
 *
 * @author 生成器生成
 * @date 2020-08-03 15:15:41
 */
@Mapper
public interface RollSparepartinfoDao {

    /**
     * 添加记录
     *
     * @param rollSparepartinfo 对象实体
     */
    void insertDataRollSparepartinfo(RollSparepartinfo rollSparepartinfo);

    /**
     * 修改记录
     *
     * @param rollSparepartinfo 对象实体
     */
    void updateDataRollSparepartinfo(RollSparepartinfo rollSparepartinfo);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollSparepartinfo> findDataRollSparepartinfoByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollSparepartinfoByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollSparepartinfo findDataRollSparepartinfoByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollSparepartinfo> findDataRollSparepartinfo();

}
