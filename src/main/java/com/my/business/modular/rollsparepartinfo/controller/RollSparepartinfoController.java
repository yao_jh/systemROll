package com.my.business.modular.rollsparepartinfo.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollsparepartinfo.entity.RollSparepartinfo;
import com.my.business.modular.rollsparepartinfo.service.RollSparepartinfoService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 备件信息表控制器层
 *
 * @author 生成器生成
 * @date 2020-08-03 15:15:43
 */
@RestController
@RequestMapping("/rollSparepartinfo")
public class RollSparepartinfoController {

    @Autowired
    private RollSparepartinfoService rollSparepartinfoService;

    /**
     * 添加记录
     *
     * @param data       对象实体
     * @param loginToken 用户token
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollSparepartinfo(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollSparepartinfoService.insertDataRollSparepartinfo(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollSparepartinfo(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollSparepartinfoService.updateDataRollSparepartinfo(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollSparepartinfoByPage(@RequestBody String data) {
        return rollSparepartinfoService.findDataRollSparepartinfoByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollSparepartinfoByIndocno(@RequestBody String data) {
        return rollSparepartinfoService.findDataRollSparepartinfoByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollSparepartinfo> findDataRollSparepartinfo() {
        return rollSparepartinfoService.findDataRollSparepartinfo();
    }
}
