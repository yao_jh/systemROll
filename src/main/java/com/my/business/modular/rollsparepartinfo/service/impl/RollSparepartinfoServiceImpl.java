package com.my.business.modular.rollsparepartinfo.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollsparepartinfo.dao.RollSparepartinfoDao;
import com.my.business.modular.rollsparepartinfo.entity.JRollSparepartinfo;
import com.my.business.modular.rollsparepartinfo.entity.RollSparepartinfo;
import com.my.business.modular.rollsparepartinfo.service.RollSparepartinfoService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 备件信息表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-03 15:15:42
 */
@Service
public class RollSparepartinfoServiceImpl implements RollSparepartinfoService {

    @Autowired
    private RollSparepartinfoDao rollSparepartinfoDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollSparepartinfo(String data, Long userId, String sname) {
        try {
            JRollSparepartinfo jrollSparepartinfo = JSON.parseObject(data, JRollSparepartinfo.class);
            RollSparepartinfo rollSparepartinfo = jrollSparepartinfo.getRollSparepartinfo();
            CodiUtil.newRecord(userId, sname, rollSparepartinfo);
            rollSparepartinfoDao.insertDataRollSparepartinfo(rollSparepartinfo);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   对象实体
     */
    public ResultData updateDataRollSparepartinfo(String data, Long userId, String sname) {
        try {
            JRollSparepartinfo jrollSparepartinfo = JSON.parseObject(data, JRollSparepartinfo.class);
            RollSparepartinfo rollSparepartinfo = jrollSparepartinfo.getRollSparepartinfo();
            CodiUtil.editRecord(userId, sname, rollSparepartinfo);
            rollSparepartinfoDao.updateDataRollSparepartinfo(rollSparepartinfo);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId            用户id
     * @param sname             用户姓名
     * @param rollSparepartinfo 对象实体
     */
    public ResultData updateRollSparepartinfo(RollSparepartinfo rollSparepartinfo, Long userId, String sname) {
        try {
            CodiUtil.editRecord(userId, sname, rollSparepartinfo);
            rollSparepartinfoDao.updateDataRollSparepartinfo(rollSparepartinfo);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollSparepartinfoByPage(String data) {
        try {
            JRollSparepartinfo jrollSparepartinfo = JSON.parseObject(data, JRollSparepartinfo.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollSparepartinfo.getPageIndex();
            Integer pageSize = jrollSparepartinfo.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollSparepartinfo.getCondition()) {
                jsonObject = JSON.parseObject(jrollSparepartinfo.getCondition().toString());
            }

            List<RollSparepartinfo> list = rollSparepartinfoDao.findDataRollSparepartinfoByPage((pageIndex-1)*pageSize, pageSize);
            Integer count = rollSparepartinfoDao.findDataRollSparepartinfoByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollSparepartinfoByIndocno(String data) {
        try {
            JRollSparepartinfo jRollSparepartinfo = JSON.parseObject(data, JRollSparepartinfo.class);
            Long indocno = jRollSparepartinfo.getIndocno();
            RollSparepartinfo rollSparepartinfo = rollSparepartinfoDao.findDataRollSparepartinfoByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollSparepartinfo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollSparepartinfo> findDataRollSparepartinfo() {
        List<RollSparepartinfo> list = rollSparepartinfoDao.findDataRollSparepartinfo();
        return list;
    }

}
