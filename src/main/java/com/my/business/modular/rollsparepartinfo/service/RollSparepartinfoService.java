package com.my.business.modular.rollsparepartinfo.service;

import com.my.business.modular.rollsparepartinfo.entity.RollSparepartinfo;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 备件信息表接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-03 15:15:41
 */
public interface RollSparepartinfoService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollSparepartinfo(String data, Long userId, String sname);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollSparepartinfo(String data, Long userId, String sname);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateRollSparepartinfo(RollSparepartinfo rollSparepartinfo, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollSparepartinfoByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param indocno 分页参数字符串
     */
    ResultData findDataRollSparepartinfoByIndocno(String indocno);

    /**
     * 查看记录
     */
    List<RollSparepartinfo> findDataRollSparepartinfo();
}
