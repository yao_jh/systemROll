package com.my.business.modular.basepressureparts.dao;

import com.my.business.modular.basepressureparts.entity.BasePressureParts;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轴承座承压件管理dao接口
 *
 * @author 生成器生成
 */
@Mapper
public interface BasePressurePartsDao {

    /**
     * 添加记录
     *
     * @param basePressureParts 对象实体
     */
    void insertDataBasePressureParts(BasePressureParts basePressureParts);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataBasePressurePartsOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataBasePressurePartsMany(String value);

    /**
     * 修改记录
     *
     * @param basePressureParts 对象实体
     */
    void updateDataBasePressureParts(BasePressureParts basePressureParts);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<BasePressureParts> findDataBasePressurePartsByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("chock_no") String chock_no, @Param("sname") String sname, @Param("production_line_id") Long production_line_id, @Param("roll_typeid") Long roll_typeid, @Param("istatus") Long istatus);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataBasePressurePartsByPageSize(@Param("chock_no") String chock_no, @Param("sname") String sname, @Param("production_line_id") Long production_line_id, @Param("roll_typeid") Long roll_typeid, @Param("istatus") Long istatus);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return 实体类
     */
    BasePressureParts findDataBasePressurePartsByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<BasePressureParts> findDataBasePressureParts();

    /**
     * 根据轴承座号查看记录
     *
     * @param chock_no 轴承座号
     */
    List<BasePressureParts> findDataBasePressurePartsByChock(@Param("chock_no") String chock_no);
}
