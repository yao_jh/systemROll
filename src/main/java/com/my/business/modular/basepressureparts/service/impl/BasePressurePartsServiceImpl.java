package com.my.business.modular.basepressureparts.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.basepressureparts.dao.BasePressurePartsDao;
import com.my.business.modular.basepressureparts.entity.BasePressureParts;
import com.my.business.modular.basepressureparts.entity.JBasePressureParts;
import com.my.business.modular.basepressureparts.service.BasePressurePartsService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 轴承座承压件管理接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-23 09:26:12
 */
@Service
public class BasePressurePartsServiceImpl implements BasePressurePartsService {

    @Autowired
    private BasePressurePartsDao basePressurePartsDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataBasePressureParts(String data, Long userId, String sname) {
        try {
            JBasePressureParts jbasePressureParts = JSON.parseObject(data, JBasePressureParts.class);
            BasePressureParts basePressureParts = jbasePressureParts.getBasePressureParts();
            CodiUtil.newRecord(userId, sname, basePressureParts);
            basePressurePartsDao.insertDataBasePressureParts(basePressureParts);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataBasePressurePartsOne(Long indocno) {
        try {
            basePressurePartsDao.deleteDataBasePressurePartsOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataBasePressurePartsMany(String str_id) {
        try {
            String sql = "delete base_pressure_parts where indocno in(" + str_id + ")";
            basePressurePartsDao.deleteDataBasePressurePartsMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataBasePressureParts(String data, Long userId, String sname) {
        try {
            JBasePressureParts jbasePressureParts = JSON.parseObject(data, JBasePressureParts.class);
            BasePressureParts basePressureParts = jbasePressureParts.getBasePressureParts();
            CodiUtil.editRecord(userId, sname, basePressureParts);
            basePressurePartsDao.updateDataBasePressureParts(basePressureParts);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataBasePressurePartsByPage(String data) {
        try {
            JBasePressureParts jbasePressureParts = JSON.parseObject(data, JBasePressureParts.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jbasePressureParts.getPageIndex();
            Integer pageSize = jbasePressureParts.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jbasePressureParts.getCondition()) {
                jsonObject = JSON.parseObject(jbasePressureParts.getCondition().toString());
            }

            String chock_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("chock_no"))) {
                chock_no = jsonObject.get("chock_no").toString();
            }

            Long production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
            }

            String sname = null;
            if (!StringUtils.isEmpty(jsonObject.get("sname"))) {
                sname = jsonObject.get("sname").toString();
            }

            Long istatus = null;
            if (!StringUtils.isEmpty(jsonObject.get("istatus"))) {
                istatus = Long.valueOf(jsonObject.get("istatus").toString());
            }

            Long roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = Long.valueOf(jsonObject.get("roll_typeid").toString());
            }
            List<BasePressureParts> list = basePressurePartsDao.findDataBasePressurePartsByPage((pageIndex - 1) * pageSize, pageSize,chock_no,sname,production_line_id,roll_typeid,istatus);
            Integer count = basePressurePartsDao.findDataBasePressurePartsByPageSize(chock_no,sname,production_line_id,roll_typeid,istatus);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataBasePressurePartsByIndocno(String data) {
        try {
            JBasePressureParts jbasePressureParts = JSON.parseObject(data, JBasePressureParts.class);
            Long indocno = jbasePressureParts.getIndocno();

            BasePressureParts basePressureParts = basePressurePartsDao.findDataBasePressurePartsByIndocno(indocno);
            return ResultData.ResultDataSuccess(basePressureParts);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<BasePressureParts> findDataBasePressureParts() {
        List<BasePressureParts> list = basePressurePartsDao.findDataBasePressureParts();
        return list;
    }

	/**
	 * 根据轴承座号查看记录
	 *
	 * @param data json字符串
	 */
    public List<BasePressureParts> findDataBasePressurePartsByChock(String data) {
        JBasePressureParts jbasePressureParts = JSON.parseObject(data, JBasePressureParts.class);
        JSONObject jsonObject = null;

        if (null != jbasePressureParts.getCondition()) {
            jsonObject = JSON.parseObject(jbasePressureParts.getCondition().toString());
        }

		String chock_no = null;
		if (!StringUtils.isEmpty(jsonObject.get("chock_no"))) {
			chock_no = jsonObject.get("chock_no").toString();
		}
		List<BasePressureParts> list = basePressurePartsDao.findDataBasePressurePartsByChock(chock_no);
        return list;
    }
}
