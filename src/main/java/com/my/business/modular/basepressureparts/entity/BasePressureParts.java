package com.my.business.modular.basepressureparts.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轴承座承压件管理实体类
 *
 * @author 生成器生成
 * @date 2020-09-23 09:26:12
 */
public class BasePressureParts extends BaseEntity {

    private Long indocno;  //主键
    private String snote;  //备注
    private String sname;  //部件名称
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private Long production_line_id;  //产线id
    private String production_line;  //产线
    private Long frame_noid;  //机架号id
    private String frame_no;  //机架号
    private Long install_location_id;  //安装位置id
    private String install_location;  //安装位置
    private Long up_location_id;  //上机位置id
    private String up_location;  //上机位置
    private String number;  //部件编号
    private String usetime;  //投用时间
    private String lasttime;  //剩余换区时间
    private String reports;  //检测报告
    private String results;  //检测结果
    private Long istatus;  //部件状态id
    private String status;  //部件状态
    private String chock_no;

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public String getSname() {
        return this.sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Long getRoll_typeid() {
        return this.roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public Long getProduction_line_id() {
        return this.production_line_id;
    }

    public void setProduction_line_id(Long production_line_id) {
        this.production_line_id = production_line_id;
    }

    public String getProduction_line() {
        return this.production_line;
    }

    public void setProduction_line(String production_line) {
        this.production_line = production_line;
    }

    public Long getFrame_noid() {
        return this.frame_noid;
    }

    public void setFrame_noid(Long frame_noid) {
        this.frame_noid = frame_noid;
    }

    public String getFrame_no() {
        return this.frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public Long getInstall_location_id() {
        return this.install_location_id;
    }

    public void setInstall_location_id(Long install_location_id) {
        this.install_location_id = install_location_id;
    }

    public String getInstall_location() {
        return this.install_location;
    }

    public void setInstall_location(String install_location) {
        this.install_location = install_location;
    }

    public Long getUp_location_id() {
        return this.up_location_id;
    }

    public void setUp_location_id(Long up_location_id) {
        this.up_location_id = up_location_id;
    }

    public String getUp_location() {
        return this.up_location;
    }

    public void setUp_location(String up_location) {
        this.up_location = up_location;
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getUsetime() {
        return this.usetime;
    }

    public void setUsetime(String usetime) {
        this.usetime = usetime;
    }

    public String getLasttime() {
        return this.lasttime;
    }

    public void setLasttime(String lasttime) {
        this.lasttime = lasttime;
    }

    public String getReports() {
        return this.reports;
    }

    public void setReports(String reports) {
        this.reports = reports;
    }

    public String getResults() {
        return this.results;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public Long getIstatus() {
        return this.istatus;
    }

    public void setIstatus(Long istatus) {
        this.istatus = istatus;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getChock_no() {
        return chock_no;
    }

    public void setChock_no(String chock_no) {
        this.chock_no = chock_no;
    }
}