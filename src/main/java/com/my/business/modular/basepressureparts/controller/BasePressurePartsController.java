package com.my.business.modular.basepressureparts.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.basepressureparts.entity.BasePressureParts;
import com.my.business.modular.basepressureparts.entity.JBasePressureParts;
import com.my.business.modular.basepressureparts.service.BasePressurePartsService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 轴承座承压件管理控制器层
 *
 * @author 生成器生成
 * @date 2020-09-23 09:26:12
 */
@RestController
@RequestMapping("/basePressureParts")
public class BasePressurePartsController {

    @Autowired
    private BasePressurePartsService basePressurePartsService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataBasePressureParts(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return basePressurePartsService.insertDataBasePressureParts(data, userId, CodiUtil.returnLm(sname));
    }

	/**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataBasePressurePartsOne(@RequestBody String data) {
        try {
            JBasePressureParts jbasePressureParts = JSON.parseObject(data, JBasePressureParts.class);
            return basePressurePartsService.deleteDataBasePressurePartsOne(jbasePressureParts.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JBasePressureParts jbasePressureParts = JSON.parseObject(data, JBasePressureParts.class);
            return basePressurePartsService.deleteDataBasePressurePartsMany(jbasePressureParts.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataBasePressureParts(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return basePressurePartsService.updateDataBasePressureParts(data, userId, CodiUtil.returnLm(sname));
    }

	/**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataBasePressurePartsByPage(@RequestBody String data) {
        return basePressurePartsService.findDataBasePressurePartsByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataBasePressurePartsByIndocno(@RequestBody String data) {
        return basePressurePartsService.findDataBasePressurePartsByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<BasePressureParts> findDataBasePressureParts() {
        return basePressurePartsService.findDataBasePressureParts();
    }

	/**
     * 根据轴承座号查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByChock"}, method = RequestMethod.POST)
    @ResponseBody
    public List<BasePressureParts> findDataBasePressurePartsByChock(@RequestBody String data) {
        return basePressurePartsService.findDataBasePressurePartsByChock(data);
    }
}
