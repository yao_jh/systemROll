package com.my.business.modular.rollgroup.dao;

import com.my.business.modular.rollgroup.entity.RollGroup;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 组表dao接口
 *
 * @author 生成器生成
 * @date 2020-09-09 15:37:16
 */
@Mapper
public interface RollGroupDao {

    /**
     * 添加记录
     *
     * @param rollGroup 对象实体
     */
    void insertDataRollGroup(RollGroup rollGroup);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollGroupOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollGroupMany(String value);

    /**
     * 修改记录
     *
     * @param rollGroup 对象实体
     */
    void updateDataRollGroup(RollGroup rollGroup);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollGroup> findDataRollGroupByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollGroupByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollGroup findDataRollGroupByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollGroup> findDataRollGroup();

}
