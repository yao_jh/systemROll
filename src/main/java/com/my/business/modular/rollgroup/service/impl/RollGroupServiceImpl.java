package com.my.business.modular.rollgroup.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollgroup.dao.RollGroupDao;
import com.my.business.modular.rollgroup.entity.JRollGroup;
import com.my.business.modular.rollgroup.entity.RollGroup;
import com.my.business.modular.rollgroup.service.RollGroupService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 组表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-09 15:37:16
 */
@Service
public class RollGroupServiceImpl implements RollGroupService {

    @Autowired
    private RollGroupDao rollGroupDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollGroup(String data, Long userId, String sname) {
        try {
            JRollGroup jrollGroup = JSON.parseObject(data, JRollGroup.class);
            RollGroup rollGroup = jrollGroup.getRollGroup();
            CodiUtil.newRecord(userId, sname, rollGroup);
            rollGroupDao.insertDataRollGroup(rollGroup);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollGroupOne(Long indocno) {
        try {
            rollGroupDao.deleteDataRollGroupOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollGroupMany(String str_id) {
        try {
            String sql = "delete roll_group where indocno in(" + str_id + ")";
            rollGroupDao.deleteDataRollGroupMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataRollGroup(String data, Long userId, String sname) {
        try {
            JRollGroup jrollGroup = JSON.parseObject(data, JRollGroup.class);
            RollGroup rollGroup = jrollGroup.getRollGroup();
            CodiUtil.editRecord(userId, sname, rollGroup);
            rollGroupDao.updateDataRollGroup(rollGroup);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollGroupByPage(String data) {
        try {
            JRollGroup jrollGroup = JSON.parseObject(data, JRollGroup.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollGroup.getPageIndex();
            Integer pageSize = jrollGroup.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollGroup.getCondition()) {
                jsonObject = JSON.parseObject(jrollGroup.getCondition().toString());
            }

            List<RollGroup> list = rollGroupDao.findDataRollGroupByPage((pageIndex - 1) * pageSize, pageSize);
            Integer count = rollGroupDao.findDataRollGroupByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollGroupByIndocno(String data) {
        try {
            JRollGroup jrollGroup = JSON.parseObject(data, JRollGroup.class);
            Long indocno = jrollGroup.getIndocno();

            RollGroup rollGroup = rollGroupDao.findDataRollGroupByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollGroup);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollGroup> findDataRollGroup() {
        List<RollGroup> list = rollGroupDao.findDataRollGroup();
        return list;
    }

}
