package com.my.business.modular.rollgroup.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 组表实体类
 *
 * @author 生成器生成
 * @date 2020-09-09 15:37:15
 */
public class RollGroup extends BaseEntity {

    private Long indocno;  //主键
    private String group_name;  //组名
    private String group_userids;  //组关联人id集合
    private String group_usernames;  //组关联人名集合
    private String snote;  //备注

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getGroup_name() {
        return this.group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getGroup_userids() {
        return this.group_userids;
    }

    public void setGroup_userids(String group_userids) {
        this.group_userids = group_userids;
    }

    public String getGroup_usernames() {
        return this.group_usernames;
    }

    public void setGroup_usernames(String group_usernames) {
        this.group_usernames = group_usernames;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }


}