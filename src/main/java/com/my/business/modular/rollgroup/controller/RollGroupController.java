package com.my.business.modular.rollgroup.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollgroup.entity.JRollGroup;
import com.my.business.modular.rollgroup.entity.RollGroup;
import com.my.business.modular.rollgroup.service.RollGroupService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 组表控制器层
 *
 * @author 生成器生成
 * @date 2020-09-09 15:37:16
 */
@RestController
@RequestMapping("/rollGroup")
public class RollGroupController {

    @Autowired
    private RollGroupService rollGroupService;

    /**
     * 添加记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollGroup(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollGroupService.insertDataRollGroup(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollGroupOne(@RequestBody String data) {
        try {
            JRollGroup jrollGroup = JSON.parseObject(data, JRollGroup.class);
            return rollGroupService.deleteDataRollGroupOne(jrollGroup.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollGroup jrollGroup = JSON.parseObject(data, JRollGroup.class);
            return rollGroupService.deleteDataRollGroupMany(jrollGroup.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollGroup(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollGroupService.updateDataRollGroup(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollGroupByPage(@RequestBody String data) {
        return rollGroupService.findDataRollGroupByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollGroupByIndocno(@RequestBody String data) {
        return rollGroupService.findDataRollGroupByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollGroup> findDataRollGroup() {
        return rollGroupService.findDataRollGroup();
    }

}
