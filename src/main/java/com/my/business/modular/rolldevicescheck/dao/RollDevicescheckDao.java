package com.my.business.modular.rolldevicescheck.dao;

import com.my.business.modular.rolldevicescheck.entity.RollDevicescheck;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 点检设备表dao接口
 *
 * @author 生成器生成
 * @date 2020-07-22 13:35:51
 */
@Mapper
public interface RollDevicescheckDao {

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollDevicescheck> findDataRollDevicescheck();


    RollDevicescheck findDataRollDevicescheckByIndocno(@Param("indocno") Long indocno);

    /**
     * 根据设备名称模糊查询
     *
     * @return list 对象集合返回
     */
    List<RollDevicescheck> findDataRollDeviceCheckByName(@Param("device_name") String deviceName);

}
