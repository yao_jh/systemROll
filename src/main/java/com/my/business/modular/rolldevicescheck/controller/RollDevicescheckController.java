package com.my.business.modular.rolldevicescheck.controller;

import com.my.business.modular.rolldevicescheck.entity.RollDevicescheck;
import com.my.business.modular.rolldevicescheck.service.RollDevicesCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 点检设备表控制器层
 *
 * @author 生成器生成
 * @date 2020-07-22 13:35:51
 */
@RestController
@RequestMapping("/rollDevicescheck")
public class RollDevicescheckController {

    @Autowired
    private RollDevicesCheckService rollDevicescheckService;

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.GET)
    public List<RollDevicescheck> findDataRollDevicescheck() {
        return rollDevicescheckService.findDataRollDevicescheck();
    }

    // test
//	@CrossOrigin
//	@RequestMapping(value = {"/findByDeviceName"}, method = RequestMethod.GET)
//	@ResponseBody
//	public List<RollDevicescheck> findDataRollDevicescheckpartByIndocno(@RequestParam(value="device_name") String device_name) {
//		return rollDevicescheckService.findDataRollDeviceCheckByName(device_name);
//	}

}
