package com.my.business.modular.rolldevicescheck.service.impl;

import com.my.business.modular.rolldevicescheck.dao.RollDevicescheckDao;
import com.my.business.modular.rolldevicescheck.entity.RollDevicescheck;
import com.my.business.modular.rolldevicescheck.service.RollDevicesCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 点检设备表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-07-22 13:35:51
 */
@Service
public class RollDevicesCheckServiceImpl implements RollDevicesCheckService {

    @Autowired
    private RollDevicescheckDao rollDevicescheckDao;

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollDevicescheck> findDataRollDevicescheck() {
        List<RollDevicescheck> list = rollDevicescheckDao.findDataRollDevicescheck();
        return list;
    }

    /**
     * 根据设备名称模糊查询
     *
     * @return list 对象集合返回
     */
    public List<RollDevicescheck> findDataRollDeviceCheckByName(String deviceName) {
        List<RollDevicescheck> list = rollDevicescheckDao.findDataRollDeviceCheckByName(deviceName);
        return list;
    }
}
