package com.my.business.modular.rolldevicescheck.entity;

import com.my.business.sys.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 点检设备表实体类
 *
 * @author 生成器生成
 * @date 2020-07-22 13:35:50
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RollDevicescheck extends BaseEntity {

    private Long indocno;  //点检设备主键
    private String devicename;  //设备名称
    private Long priority;  //优先级
    private String snote;  //删除标识
    private String description;

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getDevicename() {
        return devicename;
    }

    public void setDevicename(String devicename) {
        this.devicename = devicename;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public String getSnote() {
        return snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}