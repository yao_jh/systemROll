package com.my.business.modular.rolldevicescheck.service;

import com.my.business.modular.rolldevicescheck.entity.RollDevicescheck;

import java.util.List;

/**
 * 点检设备表接口服务类
 *
 * @author 生成器生成
 * @date 2020-07-22 13:35:51
 */
public interface RollDevicesCheckService {

    /**
     * 查看记录
     */
    List<RollDevicescheck> findDataRollDevicescheck();

    /**
     * 根据设备名称模糊查询
     *
     * @return list 对象集合返回
     */
    List<RollDevicescheck> findDataRollDeviceCheckByName(String deviceName);
}
