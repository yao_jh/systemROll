package com.my.business.modular.stiffnessscore.dao;

import com.my.business.modular.stiffnessscore.entity.StiffnessScore;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轧辊刚度评价标准——分数dao接口
 *
 * @author 生成器生成
 * @date 2020-09-08 10:46:07
 */
@Mapper
public interface StiffnessScoreDao {

    /**
     * 添加记录
     *
     * @param stiffnessScore 对象实体
     */
    void insertDataStiffnessScore(StiffnessScore stiffnessScore);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataStiffnessScoreOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataStiffnessScoreMany(String value);

    /**
     * 修改记录
     *
     * @param stiffnessScore 对象实体
     */
    void updateDataStiffnessScore(StiffnessScore stiffnessScore);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<StiffnessScore> findDataStiffnessScoreByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataStiffnessScoreByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    StiffnessScore findDataStiffnessScoreByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<StiffnessScore> findDataStiffnessScore();

    /**
     * 根据外键删除记录
     */
    void deleteDataStiffnessScoreByilinkno(@Param("ilinkno") Long ilinkno);

    /**
     * 分页查看查看记录
     *
     * @return 对象数据集合
     */
    List<StiffnessScore> findDataStiffnessScoreByIlinkno(@Param("ilinkno") Long ilinkno);
}
