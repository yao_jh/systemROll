package com.my.business.modular.stiffnessscore.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轧辊刚度评价标准——分数实体类
 *
 * @author 生成器生成
 * @date 2020-09-08 10:46:07
 */
public class StiffnessScore extends BaseEntity {

    private Long indocno;  //主键
    private Long ilinkno;  //外键
    private Double min;  //最小值
    private Double max;  //最大值
    private Double score;  //得分
    private Long score_type_id;  //评分类型id
    private String score_type;  //评分类型

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getIlinkno() {
        return this.ilinkno;
    }

    public void setIlinkno(Long ilinkno) {
        this.ilinkno = ilinkno;
    }

    public Double getMin() {
        return this.min;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    public Double getMax() {
        return this.max;
    }

    public void setMax(Double max) {
        this.max = max;
    }

    public Double getScore() {
        return this.score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Long getScore_type_id() {
        return this.score_type_id;
    }

    public void setScore_type_id(Long score_type_id) {
        this.score_type_id = score_type_id;
    }

    public String getScore_type() {
        return this.score_type;
    }

    public void setScore_type(String score_type) {
        this.score_type = score_type;
    }


}