package com.my.business.modular.stiffnessscore.service;

import com.my.business.modular.stiffnessscore.entity.StiffnessScore;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 轧辊刚度评价标准——分数接口服务类
 *
 * @author 生成器生成
 * @date 2020-09-08 10:46:07
 */
public interface StiffnessScoreService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataStiffnessScore(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataStiffnessScoreOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataStiffnessScoreMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataStiffnessScore(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataStiffnessScoreByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataStiffnessScoreByIndocno(String data);

    /**
     * 查看记录
     */
    List<StiffnessScore> findDataStiffnessScore();

    /**
     * 根据外键查看多条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataStiffnessScoreByIlinkno(String data);
}
