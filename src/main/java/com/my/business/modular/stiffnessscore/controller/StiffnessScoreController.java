package com.my.business.modular.stiffnessscore.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.stiffnessscore.entity.JStiffnessScore;
import com.my.business.modular.stiffnessscore.entity.StiffnessScore;
import com.my.business.modular.stiffnessscore.service.StiffnessScoreService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 轧辊刚度评价标准——分数控制器层
 *
 * @author 生成器生成
 * @date 2020-09-08 10:46:07
 */
@RestController
@RequestMapping("/stiffnessScore")
public class StiffnessScoreController {

    @Autowired
    private StiffnessScoreService stiffnessScoreService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataStiffnessScore(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return stiffnessScoreService.insertDataStiffnessScore(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataStiffnessScoreOne(@RequestBody String data) {
        try {
            JStiffnessScore jstiffnessScore = JSON.parseObject(data, JStiffnessScore.class);
            return stiffnessScoreService.deleteDataStiffnessScoreOne(jstiffnessScore.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JStiffnessScore jstiffnessScore = JSON.parseObject(data, JStiffnessScore.class);
            return stiffnessScoreService.deleteDataStiffnessScoreMany(jstiffnessScore.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataStiffnessScore(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return stiffnessScoreService.updateDataStiffnessScore(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataStiffnessScoreByPage(@RequestBody String data) {
        return stiffnessScoreService.findDataStiffnessScoreByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataStiffnessScoreByIndocno(@RequestBody String data) {
        return stiffnessScoreService.findDataStiffnessScoreByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<StiffnessScore> findDataStiffnessScore() {
        return stiffnessScoreService.findDataStiffnessScore();
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIlinkno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataStiffnessScoreByIlinkno(@RequestBody String data) {
        return stiffnessScoreService.findDataStiffnessScoreByIlinkno(data);
    }

}
