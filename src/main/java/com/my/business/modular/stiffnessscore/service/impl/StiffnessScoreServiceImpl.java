package com.my.business.modular.stiffnessscore.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.stiffnessscore.dao.StiffnessScoreDao;
import com.my.business.modular.stiffnessscore.entity.JStiffnessScore;
import com.my.business.modular.stiffnessscore.entity.StiffnessScore;
import com.my.business.modular.stiffnessscore.service.StiffnessScoreService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 轧辊刚度评价标准——分数接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-08 10:46:07
 */
@Service
public class StiffnessScoreServiceImpl implements StiffnessScoreService {

    @Autowired
    private StiffnessScoreDao stiffnessScoreDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataStiffnessScore(String data, Long userId, String sname) {
        try {
            JStiffnessScore jstiffnessScore = JSON.parseObject(data, JStiffnessScore.class);
            StiffnessScore stiffnessScore = jstiffnessScore.getStiffnessScore();
            stiffnessScoreDao.insertDataStiffnessScore(stiffnessScore);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataStiffnessScoreOne(Long indocno) {
        try {
            stiffnessScoreDao.deleteDataStiffnessScoreOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataStiffnessScoreMany(String str_id) {
        try {
            String sql = "delete stiffness_score where indocno in(" + str_id + ")";
            stiffnessScoreDao.deleteDataStiffnessScoreMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataStiffnessScore(String data, Long userId, String sname) {
        try {
            JStiffnessScore jstiffnessScore = JSON.parseObject(data, JStiffnessScore.class);
            StiffnessScore stiffnessScore = jstiffnessScore.getStiffnessScore();
            CodiUtil.editRecord(userId, sname, stiffnessScore);
            stiffnessScoreDao.updateDataStiffnessScore(stiffnessScore);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataStiffnessScoreByPage(String data) {
        try {
            JStiffnessScore jstiffnessScore = JSON.parseObject(data, JStiffnessScore.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jstiffnessScore.getPageIndex();
            Integer pageSize = jstiffnessScore.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jstiffnessScore.getCondition()) {
                jsonObject = JSON.parseObject(jstiffnessScore.getCondition().toString());
            }

            List<StiffnessScore> list = stiffnessScoreDao.findDataStiffnessScoreByPage((pageIndex - 1) * pageSize, pageSize);
            Integer count = stiffnessScoreDao.findDataStiffnessScoreByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataStiffnessScoreByIndocno(String data) {
        try {
            JStiffnessScore jstiffnessScore = JSON.parseObject(data, JStiffnessScore.class);
            Long indocno = jstiffnessScore.getIndocno();

            StiffnessScore stiffnessScore = stiffnessScoreDao.findDataStiffnessScoreByIndocno(indocno);
            return ResultData.ResultDataSuccess(stiffnessScore);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<StiffnessScore> findDataStiffnessScore() {
        List<StiffnessScore> list = stiffnessScoreDao.findDataStiffnessScore();
        return list;
    }

    /**
     * 根据外键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataStiffnessScoreByIlinkno(String data) {
        try {
            JStiffnessScore jstiffnessScore = JSON.parseObject(data, JStiffnessScore.class);
            Long ilinkno = jstiffnessScore.getStiffnessScore().getIlinkno();
            List<StiffnessScore> list = stiffnessScoreDao.findDataStiffnessScoreByIlinkno(ilinkno);
            return ResultData.ResultDataSuccess(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }
}
