package com.my.business.modular.rollstandingbook.service;

import com.my.business.modular.rollstandingbook.entity.RollStandingbook;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 新辊台账表接口服务类
 *
 * @author 生成器生成
 * @date 2020-07-02 16:14:40
 */
public interface RollStandingbookService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollStandingbook(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollStandingbookOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollStandingbookMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollStandingbook(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollStandingbookByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollStandingbookByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollStandingbook> findDataRollStandingbook();
}
