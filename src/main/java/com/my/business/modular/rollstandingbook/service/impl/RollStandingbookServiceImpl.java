package com.my.business.modular.rollstandingbook.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollstandingbook.dao.RollStandingbookDao;
import com.my.business.modular.rollstandingbook.entity.JRollStandingbook;
import com.my.business.modular.rollstandingbook.entity.RollStandingbook;
import com.my.business.modular.rollstandingbook.service.RollStandingbookService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 新辊台账表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-07-02 16:14:40
 */
@Service
public class RollStandingbookServiceImpl implements RollStandingbookService {

    // 新辊状态：启动
    private static final Integer ROLLSTANDINGBOOK_INITAIL = 1;
    // 新辊状态：入库
    private static final Integer ROLLSTANDINGBOOK_UNINITAIL = 0;
    // 是否让步接收： 是
    private static final Integer IS_CONCESSIONS = 1;
    // 是否让步接收： 否
    private static final Integer ISNOT_CONCESSIONS = 0;
    @Autowired
    private RollStandingbookDao rollStandingbookDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollStandingbook(String data, Long userId, String sname) {
        try {
            JRollStandingbook jrollStandingbook = JSON.parseObject(data, JRollStandingbook.class);
            RollStandingbook rollStandingbook = jrollStandingbook.getRollStandingbook();
            CodiUtil.newRecord(userId, sname, rollStandingbook);
            rollStandingbookDao.insertDataRollStandingbook(rollStandingbook);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollStandingbookOne(Long indocno) {
        try {
            rollStandingbookDao.deleteDataRollStandingbookOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollStandingbookMany(String str_id) {
        try {
            String sql = "delete roll_standingbook where indocno in(" + str_id + ")";
            rollStandingbookDao.deleteDataRollStandingbookMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   对象实体
     */
    public ResultData updateDataRollStandingbook(String data, Long userId, String sname) {
        try {
            JRollStandingbook jrollStandingbook = JSON.parseObject(data, JRollStandingbook.class);
            RollStandingbook rollStandingbook = jrollStandingbook.getRollStandingbook();
            CodiUtil.editRecord(userId, sname, rollStandingbook);
            rollStandingbookDao.updateDataRollStandingbook(rollStandingbook);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollStandingbookByPage(String data) {
        try {
            JRollStandingbook jrollStandingbook = JSON.parseObject(data, JRollStandingbook.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollStandingbook.getPageIndex();
            Integer pageSize = jrollStandingbook.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollStandingbook.getCondition()) {
                jsonObject = JSON.parseObject(jrollStandingbook.getCondition().toString());
            }

            // 添加模糊查询功能
            String rollNo = null;//辊号
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                rollNo = jsonObject.get("roll_no").toString();
            }

            String rollType = null;//辊型
            if (!StringUtils.isEmpty(jsonObject.get("roll_type"))) {
                rollType = jsonObject.get("roll_type").toString();
            }

            String arrivalDateStart = null;// 到货日期开始时间
            if (!StringUtils.isEmpty(jsonObject.get("arrival_date_start"))) {
                arrivalDateStart = jsonObject.get("arrival_date_start").toString();
            }

            String arrivalDateEnd = null;// 到货日期截止时间
            if (!StringUtils.isEmpty(jsonObject.get("arrival_date_end"))) {
                arrivalDateEnd = jsonObject.get("arrival_date_end").toString();
            }

            String ifNewMaterial = null;// 是否新材质
            if (!StringUtils.isEmpty(jsonObject.get("if_new_material"))) {
                ifNewMaterial = jsonObject.get("if_new_material").toString();
            }

            String ifNewSupplier = null;// 是否新厂家
            if (!StringUtils.isEmpty(jsonObject.get("if_new_supplier"))) {
                ifNewSupplier = jsonObject.get("if_new_supplier").toString();
            }

            Integer rollState = Integer.parseInt(jsonObject.get("roll_state").toString());
            List<RollStandingbook> list = rollStandingbookDao.findDataRollStandingbookByPage(pageIndex, pageSize, rollNo, rollType, arrivalDateStart, arrivalDateEnd, ifNewMaterial, ifNewSupplier, rollState);
            Integer count = rollStandingbookDao.findDataRollStandingbookByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollStandingbookByIndocno(String data) {
        try {
            JRollStandingbook jrollStandingbook = JSON.parseObject(data, JRollStandingbook.class);
            Long indocno = jrollStandingbook.getIndocno();

            RollStandingbook rollStandingbook = rollStandingbookDao.findDataRollStandingbookByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollStandingbook);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollStandingbook> findDataRollStandingbook() {
        List<RollStandingbook> list = rollStandingbookDao.findDataRollStandingbook();
        return list;
    }

}
