package com.my.business.modular.rollstandingbook.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollstandingbook.entity.JRollStandingbook;
import com.my.business.modular.rollstandingbook.entity.RollStandingbook;
import com.my.business.modular.rollstandingbook.service.RollStandingbookService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 新辊台账表控制器层
 *
 * @author 生成器生成
 * @date 2020-07-02 16:14:40
 */
@RestController
@RequestMapping("/rollStandingbook")
public class RollStandingbookController {

    @Autowired
    private RollStandingbookService rollStandingbookService;

    /**
     * 添加记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollStandingbook(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollStandingbookService.insertDataRollStandingbook(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollStandingbookOne(@RequestBody String data) {
        try {
            JRollStandingbook jrollStandingbook = JSON.parseObject(data, JRollStandingbook.class);
            return rollStandingbookService.deleteDataRollStandingbookOne(jrollStandingbook.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollStandingbook jrollStandingbook = JSON.parseObject(data, JRollStandingbook.class);
            return rollStandingbookService.deleteDataRollStandingbookMany(jrollStandingbook.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollStandingbook(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollStandingbookService.updateDataRollStandingbook(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollStandingbookByPage(@RequestBody String data) {
        return rollStandingbookService.findDataRollStandingbookByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollStandingbookByIndocno(@RequestBody String data) {
        return rollStandingbookService.findDataRollStandingbookByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollStandingbook> findDataRollStandingbook() {
        return rollStandingbookService.findDataRollStandingbook();
    }
}
