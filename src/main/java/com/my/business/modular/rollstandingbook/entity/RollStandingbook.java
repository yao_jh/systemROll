package com.my.business.modular.rollstandingbook.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 新辊台账表实体类
 *
 * @author 生成器生成
 * @date 2020-07-02 16:14:39
 */
public class RollStandingbook extends BaseEntity {

    private Long indocno;  //主键
    private String roll_no;  //辊号
    private Long roll_typeid;  //辊型id
    private String roll_type;  //辊型
    private Long frame_periodid;  //机架段id
    private String frame_period;  //机架段
    private Long work_layer;  //工作层尺寸
    private Long settlement_typeid;  //结算类型id
    private String aettlement_type;  //结算类型
    private String supplier;  //供货商
    private Long if_new_supplier;  //是否新供货商
    private String material;  //材质
    private Long if_new_material;  //是否新材质
    private Long locationid;  //存放地点id
    private String location;  //存放地点
    private Long diameter_theory;  //理论直径
    private Long diameter_scrap;  //报废直径
    private Long diameter_original;  //原始直径
    private Long roll_body_max;  //辊身MAX
    private Long roll_body_min;  //辊身MIN
    private Long roll_neck_max;  //辊颈MAX
    private Long roll_neck_min;  //辊颈MIN
    private String arrival_date;  //到货日期
    private Long packing_inspection;  //外包检查(0 不合格  1 合格)
    private String capitalization_date;  //资本化日期
    private String inspection_date;  //检测日期
    private Long inspection_results;  //检验结果（0 不合格  1合格）
    private String use_date;  //投用日期
    private String expired_date;  //报废日期
    private String expired_reason;  //报废原因
    private Integer if_concessions;  //是否让步接收(0 否  1 是)
    private Integer roll_state;  //轧辊状态(0 入库  1 新辊启用)
    private String snote;  //备注

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public Long getRoll_typeid() {
        return this.roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public Long getFrame_periodid() {
        return this.frame_periodid;
    }

    public void setFrame_periodid(Long frame_periodid) {
        this.frame_periodid = frame_periodid;
    }

    public String getFrame_period() {
        return this.frame_period;
    }

    public void setFrame_period(String frame_period) {
        this.frame_period = frame_period;
    }

    public Long getWork_layer() {
        return this.work_layer;
    }

    public void setWork_layer(Long work_layer) {
        this.work_layer = work_layer;
    }

    public Long getSettlement_typeid() {
        return this.settlement_typeid;
    }

    public void setSettlement_typeid(Long settlement_typeid) {
        this.settlement_typeid = settlement_typeid;
    }

    public String getAettlement_type() {
        return this.aettlement_type;
    }

    public void setAettlement_type(String aettlement_type) {
        this.aettlement_type = aettlement_type;
    }

    public String getSupplier() {
        return this.supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Long getIf_new_supplier() {
        return this.if_new_supplier;
    }

    public void setIf_new_supplier(Long if_new_supplier) {
        this.if_new_supplier = if_new_supplier;
    }

    public String getMaterial() {
        return this.material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public Long getIf_new_material() {
        return this.if_new_material;
    }

    public void setIf_new_material(Long if_new_material) {
        this.if_new_material = if_new_material;
    }

    public Long getLocationid() {
        return this.locationid;
    }

    public void setLocationid(Long locationid) {
        this.locationid = locationid;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Long getDiameter_theory() {
        return this.diameter_theory;
    }

    public void setDiameter_theory(Long diameter_theory) {
        this.diameter_theory = diameter_theory;
    }

    public Long getDiameter_scrap() {
        return this.diameter_scrap;
    }

    public void setDiameter_scrap(Long diameter_scrap) {
        this.diameter_scrap = diameter_scrap;
    }

    public Long getDiameter_original() {
        return this.diameter_original;
    }

    public void setDiameter_original(Long diameter_original) {
        this.diameter_original = diameter_original;
    }

    public Long getRoll_body_max() {
        return this.roll_body_max;
    }

    public void setRoll_body_max(Long roll_body_max) {
        this.roll_body_max = roll_body_max;
    }

    public Long getRoll_body_min() {
        return this.roll_body_min;
    }

    public void setRoll_body_min(Long roll_body_min) {
        this.roll_body_min = roll_body_min;
    }

    public Long getRoll_neck_max() {
        return this.roll_neck_max;
    }

    public void setRoll_neck_max(Long roll_neck_max) {
        this.roll_neck_max = roll_neck_max;
    }

    public Long getRoll_neck_min() {
        return this.roll_neck_min;
    }

    public void setRoll_neck_min(Long roll_neck_min) {
        this.roll_neck_min = roll_neck_min;
    }

    public String getArrival_date() {
        return this.arrival_date;
    }

    public void setArrival_date(String arrival_date) {
        this.arrival_date = arrival_date;
    }

    public Long getPacking_inspection() {
        return this.packing_inspection;
    }

    public void setPacking_inspection(Long packing_inspection) {
        this.packing_inspection = packing_inspection;
    }

    public String getCapitalization_date() {
        return this.capitalization_date;
    }

    public void setCapitalization_date(String capitalization_date) {
        this.capitalization_date = capitalization_date;
    }

    public String getInspection_date() {
        return this.inspection_date;
    }

    public void setInspection_date(String inspection_date) {
        this.inspection_date = inspection_date;
    }

    public Long getInspection_results() {
        return this.inspection_results;
    }

    public void setInspection_results(Long inspection_results) {
        this.inspection_results = inspection_results;
    }

    public String getUse_date() {
        return this.use_date;
    }

    public void setUse_date(String use_date) {
        this.use_date = use_date;
    }

    public String getExpired_date() {
        return this.expired_date;
    }

    public void setExpired_date(String expired_date) {
        this.expired_date = expired_date;
    }

    public String getExpired_reason() {
        return this.expired_reason;
    }

    public void setExpired_reason(String expired_reason) {
        this.expired_reason = expired_reason;
    }

    public Integer getIf_concessions() {
        return this.if_concessions;
    }

    public void setIf_concessions(Integer if_concessions) {
        this.if_concessions = if_concessions;
    }

    public Integer getRoll_state() {
        return this.roll_state;
    }

    public void setRoll_state(Integer roll_state) {
        this.roll_state = roll_state;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }


}