package com.my.business.modular.rollstandingbook.dao;

import com.my.business.modular.rollstandingbook.entity.RollStandingbook;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 新辊台账表dao接口
 *
 * @author 生成器生成
 * @date 2020-07-02 16:14:40
 */
@Mapper
public interface RollStandingbookDao {

    /**
     * 添加记录
     *
     * @param rollStandingbook 对象实体
     */
    void insertDataRollStandingbook(RollStandingbook rollStandingbook);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollStandingbookOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollStandingbookMany(String value);

    /**
     * 修改记录
     *
     * @param rollStandingbook 对象实体
     */
    void updateDataRollStandingbook(RollStandingbook rollStandingbook);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollStandingbook> findDataRollStandingbookByPage(@Param("pageIndex") Integer pageIndex,
                                                          @Param("pageSize") Integer pageSize,
                                                          @Param("rollNo") String rollNo,
                                                          @Param("rollType") String rollType,
                                                          @Param("arrivalDateStart") String arrivalDateStart,
                                                          @Param("arrivalDateEnd") String arrivalDateEnd,
                                                          @Param("ifNewMaterial") String ifNewMaterial,
                                                          @Param("ifNewSupplier") String ifNewSupplier,
                                                          @Param("rollState") Integer rollState);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollStandingbookByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollStandingbook findDataRollStandingbookByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollStandingbook> findDataRollStandingbook();

}
