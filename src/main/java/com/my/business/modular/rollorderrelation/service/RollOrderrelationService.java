package com.my.business.modular.rollorderrelation.service;

import com.my.business.modular.rollorderrelation.entity.RollOrderrelation;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 工单关联表接口服务类
 *
 * @author 生成器生成
 * @date 2020-09-09 10:05:17
 */
public interface RollOrderrelationService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollOrderrelation(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollOrderrelationOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollOrderrelationMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollOrderrelation(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollOrderrelationByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollOrderrelationByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollOrderrelation> findDataRollOrderrelation();
}
