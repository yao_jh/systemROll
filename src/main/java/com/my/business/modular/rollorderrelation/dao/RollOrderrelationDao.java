package com.my.business.modular.rollorderrelation.dao;

import com.my.business.modular.rollorderrelation.entity.RollOrderrelation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 工单关联表dao接口
 *
 * @author 生成器生成
 * @date 2020-09-09 10:05:17
 */
@Mapper
public interface RollOrderrelationDao {

    /**
     * 添加记录
     *
     * @param rollOrderrelation 对象实体
     */
    void insertDataRollOrderrelation(RollOrderrelation rollOrderrelation);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollOrderrelationOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollOrderrelationMany(String value);

    /**
     * 修改记录
     *
     * @param rollOrderrelation 对象实体
     */
    void updateDataRollOrderrelation(RollOrderrelation rollOrderrelation);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollOrderrelation> findDataRollOrderrelationByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollOrderrelationByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollOrderrelation findDataRollOrderrelationByIndocno(@Param("indocno") Long indocno);

    /***
     * 根据工单编码查询信息
     * @param indocno 用户id
     * @return
     */
    RollOrderrelation findDataRollOrderrelationByNo(@Param("order_no") String order_no);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollOrderrelation> findDataRollOrderrelation();

}
