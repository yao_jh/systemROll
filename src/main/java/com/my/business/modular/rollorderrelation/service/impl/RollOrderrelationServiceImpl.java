package com.my.business.modular.rollorderrelation.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollorderrelation.dao.RollOrderrelationDao;
import com.my.business.modular.rollorderrelation.entity.JRollOrderrelation;
import com.my.business.modular.rollorderrelation.entity.RollOrderrelation;
import com.my.business.modular.rollorderrelation.service.RollOrderrelationService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 工单关联表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-09 10:05:17
 */
@Service
public class RollOrderrelationServiceImpl implements RollOrderrelationService {

    @Autowired
    private RollOrderrelationDao rollOrderrelationDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollOrderrelation(String data, Long userId, String sname) {
        try {
            JRollOrderrelation jrollOrderrelation = JSON.parseObject(data, JRollOrderrelation.class);
            RollOrderrelation rollOrderrelation = jrollOrderrelation.getRollOrderrelation();
            CodiUtil.newRecord(userId, sname, rollOrderrelation);
            rollOrderrelationDao.insertDataRollOrderrelation(rollOrderrelation);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollOrderrelationOne(Long indocno) {
        try {
            rollOrderrelationDao.deleteDataRollOrderrelationOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollOrderrelationMany(String str_id) {
        try {
            String sql = "delete roll_orderrelation where indocno in(" + str_id + ")";
            rollOrderrelationDao.deleteDataRollOrderrelationMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataRollOrderrelation(String data, Long userId, String sname) {
        try {
            JRollOrderrelation jrollOrderrelation = JSON.parseObject(data, JRollOrderrelation.class);
            RollOrderrelation rollOrderrelation = jrollOrderrelation.getRollOrderrelation();
            CodiUtil.editRecord(userId, sname, rollOrderrelation);
            rollOrderrelationDao.updateDataRollOrderrelation(rollOrderrelation);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollOrderrelationByPage(String data) {
        try {
            JRollOrderrelation jrollOrderrelation = JSON.parseObject(data, JRollOrderrelation.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollOrderrelation.getPageIndex();
            Integer pageSize = jrollOrderrelation.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollOrderrelation.getCondition()) {
                jsonObject = JSON.parseObject(jrollOrderrelation.getCondition().toString());
            }

            List<RollOrderrelation> list = rollOrderrelationDao.findDataRollOrderrelationByPage((pageIndex - 1) * pageSize, pageSize);
            Integer count = rollOrderrelationDao.findDataRollOrderrelationByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollOrderrelationByIndocno(String data) {
        try {
            JRollOrderrelation jrollOrderrelation = JSON.parseObject(data, JRollOrderrelation.class);
            Long indocno = jrollOrderrelation.getIndocno();

            RollOrderrelation rollOrderrelation = rollOrderrelationDao.findDataRollOrderrelationByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollOrderrelation);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollOrderrelation> findDataRollOrderrelation() {
        List<RollOrderrelation> list = rollOrderrelationDao.findDataRollOrderrelation();
        return list;
    }

}
