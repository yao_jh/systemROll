package com.my.business.modular.rollorderrelation.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 工单关联表实体类
 *
 * @author 生成器生成
 * @date 2020-09-09 10:05:16
 */
public class RollOrderrelation extends BaseEntity {

    private Long indocno;  //主键
    private String order_no;  //工单编码
    private String order_name;  //工单名称
    private String method_url;  //接口地址
    private String order_entity;  //提交工单对应的实体对象
    private String snote;  //备注

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getOrder_no() {
        return this.order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getOrder_name() {
        return this.order_name;
    }

    public void setOrder_name(String order_name) {
        this.order_name = order_name;
    }

    public String getMethod_url() {
        return this.method_url;
    }

    public void setMethod_url(String method_url) {
        this.method_url = method_url;
    }

    public String getOrder_entity() {
        return this.order_entity;
    }

    public void setOrder_entity(String order_entity) {
        this.order_entity = order_entity;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }


}