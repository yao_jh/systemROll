package com.my.business.modular.rollmachine.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.rollmachine.entity.RollMachine;
import com.my.business.modular.rollmachine.entity.JRollMachine;
import com.my.business.modular.rollmachine.dao.RollMachineDao;
import com.my.business.modular.rollmachine.service.RollMachineService;
import com.my.business.sys.common.entity.ResultData;

/**
* 磨床接口具体实现类
* @author  生成器生成
* @date 2020-11-26 17:09:28
*/
@Service
public class RollMachineServiceImpl implements RollMachineService {
	
	@Autowired
	private RollMachineDao rollMachineDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollMachine(String data,Long userId,String sname){
		try{
			JRollMachine jrollMachine = JSON.parseObject(data,JRollMachine.class);
    		RollMachine rollMachine = jrollMachine.getRollMachine();
    		CodiUtil.newRecord(userId,sname,rollMachine);
            rollMachineDao.insertDataRollMachine(rollMachine);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollMachineOne(Long indocno){
		try {
            rollMachineDao.deleteDataRollMachineOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollMachineMany(String str_id) {
        try {
        	String sql = "delete roll_machine where indocno in(" + str_id +")";
            rollMachineDao.deleteDataRollMachineMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataRollMachine(String data,Long userId,String sname){
		try {
			JRollMachine jrollMachine = JSON.parseObject(data,JRollMachine.class);
    		RollMachine rollMachine = jrollMachine.getRollMachine();
        	CodiUtil.editRecord(userId,sname,rollMachine);
            rollMachineDao.updateDataRollMachine(rollMachine);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataRollMachineByIndocno(String data){
		try {
    		RollMachine rollMachine = JSON.parseObject(data,RollMachine.class);
            rollMachineDao.updateDataRollMachineByIndono(rollMachine.getIndocno(), rollMachine.getMachine_state());
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	public ResultData findDataRollMachineState(String data){
		try {
    		RollMachine rollMachine = JSON.parseObject(data,RollMachine.class);
            Integer i = rollMachineDao.findDataRollMachineState(rollMachine.getIndocno());
            return ResultData.ResultDataSuccessSelf("查询成功", i);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollMachineByPage(String data) {
        try {
        	JRollMachine jrollMachine = JSON.parseObject(data, JRollMachine.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jrollMachine.getPageIndex();
        	Integer pageSize = jrollMachine.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jrollMachine.getCondition()){
    			jsonObject  = JSON.parseObject(jrollMachine.getCondition().toString());
    		}
    
    		List<RollMachine> list = rollMachineDao.findDataRollMachineByPage((pageIndex-1)*pageSize, pageSize);
    		Integer count = rollMachineDao.findDataRollMachineByPageSize();
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollMachineByIndocno(String data) {
        try {
        	JRollMachine jrollMachine = JSON.parseObject(data, JRollMachine.class); 
        	Long indocno = jrollMachine.getIndocno();
        	
    		RollMachine rollMachine = rollMachineDao.findDataRollMachineByIndocno(indocno);
    		return ResultData.ResultDataSuccess(rollMachine);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<RollMachine> findDataRollMachine(){
		List<RollMachine> list = rollMachineDao.findDataRollMachine();
		return list;
	};
}
