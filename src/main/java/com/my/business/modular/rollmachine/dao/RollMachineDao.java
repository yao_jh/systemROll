package com.my.business.modular.rollmachine.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.rollmachine.entity.RollMachine;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 磨床dao接口
 * @author  生成器生成
 * @date 2020-11-26 17:09:28
 * */
@Mapper
public interface RollMachineDao {

	/**
	 * 添加记录
	 * @param rollMachine  对象实体
	 * */
	public void insertDataRollMachine(RollMachine rollMachine);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataRollMachineOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataRollMachineMany(String value);
	
	/**
	 * 修改记录
	 * @param rollMachine  对象实体
	 * */
	public void updateDataRollMachine(RollMachine rollMachine);
	
	/**
	 * 修改记录
	 * @param rollMachine  对象实体
	 * */
	public void updateDataRollMachineByIndono(@Param("indocno") Long indocno,@Param("machine_state") Long machine_state);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<RollMachine> findDataRollMachineByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);
    
    
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataRollMachineState(@Param("indocno") Long indocno);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataRollMachineByPageSize();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public RollMachine findDataRollMachineByIndocno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<RollMachine> findDataRollMachine();
	
}
