package com.my.business.modular.rollmachine.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.modular.rollmachine.entity.RollMachine;
import java.util.List;

/**
* 磨床接口服务类
* @author  生成器生成
* @date 2020-11-26 17:09:28
*/
public interface RollMachineService {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollMachine(String data,Long userId,String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollMachineOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollMachineMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataRollMachine(String data,Long userId,String sname);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataRollMachineByIndocno(String data);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollMachineByPage(String data);
    
    /**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollMachineState(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataRollMachineByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<RollMachine> findDataRollMachine();
}
