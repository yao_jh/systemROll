package com.my.business.modular.rollmachine.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.modular.rollmachine.entity.RollMachine;
import com.my.business.modular.rollmachine.entity.JRollMachine;
import com.my.business.modular.rollmachine.service.RollMachineService;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import com.my.business.sys.common.entity.ResultData;
import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.my.business.util.CodiUtil;
import org.springframework.web.bind.annotation.RequestHeader;


/**
* 磨床控制器层
* @author  生成器生成
* @date 2020-11-26 17:09:28
*/
@RestController
@RequestMapping("/rollMachine")
public class RollMachineController {

	@Autowired
	private RollMachineService rollMachineService;
	
	/**
	 * 添加记录
	 * @param userId 用户id
     * @param sname 用户姓名
	 * */
	@CrossOrigin
	@RequestMapping(value={"/insert"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData insertDataRollMachine(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return rollMachineService.insertDataRollMachine(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
	 * 根据主键删除对象
	 * @param data json字符串
	 * */
	@CrossOrigin
	@RequestMapping(value={"/deleteOne"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData deleteDataRollMachineOne(@RequestBody String data){
		try{
    		JRollMachine jrollMachine = JSON.parseObject(data,JRollMachine.class);
    		return rollMachineService.deleteDataRollMachineOne(jrollMachine.getIndocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
    	try{
    		JRollMachine jrollMachine = JSON.parseObject(data,JRollMachine.class);
    		return rollMachineService.deleteDataRollMachineMany(jrollMachine.getStr_indocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
    }
	
	/**
     * 修改记录
     * @param data json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
	@RequestMapping(value={"/update"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData updateDataRollMachine(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return rollMachineService.updateDataRollMachine(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
     * 修改记录
     * @param data json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
	@RequestMapping(value={"/updateState"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData updateState(@RequestBody String data){
		return rollMachineService.updateDataRollMachineByIndocno(data);
	};
	
	/**
     * 分页查看记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollMachineByPage(@RequestBody String data) {
        return rollMachineService.findDataRollMachineByPage(data);
    }
    
    /**
     * 分页查看记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findstate"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findstate(@RequestBody String data) {
        return rollMachineService.findDataRollMachineState(data);
    }
    
    /**
     * 根据主键查询单条记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollMachineByIndocno(@RequestBody String data) {
        return rollMachineService.findDataRollMachineByIndocno(data);
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	@CrossOrigin
	@RequestMapping(value={"/findAll"}, method=RequestMethod.POST)
	public List<RollMachine> findDataRollMachine(){
		return rollMachineService.findDataRollMachine();
	};
}
