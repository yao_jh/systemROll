package com.my.business.modular.rollmachine.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
* 磨床实体类
* @author  生成器生成
* @date 2020-11-26 17:09:27
*/
public class RollMachine extends BaseEntity{
		
	private Long indocno;  //主键
	private String machine_name;  //磨床名称
	private Long machine_state;  //磨床状态
    
    
	public void setIndocno(Long indocno){
	    this.indocno = indocno;
	}
	public Long getIndocno(){
	    return this.indocno;
	}
	public void setMachine_name(String machine_name){
	    this.machine_name = machine_name;
	}
	public String getMachine_name(){
	    return this.machine_name;
	}
	public void setMachine_state(Long machine_state){
	    this.machine_state = machine_state;
	}
	public Long getMachine_state(){
	    return this.machine_state;
	}

    
}