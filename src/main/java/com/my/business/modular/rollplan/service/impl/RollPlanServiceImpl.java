package com.my.business.modular.rollplan.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.JRollInformation;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.modular.rollplan.dao.RollPlanDao;
import com.my.business.modular.rollplan.entity.JRollPlan;
import com.my.business.modular.rollplan.entity.RollPlan;
import com.my.business.modular.rollplan.service.RollPlanService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 2250轧辊计划接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-10-10 14:28:22
 */
@Service
public class RollPlanServiceImpl implements RollPlanService {

    @Autowired
    private RollPlanDao rollPlanDao;
    @Autowired
    private RollInformationDao rollInformationDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollPlan(String data, Long userId, String sname) {
        try {
            JRollPlan jrollPlan = JSON.parseObject(data, JRollPlan.class);
            RollPlan rollPlan = jrollPlan.getRollPlan();
            CodiUtil.newRecord(userId, sname, rollPlan);
            rollPlanDao.insertDataRollPlan(rollPlan);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollPlanOne(Long indocno) {
        try {
            rollPlanDao.deleteDataRollPlanOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollPlanMany(String str_id) {
        try {
            String sql = "delete roll_plan where indocno in(" + str_id + ")";
            rollPlanDao.deleteDataRollPlanMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollPlan(String data, Long userId, String sname) {
        try {
            JRollPlan jrollPlan = JSON.parseObject(data, JRollPlan.class);
            RollPlan rollPlan = jrollPlan.getRollPlan();
            CodiUtil.editRecord(userId, sname, rollPlan);
            rollPlanDao.updateDataRollPlan(rollPlan);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }
    
    /**
     * 修改创建字段记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateCreate(String data) {
        try {
            JRollPlan jrollPlan = JSON.parseObject(data, JRollPlan.class);
            rollPlanDao.updateCreate(jrollPlan.getIndocno());
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }
    
    /**
     * 到货+1
     *
     * @param data
     */
    public ResultData updateRealTotal(String data) {
        try {
            JRollPlan jrollPlan = JSON.parseObject(data, JRollPlan.class);
            RollPlan rollPlan = jrollPlan.getRollPlan();
            rollPlanDao.updateRealTotal(rollPlan.getIndocno(), rollPlan.getReal_total());
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPlanByPage(String data) {
        try {
            JRollPlan jrollPlan = JSON.parseObject(data, JRollPlan.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollPlan.getPageIndex();
            Integer pageSize = jrollPlan.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollPlan.getCondition()) {
                jsonObject = JSON.parseObject(jrollPlan.getCondition().toString());
            }

            String roll_type = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_type"))) {
                roll_type = jsonObject.get("roll_type").toString();
            }

            String factory = null;
            if (!StringUtils.isEmpty(jsonObject.get("factory"))) {
                factory = jsonObject.get("factory").toString();
            }

            String contract_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("contract_no"))) {
                contract_no = jsonObject.get("contract_no").toString();
            }

            String yeard = null;
            if (!StringUtils.isEmpty(jsonObject.get("yeard"))) {
                yeard = jsonObject.get("yeard").toString();
            }

            List<RollPlan> list = rollPlanDao.findDataRollPlanByPage((pageIndex - 1) * pageSize, pageSize,roll_type,factory,contract_no,yeard);
            Integer count = rollPlanDao.findDataRollPlanByPageSize(roll_type,factory,contract_no,yeard);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPlanByIndocno(String data) {
        try {
            JRollPlan jrollPlan = JSON.parseObject(data, JRollPlan.class);
            Long indocno = jrollPlan.getIndocno();

            RollPlan rollPlan = rollPlanDao.findDataRollPlanByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollPlan);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollPlan> findDataRollPlan() {
        List<RollPlan> list = rollPlanDao.findDataRollPlan();
        return list;
    }

    /**
     * 确认按钮接口
     *
     * @param data json字符串
     * @return 状态
     */
    public ResultData commit(String data, Long userId, String sname) {
        try {
            JRollPlan jrollPlan = JSON.parseObject(data, JRollPlan.class);
            RollPlan rollPlan = jrollPlan.getRollPlan();
            CodiUtil.editRecord(userId, sname, rollPlan);
            rollPlanDao.updateDataRollPlanOnMath(rollPlan);

            Long production_line_id = 1L;
            String production_line = "2250";
            String order_year = rollPlan.getYeard();
            Long roll_state = 0L;
            if (!StringUtils.isEmpty(rollPlan.getF1_f4_fw()) && rollPlan.getF1_f4_fw() != 0L) {
                for (int i = 0; i < rollPlan.getF1_f4_fw(); i++) {
                    RollInformation rs = new RollInformation();
                    rs.setProduction_line_id(production_line_id); //产线id
                    rs.setProduction_line(production_line);  //产线
                    rs.setOrder_year(order_year);  //订货年

                    String roll_no = "RN000" + i + "X";
                    rs.setRoll_no(roll_no);  //虚拟辊号

                    rs.setRoll_typeid(1L); //轧辊类型id
                    rs.setRoll_type("精轧工作辊"); //轧辊类型

                    rs.setFramerangeid(1L);  //机架范围id
                    rs.setFramerange("F1-F4");  //机架范围

                    rs.setRoll_state(roll_state);//轧辊状态 在途轧辊
                    CodiUtil.newRecord(userId, sname, rs);
                    rollInformationDao.insertDataRollInformation(rs);
                }
            }

            if (!StringUtils.isEmpty(rollPlan.getF5_f7_fw()) && rollPlan.getF5_f7_fw() != 0L) {
                for (int i = 0; i < rollPlan.getF5_f7_fw(); i++) {
                    RollInformation rs = new RollInformation();
                    rs.setProduction_line_id(production_line_id); //产线id
                    rs.setProduction_line(production_line);  //产线
                    rs.setOrder_year(order_year);  //订货年

                    String roll_no = "RN100" + i + "X";
                    rs.setRoll_no(roll_no);  //虚拟辊号

                    rs.setRoll_typeid(1L); //轧辊类型id
                    rs.setRoll_type("精轧工作辊"); //轧辊类型

                    rs.setFramerangeid(2L);  //机架范围id
                    rs.setFramerange("F5-F7");  //机架范围

                    rs.setRoll_state(roll_state);//轧辊状态 在途轧辊
                    CodiUtil.newRecord(userId, sname, rs);
                    rollInformationDao.insertDataRollInformation(rs);
                }
            }

            if (!StringUtils.isEmpty(rollPlan.getFb()) && rollPlan.getFb() != 0L) {
                for (int i = 0; i < rollPlan.getFb(); i++) {
                    RollInformation rs = new RollInformation();
                    rs.setProduction_line_id(production_line_id); //产线id
                    rs.setProduction_line(production_line);  //产线
                    rs.setOrder_year(order_year);  //订货年

                    String roll_no = "RN200" + i + "X";
                    rs.setRoll_no(roll_no);  //虚拟辊号

                    rs.setRoll_typeid(3L); //轧辊类型id
                    rs.setRoll_type("精轧支撑辊"); //轧辊类型

                    rs.setFramerangeid(3L);  //机架范围id
                    rs.setFramerange("F1-F7");  //机架范围

                    rs.setRoll_state(roll_state);//轧辊状态 在途轧辊
                    CodiUtil.newRecord(userId, sname, rs);
                    rollInformationDao.insertDataRollInformation(rs);
                }
            }

            if (!StringUtils.isEmpty(rollPlan.getF7hw()) && rollPlan.getF7hw() != 0L) {
                for (int i = 0; i < rollPlan.getF7hw(); i++) {
                    RollInformation rs = new RollInformation();
                    rs.setProduction_line_id(production_line_id); //产线id
                    rs.setProduction_line(production_line);  //产线
                    rs.setOrder_year(order_year);  //订货年

                    String roll_no = "RN300" + i + "X";
                    rs.setRoll_no(roll_no);  //虚拟辊号

                    rs.setRoll_typeid(14L); //轧辊类型id
                    rs.setRoll_type("精轧工作辊"); //轧辊类型

                    rs.setFramerangeid(8L);  //机架范围id
                    rs.setFramerange("F7");  //机架范围

                    rs.setRoll_state(roll_state);//轧辊状态 在途轧辊
                    CodiUtil.newRecord(userId, sname, rs);
                    rollInformationDao.insertDataRollInformation(rs);
                }
            }

            if (!StringUtils.isEmpty(rollPlan.getF1e()) && rollPlan.getF1e() != 0L) {
                for (int i = 0; i < rollPlan.getF1e(); i++) {
                    RollInformation rs = new RollInformation();
                    rs.setProduction_line_id(production_line_id); //产线id
                    rs.setProduction_line(production_line);  //产线
                    rs.setOrder_year(order_year);  //订货年

                    String roll_no = "RN400" + i + "X";
                    rs.setRoll_no(roll_no);  //虚拟辊号

                    rs.setRoll_typeid(9L); //轧辊类型id
                    rs.setRoll_type("精轧立辊"); //轧辊类型

                    rs.setFramerangeid(9L);  //机架范围id
                    rs.setFramerange("F1E");  //机架范围

                    rs.setRoll_state(roll_state);//轧辊状态 在途轧辊
                    CodiUtil.newRecord(userId, sname, rs);
                    rollInformationDao.insertDataRollInformation(rs);
                }
            }

            if (!StringUtils.isEmpty(rollPlan.getRw()) && rollPlan.getRw() != 0L) {
                for (int i = 0; i < rollPlan.getRw(); i++) {
                    RollInformation rs = new RollInformation();
                    rs.setProduction_line_id(production_line_id); //产线id
                    rs.setProduction_line(production_line);  //产线
                    rs.setOrder_year(order_year);  //订货年

                    String roll_no = "RN500" + i + "X";
                    rs.setRoll_no(roll_no);  //虚拟辊号

                    rs.setRoll_typeid(4L); //轧辊类型id
                    rs.setRoll_type("粗轧工作辊"); //轧辊类型

                    rs.setFramerangeid(4L);  //机架范围id
                    rs.setFramerange("R1-R2");  //机架范围

                    rs.setRoll_state(roll_state);//轧辊状态 在途轧辊
                    CodiUtil.newRecord(userId, sname, rs);
                    rollInformationDao.insertDataRollInformation(rs);
                }
            }

            if (!StringUtils.isEmpty(rollPlan.getRb()) && rollPlan.getRb() != 0L) {
                for (int i = 0; i < rollPlan.getRb(); i++) {
                    RollInformation rs = new RollInformation();
                    rs.setProduction_line_id(production_line_id); //产线id
                    rs.setProduction_line(production_line);  //产线
                    rs.setOrder_year(order_year);  //订货年

                    String roll_no = "RN600" + i + "X";
                    rs.setRoll_no(roll_no);  //虚拟辊号

                    rs.setRoll_typeid(5L); //轧辊类型id
                    rs.setRoll_type("粗轧支撑辊"); //轧辊类型

                    rs.setFramerangeid(4L);  //机架范围id
                    rs.setFramerange("R1-R2");  //机架范围

                    rs.setRoll_state(roll_state);//轧辊状态 在途轧辊
                    CodiUtil.newRecord(userId, sname, rs);
                    rollInformationDao.insertDataRollInformation(rs);
                }
            }

            if (!StringUtils.isEmpty(rollPlan.getRe()) && rollPlan.getRe() != 0L) {
                for (int i = 0; i < rollPlan.getRe(); i++) {
                    RollInformation rs = new RollInformation();
                    rs.setProduction_line_id(production_line_id); //产线id
                    rs.setProduction_line(production_line);  //产线
                    rs.setOrder_year(order_year);  //订货年

                    String roll_no = "RN700" + i + "X";
                    rs.setRoll_no(roll_no);  //虚拟辊号

                    rs.setRoll_typeid(8L); //轧辊类型id
                    rs.setRoll_type("粗轧立辊"); //轧辊类型

                    rs.setFramerangeid(4L);  //机架范围id
                    rs.setFramerange("R1-R2");  //机架范围

                    rs.setRoll_state(roll_state);//轧辊状态 在途轧辊
                    CodiUtil.newRecord(userId, sname, rs);
                    rollInformationDao.insertDataRollInformation(rs);
                }
            }

            if (!StringUtils.isEmpty(rollPlan.getS1w()) && rollPlan.getS1w() != 0L) {
                for (int i = 0; i < rollPlan.getS1w(); i++) {
                    RollInformation rs = new RollInformation();
                    rs.setProduction_line_id(production_line_id); //产线id
                    rs.setProduction_line(production_line);  //产线
                    rs.setOrder_year(order_year);  //订货年

                    String roll_no = "RN800" + i + "X";
                    rs.setRoll_no(roll_no);  //虚拟辊号

                    rs.setRoll_typeid(6L); //轧辊类型id
                    rs.setRoll_type("平整工作辊"); //轧辊类型

                    rs.setFramerangeid(6L);  //机架范围id
                    rs.setFramerange("S1-S2");  //机架范围

                    rs.setRoll_state(roll_state);//轧辊状态 在途轧辊
                    CodiUtil.newRecord(userId, sname, rs);
                    rollInformationDao.insertDataRollInformation(rs);
                }
            }

            if (!StringUtils.isEmpty(rollPlan.getS1b()) && rollPlan.getS1b() != 0L) {
                for (int i = 0; i < rollPlan.getS1b(); i++) {
                    RollInformation rs = new RollInformation();
                    rs.setProduction_line_id(production_line_id); //产线id
                    rs.setProduction_line(production_line);  //产线
                    rs.setOrder_year(order_year);  //订货年

                    String roll_no = "RN900" + i + "X";
                    rs.setRoll_no(roll_no);  //虚拟辊号

                    rs.setRoll_typeid(7L); //轧辊类型id
                    rs.setRoll_type("平整支撑辊"); //轧辊类型

                    rs.setFramerangeid(6L);  //机架范围id
                    rs.setFramerange("S1-S2");  //机架范围

                    rs.setRoll_state(roll_state);//轧辊状态 在途轧辊
                    CodiUtil.newRecord(userId, sname, rs);
                    rollInformationDao.insertDataRollInformation(rs);
                }
            }

            if (!StringUtils.isEmpty(rollPlan.getHm()) && rollPlan.getHm() != 0L) {
                for (int i = 0; i < rollPlan.getHm(); i++) {
                    RollInformation rs = new RollInformation();
                    rs.setProduction_line_id(production_line_id); //产线id
                    rs.setProduction_line(production_line);  //产线
                    rs.setOrder_year(order_year);  //订货年

                    String roll_no = "RN110" + i + "X";
                    rs.setRoll_no(roll_no);  //虚拟辊号

                    rs.setRoll_typeid(10L); //轧辊类型id
                    rs.setRoll_type("锤头"); //轧辊类型

                    rs.setFramerangeid(10L);  //机架范围id
                    rs.setFramerange("SSP");  //机架范围

                    rs.setRoll_state(roll_state);//轧辊状态 在途轧辊
                    CodiUtil.newRecord(userId, sname, rs);
                    rollInformationDao.insertDataRollInformation(rs);
                }
            }
            return ResultData.ResultDataSuccess("生成成功");
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("生成失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 到货
     *
     * @param data   sysUser 对象实体
     */
    public ResultData gethave(String data) {
        try {
            JRollInformation jRollInformation = JSON.parseObject(data, JRollInformation.class);
            RollInformation rollInformation = jRollInformation.getRollInformation();
            rollInformation.setRoll_state(1L);
            rollInformationDao.updateDataRollInformation(rollInformation);

            //统计
            rollPlanDao.updateArriveTotal(rollInformation.getOrder_year());
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }
}
