package com.my.business.modular.rollplan.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.rollplan.entity.RollPlan;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 2250轧辊计划dao接口
 * @author  生成器生成
 * @date 2020-10-10 14:28:22
 * */
@Mapper
public interface RollPlanDao {

	/**
	 * 添加记录
	 * @param rollPlan  对象实体
	 * */
	public void insertDataRollPlan(RollPlan rollPlan);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataRollPlanOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataRollPlanMany(String value);
	
	/**
	 * 修改记录
	 * @param rollPlan  对象实体
	 * */
	public void updateDataRollPlan(RollPlan rollPlan);
	
	/**
	 * 修改是否生成字段
	 * @param rollPlan  对象实体
	 * */
	public void updateCreate(@Param("indocno") Long indocno);
	
	/**
	 * 到货数量加1
	 * @param rollPlan  对象实体
	 * */
	public void updateRealTotal(@Param("indocno") Long indocno,@Param("real_total") Long real_total);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<RollPlan> findDataRollPlanByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("roll_type") String roll_type,@Param("factory") String factory,@Param("contract_no") String contract_no,@Param("yeard") String yeard);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataRollPlanByPageSize(@Param("roll_type") String roll_type,@Param("factory") String factory,@Param("contract_no") String contract_no,@Param("yeard") String yeard);
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public RollPlan findDataRollPlanByIndocno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<RollPlan> findDataRollPlan();

	/**
	 * 确认数量
	 * @param rollPlan 轧辊数量
	 */
    void updateDataRollPlanOnMath(RollPlan rollPlan);

	/**
	 * 更新到货数量
	 * @param order_year 订货年
	 */
	void updateArriveTotal(@Param("order_year") String order_year);
}
