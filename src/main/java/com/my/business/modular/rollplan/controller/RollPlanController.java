package com.my.business.modular.rollplan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.modular.rollplan.entity.RollPlan;
import com.my.business.modular.rollplan.entity.JRollPlan;
import com.my.business.modular.rollplan.service.RollPlanService;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import com.my.business.sys.common.entity.ResultData;
import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.my.business.util.CodiUtil;
import org.springframework.web.bind.annotation.RequestHeader;


/**
* 2250轧辊计划控制器层
* @author  生成器生成
* @date 2020-10-10 14:28:22
*/
@RestController
@RequestMapping("/rollPlan")
public class RollPlanController {

	@Autowired
	private RollPlanService rollPlanService;
	
	/**
	 * 添加记录
	 * @param data userId 用户id
     * @param data sname 用户姓名
	 * */
	@CrossOrigin
	@RequestMapping(value={"/insert"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData insertDataRollPlan(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return rollPlanService.insertDataRollPlan(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
	 * 根据主键删除对象
	 * @param data json字符串
	 * */
	@CrossOrigin
	@RequestMapping(value={"/deleteOne"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData deleteDataRollPlanOne(@RequestBody String data){
		try{
    		JRollPlan jrollPlan = JSON.parseObject(data,JRollPlan.class);
    		return rollPlanService.deleteDataRollPlanOne(jrollPlan.getIndocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
    	try{
    		JRollPlan jrollPlan = JSON.parseObject(data,JRollPlan.class);
    		return rollPlanService.deleteDataRollPlanMany(jrollPlan.getStr_indocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
    }
	
	/**
     * 修改记录
     * @param data json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
	@RequestMapping(value={"/update"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData updateDataRollPlan(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return rollPlanService.updateDataRollPlan(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
     * 修改创建字段
     * @param data json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
	@RequestMapping(value={"/updateCreate"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData updateCreate(@RequestBody String data){
		return rollPlanService.updateCreate(data);
	};
	
	/**
     * 到货+1
     * @param data json字符串
     */
    @CrossOrigin
	@RequestMapping(value={"/updateRealTotal"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData updateRealTotal(@RequestBody String data){
		return rollPlanService.updateRealTotal(data);
	};
	
	/**
     * 分页查看记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollPlanByPage(@RequestBody String data) {
        return rollPlanService.findDataRollPlanByPage(data);
    }
    
    /**
     * 根据主键查询单条记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollPlanByIndocno(@RequestBody String data) {
        return rollPlanService.findDataRollPlanByIndocno(data);
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	@CrossOrigin
	@RequestMapping(value={"/findAll"}, method=RequestMethod.POST)
	public List<RollPlan> findDataRollPlan(){
		return rollPlanService.findDataRollPlan();
	};

	/**
	 * 确认按钮接口
	 */
	@CrossOrigin
	@RequestMapping(value = {"/commit"}, method = RequestMethod.POST)
	@ResponseBody
	public ResultData commit(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken) {
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
		String sname = common.getSname();
		Long userId = common.getUserId();
		return rollPlanService.commit(data,userId,CodiUtil.returnLm(sname));
	}

	/**
	 * 到货按钮接口
	 */
	@CrossOrigin
	@RequestMapping(value = {"/gethave"}, method = RequestMethod.POST)
	@ResponseBody
	public ResultData gethave(@RequestBody String data) {
		return rollPlanService.gethave(data);
	}
}
