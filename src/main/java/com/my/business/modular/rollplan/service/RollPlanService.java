package com.my.business.modular.rollplan.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.modular.rollplan.entity.RollPlan;
import java.util.List;

/**
* 2250轧辊计划接口服务类
* @author  生成器生成
* @date 2020-10-10 14:28:22
*/
public interface RollPlanService {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollPlan(String data, Long userId, String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollPlanOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollPlanMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataRollPlan(String data, Long userId, String sname);
	
	/**
     * 修改是否创建字段
     * @param data json字符串
     */
	public ResultData updateCreate(String data);
	
	/**
     * 到货+1
     * @param data json字符串
     */
	public ResultData updateRealTotal(String data);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPlanByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPlanByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<RollPlan> findDataRollPlan();

	/**
	 * 确认按钮接口
	 * @param data json字符串
	 * @return 状态
	 */
    ResultData commit(String data, Long userId, String sname);

	/**
	 * 到货接口
	 * @param data json字符串
	 * @return 状态
	 */
	ResultData gethave(String data);
}
