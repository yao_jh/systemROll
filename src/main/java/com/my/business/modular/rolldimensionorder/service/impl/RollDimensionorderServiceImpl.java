package com.my.business.modular.rolldimensionorder.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.intellectpush.entity.Orderdetail;
import com.my.business.modular.rolldimensionorder.dao.RollDimensionorderDao;
import com.my.business.modular.rolldimensionorder.entity.JRollDimensionorder;
import com.my.business.modular.rolldimensionorder.entity.RollDimensionorder;
import com.my.business.modular.rolldimensionorder.service.RollDimensionorderService;
import com.my.business.modular.rollorderconfig.dao.RollOrderconfigDao;
import com.my.business.modular.rollorderconfigdetail.entity.RollOrderconfigdetail;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.workflow.workflowdetailperform.dao.WorkFlowdetailPerformDao;
import com.my.business.workflow.workflowdetailperform.entity.WorkFlowdetailPerform;
import com.my.business.workflow.workparam.dao.WorkParamDao;
import com.my.business.workflow.workparam.entity.WorkParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 新辊尺寸表面工单数据接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-10 11:13:21
 */
@Service
public class RollDimensionorderServiceImpl implements RollDimensionorderService {

    @Autowired
    private RollDimensionorderDao rollDimensionorderDao;

    @Autowired
    private RollOrderconfigDao rollOrderconfigDao;
    
    @Autowired
    private WorkFlowdetailPerformDao workFlowdetailPerformDao;
    
    @Autowired
    private WorkParamDao workParamDao;
    

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollDimensionorder(String data, Long userId, String sname) {
        try {
            JRollDimensionorder jrollDimensionorder = JSON.parseObject(data, JRollDimensionorder.class);
            RollDimensionorder rollDimensionorder = jrollDimensionorder.getRollDimensionorder();
            CodiUtil.newRecord(userId, sname, rollDimensionorder);
            rollDimensionorder.setIlinkno(jrollDimensionorder.getOrder_no());
            rollDimensionorderDao.insertDataRollDimensionorder(rollDimensionorder);
            
            
            WorkFlowdetailPerform wp = workFlowdetailPerformDao.findDataWorkFlowdetailPerformByNodeidAndNoOne(jrollDimensionorder.getPerform_no(), jrollDimensionorder.getNodeid());
            Boolean b = getIfOkByentity(rollDimensionorder);
            String l = "";
            if(b) {
            	wp.setIfok(1L);
            	l = "1";
            }else {
            	wp.setIfok(0L);
            	l = "0";
            }
            
            List<WorkParam> paramList = workParamDao.findDataWorkParamAll(jrollDimensionorder.getFlowid());
        	Map<String,String> map = new HashMap<String,String>();
        	for(WorkParam p : paramList) {
        		if(p.getField_no().equals("ifok")) {
        			map.put(p.getField_no(), l);
        		}else {
        			map.put(p.getField_no(), "");
        		}
        	}
            workFlowdetailPerformDao.updateDataWorkFlowdetailPerform(wp);
            
            return ResultData.ResultDataSuccessSelf("添加数据成功", map);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }
    
    /***
     * 根据对象判断是否合格
     * @return
     */
    public Boolean getIfOkByentity(RollDimensionorder rollDimensionorder) {
    	 try {
             //根据工单获取标准配置，工单配置
             List<RollOrderconfigdetail> detail = rollOrderconfigDao.findDataRollOrderconfigByNo(rollDimensionorder.getIlinkno()).getDetail();
             //通过反射获取类含有field的字段
             Field[] f = RollDimensionorder.class.getDeclaredFields();  //获取新辊尺寸检测工单字段数组
             for (Field ff : f) {
                 ff.setAccessible(true);// 假设不为空。设置可见性，然后返回,这里必须有，不然报错
                 String field_name = ff.getName();
                 if (field_name.indexOf("field") > -1) {  //查看哪些字段含有field字样，这里是只需要含有field字样的字段，用来和工单配置中indexs一一对应
                     Long i = Long.valueOf(field_name.substring(5)); //获取含有field字眼字段后面的数字，好和工单配置中的indexs对应
                     for (RollOrderconfigdetail r : detail) {
                         if (ff.get(rollDimensionorder) != null) {
                             String order_value = ff.get(rollDimensionorder).toString();
                             if (i == r.getIndexs()) {
                                 //标准类型  标准1还是其他2
                                 if (r.getRollStandard().getStandard_type() == 1) {  //标准字段才需要做判断
                                     //字段类型   区间1还是布尔0
                                     if (r.getRollStandard().getField_type().equals("1")) {   //区间
                                         Double max = r.getRollStandard().getField_max(); //上区间
                                         Double min = r.getRollStandard().getField_min();  //下区间
                                         if (max == 100001) {  //表示上限无限
                                             Double x = Double.valueOf(order_value);   //工单填写值
                                             Double y = Double.valueOf(min);   //标准判断值
                                             if (x >= y) {
                                                 return true;
                                             } else {
                                                 return false;
                                             }
                                         } else if (min == 100000) { //表示下限无限
                                             Double x = Double.valueOf(order_value);   //工单填写值
                                             Double y = Double.valueOf(max);   //标准判断值
                                             if (x <= y) {
                                                 return true;
                                             } else {
                                                 return false;
                                             }
                                         } else {  //表示在具体数值的区间，而不是含有上下无穷+∞这样的
                                             Double x = Double.valueOf(order_value);   //工单填写值
                                             Double y = Double.valueOf(max);   //标准判断值上限
                                             Double z = Double.valueOf(min);   //标准判断值下限
                                             if (x <= y) {//填写值小于等于最大值,接着判断填写值是否小于最小值
                                             	if(x >= z) {//填写值大于最小值
                                             		return true;
                                             	}else {//填写值小于最小值，不合格
                                             		return false;
                                             	}
                                             }else {//填写值大于最大值，不合格
                                             	return false;
                                             }
                                         }
                                     } else if (r.getRollStandard().getField_type().equals("0")) {  //布尔
                                         String fieldv = r.getRollStandard().getField_value();
                                         if(fieldv.equals("0")) {
                                        	 return false;
                                         }else if(fieldv.equals("1")) {
                                        	 return true;
                                         }
                                     }
                                 }
                             }
                         }
                     }
                 }
             }
    	 }catch(Exception e) {
    		 e.printStackTrace();
    		 return false;
    	 }
		return true;
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollDimensionorderOne(Long indocno) {
        try {
            rollDimensionorderDao.deleteDataRollDimensionorderOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollDimensionorderMany(String str_id) {
        try {
            String sql = "delete roll_dimensionorder where indocno in(" + str_id + ")";
            rollDimensionorderDao.deleteDataRollDimensionorderMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataRollDimensionorder(String data, Long userId, String sname) {
        try {
            JRollDimensionorder jrollDimensionorder = JSON.parseObject(data, JRollDimensionorder.class);
            RollDimensionorder rollDimensionorder = jrollDimensionorder.getRollDimensionorder();
            CodiUtil.editRecord(userId, sname, rollDimensionorder);
            rollDimensionorderDao.updateDataRollDimensionorder(rollDimensionorder);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollDimensionorderByPage(String data) {
        try {
            JRollDimensionorder jrollDimensionorder = JSON.parseObject(data, JRollDimensionorder.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollDimensionorder.getPageIndex();
            Integer pageSize = jrollDimensionorder.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollDimensionorder.getCondition()) {
                jsonObject = JSON.parseObject(jrollDimensionorder.getCondition().toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

            String roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = jsonObject.get("roll_typeid").toString();
            }

            String factory = null;
            if (!StringUtils.isEmpty(jsonObject.get("factory"))) {
                factory = jsonObject.get("factory").toString();
            }

            String material = null;
            if (!StringUtils.isEmpty(jsonObject.get("material"))) {
                material = jsonObject.get("material").toString();
            }

            List<RollDimensionorder> list = rollDimensionorderDao.findDataRollDimensionorderByPage((pageIndex - 1)*pageSize, pageSize, roll_no, factory, material, roll_typeid);
            Integer count = rollDimensionorderDao.findDataRollDimensionorderByPageSize(roll_no, factory, material, roll_typeid);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollDimensionorderByIndocno(String data) {
        try {
            JRollDimensionorder jrollDimensionorder = JSON.parseObject(data, JRollDimensionorder.class);
            Long indocno = jrollDimensionorder.getIndocno();

            RollDimensionorder rollDimensionorder = rollDimensionorderDao.findDataRollDimensionorderByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollDimensionorder);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }
    
    /**
     * 根据编码查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollDimensionorderByNo(String data) {
        try {
        	RollDimensionorder jrollDimensionorder = JSON.parseObject(data, RollDimensionorder.class);
            String roll_no = jrollDimensionorder.getRoll_no();

            RollDimensionorder rollDimensionorder = rollDimensionorderDao.findDataRollDimensionorderByNo(roll_no);
            return ResultData.ResultDataSuccess(rollDimensionorder);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollDimensionorder> findDataRollDimensionorder() {
        List<RollDimensionorder> list = rollDimensionorderDao.findDataRollDimensionorder();
        return list;
    }

    /***
     * 查看工单详情
     * @return
     */
    @Override
    public ResultData findOrderAll(String data) {
        try {
            JRollDimensionorder jrollDimensionorder = JSON.parseObject(data, JRollDimensionorder.class);
            RollDimensionorder rollDimensionorder = jrollDimensionorder.getRollDimensionorder(); //新辊尺寸检测工单

            List<Orderdetail> order_list = new ArrayList<Orderdetail>();

            //根据工单获取标准配置，工单配置
            List<RollOrderconfigdetail> detail = rollOrderconfigDao.findDataRollOrderconfigByNo(rollDimensionorder.getIlinkno()).getDetail();

            //通过反射获取类含有field的字段
            Field[] f = RollDimensionorder.class.getDeclaredFields();  //获取新辊尺寸检测工单字段数组
            for (Field ff : f) {
                ff.setAccessible(true);// 假设不为空。设置可见性，然后返回,这里必须有，不然报错
                String field_name = ff.getName();
                if (field_name.indexOf("field") > -1) {  //查看哪些字段含有field字样，这里是只需要含有field字样的字段，用来和工单配置中indexs一一对应
                    Long i = Long.valueOf(field_name.substring(5)); //获取含有field字眼字段后面的数字，好和工单配置中的indexs对应
                    for (RollOrderconfigdetail r : detail) {
                        Orderdetail o = new Orderdetail();
                        if (ff.get(rollDimensionorder) != null) {
                            String order_value = ff.get(rollDimensionorder).toString();
                            o.setOrder_measure(order_value);//详情工单填写值
                            if (i == r.getIndexs()) {
                                o.setOrder_text(r.getRollStandard().getField_name());//详情文字
                                //标准类型  标准1还是其他2
                                if (r.getRollStandard().getStandard_type() == 1) {  //标准字段才需要做判断
                                    //字段类型   区间1还是布尔0
                                    if (r.getRollStandard().getField_type().equals("1")) {   //区间
                                        Double max = r.getRollStandard().getField_max(); //上区间
                                        Double min = r.getRollStandard().getField_min();  //下区间
                                        if (max == 100001) {  //表示上限无限
                                            Double x = Double.valueOf(order_value);   //工单填写值
                                            Double y = Double.valueOf(min);   //标准判断值
                                            if (x >= y) {
                                                o.setOrder_result(1);//符合
                                            } else {
                                                o.setOrder_result(0);//不符合
                                            }
                                            o.setOrder_standard("≥" + min);//详情标准
                                        } else if (min == 100000) { //表示下限无限
                                            Double x = Double.valueOf(order_value);   //工单填写值21
                                            Double y = Double.valueOf(max);   //标准判断值409.71
                                            if (x <= y) {
                                                o.setOrder_result(1);//符合
                                            } else {
                                                o.setOrder_result(0);//不符合
                                            }
                                            o.setOrder_standard("≤" + max);//详情标准
                                        } else {  //表示在具体数值的区间，而不是含有上下无穷+∞这样的
                                            Double x = Double.valueOf(order_value);   //工单填写值
                                            Double y = Double.valueOf(max);   //标准判断值上限
                                            Double z = Double.valueOf(min);   //标准判断值下限
                                            if (x <= y) {//填写值小于等于最大值,接着判断填写值是否小于最小值
                                            	if(x >= z) {//填写值大于最小值
                                            		o.setOrder_result(1);//符合
                                            	}else {//填写值小于最小值，不合格
                                            		o.setOrder_result(0);//不符合
                                            	}
                                            }else {//填写值大于最大值，不合格
                                            	o.setOrder_result(0);//不符合
                                            }
                                            o.setOrder_standard(z + "<=" + x + "");//详情标准
                                        }
                                    } else if (r.getRollStandard().getField_type().equals("0")) {  //布尔
                                        o.setOrder_standard(r.getRollStandard().getField_value());//详情标准
                                    }
                                }
                                o.setOrder_unit(r.getRollStandard().getField_unit());//详情单位
                                order_list.add(o);
                            }
                        }
                    }
                }
            }
            return ResultData.ResultDataSuccess(order_list);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

	@Override
	public ResultData findDataRollDimensionorderByTime(String data) {
		try {
            RollDimensionorder rollDimensionorder = JSON.parseObject(data, RollDimensionorder.class);
            List<RollDimensionorder> list = rollDimensionorderDao.findDataRollDimensionorderByTime(rollDimensionorder.getRoll_no());
            return ResultData.ResultDataSuccessSelf("查询成功", list.get(0));
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
	}

}
