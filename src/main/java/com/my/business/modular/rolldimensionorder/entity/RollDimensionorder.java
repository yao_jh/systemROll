package com.my.business.modular.rolldimensionorder.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 新辊尺寸表面工单数据实体类
 *
 * @author 生成器生成
 * @date 2020-08-10 11:13:21
 */
public class RollDimensionorder extends BaseEntity {

	private Long indocno; // 主键
	private String ilinkno; // 关联工单配置主表编码
	private String roll_no; // 辊号
	private String production_line; // 产线(1-2250,2-1580)
	private Long production_line_id; //
	private Long factory_id;
	private String factory; // 生产厂家
	private Long material_id;
	private String material; // 材质
	private Long roll_typeid; // 轧辊类型id
	private String roll_type; // 轧辊类型
	private Long frame_noid; // 机架号id
	private String frame_no; // 机架号
	private String field1; // 字段
	private String field2; // 字段
	private String field3; // 字段
	private String field4; // 字段
	private String field5; // 字段
	private String field6; // 字段
	private String field7; // 字段
	private String field8; // 字段
	private String field9; // 字段
	private String field10; // 字段
	private String field11; // 字段
	private String field12; // 字段
	private String field13; // 字段
	private String field14; // 字段
	private String field15; // 字段
	private String field16; // 字段
	private String field17; // 字段
	private String field18; // 字段
	private String field19; // 字段
	private String field20; // 字段
	private String field21; // 字段
	private String snote; // 备注

	public Long getIndocno() {
		return this.indocno;
	}

	public void setIndocno(Long indocno) {
		this.indocno = indocno;
	}

	public String getIlinkno() {
		return ilinkno;
	}

	public void setIlinkno(String ilinkno) {
		this.ilinkno = ilinkno;
	}

	public String getRoll_no() {
		return this.roll_no;
	}

	public void setRoll_no(String roll_no) {
		this.roll_no = roll_no;
	}

	public String getFactory() {
		return this.factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}

	public String getMaterial() {
		return this.material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public Long getRoll_typeid() {
		return this.roll_typeid;
	}

	public void setRoll_typeid(Long roll_typeid) {
		this.roll_typeid = roll_typeid;
	}

	public String getRoll_type() {
		return this.roll_type;
	}

	public void setRoll_type(String roll_type) {
		this.roll_type = roll_type;
	}

	public String getField1() {
		return this.field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getField2() {
		return this.field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public String getField3() {
		return this.field3;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}

	public String getField4() {
		return this.field4;
	}

	public void setField4(String field4) {
		this.field4 = field4;
	}

	public String getField5() {
		return this.field5;
	}

	public void setField5(String field5) {
		this.field5 = field5;
	}

	public String getField6() {
		return this.field6;
	}

	public void setField6(String field6) {
		this.field6 = field6;
	}

	public String getField7() {
		return this.field7;
	}

	public void setField7(String field7) {
		this.field7 = field7;
	}

	public String getField8() {
		return this.field8;
	}

	public void setField8(String field8) {
		this.field8 = field8;
	}

	public String getField9() {
		return this.field9;
	}

	public void setField9(String field9) {
		this.field9 = field9;
	}

	public String getField10() {
		return this.field10;
	}

	public void setField10(String field10) {
		this.field10 = field10;
	}

	public String getField11() {
		return this.field11;
	}

	public void setField11(String field11) {
		this.field11 = field11;
	}

	public String getField12() {
		return this.field12;
	}

	public void setField12(String field12) {
		this.field12 = field12;
	}

	public String getField13() {
		return this.field13;
	}

	public void setField13(String field13) {
		this.field13 = field13;
	}

	public String getField14() {
		return this.field14;
	}

	public void setField14(String field14) {
		this.field14 = field14;
	}

	public String getField15() {
		return this.field15;
	}

	public void setField15(String field15) {
		this.field15 = field15;
	}

	public String getField16() {
		return this.field16;
	}

	public void setField16(String field16) {
		this.field16 = field16;
	}

	public String getField17() {
		return this.field17;
	}

	public void setField17(String field17) {
		this.field17 = field17;
	}

	public String getField18() {
		return this.field18;
	}

	public void setField18(String field18) {
		this.field18 = field18;
	}

	public String getField19() {
		return this.field19;
	}

	public void setField19(String field19) {
		this.field19 = field19;
	}

	public String getField20() {
		return this.field20;
	}

	public void setField20(String field20) {
		this.field20 = field20;
	}

	public String getField21() {
		return this.field21;
	}

	public void setField21(String field21) {
		this.field21 = field21;
	}

	public String getSnote() {
		return this.snote;
	}

	public void setSnote(String snote) {
		this.snote = snote;
	}

	public String getProduction_line() {
		return production_line;
	}

	public void setProduction_line(String production_line) {
		this.production_line = production_line;
	}

	public Long getProduction_line_id() {
		return production_line_id;
	}

	public void setProduction_line_id(Long production_line_id) {
		this.production_line_id = production_line_id;
	}

	public Long getFactory_id() {
		return factory_id;
	}

	public void setFactory_id(Long factory_id) {
		this.factory_id = factory_id;
	}

	public Long getMaterial_id() {
		return material_id;
	}

	public void setMaterial_id(Long material_id) {
		this.material_id = material_id;
	}

	public Long getFrame_noid() {
		return frame_noid;
	}

	public void setFrame_noid(Long frame_noid) {
		this.frame_noid = frame_noid;
	}

	public String getFrame_no() {
		return frame_no;
	}

	public void setFrame_no(String frame_no) {
		this.frame_no = frame_no;
	}
	
}