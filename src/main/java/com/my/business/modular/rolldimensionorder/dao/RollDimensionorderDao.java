package com.my.business.modular.rolldimensionorder.dao;

import com.my.business.modular.rolldimensionorder.entity.RollDimensionorder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 新辊尺寸表面工单数据dao接口
 *
 * @author 生成器生成
 * @date 2020-08-10 11:13:21
 */
@Mapper
public interface RollDimensionorderDao {

    /**
     * 添加记录
     *
     * @param rollDimensionorder 对象实体
     */
    void insertDataRollDimensionorder(RollDimensionorder rollDimensionorder);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollDimensionorderOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollDimensionorderMany(String value);

    /**
     * 修改记录
     *
     * @param rollDimensionorder 对象实体
     */
    void updateDataRollDimensionorder(RollDimensionorder rollDimensionorder);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollDimensionorder> findDataRollDimensionorderByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("roll_no") String roll_no, @Param("factory") String factory, @Param("material") String material, @Param("roll_typeid") String roll_typeid);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollDimensionorderByPageSize(@Param("roll_no") String roll_no, @Param("factory") String factory, @Param("material") String material, @Param("roll_typeid") String roll_typeid);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollDimensionorder findDataRollDimensionorderByIndocno(@Param("indocno") Long indocno);
    
    /***
     * 根据编码查询信息
     * @param roll_no 辊号
     * @return
     */
    RollDimensionorder findDataRollDimensionorderByNo(@Param("roll_no") String roll_no);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollDimensionorder> findDataRollDimensionorder();
    
    /**
     * 根据辊号查看最新的记录
     *
     * @return 对象数据集合
     */
    List<RollDimensionorder> findDataRollDimensionorderByTime(@Param("roll_no") String roll_no);

}
