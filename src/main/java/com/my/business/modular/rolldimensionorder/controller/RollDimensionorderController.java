package com.my.business.modular.rolldimensionorder.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rolldimensionorder.entity.JRollDimensionorder;
import com.my.business.modular.rolldimensionorder.entity.RollDimensionorder;
import com.my.business.modular.rolldimensionorder.service.RollDimensionorderService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 新辊尺寸表面工单数据控制器层
 *
 * @author 生成器生成
 * @date 2020-08-10 11:13:21
 */
@RestController
@RequestMapping("/rollDimensionorder")
public class RollDimensionorderController {

    @Autowired
    private RollDimensionorderService rollDimensionorderService;

    /**
     * 添加记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollDimensionorder(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollDimensionorderService.insertDataRollDimensionorder(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollDimensionorderOne(@RequestBody String data) {
        try {
            JRollDimensionorder jrollDimensionorder = JSON.parseObject(data, JRollDimensionorder.class);
            return rollDimensionorderService.deleteDataRollDimensionorderOne(jrollDimensionorder.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollDimensionorder jrollDimensionorder = JSON.parseObject(data, JRollDimensionorder.class);
            return rollDimensionorderService.deleteDataRollDimensionorderMany(jrollDimensionorder.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollDimensionorder(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollDimensionorderService.updateDataRollDimensionorder(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDimensionorderByPage(@RequestBody String data) {
        return rollDimensionorderService.findDataRollDimensionorderByPage(data);
    }

    /**
     * 查看工单详情
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/finddetail"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findOrderDetail(@RequestBody String data) {
        return rollDimensionorderService.findOrderAll(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDimensionorderByIndocno(@RequestBody String data) {
        return rollDimensionorderService.findDataRollDimensionorderByIndocno(data);
    }
    
    /**
     * 根据辊号查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByNew"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findByNo(@RequestBody String data) {
        return rollDimensionorderService.findDataRollDimensionorderByTime(data);
    }
    
    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByNo"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDimensionorderByNo(@RequestBody String data) {
        return rollDimensionorderService.findDataRollDimensionorderByNo(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollDimensionorder> findDataRollDimensionorder() {
        return rollDimensionorderService.findDataRollDimensionorder();
    }

}
