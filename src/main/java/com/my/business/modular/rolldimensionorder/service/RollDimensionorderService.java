package com.my.business.modular.rolldimensionorder.service;

import com.my.business.modular.rolldimensionorder.entity.RollDimensionorder;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 新辊尺寸表面工单数据接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-10 11:13:21
 */
public interface RollDimensionorderService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollDimensionorder(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollDimensionorderOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollDimensionorderMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollDimensionorder(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollDimensionorderByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollDimensionorderByIndocno(String data);
    
    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollDimensionorderByNo(String data);

    /**
     * 查看记录
     */
    List<RollDimensionorder> findDataRollDimensionorder();
    
    /**
     * 查看记录
     */
    ResultData findDataRollDimensionorderByTime(String data);

    /***
     * 查看工单详情
     * @return
     */
    ResultData findOrderAll(String data);
}
