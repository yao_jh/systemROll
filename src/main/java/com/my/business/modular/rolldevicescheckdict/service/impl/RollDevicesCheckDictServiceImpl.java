package com.my.business.modular.rolldevicescheckdict.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rolldevicescheck.entity.RollDevicescheck;
import com.my.business.modular.rolldevicescheck.service.RollDevicesCheckService;
import com.my.business.modular.rolldevicescheckdict.dao.RollDevicescheckdictDao;
import com.my.business.modular.rolldevicescheckdict.entity.JRollDevicescheckdict;
import com.my.business.modular.rolldevicescheckdict.entity.RollDevicescheckdict;
import com.my.business.modular.rolldevicescheckdict.service.RollDevicesCheckDictService;
import com.my.business.modular.rolldevicescheckpart.dao.RollDevicescheckpartDao;
import com.my.business.modular.rolldevicescheckpart.entity.RollDevicescheckpart;
import com.my.business.modular.rolldevicescheckpart.service.RollDevicesCheckPartService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 点检项目字典表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-07-08 15:22:13
 */
@Service
public class RollDevicesCheckDictServiceImpl implements RollDevicesCheckDictService {

    @Autowired
    private RollDevicescheckdictDao rollDevicescheckdictDao;

    @Autowired
    private RollDevicesCheckService rollDevicesCheckService;

    @Autowired
    private RollDevicesCheckPartService rollDevicesCheckPartService;

    @Autowired
    private RollDevicescheckpartDao rollDevicescheckpartDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollDevicescheckdict(String data, Long userId, String sname) {
        try {
            JRollDevicescheckdict jrollDevicescheckdict = JSON.parseObject(data, JRollDevicescheckdict.class);
            RollDevicescheckdict rollDevicescheckdict = jrollDevicescheckdict.getRollDevicescheckdict();
            CodiUtil.newRecord(userId, sname, rollDevicescheckdict);
            rollDevicescheckdictDao.insertDataRollDevicescheckdict(rollDevicescheckdict);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollDevicescheckdictOne(Long indocno) {
        try {
            rollDevicescheckdictDao.deleteDataRollDevicescheckdictOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollDevicescheckdictMany(String str_id) {
        try {
            String sql = "update roll_devicescheckdict set idel = 0 where indocno in(" + str_id + ")";
            rollDevicescheckdictDao.deleteDataRollDevicescheckdictMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData updateDataRollDevicescheckdict(String data, Long userId, String sname) {
        try {
            JRollDevicescheckdict jrollDevicescheckdict = JSON.parseObject(data, JRollDevicescheckdict.class);
            RollDevicescheckdict rollDevicescheckdict = jrollDevicescheckdict.getRollDevicescheckdict();
            CodiUtil.editRecord(userId, sname, rollDevicescheckdict);
            rollDevicescheckdictDao.updateDataRollDevicescheckdict(rollDevicescheckdict);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollDevicescheckdictByPage(String data) {
        try {
            JRollDevicescheckdict jrollDevicescheckdict = JSON.parseObject(data, JRollDevicescheckdict.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollDevicescheckdict.getPageIndex();
            Integer pageSize = jrollDevicescheckdict.getPageSize();
            pageIndex = (pageIndex-1)*pageSize;

            // 模糊查询： 设备名称 点检项目名称 点检周期
            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollDevicescheckdict.getCondition()) {
                jsonObject = JSON.parseObject(jrollDevicescheckdict.getCondition().toString());
            }

            // 为什么不做成联级的下拉框：个人感觉，检查项目太多，做成下拉框用户体验会很差
            String deviceName = null;
            List<Long> devicesIds = null;
            if (!StringUtils.isEmpty(jsonObject.get("device_name_condition"))) {
                deviceName = jsonObject.get("device_name_condition").toString();
                List<RollDevicescheck> rollDeviceschecks = rollDevicesCheckService.findDataRollDeviceCheckByName(deviceName);
                devicesIds = rollDeviceschecks.stream().map(RollDevicescheck::getIndocno).collect(Collectors.toList());
            }

            String devicePartName = null;
            if (!StringUtils.isEmpty(jsonObject.get("device_part_name_condition"))) {
                devicePartName = jsonObject.get("device_part_name_condition").toString();
            }
            // 设备id 和 设备点检部位的name 双重查询
            List<RollDevicescheckpart> rollDevicescheckparts = rollDevicesCheckPartService.findDataRollDevicescheckpartByDeviceIdAndDevicePartName(devicesIds, devicePartName);
            List<Long> devicePartIds = rollDevicescheckparts.stream().map(RollDevicescheckpart::getIndocno).collect(Collectors.toList());
            // TDOO: 添加去重

            String checkIntervalUnit = null;
            if (!StringUtils.isEmpty(jsonObject.get("check_interval_unit_condition"))) {
                checkIntervalUnit = jsonObject.get("check_interval_unit_condition").toString();
            }

            // 查询条件：点检工角色类型 电气0/机械1
            String checkRoleType = null;
            if (!StringUtils.isEmpty(jsonObject.get("check_role_type_condition"))) {
                checkRoleType = jsonObject.get("check_role_type_condition").toString();
            }

            List<RollDevicescheckdict> list = rollDevicescheckdictDao.findDataRollDevicescheckdictByPage(pageIndex, pageSize, devicePartIds, checkIntervalUnit, checkRoleType);
            // 其余字段重新装配
            for (RollDevicescheckdict rollDevicescheckdict : list) {
                Long checkPartId = rollDevicescheckdict.getCheck_part_id();
                RollDevicescheckpart rollDevicescheckpart = rollDevicescheckpartDao.findDataRollDevicescheckPartByIndocno(checkPartId);
                rollDevicescheckdict.setDevice_name(rollDevicescheckpart.getDevice_name());
                rollDevicescheckdict.setDevice_part_name(rollDevicescheckpart.getDevice_part_name());
            }

//			Integer count = list.size();// 从接口中获取的只是每页显示的页数 不能作为总页数
            Integer count = rollDevicescheckdictDao.findDataRollDevicescheckdictByPageSize(devicePartIds, checkIntervalUnit, checkRoleType);// 查看总数
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    @Override
    public List<RollDevicescheckdict> findDataRollDevicesCheckDictByCheckType(String[] checkTypes) {
        return rollDevicescheckdictDao.findDataRollDevicesCheckDictByCheckType(checkTypes);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data indocno
     */
    public ResultData findDataRollDevicescheckdictByIndocno(String data) {
        try {
            JRollDevicescheckdict jRollDevicescheckdict = JSON.parseObject(data, JRollDevicescheckdict.class);
            Long indocno = jRollDevicescheckdict.getIndocno();

            RollDevicescheckdict rollDevicescheckdict = rollDevicescheckdictDao.findDataRollDevicescheckdictByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollDevicescheckdict);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollDevicescheckdict> findDataRollDevicescheckdict() {
        List<RollDevicescheckdict> list = rollDevicescheckdictDao.findDataRollDevicescheckdict();
        return list;
    }
}
