package com.my.business.modular.rolldevicescheckdict.dao;

import com.my.business.modular.rolldevicescheckdict.entity.RollDevicescheckdict;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 点检项目字典表dao接口
 *
 * @author 生成器生成
 * @date 2020-07-08 15:22:12
 */
@Mapper
public interface RollDevicescheckdictDao {

    /**
     * 添加记录
     *
     * @param rollDevicescheckdict 对象实体
     */
    void insertDataRollDevicescheckdict(RollDevicescheckdict rollDevicescheckdict);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollDevicescheckdictOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollDevicescheckdictMany(String value);

    /**
     * 修改记录
     *
     * @param rollDevicescheckdict 对象实体
     */
    void updateDataRollDevicescheckdict(RollDevicescheckdict rollDevicescheckdict);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollDevicescheckdict> findDataRollDevicescheckdictByPage(@Param("pageIndex") Integer pageIndex,
                                                                  @Param("pageSize") Integer pageSize,
                                                                  @Param("device_part_ids") List<Long> devicePartIds,
                                                                  @Param("check_interval_unit") String checkIntervalUnit,
                                                                  @Param("check_role_type") String checkRoleType);

    /**
     * 根据checkType的数组 查看对应的检查项目集合
     *
     * @param checkTypes 点检项数组
     */
    List<RollDevicescheckdict> findDataRollDevicesCheckDictByCheckType(@Param("checkTypes") String[] checkTypes);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合devicePartIds, checkIntervalUnit
     */
    Integer findDataRollDevicescheckdictByPageSize(@Param("device_part_ids") List<Long> devicePartIds,
                                                   @Param("check_interval_unit") String checkIntervalUnit,
                                                   @Param("check_role_type") String checkRoleType);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollDevicescheckdict findDataRollDevicescheckdictByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollDevicescheckdict> findDataRollDevicescheckdict();

}
