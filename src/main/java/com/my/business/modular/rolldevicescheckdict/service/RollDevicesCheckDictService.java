package com.my.business.modular.rolldevicescheckdict.service;

import com.my.business.modular.rolldevicescheckdict.entity.RollDevicescheckdict;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 点检项目字典表接口服务类
 *
 * @author 生成器生成
 * @date 2020-07-08 15:22:13
 */
public interface RollDevicesCheckDictService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollDevicescheckdict(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollDevicescheckdictOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollDevicescheckdictMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollDevicescheckdict(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollDevicescheckdictByPage(String data);

    /**
     * 根据checkType的数组 查看对应的检查项目集合
     *
     * @param checkTypes 点检项数组
     */
    List<RollDevicescheckdict> findDataRollDevicesCheckDictByCheckType(String[] checkTypes);

    /**
     * 查看一条数据信息
     *
     * @param id 分页参数字符串
     */
    ResultData findDataRollDevicescheckdictByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollDevicescheckdict> findDataRollDevicescheckdict();
}
