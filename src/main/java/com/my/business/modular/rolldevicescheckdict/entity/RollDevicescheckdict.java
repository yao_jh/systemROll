package com.my.business.modular.rolldevicescheckdict.entity;

import com.my.business.sys.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 点检项目字典表实体类
 *
 * @author 生成器生成
 * @date 2020-07-08 15:22:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RollDevicescheckdict extends BaseEntity {

    private Long indocno;  //主键
    private Long check_part_id;  //点检部位id
    private Long check_interval;  //点检周期
    private String check_interval_unit;  //点检周期单位（D,W,M,Y）
    private String check_content;  //点检内容
    private Long check_state;  //点检状态（0停机，1运行）
    private String check_standard;  //点检标准
    private String snote;  //备注
    private Integer check_role_type;//点检角色名称 0电气 1机械

    // 前端添加
    private String device_name;// 设备名称
    private String device_part_name;// 点检部位

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getCheck_part_id() {
        return check_part_id;
    }

    public void setCheck_part_id(Long check_part_id) {
        this.check_part_id = check_part_id;
    }

    public Long getCheck_interval() {
        return check_interval;
    }

    public void setCheck_interval(Long check_interval) {
        this.check_interval = check_interval;
    }

    public String getCheck_interval_unit() {
        return check_interval_unit;
    }

    public void setCheck_interval_unit(String check_interval_unit) {
        this.check_interval_unit = check_interval_unit;
    }

    public String getCheck_content() {
        return check_content;
    }

    public void setCheck_content(String check_content) {
        this.check_content = check_content;
    }

    public Long getCheck_state() {
        return check_state;
    }

    public void setCheck_state(Long check_state) {
        this.check_state = check_state;
    }

    public String getCheck_standard() {
        return check_standard;
    }

    public void setCheck_standard(String check_standard) {
        this.check_standard = check_standard;
    }

    public String getSnote() {
        return snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public Integer getCheck_role_type() {
        return check_role_type;
    }

    public void setCheck_role_type(Integer check_role_type) {
        this.check_role_type = check_role_type;
    }

    public String getDevice_name() {
        return device_name;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }

    public String getDevice_part_name() {
        return device_part_name;
    }

    public void setDevice_part_name(String device_part_name) {
        this.device_part_name = device_part_name;
    }
}