package com.my.business.modular.rolldevicescheckdict.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rolldevicescheckdict.entity.JRollDevicescheckdict;
import com.my.business.modular.rolldevicescheckdict.entity.RollDevicescheckdict;
import com.my.business.modular.rolldevicescheckdict.service.RollDevicesCheckDictService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 点检项目字典表控制器层
 *
 * @author 生成器生成
 * @date 2020-07-08 15:22:13
 */
@RestController
@RequestMapping("/rollDevicescheckdict")
public class RollDevicescheckdictController {

    @Autowired
    private RollDevicesCheckDictService rollDevicescheckdictService;

    /**
     * 添加记录
     *
     * @param loginToken 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollDevicescheckdict(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollDevicescheckdictService.insertDataRollDevicescheckdict(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollDevicescheckdictOne(@RequestBody String data) {
        try {
            JRollDevicescheckdict jrollDevicescheckdict = JSON.parseObject(data, JRollDevicescheckdict.class);
            return rollDevicescheckdictService.deleteDataRollDevicescheckdictOne(jrollDevicescheckdict.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollDevicescheckdict jrollDevicescheckdict = JSON.parseObject(data, JRollDevicescheckdict.class);
            return rollDevicescheckdictService.deleteDataRollDevicescheckdictMany(jrollDevicescheckdict.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollDevicescheckdict(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollDevicescheckdictService.updateDataRollDevicescheckdict(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDevicescheckdictByPage(@RequestBody String data) {
        return rollDevicescheckdictService.findDataRollDevicescheckdictByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDevicescheckdictByIndocno(@RequestBody String data) {
        return rollDevicescheckdictService.findDataRollDevicescheckdictByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollDevicescheckdict> findDataRollDevicescheckdict() {
        return rollDevicescheckdictService.findDataRollDevicescheckdict();
    }
}
