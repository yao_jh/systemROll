package com.my.business.modular.rackpairingrule.service;

import com.my.business.modular.rackpairingrule.entity.RackPairingRule;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

public interface RackPairingRuleService {
    List<RackPairingRule> findDataRackPairingRule();

    ResultData findDataRackPairingRuleByIndocno(String data);

    ResultData findDataRackPairingRuleByPage(String data);

    ResultData updateDataRackPairingRule(String data, Long userId, String returnLm);

    ResultData deleteDataRackPairingRulefoMany(String str_indocno);

    ResultData deleteDataRackPairingRulefoOne(Long indocno);

    ResultData insertDataRackPairingRule(String data, Long userId, String returnLm);

    ResultData findDataRackPairingRuleByCondiction(String data);
}
