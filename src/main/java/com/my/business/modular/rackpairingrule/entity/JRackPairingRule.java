package com.my.business.modular.rackpairingrule.entity;

import com.my.business.modular.rckurumainfo.entity.RcKurumaInfo;
import com.my.business.sys.common.entity.JCommon;

public class JRackPairingRule extends JCommon {
    private Integer pageIndex; // 第几页
    private Integer pageSize; // 每页多少数据
    private Object condition; // 查询条件
    private RackPairingRule rackPairingRule;   //对应模块的实体类
    private Long indocno;

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Object getCondition() {
        return condition;
    }

    public void setCondition(Object condition) {
        this.condition = condition;
    }

    public RackPairingRule getRackPairingRule() {
        return rackPairingRule;
    }

    public void setRackPairingRule(RackPairingRule rackPairingRule) {
        this.rackPairingRule = rackPairingRule;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getStr_indocno() {
        return str_indocno;
    }

    public void setStr_indocno(String str_indocno) {
        this.str_indocno = str_indocno;
    }

    private String str_indocno;
}
