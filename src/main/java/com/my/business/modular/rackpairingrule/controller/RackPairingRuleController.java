package com.my.business.modular.rackpairingrule.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rackpairingrule.entity.JRackPairingRule;
import com.my.business.modular.rackpairingrule.entity.RackPairingRule;
import com.my.business.modular.rackpairingrule.service.RackPairingRuleService;
import com.my.business.modular.rckurumainfo.entity.JRcKurumaInfo;
import com.my.business.modular.rckurumainfo.entity.RcKurumaInfo;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 机架和辊径差值关联规则
 */
@RestController
@RequestMapping("/rackPairingRule")
public class RackPairingRuleController {
    @Autowired
    private RackPairingRuleService rackPairingRuleService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRackPairingRule(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rackPairingRuleService.insertDataRackPairingRule(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRackPairingRulefoOne(@RequestBody String data) {
        try {
            JRackPairingRule jRackPairingRule = JSON.parseObject(data, JRackPairingRule.class);
            return rackPairingRuleService.deleteDataRackPairingRulefoOne(jRackPairingRule.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRackPairingRule jRackPairingRule = JSON.parseObject(data, JRackPairingRule.class);
            return rackPairingRuleService.deleteDataRackPairingRulefoMany(jRackPairingRule.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRackPairingRule(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rackPairingRuleService.updateDataRackPairingRule(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRackPairingRuleByPage(@RequestBody String data) {
        return rackPairingRuleService.findDataRackPairingRuleByPage(data);
    }
    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRackPairingRuleByIndocno(@RequestBody String data) {
        return rackPairingRuleService.findDataRackPairingRuleByIndocno(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByCondiction"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRackPairingRuleByCondiction(@RequestBody String data) {
        return rackPairingRuleService.findDataRackPairingRuleByCondiction(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RackPairingRule> findDataRackPairingRule() {
        return rackPairingRuleService.findDataRackPairingRule();
    }


}
