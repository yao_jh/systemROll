package com.my.business.modular.rackpairingrule.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rackpairingrule.dao.RackPairingRuleDao;
import com.my.business.modular.rackpairingrule.entity.JRackPairingRule;
import com.my.business.modular.rackpairingrule.entity.RackPairingRule;
import com.my.business.modular.rackpairingrule.service.RackPairingRuleService;
import com.my.business.modular.rckurumainfo.entity.JRcKurumaInfo;
import com.my.business.modular.rckurumainfo.entity.RcKurumaInfo;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;

@Service
public class RackPairingRuleServiceImpl implements RackPairingRuleService {
    @Autowired
    private RackPairingRuleDao rackPairingRuleDao;

    @Override
    public List<RackPairingRule> findDataRackPairingRule() {
        List<RackPairingRule> list = rackPairingRuleDao.findDataRackPairingRule();
        return list;
    }

    @Override
    public ResultData findDataRackPairingRuleByIndocno(String data) {
        try {
            JRackPairingRule jRackPairingRule = JSON.parseObject(data, JRackPairingRule.class);
            Long indocno = jRackPairingRule.getIndocno();

            RackPairingRule rackPairingRule = rackPairingRuleDao.findDataRackPairingRuleByIndocno(indocno);
            return ResultData.ResultDataSuccess(rackPairingRule);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    @Override
    public ResultData findDataRackPairingRuleByPage(String data) {
        try {
            JRackPairingRule jRackPairingRule = JSON.parseObject(data, JRackPairingRule.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jRackPairingRule.getPageIndex();
            Integer pageSize = jRackPairingRule.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jRackPairingRule.getCondition()) {
                jsonObject = JSON.parseObject(jRackPairingRule.getCondition().toString());
            }
            String frame_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_no"))) {
                frame_no = jsonObject.get("frame_no").toString();
            }
            String frame_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
                frame_noid = jsonObject.get("frame_noid").toString();
            }
            List<RackPairingRule> list = rackPairingRuleDao.findDataRackPairingRuleByPage((pageIndex - 1) * pageSize, pageSize,frame_no,frame_noid);
            Integer count = rackPairingRuleDao.findDataRackPairingRuleByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    @Override
    public ResultData updateDataRackPairingRule(String data, Long userId, String sname) {
        try {
            JRackPairingRule jRackPairingRule = JSON.parseObject(data, JRackPairingRule.class);
            RackPairingRule rackPairingRule = jRackPairingRule.getRackPairingRule();
            CodiUtil.newRecord(userId, sname, rackPairingRule);
            rackPairingRuleDao.updateDataRackPairingRule(rackPairingRule);
            //如果 此条生效 那么 其他机架此条都设置为失效
            if(rackPairingRule.getIfeffective()==0){
                rackPairingRuleDao.updateRackPairingRuleByframeNo(rackPairingRule.getFrame_no(),rackPairingRule.getIndocno());
            }
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    @Override
    public ResultData deleteDataRackPairingRulefoMany(String str_indocno) {
        try {
            String sql = "delete from rack_pairing_rule where indocno in(" + str_indocno + ")";
            rackPairingRuleDao.deleteDataRackPairingRulefoMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    @Override
    public ResultData deleteDataRackPairingRulefoOne(Long indocno) {
        try {
            rackPairingRuleDao.deleteDataRackPairingRulefoOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    @Override
    public ResultData insertDataRackPairingRule(String data, Long userId, String sname) {
        try {
            JRackPairingRule jRackPairingRule = JSON.parseObject(data, JRackPairingRule.class);
            RackPairingRule rackPairingRule = jRackPairingRule.getRackPairingRule();
            CodiUtil.newRecord(userId, sname, rackPairingRule);
            rackPairingRuleDao.insertDataRackPairingRule(rackPairingRule);
            //如果 此条生效 那么 其他机架此条都设置为失效
            if(rackPairingRule.getIfeffective()==0){
                rackPairingRuleDao.updateRackPairingRuleByframeNo(rackPairingRule.getFrame_no(),rackPairingRule.getIndocno());
            }
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    @Override
    public ResultData findDataRackPairingRuleByCondiction(String data) {
        try {
            BigDecimal result = BigDecimal.ZERO;
            JRackPairingRule jRackPairingRule = JSON.parseObject(data, JRackPairingRule.class);
            JSONObject jsonObject = null;
            if (null != jRackPairingRule.getCondition()) {
                jsonObject = JSON.parseObject(jRackPairingRule.getCondition().toString());
            }

            String frame_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_no"))) {
                frame_no = jsonObject.get("frame_no").toString();
            }
            String frame_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
                frame_noid = jsonObject.get("frame_noid").toString();
            }

            RackPairingRule rackPairingRule = rackPairingRuleDao.findDataRackPairingRuleByCondiction(frame_no,frame_noid);
            return ResultData.ResultDataSuccess(rackPairingRule);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }
}
