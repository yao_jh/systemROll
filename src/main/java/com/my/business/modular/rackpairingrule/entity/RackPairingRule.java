package com.my.business.modular.rackpairingrule.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

public class RackPairingRule extends BaseEntity {

    private Long indocno;  //主键
    private BigDecimal rollerradius;//棍缝差
    private BigDecimal minrollerradius;//最小
    private String createrackpairingruletime;//创建时间
    private String frame_no;//机架名称
    private Integer frame_noid;//机架v1值
    private Integer ifeffective;//是否有效

    public BigDecimal getMinrollerradius() {
        return minrollerradius;
    }

    public void setMinrollerradius(BigDecimal minrollerradius) {
        this.minrollerradius = minrollerradius;
    }

    public String getCreaterackpairingruletime() {
        return createrackpairingruletime;
    }

    public void setCreaterackpairingruletime(String createrackpairingruletime) {
        this.createrackpairingruletime = createrackpairingruletime;
    }
    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public BigDecimal getRollerradius() {
        return rollerradius;
    }

    public void setRollerradius(BigDecimal rollerradius) {
        this.rollerradius = rollerradius;
    }



    public String getFrame_no() {
        return frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public Integer getFrame_noid() {
        return frame_noid;
    }

    public void setFrame_noid(Integer frame_noid) {
        this.frame_noid = frame_noid;
    }

    public Integer getIfeffective() {
        return ifeffective;
    }

    public void setIfeffective(Integer ifeffective) {
        this.ifeffective = ifeffective;
    }


}
