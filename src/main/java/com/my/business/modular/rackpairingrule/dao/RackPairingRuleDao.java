package com.my.business.modular.rackpairingrule.dao;

import com.my.business.modular.rackpairingrule.entity.RackPairingRule;
import com.my.business.modular.rckurumainfo.entity.RcKurumaInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RackPairingRuleDao {


     void updateRackPairingRuleByframeNo(@Param("frame_no")String frame_no,@Param("indocno") Long indocno) ;


     void deleteDataRackPairingRulefoOne(@Param("indocno")Long indocno) ;


     void deleteDataRackPairingRulefoMany(@Param("value") String sql) ;


     void updateDataRackPairingRule(RackPairingRule rackPairingRule) ;


     Integer findDataRackPairingRuleByPageSize() ;


     List<RackPairingRule> findDataRackPairingRuleByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("frame_no")String frame_no, @Param("frame_noid")String frame_noid) ;

     RackPairingRule findDataRackPairingRuleByIndocno(@Param("indocno") Long indocno) ;

    List<RackPairingRule> findDataRackPairingRule();

     void insertDataRackPairingRule(RackPairingRule rackPairingRule);

    RackPairingRule findDataRackPairingRuleByCondiction(@Param("frame_no")String frame_no, @Param("frame_noid")String frame_noid);
}
