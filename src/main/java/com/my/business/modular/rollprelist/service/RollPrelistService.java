package com.my.business.modular.rollprelist.service;

import com.my.business.modular.rollprelist.entity.RollPrelist;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.dict.entity.DpdEntity;

import java.util.List;

/**
 * 备辊列表接口服务类
 *
 * @author 生成器生成
 * @date 2020-09-15 16:26:29
 */
public interface RollPrelistService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollPrelist(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollPrelistOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollPrelistMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollPrelist(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPrelistByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPrelistByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollPrelist> findDataRollPrelist();

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPrelistNew(String data);

    /**
     * 轧辊计划列表
     *
     * @return
     */
    List<DpdEntity> findpo();

    /**
     * 向卡夫卡发请求
     *
     * @param data 计划号
     */
    void getpo(String data);
}
