package com.my.business.modular.rollprelist.entity;

import java.util.Date;

/**
 * 轧辊计划表实体类
 *
 * @author 生成器生成
 * @date 2020-11-22 15:09:03
 */
public class Pdiplan {

    private Date curtime;  //时间
    private String prodct_plan_no;  //轧制计划号
    private String l3_coil_id;  //钢卷号
    private String final_proc;  //末道次工序
    private String usaged;  //用途代码
    private String grade_name;  //钢种牌号
    private String fxhaim;  //目标厚度
    private String fxwaim;  //目标厚度

    public Date getCurtime() {
        return this.curtime;
    }

    public void setCurtime(Date curtime) {
        this.curtime = curtime;
    }

    public String getProdct_plan_no() {
        return this.prodct_plan_no;
    }

    public void setProdct_plan_no(String prodct_plan_no) {
        this.prodct_plan_no = prodct_plan_no;
    }

    public String getL3_coil_id() {
        return this.l3_coil_id;
    }

    public void setL3_coil_id(String l3_coil_id) {
        this.l3_coil_id = l3_coil_id;
    }

    public String getFinal_proc() {
        return this.final_proc;
    }

    public void setFinal_proc(String final_proc) {
        this.final_proc = final_proc;
    }

    public String getUsaged() {
        return usaged;
    }

    public void setUsaged(String usaged) {
        this.usaged = usaged;
    }

    public String getGrade_name() {
        return this.grade_name;
    }

    public void setGrade_name(String grade_name) {
        this.grade_name = grade_name;
    }

    public String getFxhaim() {
        return this.fxhaim;
    }

    public void setFxhaim(String fxhaim) {
        this.fxhaim = fxhaim;
    }

    public String getFxwaim() {
        return this.fxwaim;
    }

    public void setFxwaim(String fxwaim) {
        this.fxwaim = fxwaim;
    }


}