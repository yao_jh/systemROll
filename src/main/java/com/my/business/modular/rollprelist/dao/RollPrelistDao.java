package com.my.business.modular.rollprelist.dao;

import com.my.business.modular.rollprelist.entity.Pdiplan;
import com.my.business.modular.rollprelist.entity.RollPrelist;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 备辊列表dao接口
 *
 * @author 生成器生成
 * @date 2020-09-15 16:26:29
 */
@Mapper
public interface RollPrelistDao {

    /**
     * 添加记录
     *
     * @param rollPrelist 对象实体
     */
    void insertDataRollPrelist(RollPrelist rollPrelist);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollPrelistOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollPrelistMany(String value);

    /**
     * 修改记录
     *
     * @param rollPrelist 对象实体
     */
    void updateDataRollPrelist(RollPrelist rollPrelist);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollPrelist> findDataRollPrelistByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("frame_noid") Long frame_noid);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollPrelistByPageSize(@Param("frame_noid") Long frame_noid);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollPrelist findDataRollPrelistByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollPrelist> findDataRollPrelist();

    List<RollPrelist> findDataRollPrelistByPage2(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("frame_noid") Long frame_noid);

    Integer findDataRollPrelistByPageSize2(@Param("frame_noid") Long frame_noid);

    /**
     * 不分页查看查看记录
     *
     * @return 对象数据集合
     */
    List<RollPrelist> findDataRollPrelistNoPage(@Param("frame_noid") Long frame_noid);

    /**
     * 查找3天内轧辊计划列表
     *
     * @return
     */
    List<Pdiplan> findpo();
}
