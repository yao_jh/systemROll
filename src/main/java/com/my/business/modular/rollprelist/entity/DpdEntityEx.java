package com.my.business.modular.rollprelist.entity;

/**
 * 携带额外信息的下拉框
 */
public class DpdEntityEx {
    private String key;
    private String value;
    private String message;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
