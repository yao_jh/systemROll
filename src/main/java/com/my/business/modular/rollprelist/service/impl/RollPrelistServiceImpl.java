package com.my.business.modular.rollprelist.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.modular.rollpaired.dao.RollPairedDao;
import com.my.business.modular.rollpaired.entity.RollPaired;
import com.my.business.modular.rollprelist.dao.RollPrelistDao;
import com.my.business.modular.rollprelist.entity.DpdEntityEx;
import com.my.business.modular.rollprelist.entity.JRollPrelist;
import com.my.business.modular.rollprelist.entity.Pdiplan;
import com.my.business.modular.rollprelist.entity.RollPrelist;
import com.my.business.modular.rollprelist.service.RollPrelistService;
import com.my.business.modular.rollprelistr.entity.DpdEntityEx2;
import com.my.business.schedule.AllKafka;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.dict.entity.DpdEntity;
import com.my.business.util.CodiUtil;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * 备辊列表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-15 16:26:29
 */
@CommonsLog
@Service
public class RollPrelistServiceImpl implements RollPrelistService {

    private final static Log logger = LogFactory.getLog(RollPrelistServiceImpl.class);
    @Autowired
    private RollPrelistDao rollPrelistDao;
    @Autowired
    private RollPairedDao rollPairedDao;
    @Autowired
    private RollInformationDao rollInformationDao;
    @Autowired
    private AllKafka allKafka;
    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollPrelist(String data, Long userId, String sname) {
        try {
            JRollPrelist jrollPrelist = JSON.parseObject(data, JRollPrelist.class);
            RollPrelist rollPrelist = jrollPrelist.getRollPrelist();
            CodiUtil.newRecord(userId, sname, rollPrelist);
            rollPrelistDao.insertDataRollPrelist(rollPrelist);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollPrelistOne(Long indocno) {
        try {
            rollPrelistDao.deleteDataRollPrelistOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollPrelistMany(String str_id) {
        try {
            String sql = "delete roll_prelist where indocno in(" + str_id + ")";
            rollPrelistDao.deleteDataRollPrelistMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollPrelist(String data, Long userId, String sname) {
        try {
            JRollPrelist jrollPrelist = JSON.parseObject(data, JRollPrelist.class);
            RollPrelist rollPrelist = jrollPrelist.getRollPrelist();
            CodiUtil.editRecord(userId, sname, rollPrelist);
            rollPrelistDao.updateDataRollPrelist(rollPrelist);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPrelistByPage(String data) {
        try {
            JRollPrelist jrollPrelist = JSON.parseObject(data, JRollPrelist.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollPrelist.getPageIndex();
            Integer pageSize = jrollPrelist.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollPrelist.getCondition()) {
                jsonObject = JSON.parseObject(jrollPrelist.getCondition().toString());
            }

            //机架号id
            Long frame_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
                frame_noid = Long.valueOf(jsonObject.get("frame_noid").toString());
            }

            Long mut = null;
            if (!StringUtils.isEmpty(jsonObject.get("mut"))) {
                mut = Long.valueOf(jsonObject.get("mut").toString());
            }
            List<RollPrelist> list = new ArrayList<>();
            Integer count = 0;
            if (mut == 0L) {
                list = rollPrelistDao.findDataRollPrelistByPage((pageIndex - 1) * pageSize, pageSize, frame_noid);
                count = rollPrelistDao.findDataRollPrelistByPageSize(frame_noid);
            } else {
                list = rollPrelistDao.findDataRollPrelistByPage2((pageIndex - 1) * pageSize, pageSize, frame_noid);
                count = rollPrelistDao.findDataRollPrelistByPageSize2(frame_noid);
            }
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPrelistByIndocno(String data) {
        try {
            JRollPrelist jrollPrelist = JSON.parseObject(data, JRollPrelist.class);
            Long indocno = jrollPrelist.getIndocno();

            RollPrelist rollPrelist = rollPrelistDao.findDataRollPrelistByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollPrelist);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollPrelist> findDataRollPrelist() {
        List<RollPrelist> list = rollPrelistDao.findDataRollPrelist();
        return list;
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPrelistNew(String data) {
        try {
            List<Map<String,String>> list_1 = findlist(1L);
            List<Map<String,String>> list_2 = findlist(2L);
            List<Map<String,String>> list_3 = findlist(3L);
            List<Map<String,String>> list_4 = findlist(4L);
            List<Map<String,String>> list_5 = findlist(5L);
            List<Map<String,String>> list_6 = findlist(6L);
            List<Map<String,String>> list_7 = findlist(7L);
            List<Map<String,String>> list_8 = findlist(15L);

            List<List<Map<String,String>>> listas = new ArrayList<>();
            listas.add(list_1);

            listas.add(list_2);
            listas.add(list_3);
            listas.add(list_4);
            listas.add(list_5);
            listas.add(list_6);
            listas.add(list_7);
            listas.add(list_8);

            return ResultData.ResultDataSuccess(listas);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    private List<Map<String,String>> findlist(Long frame_noid){
        List<Map<String,String>> list_1 = new ArrayList<>();
        List<RollPaired> list1 = rollPairedDao.findDataRollPairedB(null, null, 1L, frame_noid);
        if (list1.size() != 0 && !StringUtils.isEmpty(list1)) {
            for (int i = 0; i < list1.size(); i++) {
                String value1 = findRollRevolve(list1.get(i).getRoll_no_up());
                String value2 = findRollRevolve(list1.get(i).getRoll_no_down());
                if (!StringUtils.isEmpty(value1) && !StringUtils.isEmpty(value2)){
                    String rn = value1 + "/" + value2;
                    Map<String,String> map = new HashMap<>();
                    map.put("value",rn);
                    String rnName = "上辊："+value1 + "  " + "下辊：" + value2;
                    map.put("valueName",rnName);
                    list_1.add(map);
                }
            }
        }
        return list_1;
    }

    /**
     * 根据轧辊组合号查询各自的状态
     *
     * @param roll_no 轧辊组合号
     * @return 状态
     */
    private String findRollRevolve(String roll_no) {
        String roll_nos = null;
        if (!StringUtils.isEmpty(roll_no)) {
            RollInformation Information1 = rollInformationDao.findDataRollInformationByRollNo(roll_no);
            if (!StringUtils.isEmpty(Information1) && !StringUtils.isEmpty(Information1.getRoll_revolve()) && !StringUtils.isEmpty(Information1.getRoll_state())) {
                if (Information1.getRoll_revolve() == 4L && Information1.getRoll_state() == 3L) {
                    roll_nos = roll_no;
                }else {
                    roll_nos = null;
                }
            } else {
                roll_nos = null;
            }
        }
        return roll_nos;
    }

    @Override
    public List<DpdEntity> findpo() {
        List<DpdEntity> list = new ArrayList<>();
        List<Pdiplan> pdiplans = rollPrelistDao.findpo();
        if (!StringUtils.isEmpty(pdiplans)) {
            for (int i = 0; i < pdiplans.size(); i++) {
                DpdEntity dpdEntity = new DpdEntity();
                dpdEntity.setKey(String.valueOf(i));
                dpdEntity.setValue(pdiplans.get(i).getProdct_plan_no());
                list.add(dpdEntity);
            }
        }
        return list;
    }

    @Override
    public void getpo(String data) {
        Pdiplan pdiplan = JSON.parseObject(data, Pdiplan.class);
        JSONObject request = new JSONObject();
        request.put("groupId", "GR10");
        request.put("message", "roll_changing_car_load");
        request.put("prodct_plan_no", pdiplan.getProdct_plan_no());
        String result = request.toJSONString();
        if (!StringUtils.isEmpty(pdiplan.getProdct_plan_no())) {
            allKafka.BRSSsend(result);
            logger.debug("发送给BRSS的消息为：" + result);
        }
        logger.debug("不发送给BRSS的消息为：" + result);
    }
}
