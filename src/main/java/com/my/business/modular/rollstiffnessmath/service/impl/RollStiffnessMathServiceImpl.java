package com.my.business.modular.rollstiffnessmath.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollstiffness.entity.JEchartsA;
import com.my.business.modular.rollstiffnessmath.dao.RollStiffnessMathDao;
import com.my.business.modular.rollstiffnessmath.entity.JRollStiffnessMath;
import com.my.business.modular.rollstiffnessmath.entity.RollStiffnessMath;
import com.my.business.modular.rollstiffnessmath.service.RollStiffnessMathService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * 轧辊多变异分析信号表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-10 20:20:19
 */
@Service
public class RollStiffnessMathServiceImpl implements RollStiffnessMathService {

    @Autowired
    private RollStiffnessMathDao rollStiffnessMathDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollStiffnessMath(String data, Long userId, String sname) {
        try {
            JRollStiffnessMath jrollStiffnessMath = JSON.parseObject(data, JRollStiffnessMath.class);
            RollStiffnessMath rollStiffnessMath = jrollStiffnessMath.getRollStiffnessMath();
            CodiUtil.newRecord(userId, sname, rollStiffnessMath);
            rollStiffnessMathDao.insertDataRollStiffnessMath(rollStiffnessMath);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollStiffnessMathOne(Long indocno) {
        try {
            rollStiffnessMathDao.deleteDataRollStiffnessMathOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollStiffnessMathMany(String str_id) {
        try {
            String sql = "delete roll_stiffness_math where indocno in(" + str_id + ")";
            rollStiffnessMathDao.deleteDataRollStiffnessMathMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollStiffnessMath(String data, Long userId, String sname) {
        try {
            JRollStiffnessMath jrollStiffnessMath = JSON.parseObject(data, JRollStiffnessMath.class);
            RollStiffnessMath rollStiffnessMath = jrollStiffnessMath.getRollStiffnessMath();
            CodiUtil.editRecord(userId, sname, rollStiffnessMath);
            rollStiffnessMathDao.updateDataRollStiffnessMath(rollStiffnessMath);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollStiffnessMathByPage(String data) {
        try {
            JRollStiffnessMath jrollStiffnessMath = JSON.parseObject(data, JRollStiffnessMath.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollStiffnessMath.getPageIndex();
            Integer pageSize = jrollStiffnessMath.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollStiffnessMath.getCondition()) {
                jsonObject = JSON.parseObject(jrollStiffnessMath.getCondition().toString());
            }

            //产线id
            Long production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
            }

            String recordtime = null;
            if (!StringUtils.isEmpty(jsonObject.get("recordtime"))) {
                recordtime = jsonObject.get("recordtime").toString();
            }

            String demarcate = null;
            if (!StringUtils.isEmpty(jsonObject.get("demarcate"))) {
                demarcate = jsonObject.get("demarcate").toString();
            }

            List<RollStiffnessMath> list = rollStiffnessMathDao.findDataRollStiffnessMathByPage((pageIndex - 1)*pageSize, pageSize, recordtime, production_line_id, demarcate);
            Integer count = rollStiffnessMathDao.findDataRollStiffnessMathByPageSize(recordtime,production_line_id,demarcate);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollStiffnessMathByIndocno(String data) {
        try {
            JRollStiffnessMath jrollStiffnessMath = JSON.parseObject(data, JRollStiffnessMath.class);
            Long indocno = jrollStiffnessMath.getIndocno();

            RollStiffnessMath rollStiffnessMath = rollStiffnessMathDao.findDataRollStiffnessMathByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollStiffnessMath);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据查询条件查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollStiffnessMathBytime(String data) {
        try {
            JRollStiffnessMath jrollStiffnessMath = JSON.parseObject(data, JRollStiffnessMath.class);
            JSONObject jsonObject = null;

            if (null != jrollStiffnessMath.getCondition()) {
                jsonObject = JSON.parseObject(jrollStiffnessMath.getCondition().toString());
            }


            String recordtime = null;
            if (!StringUtils.isEmpty(jsonObject.get("recordtime"))) {
                recordtime = jsonObject.get("recordtime").toString();
            }

            Long production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
            }

            String frame_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_no"))) {
                frame_no = jsonObject.get("frame_no").toString();
            }

            RollStiffnessMath rollStiffnessMath = rollStiffnessMathDao.findDataRollStiffnessMathBytime(recordtime, production_line_id, frame_no);
            return ResultData.ResultDataSuccess(rollStiffnessMath);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollStiffnessMath> findDataRollStiffnessMath() {
        List<RollStiffnessMath> list = rollStiffnessMathDao.findDataRollStiffnessMath();
        return list;
    }
    
    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollStiffnessMath> findDataRollStiffnessMathTest() {
    	try {
    		List<RollStiffnessMath> list = rollStiffnessMathDao.findDataRollStiffnessMathTest();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 图形化——时间数值查询接口
     */
    public ResultData findEchartsC(String data) {
        try {
            JRollStiffnessMath jrollStiffnessMath = JSON.parseObject(data, JRollStiffnessMath.class);
            JSONObject jsonObject = null;

            if (null != jrollStiffnessMath.getCondition()) {
                jsonObject = JSON.parseObject(jrollStiffnessMath.getCondition().toString());
            }

            //开始时间
            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            //结束时间
            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }
            //机架号
            String frame_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_no"))) {
                frame_no = jsonObject.get("frame_no").toString();
            }
            //产线id
            Long production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
            }

            JEchartsA jEchartsA = new JEchartsA();
            List<String> catalog = new ArrayList<>();
            List<JEchartsA.xy> xyList = new ArrayList<>();
            //查出条件范围内时间点最大值和最小值
            String minTime = rollStiffnessMathDao.findminTime(dbegin, dend, production_line_id);
            String maxTime = rollStiffnessMathDao.findmaxTime(dbegin, dend, production_line_id);

            //获得X轴
            catalog.add(minTime);
            catalog.add(maxTime);

            //查出条件范围内时间点、数值
            List<RollStiffnessMath> EchartsAList = rollStiffnessMathDao.findEchartsA(dbegin, dend, frame_no, production_line_id);

            for (int i = 0; i < 4; i++) {
                JEchartsA.xy xy = new JEchartsA.xy();
                List<String> xst = new ArrayList<>();
                List<Double> yst = new ArrayList<>();
                if (i == 0) {
                    for (RollStiffnessMath entity : EchartsAList) {
                        xst.add(entity.getRecordtime());
                        yst.add(entity.getRetention());
                    }
                    //集合放入各自的实体类
                    xy.setXst(xst);
                    xy.setYst(yst);
                    xy.setName("刚度保持率(%)");
                    //实体类变成集合
                    xyList.add(xy);
                } else if (i == 1) {
                    for (RollStiffnessMath entity : EchartsAList) {
                        xst.add(entity.getRecordtime());
                        yst.add(entity.getLcosstiffness());
                    }
                    //集合放入各自的实体类
                    xy.setXst(xst);
                    xy.setYst(yst);
                    xy.setName("OS侧刚度");
                    //实体类变成集合
                    xyList.add(xy);
                } else if (i == 2) {
                    for (RollStiffnessMath entity : EchartsAList) {
                        xst.add(entity.getRecordtime());
                        yst.add(entity.getLcdsstiffness());
                    }
                    //集合放入各自的实体类
                    xy.setXst(xst);
                    xy.setYst(yst);
                    xy.setName("DS侧刚度");
                    //实体类变成集合
                    xyList.add(xy);
                } else {
                    for (RollStiffnessMath entity : EchartsAList) {
                        xst.add(entity.getRecordtime());
                        yst.add(Math.abs(entity.getBothposition_stiffness()));
                    }
                    //集合放入各自的实体类
                    xy.setXst(xst);
                    xy.setYst(yst);
                    xy.setName("两侧刚度偏差(KN/mm)");
                    //实体类变成集合
                    xyList.add(xy);
                }
            }
            jEchartsA.setCatalog(catalog);
            jEchartsA.setSeries(xyList);
            //返回json格式
            return ResultData.ResultDataSuccess(jEchartsA);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 处理文件上传
     */
    public String excelImport(MultipartFile file, HttpServletRequest request) {
        String contentType = file.getContentType();
        String fileName = file.getOriginalFilename();
        if (file.isEmpty()) {
            return "文件为空！";
        }
        try {
            //根据路径获取这个操作excel的实例
            HSSFWorkbook wb = new HSSFWorkbook(file.getInputStream());            //根据页面index 获取sheet页
            HSSFSheet sheet = wb.getSheetAt(0);
            //实体类集合
            List<RollStiffnessMath> rollStiffnessMathList = new ArrayList<>();
            HSSFRow row = null;
            //循环sesheet页中数据从第二行开始，第一行是标题
            for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
                //获取每一行数据
                row = sheet.getRow(i);
                RollStiffnessMath data = new RollStiffnessMath();
//                data.setId(Integer.valueOf((int) row.getCell(0).getNumericCellValue()));
//                data.setName(row.getCell(1).getStringCellValue());
//                SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
//                data.setCreateDate(df.parse(df.format(HSSFDateUtil.getJavaDate(row.getCell(2).getNumericCellValue()))));
//                data.setAge(Integer.valueOf((int) row.getCell(3).getNumericCellValue()));
                rollStiffnessMathList.add(data);
            }

//            //循环展示导入的数据，实际应用中应该校验并存入数据库
//            for (ImportData imdata : importDatas) {
//                SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
//                System.out.println("ID:" + imdata.getId() + " name:" + imdata.getName() + " createDate:" + df.format(imdata.getCreateDate()) + " age:" + imdata.getAge());
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "导入成功!";
    }
}
