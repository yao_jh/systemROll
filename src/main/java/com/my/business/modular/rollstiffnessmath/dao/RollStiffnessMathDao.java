package com.my.business.modular.rollstiffnessmath.dao;

import com.my.business.modular.rollstiffnessmath.entity.RollStiffnessMath;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轧辊多变异分析信号表dao接口
 *
 * @author 生成器生成
 * @date 2020-09-10 20:20:19
 */
@Mapper
public interface RollStiffnessMathDao {

    /**
     * 添加记录
     *
     * @param rollStiffnessMath 对象实体
     */
    void insertDataRollStiffnessMath(RollStiffnessMath rollStiffnessMath);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollStiffnessMathOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollStiffnessMathMany(String value);

    /**
     * 修改记录
     *
     * @param rollStiffnessMath 对象实体
     */
    void updateDataRollStiffnessMath(RollStiffnessMath rollStiffnessMath);

    /**
     * 修改记录
     *
     * @param rollStiffnessMath 对象实体
     */
    void updateDataRollStiffnessMathA(RollStiffnessMath rollStiffnessMath);


    /**
     * 修改记录
     *
     * @param rollStiffnessMath 对象实体
     */
    void updateDataRollStiffnessMathB(RollStiffnessMath rollStiffnessMath);

    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollStiffnessMath> findDataRollStiffnessMathByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("recordtime") String recordtime, @Param("production_line_id") Long production_line_id, @Param("demarcate") String demarcate);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollStiffnessMathByPageSize(@Param("recordtime") String recordtime, @Param("production_line_id") Long production_line_id, @Param("demarcate") String demarcate);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollStiffnessMath findDataRollStiffnessMathByIndocno(@Param("indocno") Long indocno);

    RollStiffnessMath findDataRollStiffnessMathByParam(@Param("recordtime") String recordtime, @Param("production_line_id") Long production_line_id, @Param("frame_no") String frame_no);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollStiffnessMath> findDataRollStiffnessMath();
    
    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollStiffnessMath> findDataRollStiffnessMathTest();
    

    /**
     * 查找最新的日期时间
     *
     * @return string
     */
    String findTime();

    /**
     * 查找重复数量
     *
     * @return
     */
    Integer findDataHave(@Param("recordtime") String recordtime, @Param("production_line_id") Long production_line_id, @Param("frame_no") String frame_no);

    /**
     * 查出条件范围内时间点最小值
     *
     * @param dbegin 开始时间
     * @param dend   结束时间
     */
    String findminTime(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("production_line_id") Long production_line_id);

    /**
     * 查出条件范围内时间点最大值
     *
     * @param dbegin 开始时间
     * @param dend   结束时间
     */
    String findmaxTime(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("production_line_id") Long production_line_id);

    /**
     * 查出条件范围内各项信息
     *
     * @param dbegin             开始时间
     * @param dend               结束时间
     * @param frame_no           机架号
     * @param production_line_id 产线id
     */
    List<RollStiffnessMath> findEchartsA(@Param("dbegin") String dbegin, @Param("dend") String dend, @Param("frame_no") String frame_no, @Param("production_line_id") Long production_line_id);

    /**
     * 查看详情
     *
     * @param recordtime         记录时间
     * @param production_line_id 产线id
     * @param frame_no           机架号
     * @return 实体类
     */
    RollStiffnessMath findDataRollStiffnessMathBytime(@Param("recordtime") String recordtime, @Param("production_line_id") Long production_line_id, @Param("frame_no") String frame_no);

    /**
     * 修改记录
     *
     * @param rollStiffnessMath 对象实体
     */
    void updateDataRollStiffnessMathByCon(RollStiffnessMath rollStiffnessMath, @Param("recordtime") String recordtime, @Param("production_line_id") Long production_line_id, @Param("frame_no") String frame_no);

    /**
     * 根据机架号，查找最新数据
     * @param production_line_id 产线
     * @param frame_no 机架号
     * @return 数据
     */
    RollStiffnessMath findDataNew(@Param("production_line_id") Long production_line_id, @Param("frame_no") String frame_no);

    /**
     * 根据机架号，查找最新数据
     * @param production_line_id 产线
     * @param frame_no 机架号
     * @return 数据
     */
    List<RollStiffnessMath> findDataNewCount(@Param("production_line_id") Long production_line_id, @Param("frame_no") String frame_no);
}
