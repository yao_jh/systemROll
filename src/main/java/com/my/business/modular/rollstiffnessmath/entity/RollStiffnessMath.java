package com.my.business.modular.rollstiffnessmath.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轧辊多变异分析信号表实体类
 *
 * @author 生成器生成
 * @date 2020-09-12 15:59:06
 */
public class RollStiffnessMath extends BaseEntity {

    private Long indocno;  //主键
    private String frame_no;  //机架号
    private Double retention;  //刚度保持率(%)
    private Double leveling;  //调平值(mm)
    private Double bothsides_stiffness;  //两侧刚度偏差(KN/mm)
    private Double bothposition_stiffness;  //两侧位置偏差(mm)
    private Double osposition;  //OS位置偏差(mm)
    private Double dsposition;  //DS位置偏差(mm)
    private Double rolling_force;  //轧制力偏差(KN)
    private Double osrollgap;  //OS辊缝偏差(mm)
    private Double dsrollgap;  //DS辊缝偏差(mm)
    private String recordtime;  //标定时间
    private Long production_line_id;  //产线id
    private String production_line;  //产线
    private String gapzeroontime;  //标定开始时间
    private String gapzeroofftime;  //标定结束时间
    private Double lcosstiffness;  //LC——OS侧刚度
    private Double lcdsstiffness;  //LC——DS侧刚度
    private Double ptosstiffness;  //PT——OS侧刚度
    private Double ptdsstiffness;  //PT——DS侧刚度
    private Double osenposition;  //OS入口位置
    private Double osexposition;  //OS出口位置
    private Double dsenposition;  //DS入口位置
    private Double dsexposition;  //DS出口位置
    private Double lcosforce;  //LC——OS侧轧制力
    private Double lcdsforce;  //LC——DS侧轧制力
    private Double ptosforce;  //PT——OS侧轧制力
    private Double ptdsforce;  //PT——DS侧轧制力
    private Double itotal;  //总分
    private Double retention_score;  //刚度保持率得分
    private Double leveling_score;  //调平值得分
    private Double bothsides_stiffness_score;  //两侧刚度偏差得分
    private Double bothposition_stiffness_score;  //两侧位置偏差得分
    private Double osposition_score;  //OS位置偏差得分
    private Double dsposition_score;  //DS位置偏差得分
    private Double rolling_force_score;  //轧制力偏差得分
    private Double osrollgap_score;  //OS辊缝偏差得分
    private Double dsrollgap_score;  //DS辊缝偏差得分
    private String demarcate;  //发送时间
    private Double os_stiffness_in;  //OS入口刚度
    private Double os_stiffness_out;  //OS出口刚度
    private Double ds_stiffness_in;  //DS入口刚度
    private Double ds_stiffness_out;  //ds出口刚度
    private Double upstepwedge;  //上阶梯位实际值
    private Double dnstepwedge;  //下阶梯位实际值

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getFrame_no() {
        return this.frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public Double getRetention() {
        return this.retention;
    }

    public void setRetention(Double retention) {
        this.retention = retention;
    }

    public Double getLeveling() {
        return this.leveling;
    }

    public void setLeveling(Double leveling) {
        this.leveling = leveling;
    }

    public Double getBothsides_stiffness() {
        return this.bothsides_stiffness;
    }

    public void setBothsides_stiffness(Double bothsides_stiffness) {
        this.bothsides_stiffness = bothsides_stiffness;
    }

    public Double getBothposition_stiffness() {
        return this.bothposition_stiffness;
    }

    public void setBothposition_stiffness(Double bothposition_stiffness) {
        this.bothposition_stiffness = bothposition_stiffness;
    }

    public Double getOsposition() {
        return this.osposition;
    }

    public void setOsposition(Double osposition) {
        this.osposition = osposition;
    }

    public Double getDsposition() {
        return this.dsposition;
    }

    public void setDsposition(Double dsposition) {
        this.dsposition = dsposition;
    }

    public Double getRolling_force() {
        return this.rolling_force;
    }

    public void setRolling_force(Double rolling_force) {
        this.rolling_force = rolling_force;
    }

    public Double getOsrollgap() {
        return this.osrollgap;
    }

    public void setOsrollgap(Double osrollgap) {
        this.osrollgap = osrollgap;
    }

    public Double getDsrollgap() {
        return this.dsrollgap;
    }

    public void setDsrollgap(Double dsrollgap) {
        this.dsrollgap = dsrollgap;
    }

    public String getRecordtime() {
        return this.recordtime;
    }

    public void setRecordtime(String recordtime) {
        this.recordtime = recordtime;
    }

    public Long getProduction_line_id() {
        return this.production_line_id;
    }

    public void setProduction_line_id(Long production_line_id) {
        this.production_line_id = production_line_id;
    }

    public String getProduction_line() {
        return this.production_line;
    }

    public void setProduction_line(String production_line) {
        this.production_line = production_line;
    }

    public Double getOs_stiffness_in() {
        return os_stiffness_in;
    }

    public void setOs_stiffness_in(Double os_stiffness_in) {
        this.os_stiffness_in = os_stiffness_in;
    }

    public Double getOs_stiffness_out() {
        return os_stiffness_out;
    }

    public void setOs_stiffness_out(Double os_stiffness_out) {
        this.os_stiffness_out = os_stiffness_out;
    }

    public Double getDs_stiffness_in() {
        return ds_stiffness_in;
    }

    public void setDs_stiffness_in(Double ds_stiffness_in) {
        this.ds_stiffness_in = ds_stiffness_in;
    }

    public Double getDs_stiffness_out() {
        return ds_stiffness_out;
    }

    public void setDs_stiffness_out(Double ds_stiffness_out) {
        this.ds_stiffness_out = ds_stiffness_out;
    }


    public Double getItotal() {
        return itotal;
    }

    public void setItotal(Double itotal) {
        this.itotal = itotal;
    }

    public Double getRetention_score() {
        return retention_score;
    }

    public void setRetention_score(Double retention_score) {
        this.retention_score = retention_score;
    }

    public Double getLeveling_score() {
        return leveling_score;
    }

    public void setLeveling_score(Double leveling_score) {
        this.leveling_score = leveling_score;
    }

    public Double getBothsides_stiffness_score() {
        return bothsides_stiffness_score;
    }

    public void setBothsides_stiffness_score(Double bothsides_stiffness_score) {
        this.bothsides_stiffness_score = bothsides_stiffness_score;
    }

    public Double getBothposition_stiffness_score() {
        return bothposition_stiffness_score;
    }

    public void setBothposition_stiffness_score(Double bothposition_stiffness_score) {
        this.bothposition_stiffness_score = bothposition_stiffness_score;
    }

    public Double getOsposition_score() {
        return osposition_score;
    }

    public void setOsposition_score(Double osposition_score) {
        this.osposition_score = osposition_score;
    }

    public Double getDsposition_score() {
        return dsposition_score;
    }

    public void setDsposition_score(Double dsposition_score) {
        this.dsposition_score = dsposition_score;
    }

    public Double getRolling_force_score() {
        return rolling_force_score;
    }

    public void setRolling_force_score(Double rolling_force_score) {
        this.rolling_force_score = rolling_force_score;
    }

    public Double getOsrollgap_score() {
        return osrollgap_score;
    }

    public void setOsrollgap_score(Double osrollgap_score) {
        this.osrollgap_score = osrollgap_score;
    }

    public Double getDsrollgap_score() {
        return dsrollgap_score;
    }

    public void setDsrollgap_score(Double dsrollgap_score) {
        this.dsrollgap_score = dsrollgap_score;
    }

    public String getDemarcate() {
        return demarcate;
    }

    public void setDemarcate(String demarcate) {
        this.demarcate = demarcate;
    }

    public String getGapzeroontime() {
        return gapzeroontime;
    }

    public void setGapzeroontime(String gapzeroontime) {
        this.gapzeroontime = gapzeroontime;
    }

    public String getGapzeroofftime() {
        return gapzeroofftime;
    }

    public void setGapzeroofftime(String gapzeroofftime) {
        this.gapzeroofftime = gapzeroofftime;
    }

    public Double getLcosstiffness() {
        return lcosstiffness;
    }

    public void setLcosstiffness(Double lcosstiffness) {
        this.lcosstiffness = lcosstiffness;
    }

    public Double getLcdsstiffness() {
        return lcdsstiffness;
    }

    public void setLcdsstiffness(Double lcdsstiffness) {
        this.lcdsstiffness = lcdsstiffness;
    }

    public Double getPtosstiffness() {
        return ptosstiffness;
    }

    public void setPtosstiffness(Double ptosstiffness) {
        this.ptosstiffness = ptosstiffness;
    }

    public Double getPtdsstiffness() {
        return ptdsstiffness;
    }

    public void setPtdsstiffness(Double ptdsstiffness) {
        this.ptdsstiffness = ptdsstiffness;
    }

    public Double getOsenposition() {
        return osenposition;
    }

    public void setOsenposition(Double osenposition) {
        this.osenposition = osenposition;
    }

    public Double getOsexposition() {
        return osexposition;
    }

    public void setOsexposition(Double osexposition) {
        this.osexposition = osexposition;
    }

    public Double getDsenposition() {
        return dsenposition;
    }

    public void setDsenposition(Double dsenposition) {
        this.dsenposition = dsenposition;
    }

    public Double getDsexposition() {
        return dsexposition;
    }

    public void setDsexposition(Double dsexposition) {
        this.dsexposition = dsexposition;
    }

    public Double getLcosforce() {
        return lcosforce;
    }

    public void setLcosforce(Double lcosforce) {
        this.lcosforce = lcosforce;
    }

    public Double getLcdsforce() {
        return lcdsforce;
    }

    public void setLcdsforce(Double lcdsforce) {
        this.lcdsforce = lcdsforce;
    }

    public Double getPtosforce() {
        return ptosforce;
    }

    public void setPtosforce(Double ptosforce) {
        this.ptosforce = ptosforce;
    }

    public Double getPtdsforce() {
        return ptdsforce;
    }

    public void setPtdsforce(Double ptdsforce) {
        this.ptdsforce = ptdsforce;
    }

    public Double getUpstepwedge() {
        return upstepwedge;
    }

    public void setUpstepwedge(Double upstepwedge) {
        this.upstepwedge = upstepwedge;
    }

    public Double getDnstepwedge() {
        return dnstepwedge;
    }

    public void setDnstepwedge(Double dnstepwedge) {
        this.dnstepwedge = dnstepwedge;
    }
}