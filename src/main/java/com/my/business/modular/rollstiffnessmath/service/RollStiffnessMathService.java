package com.my.business.modular.rollstiffnessmath.service;

import com.my.business.modular.rollstiffnessmath.entity.RollStiffnessMath;
import com.my.business.sys.common.entity.ResultData;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 轧辊多变异分析信号表接口服务类
 *
 * @author 生成器生成
 * @date 2020-09-10 20:20:19
 */
public interface RollStiffnessMathService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollStiffnessMath(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollStiffnessMathOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollStiffnessMathMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollStiffnessMath(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollStiffnessMathByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollStiffnessMathByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollStiffnessMath> findDataRollStiffnessMath();
    
    /**
     * 查看记录action测试用
     */
    List<RollStiffnessMath> findDataRollStiffnessMathTest();

    /**
     * 图形化——时间数值查询记录
     *
     * @param data 分页参数字符串
     */
    ResultData findEchartsC(String data);

    /**
     * 查看详情
     *
     * @param data
     * @return 实体类
     */
    ResultData findDataRollStiffnessMathBytime(String data);

    /**
     * 处理文件上传
     */
    String excelImport(MultipartFile file, HttpServletRequest request);
}
