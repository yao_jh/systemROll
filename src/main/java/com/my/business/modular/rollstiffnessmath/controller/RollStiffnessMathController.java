package com.my.business.modular.rollstiffnessmath.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollstiffnessmath.entity.JRollStiffnessMath;
import com.my.business.modular.rollstiffnessmath.entity.RollStiffnessMath;
import com.my.business.modular.rollstiffnessmath.service.RollStiffnessMathService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * 轧辊多变异分析信号表控制器层
 *
 * @author 生成器生成
 * @date 2020-09-10 20:20:19
 */
@RestController
@RequestMapping("/rollStiffnessMath")
public class RollStiffnessMathController {

    @Autowired
    private RollStiffnessMathService rollStiffnessMathService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollStiffnessMath(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollStiffnessMathService.insertDataRollStiffnessMath(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollStiffnessMathOne(@RequestBody String data) {
        try {
            JRollStiffnessMath jrollStiffnessMath = JSON.parseObject(data, JRollStiffnessMath.class);
            return rollStiffnessMathService.deleteDataRollStiffnessMathOne(jrollStiffnessMath.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollStiffnessMath jrollStiffnessMath = JSON.parseObject(data, JRollStiffnessMath.class);
            return rollStiffnessMathService.deleteDataRollStiffnessMathMany(jrollStiffnessMath.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollStiffnessMath(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollStiffnessMathService.updateDataRollStiffnessMath(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollStiffnessMathByPage(@RequestBody String data) {
        return rollStiffnessMathService.findDataRollStiffnessMathByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollStiffnessMathByIndocno(@RequestBody String data) {
        return rollStiffnessMathService.findDataRollStiffnessMathByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollStiffnessMath> findDataRollStiffnessMath() {
        return rollStiffnessMathService.findDataRollStiffnessMath();
    }

    /**
     * 图形化——时间指标查询接口
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findEchartsC"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findEchartsC(@RequestBody String data) {
        return rollStiffnessMathService.findEchartsC(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findBytime"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollStiffnessMathBytime(@RequestBody String data) {
        return rollStiffnessMathService.findDataRollStiffnessMathBytime(data);
    }

    /**
     * 处理文件上传
     */
    @CrossOrigin
    @RequestMapping(value = "/excelImport", method = RequestMethod.POST)
    @ResponseBody//返回json数据
    public String uploadImg(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        return rollStiffnessMathService.excelImport(file, request);
    }
}

