package com.my.business.modular.rollcooling.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollcooling.dao.RollCoolingDao;
import com.my.business.modular.rollcooling.entity.JRollCooling;
import com.my.business.modular.rollcooling.entity.RollCooling;
import com.my.business.modular.rollcooling.service.RollCoolingService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 轧辊冷却管理接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-17 10:46:42
 */
@Service
public class RollCoolingServiceImpl implements RollCoolingService {

    @Autowired
    private RollCoolingDao rollCoolingDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollCooling(String data, Long userId, String sname) {
        try {
            JRollCooling jrollCooling = JSON.parseObject(data, JRollCooling.class);
            RollCooling rollCooling = jrollCooling.getRollCooling();
            CodiUtil.newRecord(userId, sname, rollCooling);
            rollCoolingDao.insertDataRollCooling(rollCooling);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollCoolingOne(Long indocno) {
        try {
            rollCoolingDao.deleteDataRollCoolingOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollCoolingMany(String str_id) {
        try {
            String sql = "delete roll_cooling where indocno in(" + str_id + ")";
            rollCoolingDao.deleteDataRollCoolingMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataRollCooling(String data, Long userId, String sname) {
        try {
            JRollCooling jrollCooling = JSON.parseObject(data, JRollCooling.class);
            RollCooling rollCooling = jrollCooling.getRollCooling();
            CodiUtil.editRecord(userId, sname, rollCooling);
            rollCoolingDao.updateDataRollCooling(rollCooling);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollCoolingByPage(String data) {
        try {
            JRollCooling jrollCooling = JSON.parseObject(data, JRollCooling.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollCooling.getPageIndex();
            Integer pageSize = jrollCooling.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollCooling.getCondition()) {
                jsonObject = JSON.parseObject(jrollCooling.getCondition().toString());
            }

            String roll_no = null;
            String offline_time = null;  //下线时间
            String cool_starttime = null;  //冷却开始时间
            String cool_endtime = null;  //冷却结束时间
            String ifinish = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }
            if (!StringUtils.isEmpty(jsonObject.get("offline_time"))) {
            	offline_time = jsonObject.get("offline_time").toString();
            }
            if (!StringUtils.isEmpty(jsonObject.get("cool_starttime"))) {
            	cool_starttime = jsonObject.get("cool_starttime").toString();
            }
            if (!StringUtils.isEmpty(jsonObject.get("cool_endtime"))) {
            	cool_endtime = jsonObject.get("cool_endtime").toString();
            }
            if (!StringUtils.isEmpty(jsonObject.get("ifinish"))) {
            	ifinish = jsonObject.get("ifinish").toString();
            }

            List<RollCooling> list = rollCoolingDao.findDataRollCoolingByPage((pageIndex - 1) * pageSize, pageSize, ifinish,roll_no,offline_time,cool_starttime,cool_endtime);
            Integer count = rollCoolingDao.findDataRollCoolingByPageSize(ifinish,roll_no,offline_time,cool_starttime,cool_endtime);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollCoolingByIndocno(String data) {
        try {
            JRollCooling jrollCooling = JSON.parseObject(data, JRollCooling.class);
            Long indocno = jrollCooling.getIndocno();

            RollCooling rollCooling = rollCoolingDao.findDataRollCoolingByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollCooling);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollCooling> findDataRollCooling() {
        List<RollCooling> list = rollCoolingDao.findDataRollCooling();
        return list;
    }

}
