package com.my.business.modular.rollcooling.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轧辊冷却管理实体类
 *
 * @author 生成器生成
 * @date 2020-08-17 10:46:41
 */
public class RollCooling extends BaseEntity {

    private Long indocno;  //主键
    private String production_line;  //产线(1-2250,2-1580)
	private Long production_line_id;  //
    private String roll_no;  //辊号
    private Long factory_id;  //厂商id
    private String factory;  //生产厂家
    private Long material_id;  //材质id
    private String material;  //材质
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private Long frame_noid;  //机架号id
	private String frame_no;  //机架号
    private String offline_time;  //下线时间
    private Long cool_type;  //冷却方式
    private String cool_starttime;  //冷却开始时间
    private String cool_endtime;  //冷却结束时间
    private String operat_user;  //操作人姓名
    private Long operat_userid;  //操作人id
    private Long ifinish;  //是否冷却完成(0 未完成  1已完成)
    private String snote;  //备注

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getFactory() {
        return this.factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public String getMaterial() {
        return this.material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public Long getRoll_typeid() {
        return this.roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public String getOffline_time() {
        return this.offline_time;
    }

    public void setOffline_time(String offline_time) {
        this.offline_time = offline_time;
    }

    public Long getCool_type() {
        return this.cool_type;
    }

    public void setCool_type(Long cool_type) {
        this.cool_type = cool_type;
    }

    public String getCool_starttime() {
        return this.cool_starttime;
    }

    public void setCool_starttime(String cool_starttime) {
        this.cool_starttime = cool_starttime;
    }

    public String getCool_endtime() {
        return this.cool_endtime;
    }

    public void setCool_endtime(String cool_endtime) {
        this.cool_endtime = cool_endtime;
    }

    public String getOperat_user() {
        return this.operat_user;
    }

    public void setOperat_user(String operat_user) {
        this.operat_user = operat_user;
    }

    public Long getOperat_userid() {
        return this.operat_userid;
    }

    public void setOperat_userid(Long operat_userid) {
        this.operat_userid = operat_userid;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

	public Long getIfinish() {
		return ifinish;
	}

	public void setIfinish(Long ifinish) {
		this.ifinish = ifinish;
	}

	public String getProduction_line() {
		return production_line;
	}

	public void setProduction_line(String production_line) {
		this.production_line = production_line;
	}

	public Long getProduction_line_id() {
		return production_line_id;
	}

	public void setProduction_line_id(Long production_line_id) {
		this.production_line_id = production_line_id;
	}

	public Long getFactory_id() {
		return factory_id;
	}

	public void setFactory_id(Long factory_id) {
		this.factory_id = factory_id;
	}

	public Long getMaterial_id() {
		return material_id;
	}

	public void setMaterial_id(Long material_id) {
		this.material_id = material_id;
	}

	public Long getFrame_noid() {
		return frame_noid;
	}

	public void setFrame_noid(Long frame_noid) {
		this.frame_noid = frame_noid;
	}

	public String getFrame_no() {
		return frame_no;
	}

	public void setFrame_no(String frame_no) {
		this.frame_no = frame_no;
	}
    
}