package com.my.business.modular.rollcooling.service;

import com.my.business.modular.rollcooling.entity.RollCooling;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 轧辊冷却管理接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-17 10:46:42
 */
public interface RollCoolingService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollCooling(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollCoolingOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollCoolingMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollCooling(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollCoolingByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollCoolingByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollCooling> findDataRollCooling();
}
