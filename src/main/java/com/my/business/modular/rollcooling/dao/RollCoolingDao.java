package com.my.business.modular.rollcooling.dao;

import com.my.business.modular.rollcooling.entity.RollCooling;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轧辊冷却管理dao接口
 *
 * @author 生成器生成
 * @date 2020-08-17 10:46:42
 */
@Mapper
public interface RollCoolingDao {

    /**
     * 添加记录
     *
     * @param rollCooling 对象实体
     */
    void insertDataRollCooling(RollCooling rollCooling);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollCoolingOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollCoolingMany(String value);

    /**
     * 修改记录
     *
     * @param rollCooling 对象实体
     */
    void updateDataRollCooling(RollCooling rollCooling);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollCooling> findDataRollCoolingByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("ifinish") String ifinish,@Param("roll_no") String roll_no, @Param("offline_time") String offline_time, @Param("cool_starttime") String cool_starttime, @Param("cool_endtime") String cool_endtime);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollCoolingByPageSize(@Param("ifinish") String ifinish,@Param("roll_no") String roll_no, @Param("offline_time") String offline_time, @Param("cool_starttime") String cool_starttime, @Param("cool_endtime") String cool_endtime);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollCooling findDataRollCoolingByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollCooling> findDataRollCooling();

}
