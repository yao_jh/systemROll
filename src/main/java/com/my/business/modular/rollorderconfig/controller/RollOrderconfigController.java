package com.my.business.modular.rollorderconfig.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollorderconfig.entity.JRollOrderconfig;
import com.my.business.modular.rollorderconfig.entity.RollOrderconfig;
import com.my.business.modular.rollorderconfig.service.RollOrderconfigService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 工单配置控制器层
 *
 * @author 生成器生成
 * @date 2020-08-08 15:59:25
 */
@RestController
@RequestMapping("/rollOrderconfig")
public class RollOrderconfigController {

    @Autowired
    private RollOrderconfigService rollOrderconfigService;

    /**
     * 添加记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollOrderconfig(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollOrderconfigService.insertDataRollOrderconfig(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollOrderconfigOne(@RequestBody String data) {
        try {
            JRollOrderconfig jrollOrderconfig = JSON.parseObject(data, JRollOrderconfig.class);
            return rollOrderconfigService.deleteDataRollOrderconfigOne(jrollOrderconfig.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollOrderconfig jrollOrderconfig = JSON.parseObject(data, JRollOrderconfig.class);
            return rollOrderconfigService.deleteDataRollOrderconfigMany(jrollOrderconfig.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollOrderconfig(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollOrderconfigService.updateDataRollOrderconfig(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollOrderconfigByPage(@RequestBody String data) {
        return rollOrderconfigService.findDataRollOrderconfigByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollOrderconfigByIndocno(@RequestBody String data) {
        return rollOrderconfigService.findDataRollOrderconfigByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollOrderconfig> findDataRollOrderconfig() {
        return rollOrderconfigService.findDataRollOrderconfig();
    }
    
    
    /**
     * 同步数据
     */
    @CrossOrigin
    @RequestMapping(value = {"/tbdata"}, method = RequestMethod.GET)
    @ResponseBody
    public ResultData tbdata() {
        return rollOrderconfigService.tbdata();
    }

}
