package com.my.business.modular.rollorderconfig.dao;

import com.my.business.modular.rollorderconfig.entity.RollOrderconfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 工单配置dao接口
 *
 * @author 生成器生成
 * @date 2020-08-08 15:59:24
 */
@Mapper
public interface RollOrderconfigDao {

    /**
     * 添加记录
     *
     * @param rollOrderconfig 对象实体
     */
    void insertDataRollOrderconfig(RollOrderconfig rollOrderconfig);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollOrderconfigOne(@Param("indocno") Long indocno);

    /**
     * 根据编码删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollOrderconfigBySno(@Param("snoindocno") String sno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollOrderconfigMany(String value);

    /**
     * 修改记录
     *
     * @param rollOrderconfig 对象实体
     */
    void updateDataRollOrderconfig(RollOrderconfig rollOrderconfig);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollOrderconfig> findDataRollOrderconfigByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("order_no") String order_no, @Param("order_name") String order_name, @Param("roll_typeid") String roll_typeid);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollOrderconfigByPageSize(@Param("order_no") String order_no, @Param("order_name") String order_name, @Param("roll_typeid") String roll_typeid);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollOrderconfig findDataRollOrderconfigByIndocno(@Param("indocno") Long indocno);
    
    /***
     * 根据编码查询信息
     * @param indocno 用户id
     * @return
     */
    RollOrderconfig findDataRollOrderconfigByNo(@Param("order_no") String order_no);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollOrderconfig> findDataRollOrderconfig();

    /**
     * 根据条件查询目标工单——周期提醒
     * @param production_line_id 产线id
     * @param roll_typeid 轧辊类型id
     * @param frame_noid 机架号id
     * @param itype 部件类型id
     * @param install_location_id 安装位置id
     * @param up_location_id 上机位置id
     * @return 配置好的工单信息
     */
    List<RollOrderconfig> findWithCycle(@Param("production_line_id") Long production_line_id, @Param("roll_typeid") Long roll_typeid, @Param("frame_noid") Long frame_noid, @Param("itype") Long itype, @Param("install_location_id") Long install_location_id, @Param("up_location_id") Long up_location_id);


    List<RollOrderconfig> findtb();

}
