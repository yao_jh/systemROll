package com.my.business.modular.rollorderconfig.entity;

import com.my.business.modular.rollorderconfigdetail.entity.RollOrderconfigdetail;
import com.my.business.sys.common.entity.BaseEntity;

import java.util.List;

/**
 * 工单配置实体类
 *
 * @author 生成器生成
 * @date 2020-08-08 15:59:24
 */
public class RollOrderconfig extends BaseEntity {

    private Long indocno;  //主键
    private String sno;  //主子表主表编码
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private String production_line;  //产线
    private Long production_line_id; //产线id
    private Long factory_id;  //生产厂家id
    private String factory;  //生产厂家
    private Long material_id; //材质id
    private String material;  //材质
    private Long frame_noid;  //机架号id
    private String frame_no;  //机架号
    private String order_name;  //工单名称
    private String order_no;  //工单编码
    private Long itype;  //部件类型id
    private String stype;  //部件类型
    private Long install_location_id;  //安装位置id
    private String install_location;  //安装位置
    private Long up_location_id;  //上机位置id
    private String up_location;  //上机位置
    private Long framerangeid;  //机架范围id
    private String framerange;  //机架范围
    private String snote;  //备注

    private List<RollOrderconfigdetail> detail;  //子表集合

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getSno() {
        return this.sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public Long getRoll_typeid() {
        return this.roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public String getOrder_name() {
        return this.order_name;
    }

    public void setOrder_name(String order_name) {
        this.order_name = order_name;
    }

    public String getOrder_no() {
        return this.order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public List<RollOrderconfigdetail> getDetail() {
        return detail;
    }

    public void setDetail(List<RollOrderconfigdetail> detail) {
        this.detail = detail;
    }

	public String getProduction_line() {
		return production_line;
	}

	public void setProduction_line(String production_line) {
		this.production_line = production_line;
	}

	public Long getProduction_line_id() {
		return production_line_id;
	}

	public void setProduction_line_id(Long production_line_id) {
		this.production_line_id = production_line_id;
	}

	public String getFactory() {
		return factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public Long getFrame_noid() {
		return frame_noid;
	}

	public void setFrame_noid(Long frame_noid) {
		this.frame_noid = frame_noid;
	}

	public String getFrame_no() {
		return frame_no;
	}

	public void setFrame_no(String frame_no) {
		this.frame_no = frame_no;
	}

	public Long getItype() {
		return itype;
	}

	public void setItype(Long itype) {
		this.itype = itype;
	}

	public String getStype() {
		return stype;
	}

	public void setStype(String stype) {
		this.stype = stype;
	}

	public Long getInstall_location_id() {
		return install_location_id;
	}

	public void setInstall_location_id(Long install_location_id) {
		this.install_location_id = install_location_id;
	}

	public String getInstall_location() {
		return install_location;
	}

	public void setInstall_location(String install_location) {
		this.install_location = install_location;
	}

	public Long getUp_location_id() {
		return up_location_id;
	}

	public void setUp_location_id(Long up_location_id) {
		this.up_location_id = up_location_id;
	}

	public String getUp_location() {
		return up_location;
	}

	public void setUp_location(String up_location) {
		this.up_location = up_location;
	}

	public Long getFramerangeid() {
		return framerangeid;
	}

	public void setFramerangeid(Long framerangeid) {
		this.framerangeid = framerangeid;
	}

	public String getFramerange() {
		return framerange;
	}

	public void setFramerange(String framerange) {
		this.framerange = framerange;
	}

	public Long getFactory_id() {
		return factory_id;
	}

	public void setFactory_id(Long factory_id) {
		this.factory_id = factory_id;
	}

	public Long getMaterial_id() {
		return material_id;
	}

	public void setMaterial_id(Long material_id) {
		this.material_id = material_id;
	}
}