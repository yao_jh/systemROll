package com.my.business.modular.rollorderconfig.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollorderconfig.dao.RollOrderconfigDao;
import com.my.business.modular.rollorderconfig.entity.JRollOrderconfig;
import com.my.business.modular.rollorderconfig.entity.RollOrderconfig;
import com.my.business.modular.rollorderconfig.service.RollOrderconfigService;
import com.my.business.modular.rollorderconfigdetail.dao.RollOrderconfigdetailDao;
import com.my.business.modular.rollorderconfigdetail.entity.RollOrderconfigdetail;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.util.DateUtil;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 工单配置接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-08 15:59:25
 */
@Service
public class RollOrderconfigServiceImpl implements RollOrderconfigService {

    @Autowired
    private RollOrderconfigDao rollOrderconfigDao;

    @Autowired
    private RollOrderconfigdetailDao rollOrderconfigdetailDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultData insertDataRollOrderconfig(String data, Long userId, String sname) {
        try {
            JRollOrderconfig jrollOrderconfig = JSON.parseObject(data, JRollOrderconfig.class);
            RollOrderconfig rollOrderconfig = jrollOrderconfig.getRollOrderconfig();

            String uuid = "ORDER" + DateUtil.getDateYmdhms("yyyy-MM-dd HH:mm:ss") + CodiUtil.uuid();  //生成唯一标识
            rollOrderconfig.setSno(uuid);
            CodiUtil.newRecord(userId, sname, rollOrderconfig);
            rollOrderconfigDao.insertDataRollOrderconfig(rollOrderconfig);

            //子表添加
            List<RollOrderconfigdetail> detail = rollOrderconfig.getDetail();
            for (RollOrderconfigdetail entity : detail) {
                CodiUtil.newRecord(userId, sname, entity);
                entity.setIlinkno(uuid);
                rollOrderconfigdetailDao.insertDataRollOrderconfigdetail(entity);
            }
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollOrderconfigOne(Long indocno) {
        try {
            rollOrderconfigDao.deleteDataRollOrderconfigOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollOrderconfigBySno(String sno) {
        try {
            rollOrderconfigDao.deleteDataRollOrderconfigBySno(sno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollOrderconfigMany(String str_id) {
        try {
            String sql = "delete roll_orderconfig where indocno in(" + str_id + ")";
            rollOrderconfigDao.deleteDataRollOrderconfigMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultData updateDataRollOrderconfig(String data, Long userId, String sname) {
        try {
            JRollOrderconfig jrollOrderconfig = JSON.parseObject(data, JRollOrderconfig.class);
            RollOrderconfig rollOrderconfig = jrollOrderconfig.getRollOrderconfig();
            CodiUtil.editRecord(userId, sname, rollOrderconfig);
            rollOrderconfigDao.updateDataRollOrderconfig(rollOrderconfig);

            //修改子表信息
            List<RollOrderconfigdetail> detail = rollOrderconfig.getDetail();
            for (RollOrderconfigdetail entity : detail) {
            	if(!StringUtils.isEmpty(entity.getIdel()) && entity.getIdel() == 0) {  //表示需要删除子表数据
            		rollOrderconfigdetailDao.deleteDataRollOrderconfigdetailOne(entity.getIndocno());
            	}else {
            		 if (StringUtils.isEmpty(entity.getIndocno())) {
            			 entity.setIlinkno(rollOrderconfig.getSno());
                         CodiUtil.newRecord(userId, sname, entity);
                         entity.setIlinkno(rollOrderconfig.getSno());
                         rollOrderconfigdetailDao.insertDataRollOrderconfigdetail(entity);
                     } else {
                         CodiUtil.newRecordModi(userId, sname, entity);
                         rollOrderconfigdetailDao.updateDataRollOrderconfigdetail(entity);
                     }
            	}
            }

            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollOrderconfigByPage(String data) {
        try {
            JRollOrderconfig jrollOrderconfig = JSON.parseObject(data, JRollOrderconfig.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollOrderconfig.getPageIndex();
            Integer pageSize = jrollOrderconfig.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollOrderconfig.getCondition()) {
                jsonObject = JSON.parseObject(jrollOrderconfig.getCondition().toString());
            }

            String order_no = null;
            String order_name = null;
            String roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("order_no"))) {
                order_no = jsonObject.get("order_no").toString();
            }
            if (!StringUtils.isEmpty(jsonObject.get("order_name"))) {
                order_name = jsonObject.get("order_name").toString();
            }
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = jsonObject.get("roll_typeid").toString();
            }

            List<RollOrderconfig> list = rollOrderconfigDao.findDataRollOrderconfigByPage((pageIndex - 1) * pageSize, pageSize, order_no, order_name, roll_typeid);
            Integer count = rollOrderconfigDao.findDataRollOrderconfigByPageSize(order_no, order_name, roll_typeid);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollOrderconfigByIndocno(String data) {
        try {
            JRollOrderconfig jrollOrderconfig = JSON.parseObject(data, JRollOrderconfig.class);
            Long indocno = jrollOrderconfig.getIndocno();

            RollOrderconfig rollOrderconfig = rollOrderconfigDao.findDataRollOrderconfigByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollOrderconfig);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollOrderconfig> findDataRollOrderconfig() {
        List<RollOrderconfig> list = rollOrderconfigDao.findDataRollOrderconfig();
        return list;
    }

	@Override
	public ResultData tbdata() {
		try {
			List<RollOrderconfig> list = rollOrderconfigDao.findtb();
			List<RollOrderconfigdetail> list_detail = rollOrderconfigdetailDao.findtbdatadetail();
			for(RollOrderconfig r : list) {
				for(RollOrderconfigdetail r_detail : list_detail) {
					RollOrderconfigdetail d = new RollOrderconfigdetail();
					BeanUtils.copyProperties(r_detail, d);  //把entity属性赋值给后面对象
					d.setIndocno(null);
					d.setIlinkno(r.getSno());
					rollOrderconfigdetailDao.insertDataRollOrderconfigdetail(d);
				}
			}
            return ResultData.ResultDataSuccess("同步成功");
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
	}

}
