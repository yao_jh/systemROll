package com.my.business.modular.rollorderconfig.service;

import com.my.business.modular.rollorderconfig.entity.RollOrderconfig;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 工单配置接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-08 15:59:24
 */
public interface RollOrderconfigService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollOrderconfig(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollOrderconfigOne(Long indocno);

    /**
     * 根据编码删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollOrderconfigBySno(String sno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollOrderconfigMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollOrderconfig(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollOrderconfigByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollOrderconfigByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollOrderconfig> findDataRollOrderconfig();
    
    /**
     * 同步数据
     */
    ResultData tbdata();
}
