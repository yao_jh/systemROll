package com.my.business.modular.rollwear.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.basechock.entity.BaseRollSafetyReminder;
import com.my.business.modular.dictionary.dao.DictionaryDao;
import com.my.business.modular.rollgrinding.dao.RollGrindingBFDao;
import com.my.business.modular.rollgrinding.dao.RollGrindingDao;
import com.my.business.modular.rollgrinding.entity.RollGrinding;
import com.my.business.modular.rollgrinding.entity.RollGrindingBF;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.modular.rollonoffline.dao.RollOnoffLineDao;
import com.my.business.modular.rollonoffline.entity.RollOnoffLine;
import com.my.business.modular.rollwear.dao.RollWearDao;
import com.my.business.modular.rollwear.entity.JRollWear;
import com.my.business.modular.rollwear.entity.RollWear;
import com.my.business.modular.rollwear.entity.RollWearInformation;
import com.my.business.modular.rollwear.service.RollWearService;
import com.my.business.restful.GradingKafkaService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.sysbz.service.SysBzService;
import com.my.business.util.CodiUtil;
import com.my.business.util.MathUtil;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 全生命周期——磨削统计接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-11-19 14:20:54
 */
@CommonsLog
@Service
public class RollWearServiceImpl implements RollWearService {

    private final static Log logger = LogFactory.getLog(RollWearServiceImpl.class);
    @Autowired
    private RollWearDao rollWearDao;
    @Autowired
    private RollOnoffLineDao rollOnoffLineDao;
    @Autowired
    private RollInformationDao rollInformationDao;
    @Autowired
    private RollGrindingBFDao rollGrindingBFDao;
    @Autowired
    private GradingKafkaService gradingKafkaService;
    @Autowired
    private SysBzService sysBzService;
    @Autowired
    private DictionaryDao dictionaryDao;

    public static Double tofixed(String num, int fix) {
        if (!StringUtils.isEmpty(num)) {
            Double nums = Double.parseDouble(num);
            String zero = "#0.";
            if (0 == fix) {
                zero = "#0";
            } else {
                for (int i = 1; i <= fix; i++) {
                    zero = zero + "0";
                }
            }
            DecimalFormat df = new DecimalFormat(zero);
            return Double.valueOf(df.format(nums));
        } else {
            return null;
        }
    }

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollWear(String data, Long userId, String sname) {
        try {
            JRollWear jrollWear = JSON.parseObject(data, JRollWear.class);
            RollWear rollWear = jrollWear.getRollWear();
            CodiUtil.newRecord(userId, sname, rollWear);
            rollWearDao.insertDataRollWear(rollWear);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollWearOne(Long indocno) {
        try {
            rollWearDao.deleteDataRollWearOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollWearMany(String str_id) {
        try {
            String sql = "delete from roll_wear where indocno in(" + str_id + ")";
            rollWearDao.deleteDataRollWearMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollWear(String data, Long userId, String sname) {
        try {
            JRollWear jrollWear = JSON.parseObject(data, JRollWear.class);
            RollWear rollWear = jrollWear.getRollWear();
            CodiUtil.editRecord(userId, sname, rollWear);
            rollWearDao.updateDataRollWear(rollWear);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */



    public ResultData findDataRollWearByPage(String data) {
        try {
            JRollWear jrollWear = JSON.parseObject(data, JRollWear.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollWear.getPageIndex();
            Integer pageSize = jrollWear.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollWear.getCondition()) {
                jsonObject = JSON.parseObject(jrollWear.getCondition().toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

            Long production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
            }

            List<RollWear> oldList = rollWearDao.findDataRollWearByPage((pageIndex - 1) * pageSize, pageSize, roll_no, production_line_id);
            List<RollWear> list = new ArrayList<>();
            if(oldList.size()>0){
                RollWear firstBean = new  RollWear();
                firstBean.setGrind_starttime(oldList.get(0).getGrind_starttime());
                firstBean.setGrind_endtime(oldList.get(0).getGrind_endtime());
                firstBean.setRoll_no(oldList.get(0).getRoll_no());
                firstBean.setFrame_no(oldList.get(0).getFrame_no());
                list.add(firstBean);
                for(int i = 0; i <oldList.size() ; i++){
                    if(i+1<oldList.size()){
                        oldList.get(i).setGrind_starttime(oldList.get(i+1).getGrind_starttime());
                        oldList.get(i).setGrind_endtime(oldList.get(i+1).getGrind_endtime());
                        list.add(oldList.get(i));
                    }else if(i == oldList.size()-1){
                        oldList.get(i).setGrind_starttime(null);
                        oldList.get(i).setGrind_endtime(null);
                        list.add(oldList.get(i));
                    }
                }
            }
            Integer count = rollWearDao.findDataRollWearByPageSize(roll_no, production_line_id);
            count+=2;
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public String excelFindByPage(HSSFWorkbook workbook, String roll_no) {
        String filename="全生命周期";
        if(StringUtils.isEmpty(roll_no)){
            roll_no = null;return null;
        }
        List<RollWear> oldList = rollWearDao.findDataRollWearByPage(0, 100, roll_no, null);
        List<RollWear> list = new ArrayList<>();
        if(oldList.size()>0){
            RollWear firstBean = new  RollWear();
            firstBean.setGrind_starttime(oldList.get(0).getGrind_starttime());
            firstBean.setGrind_endtime(oldList.get(0).getGrind_endtime());
            firstBean.setRoll_no(oldList.get(0).getRoll_no());
            firstBean.setFrame_no(oldList.get(0).getFrame_no());
            list.add(firstBean);
            for(int i = 0; i <oldList.size() ; i++){
                if(i+1<oldList.size()){
                    oldList.get(i).setGrind_starttime(oldList.get(i+1).getGrind_starttime());
                    oldList.get(i).setGrind_endtime(oldList.get(i+1).getGrind_endtime());
                    list.add(oldList.get(i));
                }else if(i == oldList.size()-1){
                    oldList.get(i).setGrind_starttime(null);
                    oldList.get(i).setGrind_endtime(null);
                    list.add(oldList.get(i));
                }
            }
        }
        HSSFSheet sheet = workbook.createSheet("sheet1");
        getExcelFindByPage(list, sheet, workbook);
        return filename;
    }

    private void getExcelFindByPage(List<RollWear> list, HSSFSheet sheet, HSSFWorkbook workbook) {
        if(list!=null && list.size()>0) {
            // 设置表头样式 居中 加粗 宋体 20
            HSSFCellStyle cellStyleHead = workbook.createCellStyle();
            cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
            HSSFFont redFontHead = workbook.createFont();
            redFontHead.setFontHeightInPoints((short) 12);//设置字体大小
            redFontHead.setFontName("宋体");//字体型号
            cellStyleHead.setFont(redFontHead);
            HSSFRow rowhead = sheet.createRow(0);   //创建第一行头
            HSSFCell cell_00 = rowhead.createCell(0);   //创建第一行第一列
            cell_00.setCellStyle(cellStyleHead);
            cell_00.setCellValue("全生命周期");

            HSSFRow row1 = sheet.createRow(1);   //创建第二行
            HSSFCell cell_2_1 = row1.createCell(0);   //创建第二行第一列
            cell_2_1.setCellValue("序号");
            HSSFCell cell_2_2 = row1.createCell(1);   //创建第二行第二列
            cell_2_2.setCellValue("辊号");
            HSSFCell cell_2_3 = row1.createCell(2);   //创建第二行第三列
            cell_2_3.setCellValue("机架号");
            HSSFCell cell_2_4 = row1.createCell(3);   //创建第二行第四列
            cell_2_4.setCellValue("上机时间");
            HSSFCell cell_2_5 = row1.createCell(4);   //创建第二行第五列
            cell_2_5.setCellValue("下机时间");
            HSSFCell cell_2_6 = row1.createCell(5);   //创建第二行第六列
            cell_2_6.setCellValue("磨削开始时间");
            HSSFCell cell_2_7 = row1.createCell(6);   //创建第二行第七列
            cell_2_7.setCellValue("磨削结束时间");
            HSSFCell cell_2_8 = row1.createCell(7);   //创建第二行第八列
            cell_2_8.setCellValue("轧制公里");
            HSSFCell cell_2_9 = row1.createCell(8);   //创建第二行第9列
            cell_2_9.setCellValue("轧制吨数");

            HSSFCell cell_2_10 = row1.createCell(9);   //创建第二行第9列
            cell_2_10.setCellValue("下次磨前直径");
            HSSFCell cell_2_11 = row1.createCell(10);   //创建第二行第9列
            cell_2_11.setCellValue("磨前直径");
            HSSFCell cell_2_12 = row1.createCell(11);   //创建第二行第9列
            cell_2_12.setCellValue("磨后直径");
            HSSFCell cell_2_13 = row1.createCell(12);   //创建第二行第9列
            cell_2_13.setCellValue("在线磨损量");
            HSSFCell cell_2_14 = row1.createCell(13);   //创建第二行第9列
            cell_2_14.setCellValue("一次磨削量");
            HSSFCell cell_2_15 = row1.createCell(14);   //创建第二行第9列
            cell_2_15.setCellValue("二次磨削量");
            HSSFCell cell_2_16 = row1.createCell(15);   //创建第二行第9列
            cell_2_16.setCellValue("磨削总量");
            HSSFCell cell_2_17= row1.createCell(16);   //创建第二行第9列
            cell_2_17.setCellValue("圆度");
            HSSFCell cell_2_18 = row1.createCell(17);   //创建第二行第9列
            cell_2_18.setCellValue("砂轮开始直径");
            HSSFCell cell_2_19 = row1.createCell(18);   //创建第二行第9列
            cell_2_19.setCellValue("砂轮结束直径");

            //从第三行开始(包含第三行)
            for (int i = 0; i < list.size(); i++) {
                RollWear v = list.get(i);
                HSSFRow rown = sheet.createRow(i + 2);   //创建第三行
                HSSFCell cell_3_1 = rown.createCell(0);   // 1
                cell_3_1.setCellValue(i + 1);
                HSSFCell cell_3_2 = rown.createCell(1);   //2
                if (!StringUtils.isEmpty(v.getRoll_no())) {
                    cell_3_2.setCellValue(v.getRoll_no());
                }
                HSSFCell cell_3_3 = rown.createCell(2);   //3
                if (!StringUtils.isEmpty(v.getFrame_no())){
                    cell_3_3.setCellValue(v.getFrame_no());
                }
                HSSFCell cell_3_4 = rown.createCell(3);   //3
                if (!StringUtils.isEmpty(v.getOnlinetime())) {
                    cell_3_4.setCellValue(v.getOnlinetime());
                }
                HSSFCell cell_3_5 = rown.createCell(4);   //3
                if (!StringUtils.isEmpty(v.getOfflinetime())) {
                    cell_3_5.setCellValue(v.getOfflinetime());
                }
                HSSFCell cell_3_6 = rown.createCell(5);   //3
                if (!StringUtils.isEmpty(v.getGrind_starttime())) {
                    cell_3_6.setCellValue(v.getGrind_starttime());
                }
                HSSFCell cell_3_7 = rown.createCell(6);   //3
                if (!StringUtils.isEmpty(v.getGrind_endtime())) {
                    cell_3_7.setCellValue(v.getGrind_endtime());
                }
                HSSFCell cell_3_8 = rown.createCell(7);   //3
                if (!StringUtils.isEmpty(v.getRollkilometer())) {
                    cell_3_8.setCellValue(v.getRollkilometer());
                }
                HSSFCell cell_3_9 = rown.createCell(8);   //3
                if (!StringUtils.isEmpty(v.getRolltonnage())) {
                    cell_3_9.setCellValue(v.getRolltonnage());
                }
                HSSFCell cell_3_10 = rown.createCell(9);   //3
                if (!StringUtils.isEmpty(v.getLasttime_after_diameter())) {
                    cell_3_10.setCellValue(v.getLasttime_after_diameter());
                }
                HSSFCell cell_3_11 = rown.createCell(10);   //3
                if (!StringUtils.isEmpty(v.getBefore_diameter())) {
                    cell_3_11.setCellValue(v.getBefore_diameter());
                }
                HSSFCell cell_3_12 = rown.createCell(11);   //3
                if (!StringUtils.isEmpty(v.getAfter_diameter())) {
                    cell_3_12.setCellValue(v.getAfter_diameter());
                }
                HSSFCell cell_3_13 = rown.createCell(12);   //3
                if (!StringUtils.isEmpty(v.getOnline_wear())) {
                    cell_3_13.setCellValue(v.getOnline_wear());
                }
                HSSFCell cell_3_14 = rown.createCell(13);   //3
                if (!StringUtils.isEmpty(v.getFirst_wear())) {
                    cell_3_14.setCellValue(v.getFirst_wear());
                }
                HSSFCell cell_3_15 = rown.createCell(14);   //3
                if (!StringUtils.isEmpty(v.getSecond_wear())) {
                    cell_3_15.setCellValue(v.getSecond_wear());
                }
                HSSFCell cell_3_16 = rown.createCell(15);   //3
                if (!StringUtils.isEmpty(v.getWear())) {
                    cell_3_16.setCellValue(v.getWear());
                }
                HSSFCell cell_3_17 = rown.createCell(16);   //3
                if (!StringUtils.isEmpty(v.getRoundness())) {
                    cell_3_17.setCellValue(v.getRoundness());
                }
                HSSFCell cell_3_18 = rown.createCell(17);   //3
                if (!StringUtils.isEmpty(v.getWheel_dia_start())) {
                    cell_3_18.setCellValue(v.getWheel_dia_start());
                }
                HSSFCell cell_3_19 = rown.createCell(18);   //3
                if (!StringUtils.isEmpty(v.getWheel_dia_end())) {
                    cell_3_19.setCellValue(v.getWheel_dia_end());
                }
            }
            CellRangeAddress region = new CellRangeAddress(0, 0, 0, 18);
            sheet.addMergedRegion(region);
        }
    }


    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollWearByIndocno(String data) {
        try {
            JRollWear jrollWear = JSON.parseObject(data, JRollWear.class);
            Long indocno = jrollWear.getIndocno();

            RollWear rollWear = rollWearDao.findDataRollWearByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollWear);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollWear> findDataRollWear() {
        List<RollWear> list = rollWearDao.findDataRollWear();
        return list;
    }

    /**
     * 计算某一个轧辊的全生命周期磨削
     *
     * @param roll_no 辊号
     * @return
     */
    public void changedata(String roll_no) {
        if(roll_no==""||roll_no==null||"".equals(roll_no)){
            return;
        }
        rollWearDao.deleteDataRollWearByRollNo(roll_no);
        List<RollWear> list = new ArrayList<>();
        List<String> times = new ArrayList<>();
        //查找对应辊号上下线列表
        List<RollOnoffLine> rollOnoffLineList = rollOnoffLineDao.findDataRollOnoffLineByPageOrderByTime(roll_no, null);

        JSONObject request = new JSONObject();
        request.put("body", rollOnoffLineList);
        request.put("math", rollOnoffLineList.size());
        logger.debug("该辊号对应的上下线列表" + request.toJSONString());

        //查找对应辊号基本信息
        RollInformation rollInformation = rollInformationDao.findDataRollInformationByRollNo(roll_no);
if(rollInformation==null){
    return;
};
        //查找相邻两个上线时间之间磨削次数数量
        for (int j = 0; j < rollOnoffLineList.size(); j++) {
            Integer g = 0;
            if (rollOnoffLineList.size() == 1 || j + 1 == rollOnoffLineList.size()) {
                g = rollGrindingBFDao.findDataRollGrindingBFByPageSize(roll_no, null,rollOnoffLineList.get(j).getOnlinetime(),null, null,null);
            } else {
                g = rollGrindingBFDao.findDataRollGrindingBFByPageSize(roll_no, rollOnoffLineList.get(j + 1).getOnlinetime(), rollOnoffLineList.get(j).getOnlinetime(), null,null,null);
            }
            if (g > 0) {
                times.add(rollOnoffLineList.get(j).getOnlinetime());
            }
        }

        JSONObject request2 = new JSONObject();
        request2.put("body", times);
        logger.debug("该辊号对应的上下线列表" + request2.toJSONString());

        //循环把时间信息写入
        if (!StringUtils.isEmpty(rollOnoffLineList) && rollOnoffLineList.size() > 0) {

            for (int i = 0; i < rollOnoffLineList.size(); i++) {
                RollWear rollWear = new RollWear();
                BeanUtils.copyProperties(rollOnoffLineList.get(i), rollWear);
                //获取机架号
                String frame_no = rollWear.getFrame_no().replace(" ", "");
                //反查——获取机架号id
                Long frame_noid = Long.valueOf(dictionaryDao.findMapBynameV1("frameteam", frame_no));

                rollWear.setFrame_no(frame_no);
                rollWear.setFrame_noid(String.valueOf(frame_noid));

                String before_diameter = null;
                String after_diameter = null;
                String first_wear = null;
                String second_wear = null;
                Double wear = null;
                String residual_thickness = null;
                String lasttime_after_diameter = null;
                String online_wear = null;
                Long count_weartimes = 0L;  //磨削次数
                Double deviation = null;  //辊形偏差
                Double taper = null;  //锥度
                Double roundness = null;  //圆度
                Double crack = null;  //裂纹
                Double hidden_flaws = null;  //暗伤
                Double qualifiednum = null;  //合格点数
                Double wheel_dia_start = null;  //砂轮开始直径
                Double wheel_dia_end = null;  //砂轮结束直径

                //如果是最后一条上线数据（最早）
                if (i + 1 == rollOnoffLineList.size()) {
                    logger.debug("最早");
                    Integer g = rollGrindingBFDao.findDataRollGrindingBFByPageSize(roll_no, rollOnoffLineList.get(i).getOnlinetime(),  null, null,null,null);
                    logger.debug("对应数量：" + g);
                    if (g > 0) {
                        if (!StringUtils.isEmpty(rollOnoffLineList.get(i).getOnlinetime())) {

                            //lasttime_after_diameter = rollWearDao.lasttimeafterdiameterNotFirst(roll_no, times.get(rollOnoffLineList.size()-2));

                            before_diameter = rollWearDao.beforediameterFirst(roll_no, rollOnoffLineList.get(i).getOnlinetime());
                            after_diameter = rollWearDao.afterdiameterFirst(roll_no, rollOnoffLineList.get(i).getOnlinetime());
                            first_wear = rollWearDao.Firstchange(roll_no, rollOnoffLineList.get(i).getOnlinetime());
                            second_wear = rollWearDao.Secondchange(roll_no, rollOnoffLineList.get(i).getOnlinetime());
                            String w = rollWearDao.change(roll_no, rollOnoffLineList.get(i).getOnlinetime());

                            //if (!StringUtils.isEmpty(after_diameter) && !StringUtils.isEmpty(lasttime_after_diameter)) {
                            //    online_wear = String.valueOf(Double.parseDouble(after_diameter)-Double.parseDouble(lasttime_after_diameter));
                            //}
                            //rollWear.setOnline_wear(tofixed(online_wear, 3)); //在线磨削量

                            if (!StringUtils.isEmpty(w)) {
                                wear = tofixed(w, 3);
                            } else {
                                wear = 0.00;
                            }
                        }
                        if (!StringUtils.isEmpty(after_diameter) && !StringUtils.isEmpty(rollInformation.getTheorydiameter()) && !StringUtils.isEmpty(rollInformation.getWork_layer())) {
                            residual_thickness = String.valueOf((Double.parseDouble(after_diameter) - (rollInformation.getTheorydiameter() - Double.parseDouble(rollInformation.getWork_layer()))) / 2);
                        }

                        rollWear.setBefore_diameter(tofixed(before_diameter, 3)); //磨前直径
                        rollWear.setAfter_diameter(tofixed(after_diameter, 3)); //磨后直径
                        rollWear.setLasttime_after_diameter(tofixed(lasttime_after_diameter, 3)); //上一次磨后直径
                        rollWear.setOnline_wear(null); //在线磨削量
                        rollWear.setFirst_wear(tofixed(first_wear, 3)); //一次磨削量
                        rollWear.setSecond_wear(tofixed(second_wear, 3)); //二次磨削量
                        Double s = 0.00;
                        Double f = 0.00;
                        if(!StringUtils.isEmpty(first_wear) ){
                            s=tofixed(first_wear, 3);
                        }
                        if(!StringUtils.isEmpty(second_wear) ){
                            f=tofixed(second_wear, 3);
                        }
                        wear = f+s;
                        rollWear.setWear(tofixed(String.valueOf(wear), 3)); //磨削量
                        if (wear > 0.4) {
                            rollWear.setIf_power(1L); //是否为超磨辊
                        } else if (wear <= 0.4) {
                            rollWear.setIf_power(0L); //是否为超磨辊
                        } else {
                            rollWear.setIf_power(null); //是否为超磨辊
                        }
                        rollWear.setResidual_thickness(tofixed(residual_thickness, 3)); //剩余工作层厚度

                        RollGrindingBF rollGrindingBF = rollGrindingBFDao.findNewMax(roll_no, rollOnoffLineList.get(i).getOnlinetime());
                        if(rollGrindingBF==null){
                           return;
                        }
                        String begintimefirst = rollWearDao.findOldMaxFirst(roll_no, rollOnoffLineList.get(i).getOnlinetime());
                        count_weartimes = Long.valueOf(g);   //磨削次数
                        //deviation = rollGrindingBF.getDeviation();
                        //taper = rollGrindingBF.getTaper();
                        roundness = rollGrindingBF.getRoundness();
                        //crack = rollGrindingBF.getCrack();
                        //hidden_flaws = rollGrindingBF.getHidden_flaws();
                        // qualifiednum = rollGrindingBF.getQualifiednum();
                        wheel_dia_start = rollGrindingBF.getWheel_dia_start();
                        wheel_dia_end = rollGrindingBF.getWheel_dia_end();

                        rollWear.setMachine_no(rollGrindingBF.getMachineno());
                        rollWear.setGrind_starttime(begintimefirst);
                        rollWear.setGrind_endtime(rollGrindingBF.getGrind_starttime());
                        rollWear.setCount_weartimes(count_weartimes);
                        //rollWear.setDeviation(deviation);
                        //rollWear.setTaper(taper);
                        rollWear.setRoundness(roundness);
                        //rollWear.setCrack(crack);
                        //rollWear.setHidden_flaws(hidden_flaws);
                        //rollWear.setQualifiednum(qualifiednum);
                        rollWear.setWheel_dia_start(wheel_dia_start);
                        rollWear.setWheel_dia_end(wheel_dia_end);
                        rollWear.setSbz(rollGrindingBF.getSgroup());

                        //磨削重量计算
                        //公式：（重量*磨削量）/工作层
                        if (!StringUtils.isEmpty(rollInformation.getWeight()) && !StringUtils.isEmpty(wear) && !StringUtils.isEmpty(rollInformation.getWork_layer())) {
                            rollWear.setWear_height(
                                    (rollInformation.getWeight() * wear) / Double.parseDouble(rollInformation.getWork_layer())
                            );
                        } else {
                            rollWear.setWear_height(null);
                        }

                        JSONObject request3 = new JSONObject();
                        request3.put("body", rollWear);
                        logger.debug("要保存的数据1：" + request3.toJSONString());

                        if(rollWear.getGrind_endtime()!=null&&rollWear.getGrind_endtime()!=""&&!"".equals(rollWear.getGrind_endtime())){
                            Map<String, String> map = null;
                            try {
                                map = sysBzService.findbz(rollWear.getGrind_endtime());
                                if(!StringUtils.isEmpty(map.get("bz"))){
                                    rollWear.setSbz(map.get("bz"));
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        rollWearDao.insertDataRollWear(rollWear);
                    }
                } else if (i + 1 < rollOnoffLineList.size()) {
                    logger.debug("不是最早");
                    for (int l = 0; l < times.size(); l++) {
                        if (rollOnoffLineList.get(i).getOnlinetime().equals(times.get(l)) && l + 1 < times.size()) {

                            JSONObject request4 = new JSONObject();
                            request4.put("body", rollOnoffLineList.get(i).getOnlinetime());
                            request4.put("body2", times.size());
                            request4.put("body3", i);
                            logger.debug("要保存的数据1：" + request4.toJSONString());
                            //调整为 磨前直径

                           // if(l>0){
                            //    lasttime_after_diameter = rollWearDao.lasttimeafterdiameterNotFirst(roll_no, times.get(l-1));
                            //}
                            lasttime_after_diameter = rollWearDao.lasttimeafterdiameterNotFirst(roll_no, times.get(l + 1));

                            before_diameter = rollWearDao.beforediameterNotFirst(roll_no, times.get(l), times.get(l + 1));
                            after_diameter = rollWearDao.afterdiameterNotFirst(roll_no, times.get(l));
                           /* if (!StringUtils.isEmpty(after_diameter) && !StringUtils.isEmpty(lasttime_after_diameter)) {
                                online_wear = String.valueOf(Double.parseDouble(after_diameter)-Double.parseDouble(lasttime_after_diameter));
                            }*/
                            if (!StringUtils.isEmpty(before_diameter) && !StringUtils.isEmpty(lasttime_after_diameter)) {
                                online_wear = String.valueOf(Double.parseDouble(lasttime_after_diameter) - Double.parseDouble(before_diameter));
                            }
                            first_wear = rollWearDao.FirstTimesdiameterNotFirst(roll_no, times.get(l), times.get(l + 1));
                            second_wear = rollWearDao.SecondTimesdiameterNotFirst(roll_no, times.get(l), times.get(l + 1));
                            String begintimefirst = rollWearDao.findOldMaxNotFirst(roll_no, times.get(l), times.get(l + 1));
                            Double s = 0.00;
                            Double f = 0.00;
                            if (!StringUtils.isEmpty(first_wear)) {
                                f = tofixed(first_wear, 3);
                            }
                            if (!StringUtils.isEmpty(second_wear)) {
                                s = tofixed(second_wear, 3);
                            }
                            Double ow = 0.00;
                            if (!StringUtils.isEmpty(online_wear)) {
                                ow = tofixed(online_wear, 3);
                            }
                            wear = f + s+ow;
                            if (!StringUtils.isEmpty(after_diameter) && !StringUtils.isEmpty(rollInformation.getTheorydiameter()) && !StringUtils.isEmpty(rollInformation.getWork_layer())) {
                                residual_thickness = String.valueOf((Double.parseDouble(after_diameter) - (rollInformation.getTheorydiameter() - Double.parseDouble(rollInformation.getWork_layer()))) / 2);
                            }

                            rollWear.setBefore_diameter(tofixed(before_diameter, 3)); //磨前直径
                            rollWear.setAfter_diameter(tofixed(after_diameter, 3)); //磨后直径
                            rollWear.setLasttime_after_diameter(tofixed(lasttime_after_diameter, 3)); //上一次磨后直径
                            rollWear.setOnline_wear(tofixed(online_wear, 3)); //在线磨削量
                            rollWear.setFirst_wear(tofixed(first_wear, 3)); //一次磨削量
                            rollWear.setSecond_wear(tofixed(second_wear, 3)); //二次磨削量
                            rollWear.setWear(tofixed(String.valueOf(wear), 3)); //磨削量
                            if (wear > 0.4) {
                                rollWear.setIf_power(1L); //是否为超磨辊
                            } else if (wear <= 0.4) {
                                rollWear.setIf_power(0L); //是否为超磨辊
                            } else {
                                rollWear.setIf_power(null); //是否为超磨辊
                            }
                            rollWear.setResidual_thickness(tofixed(residual_thickness, 3)); //剩余工作层厚度

                            RollGrindingBF rollGrindingBF = rollGrindingBFDao.findNewMax(roll_no, rollOnoffLineList.get(i).getOnlinetime());
                            if(rollGrindingBF==null){
                                return;
                            }
                            Integer g = rollGrindingBFDao.findDataRollGrindingBFByPageSize(roll_no,  times.get(l),times.get(l+1), null, null,null);
                            count_weartimes = Long.valueOf(g);   //磨削次数
                            //deviation = rollGrindingBF.getDeviation();
                            //taper = rollGrindingBF.getTaper();
                            roundness = rollGrindingBF.getRoundness();
                            //crack = rollGrindingBF.getCrack();
                            //hidden_flaws = rollGrindingBF.getHidden_flaws();
                            //qualifiednum = rollGrindingBF.getQualifiednum();
                            wheel_dia_start = rollGrindingBF.getWheel_dia_start();
                            wheel_dia_end = rollGrindingBF.getWheel_dia_end();

                            rollWear.setMachine_no(rollGrindingBF.getMachineno());
                            rollWear.setGrind_starttime(begintimefirst);
                            rollWear.setGrind_endtime(rollGrindingBF.getGrind_starttime());
                            rollWear.setCount_weartimes(count_weartimes);
                            //rollWear.setDeviation(deviation);
                            //rollWear.setTaper(taper);
                            rollWear.setRoundness(roundness);
                            //rollWear.setCrack(crack);
                            //rollWear.setHidden_flaws(hidden_flaws);
                            //rollWear.setQualifiednum(qualifiednum);
                            rollWear.setWheel_dia_start(wheel_dia_start);
                            rollWear.setWheel_dia_end(wheel_dia_end);
                            rollWear.setSbz(rollGrindingBF.getSgroup());

                            //磨削重量计算
                            //公式：（重量*磨削量）/工作层
                            if (!StringUtils.isEmpty(rollInformation.getWeight()) && !StringUtils.isEmpty(online_wear) && !StringUtils.isEmpty(wear) && !StringUtils.isEmpty(rollInformation.getWork_layer())) {
                                logger.debug("weight= " + rollInformation.getWeight() + "onlinewear=" + online_wear + "worklayer= " + rollInformation.getWork_layer());
                                rollWear.setWear_height(
                                        (rollInformation.getWeight() * (Double.parseDouble(online_wear) + wear)) / Double.parseDouble(rollInformation.getWork_layer())
                                );
                            } else {
                                rollWear.setWear_height(null);
                            }
                            JSONObject request5 = new JSONObject();
                            request5.put("body", rollWear);
                            logger.debug("要保存的数据2：" + request5.toJSONString());

                            if(!StringUtils.isEmpty(rollWear.getGrind_endtime())){
                                Map<String, String> map = null;
                                try {
                                    map = sysBzService.findbz(rollWear.getGrind_endtime());
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                if(!StringUtils.isEmpty(map.get("bz"))){
                                    rollWear.setSbz(map.get("bz"));
                                }

                            }
                            rollWearDao.insertDataRollWear(rollWear);

                            //发送消息
                            //gradingKafkaService.sendReal(offlinemessage(rollWear));

                        } else {
                            logger.debug("范围内没有磨削数据");
                        }
                    }
                }
                /**
                 * 同步数据    roll_onoff_line roll_grinding_bf
                 *1  最新一次上线时间 lastuplinetime
                 *1  最新一次下线时间 lastlowlinetime;
                 * 2 最新一次磨削量 lastgrindingdepth;
                 *1  第一次上线时间 firstuplinetime
                 * 2 累计磨削次数 grindingcount
                 * 2 累计磨削量 lastgrindingdepth
                 * 1 累计上线次数 uplinecount
                 * 1 累计轧制公里数 rollkilometer
                 * 1 累计轧制吨位数 rolltonnagetotle
                 * 2(curvetype) 辊形  rollshape
                 * 2 圆度 roundness
                 */
                if(rollOnoffLineList.size()>0){
                    //
                    RollInformation r1 = rollInformationDao.findSynchronousRollOnoffLineByRollno(rollInformation.getRoll_no());
                    if(r1!=null){
                        if(r1.getUplinecount()>0){
                            rollInformation.setLastuplinetime(r1.getLastuplinetime());
                            rollInformation.setLastlowlinetime(r1.getLastlowlinetime());
                            rollInformation.setFirstuplinetime(r1.getFirstuplinetime());
                            rollInformation.setUplinecount(r1.getUplinecount());
                            rollInformation.setRollkilometer(r1.getRollkilometer());
                            rollInformation.setRolltonnagetotle(r1.getRolltonnagetotle());
                        }
                    }
                    //RollInformation r2 = rollInformationDao.findSynchronousRollGrindingBFByRollno(rollInformation.getRoll_no());
                    //if(r2!=null){
                     //   if(r2.getRoll_no()!=null){
                     //       rollInformation.setLastgrindingdepth(r2.getLastgrindingdepth());
                      //      rollInformation.setRollshape(r2.getRollshape());
                      //      rollInformation.setRoundness(r2.getRoundness());
                       //     rollInformation.setGrindingcount(r2.getGrindingcount());
                       //     rollInformation.setLastgrindingdepth(r2.getLastgrindingdepth());
                       //     rollInformation.setCurrentdiameter(r2.getCurrentdiameter());
                       // }
                   // }

                    rollInformationDao.updateDataRollInformation(rollInformation);
                }
            }
        }
    }

    public RollWearInformation offlinemessage(RollWear rollWear) {
        RollWearInformation rollWearInformation = new RollWearInformation();

        //第一部分构成:上下线信息载入
        rollWearInformation.setRoll_no(rollWear.getRoll_no()); //辊号
        rollWearInformation.setRollkilometer(rollWear.getRollkilometer());  //轧制公里数
        rollWearInformation.setUplinecount(rollWear.getUplinecount());  //累计上线次数
        rollWearInformation.setRolltonnage(rollWear.getRolltonnage());   //轧制吨数
        rollWearInformation.setOnlinetime(rollWear.getOnlinetime());   //上机时间
        rollWearInformation.setOfflinetime(rollWear.getOfflinetime());   //下机时间
        rollWearInformation.setFrame_no(rollWear.getFrame_no());  //机架号


        //第二部分构成:基本信息载入
        RollInformation rollInformation = rollInformationDao.findDataRollInformationByRollNo(rollWear.getRoll_no());
        if (rollInformation != null) {
            rollWearInformation.setRoll_type(rollInformation.getRoll_type());  //轧辊类型
            rollWearInformation.setRoll_position(rollInformation.getRoll_position());  //轧辊位置
            rollWearInformation.setPrice(rollInformation.getPrice());  //单价
            rollWearInformation.setMmprice(rollInformation.getMmprice());   //毫米单价
        }

        //第三部分构成：磨削信息载入
        RollOnoffLine rollOnoffLine1 = rollOnoffLineDao.findLastTime(rollWear.getRoll_no(), rollWear.getOnlinetime());//查找上一次的上下线记录
        if (!StringUtils.isEmpty(rollOnoffLine1)) {
            //String lasttime_after_diameter = rollWearDao.lasttimeafterdiameterNotFirst(rollWear.getRoll_no(), rollOnoffLine1.getOnlinetime());
            String before_diameter = rollWearDao.beforediameterNotFirst(rollWear.getRoll_no(), rollWear.getOnlinetime(), rollOnoffLine1.getOnlinetime());
            String after_diameter = rollWearDao.afterdiameterNotFirst(rollWear.getRoll_no(), rollWear.getOnlinetime());
            Double la = 0.00;
            Double be = 0.00;
            Double af = 0.00;
            //if (!StringUtils.isEmpty(lasttime_after_diameter)) {
             //   la = Double.parseDouble(lasttime_after_diameter);
            //}
            if (!StringUtils.isEmpty(before_diameter)) {
                be = Double.parseDouble(before_diameter);
            }
            if (!StringUtils.isEmpty(after_diameter)) {
                af = Double.parseDouble(after_diameter);
            }
            rollWearInformation.setOnline_wear(MathUtil.tofixed((la - be) + (be - af), 3));
        } else {
            rollWearInformation.setOnline_wear(null);
        }
        if (!StringUtils.isEmpty(rollInformation.getWeight()) && !StringUtils.isEmpty(rollWearInformation.getOnline_wear()) && !StringUtils.isEmpty(rollInformation.getWork_layer())) {
            rollWearInformation.setWear_height(
                    (rollInformation.getWeight() * rollWearInformation.getOnline_wear() / Double.parseDouble(rollInformation.getWork_layer()))
            );
        } else {
            rollWearInformation.setWear_height(null);
        }
        return rollWearInformation;
    }


}
