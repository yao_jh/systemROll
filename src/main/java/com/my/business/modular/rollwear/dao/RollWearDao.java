package com.my.business.modular.rollwear.dao;

import com.my.business.modular.rollgrinding.entity.RollGrinding;
import com.my.business.modular.rollwear.entity.RollWear;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 全生命周期——磨削统计dao接口
 *
 * @author 生成器生成
 * @date 2020-11-19 14:20:54
 */
@Mapper
public interface RollWearDao {

    /**
     * 添加记录
     *
     * @param rollWear 对象实体
     */
    void insertDataRollWear(RollWear rollWear);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollWearOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollWearMany(String value);

    /**
     * 修改记录
     *
     * @param rollWear 对象实体
     */
    void updateDataRollWear(RollWear rollWear);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollWear> findDataRollWearByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("roll_no") String roll_no, @Param("production_line_id") Long production_line_id);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollWearByPageSize(@Param("roll_no") String roll_no, @Param("production_line_id") Long production_line_id);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollWear findDataRollWearByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollWear> findDataRollWear();

    /**
     * 首次上线记录时，最早的磨削开始时间计算
     */
    String findOldMaxFirst(@Param("roll_no") String roll_no, @Param("onlinetime") String onlinetime);

    /**
     * 首次上线记录时，磨前直径计算
     */
    String beforediameterFirst(@Param("roll_no") String roll_no, @Param("onlinetime") String onlinetime);

    /**
     * 首次上线记录时，磨后直径计算
     */
    String afterdiameterFirst(@Param("roll_no") String roll_no, @Param("onlinetime") String onlinetime);

    /**
     * 首次上线记录时，一次磨削量计算
     */
    String Firstchange(@Param("roll_no") String roll_no, @Param("onlinetime") String onlinetime);

    /**
     * 首次上线记录时，二次磨削量计算
     */
    String Secondchange(@Param("roll_no") String roll_no, @Param("onlinetime") String onlinetime);

    /**
     * 首次上线记录时，磨削量计算
     */
    String change(@Param("roll_no") String roll_no, @Param("onlinetime") String onlinetime);

    /**
     * 非首次上线时，上一次磨后直径计算
     *
     * @param onlinetime 特别注明：此处为 上一次磨削后的上线时间，仅用于本方法
     * @param roll_no    辊号
     */
    String lasttimeafterdiameterNotFirst(@Param("roll_no") String roll_no, @Param("onlinetime") String onlinetime);

    /**
     * 首次上线记录时，最早的磨削开始时间计算
     */
    String findOldMaxNotFirst(@Param("roll_no") String roll_no, @Param("onlinetime") String onlinetime, @Param("lasttime") String lasttime);

    /**
     * 非首次上线时，磨前直径计算
     *
     * @param onlinetime 上线时间
     * @param lasttime   上一次上线时间
     * @param roll_no    辊号
     */
    String beforediameterNotFirst(@Param("roll_no") String roll_no, @Param("onlinetime") String onlinetime, @Param("lasttime") String lasttime);

    /**
     * 非首次上线时，磨后直径计算
     *
     * @param onlinetime 上线时间
     * @param roll_no    辊号
     */
    String afterdiameterNotFirst(@Param("roll_no") String roll_no, @Param("onlinetime") String onlinetime);

    /**
     * 非首次上线时，一次磨削量计算
     *
     * @param onlinetime 上线时间
     * @param lasttime   上一次上线时间
     * @param roll_no    辊号
     */
    String FirstTimesdiameterNotFirst(@Param("roll_no") String roll_no, @Param("onlinetime") String onlinetime, @Param("lasttime") String lasttime);

    /**
     * 非首次上线时，二次磨削量计算
     *
     * @param onlinetime 上线时间
     * @param lasttime   上一次上线时间
     * @param roll_no    辊号
     */
    String SecondTimesdiameterNotFirst(@Param("roll_no") String roll_no, @Param("onlinetime") String onlinetime, @Param("lasttime") String lasttime);

    /**
     * 根据辊号删除信息
     * @param roll_no 辊号
     */
    void deleteDataRollWearByRollNo(@Param("roll_no") String roll_no);

    List<RollWear> find();

    void deleteDuplicateDataRollGrindingBFMessage();
}
