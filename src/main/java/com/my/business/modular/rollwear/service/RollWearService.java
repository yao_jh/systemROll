package com.my.business.modular.rollwear.service;

import com.my.business.modular.rollonoffline.entity.RollOnoffLine;
import com.my.business.modular.rollwear.entity.RollWear;
import com.my.business.modular.rollwear.entity.RollWearInformation;
import com.my.business.sys.common.entity.ResultData;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.List;

/**
 * 全生命周期——磨削统计接口服务类
 *
 * @author 生成器生成
 * @date 2020-11-19 14:20:54
 */
public interface RollWearService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollWear(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollWearOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollWearMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollWear(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollWearByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollWearByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollWear> findDataRollWear();

    /**
     * 根据辊号更新全生命周期管理内容
     * @param roll_no 辊号
     */
    void changedata(String roll_no);

    /**
     * 根据轧辊下线信息发送对应数据
     */
    RollWearInformation offlinemessage(RollWear rollWear);

    String excelFindByPage(HSSFWorkbook workbook, String roll_no);
}
