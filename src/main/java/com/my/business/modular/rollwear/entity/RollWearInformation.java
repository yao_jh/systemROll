package com.my.business.modular.rollwear.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 全生命周期——磨削统计实体类
 *
 * @author 生成器生成
 * @date 2020-11-19 14:20:53
 */
public class RollWearInformation extends BaseEntity {

    private String roll_no;  //辊号
    private String roll_type;  //轧辊类型
    private String frame_no;  //机架号
    private String roll_position;  //轧辊位置
    private Double rollkilometer;  //轧制公里数
    private Long uplinecount;  //累计上线次数
    private Double rolltonnage;  //轧制吨数
    private String onlinetime;  //上机时间
    private String offlinetime;  //下机时间
    private Double online_wear;  //轧制磨削消耗量（在线磨损量+磨削量）
    private Double price;  //单价
    private Double mmprice;  //毫米单价
    private String send_time; //发送时间
    private Double wear_height;  //磨削重量
    
    private String GAPsender;
	private String GAPreceiver;
	
    public String getGAPsender() {
		return GAPsender;
	}

	public void setGAPsender(String gAPsender) {
		GAPsender = gAPsender;
	}

	public String getGAPreceiver() {
		return GAPreceiver;
	}

	public void setGAPreceiver(String gAPreceiver) {
		GAPreceiver = gAPreceiver;
	}

	public String getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getRoll_type() {
        return roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public String getFrame_no() {
        return frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public String getRoll_position() {
        return roll_position;
    }

    public void setRoll_position(String roll_position) {
        this.roll_position = roll_position;
    }

    public Double getRollkilometer() {
        return rollkilometer;
    }

    public void setRollkilometer(Double rollkilometer) {
        this.rollkilometer = rollkilometer;
    }

    public Long getUplinecount() {
        return uplinecount;
    }

    public void setUplinecount(Long uplinecount) {
        this.uplinecount = uplinecount;
    }

    public Double getRolltonnage() {
        return rolltonnage;
    }

    public void setRolltonnage(Double rolltonnage) {
        this.rolltonnage = rolltonnage;
    }

    public String getOnlinetime() {
        return onlinetime;
    }

    public void setOnlinetime(String onlinetime) {
        this.onlinetime = onlinetime;
    }

    public String getOfflinetime() {
        return offlinetime;
    }

    public void setOfflinetime(String offlinetime) {
        this.offlinetime = offlinetime;
    }

    public Double getOnline_wear() {
        return online_wear;
    }

    public void setOnline_wear(Double online_wear) {
        this.online_wear = online_wear;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getMmprice() {
        return mmprice;
    }

    public void setMmprice(Double mmprice) {
        this.mmprice = mmprice;
    }

	public String getSend_time() {
		return send_time;
	}

	public void setSend_time(String send_time) {
		this.send_time = send_time;
	}

    public Double getWear_height() {
        return wear_height;
    }

    public void setWear_height(Double wear_height) {
        this.wear_height = wear_height;
    }
}