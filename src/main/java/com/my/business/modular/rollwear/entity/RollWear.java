package com.my.business.modular.rollwear.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 全生命周期——磨削统计实体类
 *
 * @author 生成器生成
 * @date 2020-11-19 14:20:53
 */
public class RollWear extends BaseEntity {

    private Long indocno;  //主键
    private String roll_no;  //辊号
    private String onlinetime;  //上机时间
    private String offlinetime;  //下机时间
    private String off_line_reason;  //下线原因
    private Long uplinecount;  //累计上线次数
    private Double rollkilometer;  //轧制公里数
    private Double rolltonnage;  //轧制吨数
    private Double lasttime_after_diameter;  //上一次磨后直径
    private Double before_diameter;  //磨前直径
    private Double after_diameter;  //磨后直径
    private Double online_wear;  //在线磨损量
    private Double first_wear;  //一次磨损量
    private Double second_wear;  //二次磨损量
    private Double wear;  //磨损量
    private Long if_power;  //是否为超磨辊
    private Double residual_thickness;  //剩余工作层直径
    private Long count_weartimes;  //磨削次数
    private Double deviation;  //辊形偏差
    private Double taper;  //锥度
    private Double roundness;  //圆度
    private Double crack;  //裂纹
    private Double hidden_flaws;  //暗伤
    private Double qualifiednum;  //合格点数
    private Double wheel_dia_start;  //砂轮开始直径
    private Double wheel_dia_end;  //砂轮结束直径
    private String frame_noid;  //机架号id
    private String frame_no;  //机架号
    private String machine_no;  //磨床号
    private String grind_starttime;  //最早磨削开始时间
    private String grind_endtime;  //最新磨削开始时间
    private Long ibz;  //班组id
    private String sbz;  //班组
    private Double wear_height;  //磨削重量

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getOnlinetime() {
        return this.onlinetime;
    }

    public void setOnlinetime(String onlinetime) {
        this.onlinetime = onlinetime;
    }

    public String getOfflinetime() {
        return this.offlinetime;
    }

    public void setOfflinetime(String offlinetime) {
        this.offlinetime = offlinetime;
    }

    public String getOff_line_reason() {
        return this.off_line_reason;
    }

    public void setOff_line_reason(String off_line_reason) {
        this.off_line_reason = off_line_reason;
    }

    public Long getUplinecount() {
        return this.uplinecount;
    }

    public void setUplinecount(Long uplinecount) {
        this.uplinecount = uplinecount;
    }

    public Double getRollkilometer() {
        return this.rollkilometer;
    }

    public void setRollkilometer(Double rollkilometer) {
        this.rollkilometer = rollkilometer;
    }

    public Double getRolltonnage() {
        return this.rolltonnage;
    }

    public void setRolltonnage(Double rolltonnage) {
        this.rolltonnage = rolltonnage;
    }

    public Double getLasttime_after_diameter() {
        return this.lasttime_after_diameter;
    }

    public void setLasttime_after_diameter(Double lasttime_after_diameter) {
        this.lasttime_after_diameter = lasttime_after_diameter;
    }

    public Double getBefore_diameter() {
        return this.before_diameter;
    }

    public void setBefore_diameter(Double before_diameter) {
        this.before_diameter = before_diameter;
    }

    public Double getAfter_diameter() {
        return this.after_diameter;
    }

    public void setAfter_diameter(Double after_diameter) {
        this.after_diameter = after_diameter;
    }

    public Double getOnline_wear() {
        return this.online_wear;
    }

    public void setOnline_wear(Double online_wear) {
        this.online_wear = online_wear;
    }

    public Double getFirst_wear() {
        return this.first_wear;
    }

    public void setFirst_wear(Double first_wear) {
        this.first_wear = first_wear;
    }

    public Double getSecond_wear() {
        return this.second_wear;
    }

    public void setSecond_wear(Double second_wear) {
        this.second_wear = second_wear;
    }

    public Double getWear() {
        return this.wear;
    }

    public void setWear(Double wear) {
        this.wear = wear;
    }

    public Long getIf_power() {
        return this.if_power;
    }

    public void setIf_power(Long if_power) {
        this.if_power = if_power;
    }

    public Double getResidual_thickness() {
        return this.residual_thickness;
    }

    public void setResidual_thickness(Double residual_thickness) {
        this.residual_thickness = residual_thickness;
    }

    public Long getCount_weartimes() {
        return count_weartimes;
    }

    public void setCount_weartimes(Long count_weartimes) {
        this.count_weartimes = count_weartimes;
    }

    public Double getDeviation() {
        return deviation;
    }

    public void setDeviation(Double deviation) {
        this.deviation = deviation;
    }

    public Double getTaper() {
        return taper;
    }

    public void setTaper(Double taper) {
        this.taper = taper;
    }

    public Double getRoundness() {
        return roundness;
    }

    public void setRoundness(Double roundness) {
        this.roundness = roundness;
    }

    public Double getCrack() {
        return crack;
    }

    public void setCrack(Double crack) {
        this.crack = crack;
    }

    public Double getHidden_flaws() {
        return hidden_flaws;
    }

    public void setHidden_flaws(Double hidden_flaws) {
        this.hidden_flaws = hidden_flaws;
    }

    public Double getQualifiednum() {
        return qualifiednum;
    }

    public void setQualifiednum(Double qualifiednum) {
        this.qualifiednum = qualifiednum;
    }

    public Double getWheel_dia_start() {
        return wheel_dia_start;
    }

    public void setWheel_dia_start(Double wheel_dia_start) {
        this.wheel_dia_start = wheel_dia_start;
    }

    public Double getWheel_dia_end() {
        return wheel_dia_end;
    }

    public void setWheel_dia_end(Double wheel_dia_end) {
        this.wheel_dia_end = wheel_dia_end;
    }

    public String getFrame_noid() {
        return frame_noid;
    }

    public void setFrame_noid(String frame_noid) {
        this.frame_noid = frame_noid;
    }

    public String getFrame_no() {
        return frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public String getMachine_no() {
        return machine_no;
    }

    public void setMachine_no(String machine_no) {
        this.machine_no = machine_no;
    }

    public String getGrind_starttime() {
        return grind_starttime;
    }

    public void setGrind_starttime(String grind_starttime) {
        this.grind_starttime = grind_starttime;
    }

    public String getGrind_endtime() {
        return grind_endtime;
    }

    public void setGrind_endtime(String grind_endtime) {
        this.grind_endtime = grind_endtime;
    }

    public Long getIbz() {
        return ibz;
    }

    public void setIbz(Long ibz) {
        this.ibz = ibz;
    }

    public String getSbz() {
        return sbz;
    }

    public void setSbz(String sbz) {
        this.sbz = sbz;
    }

    public Double getWear_height() {
        return wear_height;
    }

    public void setWear_height(Double wear_height) {
        this.wear_height = wear_height;
    }
}