package com.my.business.modular.rollwear.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollwear.dao.RollWearDao;
import com.my.business.modular.rollwear.entity.JRollWear;
import com.my.business.modular.rollwear.entity.RollWear;
import com.my.business.modular.rollwear.service.RollWearService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * 全生命周期——磨削统计控制器层
 *
 * @author 生成器生成
 * @date 2020-11-19 14:20:54
 */
@RestController
@RequestMapping("/rollWear")
public class RollWearController {

    @Autowired
    private RollWearService rollWearService;
    @Autowired
    private RollWearDao rollWearDao;
    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollWear(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollWearService.insertDataRollWear(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollWearOne(@RequestBody String data) {
        try {
            JRollWear jrollWear = JSON.parseObject(data, JRollWear.class);
            return rollWearService.deleteDataRollWearOne(jrollWear.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollWear jrollWear = JSON.parseObject(data, JRollWear.class);
            return rollWearService.deleteDataRollWearMany(jrollWear.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollWear(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollWearService.updateDataRollWear(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollWearByPage(@RequestBody String data) {
        return rollWearService.findDataRollWearByPage(data);
    }

    /**
     * 导出全生命周期
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/excelFindByPage"}, method = RequestMethod.GET)
    @ResponseBody
    public void excelFindByPage(@RequestParam ( "roll_no" ) String roll_no ,HttpServletResponse response, HttpServletRequest request) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        String fileName = "";//设置要导出的文件的名字
        fileName = rollWearService.excelFindByPage(workbook
                ,roll_no
        );
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1) + ".xls");//URLEncoder.encode(fileName, "utf-8")
        workbook.write(response.getOutputStream());
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollWearByIndocno(@RequestBody String data) {
        return rollWearService.findDataRollWearByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollWear> findDataRollWear() {
        return rollWearService.findDataRollWear();
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/changedata"}, method = RequestMethod.POST)
    @ResponseBody
    public void changedata(@RequestBody String data) {
        RollWear rollWear = JSON.parseObject(data, RollWear.class);
        List<RollWear> list = rollWearDao.find();
        for (RollWear entity:list){
            rollWearService.changedata(entity.getRoll_no());
        }
//        rollWearService.changedata(rollWear.getRoll_no());
    }
}
