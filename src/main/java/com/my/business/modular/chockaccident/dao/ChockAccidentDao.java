package com.my.business.modular.chockaccident.dao;

import com.my.business.modular.chockaccident.entity.ChockAccident;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 事故轴承座管理dao接口
 *
 * @author 生成器生成
 * @date 2020-11-10 09:45:48
 */
@Mapper
public interface ChockAccidentDao {

    /**
     * 添加记录
     *
     * @param chockAccident 对象实体
     */
	void insertDataChockAccident(ChockAccident chockAccident);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
	void deleteDataChockAccidentOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
	void deleteDataChockAccidentMany(String value);

    /**
     * 修改记录
     *
     * @param chockAccident 对象实体
     */
	void updateDataChockAccident(ChockAccident chockAccident);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
	List<ChockAccident> findDataChockAccidentByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("production_line") String production_line,@Param("production_line_id") String production_line_id,@Param("chock_no") String chock_no, @Param("roll_typeid") Long roll_typeid, @Param("framerangeid") Long framerangeid,@Param("iblockade") Long iblockade);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
	Integer findDataChockAccidentByPageSize(@Param("production_line") String production_line,@Param("production_line_id") String production_line_id,@Param("chock_no") String chock_no, @Param("roll_typeid") Long roll_typeid, @Param("framerangeid") Long framerangeid,@Param("iblockade") Long iblockade);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
	ChockAccident findDataChockAccidentByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
	List<ChockAccident> findDataChockAccident();

}
