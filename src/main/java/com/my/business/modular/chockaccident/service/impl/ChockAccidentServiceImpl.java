package com.my.business.modular.chockaccident.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.basechock.dao.BaseChockDao;
import com.my.business.modular.basechock.entity.BaseChock;
import com.my.business.modular.chockaccident.dao.ChockAccidentDao;
import com.my.business.modular.chockaccident.entity.ChockAccident;
import com.my.business.modular.chockaccident.entity.JChockAccident;
import com.my.business.modular.chockaccident.service.ChockAccidentService;
import com.my.business.modular.rollaccident.entity.JRollAccident;
import com.my.business.modular.rollaccident.entity.RollAccident;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 事故轴承座管理接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-11-10 09:45:48
 */
@Service
public class ChockAccidentServiceImpl implements ChockAccidentService {

    @Autowired
    private ChockAccidentDao chockAccidentDao;
    @Autowired
    private BaseChockDao baseChockDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataChockAccident(String data, Long userId, String sname) {
        try {
            JChockAccident jchockAccident = JSON.parseObject(data, JChockAccident.class);
            ChockAccident chockAccident = jchockAccident.getChockAccident();
            CodiUtil.newRecord(userId, sname, chockAccident);
            chockAccidentDao.insertDataChockAccident(chockAccident);

            BaseChock baseChock = baseChockDao.findDataBaseChockByChockNo(chockAccident.getChock_no());
            if (!StringUtils.isEmpty(baseChock)) {
                baseChock.setIstatus(2L);
                baseChock.setStatus("停用");
                baseChockDao.updateDataBaseChock(baseChock);
            }
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataChockAccidentOne(Long indocno) {
        try {
            chockAccidentDao.deleteDataChockAccidentOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataChockAccidentMany(String str_id) {
        try {
            String sql = "delete chock_accident where indocno in(" + str_id + ")";
            chockAccidentDao.deleteDataChockAccidentMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataChockAccident(String data, Long userId, String sname) {
        try {
            JChockAccident jchockAccident = JSON.parseObject(data, JChockAccident.class);
            ChockAccident chockAccident = jchockAccident.getChockAccident();
            CodiUtil.editRecord(userId, sname, chockAccident);
            chockAccidentDao.updateDataChockAccident(chockAccident);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataChockAccidentByPage(String data) {
        try {
            JChockAccident jchockAccident = JSON.parseObject(data, JChockAccident.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jchockAccident.getPageIndex();
            Integer pageSize = jchockAccident.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jchockAccident.getCondition()) {
                jsonObject = JSON.parseObject(jchockAccident.getCondition().toString());
            }

            String chock_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("chock_no"))) {
                chock_no = jsonObject.get("chock_no").toString();
            }

            Long roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = Long.valueOf(jsonObject.get("roll_typeid").toString());
            }

            Long framerangeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("framerangeid"))) {
                framerangeid = Long.valueOf(jsonObject.get("framerangeid").toString());
            }

            Long iblockade = null;
            if (!StringUtils.isEmpty(jsonObject.get("iblockade"))) {
                iblockade = Long.valueOf(jsonObject.get("iblockade").toString());
            }

            String production_line = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line"))) {
                production_line = jsonObject.get("production_line").toString();
            }

            String production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = jsonObject.get("production_line_id").toString();
            }

            List<ChockAccident> list = chockAccidentDao.findDataChockAccidentByPage((pageIndex - 1) * pageSize, pageSize,production_line,production_line_id,chock_no, roll_typeid, framerangeid,iblockade);
            Integer count = chockAccidentDao.findDataChockAccidentByPageSize(production_line,production_line_id,chock_no, roll_typeid, framerangeid,iblockade);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataChockAccidentByIndocno(String data) {
        try {
            JChockAccident jchockAccident = JSON.parseObject(data, JChockAccident.class);
            Long indocno = jchockAccident.getIndocno();

            ChockAccident chockAccident = chockAccidentDao.findDataChockAccidentByIndocno(indocno);
            return ResultData.ResultDataSuccess(chockAccident);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<ChockAccident> findDataChockAccident() {
        List<ChockAccident> list = chockAccidentDao.findDataChockAccident();
        return list;
    }

    /**
     * 报废
     *
     * @param data 参数字符串
     */
    public ResultData scrapData(String data,Long v,String v2) {
        try {
            JChockAccident jChockAccident = JSON.parseObject(data, JChockAccident.class);
            ChockAccident chockAccident = jChockAccident.getChockAccident();
            String chock_no = chockAccident.getChock_no();
            chockAccident.setIblockade(1L);
            chockAccidentDao.updateDataChockAccident(chockAccident);
            BaseChock baseChock = baseChockDao.findDataBaseChockByChockNo(chock_no);
            if (!StringUtils.isEmpty(baseChock)) {
                baseChock.setIstatus(v);
                baseChock.setStatus(v2);
                baseChockDao.updateDataBaseChock(baseChock);
            }
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }
}
