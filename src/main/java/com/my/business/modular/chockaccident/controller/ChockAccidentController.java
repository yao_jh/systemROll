package com.my.business.modular.chockaccident.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.chockaccident.entity.ChockAccident;
import com.my.business.modular.chockaccident.entity.JChockAccident;
import com.my.business.modular.chockaccident.service.ChockAccidentService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 事故轴承座管理控制器层
 *
 * @author 生成器生成
 * @date 2020-11-10 09:45:48
 */
@RestController
@RequestMapping("/chockAccident")
public class ChockAccidentController {

    @Autowired
    private ChockAccidentService chockAccidentService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataChockAccident(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return chockAccidentService.insertDataChockAccident(data, userId, CodiUtil.returnLm(sname));
    }

	/**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataChockAccidentOne(@RequestBody String data) {
        try {
            JChockAccident jchockAccident = JSON.parseObject(data, JChockAccident.class);
            return chockAccidentService.deleteDataChockAccidentOne(jchockAccident.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JChockAccident jchockAccident = JSON.parseObject(data, JChockAccident.class);
            return chockAccidentService.deleteDataChockAccidentMany(jchockAccident.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataChockAccident(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return chockAccidentService.updateDataChockAccident(data, userId, CodiUtil.returnLm(sname));
    }

	/**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataChockAccidentByPage(@RequestBody String data) {
        return chockAccidentService.findDataChockAccidentByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataChockAccidentByIndocno(@RequestBody String data) {
        return chockAccidentService.findDataChockAccidentByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<ChockAccident> findDataChockAccident() {
        return chockAccidentService.findDataChockAccident();
    }

    /**
     * 报废
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/scrap"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteScrap(@RequestBody String data) {
        try {
            return chockAccidentService.scrapData(data,5L,"报废");
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 质量异议
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/mass"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData mass(@RequestBody String data) {
        try {
            return chockAccidentService.scrapData(data,8L,"质量异议");
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 送外修复
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/repair"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData repair(@RequestBody String data) {
        try {
            return chockAccidentService.scrapData(data,6L,"送外修复");
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }


    /**
     * 解锁
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/unlock"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData unlock(@RequestBody String data) {
        try {
            return chockAccidentService.scrapData(data,3L,"待使用");
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }
}
