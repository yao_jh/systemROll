package com.my.business.modular.chockaccident.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 事故轴承座管理实体类
 *
 * @author 生成器生成
 * @date 2020-11-10 09:45:47
 */
public class ChockAccident extends BaseEntity {

    private Long indocno;  //主键
    private String chock_no;  //辊号
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private Long framerangeid;  //机架范围id
    private String framerange;  //机架范围
    private String lockreason;  //封锁原因
    private String locktime;  //封锁时间
    private String unlocktime;  //解锁时间
    private Long operateid;  //操作人id
    private String operatename;  //操作人
    private String operatetime;  //操作时间
    private Long production_line_id;  //产线id
    private String production_line;  //产线
    private Long iblockade;  //封锁座标记

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getChock_no() {
        return chock_no;
    }

    public void setChock_no(String chock_no) {
        this.chock_no = chock_no;
    }

    public Long getRoll_typeid() {
        return this.roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public Long getFramerangeid() {
        return this.framerangeid;
    }

    public void setFramerangeid(Long framerangeid) {
        this.framerangeid = framerangeid;
    }

    public String getFramerange() {
        return this.framerange;
    }

    public void setFramerange(String framerange) {
        this.framerange = framerange;
    }

    public String getLockreason() {
        return this.lockreason;
    }

    public void setLockreason(String lockreason) {
        this.lockreason = lockreason;
    }

    public String getLocktime() {
        return this.locktime;
    }

    public void setLocktime(String locktime) {
        this.locktime = locktime;
    }

    public String getUnlocktime() {
        return this.unlocktime;
    }

    public void setUnlocktime(String unlocktime) {
        this.unlocktime = unlocktime;
    }

    public Long getOperateid() {
        return this.operateid;
    }

    public void setOperateid(Long operateid) {
        this.operateid = operateid;
    }

    public String getOperatename() {
        return this.operatename;
    }

    public void setOperatename(String operatename) {
        this.operatename = operatename;
    }

    public String getOperatetime() {
        return this.operatetime;
    }

    public void setOperatetime(String operatetime) {
        this.operatetime = operatetime;
    }

    public Long getProduction_line_id() {
        return this.production_line_id;
    }

    public void setProduction_line_id(Long production_line_id) {
        this.production_line_id = production_line_id;
    }

    public String getProduction_line() {
        return this.production_line;
    }

    public void setProduction_line(String production_line) {
        this.production_line = production_line;
    }

    public Long getIblockade() {
        return iblockade;
    }

    public void setIblockade(Long iblockade) {
        this.iblockade = iblockade;
    }
}