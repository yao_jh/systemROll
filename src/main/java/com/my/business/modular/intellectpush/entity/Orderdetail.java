package com.my.business.modular.intellectpush.entity;

/***
 * 工单详情实体类,只用于工单详情查询使用
 * @author cc
 *
 */
public class Orderdetail {
    private String order_text;  //工单详情中的文字
    private String order_standard;  //工单中标准值
    private String order_measure;  //工单中测量值
    private Integer order_result;  //是否合格
    private String order_unit;  //工单中的单位


    public Orderdetail() {
    }

    public Orderdetail(String order_text, String order_standard, String order_measure, Integer order_result,
                       String order_unit) {
        super();
        this.order_text = order_text;
        this.order_standard = order_standard;
        this.order_measure = order_measure;
        this.order_result = order_result;
        this.order_unit = order_unit;
    }

    public String getOrder_text() {
        return order_text;
    }

    public void setOrder_text(String order_text) {
        this.order_text = order_text;
    }

    public String getOrder_standard() {
        return order_standard;
    }

    public void setOrder_standard(String order_standard) {
        this.order_standard = order_standard;
    }

    public String getOrder_measure() {
        return order_measure;
    }

    public void setOrder_measure(String order_measure) {
        this.order_measure = order_measure;
    }

    public Integer getOrder_result() {
        return order_result;
    }

    public void setOrder_result(Integer order_result) {
        this.order_result = order_result;
    }

    public String getOrder_unit() {
        return order_unit;
    }

    public void setOrder_unit(String order_unit) {
        this.order_unit = order_unit;
    }
}
