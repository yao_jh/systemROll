package com.my.business.modular.intellectpush.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.intellectpush.service.IntellectService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/***
 * 智能推送系统控制器
 * @author cc
 *
 */
@RestController
@RequestMapping("/intellect")
public class IntellectController {

    @Autowired
    private IntellectService intellectService;


    @CrossOrigin
    @RequestMapping(value = {"/find"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData find(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return intellectService.intellectFind(userId, sname);
    }

    @CrossOrigin
    @RequestMapping(value = {"/orderfind"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData orderfind(@RequestBody String data) {
        JCommon common = JSON.parseObject(data, JCommon.class);
        String order_no = common.getModular();
        return intellectService.orderFind(order_no);
    }

    @CrossOrigin
    @RequestMapping(value = {"/xg"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData xg(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        Long userId = common.getUserId();
        return intellectService.findMisson(1L, data, userId);
    }

    @CrossOrigin
    @RequestMapping(value = {"/lq"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData lq(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        Long userId = common.getUserId();
        return intellectService.findMisson(2L, data, userId);
    }

    @CrossOrigin
    @RequestMapping(value = {"/mx"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData mx(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        Long userId = common.getUserId();
        return intellectService.findMisson(3L, data, userId);
    }

    @CrossOrigin
    @RequestMapping(value = {"/other"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData other(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        Long userId = common.getUserId();
        return intellectService.findMisson(4L, data, userId);
    }

    @CrossOrigin
    @RequestMapping(value = {"/ifinish"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData ifinish(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        Long userId = common.getUserId();
        return intellectService.findMisson(5L, data, userId);
    }

    @CrossOrigin
    @RequestMapping(value = {"/mx_order"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData mxorder(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        Long userId = common.getUserId();
        return intellectService.findMisson(6L, data, userId);
    }
    
    @CrossOrigin
    @RequestMapping(value = {"/gr"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData gr(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        Long userId = common.getUserId();
        return intellectService.findMisson(7L, data, userId);
    }
    
    @CrossOrigin
    @RequestMapping(value = {"/wn"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData wn(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        Long userId = common.getUserId();
        return intellectService.findMisson(8L, data, userId);
    }
    
    @CrossOrigin
    @RequestMapping(value = {"/wn_order"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData wn_order(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        Long userId = common.getUserId();
        return intellectService.findMisson(9L, data, userId);
    }
}
