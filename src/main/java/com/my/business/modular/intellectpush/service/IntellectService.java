package com.my.business.modular.intellectpush.service;

import com.my.business.sys.common.entity.ResultData;

public interface IntellectService {


    /**
     * 智能推送系统查询推送消息接口
     *
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData intellectFind(Long userId, String sname);

    /**
     * 智能推送系统查询工单详情接口
     */
    ResultData orderFind(String order_no);

    /**
     * 智能推送系统主页待办事项与已办事项接口
     *
     * @param v      接口序号，根据调用的不同接口，使用不同的查询语句
     * @param data   前台传递的数据结构
     * @param userId 登陆人ID
     * @return 查询结果
     */
    ResultData findMisson(Long v, String data, Long userId);
}
