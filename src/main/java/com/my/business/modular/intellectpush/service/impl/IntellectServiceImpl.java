package com.my.business.modular.intellectpush.service.impl;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.intellectpush.service.IntellectService;
import com.my.business.modular.rolldimensionorder.dao.RollDimensionorderDao;
import com.my.business.modular.rollorderconfigdetail.dao.RollOrderconfigdetailDao;
import com.my.business.modular.rollorderconfigdetail.entity.RollOrderconfigdetail;
import com.my.business.modular.rollpushnew.dao.RollPushnewDao;
import com.my.business.modular.rollpushnew.entity.JRollPushnew;
import com.my.business.modular.rollpushnew.entity.RollPushnew;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.role.dao.SysRoleDao;
import com.my.business.workflow.workparam.dao.WorkParamDao;
import com.my.business.workflow.workparam.entity.WorkParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IntellectServiceImpl implements IntellectService {

    @Autowired
    private RollPushnewDao rollPushnewDao;

    @Autowired
    private RollDimensionorderDao rollDimensionorderDao;

    @Autowired
    private RollOrderconfigdetailDao rollOrderconfigdetailDao;

    @Autowired
    private WorkParamDao workParamDao;
    @Autowired
    private SysRoleDao sysRoleDao;

    /**
     * 智能推送系统查询推送消息接口
     *
     * @param order_no 工单编码
     */
    @Override
    public ResultData orderFind(String order_no) {
        try {
            List<RollOrderconfigdetail> detail_entity = rollOrderconfigdetailDao.findDataRollOrderconfigdetailByOrderNo(order_no);
            return ResultData.ResultDataSuccessSelf("查询成功", detail_entity);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 智能推送系统查询推送消息接口
     *
     * @param userId 用户id
     * @param sname  用户姓名
     */
    @Override
    public ResultData intellectFind(Long userId, String sname) {
        try {
            List<RollPushnew> list = rollPushnewDao.findDataRollPushnewByUserId(userId);
            for (RollPushnew entity : list) {
//            	if(!StringUtils.isEmpty(entity.getOrder_no())) {
//            		List<RollOrderconfigdetail> detail_entity = rollOrderconfigdetailDao.findDataRollOrderconfigdetailByOrderNo(entity.getOrder_no());
//                	entity.setOrder_detailList(detail_entity);
//            	}

                List<WorkParam> paramList = workParamDao.findDataWorkParamAll(entity.getFlowid());
                if (paramList != null && paramList.size() > 0) {
                    Map<String, String> map = new HashMap<String, String>();
                    for (WorkParam p : paramList) {
                        map.put(p.getField_no(), "");
                    }
                    entity.setWorkparam_map(map);
                }
            }
            return ResultData.ResultDataSuccessSelf("查询成功", list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    @Override
    public ResultData findMisson(Long v, String data, Long userId) {
        try {
            JRollPushnew jrollPushnew = JSON.parseObject(data, JRollPushnew.class);

            List<RollPushnew> list = new ArrayList<>();
            String modular_no = null;//模块编码
            Long ifinish = 0L;//0是未完成 1是已完成
            Long machine_no = null;
            if (v == 1L) {
                modular_no = "xg";
                list = rollPushnewDao.findDataRollPushnewByModularNo(modular_no, ifinish, machine_no, userId);
            } else if (v == 2L) {
                modular_no = "lq";
                list = rollPushnewDao.findDataRollPushnewByModularNo(modular_no, ifinish, machine_no, userId);
            } else if (v == 3L) {
                modular_no = "mx";
                String role_ids = jrollPushnew.getRole_ids();
                if (!StringUtils.isEmpty(role_ids)) {
                    String[] arrays = role_ids.split(",");
                    if (arrays.length != 0) {
                    	for(String roleid : arrays) {
                    		List<RollPushnew> ls = rollPushnewDao.findDataRollPushnewByModularNoOnetoFour(modular_no, ifinish,Long.valueOf(roleid));
                    		if (ls != null && ls.size() > 0) {
                                list.addAll(ls);
                            }
                    	}
//                        for (int i = 0; i < arrays.length; i++) {
//                            if (!StringUtils.isEmpty(arrays[i])) {
////                                List<RollPushnew> ls = new ArrayList<>();
////                                if (Long.parseLong(arrays[i]) == 11L) {
//////                                    ls = rollPushnewDao.findDataRollPushnewByModularNo(modular_no, ifinish, null, null);
////                                	  ls = rollPushnewDao.findDataRollPushnewByModularNoOnetoFour(modular_no, ifinish);
////                                }
//                            	List<RollPushnew> ls = rollPushnewDao.findDataRollPushnewByModularNoOnetoFour(modular_no, ifinish,Long.valueOf(arrays[i]));
//                                if (!StringUtils.isEmpty(ls)) {
//                                    list.addAll(ls);
//                                }
//                            }
//                        }
                    }
                }
            } else if (v == 4L) {
                modular_no = "other";
                list = rollPushnewDao.findDataRollPushnewByModularNo(modular_no, ifinish, machine_no, userId);
            } else if (v == 5L) {
                ifinish = 1L;
                List<RollPushnew> list1 = rollPushnewDao.findDataRollPushnewByModularNo("xg", ifinish, machine_no, userId);
                List<RollPushnew> list2 = rollPushnewDao.findDataRollPushnewByModularNo("lq", ifinish, machine_no, userId);
                List<RollPushnew> list3 = rollPushnewDao.findDataRollPushnewByModularNo("other", ifinish, machine_no, userId);
                List<RollPushnew> list4 = new ArrayList<>();
                String role_ids = jrollPushnew.getRole_ids();
                if (!StringUtils.isEmpty(role_ids)) {
                    String[] arrays = role_ids.split(",");
                    if (arrays.length != 0) {
                        for (int i = 0; i < arrays.length; i++) {
                            if (!StringUtils.isEmpty(arrays[i])) {
                                List<RollPushnew> ls1 = new ArrayList<>();
                                List<RollPushnew> ls2 = new ArrayList<>();
                                if (Long.parseLong(arrays[i]) == 11L) {
                                    ls1 = rollPushnewDao.findDataRollPushnewByModularNo("mx", ifinish, null, null);
                                    ls2 = rollPushnewDao.findDataRollPushnewByModularNo("mx_order", ifinish, null, null);
                                } else {
                                    Long machineids = sysRoleDao.findMachine(Long.valueOf(arrays[i]));
                                    ls1 = rollPushnewDao.findDataRollPushnewByModularNo("mx", ifinish, machineids, userId);
                                    ls2 = rollPushnewDao.findDataRollPushnewByModularNo("mx_order", ifinish, machineids, userId);
                                }
                                if (!StringUtils.isEmpty(ls1)) {
                                    list4.addAll(ls1);
                                }
                                if (!StringUtils.isEmpty(ls2)) {
                                    list4.addAll(ls2);
                                }
                            }
                        }
                    }
                }
                List<RollPushnew> list5 = rollPushnewDao.findDataRollPushnewByModularNo("gr", ifinish, machine_no, userId);
                list.addAll(list1);
                list.addAll(list2);
                list.addAll(list3);
                list.addAll(list4);
                list.addAll(list5);

            } else if (v == 6L) {
                modular_no = "mx_order";
                String role_ids = jrollPushnew.getRole_ids();
                if (!StringUtils.isEmpty(role_ids)) {
                    String[] arrays = role_ids.split(",");
                    if (arrays.length != 0) {
                        for (int i = 0; i < arrays.length; i++) {
                            if (!StringUtils.isEmpty(arrays[i])) {
                                List<RollPushnew> ls = new ArrayList<>();
                                if (Long.parseLong(arrays[i]) == 11L) {
                                    ls = rollPushnewDao.findDataRollPushnewByModularNo(modular_no, ifinish, null, null);
                                }
                                if (!StringUtils.isEmpty(ls)) {
                                    list.addAll(ls);
                                }
                            }
                        }
                    }
                }
            }else if (v == 7L) {
                modular_no = "gr";
                list = rollPushnewDao.findDataRollPushnewByModularNo(modular_no, ifinish, machine_no, userId);
            }else if(v == 8L) {
            	modular_no = "mx";
                String role_ids = jrollPushnew.getRole_ids();
                if (!StringUtils.isEmpty(role_ids)) {
                    String[] arrays = role_ids.split(",");
                    if (arrays.length != 0) {
                        for (int i = 0; i < arrays.length; i++) {
                            if (!StringUtils.isEmpty(arrays[i])) {
//                                List<RollPushnew> ls = new ArrayList<>();
//                                if (Long.parseLong(arrays[i]) == 29L) {
//                                    ls = rollPushnewDao.findDataRollPushnewByModularNoWn(modular_no, ifinish);
//                                }
                            	List<RollPushnew> ls = rollPushnewDao.findDataRollPushnewByModularNoWn(modular_no, ifinish,Long.valueOf(arrays[i]));
                                if (!StringUtils.isEmpty(ls)) {
                                    list.addAll(ls);
                                }
                            }
                        }
                    }
                }
            }else if(v == 9L) {
            	modular_no = "mx_order";
                String role_ids = jrollPushnew.getRole_ids();
                if (!StringUtils.isEmpty(role_ids)) {
                    String[] arrays = role_ids.split(",");
                    if (arrays.length != 0) {
                        for (int i = 0; i < arrays.length; i++) {
                            if (!StringUtils.isEmpty(arrays[i])) {
                                List<RollPushnew> ls = new ArrayList<>();
                                if (Long.parseLong(arrays[i]) == 29L) {
                                    ls = rollPushnewDao.findDataRollPushnewByModularNoWn(modular_no, ifinish,null);
                                }
                                if (!StringUtils.isEmpty(ls)) {
                                    list.addAll(ls);
                                }
                            }
                        }
                    }
                }
            }

            for (RollPushnew entity : list) {
//            	if(!StringUtils.isEmpty(entity.getOrder_no())) {
//            		List<RollOrderconfigdetail> detail_entity = rollOrderconfigdetailDao.findDataRollOrderconfigdetailByOrderNo(entity.getOrder_no());
//                	entity.setOrder_detailList(detail_entity);
//            	}

                List<WorkParam> paramList = workParamDao.findDataWorkParamAll(entity.getFlowid());
                if (paramList != null && paramList.size() > 0) {
                    Map<String, String> map = new HashMap<String, String>();
                    for (WorkParam p : paramList) {
                        map.put(p.getField_no(), "");
                    }
                    entity.setWorkparam_map(map);
                }
            }
            for (int i = 0; i < list.size() - 1; i++) {
                for (int j = list.size() - 1; j > i; j--) {
                    if (list.get(j).getIndocno().equals(list.get(i).getIndocno())) {
                        list.remove(j);
                    }
                }
            }
            return ResultData.ResultDataSuccessSelf("查询成功", list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

}
