package com.my.business.modular.datafiles.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.util.Date;

/**
 * 数据文件展示实体类
 *
 * @author 生成器生成
 * @date 2020-08-26 11:13:14
 */
public class Datafiles extends BaseEntity {

    private Long indocno;  //主键
    private Long grinder;  //磨床号
    private Date initial_date_time;  //开始时间

    private String first_theopro;  //磨前标准曲线
    private String first_realpro;  //磨前辊形曲线
    private String final_theopro;  //磨后标准曲线
    private String final_realpro;  //磨后辊形曲线
    private String roundness;  //圆度曲线
    private String endinspekt;  //探伤图

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getGrinder() {
        return this.grinder;
    }

    public void setGrinder(Long grinder) {
        this.grinder = grinder;
    }

    public Date getInitial_date_time() {
        return this.initial_date_time;
    }

    public void setInitial_date_time(Date initial_date_time) {
        this.initial_date_time = initial_date_time;
    }

    public String getFirst_theopro() {
        return this.first_theopro;
    }

    public void setFirst_theopro(String first_theopro) {
        this.first_theopro = first_theopro;
    }

    public String getFirst_realpro() {
        return this.first_realpro;
    }

    public void setFirst_realpro(String first_realpro) {
        this.first_realpro = first_realpro;
    }

    public String getFinal_theopro() {
        return this.final_theopro;
    }

    public void setFinal_theopro(String final_theopro) {
        this.final_theopro = final_theopro;
    }

    public String getFinal_realpro() {
        return this.final_realpro;
    }

    public void setFinal_realpro(String final_realpro) {
        this.final_realpro = final_realpro;
    }

    public String getRoundness() {
        return this.roundness;
    }

    public void setRoundness(String roundness) {
        this.roundness = roundness;
    }

    public String getEndinspekt() {
        return this.endinspekt;
    }

    public void setEndinspekt(String endinspekt) {
        this.endinspekt = endinspekt;
    }


}