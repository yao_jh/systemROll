package com.my.business.modular.datafiles.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.datafiles.dao.DatafilesDao;
import com.my.business.modular.datafiles.entity.Datafiles;
import com.my.business.modular.datafiles.entity.JDatafiles;
import com.my.business.modular.datafiles.service.DatafilesService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 数据文件展示接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-26 11:13:14
 */
@Service
public class DatafilesServiceImpl implements DatafilesService {

    @Autowired
    private DatafilesDao datafilesDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataDatafiles(String data, Long userId, String sname) {
        try {
            JDatafiles jdatafiles = JSON.parseObject(data, JDatafiles.class);
            Datafiles datafiles = jdatafiles.getDatafiles();
            CodiUtil.newRecord(userId, sname, datafiles);
            datafilesDao.insertDataDatafiles(datafiles);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataDatafilesOne(Long indocno) {
        try {
            datafilesDao.deleteDataDatafilesOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataDatafilesMany(String str_id) {
        try {
            String sql = "delete datafiles where indocno in(" + str_id + ")";
            datafilesDao.deleteDataDatafilesMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataDatafiles(String data, Long userId, String sname) {
        try {
            JDatafiles jdatafiles = JSON.parseObject(data, JDatafiles.class);
            Datafiles datafiles = jdatafiles.getDatafiles();
            CodiUtil.editRecord(userId, sname, datafiles);
            datafilesDao.updateDataDatafiles(datafiles);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataDatafilesByPage(String data) {
        try {
            JDatafiles jdatafiles = JSON.parseObject(data, JDatafiles.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jdatafiles.getPageIndex();
            Integer pageSize = jdatafiles.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jdatafiles.getCondition()) {
                jsonObject = JSON.parseObject(jdatafiles.getCondition().toString());
            }

            List<Datafiles> list = datafilesDao.findDataDatafilesByPage((pageIndex - 1) * pageSize, pageSize);
            Integer count = datafilesDao.findDataDatafilesByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataDatafilesByIndocno(String data) {
        try {
            JDatafiles jdatafiles = JSON.parseObject(data, JDatafiles.class);
            Long indocno = jdatafiles.getIndocno();

            Datafiles datafiles = datafilesDao.findDataDatafilesByIndocno(indocno);
            return ResultData.ResultDataSuccess(datafiles);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<Datafiles> findDataDatafiles() {
        List<Datafiles> list = datafilesDao.findDataDatafiles();
        return list;
    }

}
