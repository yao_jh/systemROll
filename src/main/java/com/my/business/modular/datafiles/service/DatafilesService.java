package com.my.business.modular.datafiles.service;

import com.my.business.modular.datafiles.entity.Datafiles;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 数据文件展示接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-26 11:13:14
 */
public interface DatafilesService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataDatafiles(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataDatafilesOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataDatafilesMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataDatafiles(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataDatafilesByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataDatafilesByIndocno(String data);

    /**
     * 查看记录
     */
    List<Datafiles> findDataDatafiles();
}
