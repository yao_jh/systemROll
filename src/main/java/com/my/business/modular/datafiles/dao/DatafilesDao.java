package com.my.business.modular.datafiles.dao;

import com.my.business.modular.datafiles.entity.Datafiles;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 数据文件展示dao接口
 *
 * @author 生成器生成
 * @date 2020-08-26 11:13:14
 */
@Mapper
public interface DatafilesDao {

    /**
     * 添加记录
     *
     * @param datafiles 对象实体
     */
    void insertDataDatafiles(Datafiles datafiles);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataDatafilesOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataDatafilesMany(String value);

    /**
     * 修改记录
     *
     * @param datafiles 对象实体
     */
    void updateDataDatafiles(Datafiles datafiles);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<Datafiles> findDataDatafilesByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataDatafilesByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    Datafiles findDataDatafilesByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<Datafiles> findDataDatafiles();

}
