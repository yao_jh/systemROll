package com.my.business.modular.grindforecast.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.modular.grindforecast.entity.GrindForecast;
import java.util.List;

/**
* 磨削预测表接口服务类
* @author  生成器生成
* @date 2020-11-08 10:13:18
*/
public interface GrindForecastService {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataGrindForecast(String data,Long userId,String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataGrindForecastOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataGrindForecastMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataGrindForecast(String data,Long userId,String sname);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataGrindForecastByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataGrindForecastByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<GrindForecast> findDataGrindForecast();
}
