package com.my.business.modular.grindforecast.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.grindforecast.entity.GrindForecast;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 磨削预测表dao接口
 * @author  生成器生成
 * @date 2020-11-08 10:13:18
 * */
@Mapper
public interface GrindForecastDao {

	/**
	 * 添加记录
	 * @param grindForecast  对象实体
	 * */
	public void insertDataGrindForecast(GrindForecast grindForecast);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataGrindForecastOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataGrindForecastMany(String value);
	
	/**
	 * 修改记录
	 * @param grindForecast  对象实体
	 * */
	public void updateDataGrindForecast(GrindForecast grindForecast);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<GrindForecast> findDataGrindForecastByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataGrindForecastByPageSize();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public GrindForecast findDataGrindForecastByIndocno(@Param("indocno") Long indocno);
    
    /***
     * 根据轧辊类型和位置和机架号查询数据
     * @param roll_typeid 类型id
     * @param roll_positionid 位置id
     * @param frame_noid 机架号id
     * @return
     */
    public GrindForecast findDataGrindForecastByState(@Param("roll_typeid") Long roll_typeid,@Param("roll_positionid") Long roll_positionid,@Param("frame_noid") Long frame_noid);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<GrindForecast> findDataGrindForecast();
	
}
