package com.my.business.modular.grindforecast.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.grindforecast.entity.GrindForecast;
import com.my.business.modular.grindforecast.entity.JGrindForecast;
import com.my.business.modular.grindforecast.dao.GrindForecastDao;
import com.my.business.modular.grindforecast.service.GrindForecastService;
import com.my.business.sys.common.entity.ResultData;

/**
* 磨削预测表接口具体实现类
* @author  生成器生成
* @date 2020-11-08 10:13:18
*/
@Service
public class GrindForecastServiceImpl implements GrindForecastService {
	
	@Autowired
	private GrindForecastDao grindForecastDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataGrindForecast(String data,Long userId,String sname){
		try{
			JGrindForecast jgrindForecast = JSON.parseObject(data,JGrindForecast.class);
    		GrindForecast grindForecast = jgrindForecast.getGrindForecast();
    		CodiUtil.newRecord(userId,sname,grindForecast);
            grindForecastDao.insertDataGrindForecast(grindForecast);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataGrindForecastOne(Long indocno){
		try {
            grindForecastDao.deleteDataGrindForecastOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataGrindForecastMany(String str_id) {
        try {
        	String sql = "delete from grind_forecast where indocno in(" + str_id +")";
            grindForecastDao.deleteDataGrindForecastMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataGrindForecast(String data,Long userId,String sname){
		try {
			JGrindForecast jgrindForecast = JSON.parseObject(data,JGrindForecast.class);
    		GrindForecast grindForecast = jgrindForecast.getGrindForecast();
        	CodiUtil.editRecord(userId,sname,grindForecast);
            grindForecastDao.updateDataGrindForecast(grindForecast);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataGrindForecastByPage(String data) {
        try {
        	JGrindForecast jgrindForecast = JSON.parseObject(data, JGrindForecast.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jgrindForecast.getPageIndex();
        	Integer pageSize = jgrindForecast.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jgrindForecast.getCondition()){
    			jsonObject  = JSON.parseObject(jgrindForecast.getCondition().toString());
    		}
    
    		List<GrindForecast> list = grindForecastDao.findDataGrindForecastByPage((pageIndex-1)*pageSize, pageSize);
    		Integer count = grindForecastDao.findDataGrindForecastByPageSize();
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataGrindForecastByIndocno(String data) {
        try {
        	JGrindForecast jgrindForecast = JSON.parseObject(data, JGrindForecast.class); 
        	Long indocno = jgrindForecast.getIndocno();
        	
    		GrindForecast grindForecast = grindForecastDao.findDataGrindForecastByIndocno(indocno);
    		return ResultData.ResultDataSuccess(grindForecast);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<GrindForecast> findDataGrindForecast(){
		List<GrindForecast> list = grindForecastDao.findDataGrindForecast();
		return list;
	};
}
