package com.my.business.modular.grindforecast.entity;

import java.util.Date;
import com.my.business.sys.common.entity.BaseEntity;

/**
* 磨削预测表实体类
* @author  生成器生成
* @date 2020-11-08 10:13:18
*/
public class GrindForecast extends BaseEntity{
		
	private Long indocno;  //主键
	private Long roll_typeid;  //轧辊类型id
	private String roll_type;  //轧辊类型
	private Long roll_positionid;  //轧辊位置id
	private String roll_position;  //轧辊位置
	private Long frame_noid;  //机架号id
	private String frame_no;  //机架号
	private Double k_value;  //预测值
    
    
	public void setIndocno(Long indocno){
	    this.indocno = indocno;
	}
	public Long getIndocno(){
	    return this.indocno;
	}
	public void setRoll_typeid(Long roll_typeid){
	    this.roll_typeid = roll_typeid;
	}
	public Long getRoll_typeid(){
	    return this.roll_typeid;
	}
	public void setRoll_type(String roll_type){
	    this.roll_type = roll_type;
	}
	public String getRoll_type(){
	    return this.roll_type;
	}
	public void setRoll_positionid(Long roll_positionid){
	    this.roll_positionid = roll_positionid;
	}
	public Long getRoll_positionid(){
	    return this.roll_positionid;
	}
	public void setRoll_position(String roll_position){
	    this.roll_position = roll_position;
	}
	public String getRoll_position(){
	    return this.roll_position;
	}
	public void setFrame_noid(Long frame_noid){
	    this.frame_noid = frame_noid;
	}
	public Long getFrame_noid(){
	    return this.frame_noid;
	}
	public void setFrame_no(String frame_no){
	    this.frame_no = frame_no;
	}
	public String getFrame_no(){
	    return this.frame_no;
	}
	public void setK_value(Double k_value){
	    this.k_value = k_value;
	}
	public Double getK_value(){
	    return this.k_value;
	}

    
}