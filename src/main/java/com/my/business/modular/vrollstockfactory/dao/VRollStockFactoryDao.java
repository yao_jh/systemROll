package com.my.business.modular.vrollstockfactory.dao;

import com.my.business.modular.vrollstockfactory.entity.VRollStockFactory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轧辊仓库管理——厂商dao接口
 *
 * @author 生成器生成
 * @date 2020-08-14 10:17:16
 */
@Mapper
public interface VRollStockFactoryDao {

    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<VRollStockFactory> findDataVRollStockFactoryByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataVRollStockFactoryByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    VRollStockFactory findDataVRollStockFactoryByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<VRollStockFactory> findDataVRollStockFactory();

}
