package com.my.business.modular.vrollstockfactory.service;

import com.my.business.modular.vrollstockfactory.entity.VRollStockFactory;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 轧辊仓库管理——厂商接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-14 10:17:16
 */
public interface VRollStockFactoryService {

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataVRollStockFactoryByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataVRollStockFactoryByIndocno(String data);

    /**
     * 查看记录
     */
    List<VRollStockFactory> findDataVRollStockFactory();
}
