package com.my.business.modular.vrollstockfactory.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.vrollstockfactory.dao.VRollStockFactoryDao;
import com.my.business.modular.vrollstockfactory.entity.JVRollStockFactory;
import com.my.business.modular.vrollstockfactory.entity.VRollStockFactory;
import com.my.business.modular.vrollstockfactory.service.VRollStockFactoryService;
import com.my.business.sys.common.entity.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 轧辊仓库管理——厂商接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-14 10:17:16
 */
@Service
public class VRollStockFactoryServiceImpl implements VRollStockFactoryService {

    @Autowired
    private VRollStockFactoryDao vRollStockFactoryDao;

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataVRollStockFactoryByPage(String data) {
        try {
            JVRollStockFactory jvRollStockFactory = JSON.parseObject(data, JVRollStockFactory.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jvRollStockFactory.getPageIndex();
            Integer pageSize = jvRollStockFactory.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jvRollStockFactory.getCondition()) {
                jsonObject = JSON.parseObject(jvRollStockFactory.getCondition().toString());
            }

            List<VRollStockFactory> list = vRollStockFactoryDao.findDataVRollStockFactoryByPage((pageIndex - 1) * pageSize, pageSize);
            Integer count = vRollStockFactoryDao.findDataVRollStockFactoryByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataVRollStockFactoryByIndocno(String data) {
        try {
            JVRollStockFactory jvRollStockFactory = JSON.parseObject(data, JVRollStockFactory.class);
            Long indocno = jvRollStockFactory.getIndocno();

            VRollStockFactory vRollStockFactory = vRollStockFactoryDao.findDataVRollStockFactoryByIndocno(indocno);
            return ResultData.ResultDataSuccess(vRollStockFactory);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<VRollStockFactory> findDataVRollStockFactory() {
        List<VRollStockFactory> list = vRollStockFactoryDao.findDataVRollStockFactory();
        return list;
    }

}
