package com.my.business.modular.vrollstockfactory.controller;

import com.my.business.modular.vrollstockfactory.entity.VRollStockFactory;
import com.my.business.modular.vrollstockfactory.service.VRollStockFactoryService;
import com.my.business.sys.common.entity.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 轧辊仓库管理——厂商控制器层
 *
 * @author 生成器生成
 * @date 2020-08-14 10:17:16
 */
@RestController
@RequestMapping("/vRollStockFactory")
public class VRollStockFactoryController {

    @Autowired
    private VRollStockFactoryService vRollStockFactoryService;

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataVRollStockFactoryByPage(@RequestBody String data) {
        return vRollStockFactoryService.findDataVRollStockFactoryByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataVRollStockFactoryByIndocno(@RequestBody String data) {
        return vRollStockFactoryService.findDataVRollStockFactoryByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<VRollStockFactory> findDataVRollStockFactory() {
        return vRollStockFactoryService.findDataVRollStockFactory();
    }

}
