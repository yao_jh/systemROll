package com.my.business.modular.vrollstockfactory.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轧辊仓库管理——厂商实体类
 *
 * @author 生成器生成
 * @date 2020-08-14 10:17:15
 */
public class VRollStockFactory extends BaseEntity {

    private String factory;  //生产厂家
    private Long on_the_way;  //
    private Long not_disassembly;  //
    private Long awaiting_use;  //
    private Long turnover;  //
    private Long repaired;  //
    private Long discontinuation;  //
    private Long scrap;  //

    public String getFactory() {
        return this.factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public Long getOn_the_way() {
        return this.on_the_way;
    }

    public void setOn_the_way(Long on_the_way) {
        this.on_the_way = on_the_way;
    }

    public Long getNot_disassembly() {
        return this.not_disassembly;
    }

    public void setNot_disassembly(Long not_disassembly) {
        this.not_disassembly = not_disassembly;
    }

    public Long getAwaiting_use() {
        return this.awaiting_use;
    }

    public void setAwaiting_use(Long awaiting_use) {
        this.awaiting_use = awaiting_use;
    }

    public Long getTurnover() {
        return this.turnover;
    }

    public void setTurnover(Long turnover) {
        this.turnover = turnover;
    }

    public Long getRepaired() {
        return this.repaired;
    }

    public void setRepaired(Long repaired) {
        this.repaired = repaired;
    }

    public Long getDiscontinuation() {
        return this.discontinuation;
    }

    public void setDiscontinuation(Long discontinuation) {
        this.discontinuation = discontinuation;
    }

    public Long getScrap() {
        return this.scrap;
    }

    public void setScrap(Long scrap) {
        this.scrap = scrap;
    }


}