package com.my.business.modular.rollpartshistory.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollpartshistory.entity.JRollPartsHistory;
import com.my.business.modular.rollpartshistory.entity.RollPartsHistory;
import com.my.business.modular.rollpartshistory.service.RollPartsHistoryService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 设备备件管理历史记录控制器层
 *
 * @author 生成器生成
 * @date 2020-10-15 10:39:40
 */
@RestController
@RequestMapping("/rollPartsHistory")
public class RollPartsHistoryController {

    @Autowired
    private RollPartsHistoryService rollPartsHistoryService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollPartsHistory(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollPartsHistoryService.insertDataRollPartsHistory(data, userId, CodiUtil.returnLm(sname));
    }

	/**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollPartsHistoryOne(@RequestBody String data) {
        try {
            JRollPartsHistory jrollPartsHistory = JSON.parseObject(data, JRollPartsHistory.class);
            return rollPartsHistoryService.deleteDataRollPartsHistoryOne(jrollPartsHistory.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollPartsHistory jrollPartsHistory = JSON.parseObject(data, JRollPartsHistory.class);
            return rollPartsHistoryService.deleteDataRollPartsHistoryMany(jrollPartsHistory.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollPartsHistory(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollPartsHistoryService.updateDataRollPartsHistory(data, userId, CodiUtil.returnLm(sname));
    }

	/**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollPartsHistoryByPage(@RequestBody String data) {
        return rollPartsHistoryService.findDataRollPartsHistoryByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollPartsHistoryByIndocno(@RequestBody String data) {
        return rollPartsHistoryService.findDataRollPartsHistoryByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollPartsHistory> findDataRollPartsHistory() {
        return rollPartsHistoryService.findDataRollPartsHistory();
    }

}
