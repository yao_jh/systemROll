package com.my.business.modular.rollpartshistory.service;

import com.my.business.modular.rollpartshistory.entity.RollPartsHistory;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 设备备件管理历史记录接口服务类
 *
 * @author 生成器生成
 * @date 2020-10-15 10:39:40
 */
public interface RollPartsHistoryService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
	ResultData insertDataRollPartsHistory(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
	ResultData deleteDataRollPartsHistoryOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
	ResultData deleteDataRollPartsHistoryMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
	ResultData updateDataRollPartsHistory(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
	ResultData findDataRollPartsHistoryByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
	ResultData findDataRollPartsHistoryByIndocno(String data);

    /**
     * 查看记录
     */
	List<RollPartsHistory> findDataRollPartsHistory();
}
