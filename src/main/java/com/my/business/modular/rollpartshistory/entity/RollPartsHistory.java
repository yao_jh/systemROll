package com.my.business.modular.rollpartshistory.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 设备备件管理历史记录实体类
 *
 * @author 生成器生成
 * @date 2020-10-15 10:39:40
 */
public class RollPartsHistory extends BaseEntity {

    private Long indocno;  //主键
    private Long ilinkno;  //外键
    private String eq_name;  //备件名称
    private String eq_code;  //备件编码
    private Long production_line_id;  //产线id
    private String production_line;  //产线
    private String save_place;  //存放地点
    private String work_place;  //所辖作业区
    private Long eq_count;  //备件数量
    private Double price_buy;  //购入单价
    private Double price_repair;  //修复单价
    private Long istatus_in;  //入库状态
    private Long istatus_out;  //出库去向
    private Long stock_state;  //库存状态
    private Long ilinkno_check;  //检测工单主键
    private Long eq_in;  //入库数量
    private Long eq_out;  //出库数量
    private Long if_import;  //是否为重要设备
    private String sized;  //规格型号
    private String dbegin;  //时间从
    private String dend;  //时间至

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getIlinkno() {
        return this.ilinkno;
    }

    public void setIlinkno(Long ilinkno) {
        this.ilinkno = ilinkno;
    }

    public String getEq_name() {
        return this.eq_name;
    }

    public void setEq_name(String eq_name) {
        this.eq_name = eq_name;
    }

    public String getEq_code() {
        return this.eq_code;
    }

    public void setEq_code(String eq_code) {
        this.eq_code = eq_code;
    }

    public Long getProduction_line_id() {
        return this.production_line_id;
    }

    public void setProduction_line_id(Long production_line_id) {
        this.production_line_id = production_line_id;
    }

    public String getProduction_line() {
        return this.production_line;
    }

    public void setProduction_line(String production_line) {
        this.production_line = production_line;
    }

    public String getSave_place() {
        return this.save_place;
    }

    public void setSave_place(String save_place) {
        this.save_place = save_place;
    }

    public String getWork_place() {
        return this.work_place;
    }

    public void setWork_place(String work_place) {
        this.work_place = work_place;
    }

    public Long getEq_count() {
        return this.eq_count;
    }

    public void setEq_count(Long eq_count) {
        this.eq_count = eq_count;
    }

    public Double getPrice_buy() {
        return this.price_buy;
    }

    public void setPrice_buy(Double price_buy) {
        this.price_buy = price_buy;
    }

    public Double getPrice_repair() {
        return this.price_repair;
    }

    public void setPrice_repair(Double price_repair) {
        this.price_repair = price_repair;
    }

    public Long getIstatus_in() {
        return this.istatus_in;
    }

    public void setIstatus_in(Long istatus_in) {
        this.istatus_in = istatus_in;
    }

    public Long getIstatus_out() {
        return this.istatus_out;
    }

    public void setIstatus_out(Long istatus_out) {
        this.istatus_out = istatus_out;
    }

    public Long getStock_state() {
        return this.stock_state;
    }

    public void setStock_state(Long stock_state) {
        this.stock_state = stock_state;
    }

    public Long getIlinkno_check() {
        return this.ilinkno_check;
    }

    public void setIlinkno_check(Long ilinkno_check) {
        this.ilinkno_check = ilinkno_check;
    }

    public Long getEq_in() {
        return this.eq_in;
    }

    public void setEq_in(Long eq_in) {
        this.eq_in = eq_in;
    }

    public Long getEq_out() {
        return this.eq_out;
    }

    public void setEq_out(Long eq_out) {
        this.eq_out = eq_out;
    }

    public Long getIf_import() {
        return this.if_import;
    }

    public void setIf_import(Long if_import) {
        this.if_import = if_import;
    }

    public String getSized() {
        return this.sized;
    }

    public void setSized(String sized) {
        this.sized = sized;
    }

    public String getDbegin() {
        return dbegin;
    }

    public void setDbegin(String dbegin) {
        this.dbegin = dbegin;
    }

    public String getDend() {
        return dend;
    }

    public void setDend(String dend) {
        this.dend = dend;
    }
}