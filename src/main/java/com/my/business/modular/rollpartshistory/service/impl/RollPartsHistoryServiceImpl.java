package com.my.business.modular.rollpartshistory.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollpartshistory.dao.RollPartsHistoryDao;
import com.my.business.modular.rollpartshistory.entity.JRollPartsHistory;
import com.my.business.modular.rollpartshistory.entity.RollPartsHistory;
import com.my.business.modular.rollpartshistory.service.RollPartsHistoryService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 设备备件管理历史记录接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-10-15 10:39:40
 */
@Service
public class RollPartsHistoryServiceImpl implements RollPartsHistoryService {

    @Autowired
    private RollPartsHistoryDao rollPartsHistoryDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollPartsHistory(String data, Long userId, String sname) {
        try {
            JRollPartsHistory jrollPartsHistory = JSON.parseObject(data, JRollPartsHistory.class);
            RollPartsHistory rollPartsHistory = jrollPartsHistory.getRollPartsHistory();
            CodiUtil.newRecord(userId, sname, rollPartsHistory);
            rollPartsHistoryDao.insertDataRollPartsHistory(rollPartsHistory);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollPartsHistoryOne(Long indocno) {
        try {
            rollPartsHistoryDao.deleteDataRollPartsHistoryOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollPartsHistoryMany(String str_id) {
        try {
            String sql = "delete roll_parts_history where indocno in(" + str_id + ")";
            rollPartsHistoryDao.deleteDataRollPartsHistoryMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollPartsHistory(String data, Long userId, String sname) {
        try {
            JRollPartsHistory jrollPartsHistory = JSON.parseObject(data, JRollPartsHistory.class);
            RollPartsHistory rollPartsHistory = jrollPartsHistory.getRollPartsHistory();
            CodiUtil.editRecord(userId, sname, rollPartsHistory);
            rollPartsHistoryDao.updateDataRollPartsHistory(rollPartsHistory);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPartsHistoryByPage(String data) {
        try {
            JRollPartsHistory jrollPartsHistory = JSON.parseObject(data, JRollPartsHistory.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollPartsHistory.getPageIndex();
            Integer pageSize = jrollPartsHistory.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollPartsHistory.getCondition()) {
                jsonObject = JSON.parseObject(jrollPartsHistory.getCondition().toString());
            }

            String eq_name = null;
            if (!StringUtils.isEmpty(jsonObject.get("eq_name"))) {
                eq_name = jsonObject.get("eq_name").toString();
            }

            String eq_code = null;
            if (!StringUtils.isEmpty(jsonObject.get("eq_code"))) {
                eq_code = jsonObject.get("eq_code").toString();
            }

            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            List<RollPartsHistory> list = rollPartsHistoryDao.findDataRollPartsHistoryByPage((pageIndex - 1) * pageSize, pageSize,eq_name,eq_code,dbegin,dend);
            Integer count = rollPartsHistoryDao.findDataRollPartsHistoryByPageSize(eq_name,eq_code,dbegin,dend);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPartsHistoryByIndocno(String data) {
        try {
            JRollPartsHistory jrollPartsHistory = JSON.parseObject(data, JRollPartsHistory.class);
            Long indocno = jrollPartsHistory.getIndocno();

            RollPartsHistory rollPartsHistory = rollPartsHistoryDao.findDataRollPartsHistoryByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollPartsHistory);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollPartsHistory> findDataRollPartsHistory() {
        List<RollPartsHistory> list = rollPartsHistoryDao.findDataRollPartsHistory();
        return list;
    }

}
