package com.my.business.modular.rollpartshistory.dao;

import com.my.business.modular.rollpartshistory.entity.RollPartsHistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 设备备件管理历史记录dao接口
 *
 * @author 生成器生成
 * @date 2020-10-15 10:39:40
 */
@Mapper
public interface RollPartsHistoryDao {

    /**
     * 添加记录
     *
     * @param rollPartsHistory 对象实体
     */
    void insertDataRollPartsHistory(RollPartsHistory rollPartsHistory);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollPartsHistoryOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollPartsHistoryMany(String value);

    /**
     * 修改记录
     *
     * @param rollPartsHistory 对象实体
     */
    void updateDataRollPartsHistory(RollPartsHistory rollPartsHistory);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollPartsHistory> findDataRollPartsHistoryByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("eq_name") String eq_name, @Param("eq_code") String eq_code, @Param("dbegin") String dbegin, @Param("dend") String dend);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollPartsHistoryByPageSize(@Param("eq_name") String eq_name, @Param("eq_code") String eq_code, @Param("dbegin") String dbegin, @Param("dend") String dend);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollPartsHistory findDataRollPartsHistoryByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollPartsHistory> findDataRollPartsHistory();

}
