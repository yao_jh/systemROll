package com.my.business.modular.rollticket.service;

import com.my.business.modular.rollticket.entity.RollTicket;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 辊票接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-28 09:34:20
 */
public interface RollTicketService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollTicket(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollTicketOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollTicketMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollTicket(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollTicketByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollTicketByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollTicket> findDataRollTicket();
}
