package com.my.business.modular.rollticket.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollticket.dao.RollTicketDao;
import com.my.business.modular.rollticket.entity.JRollTicket;
import com.my.business.modular.rollticket.entity.RollTicket;
import com.my.business.modular.rollticket.service.RollTicketService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 辊票接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-28 09:34:20
 */
@Service
public class RollTicketServiceImpl implements RollTicketService {

    @Autowired
    private RollTicketDao rollTicketDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollTicket(String data, Long userId, String sname) {
        try {
            JRollTicket jrollTicket = JSON.parseObject(data, JRollTicket.class);
            RollTicket rollTicket = jrollTicket.getRollTicket();
            CodiUtil.newRecord(userId, sname, rollTicket);
            rollTicketDao.insertDataRollTicket(rollTicket);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollTicketOne(Long indocno) {
        try {
            rollTicketDao.deleteDataRollTicketOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollTicketMany(String str_id) {
        try {
            String sql = "delete roll_ticket where indocno in(" + str_id + ")";
            rollTicketDao.deleteDataRollTicketMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollTicket(String data, Long userId, String sname) {
        try {
            JRollTicket jrollTicket = JSON.parseObject(data, JRollTicket.class);
            RollTicket rollTicket = jrollTicket.getRollTicket();
            CodiUtil.editRecord(userId, sname, rollTicket);
            rollTicketDao.updateDataRollTicket(rollTicket);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollTicketByPage(String data) {
        try {
            JRollTicket jrollTicket = JSON.parseObject(data, JRollTicket.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollTicket.getPageIndex();
            Integer pageSize = jrollTicket.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollTicket.getCondition()) {
                jsonObject = JSON.parseObject(jrollTicket.getCondition().toString());
            }

            List<RollTicket> list = rollTicketDao.findDataRollTicketByPage((pageIndex - 1) * pageSize, pageSize);
            Integer count = rollTicketDao.findDataRollTicketByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollTicketByIndocno(String data) {
        try {
            JRollTicket jrollTicket = JSON.parseObject(data, JRollTicket.class);
            Long indocno = jrollTicket.getIndocno();

            RollTicket rollTicket = rollTicketDao.findDataRollTicketByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollTicket);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollTicket> findDataRollTicket() {
        List<RollTicket> list = rollTicketDao.findDataRollTicket();
        return list;
    }

}
