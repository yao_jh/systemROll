package com.my.business.modular.rollticket.dao;

import com.my.business.modular.rollticket.entity.RollTicket;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 辊票dao接口
 *
 * @author 生成器生成
 * @date 2020-08-28 09:34:20
 */
@Mapper
public interface RollTicketDao {

    /**
     * 添加记录
     *
     * @param rollTicket 对象实体
     */
    void insertDataRollTicket(RollTicket rollTicket);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollTicketOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollTicketMany(String value);

    /**
     * 修改记录
     *
     * @param rollTicket 对象实体
     */
    void updateDataRollTicket(RollTicket rollTicket);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollTicket> findDataRollTicketByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollTicketByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollTicket findDataRollTicketByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollTicket> findDataRollTicket();

}
