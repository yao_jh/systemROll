package com.my.business.modular.rollticket.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 辊票实体类
 *
 * @author 生成器生成
 * @date 2020-08-28 09:34:19
 */
public class RollTicket extends BaseEntity {

    private Long indocno;  //主键
    private String roll_no;  //辊号
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private Long roll_positionid;  //轧辊位置id
    private String roll_position;  //轧辊位置
    private String ds_chock_no;  //DS侧座号
    private String os_chock_no;  //OS侧座号
    private Double roll_size_now;  //当前辊径
    private Long confirm_firstid;  //第一确认人
    private String confirm_first;  //第一确认人

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public Long getRoll_typeid() {
        return this.roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public Long getRoll_positionid() {
        return this.roll_positionid;
    }

    public void setRoll_positionid(Long roll_positionid) {
        this.roll_positionid = roll_positionid;
    }

    public String getRoll_position() {
        return this.roll_position;
    }

    public void setRoll_position(String roll_position) {
        this.roll_position = roll_position;
    }

    public String getDs_chock_no() {
        return this.ds_chock_no;
    }

    public void setDs_chock_no(String ds_chock_no) {
        this.ds_chock_no = ds_chock_no;
    }

    public String getOs_chock_no() {
        return this.os_chock_no;
    }

    public void setOs_chock_no(String os_chock_no) {
        this.os_chock_no = os_chock_no;
    }

    public Double getRoll_size_now() {
        return this.roll_size_now;
    }

    public void setRoll_size_now(Double roll_size_now) {
        this.roll_size_now = roll_size_now;
    }

    public Long getConfirm_firstid() {
        return this.confirm_firstid;
    }

    public void setConfirm_firstid(Long confirm_firstid) {
        this.confirm_firstid = confirm_firstid;
    }

    public String getConfirm_first() {
        return this.confirm_first;
    }

    public void setConfirm_first(String confirm_first) {
        this.confirm_first = confirm_first;
    }


}