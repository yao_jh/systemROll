package com.my.business.modular.basechock.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.math.BigDecimal;

/**
 * 轴承座综合台账实体类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
public class BaseChock extends BaseEntity {

    private Long indocno;  //主键
    private String chock_no;  //轴承座号
    private String roll_no;  //辊号
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private Long production_line_id;  //产线id
    private String production_line;  //产线
    private Long frame_noid;  //机架号id
    private String frame_no;  //机架号
    private Long install_location_id;  //安装位置id
    private String install_location;  //安装位置
    private Long up_location_id;  //上机位置id
    private String up_location;  //上机位置
    private String bearing_type;  //轴承类型
    private String workface;  //轴承承载区
    private Long istatus;  //轴承座状态id
    private String status;  //轴承座状态
    private String usetime;  //投用时间
    private String scraptime;  //报废时间
    private String lasttime;  //剩余换区时间
    private String reports;  //检测报告
    private String results;  //检测结果
    private String snote;  //备注
    private int factory_id;//厂家id
    private String factory_name;//厂家名称

    private BigDecimal single_times;//单次累计时间
    private BigDecimal interval_times;//间隔提醒时间
    private BigDecimal reminder_times;//提醒处理次数
    private BigDecimal total_times;//总累计时间
    private BigDecimal discard_times;//报废提醒时间
    private String discard_time;//报废提醒时间
    private int ifdiscard;//是否报废 0 正常 1 报废

    public String getDiscard_time() {
        return discard_time;
    }

    public void setDiscard_time(String discard_time) {
        this.discard_time = discard_time;
    }

    public BigDecimal getSingle_times() {
        return single_times;
    }

    public void setSingle_times(BigDecimal single_times) {
        this.single_times = single_times;
    }

    public BigDecimal getInterval_times() {
        return interval_times;
    }

    public void setInterval_times(BigDecimal interval_times) {
        this.interval_times = interval_times;
    }

    public BigDecimal getReminder_times() {
        return reminder_times;
    }

    public void setReminder_times(BigDecimal reminder_times) {
        this.reminder_times = reminder_times;
    }

    public BigDecimal getTotal_times() {
        return total_times;
    }

    public void setTotal_times(BigDecimal total_times) {
        this.total_times = total_times;
    }

    public BigDecimal getDiscard_times() {
        return discard_times;
    }

    public void setDiscard_times(BigDecimal discard_times) {
        this.discard_times = discard_times;
    }

    public int getIfdiscard() {
        return ifdiscard;
    }

    public void setIfdiscard(int ifdiscard) {
        this.ifdiscard = ifdiscard;
    }

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getChock_no() {
        return this.chock_no;
    }

    public void setChock_no(String chock_no) {
        this.chock_no = chock_no;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public Long getRoll_typeid() {
        return this.roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public Long getProduction_line_id() {
        return this.production_line_id;
    }

    public void setProduction_line_id(Long production_line_id) {
        this.production_line_id = production_line_id;
    }

    public String getProduction_line() {
        return this.production_line;
    }

    public void setProduction_line(String production_line) {
        this.production_line = production_line;
    }

    public Long getFrame_noid() {
        return this.frame_noid;
    }

    public void setFrame_noid(Long frame_noid) {
        this.frame_noid = frame_noid;
    }

    public String getFrame_no() {
        return this.frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public Long getInstall_location_id() {
        return this.install_location_id;
    }

    public void setInstall_location_id(Long install_location_id) {
        this.install_location_id = install_location_id;
    }

    public String getInstall_location() {
        return this.install_location;
    }

    public void setInstall_location(String install_location) {
        this.install_location = install_location;
    }

    public Long getUp_location_id() {
        return this.up_location_id;
    }

    public void setUp_location_id(Long up_location_id) {
        this.up_location_id = up_location_id;
    }

    public String getUp_location() {
        return this.up_location;
    }

    public void setUp_location(String up_location) {
        this.up_location = up_location;
    }

    public String getBearing_type() {
        return this.bearing_type;
    }

    public void setBearing_type(String bearing_type) {
        this.bearing_type = bearing_type;
    }

    public String getWorkface() {
        return this.workface;
    }

    public void setWorkface(String workface) {
        this.workface = workface;
    }

    public Long getIstatus() {
        return this.istatus;
    }

    public void setIstatus(Long istatus) {
        this.istatus = istatus;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsetime() {
        return this.usetime;
    }

    public void setUsetime(String usetime) {
        this.usetime = usetime;
    }

    public String getScraptime() {
        return this.scraptime;
    }

    public void setScraptime(String scraptime) {
        this.scraptime = scraptime;
    }

    public String getLasttime() {
        return this.lasttime;
    }

    public void setLasttime(String lasttime) {
        this.lasttime = lasttime;
    }

    public String getReports() {
        return this.reports;
    }

    public void setReports(String reports) {
        this.reports = reports;
    }

    public String getResults() {
        return this.results;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public int getFactory_id() {
        return factory_id;
    }

    public void setFactory_id(int factory_id) {
        this.factory_id = factory_id;
    }

    public String getFactory_name() {
        return factory_name;
    }

    public void setFactory_name(String factory_name) {
        this.factory_name = factory_name;
    }
}