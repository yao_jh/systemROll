package com.my.business.modular.basechock.dao;

import com.my.business.modular.basechock.entity.BaseBearing;
import com.my.business.modular.basechock.entity.BaseEquipment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 设备履历综合台账dao接口
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@Mapper
public interface BaseEquipmentDao {

    /**
     * 添加记录
     *
     * @param baseChock 对象实体
     */
    void insertDataBaseEquipment(BaseEquipment baseEquipment);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataBaseEquipmentOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataBaseEquipmentMany(String value);

    /**
     * 修改记录
     *
     * @param baseChock 对象实体
     */
    void updateDataBaseEquipment(BaseEquipment baseEquipment);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<BaseEquipment> findDataBaseEquipmentByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,
                                                    @Param("sclass_name")String sclass_name, @Param("sgroup_name")String sgroup_name, @Param("factory_name")String factory_name, @Param("operator_name")String operator_name,
                                                    @Param("dbegin")String dbegin, @Param("dend")String dend, @Param("specifications")String specifications, @Param("equipment_name")String equipment_name,
                                                    @Param("equipment_parentname")String equipment_parentname, @Param("equipment_pparentname")String equipment_pparentname, @Param("ifdiscard")String ifdiscard,@Param("equipment_id")String equipment_id);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataBaseEquipmentByPageSize(@Param("sclass_name")String sclass_name, @Param("sgroup_name")String sgroup_name, @Param("factory_name")String factory_name, @Param("operator_name")String operator_name,
                                            @Param("dbegin")String dbegin, @Param("dend")String dend, @Param("specifications")String specifications, @Param("equipment_name")String equipment_name,
                                            @Param("equipment_parentname")String equipment_parentname, @Param("equipment_pparentname")String equipment_pparentname, @Param("ifdiscard")String ifdiscard,@Param("equipment_id")String equipment_id);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    BaseEquipment findDataBaseEquipmentByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<BaseEquipment> findDataBaseEquipment();

    void updateDataBaseEquipmentDiscardTime(@Param("indocno") Long indocno);

    void updateDataBaseEquipmentDiscardTimeCancel(@Param("indocno") Long indocno);

}
