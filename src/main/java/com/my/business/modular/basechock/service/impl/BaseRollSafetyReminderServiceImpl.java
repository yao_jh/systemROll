package com.my.business.modular.basechock.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.basechock.dao.BaseCostAccountingMainDao;
import com.my.business.modular.basechock.dao.BaseRollSafetyReminderDao;
import com.my.business.modular.basechock.entity.*;
import com.my.business.modular.basechock.service.BaseCostAccountingMainService;
import com.my.business.modular.basechock.service.BaseRollSafetyReminderService;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.sys.common.entity.MapXYEntity;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.util.DateUtil;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 轧辊安全期提醒   综合台账接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@Service
public class BaseRollSafetyReminderServiceImpl implements BaseRollSafetyReminderService {

    @Autowired
    private BaseRollSafetyReminderDao baseRollSafetyReminderDao;
    @Autowired
    private RollInformationDao rollInformationDao;
    @Autowired
    private BaseCostAccountingMainDao baseCostAccountingMainDao;
    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertBaseRollSafetyReminder(String data, Long userId, String sname) {
        try {
            JBaseRollSafetyReminder jBaseRollSafetyReminder = JSON.parseObject(data, JBaseRollSafetyReminder.class);
            BaseRollSafetyReminder baseRollSafetyReminder = jBaseRollSafetyReminder.getBaseRollSafetyReminder();
            CodiUtil.newRecord(userId, sname, baseRollSafetyReminder);
            baseRollSafetyReminderDao.insertBaseRollSafetyReminder(baseRollSafetyReminder);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteBaseRollSafetyReminderOne(Long indocno) {
        try {
            baseRollSafetyReminderDao.deleteBaseRollSafetyReminderOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateBaseRollSafetyReminder(String data, Long userId, String sname) {
        try {
            JBaseRollSafetyReminder jBaseRollSafetyReminder = JSON.parseObject(data, JBaseRollSafetyReminder.class);
            BaseRollSafetyReminder baseRollSafetyReminder = jBaseRollSafetyReminder.getBaseRollSafetyReminder();
            CodiUtil.editRecord(userId, sname, baseRollSafetyReminder);
            baseRollSafetyReminderDao.updateBaseRollSafetyReminder(baseRollSafetyReminder);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findBaseRollSafetyReminderByPage(String data) {
        try {
            JBaseRollSafetyReminder jBaseRollSafetyReminder = JSON.parseObject(data, JBaseRollSafetyReminder.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jBaseRollSafetyReminder.getPageIndex();
            Integer pageSize = jBaseRollSafetyReminder.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jBaseRollSafetyReminder.getCondition()) {
                jsonObject = JSON.parseObject(jBaseRollSafetyReminder.getCondition().toString());
            }else{
                jsonObject = new JSONObject();
            }
            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }
            String roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = jsonObject.get("roll_typeid").toString();
            }
            String material_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("material_id"))) {
                material_id = jsonObject.get("material_id").toString();
            }
            String framerangeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("framerangeid"))) {
                framerangeid = jsonObject.get("framerangeid").toString();
            }

            List<BaseRollSafetyReminder> list = baseRollSafetyReminderDao.findBaseRollSafetyReminderByPage((pageIndex - 1) * pageSize, pageSize,roll_typeid,material_id,framerangeid);
            Integer count = baseRollSafetyReminderDao.findBaseRollSafetyReminderByPageSize(roll_typeid,material_id,framerangeid);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findBaseRollSafetyReminderByIndocno(String data) {
        try {
            JBaseRollSafetyReminder jBaseRollSafetyReminder = JSON.parseObject(data, JBaseRollSafetyReminder.class);

            Long indocno = jBaseRollSafetyReminder.getIndocno();
            BaseRollSafetyReminder baseRollSafetyReminder = baseRollSafetyReminderDao.findBaseRollSafetyReminderByIndocno(indocno);
            return ResultData.ResultDataSuccess(baseRollSafetyReminder);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public ResultData findBaseRollSafetyReminder(String data) {
        try{
            JBaseRollSafetyReminder jBaseRollSafetyReminder = JSON.parseObject(data, JBaseRollSafetyReminder.class);
            JSONObject jsonObject = null;

            if (null != jBaseRollSafetyReminder.getCondition()) {
                jsonObject = JSON.parseObject(jBaseRollSafetyReminder.getCondition().toString());
            }else{
                jsonObject = new JSONObject();
            }

            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }else{
                dbegin = DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss");
            }

            String roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = jsonObject.get("roll_typeid").toString();
            }
            String material_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("material_id"))) {
                material_id = jsonObject.get("material_id").toString();
            }
            String framerangeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("framerangeid"))) {
                framerangeid = jsonObject.get("framerangeid").toString();
            }
            List<BaseRollSafetyReminder> list = baseRollSafetyReminderDao.findBaseRollSafetyReminderByPage(0, 50,roll_typeid,material_id,framerangeid);
            for (BaseRollSafetyReminder bean : list){
                BigDecimal count = rollInformationDao.findRollDiameterByPageSize(bean.getRoll_typeid()+"",bean.getFramerangeid()+"",null,bean.getMaterial_id()+"",dbegin);
                if(count!=null){
                    bean.setS_inventory(count);
                }
                if(StringUtils.isEmpty(bean.getD_inventory())){
                String str=dbegin.substring(5,7);
                String start = "";// >= 2020-10-01 08:00:00
                String end = "";// < 2021-01-01 08:00:00
                // 按照季度 查询辊耗
                List<BaseCostAccountingReport> listBaseCostAccountingReport = null;
                if(Long.valueOf(str)==1L||Long.valueOf(str)==2L||Long.valueOf(str)==3L){
                    start =String.valueOf(Long.valueOf(dbegin.substring(0,4))-1)+"-10-01 08:00:00";
                    end =dbegin.substring(0,4)+"-01-01 07:59:59";
                    listBaseCostAccountingReport= baseCostAccountingMainDao.findDataBaseCostAccountingByPage(0, 50,null,null,bean.getRoll_typeid()+"",bean.getMaterial_id()+"",bean.getFramerangeid()+"",start,end);
                }else if(Long.valueOf(str)==4L||Long.valueOf(str)==5L||Long.valueOf(str)==6L){
                    start =dbegin.substring(0,4)+"-01-01 08:00:00";
                    end =dbegin.substring(0,4)+"-04-01 07:59:59";
                    listBaseCostAccountingReport= baseCostAccountingMainDao.findDataBaseCostAccountingByPage(0, 50,null,null,bean.getRoll_typeid()+"",bean.getMaterial_id()+"",bean.getFramerangeid()+"",start,end);
                }else if(Long.valueOf(str)==7L||Long.valueOf(str)==8L||Long.valueOf(str)==9L){
                    start =dbegin.substring(0,4)+"-04-01 08:00:00";
                    end =dbegin.substring(0,4)+"-07-01 07:59:59";
                    listBaseCostAccountingReport= baseCostAccountingMainDao.findDataBaseCostAccountingByPage(0, 50,null,null,bean.getRoll_typeid()+"",bean.getMaterial_id()+"",bean.getFramerangeid()+"",start,end);
                }else if(Long.valueOf(str)==10L||Long.valueOf(str)==11L||Long.valueOf(str)==12L){
                    start =dbegin.substring(0,4)+"-07-01 08:00:00";
                    end =dbegin.substring(0,4)+"-10-01 07:59:59";
                    listBaseCostAccountingReport= baseCostAccountingMainDao.findDataBaseCostAccountingByPage(0, 50,null,null,bean.getRoll_typeid()+"",bean.getMaterial_id()+"",bean.getFramerangeid()+"",start,end);
                }
                if(listBaseCostAccountingReport!=null&&listBaseCostAccountingReport.size()>0){
                    for (BaseCostAccountingReport b:listBaseCostAccountingReport){
                        if(!StringUtils.isEmpty(b.getMmnumber())){
                            if(StringUtils.isEmpty(bean.getD_inventory())){
                                bean.setD_inventory(BigDecimal.ZERO);
                            }
                            bean.setD_inventory(bean.getD_inventory().add(b.getMmnumber()));
                        }
                    }
                }
                }//bean.getSafe_inventory()!=null&&
                if(bean.getD_inventory()!=null){
                    /*bean.setEffective_value(bean.getS_inventory().
                            subtract(bean.getSafe_inventory()).
                            add(BigDecimal.valueOf(140)).subtract(bean.getD_inventory())

                    ) ;*/
                    bean.setSafe_inventory(bean.getD_inventory().divide(BigDecimal.valueOf(6),3,BigDecimal.ROUND_HALF_UP));
                }
                if(!StringUtils.isEmpty(bean.getS_inventory())&&!StringUtils.isEmpty(bean.getD_inventory())&&!StringUtils.isEmpty(bean.getPurchasing_cycle())){
                    BigDecimal value = (bean.getS_inventory().subtract(bean.getSafe_inventory())).multiply(BigDecimal.valueOf(3)).divide(bean.getD_inventory(),1, BigDecimal.ROUND_HALF_UP).subtract(bean.getPurchasing_cycle()).multiply(BigDecimal.valueOf(30));

                    Calendar c = Calendar.getInstance();
                    c.setTime(new Date());
                    c.add(Calendar.DAY_OF_MONTH, value.intValue());// 为负数时，则为前几天
                    SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    bean.setProcurement_date(f.format(c.getTime()));
                }

            }

            List<MapXYEntity> map = new ArrayList<MapXYEntity>();
            if(list!=null&&list.size()>0){
                for (BaseRollSafetyReminder m:list){
                    MapXYEntity t = new MapXYEntity();
                    if(!StringUtils.isEmpty(m.getRoll_type()+" "+m.getMaterial()+" "+m.getFramerange())){
                        String str="";
                        if(m.getMaterial().equals("高速钢")){
                            str+="H";
                        }
                        str+=m.getFramerange();
                        t.setX(str);
                    }else{
                        t.setX(0);
                    }
                    if(!StringUtils.isEmpty(m.getS_inventory())&&!StringUtils.isEmpty(m.getD_inventory())){
                        t.setY1((m.getS_inventory().subtract(m.getSafe_inventory())).multiply(BigDecimal.valueOf(3)).divide(m.getD_inventory(),1, BigDecimal.ROUND_HALF_UP));
                        //如果
                    }else{
                        t.setY1(0);
                    }
                    map.add(t);
                }
            }

            return ResultData.ResultDataSuccess(list,0,null,map);

        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public String excelBaseRollSafetyReminder(HSSFWorkbook workbook, String roll_typeid
            ,String material_id
            ,String framerangeid
            ,String dbegin) {
        String filename="轧辊安全期提醒";

        if(StringUtils.isEmpty(roll_typeid)){
            roll_typeid = null;
        }
        if(StringUtils.isEmpty(material_id)){
            material_id = null;
        }
        if(StringUtils.isEmpty(framerangeid)){
            framerangeid = null;
        }
        if(StringUtils.isEmpty(dbegin)){
            dbegin = null;
        }

            if (!StringUtils.isEmpty(dbegin)) {
                dbegin = DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss");
            }
            List<BaseRollSafetyReminder> list = baseRollSafetyReminderDao.findBaseRollSafetyReminderByPage(0, 50,roll_typeid,material_id,framerangeid);
            for (BaseRollSafetyReminder bean : list){
                BigDecimal count = rollInformationDao.findRollDiameterByPageSize(bean.getRoll_typeid()+"",bean.getFramerangeid()+"",null,bean.getMaterial_id()+"",dbegin);
                if(count!=null){
                    bean.setS_inventory(count);
                }
                if(StringUtils.isEmpty(bean.getD_inventory())) {
                    String str = dbegin.substring(5, 7);
                    String start = "";// >= 2020-10-01 08:00:00
                    String end = "";// < 2021-01-01 08:00:00
                    // 按照季度 查询辊耗
                    List<BaseCostAccountingReport> listBaseCostAccountingReport = null;
                    if (Long.valueOf(str) == 1L || Long.valueOf(str) == 2L || Long.valueOf(str) == 3L) {
                        start = String.valueOf(Long.valueOf(dbegin.substring(0, 4)) - 1) + "-10-01 08:00:00";
                        end = dbegin.substring(0, 4) + "-01-01 07:59:59";
                        listBaseCostAccountingReport = baseCostAccountingMainDao.findDataBaseCostAccountingByPage(0, 50, null, null, bean.getRoll_typeid()+"",bean.getMaterial_id()+"",bean.getFramerangeid()+"", start, end);
                    } else if (Long.valueOf(str) == 4L || Long.valueOf(str) == 5L || Long.valueOf(str) == 6L) {
                        start = dbegin.substring(0, 4) + "-01-01 08:00:00";
                        end = dbegin.substring(0, 4) + "-04-01 07:59:59";
                        listBaseCostAccountingReport = baseCostAccountingMainDao.findDataBaseCostAccountingByPage(0, 50, null, null,bean.getRoll_typeid()+"",bean.getMaterial_id()+"",bean.getFramerangeid()+"", start, end);
                    } else if (Long.valueOf(str) == 7L || Long.valueOf(str) == 8L || Long.valueOf(str) == 9L) {
                        start = dbegin.substring(0, 4) + "-04-01 08:00:00";
                        end = dbegin.substring(0, 4) + "-07-01 07:59:59";
                        listBaseCostAccountingReport = baseCostAccountingMainDao.findDataBaseCostAccountingByPage(0, 50, null, null, bean.getRoll_typeid()+"",bean.getMaterial_id()+"",bean.getFramerangeid()+"", start, end);
                    } else if (Long.valueOf(str) == 10L || Long.valueOf(str) == 11L || Long.valueOf(str) == 12L) {
                        start = dbegin.substring(0, 4) + "-07-01 08:00:00";
                        end = dbegin.substring(0, 4) + "-10-01 07:59:59";
                        listBaseCostAccountingReport = baseCostAccountingMainDao.findDataBaseCostAccountingByPage(0, 50, null, null, bean.getRoll_typeid()+"",bean.getMaterial_id()+"",bean.getFramerangeid()+"", start, end);
                    }
                    if (listBaseCostAccountingReport != null && listBaseCostAccountingReport.size() > 0) {
                        for (BaseCostAccountingReport b : listBaseCostAccountingReport) {
                            if (!StringUtils.isEmpty(b.getMmnumber())) {
                                if(StringUtils.isEmpty(bean.getD_inventory())){
                                    bean.setD_inventory(BigDecimal.ZERO);
                                }
                                bean.setD_inventory(bean.getD_inventory().add(b.getMmnumber()));
                            }
                        }
                    }
                }
                /*if(bean.getSafe_inventory()!=null&&bean.getS_inventory()!=null&&bean.getD_inventory()!=null){
                    bean.setEffective_value(bean.getS_inventory().
                            subtract(bean.getSafe_inventory()).
                            add(BigDecimal.valueOf(140)).subtract(bean.getD_inventory())

                    ) ;
                }*/
                if(bean.getD_inventory()!=null){
                    /*bean.setEffective_value(bean.getS_inventory().
                            subtract(bean.getSafe_inventory()).
                            add(BigDecimal.valueOf(140)).subtract(bean.getD_inventory())

                    ) ;*/
                    bean.setSafe_inventory(bean.getD_inventory().divide(BigDecimal.valueOf(6),3,BigDecimal.ROUND_HALF_UP));
                }
                if(!StringUtils.isEmpty(bean.getS_inventory())&&!StringUtils.isEmpty(bean.getD_inventory())&&!StringUtils.isEmpty(bean.getPurchasing_cycle())){
                    BigDecimal value = (bean.getS_inventory().subtract(bean.getSafe_inventory())).multiply(BigDecimal.valueOf(3)).divide(bean.getD_inventory(),1, BigDecimal.ROUND_HALF_UP).subtract(bean.getPurchasing_cycle()).multiply(BigDecimal.valueOf(30));

                    Calendar c = Calendar.getInstance();
                    c.setTime(new Date());
                    c.add(Calendar.DAY_OF_MONTH, value.intValue());// 为负数时，则为前几天
                    SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    bean.setProcurement_date(f.format(c.getTime()));
                }

            }
        HSSFSheet sheet = workbook.createSheet("sheet1");
        getExcelBaseRollSafetyReminder(list, sheet, workbook);
        return filename;
    }

    private void getExcelBaseRollSafetyReminder(List<BaseRollSafetyReminder> list, HSSFSheet sheet, HSSFWorkbook workbook) {
        if(list!=null && list.size()>0) {
            // 设置表头样式 居中 加粗 宋体 20
            HSSFCellStyle cellStyleHead = workbook.createCellStyle();
            cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
            HSSFFont redFontHead = workbook.createFont();
            redFontHead.setFontHeightInPoints((short) 12);//设置字体大小
            redFontHead.setFontName("宋体");//字体型号
            cellStyleHead.setFont(redFontHead);
            HSSFRow rowhead = sheet.createRow(0);   //创建第一行头
            HSSFCell cell_00 = rowhead.createCell(0);   //创建第一行第一列
            cell_00.setCellStyle(cellStyleHead);
            cell_00.setCellValue("轧辊安全期提醒");

            HSSFRow row1 = sheet.createRow(1);   //创建第二行
            HSSFCell cell_2_1 = row1.createCell(0);   //创建第二行第一列
            cell_2_1.setCellValue("序号");
            HSSFCell cell_2_2 = row1.createCell(1);   //创建第二行第二列
            cell_2_2.setCellValue("轧辊类型");
            HSSFCell cell_2_3 = row1.createCell(2);   //创建第二行第三列
            cell_2_3.setCellValue("材质");
            HSSFCell cell_2_4 = row1.createCell(3);   //创建第二行第四列
            cell_2_4.setCellValue("机架范围");
            HSSFCell cell_2_5 = row1.createCell(4);   //创建第二行第五列
            cell_2_5.setCellValue("季度消耗");
            HSSFCell cell_2_6 = row1.createCell(5);   //创建第二行第六列
            cell_2_6.setCellValue("安全库存");
            HSSFCell cell_2_7 = row1.createCell(6);   //创建第二行第七列
            cell_2_7.setCellValue("结存");
            HSSFCell cell_2_8 = row1.createCell(7);   //创建第二行第八列
            cell_2_8.setCellValue("采购周期");
            HSSFCell cell_2_9 = row1.createCell(8);   //创建第二行第9列
            cell_2_9.setCellValue("预计采购时间");

            //从第三行开始(包含第三行)
            for (int i = 0; i < list.size(); i++) {
                BaseRollSafetyReminder v = list.get(i);
                HSSFRow rown = sheet.createRow(i + 2);   //创建第三行
                HSSFCell cell_3_1 = rown.createCell(0);   // 1
                cell_3_1.setCellValue(i + 1);
                HSSFCell cell_3_2 = rown.createCell(1);   //2
                if (!StringUtils.isEmpty(v.getRoll_type())) {
                    cell_3_2.setCellValue(v.getRoll_type());
                }
                HSSFCell cell_3_3 = rown.createCell(2);   //3
                if (!StringUtils.isEmpty(v.getMaterial())) {
                    cell_3_3.setCellValue(v.getMaterial());
                }
                HSSFCell cell_3_4 = rown.createCell(3);   //4
                if (!StringUtils.isEmpty(v.getFramerange())) {
                    cell_3_4.setCellValue(v.getFramerange());
                }
                HSSFCell cell_3_5 = rown.createCell(4);   //5
                if (!StringUtils.isEmpty(v.getD_inventory())) {
                    cell_3_5.setCellValue(v.getD_inventory().toString());
                }
                HSSFCell cell_3_6 = rown.createCell(5);   //6
                if (!StringUtils.isEmpty(v.getSafe_inventory())) {
                    cell_3_6.setCellValue(v.getSafe_inventory().toString());
                }
                HSSFCell cell_3_7 = rown.createCell(6);   //7
                if (!StringUtils.isEmpty(v.getS_inventory())) {
                    cell_3_7.setCellValue(v.getS_inventory().toString());
                }
                HSSFCell cell_3_8 = rown.createCell(7);   //8
                if (!StringUtils.isEmpty(v.getPurchasing_cycle())) {
                    cell_3_8.setCellValue(v.getPurchasing_cycle().toString());
                }
                HSSFCell cell_3_9 = rown.createCell(8);   //9
                if (!StringUtils.isEmpty(v.getProcurement_date())) {
                    cell_3_9.setCellValue(v.getProcurement_date());
                }
            }
            CellRangeAddress region = new CellRangeAddress(0, 0, 0, 8);
            sheet.addMergedRegion(region);
        }
    }

}
