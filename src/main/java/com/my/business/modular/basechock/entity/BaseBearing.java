package com.my.business.modular.basechock.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.math.BigDecimal;

/**
 * 轴承综合台账实体类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
public class BaseBearing extends BaseEntity {

    private Long indocno;  //主键
    private String chock_no;  //轴承座号

    private String bearing_no;//轴承号
    private String bearing_name;//轴承名称
    private String usetime;//开始时间
    private BigDecimal single_times;//单次累计时间
    private BigDecimal interval_times;//间隔提醒时间
    private BigDecimal reminder_times;//提醒处理次数
    private BigDecimal total_times;//总累计时间
    private BigDecimal discard_times;//报废提醒时间
    private String discard_time;//报废时间
    private int ifdiscard;//是否报废 0 正常 1 报废
    private int bearing_type;//1 轴承 2 密封件  3 砂轮

    private int factory_id;//厂家id
    private String factory_name;//厂家名称

    private BigDecimal sand_thickness;//砂厚
    private BigDecimal sand_article;//砂轮粒度
private  int seals_type;//密封件 类型
    private  String seals_name;//密封件 类型

    public String getSeals_name() {
        return seals_name;
    }

    public void setSeals_name(String seals_name) {
        this.seals_name = seals_name;
    }

    public int getSeals_type() {
        return seals_type;
    }

    public void setSeals_type(int seals_type) {
        this.seals_type = seals_type;
    }

    public BigDecimal getSand_article() {
        return sand_article;
    }

    public void setSand_article(BigDecimal sand_article) {
        this.sand_article = sand_article;
    }

    public BigDecimal getSand_thickness() {
        return sand_thickness;
    }

    public void setSand_thickness(BigDecimal sand_thickness) {
        this.sand_thickness = sand_thickness;
    }

    public int getFactory_id() {
        return factory_id;
    }

    public void setFactory_id(int factory_id) {
        this.factory_id = factory_id;
    }

    public String getFactory_name() {
        return factory_name;
    }

    public void setFactory_name(String factory_name) {
        this.factory_name = factory_name;
    }

    public int getBearing_type() {
        return bearing_type;
    }

    public void setBearing_type(int bearing_type) {
        this.bearing_type = bearing_type;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getChock_no() {
        return chock_no;
    }

    public void setChock_no(String chock_no) {
        this.chock_no = chock_no;
    }

    public String getBearing_no() {
        return bearing_no;
    }

    public void setBearing_no(String bearing_no) {
        this.bearing_no = bearing_no;
    }

    public String getBearing_name() {
        return bearing_name;
    }

    public void setBearing_name(String bearing_name) {
        this.bearing_name = bearing_name;
    }

    public String getUsetime() {
        return usetime;
    }

    public void setUsetime(String usetime) {
        this.usetime = usetime;
    }

    public BigDecimal getSingle_times() {
        return single_times;
    }

    public void setSingle_times(BigDecimal single_times) {
        this.single_times = single_times;
    }

    public BigDecimal getInterval_times() {
        return interval_times;
    }

    public void setInterval_times(BigDecimal interval_times) {
        this.interval_times = interval_times;
    }

    public BigDecimal getReminder_times() {
        return reminder_times;
    }

    public void setReminder_times(BigDecimal reminder_times) {
        this.reminder_times = reminder_times;
    }

    public BigDecimal getTotal_times() {
        return total_times;
    }

    public void setTotal_times(BigDecimal total_times) {
        this.total_times = total_times;
    }

    public BigDecimal getDiscard_times() {
        return discard_times;
    }

    public void setDiscard_times(BigDecimal discard_times) {
        this.discard_times = discard_times;
    }

    public String getDiscard_time() {
        return discard_time;
    }

    public void setDiscard_time(String discard_time) {
        this.discard_time = discard_time;
    }

    public int getIfdiscard() {
        return ifdiscard;
    }

    public void setIfdiscard(int ifdiscard) {
        this.ifdiscard = ifdiscard;
    }
}