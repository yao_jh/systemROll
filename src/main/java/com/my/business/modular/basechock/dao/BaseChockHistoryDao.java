package com.my.business.modular.basechock.dao;

import com.my.business.modular.basechock.entity.BaseChock;
import com.my.business.modular.basechock.entity.BaseChockHistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轴承座综合台账dao接口
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@Mapper
public interface BaseChockHistoryDao {

    /**
     * 添加记录
     *
     * @param baseChock 对象实体
     */
    void insertDataBaseChockHistory(BaseChockHistory baseChockHistory);

    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<BaseChockHistory> findDataBaseChockHistoryByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("chock_no") String chock_no, @Param("roll_no") String roll_no, @Param("frame_noid") Long frame_noid, @Param("frame_no") String frame_no, @Param("production_line_id") Long production_line_id, @Param("roll_typeid") Long roll_typeid, @Param("istatus") Long istatus);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataBaseChockHistoryByPageSize(@Param("chock_no") String chock_no, @Param("roll_no") String roll_no, @Param("frame_noid") Long frame_noid, @Param("frame_no") String frame_no, @Param("production_line_id") Long production_line_id, @Param("roll_typeid") Long roll_typeid, @Param("istatus") Long istatus);

}
