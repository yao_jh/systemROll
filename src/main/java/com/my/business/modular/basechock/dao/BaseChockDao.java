package com.my.business.modular.basechock.dao;

import com.my.business.modular.basechock.entity.BaseChock;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轴承座综合台账dao接口
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@Mapper
public interface BaseChockDao {

    /**
     * 添加记录
     *
     * @param baseChock 对象实体
     */
    void insertDataBaseChock(BaseChock baseChock);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataBaseChockOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataBaseChockMany(String value);

    /**
     * 修改记录
     *
     * @param baseChock 对象实体
     */
    void updateDataBaseChock(BaseChock baseChock);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<BaseChock> findDataBaseChockByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("chock_no") String chock_no, @Param("roll_no") String roll_no, @Param("frame_noid") Long frame_noid, @Param("frame_no") String frame_no, @Param("production_line_id") Long production_line_id, @Param("roll_typeid") Long roll_typeid, @Param("istatus") Long istatus,@Param("ifdiscard") Integer ifdiscard);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataBaseChockByPageSize(@Param("chock_no") String chock_no, @Param("roll_no") String roll_no, @Param("frame_noid") Long frame_noid, @Param("frame_no") String frame_no, @Param("production_line_id") Long production_line_id, @Param("roll_typeid") Long roll_typeid, @Param("istatus") Long istatus,@Param("ifdiscard") Integer ifdiscard);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    BaseChock findDataBaseChockByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<BaseChock> findDataBaseChock();

    /**
     * 根据轴承座号查找数据
     * @param chock_no 轴承座号
     * @return
     */
    BaseChock findDataBaseChockByChockNo(@Param("chock_no") String chock_no);

    List<BaseChock> findListByBaseChock(@Param("install_location_id") String install_location_id, @Param("up_location_id") String up_location_id);

    void updateDataBaseChockDiscardTime(@Param("indocno") Long indocno);

    void updateDataBaseChockSingleTimes(@Param("indocno") Long indocno);

    void updateDataBaseChockDiscardTimeCancel(@Param("indocno")Long indocno);
}
