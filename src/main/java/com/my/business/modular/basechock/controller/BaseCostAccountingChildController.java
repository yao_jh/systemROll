package com.my.business.modular.basechock.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.basechock.entity.BaseEquipment;
import com.my.business.modular.basechock.entity.JBaseCostAccountingChild;
import com.my.business.modular.basechock.entity.JBaseEquipment;
import com.my.business.modular.basechock.service.BaseCostAccountingChildService;
import com.my.business.modular.basechock.service.BaseEquipmentService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 料号规格定制子表 综合台账控制器层
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@RestController
@RequestMapping("/baseCostAccountingChild")
public class BaseCostAccountingChildController {

    @Autowired
    private BaseCostAccountingChildService baseCostAccountingChildService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataBaseCostAccountingChild(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return baseCostAccountingChildService.insertDataBaseCostAccountingChild(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataBaseCostAccountingChildOne(@RequestBody String data) {
        try {
            JBaseCostAccountingChild jBaseCostAccountingChild = JSON.parseObject(data, JBaseCostAccountingChild.class);
            return baseCostAccountingChildService.deleteDataBaseCostAccountingChildOne(jBaseCostAccountingChild.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }


    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataBaseCostAccountingChild(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return baseCostAccountingChildService.updateDataBaseCostAccountingChild(data, userId, CodiUtil.returnLm(sname));
    }



    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataBaseCostAccountingChildByPage(@RequestBody String data) {
        return baseCostAccountingChildService.findDataBaseCostAccountingChildByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataBaseCostAccountingChildByIndocno(@RequestBody String data) {
        return baseCostAccountingChildService.findDataBaseCostAccountingChildByIndocno(data);
    }


}
