package com.my.business.modular.basechock.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.basechock.entity.JBaseCostAccountingChild;
import com.my.business.modular.basechock.service.BaseCostAccountingChildService;
import com.my.business.modular.basechock.service.BaseKpiService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * kpi
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@RestController
@RequestMapping("/baseKpi")
public class BaseKpiController {

    @Autowired
    private BaseKpiService baseKpiService;

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataKpi(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return baseKpiService.updateDataBaseKpi(data, userId, CodiUtil.returnLm(sname));
    }



    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataKpiByPage(@RequestBody String data) {
        return baseKpiService.findDataBaseKpiByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataKpiByIndocno(@RequestBody String data) {
        return baseKpiService.findDataBaseKpiByIndocno(data);
    }


}
