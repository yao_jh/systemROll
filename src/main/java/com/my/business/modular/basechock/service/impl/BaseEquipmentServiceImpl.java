package com.my.business.modular.basechock.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.basechock.dao.BaseBearingDao;
import com.my.business.modular.basechock.dao.BaseChockDao;
import com.my.business.modular.basechock.dao.BaseEquipmentDao;
import com.my.business.modular.basechock.entity.*;
import com.my.business.modular.basechock.service.BaseBearingService;
import com.my.business.modular.basechock.service.BaseEquipmentService;
import com.my.business.modular.dictionary.dao.DictionaryDao;
import com.my.business.modular.dictionary.entity.Dictionary;
import com.my.business.modular.rollonoffline.entity.RollOnoffLine;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 设备履历综合台账接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@Service
public class BaseEquipmentServiceImpl implements BaseEquipmentService {

    @Autowired
    private BaseEquipmentDao baseEquipmentDao;

    @Autowired
    private DictionaryDao dictionaryDao;
    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataBaseEquipment(String data, Long userId, String sname) {
        try {
            Long time = 0L;
            JBaseEquipment jbaseEquipment = JSON.parseObject(data, JBaseEquipment.class);
            BaseEquipment baseEquipment = jbaseEquipment.getBaseEquipment();
            if(!StringUtils.isEmpty(baseEquipment.getDiscard_time())&&!StringUtils.isEmpty(baseEquipment.getUsetime())){
                time=DateUtil.getStringToDate("yyyy-MM-dd HH:mm:ss",baseEquipment.getUsetime()).getTime()
                        - DateUtil.getStringToDate("yyyy-MM-dd HH:mm:ss",baseEquipment.getDiscard_time()).getTime();
                baseEquipment.setTotal_times(BigDecimal.valueOf(time));
            }else{
                baseEquipment.setTotal_times(BigDecimal.ZERO);
            }
            if(!StringUtils.isEmpty(baseEquipment.getEquipment_id())){
                Dictionary d1 = dictionaryDao.findDataBaseEquipmentBySparentId(baseEquipment.getEquipment_id());
                if(d1!=null){
                    baseEquipment.setEquipment_parentid(d1.getIndocno());
                    baseEquipment.setEquipment_parentname(d1.getSname());
                    Dictionary d2 = dictionaryDao.findDataBaseEquipmentBySparentId(d1.getIndocno());
                    if(d2!=null){
                        baseEquipment.setEquipment_pparentid(d2.getIndocno());
                        baseEquipment.setEquipment_pparentname(d2.getSname());
                    }
                }
            }
            CodiUtil.newRecord(userId, sname, baseEquipment);

            baseEquipmentDao.insertDataBaseEquipment(baseEquipment);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataBaseEquipmentOne(Long indocno) {
        try {
            baseEquipmentDao.deleteDataBaseEquipmentOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataBaseEquipmentMany(String str_id) {
        try {
            String sql = "delete from base_equipment where indocno in(" + str_id + ")";
            baseEquipmentDao.deleteDataBaseEquipmentMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataBaseEquipment(String data, Long userId, String sname) {
        try {
            JBaseEquipment jbaseEquipment = JSON.parseObject(data, JBaseEquipment.class);
            BaseEquipment baseEquipment= jbaseEquipment.getBaseEquipment();
            if(!StringUtils.isEmpty(baseEquipment.getEquipment_id())){
                Dictionary d1 = dictionaryDao.findDataBaseEquipmentBySparentId(baseEquipment.getEquipment_id());
                if(d1!=null){
                    baseEquipment.setEquipment_parentid(d1.getIndocno());
                    baseEquipment.setEquipment_parentname(d1.getSname());
                    Dictionary d2 = dictionaryDao.findDataBaseEquipmentBySparentId(d1.getIndocno());
                    if(d2!=null){
                        baseEquipment.setEquipment_pparentid(d2.getIndocno());
                        baseEquipment.setEquipment_pparentname(d2.getSname());
                    }
                }
            }
            CodiUtil.editRecord(userId, sname, baseEquipment);
            baseEquipmentDao.updateDataBaseEquipment(baseEquipment);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataBaseEquipmentByPage(String data) {
        try {
            JBaseEquipment jbaseEquipment = JSON.parseObject(data, JBaseEquipment.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jbaseEquipment.getPageIndex();
            Integer pageSize = jbaseEquipment.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jbaseEquipment.getCondition()) {
                jsonObject = JSON.parseObject(jbaseEquipment.getCondition().toString());
            }else{
                jsonObject = new JSONObject();
            }

            String equipment_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("equipment_id"))) {
                equipment_id = jsonObject.get("equipment_id").toString();
            }
            //班次 班组 厂家 操作人  开始时间 结束时间  规格  配件名称(3) 组件名称(2) 设备名称(1)  是否更换
            String sclass_name = null;
            if (!StringUtils.isEmpty(jsonObject.get("sclass_name"))) {
                sclass_name = jsonObject.get("sclass_name").toString();
            }

            String sgroup_name = null;
            if (!StringUtils.isEmpty(jsonObject.get("sgroup_name"))) {
                sgroup_name = jsonObject.get("sgroup_name").toString();
            }

            String factory_name = null;
            if (!StringUtils.isEmpty(jsonObject.get("factory_name"))) {
                factory_name = jsonObject.get("factory_name").toString();
            }

            String operator_name = null;
            if (!StringUtils.isEmpty(jsonObject.get("operator_name"))) {
                operator_name = jsonObject.get("operator_name").toString();
            }

            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            String specifications = null;
            if (!StringUtils.isEmpty(jsonObject.get("specifications"))) {
                specifications = jsonObject.get("specifications").toString();
            }

            String equipment_name = null;
            if (!StringUtils.isEmpty(jsonObject.get("equipment_name"))) {
                equipment_name = jsonObject.get("equipment_name").toString();
            }

            String equipment_parentname = null;
            if (!StringUtils.isEmpty(jsonObject.get("equipment_parentname"))) {
                equipment_parentname = jsonObject.get("equipment_parentname").toString();
            }

            String equipment_pparentname = null;
            if (!StringUtils.isEmpty(jsonObject.get("equipment_pparentname"))) {
                equipment_pparentname = jsonObject.get("equipment_pparentname").toString();
            }
            String ifdiscard = null;
            if (!StringUtils.isEmpty(jsonObject.get("ifdiscard"))) {
                ifdiscard = jsonObject.get("ifdiscard").toString();
            }

            List<BaseEquipment> list = baseEquipmentDao.findDataBaseEquipmentByPage((pageIndex - 1) * pageSize, pageSize,sclass_name,sgroup_name,factory_name,operator_name,dbegin,dend,specifications,equipment_name,equipment_parentname,equipment_pparentname,ifdiscard,equipment_id);
            Integer count = baseEquipmentDao.findDataBaseEquipmentByPageSize(sclass_name,sgroup_name,factory_name,operator_name,dbegin,dend,specifications,equipment_name,equipment_parentname,equipment_pparentname,ifdiscard,equipment_id);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataBaseEquipmentByIndocno(String data) {
        try {
            JBaseEquipment jbaseEquipment= JSON.parseObject(data, JBaseEquipment.class);
            Long indocno = jbaseEquipment.getIndocno();
            BaseEquipment baseEquipment = baseEquipmentDao.findDataBaseEquipmentByIndocno(indocno);
            return ResultData.ResultDataSuccess(baseEquipment);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<BaseEquipment> findDataBaseEquipment() {
        List<BaseEquipment> list = baseEquipmentDao.findDataBaseEquipment();
        return list;
    }


    public ResultData updateDataBaseEquipmentDiscardTime(String data) {
        try {
            JBaseEquipment jbaseEquipment= JSON.parseObject(data, JBaseEquipment.class);
            Long indocno = jbaseEquipment.getIndocno();
            baseEquipmentDao.updateDataBaseEquipmentDiscardTime(indocno);
            return ResultData.ResultDataSuccessSelf("修改数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }


    public ResultData updateDataBaseEquipmentDiscardTimeCancel(String data) {
        try {
            JBaseEquipment jbaseEquipment= JSON.parseObject(data, JBaseEquipment.class);
            Long indocno = jbaseEquipment.getIndocno();
            baseEquipmentDao.updateDataBaseEquipmentDiscardTimeCancel(indocno);
            return ResultData.ResultDataSuccessSelf("修改数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public ResultData findDataDictionary(String data) {
        try {
        JBaseEquipment jbaseEquipment = JSON.parseObject(data, JBaseEquipment.class);
        JSONObject jsonObject = null;
        if (null != jbaseEquipment.getCondition()) {
            jsonObject = JSON.parseObject(jbaseEquipment.getCondition().toString());
        }else{
            jsonObject = new JSONObject();
        }
        List<Dictionary> last = new ArrayList<>();
        if(!StringUtils.isEmpty(jsonObject.get("dicno"))){
            String dicno = jsonObject.get("dicno").toString();;
            List<Dictionary> list = dictionaryDao.findDataDictionaryByDicno(1L,dicno+"01");
            for (Dictionary entity : list){//循环1级节点
                List<Dictionary> listN = dictionaryDao.findDataDictionary(2L,entity.getIndocno());
                for (Dictionary ent : listN){//循环2级节点
                    List<Dictionary> listL = dictionaryDao.findDataDictionary(3L,ent.getIndocno());
                    ent.setDetail(listL);
                }
                entity.setDetail(listN);
                last.add(entity);
            }
        }
            return ResultData.ResultDataSuccess(last);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

}
