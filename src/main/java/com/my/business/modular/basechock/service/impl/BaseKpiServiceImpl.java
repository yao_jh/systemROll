package com.my.business.modular.basechock.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.basechock.dao.BaseCostAccountingChildDao;
import com.my.business.modular.basechock.dao.BaseKpiDao;
import com.my.business.modular.basechock.entity.BaseCostAccountingChild;
import com.my.business.modular.basechock.entity.BaseKpi;
import com.my.business.modular.basechock.entity.JBaseCostAccountingChild;
import com.my.business.modular.basechock.entity.JBaseKpi;
import com.my.business.modular.basechock.service.BaseCostAccountingChildService;
import com.my.business.modular.basechock.service.BaseKpiService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 料号规格定制子表 综合台账接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@Service
public class BaseKpiServiceImpl implements BaseKpiService {

    @Autowired
    private BaseKpiDao baseKpiDao;



    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataBaseKpi(String data, Long userId, String sname) {
        try {
            JBaseKpi jBaseKpi = JSON.parseObject(data, JBaseKpi.class);
            BaseKpi baseKpi = jBaseKpi.getBaseKpi();
            CodiUtil.editRecord(userId, sname, baseKpi);
            baseKpiDao.updateDataBaseKpi(baseKpi);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataBaseKpiByPage(String data) {
        try {
            JBaseKpi jBaseKpi= JSON.parseObject(data, JBaseKpi.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jBaseKpi.getPageIndex();
            Integer pageSize = jBaseKpi.getPageSize();
            if (null != jBaseKpi.getCondition()) {
                jsonObject = JSON.parseObject(jBaseKpi.getCondition().toString());
            }else{
                jsonObject = new JSONObject();
            }
            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }
            List<BaseKpi>  list = baseKpiDao.findDataBaseKpiByPage((pageIndex - 1) * pageSize, pageSize);
            Integer count = baseKpiDao.findDataBaseKpiByPageSize();
            return ResultData.ResultDataSuccess(list,count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataBaseKpiByIndocno(String data) {
        try {
            JBaseKpi jBaseKpi= JSON.parseObject(data, JBaseKpi.class);
            Long indocno = jBaseKpi.getIndocno();
            BaseKpi baseKpi = baseKpiDao.findDataBaseKpiByIndocno(indocno);
            return ResultData.ResultDataSuccess(baseKpi);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

}
