package com.my.business.modular.basechock.dao;

import com.my.business.modular.basechock.entity.BaseBearing;
import com.my.business.modular.basechock.entity.BaseChock;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轴承综合台账dao接口
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@Mapper
public interface BaseBearingDao {

    /**
     * 添加记录
     *
     * @param baseChock 对象实体
     */
    void insertDataBaseBearing(BaseBearing baseBearing);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataBaseBearingOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataBaseBearingMany(String value);

    /**
     * 修改记录
     *
     * @param baseChock 对象实体
     */
    void updateDataBaseBearing(BaseBearing baseBearing);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<BaseBearing> findDataBaseBearingByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("chock_no") String chock_no,@Param("bearing_no") String bearing_no,@Param("ifdiscard") Integer ifdiscard,@Param("bearing_type") Integer bearing_type,@Param("seals_type") String seals_type);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataBaseBearingByPageSize(@Param("chock_no") String chock_no,@Param("bearing_no") String bearing_no,@Param("ifdiscard") Integer ifdiscard,@Param("bearing_type") Integer bearing_type,@Param("seals_type") String seals_type);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    BaseBearing findDataBaseBearingByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<BaseBearing> findDataBaseBearing();

    BaseBearing findDataBaseBearingByBearingNo(@Param("bearing_no") String bearing_no);

    void updateDataBaseBearingDiscardTime(@Param("indocno") Long indocno);

    void updateDataBaseBearingSingleTimes(@Param("indocno") Long indocno);

    List<BaseBearing> findListByBearingType(@Param("bearing_type") int bearing_type,@Param("seals_type") String seals_type);

    void updateDataBaseBearingDiscardTimeCancel(@Param("indocno")Long indocno);
}
