package com.my.business.modular.basechock.dao;

import com.my.business.modular.basechock.entity.BaseCostAccountingMain;
import com.my.business.modular.basechock.entity.BaseCostAccountingReport;
import com.my.business.modular.basechock.entity.BaseEquipment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 料号规格定制主表综合台账dao接口
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@Mapper
public interface BaseCostAccountingMainDao {

    /**
     * 添加记录
     *
     * @param baseChock 对象实体
     */
    void insertDataBaseCostAccountingMain(BaseCostAccountingMain baseCostAccountingMain);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataBaseCostAccountingMainOne(@Param("indocno") Long indocno);

    /**
     * 修改记录
     *
     * @param baseChock 对象实体
     */
    void updateDataBaseCostAccountingMain(BaseCostAccountingMain baseCostAccountingMain);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<BaseCostAccountingMain> findDataBaseCostAccountingMainByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,
                                                                      @Param("material_noid") String material_noid,@Param("specifications_noid") String specifications_noid,
                                                                      @Param("roll_typeid") String roll_typeid,@Param("material_id") String material_id,
                                                                      @Param("framerangeid") String framerangeid
                                                                      );
    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataBaseCostAccountingMainByPageSize(
            @Param("material_noid") String material_noid,@Param("specifications_noid") String specifications_noid,
            @Param("roll_typeid") String roll_typeid,@Param("material_id") String material_id,
            @Param("framerangeid") String framerangeid
    );

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    BaseCostAccountingMain findDataBaseCostAccountingMainByIndocno(@Param("indocno") Long indocno);


    Integer findDataBaseCostAccountingByPageSize( @Param("material_noid") String material_noid,@Param("specifications_noid") String specifications_noid,
                                                  @Param("roll_typeid") String roll_typeid,@Param("material_id") String material_id,
                                                  @Param("framerangeid") String framerangeid,@Param("dbegin") String dbegin,@Param("dend") String dend);

    List<BaseCostAccountingReport> findDataBaseCostAccountingByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,
                                                                        @Param("material_noid") String material_noid, @Param("specifications_noid") String specifications_noid,
                                                                        @Param("roll_typeid") String roll_typeid, @Param("material_id") String material_id,
                                                                        @Param("framerangeid") String framerangeid,@Param("dbegin") String dbegin,@Param("dend") String dend
    );

    List<BaseCostAccountingReport> findBaseCostAccountingByReportByPage(@Param("roll_no")String roll_no, @Param("factory_id")String factory_id,
                                                                        @Param("frame_noid")String frame_noid, @Param("dbegin")String dbegin,
                                                                        @Param("dend")String dend
    ,@Param("material_id") String  material_id);

    List<BaseCostAccountingReport> findBaseCostAccountingABigScreenByPage( @Param("material_noid") String material_noid, @Param("specifications_noid") String specifications_noid,
                                                                           @Param("roll_typeid") String roll_typeid, @Param("material_id") String material_id,
                                                                           @Param("framerangeid") String framerangeid,@Param("dbegin") String dbegin,@Param("dend") String dend
    );
}
