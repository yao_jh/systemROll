package com.my.business.modular.basechock.service;

import com.my.business.sys.common.entity.ResultData;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * 轧辊安全期提醒  综合台账接口服务类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
public interface BaseRollSafetyReminderService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertBaseRollSafetyReminder(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteBaseRollSafetyReminderOne(Long indocno);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateBaseRollSafetyReminder(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findBaseRollSafetyReminderByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findBaseRollSafetyReminderByIndocno(String data);


    ResultData findBaseRollSafetyReminder(String data);

    String excelBaseRollSafetyReminder(HSSFWorkbook workbook
    ,String roll_typeid
                                       ,String material_id
                                       ,String framerangeid
                                       ,String dbegin
    );
}
