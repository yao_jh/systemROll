package com.my.business.modular.basechock.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.basechock.entity.BaseChock;
import com.my.business.modular.basechock.entity.JBaseChock;
import com.my.business.modular.basechock.service.BaseChockService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 轴承座综合台账控制器层
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@RestController
@RequestMapping("/baseChock")
public class BaseChockController {

    @Autowired
    private BaseChockService baseChockService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataBaseChock(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return baseChockService.insertDataBaseChock(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataBaseChockOne(@RequestBody String data) {
        try {
            JBaseChock jbaseChock = JSON.parseObject(data, JBaseChock.class);
            return baseChockService.deleteDataBaseChockOne(jbaseChock.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JBaseChock jbaseChock = JSON.parseObject(data, JBaseChock.class);
            return baseChockService.deleteDataBaseChockMany(jbaseChock.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataBaseChock(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return baseChockService.updateDataBaseChock(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataBaseChockByPage(@RequestBody String data) {
        return baseChockService.findDataBaseChockByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataBaseChockByIndocno(@RequestBody String data) {
        return baseChockService.findDataBaseChockByIndocno(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByChockNo"}, method = RequestMethod.POST)
    @ResponseBody
    public BaseChock findDataBaseChockByChockNo(@RequestBody String data) {
        return baseChockService.findDataBaseChockByChockNo(data);
    }
    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<BaseChock> findDataBaseChock() {
        return baseChockService.findDataBaseChock();
    }

    /**
     * 根据类型 1 2 3  查询 符合条件的轴承 密封件 --砂轮
     */
    @CrossOrigin
    @RequestMapping(value = {"/findListByBaseChock"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findListByBaseChock(@RequestBody String data) {
        return baseChockService.findListByBaseChock(data);
    }

    /**
     *  单次时间提醒
     */
    @CrossOrigin
    @RequestMapping(value = {"/updateSingleTimes"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataBaseChockSingleTimes(@RequestBody String data) {
        return baseChockService.updateDataBaseChockSingleTimes(data);
    }

    /**
     *  报废处理
     */
    @CrossOrigin
    @RequestMapping(value = {"/updateDiscardTime"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataBaseChockDiscardTime(@RequestBody String data) {
        return baseChockService.updateDataBaseChockDiscardTime(data);
    }
    /**
     *  取消报废处理
     */
    @CrossOrigin
    @RequestMapping(value = {"/updateDiscardTimeCancel"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataBaseChockDiscardTimeCancel(@RequestBody String data) {
        return baseChockService.updateDataBaseChockDiscardTimeCancel(data);
    }
}
