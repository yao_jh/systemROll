package com.my.business.modular.basechock.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.basechock.entity.JBaseEquipment;
import com.my.business.modular.basechock.entity.JBaseRollSafetyReminder;
import com.my.business.modular.basechock.service.BaseCostAccountingMainService;
import com.my.business.modular.basechock.service.BaseRollSafetyReminderService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;


/**
 *  轧辊安全期提醒  综合台账控制器层
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@RestController
@RequestMapping("/baseRollSafetyReminder")
public class BaseRollSafetyReminderController {

    @Autowired
    private BaseRollSafetyReminderService baseRollSafetyReminderService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertBaseRollSafetyReminder(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return baseRollSafetyReminderService.insertBaseRollSafetyReminder(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteBaseRollSafetyReminderOne(@RequestBody String data) {
        try {
            JBaseRollSafetyReminder jBaseRollSafetyReminder = JSON.parseObject(data, JBaseRollSafetyReminder.class);
            return baseRollSafetyReminderService.deleteBaseRollSafetyReminderOne(jBaseRollSafetyReminder.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateBaseRollSafetyReminder(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return baseRollSafetyReminderService.updateBaseRollSafetyReminder(data, userId, CodiUtil.returnLm(sname));
    }



    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findBaseRollSafetyReminderByPage(@RequestBody String data) {
        return baseRollSafetyReminderService.findBaseRollSafetyReminderByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findBaseRollSafetyReminderByIndocno(@RequestBody String data) {
        return baseRollSafetyReminderService.findBaseRollSafetyReminderByIndocno(data);
    }

    /**
     * 轧辊安全期提醒
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findBaseRollSafetyReminder"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findBaseRollSafetyReminder(@RequestBody String data) {
        return baseRollSafetyReminderService.findBaseRollSafetyReminder(data);
    }
    /**
     * 导出 轧辊安全期提醒
     */
    @CrossOrigin
    @RequestMapping(value = {"/excelBaseRollSafetyReminder"}, method = RequestMethod.GET)
    @ResponseBody
    public void getExcelBaseRollSafetyReminder(
            @RequestParam ( "roll_typeid" ) String roll_typeid,
            @RequestParam ( "material_id" ) String material_id,
            @RequestParam ( "framerangeid" ) String framerangeid,
            @RequestParam ( "dbegin" ) String dbegin,
            HttpServletResponse response, HttpServletRequest request) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        String fileName = "";//设置要导出的文件的名字
        fileName = baseRollSafetyReminderService.excelBaseRollSafetyReminder(workbook
        ,roll_typeid
                ,material_id
                ,framerangeid
                ,dbegin
        );
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1) + ".xls");//URLEncoder.encode(fileName, "utf-8")
        workbook.write(response.getOutputStream());
    }
}
