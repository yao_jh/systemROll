package com.my.business.modular.basechock.service;

import com.my.business.modular.basechock.entity.BaseBearing;
import com.my.business.modular.basechock.entity.BaseChock;
import com.my.business.modular.rollonoffline.entity.RollOnoffLine;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 轴承综合台账接口服务类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
public interface BaseBearingService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataBaseBearing(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataBaseBearingOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataBaseBearingMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataBaseBearing(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataBaseBearingByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataBaseBearingByIndocno(String data);

    /**
     * 查看记录
     */
    List<BaseBearing> findDataBaseBearing();

    void changeBearingData(RollOnoffLine rollOnoffLine);

    ResultData updateDataBaseBearingSingleTimes(String data);

    ResultData updateDataBaseBearingDiscardTime(String data);

    ResultData updateDataBaseBearingByChockNo(String data);

    ResultData findListByBearingType(String data);

    ResultData updateDataBaseBearingDiscardTimeCancel(String data);
}
