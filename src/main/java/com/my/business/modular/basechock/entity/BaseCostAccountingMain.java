package com.my.business.modular.basechock.entity;

import com.my.business.modular.dictionary.entity.Dictionary;
import com.my.business.sys.common.entity.BaseEntity;

import java.math.BigDecimal;
import java.util.List;

/**
 * 料号规格定制主表综合台账实体类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
public class BaseCostAccountingMain extends BaseEntity {

    private Long indocno;  //主键
    private Long material_noid;//料号id
    private String material_no;  //料号名称

    private Long specifications_noid;//规格id
    private String specifications_no;  //规格名称

    private Long roll_typeid;//轧辊类型id
    private String roll_type;  //轧辊类型名称

    private Long material_id;//材质id
    private String material;  //材质名称

    private Long framerangeid;//机架范围id
    private String framerange;  //机架范围名称

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getMaterial_noid() {
        return material_noid;
    }

    public void setMaterial_noid(Long material_noid) {
        this.material_noid = material_noid;
    }

    public String getMaterial_no() {
        return material_no;
    }

    public void setMaterial_no(String material_no) {
        this.material_no = material_no;
    }

    public Long getSpecifications_noid() {
        return specifications_noid;
    }

    public void setSpecifications_noid(Long specifications_noid) {
        this.specifications_noid = specifications_noid;
    }

    public String getSpecifications_no() {
        return specifications_no;
    }

    public void setSpecifications_no(String specifications_no) {
        this.specifications_no = specifications_no;
    }

    public Long getRoll_typeid() {
        return roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public Long getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(Long material_id) {
        this.material_id = material_id;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public Long getFramerangeid() {
        return framerangeid;
    }

    public void setFramerangeid(Long framerangeid) {
        this.framerangeid = framerangeid;
    }

    public String getFramerange() {
        return framerange;
    }

    public void setFramerange(String framerange) {
        this.framerange = framerange;
    }
}