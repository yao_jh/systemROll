package com.my.business.modular.basechock.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.basechock.dao.BaseChockDao;
import com.my.business.modular.basechock.dao.BaseChockHistoryDao;
import com.my.business.modular.basechock.entity.BaseChock;
import com.my.business.modular.basechock.entity.BaseChockHistory;
import com.my.business.modular.basechock.entity.JBaseBearing;
import com.my.business.modular.basechock.entity.JBaseChock;
import com.my.business.modular.basechock.service.BaseChockService;
import com.my.business.modular.chockdismounting.dao.ChockDismountingDao;
import com.my.business.modular.chockdismounting.entity.ChockDismounting;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.modular.rollpaired.dao.RollPairedDao;
import com.my.business.modular.rollpaired.entity.RollPaired;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.util.DateUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * 轴承座综合台账接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@Service
public class BaseChockServiceImpl implements BaseChockService {

    @Autowired
    private BaseChockDao baseChockDao;

    @Autowired
    private BaseChockHistoryDao baseChockHistoryDao;

    @Autowired
    private ChockDismountingDao chockDismountingDao;

    @Autowired
    private RollInformationDao rollInformationDao;
    @Autowired
    private RollPairedDao rollPairedDao;
    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataBaseChock(String data, Long userId, String sname) {
        try {
            JBaseChock jbaseChock = JSON.parseObject(data, JBaseChock.class);
            BaseChock baseChock = jbaseChock.getBaseChock();
            CodiUtil.newRecord(userId, sname, baseChock);

            RollInformation rollInformation = new RollInformation();

            if(!StringUtils.isEmpty(baseChock.getRoll_no())){
                rollInformation = rollInformationDao.findDataRollInformationByRollNo(baseChock.getRoll_no());
                if(rollInformation!=null){
                    baseChock.setRoll_type(rollInformation.getRoll_type());
                    baseChock.setRoll_typeid(rollInformation.getRoll_typeid());
                    baseChock.setFrame_no(rollInformation.getFrame_no());
                    baseChock.setFrame_noid(rollInformation.getFrame_noid());
                    baseChock.setProduction_line(rollInformation.getProduction_line());
                    baseChock.setProduction_line_id(rollInformation.getProduction_line_id());
                }
                List<ChockDismounting> newList = chockDismountingDao.findDataChockDismountingByPage(0, 1, baseChock.getRoll_no(), null, null, null, null);
                if(newList.size()>0){
                    for(ChockDismounting  chockDismounting:newList){
                        if(baseChock.getInstall_location_id()==1||baseChock.getInstall_location_id()==1L){//1os侧   2 ds侧
                            chockDismounting.setOs_no(baseChock.getChock_no());
                            chockDismountingDao.updateDataChockDismounting(chockDismounting);
                        }else if(baseChock.getInstall_location_id()==2||baseChock.getInstall_location_id()==2L){
                            chockDismounting.setDs_no(baseChock.getChock_no());
                            chockDismountingDao.updateDataChockDismounting(chockDismounting);
                        }

                        String os_no = null;
                        String ds_no = null;
                        String roll_no = chockDismounting.getRoll_no();
                        String bc_no = null;
                        String sj_no = null;
                        Long sjid_no = 0L;
                        if (!StringUtils.isEmpty(chockDismounting.getUp_location_id())){
                            sjid_no = chockDismounting.getUp_location_id();
                        }
                        if (!StringUtils.isEmpty(chockDismounting.getUp_location())){
                            sj_no = chockDismounting.getUp_location();
                        }

                        if (!StringUtils.isEmpty(chockDismounting.getOs_no())){
                            os_no = chockDismounting.getOs_no();
                        }
                        if (!StringUtils.isEmpty(chockDismounting.getDs_no())){
                            ds_no = chockDismounting.getDs_no();
                        }
                        bc_no = os_no + "-" + ds_no;
                        rollInformationDao.updateChockNo(os_no,ds_no,bc_no,roll_no,sj_no,sjid_no);

                    }
                }else{
                    ChockDismounting newChockDismounting = new ChockDismounting();
                    newChockDismounting.setRoll_no(baseChock.getRoll_no());
                    newChockDismounting.setRoll_typeid(baseChock.getRoll_typeid());
                    newChockDismounting.setRoll_type(baseChock.getRoll_type());
                    newChockDismounting.setProduction_line_id(baseChock.getProduction_line_id());
                    newChockDismounting.setProduction_line(baseChock.getProduction_line());
                    newChockDismounting.setFrame_noid(baseChock.getFrame_noid());
                    newChockDismounting.setFrame_no(baseChock.getFrame_no());
                    newChockDismounting.setInstall_location_id(baseChock.getInstall_location_id());
                    newChockDismounting.setInstall_location(baseChock.getInstall_location());
                    newChockDismounting.setUp_location_id(baseChock.getUp_location_id());
                    newChockDismounting.setUp_location(baseChock.getUp_location());
                    if(baseChock.getInstall_location_id()==1||baseChock.getInstall_location_id()==1L){//1os侧   2 ds侧
                        newChockDismounting.setOs_no(baseChock.getChock_no());
                    }else if(baseChock.getInstall_location_id()==2||baseChock.getInstall_location_id()==2L){
                        newChockDismounting.setDs_no(baseChock.getChock_no());
                    }

                    String os_no = null;
                    String ds_no = null;
                    String roll_no = newChockDismounting.getRoll_no();
                    String bc_no = null;
                    String sj_no = null;
                    Long sjid_no = 0L;
                    if (!StringUtils.isEmpty(newChockDismounting.getUp_location_id())){
                        sjid_no = newChockDismounting.getUp_location_id();
                    }
                    if (!StringUtils.isEmpty(newChockDismounting.getUp_location())){
                        sj_no = newChockDismounting.getUp_location();
                    }
                    if (!StringUtils.isEmpty(newChockDismounting.getOs_no())){
                        os_no = newChockDismounting.getOs_no();
                    }
                    if (!StringUtils.isEmpty(newChockDismounting.getDs_no())){
                        ds_no = newChockDismounting.getDs_no();
                    }
                    bc_no = os_no + "-" + ds_no;
                    rollInformationDao.updateChockNo(os_no,ds_no,bc_no,roll_no,sj_no,sjid_no);
                    chockDismountingDao.insertDataChockDismounting(newChockDismounting);
                }
            }

            if(!StringUtils.isEmpty(baseChock.getChock_no())&&baseChockDao.findDataBaseChockByChockNo(baseChock.getChock_no())!=null){
                return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + "重复请重新输入", null);
            }

            baseChockDao.insertDataBaseChock(baseChock);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataBaseChockOne(Long indocno) {

        BaseChock oldBean = new BaseChock();
        BaseChockHistory bean  = new BaseChockHistory();
        oldBean = baseChockDao.findDataBaseChockByIndocno(indocno);
        if(!StringUtils.isEmpty(oldBean.getRoll_no())){
            BeanUtils.copyProperties(oldBean,bean);
            bean.setStopusetime(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));
            baseChockHistoryDao.insertDataBaseChockHistory(bean);
            //联动修改信息
            // 更改 原来此辊子在 解封表中的字段为空
            List<ChockDismounting> oldList = chockDismountingDao.findDataChockDismountingByPage(0, 1, oldBean.getRoll_no(), null, null, null, null);
            if(oldList.size()>0){
                for(ChockDismounting  chockDismounting:oldList){
                    if(oldBean.getInstall_location_id()==1||oldBean.getInstall_location_id()==1L){//1os侧   2 ds侧
                        chockDismounting.setOs_no(null);
                        chockDismountingDao.updateDataChockDismounting(chockDismounting);
                    }else if(oldBean.getInstall_location_id()==2||oldBean.getInstall_location_id()==2L){
                        chockDismounting.setDs_no(null);
                        chockDismountingDao.updateDataChockDismounting(chockDismounting);
                    }
                    String os_no = null;
                    String ds_no = null;
                    String roll_no = chockDismounting.getRoll_no();
                    String bc_no = null;
                    String sj_no = null;
                    rollInformationDao.updateChockNo(os_no,ds_no,bc_no,roll_no,sj_no,0L);
                }
            }

        }

        try {
            baseChockDao.deleteDataBaseChockOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataBaseChockMany(String str_id) {
        try {
            String sql = "delete from base_chock where indocno in(" + str_id + ")";
            baseChockDao.deleteDataBaseChockMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataBaseChock(String data, Long userId, String sname) {
        try {
            JBaseChock jbaseChock = JSON.parseObject(data, JBaseChock.class);
            BaseChock baseChock = jbaseChock.getBaseChock();
            CodiUtil.editRecord(userId, sname, baseChock);

            //  修改 轴承座台账时 如果更改了 辊号 那么 保存历史记录 且 更改拆封的数据
            BaseChock oldBean = new BaseChock();
            BaseChockHistory bean = new BaseChockHistory();
            oldBean = baseChockDao.findDataBaseChockByIndocno(baseChock.getIndocno());
            if(!StringUtils.isEmpty(oldBean.getRoll_no())&&StringUtils.isEmpty(baseChock.getRoll_no())){
                if(!baseChock.getRoll_no().equals(oldBean.getRoll_no())){
                    BeanUtils.copyProperties(oldBean,bean);
                    bean.setStopusetime(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));
                    baseChockHistoryDao.insertDataBaseChockHistory(bean);
                }
                // 更改 原来此辊子在 解封表中的字段为空
                List<ChockDismounting> oldList = chockDismountingDao.findDataChockDismountingByPage(0, 1, oldBean.getRoll_no(), null, null, null, null);
                if(oldList.size()>0){
                    for(ChockDismounting  chockDismounting:oldList){
                        if(oldBean.getInstall_location_id()==1||oldBean.getInstall_location_id()==1L){//1os侧   2 ds侧
                            chockDismounting.setOs_no(null);
                            chockDismountingDao.updateDataChockDismounting(chockDismounting);
                        }else if(oldBean.getInstall_location_id()==2||oldBean.getInstall_location_id()==2L){
                            chockDismounting.setDs_no(null);
                            chockDismountingDao.updateDataChockDismounting(chockDismounting);
                        }
                        String os_no = null;
                        String ds_no = null;
                        String roll_no = chockDismounting.getRoll_no();
                        String bc_no = null;
                        String sj_no = null;
                        Long sjid_no = 0L;
                        if (!StringUtils.isEmpty(chockDismounting.getUp_location_id())){
                            sjid_no = chockDismounting.getUp_location_id();
                        }
                        if (!StringUtils.isEmpty(chockDismounting.getUp_location())){
                            sj_no = chockDismounting.getUp_location();
                        }
                        if (!StringUtils.isEmpty(chockDismounting.getOs_no())){
                            os_no = chockDismounting.getOs_no();
                        }
                        if (!StringUtils.isEmpty(chockDismounting.getDs_no())){
                            ds_no = chockDismounting.getDs_no();
                        }
                        bc_no = os_no + "-" + ds_no;
                        rollInformationDao.updateChockNo(os_no,ds_no,bc_no,roll_no,sj_no,sjid_no);
                    }
                }
            }

                if(!StringUtils.isEmpty(baseChock.getRoll_no())){
                    RollInformation rollInformation = new RollInformation();
                    rollInformation = rollInformationDao.findDataRollInformationByRollNo(baseChock.getRoll_no());
                    if(rollInformation!=null){
                        baseChock.setRoll_type(rollInformation.getRoll_type());
                        baseChock.setRoll_typeid(rollInformation.getRoll_typeid());
                        baseChock.setFrame_no(rollInformation.getFrame_no());
                        baseChock.setFrame_noid(rollInformation.getFrame_noid());
                        baseChock.setProduction_line(rollInformation.getProduction_line());
                        baseChock.setProduction_line_id(rollInformation.getProduction_line_id());
                    }

                    List<ChockDismounting> newList = chockDismountingDao.findDataChockDismountingByPage(0, 1, baseChock.getRoll_no(), null, null, null, null);
                    if(newList.size()>0){
                        for(ChockDismounting  chockDismounting:newList){
                            if(baseChock.getInstall_location_id()==1||baseChock.getInstall_location_id()==1L){//1os侧   2 ds侧
                                chockDismounting.setOs_no(baseChock.getChock_no());
                                chockDismountingDao.updateDataChockDismounting(chockDismounting);
                            }else if(baseChock.getInstall_location_id()==2||baseChock.getInstall_location_id()==2L) {
                                chockDismounting.setDs_no(baseChock.getChock_no());
                                chockDismountingDao.updateDataChockDismounting(chockDismounting);
                            }
                                String os_no = null;
                                String ds_no = null;
                                String roll_no = chockDismounting.getRoll_no();
                                String bc_no = null;
                                String sj_no = null;
                                Long sjid_no = 0L;
                                if (!StringUtils.isEmpty(chockDismounting.getUp_location_id())){
                                    sjid_no = chockDismounting.getUp_location_id();
                                }
                                if (!StringUtils.isEmpty(chockDismounting.getUp_location())){
                                    sj_no = chockDismounting.getUp_location();
                                }
                                if (!StringUtils.isEmpty(chockDismounting.getOs_no())){
                                    os_no = chockDismounting.getOs_no();
                                }
                                if (!StringUtils.isEmpty(chockDismounting.getDs_no())){
                                    ds_no = chockDismounting.getDs_no();
                                }
                                bc_no = os_no + "-" + ds_no;
                                rollInformationDao.updateChockNo(os_no,ds_no,bc_no,roll_no,sj_no,sjid_no);
                        }
                    }else{
                        ChockDismounting newChockDismounting = new ChockDismounting();
                        newChockDismounting.setRoll_no(baseChock.getRoll_no());
                        newChockDismounting.setRoll_typeid(baseChock.getRoll_typeid());
                        newChockDismounting.setRoll_type(baseChock.getRoll_type());
                        newChockDismounting.setProduction_line_id(baseChock.getProduction_line_id());
                        newChockDismounting.setProduction_line(baseChock.getProduction_line());
                        newChockDismounting.setFrame_noid(baseChock.getFrame_noid());
                        newChockDismounting.setFrame_no(baseChock.getFrame_no());
                        newChockDismounting.setInstall_location_id(baseChock.getInstall_location_id());
                        newChockDismounting.setInstall_location(baseChock.getInstall_location());
                        newChockDismounting.setUp_location_id(baseChock.getUp_location_id());
                        newChockDismounting.setUp_location(baseChock.getUp_location());
                        if(baseChock.getInstall_location_id()==1||baseChock.getInstall_location_id()==1L){//1os侧   2 ds侧
                            newChockDismounting.setOs_no(baseChock.getChock_no());
                        }else if(baseChock.getInstall_location_id()==2||baseChock.getInstall_location_id()==2L){
                            newChockDismounting.setDs_no(baseChock.getChock_no());
                        }
                        String os_no = null;
                        String ds_no = null;
                        String roll_no = newChockDismounting.getRoll_no();
                        String bc_no = null;
                        String sj_no = null;

                        if (!StringUtils.isEmpty(newChockDismounting.getUp_location())){
                            sj_no = newChockDismounting.getUp_location();
                        }
                        Long sjid_no = 0L;
                        if (!StringUtils.isEmpty(newChockDismounting.getUp_location_id())){
                            sjid_no = newChockDismounting.getUp_location_id();
                        }
                        if (!StringUtils.isEmpty(newChockDismounting.getOs_no())){
                            os_no = newChockDismounting.getOs_no();
                        }
                        if (!StringUtils.isEmpty(newChockDismounting.getDs_no())){
                            ds_no = newChockDismounting.getDs_no();
                        }
                        bc_no = os_no + "-" + ds_no;
                        rollInformationDao.updateChockNo(os_no,ds_no,bc_no,roll_no,sj_no,sjid_no);
                        chockDismountingDao.insertDataChockDismounting(newChockDismounting);
                    }
                }
                if(!StringUtils.isEmpty(baseChock.getSingle_times())){
                    baseChock.setSingle_times((baseChock.getSingle_times().multiply(BigDecimal.valueOf(60*60*24))));
                }
            if(!StringUtils.isEmpty(baseChock.getTotal_times())){
                baseChock.setTotal_times((baseChock.getTotal_times().multiply(BigDecimal.valueOf(60*60*24))));
            }
            baseChockDao.updateDataBaseChock(baseChock);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataBaseChockByPage(String data) {
        try {
            JBaseChock jbaseChock = JSON.parseObject(data, JBaseChock.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jbaseChock.getPageIndex();
            Integer pageSize = jbaseChock.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jbaseChock.getCondition()) {
                jsonObject = JSON.parseObject(jbaseChock.getCondition().toString());
            }

            String chock_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("chock_no"))) {
                chock_no = jsonObject.get("chock_no").toString();
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

			Long frame_noid = null;
			if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
				frame_noid = Long.valueOf(jsonObject.get("frame_noid").toString());
			}

			String frame_no = null;
			if (!StringUtils.isEmpty(jsonObject.get("frame_no"))) {
				frame_no = jsonObject.get("frame_no").toString();
			}

			Long production_line_id = null;
			if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
				production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
			}

			Long roll_typeid = null;
			if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
				roll_typeid = Long.valueOf(jsonObject.get("roll_typeid").toString());
			}

			Long istatus = null;
			if (!StringUtils.isEmpty(jsonObject.get("istatus"))) {
				istatus = Long.valueOf(jsonObject.get("istatus").toString());
			}

            Integer ifdiscard = 0;
            if (!StringUtils.isEmpty(jsonObject.get("ifdiscard"))) {
                ifdiscard = Integer.valueOf(jsonObject.get("ifdiscard").toString());
            }

            List<BaseChock> list = baseChockDao.findDataBaseChockByPage((pageIndex - 1) * pageSize, pageSize,chock_no,roll_no,frame_noid,frame_no,production_line_id,roll_typeid,istatus,ifdiscard);
            Integer count = baseChockDao.findDataBaseChockByPageSize(chock_no,roll_no,frame_noid,frame_no,production_line_id,roll_typeid,istatus,ifdiscard);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataBaseChockByIndocno(String data) {
        try {
            JBaseChock jbaseChock = JSON.parseObject(data, JBaseChock.class);
            Long indocno = jbaseChock.getIndocno();

            BaseChock baseChock = baseChockDao.findDataBaseChockByIndocno(indocno);
            return ResultData.ResultDataSuccess(baseChock);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<BaseChock> findDataBaseChock() {
        List<BaseChock> list = baseChockDao.findDataBaseChock();
        return list;
    }

    public ResultData findListByBaseChock(String data) {
        try {
            JBaseChock jbaseChock = JSON.parseObject(data, JBaseChock.class);
            BaseChock baseChock = jbaseChock.getBaseChock();
            String  install_location_id = null;
            String  up_location_id = null;
            if (!StringUtils.isEmpty(baseChock.getRoll_no())) {
                RollPaired r1 = rollPairedDao.findDataRollPairedByRollUpOther(baseChock.getRoll_no(), null, null,null);
                if(r1!=null){
                    up_location_id = 1+"";//TOP
                }
                RollPaired r2 = rollPairedDao.findDataRollPairedByRollDownOther(baseChock.getRoll_no(), null, null,null);
                if(r2!=null){
                    up_location_id = 2+"";//TOP
                }
            }
            if(baseChock.getInstall_location_id()==1||baseChock.getInstall_location_id()==1L){
                install_location_id=1+"";
            }else if(baseChock.getInstall_location_id()==2||baseChock.getInstall_location_id()==2L){
                install_location_id=2+"";
            }
            List<BaseChock> list = baseChockDao.findListByBaseChock(install_location_id,up_location_id);
            return ResultData.ResultDataSuccess(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public BaseChock findDataBaseChockByChockNo(String data) {
            JBaseChock jbaseChock = JSON.parseObject(data, JBaseChock.class);
            BaseChock bean = jbaseChock.getBaseChock();
            String chockNo = bean.getChock_no();
            BaseChock baseChock = baseChockDao.findDataBaseChockByChockNo(chockNo);
            return baseChock;
    }


    public ResultData updateDataBaseChockSingleTimes(String data) {
        try {
            JBaseChock jbaseChock = JSON.parseObject(data, JBaseChock.class);
            Long indocno = jbaseChock.getIndocno();

            baseChockDao.updateDataBaseChockSingleTimes(indocno);
            return ResultData.ResultDataSuccessSelf("修改数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    @Override
    public ResultData updateDataBaseChockDiscardTimeCancel(String data) {
        try {
            JBaseChock jbaseChock = JSON.parseObject(data, JBaseChock.class);
            Long indocno = jbaseChock.getIndocno();

            baseChockDao.updateDataBaseChockDiscardTimeCancel(indocno);
            return ResultData.ResultDataSuccessSelf("修改数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public ResultData updateDataBaseChockDiscardTime(String data) {
        try {
            JBaseChock jbaseChock = JSON.parseObject(data, JBaseChock.class);
            Long indocno = jbaseChock.getIndocno();

            baseChockDao.updateDataBaseChockDiscardTime(indocno);
            return ResultData.ResultDataSuccessSelf("修改数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

}
