package com.my.business.modular.basechock.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.math.BigDecimal;

/**
 *  轧辊安全期提醒 综合台账实体类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
public class BaseRollSafetyReminder extends BaseEntity {

    private Long indocno;  //主键
    private Long roll_typeid;//轧辊类型id
    private String roll_type;  //轧辊类型名称

    private Long material_id;//材质id
    private String material;  //材质名称

    private Long framerangeid;//机架范围id
    private String framerange;  //机架范围名称

    private BigDecimal purchasing_cycle;//采购周期
    private BigDecimal safe_inventory;//安全库存
    private BigDecimal d_inventory;//季度消耗

    private BigDecimal s_inventory;//结存
    private BigDecimal effective_value;//理论值

    private String  procurement_date;//采购日期

    public String getProcurement_date() {
        return procurement_date;
    }

    public void setProcurement_date(String procurement_date) {
        this.procurement_date = procurement_date;
    }

    public BigDecimal getPurchasing_cycle() {
        return purchasing_cycle;
    }

    public void setPurchasing_cycle(BigDecimal purchasing_cycle) {
        this.purchasing_cycle = purchasing_cycle;
    }

    public BigDecimal getSafe_inventory() {
        return safe_inventory;
    }

    public void setSafe_inventory(BigDecimal safe_inventory) {
        this.safe_inventory = safe_inventory;
    }

    public BigDecimal getS_inventory() {
        return s_inventory;
    }

    public void setS_inventory(BigDecimal s_inventory) {
        this.s_inventory = s_inventory;
    }

    public BigDecimal getD_inventory() {
        return d_inventory;
    }

    public void setD_inventory(BigDecimal d_inventory) {
        this.d_inventory = d_inventory;
    }

    public BigDecimal getEffective_value() {
        return effective_value;
    }

    public void setEffective_value(BigDecimal effective_value) {
        this.effective_value = effective_value;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getRoll_typeid() {
        return roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public Long getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(Long material_id) {
        this.material_id = material_id;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public Long getFramerangeid() {
        return framerangeid;
    }

    public void setFramerangeid(Long framerangeid) {
        this.framerangeid = framerangeid;
    }

    public String getFramerange() {
        return framerange;
    }

    public void setFramerange(String framerange) {
        this.framerange = framerange;
    }
}