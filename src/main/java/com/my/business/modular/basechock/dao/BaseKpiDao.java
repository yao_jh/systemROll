package com.my.business.modular.basechock.dao;

import com.my.business.modular.basechock.entity.BaseCostAccountingChild;
import com.my.business.modular.basechock.entity.BaseKpi;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * kpi
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@Mapper
public interface BaseKpiDao {


    /**
     * 修改记录
     *
     * @param baseChock 对象实体
     */
    void updateDataBaseKpi(BaseKpi baseKpi);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<BaseKpi> findDataBaseKpiByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    BaseKpi findDataBaseKpiByIndocno(@Param("indocno") Long indocno);


    Integer findDataBaseKpiByPageSize();
}
