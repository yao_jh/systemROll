package com.my.business.modular.basechock.entity;

import com.my.business.sys.common.entity.JCommon;

/**
 * 轴承座综合台账json的实体类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
public class JBaseChockHistory extends JCommon {

    private Integer pageIndex; // 第几页
    private Integer pageSize; // 每页多少数据
    private Object condition; // 查询条件
    private BaseChockHistory baseChockHistory;   //对应模块的实体类
    private Long indocno;
    private String str_indocno;

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Object getCondition() {
        return condition;
    }

    public void setCondition(Object condition) {
        this.condition = condition;
    }

    public BaseChockHistory getBaseChockHistory() {
        return baseChockHistory;
    }

    public void setBaseChockHistory(BaseChockHistory baseChockHistory) {
        this.baseChockHistory = baseChockHistory;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getStr_indocno() {
        return str_indocno;
    }

    public void setStr_indocno(String str_indocno) {
        this.str_indocno = str_indocno;
    }
}