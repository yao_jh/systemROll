package com.my.business.modular.basechock.entity;

import com.my.business.modular.dictionary.entity.Dictionary;
import com.my.business.sys.common.entity.BaseEntity;

import java.math.BigDecimal;
import java.util.List;

/**
 * 料号规格定制子表 综合台账实体类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
public class BaseCostAccountingChild extends BaseEntity {

    private Long indocno;  //主键
    private Long parent_id;//父项id
    private String usetime;//开始时间
    private BigDecimal unit_price;//单价
    private BigDecimal working_layer;//工作层

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getParent_id() {
        return parent_id;
    }

    public void setParent_id(Long parent_id) {
        this.parent_id = parent_id;
    }

    public String getUsetime() {
        return usetime;
    }

    public void setUsetime(String usetime) {
        this.usetime = usetime;
    }

    public BigDecimal getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(BigDecimal unit_price) {
        this.unit_price = unit_price;
    }

    public BigDecimal getWorking_layer() {
        return working_layer;
    }

    public void setWorking_layer(BigDecimal working_layer) {
        this.working_layer = working_layer;
    }
}