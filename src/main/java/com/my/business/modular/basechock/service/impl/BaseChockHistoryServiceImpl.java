package com.my.business.modular.basechock.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.basechock.dao.BaseChockDao;
import com.my.business.modular.basechock.dao.BaseChockHistoryDao;
import com.my.business.modular.basechock.entity.BaseChock;
import com.my.business.modular.basechock.entity.BaseChockHistory;
import com.my.business.modular.basechock.entity.JBaseChock;
import com.my.business.modular.basechock.entity.JBaseChockHistory;
import com.my.business.modular.basechock.service.BaseChockHistoryService;
import com.my.business.modular.basechock.service.BaseChockService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 轴承座综合台账接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@Service
public class BaseChockHistoryServiceImpl implements BaseChockHistoryService {

    @Autowired
    private BaseChockHistoryDao baseChockHistoryDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataBaseChockHistory(String data, Long userId, String sname) {
        try {
            JBaseChockHistory jbaseChockHistory = JSON.parseObject(data, JBaseChockHistory.class);
            BaseChockHistory baseChockHistory = jbaseChockHistory.getBaseChockHistory();
            CodiUtil.newRecord(userId, sname, baseChockHistory);
            baseChockHistoryDao.insertDataBaseChockHistory(baseChockHistory);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataBaseChockHistoryByPage(String data) {
        try {
            JBaseChockHistory jbaseChockHistory = JSON.parseObject(data, JBaseChockHistory.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jbaseChockHistory.getPageIndex();
            Integer pageSize = jbaseChockHistory.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jbaseChockHistory.getCondition()) {
                jsonObject = JSON.parseObject(jbaseChockHistory.getCondition().toString());
            }

            String chock_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("chock_no"))) {
                chock_no = jsonObject.get("chock_no").toString();
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

			Long frame_noid = null;
			if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
				frame_noid = Long.valueOf(jsonObject.get("frame_noid").toString());
			}

			String frame_no = null;
			if (!StringUtils.isEmpty(jsonObject.get("frame_no"))) {
				frame_no = jsonObject.get("frame_no").toString();
			}

			Long production_line_id = null;
			if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
				production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
			}

			Long roll_typeid = null;
			if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
				roll_typeid = Long.valueOf(jsonObject.get("roll_typeid").toString());
			}

			Long istatus = null;
			if (!StringUtils.isEmpty(jsonObject.get("istatus"))) {
				istatus = Long.valueOf(jsonObject.get("istatus").toString());
			}

            List<BaseChockHistory> list = baseChockHistoryDao.findDataBaseChockHistoryByPage((pageIndex - 1) * pageSize, pageSize,chock_no,roll_no,frame_noid,frame_no,production_line_id,roll_typeid,istatus);
            Integer count = baseChockHistoryDao.findDataBaseChockHistoryByPageSize(chock_no,roll_no,frame_noid,frame_no,production_line_id,roll_typeid,istatus);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }
}
