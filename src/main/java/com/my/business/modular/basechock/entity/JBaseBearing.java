package com.my.business.modular.basechock.entity;

import com.my.business.sys.common.entity.JCommon;

/**
 * 轴承综合台账json的实体类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
public class JBaseBearing extends JCommon {

    private Integer pageIndex; // 第几页
    private Integer pageSize; // 每页多少数据
    private Object condition; // 查询条件
    private BaseBearing baseBearing;   //对应模块的实体类
    private Long indocno;
    private String str_indocno;

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Object getCondition() {
        return condition;
    }

    public void setCondition(Object condition) {
        this.condition = condition;
    }

    public BaseBearing getBaseBearing() {
        return baseBearing;
    }

    public void setBaseBearing(BaseBearing baseBearing) {
        this.baseBearing = baseBearing;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getStr_indocno() {
        return str_indocno;
    }

    public void setStr_indocno(String str_indocno) {
        this.str_indocno = str_indocno;
    }
}