package com.my.business.modular.basechock.service;

import com.my.business.modular.basechock.entity.BaseEquipment;
import com.my.business.sys.common.entity.ResultData;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.List;

/**
 * 料号规格定制主表综合台账接口服务类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
public interface BaseCostAccountingMainService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataBaseCostAccountingMain(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataBaseCostAccountingMainOne(Long indocno);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataBaseCostAccountingMain(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataBaseCostAccountingMainByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataBaseCostAccountingMainByIndocno(String data);

    ResultData findBaseCostAccounting(String data);

    String excelBaseCostAccounting(HSSFWorkbook workbook, String material_noid
            , String specifications_noid, String roll_typeid, String material_id, String framerangeid
            , String dbegin, String dend);

    ResultData findBaseCostAccountingByReport(String data);

    String excelBaseCostAccountingByReport(HSSFWorkbook workbook,String roll_no
            , String factory_id
            , String frame_noid
            , String dbegin
            , String dend
    ,String material_id);

    ResultData findBaseCostAccountingABigScreen(String data);
}
