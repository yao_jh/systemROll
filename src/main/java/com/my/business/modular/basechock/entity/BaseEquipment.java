package com.my.business.modular.basechock.entity;

import com.my.business.modular.dictionary.entity.Dictionary;
import com.my.business.sys.common.entity.BaseEntity;

import java.math.BigDecimal;
import java.util.List;

/**
 * 设备履历综合台账实体类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
public class BaseEquipment extends BaseEntity {

    private Long indocno;  //主键
    private Long equipment_id;//配件id
    private String equipment_name;  //配件名称
    private String specifications;//规格
    private String usetime;//开始时间
    private BigDecimal interval_times;//提醒时间
    private BigDecimal total_times;//累计时间
    private String discard_time;//更换时间
    private int ifdiscard;//是否更换 0 正常 1 更换
    private Long factory_id;//厂家id
    private String factory_name;//厂家名称
    private Long operator_id;//操作人id
    private String operator_name;//操作人名称
    private  String sgroup_name;//班组名称
    private  String sclass_name;//班次名称
    private  String memo;//备注
    private String dbegin;
    private String dend;
    private Long modify_id;//修改人id
    private String modify_name;//修改人名称

    private String equipment_pparentname;//设备名称
    private Long equipment_pparentid;//设备id
    private String equipment_parentname;//组件名称
    private Long equipment_parentid;//组件id
    private List<Dictionary> detail;

    private BigDecimal size_start;//开始尺寸
    private BigDecimal size_end;//结束尺寸

    public BigDecimal getSize_start() {
        return size_start;
    }

    public void setSize_start(BigDecimal size_start) {
        this.size_start = size_start;
    }

    public BigDecimal getSize_end() {
        return size_end;
    }

    public void setSize_end(BigDecimal size_end) {
        this.size_end = size_end;
    }

    public List<Dictionary> getDetail() {
        return detail;
    }

    public void setDetail(List<Dictionary> detail) {
        this.detail = detail;
    }

    public String getEquipment_pparentname() {
        return equipment_pparentname;
    }

    public void setEquipment_pparentname(String equipment_pparentname) {
        this.equipment_pparentname = equipment_pparentname;
    }

    public Long getEquipment_pparentid() {
        return equipment_pparentid;
    }

    public void setEquipment_pparentid(Long equipment_pparentid) {
        this.equipment_pparentid = equipment_pparentid;
    }

    public String getEquipment_parentname() {
        return equipment_parentname;
    }

    public void setEquipment_parentname(String equipment_parentname) {
        this.equipment_parentname = equipment_parentname;
    }

    public Long getEquipment_parentid() {
        return equipment_parentid;
    }

    public void setEquipment_parentid(Long equipment_parentid) {
        this.equipment_parentid = equipment_parentid;
    }

    public Long getModify_id() {
        return modify_id;
    }

    public void setModify_id(Long modify_id) {
        this.modify_id = modify_id;
    }

    public String getModify_name() {
        return modify_name;
    }

    public void setModify_name(String modify_name) {
        this.modify_name = modify_name;
    }

    public String getDbegin() {
        return dbegin;
    }

    public void setDbegin(String dbegin) {
        this.dbegin = dbegin;
    }

    public String getDend() {
        return dend;
    }

    public void setDend(String dend) {
        this.dend = dend;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getEquipment_id() {
        return equipment_id;
    }

    public void setEquipment_id(Long equipment_id) {
        this.equipment_id = equipment_id;
    }

    public String getEquipment_name() {
        return equipment_name;
    }

    public void setEquipment_name(String equipment_name) {
        this.equipment_name = equipment_name;
    }

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public String getUsetime() {
        return usetime;
    }

    public void setUsetime(String usetime) {
        this.usetime = usetime;
    }

    public BigDecimal getInterval_times() {
        return interval_times;
    }

    public void setInterval_times(BigDecimal interval_times) {
        this.interval_times = interval_times;
    }

    public BigDecimal getTotal_times() {
        return total_times;
    }

    public void setTotal_times(BigDecimal total_times) {
        this.total_times = total_times;
    }

    public String getDiscard_time() {
        return discard_time;
    }

    public void setDiscard_time(String discard_time) {
        this.discard_time = discard_time;
    }

    public int getIfdiscard() {
        return ifdiscard;
    }

    public void setIfdiscard(int ifdiscard) {
        this.ifdiscard = ifdiscard;
    }

    public Long getFactory_id() {
        return factory_id;
    }

    public void setFactory_id(Long factory_id) {
        this.factory_id = factory_id;
    }

    public String getFactory_name() {
        return factory_name;
    }

    public void setFactory_name(String factory_name) {
        this.factory_name = factory_name;
    }

    public Long getOperator_id() {
        return operator_id;
    }

    public void setOperator_id(Long operator_id) {
        this.operator_id = operator_id;
    }

    public String getOperator_name() {
        return operator_name;
    }

    public void setOperator_name(String operator_name) {
        this.operator_name = operator_name;
    }

    public String getSgroup_name() {
        return sgroup_name;
    }

    public void setSgroup_name(String sgroup_name) {
        this.sgroup_name = sgroup_name;
    }

    public String getSclass_name() {
        return sclass_name;
    }

    public void setSclass_name(String sclass_name) {
        this.sclass_name = sclass_name;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
}