package com.my.business.modular.basechock.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.basechock.entity.BaseBearing;
import com.my.business.modular.basechock.entity.BaseEquipment;
import com.my.business.modular.basechock.entity.JBaseBearing;
import com.my.business.modular.basechock.entity.JBaseEquipment;
import com.my.business.modular.basechock.service.BaseBearingService;
import com.my.business.modular.basechock.service.BaseEquipmentService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 设备履历综合台账控制器层
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@RestController
@RequestMapping("/baseEquipment")
public class BaseEquipmentController {

    @Autowired
    private BaseEquipmentService baseEquipmentService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataBaseEquipment(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return baseEquipmentService.insertDataBaseEquipment(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataBaseEquipmentOne(@RequestBody String data) {
        try {
            JBaseEquipment jbaseEquipment = JSON.parseObject(data, JBaseEquipment.class);
            return baseEquipmentService.deleteDataBaseEquipmentOne(jbaseEquipment.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JBaseEquipment jbaseEquipment = JSON.parseObject(data, JBaseEquipment.class);
            return baseEquipmentService.deleteDataBaseEquipmentMany(jbaseEquipment.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataBaseEquipment(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return baseEquipmentService.updateDataBaseEquipment(data, userId, CodiUtil.returnLm(sname));
    }



    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataBaseEquipmentByPage(@RequestBody String data) {
        return baseEquipmentService.findDataBaseEquipmentByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataBaseEquipmentByIndocno(@RequestBody String data) {
        return baseEquipmentService.findDataBaseEquipmentByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<BaseEquipment> findDataBaseEquipment() {
        return baseEquipmentService.findDataBaseEquipment();
    }

    /**
     * 更换
     */
    @CrossOrigin
    @RequestMapping(value = {"/updateDiscardTime"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataBaseEquipmentDiscardTime(@RequestBody String data) {
        return baseEquipmentService.updateDataBaseEquipmentDiscardTime(data);
    }

    /**
     * 取消更换
     */
    @CrossOrigin
    @RequestMapping(value = {"/updateDiscardTimeCancel"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataBaseEquipmentDiscardTimeCancel(@RequestBody String data) {
        return baseEquipmentService.updateDataBaseEquipmentDiscardTimeCancel(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAllDictionary"}, method = RequestMethod.POST)
    public ResultData findDataDictionary(@RequestBody String data) {
        return baseEquipmentService.findDataDictionary(data);
    }


}
