package com.my.business.modular.basechock.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.basechock.entity.BaseBearing;
import com.my.business.modular.basechock.entity.BaseChock;
import com.my.business.modular.basechock.entity.JBaseBearing;
import com.my.business.modular.basechock.entity.JBaseChock;
import com.my.business.modular.basechock.service.BaseBearingService;
import com.my.business.modular.basechock.service.BaseChockService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 轴承综合台账控制器层
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@RestController
@RequestMapping("/baseBearing")
public class BaseBearingController {

    @Autowired
    private BaseBearingService baseBearingService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataBaseBearing(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return baseBearingService.insertDataBaseBearing(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataBaseBearingOne(@RequestBody String data) {
        try {
            JBaseBearing jbaseBearing = JSON.parseObject(data, JBaseBearing.class);
            return baseBearingService.deleteDataBaseBearingOne(jbaseBearing.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JBaseBearing jbaseBearing = JSON.parseObject(data, JBaseBearing.class);
            return baseBearingService.deleteDataBaseBearingMany(jbaseBearing.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataBaseBearing(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return baseBearingService.updateDataBaseBearing(data, userId, CodiUtil.returnLm(sname));
    }

    @CrossOrigin
    @RequestMapping(value = {"/updateByChockNo"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataBaseBearingByChockNo(@RequestBody String data) {
        return baseBearingService.updateDataBaseBearingByChockNo(data);
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataBaseBearingByPage(@RequestBody String data) {
        return baseBearingService.findDataBaseBearingByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataBaseBearingByIndocno(@RequestBody String data) {
        return baseBearingService.findDataBaseBearingByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<BaseBearing> findDataBaseBearing() {
        return baseBearingService.findDataBaseBearing();
    }

    /**
     * 单次时间提醒
     */
    @CrossOrigin
    @RequestMapping(value = {"/updateSingleTimes"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataBaseBearingSingleTimes(@RequestBody String data) {
        return baseBearingService.updateDataBaseBearingSingleTimes(data);
    }

    /**
     * 报废
     */
    @CrossOrigin
    @RequestMapping(value = {"/updateDiscardTime"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataBaseBearingDiscardTime(@RequestBody String data) {
        return baseBearingService.updateDataBaseBearingDiscardTime(data);
    }

    /**
     * 取消报废
     */
    @CrossOrigin
    @RequestMapping(value = {"/updateDiscardTimeCancel"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataBaseBearingDiscardTimeCancel(@RequestBody String data) {
        return baseBearingService.updateDataBaseBearingDiscardTimeCancel(data);
    }

    /**
     * 根据类型 1 2 3  查询 符合条件的轴承 密封件 --砂轮
     */
    @CrossOrigin
    @RequestMapping(value = {"/findListByBearingType"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findListByBearingType(@RequestBody String data) {
        return baseBearingService.findListByBearingType(data);
    }
}
