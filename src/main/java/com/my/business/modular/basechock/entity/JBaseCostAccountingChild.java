package com.my.business.modular.basechock.entity;

import com.my.business.sys.common.entity.JCommon;

/**
 * 料号规格定制子表 综合台账json的实体类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
public class JBaseCostAccountingChild extends JCommon {

    private Integer pageIndex; // 第几页
    private Integer pageSize; // 每页多少数据
    private Object condition; // 查询条件
    private BaseCostAccountingChild baseCostAccountingChild;   //对应模块的实体类
    private Long indocno;
    private String str_indocno;
    private Long parent_id; //父项id

    public Long getParent_id() {
        return parent_id;
    }

    public void setParent_id(Long parent_id) {
        this.parent_id = parent_id;
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Object getCondition() {
        return condition;
    }

    public void setCondition(Object condition) {
        this.condition = condition;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getStr_indocno() {
        return str_indocno;
    }

    public void setStr_indocno(String str_indocno) {
        this.str_indocno = str_indocno;
    }

    public BaseCostAccountingChild getBaseCostAccountingChild() {
        return baseCostAccountingChild;
    }

    public void setBaseCostAccountingChild(BaseCostAccountingChild baseCostAccountingChild) {
        this.baseCostAccountingChild = baseCostAccountingChild;
    }
}