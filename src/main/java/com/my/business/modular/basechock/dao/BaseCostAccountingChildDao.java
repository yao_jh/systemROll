package com.my.business.modular.basechock.dao;

import com.my.business.modular.basechock.entity.BaseCostAccountingChild;
import com.my.business.modular.basechock.entity.BaseEquipment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 料号规格定制子表 综合台账dao接口
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@Mapper
public interface BaseCostAccountingChildDao {

    /**
     * 添加记录
     *
     * @param baseChock 对象实体
     */
    void insertDataBaseCostAccountingChild(BaseCostAccountingChild baseCostAccountingChild);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataBaseCostAccountingChildOne(@Param("indocno") Long indocno);


    /**
     * 修改记录
     *
     * @param baseChock 对象实体
     */
    void updateDataBaseCostAccountingChild(BaseCostAccountingChild baseCostAccountingChild);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<BaseCostAccountingChild> findDataBaseCostAccountingChildByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("parent_id") Long parent_id,@Param("dbegin") String dbegin);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    BaseCostAccountingChild findDataBaseCostAccountingChildByIndocno(@Param("indocno") Long indocno);


    Integer findDataBaseCostAccountingChildByPageSize(@Param("parent_id") Long parent_id,@Param("dbegin") String dbegin);
}
