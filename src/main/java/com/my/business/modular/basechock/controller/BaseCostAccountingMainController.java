package com.my.business.modular.basechock.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.basechock.entity.BaseEquipment;
import com.my.business.modular.basechock.entity.JBaseCostAccountingMain;
import com.my.business.modular.basechock.entity.JBaseEquipment;
import com.my.business.modular.basechock.service.BaseCostAccountingMainService;
import com.my.business.modular.basechock.service.BaseEquipmentService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;


/**
 * 料号规格定制主表综合台账控制器层
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@RestController
@RequestMapping("/baseCostAccountingMain")
public class BaseCostAccountingMainController {

    @Autowired
    private BaseCostAccountingMainService baseCostAccountingMainService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataBaseCostAccountingMain(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return baseCostAccountingMainService.insertDataBaseCostAccountingMain(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteBaseCostAccountingMainOne(@RequestBody String data) {
        try {
            JBaseCostAccountingMain jBaseCostAccountingMain = JSON.parseObject(data, JBaseCostAccountingMain.class);
            return baseCostAccountingMainService.deleteDataBaseCostAccountingMainOne(jBaseCostAccountingMain.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataBaseCostAccountingMain(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return baseCostAccountingMainService.updateDataBaseCostAccountingMain(data, userId, CodiUtil.returnLm(sname));
    }



    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataBaseCostAccountingMainByPage(@RequestBody String data) {
        return baseCostAccountingMainService.findDataBaseCostAccountingMainByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataBaseCostAccountingMainByIndocno(@RequestBody String data) {
        return baseCostAccountingMainService.findDataBaseCostAccountingMainByIndocno(data);
    }

    /**
     * 成本核算
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findBaseCostAccounting"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findBaseCostAccounting(@RequestBody String data) {
        return baseCostAccountingMainService.findBaseCostAccounting(data);
    }

    /**
     * 大屏幕 成本核算
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findBaseCostAccountingABigScreen"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findBaseCostAccountingABigScreen(@RequestBody String data) {
        return baseCostAccountingMainService.findBaseCostAccountingABigScreen(data);
    }

    /**
     * 导出 成本核算
     */
    @CrossOrigin
    @RequestMapping(value = {"/excelBaseCostAccounting"}, method = RequestMethod.GET)
    @ResponseBody
    public void getExcelBaseCostAccounting(
            @RequestParam ( "material_noid" ) String material_noid,
            @RequestParam ( "specifications_noid" ) String specifications_noid,
            @RequestParam ( "roll_typeid" ) String roll_typeid,
            @RequestParam ( "material_id" ) String material_id,
            @RequestParam ( "framerangeid" ) String framerangeid,
            @RequestParam ( "dbegin" ) String dbegin,
            @RequestParam ( "dend" ) String dend,
            HttpServletResponse response, HttpServletRequest request) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        String fileName = "";//设置要导出的文件的名字
        fileName = baseCostAccountingMainService.excelBaseCostAccounting(workbook,
                material_noid,specifications_noid
                ,roll_typeid,material_id,framerangeid
                ,dbegin,dend
        );
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1) + ".xls");//URLEncoder.encode(fileName, "utf-8")
        workbook.write(response.getOutputStream());
    }

    /**
     * 厂家机架辊耗重量统计
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findBaseCostAccountingByReport"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findBaseCostAccountingByReport(@RequestBody String data) {
        return baseCostAccountingMainService.findBaseCostAccountingByReport(data);
    }
    /**
     * 导出 厂家机架辊耗重量统计
     */
    @CrossOrigin
    @RequestMapping(value = {"/excelBaseCostAccountingByReport"}, method = RequestMethod.GET)
    @ResponseBody
    public void getExcelBaseCostAccountingByReport(
            @RequestParam ( "roll_no" ) String roll_no,
            @RequestParam ( "factory_id" ) String factory_id,
            @RequestParam ( "frame_noid" ) String frame_noid,
            @RequestParam ( "dbegin" ) String dbegin,
            @RequestParam ( "dend" ) String dend,
            @RequestParam ( "material_id" ) String material_id,
            HttpServletResponse response, HttpServletRequest request) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        String fileName = "";//设置要导出的文件的名字
        fileName = baseCostAccountingMainService.excelBaseCostAccountingByReport(workbook
                ,roll_no
                ,factory_id
                ,frame_noid
                ,dbegin
                ,dend
                ,material_id
                );
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1) + ".xls");//URLEncoder.encode(fileName, "utf-8")
        workbook.write(response.getOutputStream());
    }

}
