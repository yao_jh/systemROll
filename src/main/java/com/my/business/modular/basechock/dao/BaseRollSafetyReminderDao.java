package com.my.business.modular.basechock.dao;

import com.my.business.modular.basechock.entity.BaseCostAccountingMain;
import com.my.business.modular.basechock.entity.BaseCostAccountingReport;
import com.my.business.modular.basechock.entity.BaseRollSafetyReminder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *  轧辊安全期提醒  综合台账dao接口
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@Mapper
public interface BaseRollSafetyReminderDao {

    /**
     * 添加记录
     *
     * @param baseChock 对象实体
     */
    void insertBaseRollSafetyReminder(BaseRollSafetyReminder baseRollSafetyReminder);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteBaseRollSafetyReminderOne(@Param("indocno") Long indocno);

    /**
     * 修改记录
     *
     * @param baseChock 对象实体
     */
    void updateBaseRollSafetyReminder(BaseRollSafetyReminder baseRollSafetyReminder);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<BaseRollSafetyReminder> findBaseRollSafetyReminderByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,
                                                                      @Param("roll_typeid") String roll_typeid, @Param("material_id") String material_id,
                                                                      @Param("framerangeid") String framerangeid
    );
    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findBaseRollSafetyReminderByPageSize(
            @Param("roll_typeid") String roll_typeid, @Param("material_id") String material_id,
            @Param("framerangeid") String framerangeid
    );

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    BaseRollSafetyReminder findBaseRollSafetyReminderByIndocno(@Param("indocno") Long indocno);

}
