package com.my.business.modular.basechock.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.basechock.dao.BaseCostAccountingMainDao;
import com.my.business.modular.basechock.dao.BaseEquipmentDao;
import com.my.business.modular.basechock.entity.*;
import com.my.business.modular.basechock.service.BaseCostAccountingMainService;
import com.my.business.modular.basechock.service.BaseEquipmentService;
import com.my.business.modular.dictionary.dao.DictionaryDao;
import com.my.business.modular.dictionary.entity.Dictionary;
import com.my.business.modular.rollinformation.entity.JRollInformation;
import com.my.business.sys.common.entity.MapXYEntity;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.util.DateUtil;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 料号规格定制主表综合台账接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@Service
public class BaseCostAccountingMainServiceImpl implements BaseCostAccountingMainService {

    @Autowired
    private BaseCostAccountingMainDao baseCostAccountingMainDao;
@Autowired
private DictionaryDao dictionaryDao;
    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataBaseCostAccountingMain(String data, Long userId, String sname) {
        try {
            JBaseCostAccountingMain jBaseCostAccountingMain = JSON.parseObject(data, JBaseCostAccountingMain.class);
            BaseCostAccountingMain baseCostAccountingMain = jBaseCostAccountingMain.getBaseCostAccountingMain();
            CodiUtil.newRecord(userId, sname, baseCostAccountingMain);
            baseCostAccountingMainDao.insertDataBaseCostAccountingMain(baseCostAccountingMain);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataBaseCostAccountingMainOne(Long indocno) {
        try {
            baseCostAccountingMainDao.deleteDataBaseCostAccountingMainOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataBaseCostAccountingMain(String data, Long userId, String sname) {
        try {
            JBaseCostAccountingMain jBaseCostAccountingMain = JSON.parseObject(data, JBaseCostAccountingMain.class);
            BaseCostAccountingMain baseCostAccountingMain = jBaseCostAccountingMain.getBaseCostAccountingMain();
            CodiUtil.editRecord(userId, sname, baseCostAccountingMain);
            baseCostAccountingMainDao.updateDataBaseCostAccountingMain(baseCostAccountingMain);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataBaseCostAccountingMainByPage(String data) {
        try {
            JBaseCostAccountingMain jBaseCostAccountingMain = JSON.parseObject(data, JBaseCostAccountingMain.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jBaseCostAccountingMain.getPageIndex();
            Integer pageSize = jBaseCostAccountingMain.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jBaseCostAccountingMain.getCondition()) {
                jsonObject = JSON.parseObject(jBaseCostAccountingMain.getCondition().toString());
            }else{
                jsonObject = new JSONObject();
            }

            String material_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("material_noid"))) {
                material_noid = jsonObject.get("material_noid").toString();
            }
            String specifications_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("specifications_noid"))) {
                specifications_noid = jsonObject.get("specifications_noid").toString();
            }
            String roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = jsonObject.get("roll_typeid").toString();
            }
            String material_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("material_id"))) {
                material_id = jsonObject.get("material_id").toString();
            }
            String framerangeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("framerangeid"))) {
                framerangeid = jsonObject.get("framerangeid").toString();
            }

            List<BaseCostAccountingMain> list = baseCostAccountingMainDao.findDataBaseCostAccountingMainByPage((pageIndex - 1) * pageSize, pageSize,material_noid,specifications_noid,roll_typeid,material_id,framerangeid);
            Integer count = baseCostAccountingMainDao.findDataBaseCostAccountingMainByPageSize(material_noid,specifications_noid,roll_typeid,material_id,framerangeid);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataBaseCostAccountingMainByIndocno(String data) {
        try {
            JBaseCostAccountingMain jBaseCostAccountingMain = JSON.parseObject(data, JBaseCostAccountingMain.class);

            Long indocno = jBaseCostAccountingMain.getIndocno();
            BaseCostAccountingMain baseCostAccountingMain = baseCostAccountingMainDao.findDataBaseCostAccountingMainByIndocno(indocno);
            return ResultData.ResultDataSuccess(baseCostAccountingMain);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public ResultData findBaseCostAccounting(String data) {

        try{
        JBaseCostAccountingMain jBaseCostAccountingMain = JSON.parseObject(data, JBaseCostAccountingMain.class);
        JSONObject jsonObject = null;
        Integer pageIndex = jBaseCostAccountingMain.getPageIndex();
        Integer pageSize = jBaseCostAccountingMain.getPageSize();

        if (null == pageIndex || null == pageSize) {
            return ResultData.ResultDataFaultSelf("分页参数没有传", null);
        }

        if (null != jBaseCostAccountingMain.getCondition()) {
            jsonObject = JSON.parseObject(jBaseCostAccountingMain.getCondition().toString());
        }else{
            jsonObject = new JSONObject();
        }
        String material_noid = null;
        if (!StringUtils.isEmpty(jsonObject.get("material_noid"))) {
            material_noid = jsonObject.get("material_noid").toString();
        }
        String specifications_noid = null;
        if (!StringUtils.isEmpty(jsonObject.get("specifications_noid"))) {
            specifications_noid = jsonObject.get("specifications_noid").toString();
        }
        String roll_typeid = null;
        if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
            roll_typeid = jsonObject.get("roll_typeid").toString();
        }
        String material_id = null;
        if (!StringUtils.isEmpty(jsonObject.get("material_id"))) {
            material_id = jsonObject.get("material_id").toString();
        }
        String framerangeid = null;
        if (!StringUtils.isEmpty(jsonObject.get("framerangeid"))) {
            framerangeid = jsonObject.get("framerangeid").toString();
        }
        String startStr=null;
            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
                startStr = dbegin.substring(5,7);
            }

            String dend = null;
            String endStr = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
                endStr = dend.substring(5,7);
            }

        List<BaseCostAccountingReport> list = baseCostAccountingMainDao.findDataBaseCostAccountingByPage((pageIndex - 1) * pageSize, pageSize,material_noid,specifications_noid,roll_typeid,material_id,framerangeid,dbegin,dend);
        Integer count = baseCostAccountingMainDao.findDataBaseCostAccountingByPageSize(material_noid,specifications_noid,roll_typeid,material_id,framerangeid,dbegin,dend);
        BaseCostAccountingReport  baseCostAccountingReport= new BaseCostAccountingReport();
            baseCostAccountingReport.setConsumption(BigDecimal.ZERO);
            baseCostAccountingReport.setCost(BigDecimal.ZERO);
        for (BaseCostAccountingReport bean:list) {
            if(!StringUtils.isEmpty(bean.getMmnumber())&&!StringUtils.isEmpty(bean.getWorking_layer())){
                bean.setWeight(bean.getMmnumber().multiply(bean.getWorking_layer()));
            }
            if(!StringUtils.isEmpty(bean.getWeight())&&!StringUtils.isEmpty(bean.getUnit_price())){
                bean.setMoney(bean.getWeight().multiply(bean.getUnit_price()));
            }
                if(!StringUtils.isEmpty(bean.getWeight())){
                    baseCostAccountingReport.setConsumption(
                            baseCostAccountingReport.getConsumption().add(bean.getWeight().multiply(BigDecimal.valueOf(1000)))//.multiply(BigDecimal.valueOf(1000))
                    );
                    baseCostAccountingReport.setCost(
                            baseCostAccountingReport.getCost().add(bean.getMoney())
                    );
                }
            }

        if(!StringUtils.isEmpty(startStr)&&!StringUtils.isEmpty(endStr)){
            int start = Integer.valueOf(startStr);
            int end = Integer.valueOf(endStr);
            if(start>end){
                if(start<12){
                    for(int i = start;i<=12;i++){
                        String value =dictionaryDao.findMapBynameV1("production"+i,i+"月产量");
                        if(!StringUtils.isEmpty(value)){
                            if(StringUtils.isEmpty(baseCostAccountingReport.getProduction())){
                                baseCostAccountingReport.setProduction(BigDecimal.ZERO);
                            }
                            baseCostAccountingReport.setProduction(baseCostAccountingReport.getProduction().add(BigDecimal.valueOf(Long.valueOf(value))));
                        }
                    }
                }
                for(int i = 1;i<=end;i++){
                    String value =dictionaryDao.findMapBynameV1("production"+i,i+"月产量");
                    if(!StringUtils.isEmpty(value)){
                        if(StringUtils.isEmpty(baseCostAccountingReport.getProduction())){
                            baseCostAccountingReport.setProduction(BigDecimal.ZERO);
                        }
                        baseCostAccountingReport.setProduction(baseCostAccountingReport.getProduction().add(BigDecimal.valueOf(Long.valueOf(value))));
                    }
                }
            }else{
                for(int i = start+1;i<=end;i++){
                    String value =dictionaryDao.findMapBynameV1("production"+i,i+"月产量");
                    if(!StringUtils.isEmpty(value)){
                        if(StringUtils.isEmpty(baseCostAccountingReport.getProduction())){
                            baseCostAccountingReport.setProduction(BigDecimal.ZERO);
                        }
                        baseCostAccountingReport.setProduction(baseCostAccountingReport.getProduction().add(BigDecimal.valueOf(Long.valueOf(value))));
                    }
                }
            }
        }
            if(!StringUtils.isEmpty(baseCostAccountingReport.getProduction())){
                if(!StringUtils.isEmpty(baseCostAccountingReport.getConsumption())){
                    baseCostAccountingReport.setConsumption(baseCostAccountingReport.getConsumption().divide(baseCostAccountingReport.getProduction(),2,BigDecimal.ROUND_HALF_UP));
                }
                if(!StringUtils.isEmpty(baseCostAccountingReport.getCost())){
                    baseCostAccountingReport.setCost(baseCostAccountingReport.getCost().divide(baseCostAccountingReport.getProduction(),2,BigDecimal.ROUND_HALF_UP));
                }
            }else{
                baseCostAccountingReport.setCost(BigDecimal.ZERO);
                baseCostAccountingReport.setConsumption(BigDecimal.ZERO);
            }
        return ResultData.ResultDataSuccess(list, count,baseCostAccountingReport);

        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public String excelBaseCostAccounting(HSSFWorkbook workbook,
                                          String material_noid
            , String specifications_noid, String roll_typeid, String material_id, String framerangeid
            , String dbegin, String dend) {
        if(StringUtils.isEmpty(material_noid)){
            material_noid = null;
        }
        if(StringUtils.isEmpty(specifications_noid)){
            specifications_noid = null;
        }
        if(StringUtils.isEmpty(roll_typeid)){
            roll_typeid = null;
        }
        if(StringUtils.isEmpty(material_id)){
            material_id = null;
        }
        if(StringUtils.isEmpty(framerangeid)){
            framerangeid = null;
        }
        String startStr=null;
        String endStr = null;
        if(StringUtils.isEmpty(dbegin)){

            dbegin = null;
        }        if(StringUtils.isEmpty(dend)){
            dend = null;

        }
        startStr = dbegin.substring(5,7);
        endStr = dend.substring(5,7);

        String filename="每月成本核算";


        List<BaseCostAccountingReport> list = baseCostAccountingMainDao.findDataBaseCostAccountingByPage(0, 100,material_noid,specifications_noid,roll_typeid,material_id,framerangeid,dbegin,dend);
        BaseCostAccountingReport  baseCostAccountingReport= new BaseCostAccountingReport();
        baseCostAccountingReport.setConsumption(BigDecimal.ZERO);
        baseCostAccountingReport.setCost(BigDecimal.ZERO);
        for (BaseCostAccountingReport bean:list) {
            if(!StringUtils.isEmpty(bean.getMmnumber())&&!StringUtils.isEmpty(bean.getWorking_layer())){
                bean.setWeight(bean.getMmnumber().multiply(bean.getWorking_layer()));
            }
            if(!StringUtils.isEmpty(bean.getWeight())&&!StringUtils.isEmpty(bean.getUnit_price())){
                bean.setMoney(bean.getWeight().multiply(bean.getUnit_price()));
            }
            if(!StringUtils.isEmpty(bean.getWeight())){
                baseCostAccountingReport.setConsumption(
                        baseCostAccountingReport.getConsumption().add(bean.getWeight().multiply(BigDecimal.valueOf(1000)))
                );
                baseCostAccountingReport.setCost(
                        baseCostAccountingReport.getCost().add(bean.getMoney())
                );
            }
        }

        if(!StringUtils.isEmpty(startStr)&&!StringUtils.isEmpty(endStr)){
            int start = Integer.valueOf(startStr);
            int end = Integer.valueOf(endStr);
            if(start>end){
                if(start<12){
                    for(int i = start;i<=12;i++){
                        String value =dictionaryDao.findMapBynameV1("production"+i,i+"月产量");
                        if(!StringUtils.isEmpty(value)){
                            if(StringUtils.isEmpty(baseCostAccountingReport.getProduction())){
                                baseCostAccountingReport.setProduction(BigDecimal.ZERO);
                            }
                            baseCostAccountingReport.setProduction(baseCostAccountingReport.getProduction().add(BigDecimal.valueOf(Long.valueOf(value))));
                        }
                    }
                }

                for(int i = 1;i<=end;i++){
                    String value =dictionaryDao.findMapBynameV1("production"+i,i+"月产量");
                    if(!StringUtils.isEmpty(value)){
                        if(StringUtils.isEmpty(baseCostAccountingReport.getProduction())){
                            baseCostAccountingReport.setProduction(BigDecimal.ZERO);
                        }
                        baseCostAccountingReport.setProduction(baseCostAccountingReport.getProduction().add(BigDecimal.valueOf(Long.valueOf(value))));
                    }
                }
            }else{
                for(int i = start+1;i<=end;i++){
                    String value =dictionaryDao.findMapBynameV1("production"+i,i+"月产量");
                    if(!StringUtils.isEmpty(value)){
                        if(StringUtils.isEmpty(baseCostAccountingReport.getProduction())){
                            baseCostAccountingReport.setProduction(BigDecimal.ZERO);
                        }
                        baseCostAccountingReport.setProduction(baseCostAccountingReport.getProduction().add(BigDecimal.valueOf(Long.valueOf(value))));
                    }
                }
            }
        }
        if(!StringUtils.isEmpty(baseCostAccountingReport.getProduction())){
            if(!StringUtils.isEmpty(baseCostAccountingReport.getConsumption())){
                baseCostAccountingReport.setConsumption(baseCostAccountingReport.getConsumption().divide(baseCostAccountingReport.getProduction(),2,BigDecimal.ROUND_HALF_UP));
            }
            if(!StringUtils.isEmpty(baseCostAccountingReport.getCost())){
                baseCostAccountingReport.setCost(baseCostAccountingReport.getCost().divide(baseCostAccountingReport.getProduction(),2,BigDecimal.ROUND_HALF_UP));
            }
        }else{
        baseCostAccountingReport.setCost(BigDecimal.ZERO);
        baseCostAccountingReport.setConsumption(BigDecimal.ZERO);
    }


        HSSFSheet sheet = workbook.createSheet("sheet1");
        getExcelBaseCostAccounting(list,baseCostAccountingReport, sheet, workbook);
        return filename;
    }

    public ResultData findBaseCostAccountingByReport(String data) {
        try{
            JBaseCostAccountingMain jBaseCostAccountingMain = JSON.parseObject(data, JBaseCostAccountingMain.class);
            JSONObject jsonObject = null;

            if (null != jBaseCostAccountingMain.getCondition()) {
                jsonObject = JSON.parseObject(jBaseCostAccountingMain.getCondition().toString());
            }else{
                jsonObject = new JSONObject();
            }

            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }

            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }
            String factory_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("factory_id"))) {
                factory_id = jsonObject.get("factory_id").toString();
            }
            String frame_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
                frame_noid = jsonObject.get("frame_noid").toString();
            }
            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }
            String material_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("material_id"))) {
                material_id = jsonObject.get("material_id").toString();
            }


            List<BaseCostAccountingReport> list = baseCostAccountingMainDao.findBaseCostAccountingByReportByPage(roll_no,factory_id,frame_noid,dbegin,dend,material_id);
            for (BaseCostAccountingReport bean:list) {
                if(!StringUtils.isEmpty(bean.getMmnumber())&&!StringUtils.isEmpty(bean.getWorking_layer())){
                    bean.setWeight(bean.getMmnumber().multiply(bean.getWorking_layer()));
                }
            }
            return ResultData.ResultDataSuccess(list);

        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public String excelBaseCostAccountingByReport(HSSFWorkbook workbook
            , String roll_no
            , String factory_id
            , String frame_noid
            , String dbegin
            , String dend
                                                  ,String material_id
                                                  ) {
        String filename="厂家轧辊消耗报告";
        if(StringUtils.isEmpty(roll_no)){
            roll_no = null;
        }
        if(StringUtils.isEmpty(factory_id)){
            factory_id = null;
        }
        if(StringUtils.isEmpty(frame_noid)){
            frame_noid = null;
        }
        if(StringUtils.isEmpty(dbegin)){
            dbegin = null;
        }
        if(StringUtils.isEmpty(dend)){
            dend = null;
        }
        if(StringUtils.isEmpty(material_id)){
            material_id = null;
        }
        List<BaseCostAccountingReport> list = baseCostAccountingMainDao.findBaseCostAccountingByReportByPage(roll_no,factory_id,frame_noid,dbegin,dend,material_id);
        for (BaseCostAccountingReport bean:list) {
            if(!StringUtils.isEmpty(bean.getMmnumber())&&!StringUtils.isEmpty(bean.getWorking_layer())) {
                bean.setWeight(bean.getMmnumber().multiply(bean.getWorking_layer()));
            }
        }
        HSSFSheet sheet = workbook.createSheet("sheet1");
        getExcelBaseCostAccountingByReport(list, sheet, workbook);
        return filename;
    }

    public ResultData findBaseCostAccountingABigScreen(String data) {
        try{
            JBaseCostAccountingMain jBaseCostAccountingMain = JSON.parseObject(data, JBaseCostAccountingMain.class);
            JSONObject jsonObject = null;
            //Integer pageIndex = jBaseCostAccountingMain.getPageIndex();
            //Integer pageSize = jBaseCostAccountingMain.getPageSize();

            //if (null == pageIndex || null == pageSize) {
            //    return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            //}

            if (null != jBaseCostAccountingMain.getCondition()) {
                jsonObject = JSON.parseObject(jBaseCostAccountingMain.getCondition().toString());
            }else{
                jsonObject = new JSONObject();
            }
            String material_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("material_noid"))) {
                material_noid = jsonObject.get("material_noid").toString();
            }
            String specifications_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("specifications_noid"))) {
                specifications_noid = jsonObject.get("specifications_noid").toString();
            }
            String roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = jsonObject.get("roll_typeid").toString();
            }
            String material_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("material_id"))) {
                material_id = jsonObject.get("material_id").toString();
            }
            String framerangeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("framerangeid"))) {
                framerangeid = jsonObject.get("framerangeid").toString();
            }
            String dend = null;
            String dbegin = null;
            if(!StringUtils.isEmpty(jsonObject.get("dbegin"))){
                if(Long.valueOf((String) jsonObject.get("dbegin"))>0){
                    dend = DateUtil.getTimeStrByMonth(2,(String) jsonObject.get("dbegin"));
                    dbegin = DateUtil.getTimeStrByMonth(1,(String) jsonObject.get("dbegin"));
                }else{
                dend = DateUtil.getTimeStr(2);
                dbegin = DateUtil.getTimeStr(1);
            }
            }else{
                dend = DateUtil.getTimeStr(2);
                dbegin = DateUtil.getTimeStr(1);
            }
            //String dend = null;
            String endStr = null;
            //dend = DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss");
            String startStr=null;
            //String dbegin = null;
            //dbegin = dend.substring(0,4)+"-01-01 08:00:00";
            List<BaseCostAccountingReport> list = baseCostAccountingMainDao.findBaseCostAccountingABigScreenByPage(material_noid,specifications_noid,roll_typeid,material_id,framerangeid,dbegin,dend);
            BaseCostAccountingReport  baseCostAccountingReport= new BaseCostAccountingReport();
            String month = dend.substring(5,7);
            String value="1000";
            if(!StringUtils.isEmpty(month)&&!StringUtils.isEmpty(month)){
                int start = Integer.valueOf(month);
                //int end = Integer.valueOf(month);
                //for(int i = start;i<=end-1;i++){
                     value =dictionaryDao.findMapBynameV1("production"+start,start+"月产量");
                //}
            }

            baseCostAccountingReport.setConsumption(BigDecimal.ZERO);
            baseCostAccountingReport.setCost(BigDecimal.ZERO);
            for (BaseCostAccountingReport bean:list) {
                if(!StringUtils.isEmpty(bean.getMmnumber())&&!StringUtils.isEmpty(bean.getWorking_layer())){
                    bean.setWeight(bean.getMmnumber().multiply(bean.getWorking_layer()));
                }
                if(!StringUtils.isEmpty(bean.getWeight())&&!StringUtils.isEmpty(bean.getUnit_price())){
                    bean.setMoney(bean.getWeight().multiply(bean.getUnit_price()));
                }

                if(!StringUtils.isEmpty(bean.getWeight())){
                    baseCostAccountingReport.setConsumption(
                            baseCostAccountingReport.getConsumption().add(bean.getWeight().multiply(BigDecimal.valueOf(1000)))
                    );
                    baseCostAccountingReport.setCost(
                            baseCostAccountingReport.getCost().add(bean.getMoney())
                    );

                    /*if(!StringUtils.isEmpty(value)){
                        if(StringUtils.isEmpty(bean.getProduction())){
                            bean.setProduction(BigDecimal.ONE);
                        }else{
                            bean.setProduction(BigDecimal.valueOf(Long.valueOf(value)));
                        }
                        bean.setConsumption(
                                bean.getWeight().multiply(BigDecimal.valueOf(1000)).divide(bean.getProduction())
                        );
                        if(!StringUtils.isEmpty(bean.getMoney())){
                            bean.setCost(bean.getMoney().divide(bean.getProduction()));
                        }
                    }*/

                }
            }

            if(!StringUtils.isEmpty(value)){
                if(StringUtils.isEmpty(baseCostAccountingReport.getProduction())){
                    baseCostAccountingReport.setProduction(BigDecimal.ZERO);
                }
                baseCostAccountingReport.setProduction(baseCostAccountingReport.getProduction().add(BigDecimal.valueOf(Long.valueOf(value))));
            }

            if(!StringUtils.isEmpty(baseCostAccountingReport.getProduction())){
                if(!StringUtils.isEmpty(baseCostAccountingReport.getConsumption())){
                    baseCostAccountingReport.setConsumption(baseCostAccountingReport.getConsumption().divide(baseCostAccountingReport.getProduction(),2,BigDecimal.ROUND_HALF_UP));
                }
                if(!StringUtils.isEmpty(baseCostAccountingReport.getCost())){
                    baseCostAccountingReport.setCost(baseCostAccountingReport.getCost().divide(baseCostAccountingReport.getProduction(),2,BigDecimal.ROUND_HALF_UP));
                }
            }

            List<MapXYEntity> map = new ArrayList<MapXYEntity>();
            if(list!=null&&list.size()>0){
                for (BaseCostAccountingReport m:list){
                    MapXYEntity t = new MapXYEntity();
                    if(!StringUtils.isEmpty(m.getUsetime()+" "+m.getRoll_type()+" "+m.getMaterial()+" "+m.getFramerange())){
                        String str="";
                        if(m.getMaterial().equals("高速钢")){
                            str+="H";
                        }
                            str+=m.getFramerange();
                        t.setX(str);
                        //t.setX(m.getUsetime()+" "+m.getRoll_type()+" "+m.getMaterial()+" "+m.getFramerange());
                    }else{
                        t.setX(0);
                    }
                    if(!StringUtils.isEmpty(m.getMmnumber())){
                        t.setY1(m.getMmnumber());
                    }else{
                        t.setY1(0);
                    }

                    if(!StringUtils.isEmpty(m.getConsumption())){
                        t.setY2(m.getConsumption());
                    }else{
                        t.setY2(0);
                    }

                    if(!StringUtils.isEmpty(m.getCost())){
                        t.setY3(m.getCost());
                    }else{
                        t.setY3(0);
                    }

                    map.add(t);
                }
            }

            return ResultData.ResultDataSuccess(list, 0,baseCostAccountingReport,map);

        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    private void getExcelBaseCostAccountingByReport(List<BaseCostAccountingReport> list, HSSFSheet sheet, HSSFWorkbook workbook) {
        if(list!=null && list.size()>0) {
            // 设置表头样式 居中 加粗 宋体 20
            HSSFCellStyle cellStyleHead = workbook.createCellStyle();
            cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
            HSSFFont redFontHead = workbook.createFont();
            redFontHead.setFontHeightInPoints((short) 12);//设置字体大小
            redFontHead.setFontName("宋体");//字体型号
            cellStyleHead.setFont(redFontHead);
            HSSFRow rowhead = sheet.createRow(0);   //创建第一行头
            HSSFCell cell_00 = rowhead.createCell(0);   //创建第一行第一列
            cell_00.setCellStyle(cellStyleHead);
            cell_00.setCellValue("厂家轧辊消耗报告");
            HSSFRow row1 = sheet.createRow(1);   //创建第二行
            HSSFCell cell_2_1 = row1.createCell(0);   //创建第二行第一列
            cell_2_1.setCellValue("序号");
            HSSFCell cell_2_2 = row1.createCell(1);   //创建第二行第二列
            cell_2_2.setCellValue("厂家");
            HSSFCell cell_2_3 = row1.createCell(2);   //创建第二行第三列
            cell_2_3.setCellValue("机架号");
            HSSFCell cell_2_4 = row1.createCell(3);   //创建第二行第四列
            cell_2_4.setCellValue("轧制重量");
            HSSFCell cell_2_5 = row1.createCell(4);   //创建第二行第五列
            cell_2_5.setCellValue("轧制公里数");
            HSSFCell cell_2_6 = row1.createCell(5);   //创建第二行第六列
            cell_2_6.setCellValue("消耗毫米");
            HSSFCell cell_2_7 = row1.createCell(6);   //创建第二行第七列
            cell_2_7.setCellValue("折合毫米工作层重量（t）");

            HSSFCell cell_2_8 = row1.createCell(7);   //创建第二行第七列
            cell_2_8.setCellValue("毫米轧制量");

            HSSFCell cell_2_9 = row1.createCell(8);   //创建第二行第七列
            cell_2_9.setCellValue("材质");



            //从第三行开始(包含第三行)
            for (int i = 0; i < list.size(); i++) {
                BaseCostAccountingReport v = list.get(i);
                HSSFRow rown = sheet.createRow(i + 2);   //创建第三行
                HSSFCell cell_3_1 = rown.createCell(0);   // 1
                cell_3_1.setCellValue(i + 1);
                HSSFCell cell_3_2 = rown.createCell(1);   //2
                if (!StringUtils.isEmpty(v.getFactory())) {
                    cell_3_2.setCellValue(v.getFactory());
                }
                HSSFCell cell_3_3 = rown.createCell(2);   //3
                if (!StringUtils.isEmpty(v.getFrame_no())) {
                    cell_3_3.setCellValue(v.getFrame_no());
                }
                HSSFCell cell_3_4 = rown.createCell(3);   //4
                if (!StringUtils.isEmpty(v.getRolltonnage())) {
                    cell_3_4.setCellValue(v.getRolltonnage().toString());
                }
                HSSFCell cell_3_5 = rown.createCell(4);   //5
                if (!StringUtils.isEmpty(v.getRollkilometer())) {
                    cell_3_5.setCellValue(v.getRollkilometer().toString());
                }
                HSSFCell cell_3_6 = rown.createCell(5);   //6
                if (!StringUtils.isEmpty(v.getMmnumber())) {
                    cell_3_6.setCellValue(v.getMmnumber().toString());
                }
                HSSFCell cell_3_7 = rown.createCell(6);   //7
                if (!StringUtils.isEmpty(v.getWeight())) {
                    cell_3_7.setCellValue(v.getWeight().toString());
                }
                HSSFCell cell_3_8 = rown.createCell(7);   //7
                if (!StringUtils.isEmpty(v.getConsumption())) {
                    cell_3_8.setCellValue(v.getConsumption().toString());
                }

                HSSFCell cell_3_9 = rown.createCell(8);   //7
                if (!StringUtils.isEmpty(v.getMaterial())) {
                    cell_3_9.setCellValue(v.getMaterial());
                }

            }
            CellRangeAddress region = new CellRangeAddress(0, 0, 0, 8);
            sheet.addMergedRegion(region);
            }
        }

    private void getExcelBaseCostAccounting(List<BaseCostAccountingReport> list, BaseCostAccountingReport baseCostAccountingReport, HSSFSheet sheet, HSSFWorkbook workbook) {
        if(list!=null && list.size()>0) {
            // 设置表头样式 居中 加粗 宋体 20
            HSSFCellStyle cellStyleHead = workbook.createCellStyle();
            cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
            HSSFFont redFontHead = workbook.createFont();
            redFontHead.setFontHeightInPoints((short) 12);//设置字体大小
            redFontHead.setFontName("宋体");//字体型号
            cellStyleHead.setFont(redFontHead);
            HSSFRow rowhead = sheet.createRow(0);   //创建第一行头
            HSSFCell cell_00 = rowhead.createCell(0);   //创建第一行第一列
            cell_00.setCellStyle(cellStyleHead);
            cell_00.setCellValue("每月成本核算");

            HSSFRow row1 = sheet.createRow(1);   //创建第二行
            HSSFCell cell_2_1 = row1.createCell(0);   //创建第二行第一列
            cell_2_1.setCellValue("序号");
            HSSFCell cell_2_2 = row1.createCell(1);   //创建第二行第二列
            cell_2_2.setCellValue("料号");
            HSSFCell cell_2_3 = row1.createCell(2);   //创建第二行第三列
            cell_2_3.setCellValue("轧辊类型");
            HSSFCell cell_2_4 = row1.createCell(3);   //创建第二行第四列
            cell_2_4.setCellValue("规格");
            HSSFCell cell_2_5 = row1.createCell(4);   //创建第二行第五列
            cell_2_5.setCellValue("机架范围");
            HSSFCell cell_2_6 = row1.createCell(5);   //创建第二行第六列
            cell_2_6.setCellValue("材质");
            HSSFCell cell_2_7 = row1.createCell(6);   //创建第二行第七列
            cell_2_7.setCellValue("折合毫米工作层重量（t）");
            HSSFCell cell_2_8 = row1.createCell(7);   //创建第二行第八列
            cell_2_8.setCellValue("轧辊单价");

            HSSFCell cell_2_9 = row1.createCell(8);   //创建第二行第9列
            cell_2_9.setCellValue("毫米");
            HSSFCell cell_2_10 = row1.createCell(9);   //创建第二行第10列
            cell_2_10.setCellValue("重量");

            HSSFCell cell_2_11 = row1.createCell(10);   //创建第二行第11列
            cell_2_11.setCellValue("金额");
            HSSFCell cell_2_12 = row1.createCell(11);   //创建第二行第12列
            cell_2_12.setCellValue("吨钢消耗（kg）");
            HSSFCell cell_2_13 = row1.createCell(12);   //创建第二行第13列
            cell_2_13.setCellValue("吨钢成本（元/t）");
            //从第三行开始(包含第三行)
            for (int i = 0; i < list.size(); i++) {
                BaseCostAccountingReport v = list.get(i);
                HSSFRow rown = sheet.createRow(i + 2);   //创建第三行
                HSSFCell cell_3_1 = rown.createCell(0);   // 1
                cell_3_1.setCellValue(i + 1);
                HSSFCell cell_3_2 = rown.createCell(1);   //2
                if (!StringUtils.isEmpty(v.getMaterial_no())) {
                    cell_3_2.setCellValue(v.getMaterial_no());
                }
                HSSFCell cell_3_3 = rown.createCell(2);   //3
                if (!StringUtils.isEmpty(v.getRoll_type())) {
                    cell_3_3.setCellValue(v.getRoll_type());
                }
                HSSFCell cell_3_4 = rown.createCell(3);   //4
                if (!StringUtils.isEmpty(v.getSpecifications_no())) {
                    cell_3_4.setCellValue(v.getSpecifications_no());
                }
                HSSFCell cell_3_5 = rown.createCell(4);   //5
                if (!StringUtils.isEmpty(v.getFramerange())) {
                    cell_3_5.setCellValue(v.getFramerange());
                }
                HSSFCell cell_3_6 = rown.createCell(5);   //6
                if (!StringUtils.isEmpty(v.getMaterial())) {
                    cell_3_6.setCellValue(v.getMaterial());
                }
                HSSFCell cell_3_7 = rown.createCell(6);   //7
                if (!StringUtils.isEmpty(v.getWorking_layer())) {
                    cell_3_7.setCellValue(v.getWorking_layer().toString());
                }
                HSSFCell cell_3_8 = rown.createCell(7);   //8
                if (!StringUtils.isEmpty(v.getUnit_price())) {
                    cell_3_8.setCellValue(v.getUnit_price().toString());
                }
                HSSFCell cell_3_9 = rown.createCell(8);   //9
                if (!StringUtils.isEmpty(v.getMmnumber())) {
                    cell_3_9.setCellValue(v.getMmnumber().toString());
                }
                HSSFCell cell_3_10 = rown.createCell(9);   //10
                if (!StringUtils.isEmpty(v.getWeight())) {
                        cell_3_10.setCellValue(v.getWeight().toString());
                }
                HSSFCell cell_3_11 = rown.createCell(10);   //11
                if (!StringUtils.isEmpty(v.getMoney())) {
                    cell_3_11.setCellValue(v.getMoney().toString());
                }
                HSSFCell cell_3_12 = rown.createCell(11);   //12
                if (!StringUtils.isEmpty(v.getConsumption())) {
                    cell_3_12.setCellValue(v.getConsumption().toString());
                }
                HSSFCell cell_3_13 = rown.createCell(12);   //12
                if (!StringUtils.isEmpty(v.getCost())) {
                    cell_3_13.setCellValue(v.getCost().toString());
                }
            }
            CellRangeAddress region = new CellRangeAddress(0, 0, 0, 12);
            sheet.addMergedRegion(region);
        }
    }

}
