package com.my.business.modular.basechock.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.basechock.dao.BaseCostAccountingChildDao;
import com.my.business.modular.basechock.dao.BaseEquipmentDao;
import com.my.business.modular.basechock.entity.BaseCostAccountingChild;
import com.my.business.modular.basechock.entity.BaseEquipment;
import com.my.business.modular.basechock.entity.JBaseCostAccountingChild;
import com.my.business.modular.basechock.entity.JBaseEquipment;
import com.my.business.modular.basechock.service.BaseCostAccountingChildService;
import com.my.business.modular.basechock.service.BaseEquipmentService;
import com.my.business.modular.dictionary.dao.DictionaryDao;
import com.my.business.modular.dictionary.entity.Dictionary;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 料号规格定制子表 综合台账接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@Service
public class BaseCostAccountingChildServiceImpl implements BaseCostAccountingChildService {

    @Autowired
    private BaseCostAccountingChildDao baseCostAccountingChildDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataBaseCostAccountingChild(String data, Long userId, String sname) {
        try {
            Long time = 0L;
            JBaseCostAccountingChild jBaseCostAccountingChild = JSON.parseObject(data, JBaseCostAccountingChild.class);
            BaseCostAccountingChild baseCostAccountingChild = jBaseCostAccountingChild.getBaseCostAccountingChild();
            CodiUtil.newRecord(userId, sname, baseCostAccountingChild);

            baseCostAccountingChildDao.insertDataBaseCostAccountingChild(baseCostAccountingChild);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataBaseCostAccountingChildOne(Long indocno) {
        try {
            baseCostAccountingChildDao.deleteDataBaseCostAccountingChildOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }


    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataBaseCostAccountingChild(String data, Long userId, String sname) {
        try {
            JBaseCostAccountingChild jBaseCostAccountingChild = JSON.parseObject(data, JBaseCostAccountingChild.class);
            BaseCostAccountingChild baseCostAccountingChild = jBaseCostAccountingChild.getBaseCostAccountingChild();
            CodiUtil.editRecord(userId, sname, baseCostAccountingChild);
            baseCostAccountingChildDao.updateDataBaseCostAccountingChild(baseCostAccountingChild);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataBaseCostAccountingChildByPage(String data) {
        try {
            JBaseCostAccountingChild jBaseCostAccountingChild= JSON.parseObject(data, JBaseCostAccountingChild.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jBaseCostAccountingChild.getPageIndex();
            Integer pageSize = jBaseCostAccountingChild.getPageSize();
            if (null != jBaseCostAccountingChild.getCondition()) {
                jsonObject = JSON.parseObject(jBaseCostAccountingChild.getCondition().toString());
            }else{
                jsonObject = new JSONObject();
            }
            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }
            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }
            Long parent_id = jBaseCostAccountingChild.getParent_id();
            List<BaseCostAccountingChild>  list = baseCostAccountingChildDao.findDataBaseCostAccountingChildByPage((pageIndex - 1) * pageSize, pageSize,parent_id,dbegin);
            Integer count = baseCostAccountingChildDao.findDataBaseCostAccountingChildByPageSize(parent_id,dbegin);
            return ResultData.ResultDataSuccess(list,count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataBaseCostAccountingChildByIndocno(String data) {
        try {
            JBaseCostAccountingChild jBaseCostAccountingChild= JSON.parseObject(data, JBaseCostAccountingChild.class);
            Long indocno = jBaseCostAccountingChild.getIndocno();
            BaseCostAccountingChild baseCostAccountingChild = baseCostAccountingChildDao.findDataBaseCostAccountingChildByIndocno(indocno);
            return ResultData.ResultDataSuccess(baseCostAccountingChild);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

}
