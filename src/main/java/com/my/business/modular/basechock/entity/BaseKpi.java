package com.my.business.modular.basechock.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.math.BigDecimal;

/**
 * kpi
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
public class BaseKpi extends BaseEntity {

    private Long indocno;  //主键
    private String kpiname;  //名称
    private BigDecimal expect_value;//期望值
    private BigDecimal actual_value;//实际值

    private BigDecimal proportion;

    public BigDecimal getProportion() {
        return proportion;
    }

    public void setProportion(BigDecimal proportion) {
        this.proportion = proportion;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getKpiname() {
        return kpiname;
    }

    public void setKpiname(String kpiname) {
        this.kpiname = kpiname;
    }

    public BigDecimal getExpect_value() {
        return expect_value;
    }

    public void setExpect_value(BigDecimal expect_value) {
        this.expect_value = expect_value;
    }

    public BigDecimal getActual_value() {
        return actual_value;
    }

    public void setActual_value(BigDecimal actual_value) {
        this.actual_value = actual_value;
    }
}