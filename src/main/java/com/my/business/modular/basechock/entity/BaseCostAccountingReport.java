package com.my.business.modular.basechock.entity;

import com.my.business.sys.common.entity.BaseEntity;

import java.math.BigDecimal;

/**
 * 料号规格定制主表综合台账实体类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
public class BaseCostAccountingReport extends BaseEntity {

    private Long indocno;  //主键
    private Long material_noid;//料号id
    private String material_no;  //料号名称

    private Long specifications_noid;//规格id
    private String specifications_no;  //规格名称

    private Long roll_typeid;//轧辊类型id
    private String roll_type;  //轧辊类型名称

    private Long material_id;//材质id
    private String material;  //材质名称

    private Long framerangeid;//机架范围id
    private String framerange;  //机架范围名称

    private String usetime;//开始时间
    private BigDecimal unit_price;//单价
    private BigDecimal working_layer;//工作层
    private Long parent_id;//父项id

private  BigDecimal production;//产量

    private BigDecimal money;//金额
    private BigDecimal consumption;//吨钢消耗
    private BigDecimal cost;//吨钢成本


    private Long factory_id;//厂家id
    private String factory;//厂家名称
    private Long frame_noid;//机架id
    private String frame_no;//机架名称
    private BigDecimal mmnumber ;//毫米数
    private BigDecimal weight;//重量
    private BigDecimal rollkilometer;//轧制公里数
    private BigDecimal rolltonnage;//轧制吨数

    public BigDecimal getProduction() {
        return production;
    }

    public void setProduction(BigDecimal production) {
        this.production = production;
    }

    public Long getFactory_id() {
        return factory_id;
    }

    public void setFactory_id(Long factory_id) {
        this.factory_id = factory_id;
    }

    public String getFactory() {
        return factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public Long getFrame_noid() {
        return frame_noid;
    }

    public void setFrame_noid(Long frame_noid) {
        this.frame_noid = frame_noid;
    }

    public String getFrame_no() {
        return frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public BigDecimal getRollkilometer() {
        return rollkilometer;
    }

    public void setRollkilometer(BigDecimal rollkilometer) {
        this.rollkilometer = rollkilometer;
    }

    public BigDecimal getRolltonnage() {
        return rolltonnage;
    }

    public void setRolltonnage(BigDecimal rolltonnage) {
        this.rolltonnage = rolltonnage;
    }

    public BigDecimal getMmnumber() {
        return mmnumber;
    }

    public void setMmnumber(BigDecimal mmnumber) {
        this.mmnumber = mmnumber;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getConsumption() {
        return consumption;
    }

    public void setConsumption(BigDecimal consumption) {
        this.consumption = consumption;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Long getParent_id() {
        return parent_id;
    }

    public void setParent_id(Long parent_id) {
        this.parent_id = parent_id;
    }

    public String getUsetime() {
        return usetime;
    }

    public void setUsetime(String usetime) {
        this.usetime = usetime;
    }

    public BigDecimal getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(BigDecimal unit_price) {
        this.unit_price = unit_price;
    }

    public BigDecimal getWorking_layer() {
        return working_layer;
    }

    public void setWorking_layer(BigDecimal working_layer) {
        this.working_layer = working_layer;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getMaterial_noid() {
        return material_noid;
    }

    public void setMaterial_noid(Long material_noid) {
        this.material_noid = material_noid;
    }

    public String getMaterial_no() {
        return material_no;
    }

    public void setMaterial_no(String material_no) {
        this.material_no = material_no;
    }

    public Long getSpecifications_noid() {
        return specifications_noid;
    }

    public void setSpecifications_noid(Long specifications_noid) {
        this.specifications_noid = specifications_noid;
    }

    public String getSpecifications_no() {
        return specifications_no;
    }

    public void setSpecifications_no(String specifications_no) {
        this.specifications_no = specifications_no;
    }

    public Long getRoll_typeid() {
        return roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public Long getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(Long material_id) {
        this.material_id = material_id;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public Long getFramerangeid() {
        return framerangeid;
    }

    public void setFramerangeid(Long framerangeid) {
        this.framerangeid = framerangeid;
    }

    public String getFramerange() {
        return framerange;
    }

    public void setFramerange(String framerange) {
        this.framerange = framerange;
    }
}