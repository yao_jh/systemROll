package com.my.business.modular.basechock.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.basechock.entity.BaseChock;
import com.my.business.modular.basechock.entity.JBaseChock;
import com.my.business.modular.basechock.service.BaseChockHistoryService;
import com.my.business.modular.basechock.service.BaseChockService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 轴承座综合台账控制器层
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@RestController
@RequestMapping("/baseChockHistory")
public class BaseChockHistoryController {

    @Autowired
    private BaseChockHistoryService baseChockHistoryService;

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataBaseChockHistoryByPage(@RequestBody String data) {
        return baseChockHistoryService.findDataBaseChockHistoryByPage(data);
    }

}
