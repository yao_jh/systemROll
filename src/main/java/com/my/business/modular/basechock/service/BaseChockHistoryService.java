package com.my.business.modular.basechock.service;

import com.my.business.modular.basechock.entity.BaseChock;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 轴承座综合台账接口服务类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
public interface BaseChockHistoryService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataBaseChockHistory(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataBaseChockHistoryByPage(String data);

}
