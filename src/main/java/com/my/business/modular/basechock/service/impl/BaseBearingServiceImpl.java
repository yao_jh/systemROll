package com.my.business.modular.basechock.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.basechock.dao.BaseBearingDao;
import com.my.business.modular.basechock.dao.BaseChockDao;
import com.my.business.modular.basechock.dao.BaseChockHistoryDao;
import com.my.business.modular.basechock.entity.*;
import com.my.business.modular.basechock.service.BaseBearingService;
import com.my.business.modular.basechock.service.BaseChockService;
import com.my.business.modular.chockdismounting.dao.ChockDismountingDao;
import com.my.business.modular.chockdismounting.entity.ChockDismounting;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.modular.rollonoffline.entity.RollOnoffLine;
import com.my.business.modular.rollwear.service.impl.RollWearServiceImpl;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 轴承综合台账接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-24 10:34:49
 */
@Service
public class BaseBearingServiceImpl implements BaseBearingService {
    private final static Log logger = LogFactory.getLog(BaseBearingServiceImpl.class);

    @Autowired
    private BaseBearingDao baseBearingDao;

    @Autowired
    private BaseChockDao baseChockDao;
    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataBaseBearing(String data, Long userId, String sname) {
        try {
            JBaseBearing jbaseBearing = JSON.parseObject(data, JBaseBearing.class);
            BaseBearing baseBearing = jbaseBearing.getBaseBearing();
            CodiUtil.newRecord(userId, sname, baseBearing);
            if(!StringUtils.isEmpty(baseBearing.getBearing_no())&&baseBearingDao.findDataBaseBearingByBearingNo(baseBearing.getBearing_no())!=null){
                return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + "重复请重新输入", null);
            }
            baseBearingDao.insertDataBaseBearing(baseBearing);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataBaseBearingOne(Long indocno) {
        try {
            baseBearingDao.deleteDataBaseBearingOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataBaseBearingMany(String str_id) {
        try {
            String sql = "delete from base_bearing where indocno in(" + str_id + ")";
            baseBearingDao.deleteDataBaseBearingMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataBaseBearing(String data, Long userId, String sname) {
        try {
            JBaseBearing jbaseBearing = JSON.parseObject(data, JBaseBearing.class);
            BaseBearing baseBearing = jbaseBearing.getBaseBearing();
            CodiUtil.editRecord(userId, sname, baseBearing);
            if(StringUtils.isEmpty(baseBearing.getBearing_type())){
                return ResultData.ResultDataFaultSelf("修改失败,失败信息为", null);
            }
            if(StringUtils.isEmpty(baseBearing.getChock_no())){
                baseBearing.setSingle_times(BigDecimal.ZERO);
            }

            if(!StringUtils.isEmpty(baseBearing.getSingle_times())){
                baseBearing.setSingle_times((baseBearing.getSingle_times().multiply(BigDecimal.valueOf(60*60*24))));
            }
            if(!StringUtils.isEmpty(baseBearing.getTotal_times())){
                baseBearing.setTotal_times((baseBearing.getTotal_times().multiply(BigDecimal.valueOf(60*60*24))));
            }
            baseBearingDao.updateDataBaseBearing(baseBearing);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataBaseBearingByPage(String data) {
        try {
            JBaseBearing jbaseBearing = JSON.parseObject(data, JBaseBearing.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jbaseBearing.getPageIndex();
            Integer pageSize = jbaseBearing.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jbaseBearing.getCondition()) {
                jsonObject = JSON.parseObject(jbaseBearing.getCondition().toString());
            }

            String chock_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("chock_no"))) {
                chock_no = jsonObject.get("chock_no").toString();
            }

            String bearing_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("bearing_no"))) {
                bearing_no = jsonObject.get("bearing_no").toString();
            }
            Integer ifdiscard = 0;
            if (!StringUtils.isEmpty(jsonObject.get("ifdiscard"))) {
                ifdiscard = Integer.valueOf(jsonObject.get("ifdiscard").toString());
            }

            Integer bearing_type = 0;
            if (!StringUtils.isEmpty(jsonObject.get("bearing_type"))) {
                bearing_type = Integer.valueOf(jsonObject.get("bearing_type").toString());
            }
            String seals_type = null;
            if (!StringUtils.isEmpty(jsonObject.get("seals_type"))) {
                seals_type = jsonObject.get("seals_type").toString();
            }
            List<BaseBearing> list = baseBearingDao.findDataBaseBearingByPage((pageIndex - 1) * pageSize, pageSize,chock_no,bearing_no,ifdiscard,bearing_type,seals_type);
            Integer count = baseBearingDao.findDataBaseBearingByPageSize(chock_no,bearing_no,ifdiscard,bearing_type,seals_type);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataBaseBearingByIndocno(String data) {
        try {
            JBaseBearing jbaseBearing= JSON.parseObject(data, JBaseBearing.class);
            Long indocno = jbaseBearing.getIndocno();
            BaseBearing baseBearing = baseBearingDao.findDataBaseBearingByIndocno(indocno);
            return ResultData.ResultDataSuccess(baseBearing);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<BaseBearing> findDataBaseBearing() {
        List<BaseBearing> list = baseBearingDao.findDataBaseBearing();
        return list;
    }

    public void changeBearingData(RollOnoffLine rollOnoffLine) {
        Long time = 0L;
        if(!StringUtils.isEmpty(rollOnoffLine.getRoll_no())&&!StringUtils.isEmpty(rollOnoffLine.getOnlinetime())&&!StringUtils.isEmpty(rollOnoffLine.getOfflinetime())){
            //根据棍子查询轴承座信息
            time=DateUtil.getStringToDate("yyyy-MM-dd HH:mm:ss",rollOnoffLine.getOfflinetime()).getTime() - DateUtil.getStringToDate("yyyy-MM-dd HH:mm:ss",rollOnoffLine.getOnlinetime()).getTime();
            if(time!=null&&time>0){
                // 根据 棍子 查询 轴承座号
                List<BaseChock> listChock = baseChockDao.findDataBaseChockByPage(0, 10,null,rollOnoffLine.getRoll_no(),null,null,null,null,null,0);
                List<BaseBearing> listBearing = null;
                if(listChock!=null&&listChock.size()>0){
                    for(BaseChock baseChock:listChock) {

                        if(StringUtils.isEmpty(baseChock.getSingle_times())){
                            baseChock.setSingle_times(BigDecimal.ZERO);
                        }
                        if(StringUtils.isEmpty(baseChock.getTotal_times())){
                            baseChock.setTotal_times(BigDecimal.ZERO);
                        }
                        baseChock.setSingle_times((baseChock.getSingle_times().multiply(BigDecimal.valueOf(60*60*24))).add(BigDecimal.valueOf(time/1000)));
                        baseChock.setTotal_times((baseChock.getTotal_times().multiply(BigDecimal.valueOf(60*60*24))).add(BigDecimal.valueOf(time/1000)));
                        //增加轴承座的累计时间
                        baseChockDao.updateDataBaseChock(baseChock);
                        logger.debug("累计轴承座时长"+baseChock.getChock_no()+"棍子时长"+baseChock.getRoll_no());
                        //根据轴承座号 和 是否报废  查询 轴承和密封件信息
                        listBearing = baseBearingDao.findDataBaseBearingByPage(0,10,baseChock.getChock_no(),null,0,null,null);
                        if(listBearing!=null&&listBearing.size()>0){
                            for (BaseBearing baseBearing:listBearing) {
                                if(StringUtils.isEmpty(baseBearing.getSingle_times())){
                                    baseBearing.setSingle_times(BigDecimal.ZERO);
                                }
                                if(StringUtils.isEmpty(baseBearing.getTotal_times())){
                                    baseBearing.setTotal_times(BigDecimal.ZERO);
                                }
                                baseBearing.setSingle_times((baseBearing.getSingle_times().multiply(BigDecimal.valueOf(60*60*24))).add(BigDecimal.valueOf(time/1000)));
                                baseBearing.setTotal_times((baseBearing.getTotal_times().multiply(BigDecimal.valueOf(60*60*24))).add(BigDecimal.valueOf(time/1000)));
                                baseBearingDao.updateDataBaseBearing(baseBearing);
                                logger.debug("累计轴承座时长"+baseBearing.getChock_no()+"累计轴承时长"+baseBearing.getBearing_no());
                            }
                        }
                    }
                }
            }
        }
    }

    public ResultData updateDataBaseBearingSingleTimes(String data) {
        try {
            JBaseBearing jbaseBearing= JSON.parseObject(data, JBaseBearing.class);
            Long indocno = jbaseBearing.getIndocno();
            baseBearingDao.updateDataBaseBearingSingleTimes(indocno);
            return ResultData.ResultDataSuccessSelf("修改数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public ResultData updateDataBaseBearingDiscardTime(String data) {
        try {
            JBaseBearing jbaseBearing= JSON.parseObject(data, JBaseBearing.class);
            Long indocno = jbaseBearing.getIndocno();
            baseBearingDao.updateDataBaseBearingDiscardTime(indocno);
            return ResultData.ResultDataSuccessSelf("修改数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public ResultData updateDataBaseBearingByChockNo(String data) {
        try {
        JBaseBearing jbaseBearing= JSON.parseObject(data, JBaseBearing.class);
        BaseBearing baseBearing = jbaseBearing.getBaseBearing();
        Long indocno = baseBearing.getIndocno();
        BaseBearing bean = baseBearingDao.findDataBaseBearingByIndocno(indocno);
        bean.setChock_no(baseBearing.getChock_no());
        baseBearingDao.updateDataBaseBearing(bean);
        return ResultData.ResultDataSuccessSelf("修改数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    @Override
    public ResultData findListByBearingType(String data) {
        try {
        JBaseBearing jbaseBearing= JSON.parseObject(data, JBaseBearing.class);
        BaseBearing baseBearing = jbaseBearing.getBaseBearing();
        int bearing_type = baseBearing.getBearing_type();
            String seals_type = null;
            if(!StringUtils.isEmpty(baseBearing.getSeals_type())&&baseBearing.getSeals_type()!=0){
                 seals_type = baseBearing.getSeals_type()+"";
             }

        List<BaseBearing> list = baseBearingDao.findListByBearingType(bearing_type,seals_type);
            return ResultData.ResultDataSuccessSelf("查询数据成功", list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    public ResultData updateDataBaseBearingDiscardTimeCancel(String data) {
        try {
            JBaseBearing jbaseBearing= JSON.parseObject(data, JBaseBearing.class);
            Long indocno = jbaseBearing.getIndocno();
            baseBearingDao.updateDataBaseBearingDiscardTimeCancel(indocno);
            return ResultData.ResultDataSuccessSelf("修改数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

}
