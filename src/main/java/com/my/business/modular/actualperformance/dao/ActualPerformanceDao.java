package com.my.business.modular.actualperformance.dao;

import com.my.business.modular.actualperformance.entity.ActualPerformance;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 实绩表dao接口
 *
 * @author 生成器生成
 * @date 2020-08-12 14:59:56
 */
@Mapper
public interface ActualPerformanceDao {

    /**
     * 添加记录
     *
     * @param actualPerformance 对象实体
     */
    void insertDataActualPerformance(ActualPerformance actualPerformance);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataActualPerformanceOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataActualPerformanceMany(String value);

    /**
     * 修改记录
     *
     * @param actualPerformance 对象实体
     */
    void updateDataActualPerformance(ActualPerformance actualPerformance);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<ActualPerformance> findDataActualPerformanceByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("roll_no") String roll_no);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataActualPerformanceByPageSize(@Param("roll_no") String roll_no);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    ActualPerformance findDataActualPerformanceByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<ActualPerformance> findDataActualPerformance();

}
