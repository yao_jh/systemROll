package com.my.business.modular.actualperformance.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 实绩表实体类
 *
 * @author 生成器生成
 * @date 2020-08-12 14:59:56
 */
public class ActualPerformance extends BaseEntity {

    private Long indocno;  //主键
    private String roll_no;  //辊号
    private String check_time;  //检测时间
    private Long ifok;  //检测是否合格
    private String repair_time;  //检测时间
    private String repair_rate;  //修复量
    private Double roll_diameter;  //当前辊径(mm)
    private String repaired_upline;  //修复后上机时间
    private String repaired_downline;  //修复后下机时间
    private Long downline_state;  //下机状态

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getCheck_time() {
        return this.check_time;
    }

    public void setCheck_time(String check_time) {
        this.check_time = check_time;
    }

    public Long getIfok() {
        return this.ifok;
    }

    public void setIfok(Long ifok) {
        this.ifok = ifok;
    }

    public String getRepair_time() {
        return this.repair_time;
    }

    public void setRepair_time(String repair_time) {
        this.repair_time = repair_time;
    }

    public String getRepair_rate() {
        return this.repair_rate;
    }

    public void setRepair_rate(String repair_rate) {
        this.repair_rate = repair_rate;
    }

    public Double getRoll_diameter() {
        return this.roll_diameter;
    }

    public void setRoll_diameter(Double roll_diameter) {
        this.roll_diameter = roll_diameter;
    }

    public String getRepaired_upline() {
        return this.repaired_upline;
    }

    public void setRepaired_upline(String repaired_upline) {
        this.repaired_upline = repaired_upline;
    }

    public String getRepaired_downline() {
        return this.repaired_downline;
    }

    public void setRepaired_downline(String repaired_downline) {
        this.repaired_downline = repaired_downline;
    }

    public Long getDownline_state() {
        return this.downline_state;
    }

    public void setDownline_state(Long downline_state) {
        this.downline_state = downline_state;
    }


}