package com.my.business.modular.actualperformance.service;

import com.my.business.modular.actualperformance.entity.ActualPerformance;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 实绩表接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-12 14:59:56
 */
public interface ActualPerformanceService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataActualPerformance(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataActualPerformanceOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataActualPerformanceMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataActualPerformance(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataActualPerformanceByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataActualPerformanceByIndocno(String data);

    /**
     * 查看记录
     */
    List<ActualPerformance> findDataActualPerformance();
}
