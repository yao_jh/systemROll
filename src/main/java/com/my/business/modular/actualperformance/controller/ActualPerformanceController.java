package com.my.business.modular.actualperformance.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.actualperformance.entity.ActualPerformance;
import com.my.business.modular.actualperformance.entity.JActualPerformance;
import com.my.business.modular.actualperformance.service.ActualPerformanceService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 实绩表控制器层
 *
 * @author 生成器生成
 * @date 2020-08-12 14:59:56
 */
@RestController
@RequestMapping("/actualPerformance")
public class ActualPerformanceController {

    @Autowired
    private ActualPerformanceService actualPerformanceService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataActualPerformance(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return actualPerformanceService.insertDataActualPerformance(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataActualPerformanceOne(@RequestBody String data) {
        try {
            JActualPerformance jactualPerformance = JSON.parseObject(data, JActualPerformance.class);
            return actualPerformanceService.deleteDataActualPerformanceOne(jactualPerformance.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JActualPerformance jactualPerformance = JSON.parseObject(data, JActualPerformance.class);
            return actualPerformanceService.deleteDataActualPerformanceMany(jactualPerformance.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataActualPerformance(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return actualPerformanceService.updateDataActualPerformance(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataActualPerformanceByPage(@RequestBody String data) {
        return actualPerformanceService.findDataActualPerformanceByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataActualPerformanceByIndocno(@RequestBody String data) {
        return actualPerformanceService.findDataActualPerformanceByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<ActualPerformance> findDataActualPerformance() {
        return actualPerformanceService.findDataActualPerformance();
    }

}
