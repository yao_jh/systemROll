package com.my.business.modular.rollpaired.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollpaired.entity.JRollPaired;
import com.my.business.modular.rollpaired.entity.RollPaired;
import com.my.business.modular.rollpaired.entity.RollPairedHistory;
import com.my.business.modular.rollpaired.service.RollPairedHistoryService;
import com.my.business.modular.rollpaired.service.RollPairedService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 轧辊配对表控制器层
 *
 * @author 生成器生成
 * @date 2020-08-11 13:45:14
 */
@RestController
@RequestMapping("/rollPairedHistory")
public class RollPairedHistoryController {

    @Autowired
    private RollPairedHistoryService rollPairedHistoryService;


    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollPairedHistoryByPage(@RequestBody String data) {
        return rollPairedHistoryService.findDataRollPairedHistoryByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollPairedHistoryByIndocno(@RequestBody String data) {
        return rollPairedHistoryService.findDataRollPairedHistoryByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollPairedHistory> findDataRollPairedHistory() {
        return rollPairedHistoryService.findDataRollPairedHistory();
    }

}
