package com.my.business.modular.rollpaired.service;

import com.my.business.modular.rollpaired.entity.RollPaired;
import com.my.business.modular.rollpaired.entity.RollPairedHistory;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 轧辊配对表接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-11 13:45:14
 */
public interface RollPairedHistoryService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollPairedHistory(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPairedHistoryByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPairedHistoryByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollPairedHistory> findDataRollPairedHistory();

}
