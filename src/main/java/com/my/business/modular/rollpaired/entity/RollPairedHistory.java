package com.my.business.modular.rollpaired.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轧辊配对表实体类
 *
 * @author 生成器生成
 * @date 2020-08-11 13:45:13
 */
public class RollPairedHistory extends BaseEntity {

    private Long indocno;  //主键
    private Long ilinkno_up;  //外键——上辊主键
    private String roll_no_up;  //上辊辊号
    private Long ilinkno_down;  //外键——下辊主键
    private String roll_no_down;  //下辊辊号
    private Long frame_noid;  //使用机组号id
    private String frame_no;  //使用机组号
    private String production_line;  //产线
    private Long production_line_id; //产线id
    private String factory;  //生产厂家
    private String material;  //材质
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private String roll_revolve;  //轧辊周转状态
    private String dbegin;
    private String dend;
    private String operate_time;  //操作时间
    private String operate_name;  //操作模式( 配对 拆解 )

    public String getDbegin() {
        return dbegin;
    }

    public void setDbegin(String dbegin) {
        this.dbegin = dbegin;
    }

    public String getDend() {
        return dend;
    }

    public void setDend(String dend) {
        this.dend = dend;
    }

    public String getOperate_time() {
        return operate_time;
    }

    public void setOperate_time(String operate_time) {
        this.operate_time = operate_time;
    }

    public String getOperate_name() {
        return operate_name;
    }

    public void setOperate_name(String operate_name) {
        this.operate_name = operate_name;
    }

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getIlinkno_up() {
        return this.ilinkno_up;
    }

    public void setIlinkno_up(Long ilinkno_up) {
        this.ilinkno_up = ilinkno_up;
    }

    public String getRoll_no_up() {
        return this.roll_no_up;
    }

    public void setRoll_no_up(String roll_no_up) {
        this.roll_no_up = roll_no_up;
    }

    public Long getIlinkno_down() {
        return this.ilinkno_down;
    }

    public void setIlinkno_down(Long ilinkno_down) {
        this.ilinkno_down = ilinkno_down;
    }

    public String getRoll_no_down() {
        return this.roll_no_down;
    }

    public void setRoll_no_down(String roll_no_down) {
        this.roll_no_down = roll_no_down;
    }

    public Long getFrame_noid() {
        return frame_noid;
    }

    public void setFrame_noid(Long frame_noid) {
        this.frame_noid = frame_noid;
    }

    public String getFrame_no() {
        return frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

	public String getProduction_line() {
		return production_line;
	}

	public void setProduction_line(String production_line) {
		this.production_line = production_line;
	}

	public Long getProduction_line_id() {
		return production_line_id;
	}

	public void setProduction_line_id(Long production_line_id) {
		this.production_line_id = production_line_id;
	}

	public String getFactory() {
		return factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public Long getRoll_typeid() {
		return roll_typeid;
	}

	public void setRoll_typeid(Long roll_typeid) {
		this.roll_typeid = roll_typeid;
	}

	public String getRoll_type() {
		return roll_type;
	}

	public void setRoll_type(String roll_type) {
		this.roll_type = roll_type;
	}

	public String getRoll_revolve() {
		return roll_revolve;
	}

	public void setRoll_revolve(String roll_revolve) {
		this.roll_revolve = roll_revolve;
	}
	
}