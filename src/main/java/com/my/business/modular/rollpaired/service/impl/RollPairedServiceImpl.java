package com.my.business.modular.rollpaired.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollpaired.dao.RollPairedDao;
import com.my.business.modular.rollpaired.dao.RollPairedHistoryDao;
import com.my.business.modular.rollpaired.entity.JRollPaired;
import com.my.business.modular.rollpaired.entity.RollPaired;
import com.my.business.modular.rollpaired.entity.RollPairedHistory;
import com.my.business.modular.rollpaired.service.RollPairedService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.util.DateUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 轧辊配对表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-11 13:45:14
 */
@Service
public class RollPairedServiceImpl implements RollPairedService {

    @Autowired
    private RollPairedDao rollPairedDao;
    @Autowired
    private RollInformationDao rollInformationDao;
    @Autowired
    private RollPairedHistoryDao rollPairedHistoryDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollPaired(String data, Long userId, String sname) {
        try {
            JRollPaired jrollPaired = JSON.parseObject(data, JRollPaired.class);
            RollPaired rollPaired = jrollPaired.getRollPaired();
            CodiUtil.newRecord(userId, sname, rollPaired);
            rollPairedDao.insertDataRollPaired(rollPaired);

            rollInformationDao.updatePairedByNo(rollPaired.getRoll_no_up(),1L,1L,"TOP");
            rollInformationDao.updatePairedByNo(rollPaired.getRoll_no_down(),1L,2L,"BOT");
            RollPairedHistory bean = new RollPairedHistory();
            BeanUtils.copyProperties(rollPaired,bean);

            bean.setOperate_time(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));
            bean.setOperate_name("配对");
            //存储配对历史记录
            rollPairedHistoryDao.insertDataRollPairedHistory(bean);

            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollPairedOne(Long indocno) {
        try {
            rollPairedDao.deleteDataRollPairedOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollPairedMany(String str_id) {
        try {
            String sql = "delete roll_paired where indocno in(" + str_id + ")";
            rollPairedDao.deleteDataRollPairedMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollPaired(String data, Long userId, String sname) {
        try {
            JRollPaired jrollPaired = JSON.parseObject(data, JRollPaired.class);
            RollPaired rollPaired = jrollPaired.getRollPaired();
            CodiUtil.editRecord(userId, sname, rollPaired);
            rollPairedDao.updateDataRollPaired(rollPaired);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPairedByPage(String data) {
        try {
            JRollPaired jrollPaired = JSON.parseObject(data, JRollPaired.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollPaired.getPageIndex();
            Integer pageSize = jrollPaired.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollPaired.getCondition()) {
                jsonObject = JSON.parseObject(jrollPaired.getCondition().toString());
            }

            List<RollPaired> list = rollPairedDao.findDataRollPairedByPage((pageIndex - 1) * pageSize, pageSize,null,null,null, null, null,null,null);
            Integer count = rollPairedDao.findDataRollPairedByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPairedByIndocno(String data) {
        try {
            JRollPaired jrollPaired = JSON.parseObject(data, JRollPaired.class);
            Long indocno = jrollPaired.getIndocno();

            RollPaired rollPaired = rollPairedDao.findDataRollPairedByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollPaired);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollPaired> findDataRollPaired() {
        List<RollPaired> list = rollPairedDao.findDataRollPaired(null,null,null, null, null,null);
        return list;
    }

    public ResultData updateDataRollPairedByRollNo(String data) {
        try{
            JRollPaired jrollPaired = JSON.parseObject(data, JRollPaired.class);
            RollPaired rollPaired = jrollPaired.getRollPaired();
            rollPairedDao.updateDataRollPairedByRollNo(rollPaired);
        return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /***
     * 去除配对
     */
	@Override
	public ResultData removeRollPaired(String data) {
		 try {
            RollPaired rollPaired = JSON.parseObject(data, RollPaired.class);
            RollPaired  roll_remove = rollPairedDao.findDataRollPairedByRollUp(rollPaired.getRoll_no_up());

             if(roll_remove != null){
                 //保存 操作记录
                 RollPairedHistory bean = new RollPairedHistory();
                 BeanUtils.copyProperties(roll_remove,bean);

                 bean.setOperate_time(DateUtil.getDateToString("yyyy-MM-dd HH:mm:ss"));
                 bean.setOperate_name("拆解");
                 //存储配对历史记录
                 rollPairedHistoryDao.insertDataRollPairedHistory(bean);

                 rollPairedDao.deleteDataRollPairedOne(roll_remove.getIndocno());
                 rollInformationDao.updatePairedByNo(roll_remove.getRoll_no_up(),0L,null,null);
                 rollInformationDao.updatePairedByNo(roll_remove.getRoll_no_down(),0L,null,null);
             }


            return ResultData.ResultDataSuccessSelf("拆除成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("拆除失败，错误信息为" + e.getMessage(), null);
        }
	}

}
