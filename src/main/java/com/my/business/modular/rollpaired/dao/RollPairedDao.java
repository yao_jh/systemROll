package com.my.business.modular.rollpaired.dao;

import com.my.business.modular.rollinformation.entity.RollInformation;
import com.my.business.modular.rollpaired.entity.RollPaired;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轧辊配对表dao接口
 *
 * @author 生成器生成
 * @date 2020-08-11 13:45:14
 */
@Mapper
public interface RollPairedDao {

    /**
     * 添加记录
     *
     * @param rollPaired 对象实体
     */
    void insertDataRollPaired(RollPaired rollPaired);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollPairedOne(@Param("indocno") Long indocno);
    

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollPairedMany(String value);

    /**
     * 修改记录
     *
     * @param rollPaired 对象实体
     */
    void updateDataRollPaired(RollPaired rollPaired);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollPaired> findDataRollPairedByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("roll_revolve") String roll_revolve,@Param("production_line_id") String production_line_id,@Param("roll_typeid") String roll_typeid,@Param("factory") String factory,@Param("material") String material,@Param("frame_noid") String frame_noid,@Param("roll_no") String roll_no);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollPairedByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollPaired findDataRollPairedByIndocno(@Param("indocno") Long indocno);
    
    
    /***
     * 根据上辊号和其他参数查询信息
     * @param roll_no_up 上辊号
     * @return
     */
    RollPaired findDataRollPairedByRollUpOther(@Param("roll_no_up") String roll_no_up,@Param("roll_typeid") String roll_typeid,@Param("material") String material,@Param("frame_noid") String frame_noid);
    
    /***
     * 根据下辊号和其他参数查询信息
     * @param roll_no_up 上辊号
     * @return
     */
    RollPaired findDataRollPairedByRollDownOther(@Param("roll_no_down") String roll_no_down,@Param("roll_typeid") String roll_typeid,@Param("material") String material,@Param("frame_noid") String frame_noid);
    

    /***
     * 根据上辊号查询信息
     * @param roll_no_up 上辊号
     * @return
     */
    RollPaired findDataRollPairedByRollUp(@Param("roll_no_up") String roll_no_up);
    
    /***
     * 根据下辊号查询信息
     * @param roll_no_up 上辊号
     * @return
     */
    RollPaired findDataRollPairedByRollDown(@Param("roll_no_down") String roll_no_down);
    
    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollPaired> findDataRollPaired(@Param("roll_revolve") String roll_revolve,@Param("production_line_id") String production_line_id,@Param("roll_typeid") String roll_typeid,@Param("factory") String factory,@Param("material") String material,@Param("frame_noid") String frame_noid);

    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollPaired> findDataRollPairedB(@Param("roll_revolve") String roll_revolve,@Param("production_line_id") Long production_line_id,@Param("roll_typeid") Long roll_typeid,@Param("frame_noid") Long frame_noid);

    /**
     * 同步轧辊状态
     */
    void updateRollRrevolve();

    List<RollInformation> findDataRollInformationByMes(@Param("roll_typeid") Long roll_typeid);

    void updateDataRollPairedByRollNo(RollPaired rollPaired);
}
