package com.my.business.modular.rollpaired.dao;

import com.my.business.modular.rollpaired.entity.RollPaired;
import com.my.business.modular.rollpaired.entity.RollPairedHistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轧辊配对表dao接口
 *
 * @author 生成器生成
 * @date 2020-08-11 13:45:14
 */
@Mapper
public interface RollPairedHistoryDao {

    /**
     * 添加记录
     *
     * @param rollPaired 对象实体
     */
    void insertDataRollPairedHistory(RollPairedHistory rollPairedHistory);

    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollPairedHistory> findDataRollPairedHistoryByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("roll_revolve") String roll_revolve, @Param("production_line_id") String production_line_id, @Param("roll_typeid") String roll_typeid, @Param("factory") String factory, @Param("material") String material, @Param("frame_noid") String frame_noid,@Param("roll_no") String roll_no,@Param("dbegin") String dbegin,@Param("dend") String dend,@Param("operate_name") String operate_name);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollPairedHistoryByPageSize(@Param("roll_no") String roll_no,@Param("dbegin") String dbegin,@Param("dend")String dend,@Param("operate_name")String operate_name);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollPairedHistory findDataRollPairedHistoryByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollPairedHistory> findDataRollPairedHistory(@Param("roll_revolve") String roll_revolve, @Param("production_line_id") String production_line_id, @Param("roll_typeid") String roll_typeid, @Param("factory") String factory, @Param("material") String material, @Param("frame_noid") String frame_noid);


}
