package com.my.business.modular.rollpaired.service;

import com.my.business.modular.rollpaired.entity.RollPaired;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 轧辊配对表接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-11 13:45:14
 */
public interface RollPairedService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollPaired(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollPairedOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollPairedMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollPaired(String data, Long userId, String sname);
    
    /**
     * 去除配对
     *
     * @param data   json字符串
     */
    ResultData removeRollPaired(String data);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPairedByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPairedByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollPaired> findDataRollPaired();

    ResultData updateDataRollPairedByRollNo(String data);
}
