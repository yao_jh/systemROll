package com.my.business.modular.rollpaired.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.modular.rollpaired.dao.RollPairedDao;
import com.my.business.modular.rollpaired.dao.RollPairedHistoryDao;
import com.my.business.modular.rollpaired.entity.JRollPaired;
import com.my.business.modular.rollpaired.entity.JRollPairedHistory;
import com.my.business.modular.rollpaired.entity.RollPaired;
import com.my.business.modular.rollpaired.entity.RollPairedHistory;
import com.my.business.modular.rollpaired.service.RollPairedHistoryService;
import com.my.business.modular.rollpaired.service.RollPairedService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 轧辊配对表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-11 13:45:14
 */
@Service
public class RollPairedHistoryServiceImpl implements RollPairedHistoryService {

    @Autowired
    private RollPairedHistoryDao rollPairedHistoryDao;
    @Autowired
    private RollInformationDao rollInformationDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollPairedHistory(String data, Long userId, String sname) {
        try {
            JRollPairedHistory jrollPairedHistory = JSON.parseObject(data, JRollPairedHistory.class);
            RollPairedHistory rollPairedHistory = jrollPairedHistory.getRollPairedHistory();
            CodiUtil.newRecord(userId, sname, rollPairedHistory);
            rollPairedHistoryDao.insertDataRollPairedHistory(rollPairedHistory);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPairedHistoryByPage(String data) {
        try {
            JRollPairedHistory jrollPairedHistory = JSON.parseObject(data, JRollPairedHistory.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollPairedHistory.getPageIndex();
            Integer pageSize = jrollPairedHistory.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollPairedHistory.getCondition()) {
                jsonObject = JSON.parseObject(jrollPairedHistory.getCondition().toString());
            }else{
                jsonObject = new JSONObject();
            }
            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }
            String dbegin = null;
            if (!StringUtils.isEmpty(jsonObject.get("dbegin"))) {
                dbegin = jsonObject.get("dbegin").toString();
            }
            String operate_name = null;
            if (!StringUtils.isEmpty(jsonObject.get("operate_name"))) {
                operate_name = jsonObject.get("operate_name").toString();
            }

            String dend = null;
            if (!StringUtils.isEmpty(jsonObject.get("dend"))) {
                dend = jsonObject.get("dend").toString();
            }

            List<RollPairedHistory> list = rollPairedHistoryDao.findDataRollPairedHistoryByPage((pageIndex - 1) * pageSize, pageSize,null,null,null, null, null,null,roll_no,dbegin,dend,operate_name);
            Integer count = rollPairedHistoryDao.findDataRollPairedHistoryByPageSize(roll_no,dbegin,dend,operate_name);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPairedHistoryByIndocno(String data) {
        try {
            JRollPairedHistory jrollPairedHistory = JSON.parseObject(data, JRollPairedHistory.class);
            Long indocno = jrollPairedHistory.getIndocno();
            RollPairedHistory rollPairedHistory = rollPairedHistoryDao.findDataRollPairedHistoryByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollPairedHistory);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollPairedHistory> findDataRollPairedHistory() {
        List<RollPairedHistory> list = rollPairedHistoryDao.findDataRollPairedHistory(null,null,null, null, null,null);
        return list;
    }

}
