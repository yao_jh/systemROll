package com.my.business.modular.rolldetectionorder.service;

import com.my.business.modular.rolldetectionorder.entity.RollDetectionorder;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 新辊超声硬度检测工单接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-25 09:31:34
 */
public interface RollDetectionorderService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollDetectionorder(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollDetectionorderOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollDetectionorderMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollDetectionorder(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollDetectionorderByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollDetectionorderByIndocno(String data);
    
    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollDetectionorderByNo(String data);

    /**
     * 查看记录
     */
    List<RollDetectionorder> findDataRollDetectionorder();
    
    /**
     * 查看记录
     */
    ResultData findDataRollDetectionorderByTime(String data);
}
