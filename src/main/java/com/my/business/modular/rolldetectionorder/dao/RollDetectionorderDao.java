package com.my.business.modular.rolldetectionorder.dao;

import com.my.business.modular.rolldetectionorder.entity.RollDetectionorder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 新辊超声硬度检测工单dao接口
 *
 * @author 生成器生成
 * @date 2020-08-25 09:31:33
 */
@Mapper
public interface RollDetectionorderDao {

    /**
     * 添加记录
     *
     * @param rollDetectionorder 对象实体
     */
    void insertDataRollDetectionorder(RollDetectionorder rollDetectionorder);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollDetectionorderOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollDetectionorderMany(String value);

    /**
     * 修改记录
     *
     * @param rollDetectionorder 对象实体
     */
    void updateDataRollDetectionorder(RollDetectionorder rollDetectionorder);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollDetectionorder> findDataRollDetectionorderByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollDetectionorderByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollDetectionorder findDataRollDetectionorderByIndocno(@Param("indocno") Long indocno);
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollDetectionorder findDataRollDetectionorderByNo(@Param("roll_no") String roll_no);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollDetectionorder> findDataRollDetectionorder();
    
    /**
     * 根据辊号查看最新的记录
     *
     * @return 对象数据集合
     */
    List<RollDetectionorder> findDataRollDetectionorderBytime(@Param("roll_no") String roll_no);

}
