package com.my.business.modular.rolldetectionorder.entity;

import com.my.business.sys.common.entity.JCommon;

/**
 * 新辊超声硬度检测工单json的实体类
 *
 * @author 生成器生成
 * @date 2020-08-25 09:31:33
 */
public class JRollDetectionorder extends JCommon {

    private Integer pageIndex; // 第几页
    private Integer pageSize; // 每页多少数据
    private Object condition; // 查询条件
    private RollDetectionorder rollDetectionorder;   //对应模块的实体类
    private Long indocno;
    private String str_indocno;
    private Long flowid;
    private String order_no;
    private String nodeid;  //步骤号
    private String perform_no;  //工作流步骤组编号

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Object getCondition() {
        return condition;
    }

    public void setCondition(Object condition) {
        this.condition = condition;
    }

    public RollDetectionorder getRollDetectionorder() {
        return rollDetectionorder;
    }

    public void setRollDetectionorder(RollDetectionorder rollDetectionorder) {
        this.rollDetectionorder = rollDetectionorder;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getStr_indocno() {
        return str_indocno;
    }

    public void setStr_indocno(String str_indocno) {
        this.str_indocno = str_indocno;
    }

	public String getOrder_no() {
		return order_no;
	}

	public void setOrder_no(String order_no) {
		this.order_no = order_no;
	}

	public String getNodeid() {
		return nodeid;
	}

	public void setNodeid(String nodeid) {
		this.nodeid = nodeid;
	}

	public String getPerform_no() {
		return perform_no;
	}

	public void setPerform_no(String perform_no) {
		this.perform_no = perform_no;
	}

	public Long getFlowid() {
		return flowid;
	}

	public void setFlowid(Long flowid) {
		this.flowid = flowid;
	}
}