package com.my.business.modular.rolldetectionorder.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rolldetectionorder.dao.RollDetectionorderDao;
import com.my.business.modular.rolldetectionorder.entity.JRollDetectionorder;
import com.my.business.modular.rolldetectionorder.entity.RollDetectionorder;
import com.my.business.modular.rolldetectionorder.service.RollDetectionorderService;
import com.my.business.modular.rollorderconfig.dao.RollOrderconfigDao;
import com.my.business.modular.rollorderconfigdetail.entity.RollOrderconfigdetail;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.workflow.workflowdetailperform.dao.WorkFlowdetailPerformDao;
import com.my.business.workflow.workflowdetailperform.entity.WorkFlowdetailPerform;
import com.my.business.workflow.workparam.dao.WorkParamDao;
import com.my.business.workflow.workparam.entity.WorkParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 新辊超声硬度检测工单接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-25 09:31:34
 */
@Service
public class RollDetectionorderServiceImpl implements RollDetectionorderService {

    @Autowired
    private RollDetectionorderDao rollDetectionorderDao;
    
    @Autowired
    private RollOrderconfigDao rollOrderconfigDao;
    
    @Autowired
    private WorkFlowdetailPerformDao workFlowdetailPerformDao;
    
    @Autowired
    private WorkParamDao workParamDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollDetectionorder(String data, Long userId, String sname) {
        try {
            JRollDetectionorder jrollDetectionorder = JSON.parseObject(data, JRollDetectionorder.class);
            RollDetectionorder rollDetectionorder = jrollDetectionorder.getRollDetectionorder();
            CodiUtil.newRecord(userId, sname, rollDetectionorder);
            rollDetectionorderDao.insertDataRollDetectionorder(rollDetectionorder);
            
            WorkFlowdetailPerform wp = workFlowdetailPerformDao.findDataWorkFlowdetailPerformByNodeidAndNoOne(jrollDetectionorder.getPerform_no(), jrollDetectionorder.getNodeid());
            Boolean b = getIfOkByentity(rollDetectionorder);
            
            String l = "";
            if(b) {
            	wp.setIfok(1L);
            	l = "1";
            }else {
            	wp.setIfok(0L);
            	l = "0";
            }
            List<WorkParam> paramList = workParamDao.findDataWorkParamAll(jrollDetectionorder.getFlowid());
        	Map<String,String> map = new HashMap<String,String>();
        	for(WorkParam p : paramList) {
        		if(p.getField_no().equals("ifok")) {
        			map.put(p.getField_no(), l);
        		}else {
        			map.put(p.getField_no(), "");
        		}
        	}
            workFlowdetailPerformDao.updateDataWorkFlowdetailPerform(wp);
            return ResultData.ResultDataSuccessSelf("添加数据成功", map);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }
    
    
    /***
     * 根据对象判断是否合格
     * @return
     */
    public Boolean getIfOkByentity(RollDetectionorder rollDetectionorder) {
    	 try {
             //根据工单获取标准配置，工单配置
             List<RollOrderconfigdetail> detail = rollOrderconfigDao.findDataRollOrderconfigByNo(rollDetectionorder.getIlinkno()).getDetail();
             //通过反射获取类含有field的字段
             Field[] f = RollDetectionorder.class.getDeclaredFields();  //获取新辊尺寸检测工单字段数组
             for (Field ff : f) {
                 ff.setAccessible(true);// 假设不为空。设置可见性，然后返回,这里必须有，不然报错
                 String field_name = ff.getName();
                 if (field_name.indexOf("field") > -1) {  //查看哪些字段含有field字样，这里是只需要含有field字样的字段，用来和工单配置中indexs一一对应
                     Long i = Long.valueOf(field_name.substring(5)); //获取含有field字眼字段后面的数字，好和工单配置中的indexs对应
                     for (RollOrderconfigdetail r : detail) {
                         if (ff.get(rollDetectionorder) != null) {
                             String order_value = ff.get(rollDetectionorder).toString();
                             if (i == r.getIndexs()) {
                                 //标准类型  标准1还是其他2
                                 if (r.getRollStandard().getStandard_type() == 1) {  //标准字段才需要做判断
                                     //字段类型   区间1还是布尔0
                                     if (r.getRollStandard().getField_type().equals("1")) {   //区间
                                         Double max = r.getRollStandard().getField_max(); //上区间
                                         Double min = r.getRollStandard().getField_min();  //下区间
                                         if (max == 100001) {  //表示上限无限
                                             Double x = Double.valueOf(order_value);   //工单填写值
                                             Double y = Double.valueOf(min);   //标准判断值
                                             if (x >= y) {
                                                 return true;
                                             } else {
                                                 return false;
                                             }
                                         } else if (min == 100000) { //表示下限无限
                                             Double x = Double.valueOf(order_value);   //工单填写值
                                             Double y = Double.valueOf(max);   //标准判断值
                                             if (x <= y) {
                                                 return true;
                                             } else {
                                                 return false;
                                             }
                                         } else {  //表示在具体数值的区间，而不是含有上下无穷+∞这样的
                                             Double x = Double.valueOf(order_value);   //工单填写值
                                             Double y = Double.valueOf(max);   //标准判断值上限
                                             Double z = Double.valueOf(min);   //标准判断值下限
                                             if (x <= y) {//填写值小于等于最大值,接着判断填写值是否小于最小值
                                             	if(x >= z) {//填写值大于最小值
                                             		return true;
                                             	}else {//填写值小于最小值，不合格
                                             		return false;
                                             	}
                                             }else {//填写值大于最大值，不合格
                                             	return false;
                                             }
                                         }
                                     } else if (r.getRollStandard().getField_type().equals("0")) {  //布尔
                                         String fieldv = r.getRollStandard().getField_value();
                                         if(fieldv.equals("0")) {
                                        	 return false;
                                         }else if(fieldv.equals("1")) {
                                        	 return true;
                                         }
                                     }
                                 }
                             }
                         }
                     }
                 }
             }
    	 }catch(Exception e) {
    		 e.printStackTrace();
    		 return false;
    	 }
		return true;
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollDetectionorderOne(Long indocno) {
        try {
            rollDetectionorderDao.deleteDataRollDetectionorderOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollDetectionorderMany(String str_id) {
        try {
            String sql = "delete roll_detectionorder where indocno in(" + str_id + ")";
            rollDetectionorderDao.deleteDataRollDetectionorderMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId  用户id
     * @param sname   用户姓名
     * @param sysUser 对象实体
     */
    public ResultData updateDataRollDetectionorder(String data, Long userId, String sname) {
        try {
            JRollDetectionorder jrollDetectionorder = JSON.parseObject(data, JRollDetectionorder.class);
            RollDetectionorder rollDetectionorder = jrollDetectionorder.getRollDetectionorder();
            CodiUtil.editRecord(userId, sname, rollDetectionorder);
            rollDetectionorderDao.updateDataRollDetectionorder(rollDetectionorder);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollDetectionorderByPage(String data) {
        try {
            JRollDetectionorder jrollDetectionorder = JSON.parseObject(data, JRollDetectionorder.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollDetectionorder.getPageIndex();
            Integer pageSize = jrollDetectionorder.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollDetectionorder.getCondition()) {
                jsonObject = JSON.parseObject(jrollDetectionorder.getCondition().toString());
            }

            List<RollDetectionorder> list = rollDetectionorderDao.findDataRollDetectionorderByPage((pageIndex - 1) * pageSize, pageSize);
            Integer count = rollDetectionorderDao.findDataRollDetectionorderByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollDetectionorderByIndocno(String data) {
        try {
            JRollDetectionorder jrollDetectionorder = JSON.parseObject(data, JRollDetectionorder.class);
            Long indocno = jrollDetectionorder.getIndocno();

            RollDetectionorder rollDetectionorder = rollDetectionorderDao.findDataRollDetectionorderByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollDetectionorder);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }
    
    /**
     * 根据编码查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollDetectionorderByNo(String data) {
        try {
        	RollDetectionorder jrollDetectionorder = JSON.parseObject(data, RollDetectionorder.class);
            String roll_no = jrollDetectionorder.getRoll_no();

            RollDetectionorder rollDetectionorder = rollDetectionorderDao.findDataRollDetectionorderByNo(roll_no);
            return ResultData.ResultDataSuccess(rollDetectionorder);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }
    
    /**
     * 根据编码查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollDetectionorderByTime(String data) {
        try {
        	RollDetectionorder jrollDetectionorder = JSON.parseObject(data, RollDetectionorder.class);
            String roll_no = jrollDetectionorder.getRoll_no();
            List<RollDetectionorder> list = rollDetectionorderDao.findDataRollDetectionorderBytime(roll_no);
            return ResultData.ResultDataSuccessSelf("查询成功", list.get(0));
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollDetectionorder> findDataRollDetectionorder() {
        List<RollDetectionorder> list = rollDetectionorderDao.findDataRollDetectionorder();
        return list;
    }

}
