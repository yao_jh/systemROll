package com.my.business.modular.rolldetectionorder.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rolldetectionorder.entity.JRollDetectionorder;
import com.my.business.modular.rolldetectionorder.entity.RollDetectionorder;
import com.my.business.modular.rolldetectionorder.service.RollDetectionorderService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 新辊超声硬度检测工单控制器层
 *
 * @author 生成器生成
 * @date 2020-08-25 09:31:34
 */
@RestController
@RequestMapping("/rollDetectionorder")
public class RollDetectionorderController {

    @Autowired
    private RollDetectionorderService rollDetectionorderService;

    /**
     * 添加记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollDetectionorder(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollDetectionorderService.insertDataRollDetectionorder(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollDetectionorderOne(@RequestBody String data) {
        try {
            JRollDetectionorder jrollDetectionorder = JSON.parseObject(data, JRollDetectionorder.class);
            return rollDetectionorderService.deleteDataRollDetectionorderOne(jrollDetectionorder.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollDetectionorder jrollDetectionorder = JSON.parseObject(data, JRollDetectionorder.class);
            return rollDetectionorderService.deleteDataRollDetectionorderMany(jrollDetectionorder.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollDetectionorder(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollDetectionorderService.updateDataRollDetectionorder(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDetectionorderByPage(@RequestBody String data) {
        return rollDetectionorderService.findDataRollDetectionorderByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDetectionorderByIndocno(@RequestBody String data) {
        return rollDetectionorderService.findDataRollDetectionorderByIndocno(data);
    }
    
    /**
     * 根据辊号查询最新记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByNew"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findByNo(@RequestBody String data) {
        return rollDetectionorderService.findDataRollDetectionorderByTime(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollDetectionorder> findDataRollDetectionorder() {
        return rollDetectionorderService.findDataRollDetectionorder();
    }

}
