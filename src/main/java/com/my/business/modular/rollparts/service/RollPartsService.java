package com.my.business.modular.rollparts.service;

import com.my.business.modular.rollparts.entity.RollParts;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 设备备件管理接口服务类
 *
 * @author 生成器生成
 * @date 2020-10-13 16:32:04
 */
public interface RollPartsService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollParts(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollPartsOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollPartsMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollParts(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPartsByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPartsByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollParts> findDataRollParts();
}
