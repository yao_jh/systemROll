package com.my.business.modular.rollparts.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollparts.dao.RollPartsDao;
import com.my.business.modular.rollparts.entity.JRollParts;
import com.my.business.modular.rollparts.entity.RollParts;
import com.my.business.modular.rollparts.service.RollPartsService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 设备备件管理接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-10-13 16:32:04
 */
@Service
public class RollPartsServiceImpl implements RollPartsService {

    @Autowired
    private RollPartsDao rollPartsDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollParts(String data, Long userId, String sname) {
        try {
            JRollParts jrollParts = JSON.parseObject(data, JRollParts.class);
            RollParts rollParts = jrollParts.getRollParts();
            CodiUtil.newRecord(userId, sname, rollParts);
            rollPartsDao.insertDataRollParts(rollParts);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollPartsOne(Long indocno) {
        try {
            rollPartsDao.deleteDataRollPartsOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollPartsMany(String str_id) {
        try {
            String sql = "delete roll_parts where indocno in(" + str_id + ")";
            rollPartsDao.deleteDataRollPartsMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollParts(String data, Long userId, String sname) {
        try {
            JRollParts jrollParts = JSON.parseObject(data, JRollParts.class);
            RollParts rollParts = jrollParts.getRollParts();
            CodiUtil.editRecord(userId, sname, rollParts);
            rollPartsDao.updateDataRollParts(rollParts);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPartsByPage(String data) {
        try {
            JRollParts jrollParts = JSON.parseObject(data, JRollParts.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollParts.getPageIndex();
            Integer pageSize = jrollParts.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollParts.getCondition()) {
                jsonObject = JSON.parseObject(jrollParts.getCondition().toString());
            }

            Long eq_type_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("eq_type_id"))) {
                eq_type_id = Long.valueOf(jsonObject.get("eq_type_id").toString());
            }

            String eq_name = null;
            if (!StringUtils.isEmpty(jsonObject.get("eq_name"))) {
                eq_name = jsonObject.get("eq_name").toString();
            }

            String sized = null;
            if (!StringUtils.isEmpty(jsonObject.get("sized"))) {
                sized = jsonObject.get("sized").toString();
            }

            List<RollParts> list = rollPartsDao.findDataRollPartsByPage((pageIndex - 1) * pageSize, pageSize, eq_type_id, eq_name, sized);
            Integer count = rollPartsDao.findDataRollPartsByPageSize(eq_type_id, eq_name, sized);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPartsByIndocno(String data) {
        try {
            JRollParts jrollParts = JSON.parseObject(data, JRollParts.class);
            Long indocno = jrollParts.getIndocno();

            RollParts rollParts = rollPartsDao.findDataRollPartsByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollParts);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollParts> findDataRollParts() {
        List<RollParts> list = rollPartsDao.findDataRollParts();
        return list;
    }

}
