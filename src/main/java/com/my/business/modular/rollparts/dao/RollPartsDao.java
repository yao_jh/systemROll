package com.my.business.modular.rollparts.dao;

import com.my.business.modular.rollparts.entity.RollParts;
import com.my.business.modular.rollpartsdetail.entity.RollPartsDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 设备备件管理dao接口
 *
 * @author 生成器生成
 * @date 2020-10-13 16:32:04
 */
@Mapper
public interface RollPartsDao {

    /**
     * 添加记录
     *
     * @param rollParts 对象实体
     */
    void insertDataRollParts(RollParts rollParts);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollPartsOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollPartsMany(String value);

    /**
     * 修改记录
     *
     * @param rollParts 对象实体
     */
    void updateDataRollParts(RollParts rollParts);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollParts> findDataRollPartsByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("eq_type_id") Long eq_type_id, @Param("eq_name") String eq_name, @Param("sized") String sized);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollPartsByPageSize(@Param("eq_type_id") Long eq_type_id, @Param("eq_name") String eq_name, @Param("sized") String sized);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollParts findDataRollPartsByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollParts> findDataRollParts();

    /**
     * 入库状态，数量增加
     *
     * @param indocno      子表外键，也就是主表主键
     * @param num          入库数量
     * @param parts_new    新备件数量
     * @param parts_old    旧备件数量
     * @param parts_bad    损坏件数量
     * @param parts_scrap  报废件数量
     * @param parts_repair 送修件数量
     * @param parts_use    在机件数量
     */
    void updateDataRollPartsA(@Param("indocno") Long indocno, @Param("num") Long num, @Param("parts_new") Long parts_new, @Param("parts_old") Long parts_old, @Param("parts_bad") Long parts_bad, @Param("parts_scrap") Long parts_scrap, @Param("parts_repair") Long parts_repair, @Param("parts_use") Long parts_use);

    /**
     * 出库状态，数量减少
     *
     * @param indocno      子表外键，也就是主表主键
     * @param num          入库数量
     * @param parts_new    新备件数量
     * @param parts_old    旧备件数量
     * @param parts_bad    损坏件数量
     * @param parts_scrap  报废件数量
     * @param parts_repair 送修件数量
     * @param parts_use    在机件数量
     */
    void updateDataRollPartsM(@Param("indocno") Long indocno, @Param("num") Long num, @Param("parts_new") Long parts_new, @Param("parts_old") Long parts_old, @Param("parts_bad") Long parts_bad, @Param("parts_scrap") Long parts_scrap, @Param("parts_repair") Long parts_repair, @Param("parts_use") Long parts_use);
}
