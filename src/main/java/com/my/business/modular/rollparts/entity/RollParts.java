package com.my.business.modular.rollparts.entity;

import com.my.business.modular.rollpartsdetail.entity.RollPartsDetail;
import com.my.business.sys.common.entity.BaseEntity;

import java.util.List;

/**
 * 设备备件管理实体类
 *
 * @author 生成器生成
 * @date 2020-10-13 16:32:03
 */
public class RollParts extends BaseEntity {

    private Long indocno;  //主键
    private Long production_line_id;  //产线id
    private String production_line;  //产线
    private String eq_name;  //备件名称
    private Long eq_type_id;  //设备类型id
    private String eq_type;  //设备类型
    private String sized;  //规格型号
    private String picno;  //图号
    private Long stock_count;  //库存数量
    private Long parts_new;  //新备件数量
    private Long parts_old;  //旧备件（修复件）数量
    private Long parts_bad;  //损坏件数量
    private Long parts_scrap;  //报废件数量
    private Long parts_repair;  //送修件数量
    private Long parts_use;  //在机件数量
    private Long if_import;  //是否为重要设备
    private String snote;  //备注

    private List<RollPartsDetail> detail; //子表集合

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getProduction_line_id() {
        return this.production_line_id;
    }

    public void setProduction_line_id(Long production_line_id) {
        this.production_line_id = production_line_id;
    }

    public String getProduction_line() {
        return this.production_line;
    }

    public void setProduction_line(String production_line) {
        this.production_line = production_line;
    }

    public String getEq_name() {
        return this.eq_name;
    }

    public void setEq_name(String eq_name) {
        this.eq_name = eq_name;
    }

    public Long getEq_type_id() {
        return this.eq_type_id;
    }

    public void setEq_type_id(Long eq_type_id) {
        this.eq_type_id = eq_type_id;
    }

    public String getEq_type() {
        return this.eq_type;
    }

    public void setEq_type(String eq_type) {
        this.eq_type = eq_type;
    }

    public String getSized() {
        return this.sized;
    }

    public void setSized(String sized) {
        this.sized = sized;
    }

    public String getPicno() {
        return this.picno;
    }

    public void setPicno(String picno) {
        this.picno = picno;
    }

    public Long getStock_count() {
        return this.stock_count;
    }

    public void setStock_count(Long stock_count) {
        this.stock_count = stock_count;
    }

    public Long getParts_new() {
        return this.parts_new;
    }

    public void setParts_new(Long parts_new) {
        this.parts_new = parts_new;
    }

    public Long getParts_old() {
        return this.parts_old;
    }

    public void setParts_old(Long parts_old) {
        this.parts_old = parts_old;
    }

    public Long getParts_bad() {
        return this.parts_bad;
    }

    public void setParts_bad(Long parts_bad) {
        this.parts_bad = parts_bad;
    }

    public Long getParts_scrap() {
        return this.parts_scrap;
    }

    public void setParts_scrap(Long parts_scrap) {
        this.parts_scrap = parts_scrap;
    }

    public Long getParts_repair() {
        return this.parts_repair;
    }

    public void setParts_repair(Long parts_repair) {
        this.parts_repair = parts_repair;
    }

    public Long getParts_use() {
        return this.parts_use;
    }

    public void setParts_use(Long parts_use) {
        this.parts_use = parts_use;
    }

    public Long getIf_import() {
        return this.if_import;
    }

    public void setIf_import(Long if_import) {
        this.if_import = if_import;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public List<RollPartsDetail> getDetail() {
        return detail;
    }

    public void setDetail(List<RollPartsDetail> detail) {
        this.detail = detail;
    }
}