package com.my.business.modular.cycleremind.dao;

import com.my.business.modular.cycleremind.entity.CycleRemind;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轴承座周期管理dao接口
 *
 * @author 生成器生成
 * @date 2020-09-23 11:32:56
 */
@Mapper
public interface CycleRemindDao {

    /**
     * 添加记录
     *
     * @param cycleRemind 对象实体
     */
    void insertDataCycleRemind(CycleRemind cycleRemind);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataCycleRemindOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataCycleRemindMany(String value);

    /**
     * 修改记录
     *
     * @param cycleRemind 对象实体
     */
    void updateDataCycleRemind(CycleRemind cycleRemind);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<CycleRemind> findDataCycleRemindByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize,@Param("sname") String sname,@Param("itype") Long itype,@Param("stype") String stype,@Param("sno") String sno);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataCycleRemindByPageSize(@Param("sname") String sname,@Param("itype") Long itype,@Param("stype") String stype,@Param("sno") String sno);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    CycleRemind findDataCycleRemindByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<CycleRemind> findDataCycleRemind();

    /**
     * 查找要推送的消息
     * @return 实体类
     */
    List<CycleRemind> findpush();

    /**
     * 查找要推送的消息
     * @return 实体类
     */
    List<CycleRemind> findchange();

    /**
     * 更新日期
     * @param indocno
     */
    void changedays(@Param("indocno") Long indocno,@Param("days") String days);

}
