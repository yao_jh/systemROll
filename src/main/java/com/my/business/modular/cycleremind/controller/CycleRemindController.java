package com.my.business.modular.cycleremind.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.cycleremind.entity.CycleRemind;
import com.my.business.modular.cycleremind.entity.JCycleRemind;
import com.my.business.modular.cycleremind.service.CycleRemindService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 轴承座周期管理控制器层
 *
 * @author 生成器生成
 * @date 2020-09-23 11:32:56
 */
@RestController
@RequestMapping("/cycleRemind")
public class CycleRemindController {

    @Autowired
    private CycleRemindService cycleRemindService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataCycleRemind(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return cycleRemindService.insertDataCycleRemind(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataCycleRemindOne(@RequestBody String data) {
        try {
            JCycleRemind jcycleRemind = JSON.parseObject(data, JCycleRemind.class);
            return cycleRemindService.deleteDataCycleRemindOne(jcycleRemind.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JCycleRemind jcycleRemind = JSON.parseObject(data, JCycleRemind.class);
            return cycleRemindService.deleteDataCycleRemindMany(jcycleRemind.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataCycleRemind(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return cycleRemindService.updateDataCycleRemind(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataCycleRemindByPage(@RequestBody String data) {
        return cycleRemindService.findDataCycleRemindByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataCycleRemindByIndocno(@RequestBody String data) {
        return cycleRemindService.findDataCycleRemindByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<CycleRemind> findDataCycleRemind() {
        return cycleRemindService.findDataCycleRemind();
    }

    /**
     * 数据字典通用查询，下拉模式
     *
     */
    @CrossOrigin
    @RequestMapping(value = {"/findMap"}, method = RequestMethod.POST)
    public ResultData findDataDic() {
        return cycleRemindService.findMap();
    }
}
