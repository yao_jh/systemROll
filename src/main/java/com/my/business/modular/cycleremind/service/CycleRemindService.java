package com.my.business.modular.cycleremind.service;

import com.my.business.modular.cycleremind.entity.CycleRemind;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 轴承座周期管理接口服务类
 *
 * @author 生成器生成
 * @date 2020-09-23 11:32:56
 */
public interface CycleRemindService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataCycleRemind(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataCycleRemindOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataCycleRemindMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataCycleRemind(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataCycleRemindByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataCycleRemindByIndocno(String data);

    /**
     * 查看记录
     */
    List<CycleRemind> findDataCycleRemind();

    /**
     * 挂接工作流用
     */
    ResultData findMap();
}
