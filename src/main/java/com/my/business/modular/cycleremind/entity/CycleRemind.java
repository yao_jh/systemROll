package com.my.business.modular.cycleremind.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轴承座周期管理实体类
 *
 * @author 生成器生成
 * @date 2020-09-23 11:32:56
 */
public class CycleRemind extends BaseEntity {

    private Long indocno;  //主键
    private String sname;  //部件名称
    private Long itype;  //部件类型id
    private String stype;  //部件类型
    private String sno;  //部件编号
    private Long production_line_id;  //产线id
    private String production_line;  //产线
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private Long frame_noid;  //机架号id
    private String frame_no;  //机架号
    private Long install_location_id;  //安装位置id
    private String install_location;  //安装位置
    private Long up_location_id;  //上机位置id
    private String up_location;  //上机位置
    private Long days;  //提前提醒天数
    private String news;  //提醒内容
    private Long cycle;  //周期
    private Long unit;  //周期单位
    private String flow_no;  //挂接的工作流id
    private String pre_time;  //开始执行时间
    private String next_time;  //下一次执行日期
    private String snote;  //备注

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getSname() {
        return this.sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Long getItype() {
        return this.itype;
    }

    public void setItype(Long itype) {
        this.itype = itype;
    }

    public String getStype() {
        return this.stype;
    }

    public void setStype(String stype) {
        this.stype = stype;
    }

    public String getSno() {
        return this.sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public Long getDays() {
        return this.days;
    }

    public void setDays(Long days) {
        this.days = days;
    }

    public Long getCycle() {
        return this.cycle;
    }

    public void setCycle(Long cycle) {
        this.cycle = cycle;
    }

    public Long getUnit() {
        return this.unit;
    }

    public void setUnit(Long unit) {
        this.unit = unit;
    }

    public String getPre_time() {
        return this.pre_time;
    }

    public void setPre_time(String pre_time) {
        this.pre_time = pre_time;
    }

    public String getNext_time() {
        return this.next_time;
    }

    public void setNext_time(String next_time) {
        this.next_time = next_time;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public String getFlow_no() {
        return flow_no;
    }

    public void setFlow_no(String flow_no) {
        this.flow_no = flow_no;
    }

    public String getNews() {
        return news;
    }

    public void setNews(String news) {
        this.news = news;
    }

    public Long getProduction_line_id() {
        return production_line_id;
    }

    public void setProduction_line_id(Long production_line_id) {
        this.production_line_id = production_line_id;
    }

    public String getProduction_line() {
        return production_line;
    }

    public void setProduction_line(String production_line) {
        this.production_line = production_line;
    }

    public Long getRoll_typeid() {
        return roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public Long getFrame_noid() {
        return frame_noid;
    }

    public void setFrame_noid(Long frame_noid) {
        this.frame_noid = frame_noid;
    }

    public String getFrame_no() {
        return frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public Long getInstall_location_id() {
        return install_location_id;
    }

    public void setInstall_location_id(Long install_location_id) {
        this.install_location_id = install_location_id;
    }

    public String getInstall_location() {
        return install_location;
    }

    public void setInstall_location(String install_location) {
        this.install_location = install_location;
    }

    public Long getUp_location_id() {
        return up_location_id;
    }

    public void setUp_location_id(Long up_location_id) {
        this.up_location_id = up_location_id;
    }

    public String getUp_location() {
        return up_location;
    }

    public void setUp_location(String up_location) {
        this.up_location = up_location;
    }
}