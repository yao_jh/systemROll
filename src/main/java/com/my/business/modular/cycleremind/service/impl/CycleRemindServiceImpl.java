package com.my.business.modular.cycleremind.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.cycleremind.dao.CycleRemindDao;
import com.my.business.modular.cycleremind.entity.CycleRemind;
import com.my.business.modular.cycleremind.entity.JCycleRemind;
import com.my.business.modular.cycleremind.service.CycleRemindService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.sys.dict.entity.DpdEntity;
import com.my.business.util.CodiUtil;
import com.my.business.workflow.workflow.dao.WorkFlowDao;
import com.my.business.workflow.workflow.entity.WorkFlow;
import com.my.business.workflow.workflow.service.WorkFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 轴承座周期管理接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-23 11:32:56
 */
@Service
public class CycleRemindServiceImpl implements CycleRemindService {

    @Autowired
    private CycleRemindDao cycleRemindDao;

    @Autowired
    private WorkFlowDao workFlowDao;
    @Autowired
    private WorkFlowService workFlowService;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataCycleRemind(String data, Long userId, String sname) {
        try {
            JCycleRemind jcycleRemind = JSON.parseObject(data, JCycleRemind.class);
            CycleRemind cycleRemind = jcycleRemind.getCycleRemind();
            cycleRemind.setNext_time(cycleRemind.getPre_time());
            CodiUtil.newRecord(userId, sname, cycleRemind);
            cycleRemindDao.insertDataCycleRemind(cycleRemind);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataCycleRemindOne(Long indocno) {
        try {
            cycleRemindDao.deleteDataCycleRemindOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataCycleRemindMany(String str_id) {
        try {
            String sql = "delete cycle_remind where indocno in(" + str_id + ")";
            cycleRemindDao.deleteDataCycleRemindMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataCycleRemind(String data, Long userId, String sname) {
        try {
            JCycleRemind jcycleRemind = JSON.parseObject(data, JCycleRemind.class);
            CycleRemind cycleRemind = jcycleRemind.getCycleRemind();
            cycleRemind.setNext_time(cycleRemind.getPre_time());
            CodiUtil.editRecord(userId, sname, cycleRemind);
            cycleRemindDao.updateDataCycleRemind(cycleRemind);
            //修改后立即看看有没有要当日推送的消息
            push();
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataCycleRemindByPage(String data) {
        try {
            JCycleRemind jcycleRemind = JSON.parseObject(data, JCycleRemind.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jcycleRemind.getPageIndex();
            Integer pageSize = jcycleRemind.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jcycleRemind.getCondition()) {
                jsonObject = JSON.parseObject(jcycleRemind.getCondition().toString());
            }

            String sname = null;
            if (!StringUtils.isEmpty(jsonObject.get("sname"))) {
                sname = jsonObject.get("sname").toString();
            }

            Long itype = null;
            if (!StringUtils.isEmpty(jsonObject.get("itype"))) {
                itype = Long.valueOf(jsonObject.get("itype").toString());
            }

            String sno = null;
            if (!StringUtils.isEmpty(jsonObject.get("sno"))) {
                sno = jsonObject.get("sno").toString();
            }

            String stype = null;
            if (!StringUtils.isEmpty(jsonObject.get("stype"))) {
                stype = jsonObject.get("stype").toString();
            }

            List<CycleRemind> list = cycleRemindDao.findDataCycleRemindByPage((pageIndex - 1) * pageSize, pageSize, sname, itype, stype, sno);
            Integer count = cycleRemindDao.findDataCycleRemindByPageSize(sname, itype, stype, sno);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataCycleRemindByIndocno(String data) {
        try {
            JCycleRemind jcycleRemind = JSON.parseObject(data, JCycleRemind.class);
            Long indocno = jcycleRemind.getIndocno();

            CycleRemind cycleRemind = cycleRemindDao.findDataCycleRemindByIndocno(indocno);
            return ResultData.ResultDataSuccess(cycleRemind);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<CycleRemind> findDataCycleRemind() {
        List<CycleRemind> list = cycleRemindDao.findDataCycleRemind();
        return list;
    }

    @Transactional(rollbackFor = Exception.class)
    @Scheduled(cron = "${timecycle}")
    //每天触发一次
    public void push() throws ParseException {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        //查找所有需要提醒的内容
        List<CycleRemind> list = cycleRemindDao.findpush();
        if (list.size() > 0) {
            for (CycleRemind entity : list) {
                try {
                    Map<String, String> map = new HashMap<>();
                    map.put("production_line_id", String.valueOf(entity.getProduction_line_id()));
                    map.put("production_line", entity.getProduction_line());
                    map.put("roll_typeid", String.valueOf(entity.getRoll_typeid()));
                    map.put("roll_type", entity.getRoll_type());
                    map.put("itype", String.valueOf(entity.getItype()));
                    map.put("stype", entity.getStype());
                    map.put("frame_noid", String.valueOf(entity.getFrame_noid()));
                    map.put("frame_no", entity.getFrame_no());
                    map.put("install_location_id", String.valueOf(entity.getInstall_location_id()));
                    map.put("install_location", String.valueOf(entity.getInstall_location()));
                    map.put("up_location_id", String.valueOf(entity.getUp_location_id()));
                    map.put("up_location", String.valueOf(entity.getUp_location()));
                    workFlowService.startWorkFlow(map, entity.getFlow_no(), null, "周期自动提醒");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        //查找所有周期到期的数据
        List<CycleRemind> listchange = cycleRemindDao.findchange();
        String days = null;
        if (listchange.size() > 0) {
            //根据周期单位和周期天数，变更下一次执行时间，原开始时间变为当日
            for (CycleRemind es : listchange) {
                if (es.getUnit() == 3L) {
                    //日
                    c.setTime(new Date());
                    c.add(Calendar.DATE, es.getCycle().intValue());
                    days = df.format(c.getTime());
                    cycleRemindDao.changedays(es.getIndocno(), days);
                } else if (es.getUnit() == 2L) {
                    //月
                    c.setTime(new Date());
                    c.add(Calendar.MONTH, es.getCycle().intValue());
                    days = df.format(c.getTime());
                    cycleRemindDao.changedays(es.getIndocno(), days);
                } else if (es.getUnit() == 1L) {
                    //年
                    c.setTime(new Date());
                    c.add(Calendar.YEAR, es.getCycle().intValue());
                    days = df.format(c.getTime());
                    cycleRemindDao.changedays(es.getIndocno(), days);
                }
            }
        }
    }

    /**
     * 工作流挂接，下拉模式
     */
    public ResultData findMap() {
        try {
            List<WorkFlow> list = workFlowDao.findDataWorkFlow();
            List<DpdEntity> listdpd = new ArrayList<>();
            for (WorkFlow entity : list) {
                DpdEntity dpdEntity = new DpdEntity();
                dpdEntity.setKey(entity.getFlow_no());
                dpdEntity.setValue(entity.getFlow_name());
                listdpd.add(dpdEntity);
            }
            return ResultData.ResultDataSuccessSelf("获取成功", listdpd);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("获取失败" + e.getMessage(), null);
        }
    }
}
