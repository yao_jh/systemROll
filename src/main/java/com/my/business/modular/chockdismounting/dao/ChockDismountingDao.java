package com.my.business.modular.chockdismounting.dao;

import com.my.business.modular.chockdismounting.entity.ChockDismounting;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轴承座拆装管理dao接口
 *
 * @author 生成器生成
 * @date 2020-09-23 09:17:09
 */
@Mapper
public interface ChockDismountingDao {

    /**
     * 添加记录
     *
     * @param chockDismounting 对象实体
     */
	void insertDataChockDismounting(ChockDismounting chockDismounting);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
	void deleteDataChockDismountingOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
	void deleteDataChockDismountingMany(String value);

    /**
     * 修改记录
     *
     * @param chockDismounting 对象实体
     */
	void updateDataChockDismounting(ChockDismounting chockDismounting);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
	List<ChockDismounting> findDataChockDismountingByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("roll_no") String roll_no, @Param("roll_typeid") Long roll_typeid, @Param("frame_noid") Long frame_noid, @Param("frame_no") String frame_no, @Param("production_line_id") Long production_line_id);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
	Integer findDataChockDismountingByPageSize(@Param("roll_no") String roll_no, @Param("roll_typeid") Long roll_typeid, @Param("frame_noid") Long frame_noid, @Param("frame_no") String frame_no, @Param("production_line_id") Long production_line_id);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
	ChockDismounting findDataChockDismountingByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
	List<ChockDismounting> findDataChockDismounting();

    /**
     * 根据辊号查询该辊号的最新一条数据
     * @param roll_no 辊号
     * @return 数据
     */
    ChockDismounting findDataNewest(@Param("roll_no") String roll_no);
}
