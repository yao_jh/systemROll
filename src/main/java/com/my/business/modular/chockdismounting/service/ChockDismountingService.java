package com.my.business.modular.chockdismounting.service;

import com.my.business.modular.chockdismounting.entity.ChockDismounting;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 轴承座拆装管理接口服务类
 *
 * @author 生成器生成
 * @date 2020-09-23 09:17:09
 */
public interface ChockDismountingService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataChockDismounting(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataChockDismountingOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataChockDismountingMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataChockDismounting(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataChockDismountingByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataChockDismountingByIndocno(String data);

    /**
     * 查看记录
     */
    List<ChockDismounting> findDataChockDismounting();
}
