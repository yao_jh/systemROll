package com.my.business.modular.chockdismounting.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.chockdismounting.dao.ChockDismountingDao;
import com.my.business.modular.chockdismounting.entity.ChockDismounting;
import com.my.business.modular.chockdismounting.entity.JChockDismounting;
import com.my.business.modular.chockdismounting.service.ChockDismountingService;
import com.my.business.modular.rollinformation.dao.RollInformationDao;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 轴承座拆装管理接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-09-23 09:17:09
 */
@Service
public class ChockDismountingServiceImpl implements ChockDismountingService {

    @Autowired
    private ChockDismountingDao chockDismountingDao;
    @Autowired
    private RollInformationDao rollInformationDao;
    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataChockDismounting(String data, Long userId, String sname) {
        try {
            JChockDismounting jchockDismounting = JSON.parseObject(data, JChockDismounting.class);
            ChockDismounting chockDismounting = jchockDismounting.getChockDismounting();
            CodiUtil.newRecord(userId, sname, chockDismounting);
            chockDismountingDao.insertDataChockDismounting(chockDismounting);

            String os_no = null;
            String ds_no = null;
            String roll_no = chockDismounting.getRoll_no();
            String bc_no = null;
            if (!StringUtils.isEmpty(chockDismounting.getOs_no())){
                os_no = chockDismounting.getOs_no();
            }
            if (!StringUtils.isEmpty(chockDismounting.getDs_no())){
                ds_no = chockDismounting.getDs_no();
            }
            bc_no = os_no + "-" + ds_no;
            rollInformationDao.updateChockNo(os_no,ds_no,bc_no,roll_no,null,0L);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataChockDismountingOne(Long indocno) {
        try {
            ChockDismounting chockDismounting = chockDismountingDao.findDataChockDismountingByIndocno(indocno);
            String roll_no = chockDismounting.getRoll_no();
            rollInformationDao.updateChockNo(null,null, null,roll_no,null,0L);
            chockDismountingDao.deleteDataChockDismountingOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataChockDismountingMany(String str_id) {
        try {
            String sql = "delete chock_dismounting where indocno in(" + str_id + ")";
            chockDismountingDao.deleteDataChockDismountingMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataChockDismounting(String data, Long userId, String sname) {
        try {
            JChockDismounting jchockDismounting = JSON.parseObject(data, JChockDismounting.class);
            ChockDismounting chockDismounting = jchockDismounting.getChockDismounting();
            CodiUtil.editRecord(userId, sname, chockDismounting);
            chockDismountingDao.updateDataChockDismounting(chockDismounting);

            String os_no = null;
            String ds_no = null;
            String roll_no = chockDismounting.getRoll_no();
            String bc_no = null;
            if (!StringUtils.isEmpty(chockDismounting.getOs_no())){
                os_no = chockDismounting.getOs_no();
            }
            if (!StringUtils.isEmpty(chockDismounting.getDs_no())){
                ds_no = chockDismounting.getDs_no();
            }
            bc_no = os_no + "-" + ds_no;
            rollInformationDao.updateChockNo(os_no,ds_no,bc_no,roll_no,null,0L);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

	/**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataChockDismountingByPage(String data) {
        try {
            JChockDismounting jchockDismounting = JSON.parseObject(data, JChockDismounting.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jchockDismounting.getPageIndex();
            Integer pageSize = jchockDismounting.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jchockDismounting.getCondition()) {
                jsonObject = JSON.parseObject(jchockDismounting.getCondition().toString());
            }

            Long roll_typeid = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_typeid"))) {
                roll_typeid = Long.valueOf(jsonObject.get("roll_typeid").toString());
            }

            String roll_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("roll_no"))) {
                roll_no = jsonObject.get("roll_no").toString();
            }

            Long frame_noid = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_noid"))) {
                frame_noid = Long.valueOf(jsonObject.get("frame_noid").toString());
            }

            String frame_no = null;
            if (!StringUtils.isEmpty(jsonObject.get("frame_no"))) {
                frame_no = jsonObject.get("frame_no").toString();
            }

            Long production_line_id = null;
            if (!StringUtils.isEmpty(jsonObject.get("production_line_id"))) {
                production_line_id = Long.valueOf(jsonObject.get("production_line_id").toString());
            }

            List<ChockDismounting> list = chockDismountingDao.findDataChockDismountingByPage((pageIndex - 1) * pageSize, pageSize, roll_no, roll_typeid, frame_noid, frame_no, production_line_id);
            Integer count = chockDismountingDao.findDataChockDismountingByPageSize(roll_no, roll_typeid, frame_noid, frame_no, production_line_id);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataChockDismountingByIndocno(String data) {
        try {
            JChockDismounting jchockDismounting = JSON.parseObject(data, JChockDismounting.class);
            Long indocno = jchockDismounting.getIndocno();

            ChockDismounting chockDismounting = chockDismountingDao.findDataChockDismountingByIndocno(indocno);
            return ResultData.ResultDataSuccess(chockDismounting);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<ChockDismounting> findDataChockDismounting() {
        List<ChockDismounting> list = chockDismountingDao.findDataChockDismounting();
        return list;
    }

}
