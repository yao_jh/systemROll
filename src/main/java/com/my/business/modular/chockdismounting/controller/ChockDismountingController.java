package com.my.business.modular.chockdismounting.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.chockdismounting.entity.ChockDismounting;
import com.my.business.modular.chockdismounting.entity.JChockDismounting;
import com.my.business.modular.chockdismounting.service.ChockDismountingService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 轴承座拆装管理控制器层
 *
 * @author 生成器生成
 * @date 2020-09-23 09:17:09
 */
@RestController
@RequestMapping("/chockDismounting")
public class ChockDismountingController {

    @Autowired
    private ChockDismountingService chockDismountingService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataChockDismounting(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return chockDismountingService.insertDataChockDismounting(data, userId, CodiUtil.returnLm(sname));
    }

	/**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataChockDismountingOne(@RequestBody String data) {
        try {
            JChockDismounting jchockDismounting = JSON.parseObject(data, JChockDismounting.class);
            return chockDismountingService.deleteDataChockDismountingOne(jchockDismounting.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

	/**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JChockDismounting jchockDismounting = JSON.parseObject(data, JChockDismounting.class);
            return chockDismountingService.deleteDataChockDismountingMany(jchockDismounting.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataChockDismounting(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return chockDismountingService.updateDataChockDismounting(data, userId, CodiUtil.returnLm(sname));
    }

	/**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataChockDismountingByPage(@RequestBody String data) {
        return chockDismountingService.findDataChockDismountingByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataChockDismountingByIndocno(@RequestBody String data) {
        return chockDismountingService.findDataChockDismountingByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<ChockDismounting> findDataChockDismounting() {
        return chockDismountingService.findDataChockDismounting();
    }

}
