package com.my.business.modular.chockchangeposition1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.modular.chockchangeposition1.entity.ChockChangePosition1;
import com.my.business.modular.chockchangeposition1.entity.JChockChangePosition1;
import com.my.business.modular.chockchangeposition1.service.ChockChangePosition1Service;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import com.my.business.sys.common.entity.ResultData;
import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.my.business.util.CodiUtil;
import org.springframework.web.bind.annotation.RequestHeader;


/**
* 轴承座换区工单——精轧工作辊2250控制器层
* @author  生成器生成
* @date 2020-10-26 20:53:32
*/
@RestController
@RequestMapping("/chockChangePosition1")
public class ChockChangePosition1Controller {

	@Autowired
	private ChockChangePosition1Service chockChangePosition1Service;
	
	/**
	 * 添加记录
	 * @param data userId 用户id
     * @param data sname 用户姓名
	 * */
	@CrossOrigin
	@RequestMapping(value={"/insert"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData insertDataChockChangePosition1(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return chockChangePosition1Service.insertDataChockChangePosition1(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
	 * 根据主键删除对象
	 * @param data json字符串
	 * */
	@CrossOrigin
	@RequestMapping(value={"/deleteOne"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData deleteDataChockChangePosition1One(@RequestBody String data){
		try{
    		JChockChangePosition1 jchockChangePosition1 = JSON.parseObject(data,JChockChangePosition1.class);
    		return chockChangePosition1Service.deleteDataChockChangePosition1One(jchockChangePosition1.getIndocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
    	try{
    		JChockChangePosition1 jchockChangePosition1 = JSON.parseObject(data,JChockChangePosition1.class);
    		return chockChangePosition1Service.deleteDataChockChangePosition1Many(jchockChangePosition1.getStr_indocno());
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null); 
    	}
    }
	
	/**
     * 修改记录
     * @param data json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
	@RequestMapping(value={"/update"}, method=RequestMethod.POST)
	@ResponseBody
	public ResultData updateDataChockChangePosition1(@RequestBody String data,@RequestHeader(value="loginToken") String loginToken){
		JCommon common = JSON.parseObject(loginToken,JCommon.class);
    	String sname = common.getSname();
    	Long userId = common.getUserId();
		return chockChangePosition1Service.updateDataChockChangePosition1(data,userId,CodiUtil.returnLm(sname));
	};
	
	/**
     * 分页查看记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataChockChangePosition1ByPage(@RequestBody String data) {
        return chockChangePosition1Service.findDataChockChangePosition1ByPage(data);
    }
    
    /**
     * 根据主键查询单条记录
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataChockChangePosition1ByIndocno(@RequestBody String data) {
        return chockChangePosition1Service.findDataChockChangePosition1ByIndocno(data);
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	@CrossOrigin
	@RequestMapping(value={"/findAll"}, method=RequestMethod.POST)
	public List<ChockChangePosition1> findDataChockChangePosition1(){
		return chockChangePosition1Service.findDataChockChangePosition1();
	};
}
