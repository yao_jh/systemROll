package com.my.business.modular.chockchangeposition1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.chockchangeposition1.entity.ChockChangePosition1;
import com.my.business.modular.chockchangeposition1.entity.JChockChangePosition1;
import com.my.business.modular.chockchangeposition1.dao.ChockChangePosition1Dao;
import com.my.business.modular.chockchangeposition1.service.ChockChangePosition1Service;
import com.my.business.sys.common.entity.ResultData;

/**
* 轴承座换区工单——精轧工作辊2250接口具体实现类
* @author  生成器生成
* @date 2020-10-26 20:53:31
*/
@Service
public class ChockChangePosition1ServiceImpl implements ChockChangePosition1Service {
	
	@Autowired
	private ChockChangePosition1Dao chockChangePosition1Dao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataChockChangePosition1(String data,Long userId,String sname){
		try{
			JChockChangePosition1 jchockChangePosition1 = JSON.parseObject(data,JChockChangePosition1.class);
    		ChockChangePosition1 chockChangePosition1 = jchockChangePosition1.getChockChangePosition1();
    		CodiUtil.newRecord(userId,sname,chockChangePosition1);
            chockChangePosition1Dao.insertDataChockChangePosition1(chockChangePosition1);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataChockChangePosition1One(Long indocno){
		try {
            chockChangePosition1Dao.deleteDataChockChangePosition1One(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataChockChangePosition1Many(String str_id) {
        try {
        	String sql = "delete chock_change_position1 where indocno in(" + str_id +")";
            chockChangePosition1Dao.deleteDataChockChangePosition1Many(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param data sysUser 对象实体
     */
	public ResultData updateDataChockChangePosition1(String data,Long userId,String sname){
		try {
			JChockChangePosition1 jchockChangePosition1 = JSON.parseObject(data,JChockChangePosition1.class);
    		ChockChangePosition1 chockChangePosition1 = jchockChangePosition1.getChockChangePosition1();
        	CodiUtil.editRecord(userId,sname,chockChangePosition1);
            chockChangePosition1Dao.updateDataChockChangePosition1(chockChangePosition1);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataChockChangePosition1ByPage(String data) {
        try {
        	JChockChangePosition1 jchockChangePosition1 = JSON.parseObject(data, JChockChangePosition1.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jchockChangePosition1.getPageIndex();
        	Integer pageSize = jchockChangePosition1.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jchockChangePosition1.getCondition()){
    			jsonObject  = JSON.parseObject(jchockChangePosition1.getCondition().toString());
    		}
    
    		List<ChockChangePosition1> list = chockChangePosition1Dao.findDataChockChangePosition1ByPage((pageIndex-1)*pageSize, pageSize);
    		Integer count = chockChangePosition1Dao.findDataChockChangePosition1ByPageSize();
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataChockChangePosition1ByIndocno(String data) {
        try {
        	JChockChangePosition1 jchockChangePosition1 = JSON.parseObject(data, JChockChangePosition1.class); 
        	Long indocno = jchockChangePosition1.getIndocno();
        	
    		ChockChangePosition1 chockChangePosition1 = chockChangePosition1Dao.findDataChockChangePosition1ByIndocno(indocno);
    		return ResultData.ResultDataSuccess(chockChangePosition1);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<ChockChangePosition1> findDataChockChangePosition1(){
		List<ChockChangePosition1> list = chockChangePosition1Dao.findDataChockChangePosition1();
		return list;
	};
}
