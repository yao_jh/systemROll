package com.my.business.modular.chockchangeposition1.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.chockchangeposition1.entity.ChockChangePosition1;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 轴承座换区工单——精轧工作辊2250dao接口
 * @author  生成器生成
 * @date 2020-10-26 20:53:31
 * */
@Mapper
public interface ChockChangePosition1Dao {

	/**
	 * 添加记录
	 * @param chockChangePosition1  对象实体
	 * */
	public void insertDataChockChangePosition1(ChockChangePosition1 chockChangePosition1);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataChockChangePosition1One(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataChockChangePosition1Many(String value);
	
	/**
	 * 修改记录
	 * @param chockChangePosition1  对象实体
	 * */
	public void updateDataChockChangePosition1(ChockChangePosition1 chockChangePosition1);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<ChockChangePosition1> findDataChockChangePosition1ByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataChockChangePosition1ByPageSize();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public ChockChangePosition1 findDataChockChangePosition1ByIndocno(@Param("indocno") Long indocno);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<ChockChangePosition1> findDataChockChangePosition1();
	
}
