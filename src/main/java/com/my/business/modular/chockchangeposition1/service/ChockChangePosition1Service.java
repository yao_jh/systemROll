package com.my.business.modular.chockchangeposition1.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.modular.chockchangeposition1.entity.ChockChangePosition1;
import java.util.List;

/**
* 轴承座换区工单——精轧工作辊2250接口服务类
* @author  生成器生成
* @date 2020-10-26 20:53:31
*/
public interface ChockChangePosition1Service {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataChockChangePosition1(String data, Long userId, String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataChockChangePosition1One(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataChockChangePosition1Many(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataChockChangePosition1(String data, Long userId, String sname);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataChockChangePosition1ByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataChockChangePosition1ByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<ChockChangePosition1> findDataChockChangePosition1();
}
