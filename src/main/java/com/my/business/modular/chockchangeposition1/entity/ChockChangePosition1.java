package com.my.business.modular.chockchangeposition1.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轴承座换区工单——精轧工作辊2250实体类
 *
 * @author 生成器生成
 * @date 2020-10-26 20:53:31
 */
public class ChockChangePosition1 extends BaseEntity {

    private Long indocno;  //主键
    private String roll_no;  //辊号
    private Long roll_typeid;  //轧辊类型id
    private String roll_type;  //轧辊类型
    private Long frame_noid;  //机架号id
    private String frame_no;  //机架号
    private Long production_line_id;  //
    private String production_line;  //产线(0-1250,1-1580)
    private String field1;  //字段
    private String field2;  //字段
    private String field3;  //字段
    private String field4;  //字段
    private String field5;  //字段
    private String field6;  //字段
    private String field7;  //字段
    private String field8;  //字段
    private String field9;  //字段
    private String field10;  //字段
    private String field11;  //字段
    private String field12;  //字段
    private String field13;  //字段
    private String field14;  //字段
    private String field15;  //字段
    private String field16;  //字段
    private String field17;  //字段
    private String field18;  //字段
    private String field19;  //字段
    private String field20;  //字段
    private String field21;  //字段
    private String field22;  //字段
    private String field23;  //字段
    private String field24;  //字段
    private String field25;  //字段
    private String field26;  //字段
    private String field27;  //字段
    private String field28;  //字段
    private String field29;  //字段
    private String field30;  //字段
    private String field31;  //字段
    private String field32;  //字段
    private String field33;  //字段
    private String field34;  //字段
    private String snote;  //备注

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getRoll_no() {
        return this.roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public Long getRoll_typeid() {
        return this.roll_typeid;
    }

    public void setRoll_typeid(Long roll_typeid) {
        this.roll_typeid = roll_typeid;
    }

    public String getRoll_type() {
        return this.roll_type;
    }

    public void setRoll_type(String roll_type) {
        this.roll_type = roll_type;
    }

    public Long getFrame_noid() {
        return this.frame_noid;
    }

    public void setFrame_noid(Long frame_noid) {
        this.frame_noid = frame_noid;
    }

    public String getFrame_no() {
        return this.frame_no;
    }

    public void setFrame_no(String frame_no) {
        this.frame_no = frame_no;
    }

    public Long getProduction_line_id() {
        return this.production_line_id;
    }

    public void setProduction_line_id(Long production_line_id) {
        this.production_line_id = production_line_id;
    }

    public String getProduction_line() {
        return this.production_line;
    }

    public void setProduction_line(String production_line) {
        this.production_line = production_line;
    }

    public String getField1() {
        return this.field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return this.field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    public String getField3() {
        return this.field3;
    }

    public void setField3(String field3) {
        this.field3 = field3;
    }

    public String getField4() {
        return this.field4;
    }

    public void setField4(String field4) {
        this.field4 = field4;
    }

    public String getField5() {
        return this.field5;
    }

    public void setField5(String field5) {
        this.field5 = field5;
    }

    public String getField6() {
        return this.field6;
    }

    public void setField6(String field6) {
        this.field6 = field6;
    }

    public String getField7() {
        return this.field7;
    }

    public void setField7(String field7) {
        this.field7 = field7;
    }

    public String getField8() {
        return this.field8;
    }

    public void setField8(String field8) {
        this.field8 = field8;
    }

    public String getField9() {
        return this.field9;
    }

    public void setField9(String field9) {
        this.field9 = field9;
    }

    public String getField10() {
        return this.field10;
    }

    public void setField10(String field10) {
        this.field10 = field10;
    }

    public String getField11() {
        return this.field11;
    }

    public void setField11(String field11) {
        this.field11 = field11;
    }

    public String getField12() {
        return this.field12;
    }

    public void setField12(String field12) {
        this.field12 = field12;
    }

    public String getField13() {
        return this.field13;
    }

    public void setField13(String field13) {
        this.field13 = field13;
    }

    public String getField14() {
        return this.field14;
    }

    public void setField14(String field14) {
        this.field14 = field14;
    }

    public String getField15() {
        return this.field15;
    }

    public void setField15(String field15) {
        this.field15 = field15;
    }

    public String getField16() {
        return this.field16;
    }

    public void setField16(String field16) {
        this.field16 = field16;
    }

    public String getField17() {
        return this.field17;
    }

    public void setField17(String field17) {
        this.field17 = field17;
    }

    public String getField18() {
        return this.field18;
    }

    public void setField18(String field18) {
        this.field18 = field18;
    }

    public String getField19() {
        return this.field19;
    }

    public void setField19(String field19) {
        this.field19 = field19;
    }

    public String getField20() {
        return this.field20;
    }

    public void setField20(String field20) {
        this.field20 = field20;
    }

    public String getField21() {
        return this.field21;
    }

    public void setField21(String field21) {
        this.field21 = field21;
    }

    public String getField22() {
        return this.field22;
    }

    public void setField22(String field22) {
        this.field22 = field22;
    }

    public String getField23() {
        return this.field23;
    }

    public void setField23(String field23) {
        this.field23 = field23;
    }

    public String getField24() {
        return this.field24;
    }

    public void setField24(String field24) {
        this.field24 = field24;
    }

    public String getField25() {
        return this.field25;
    }

    public void setField25(String field25) {
        this.field25 = field25;
    }

    public String getField26() {
        return this.field26;
    }

    public void setField26(String field26) {
        this.field26 = field26;
    }

    public String getField27() {
        return this.field27;
    }

    public void setField27(String field27) {
        this.field27 = field27;
    }

    public String getField28() {
        return this.field28;
    }

    public void setField28(String field28) {
        this.field28 = field28;
    }

    public String getField29() {
        return this.field29;
    }

    public void setField29(String field29) {
        this.field29 = field29;
    }

    public String getField30() {
        return this.field30;
    }

    public void setField30(String field30) {
        this.field30 = field30;
    }

    public String getField31() {
        return this.field31;
    }

    public void setField31(String field31) {
        this.field31 = field31;
    }

    public String getField32() {
        return this.field32;
    }

    public void setField32(String field32) {
        this.field32 = field32;
    }

    public String getField33() {
        return this.field33;
    }

    public void setField33(String field33) {
        this.field33 = field33;
    }

    public String getField34() {
        return this.field34;
    }

    public void setField34(String field34) {
        this.field34 = field34;
    }

    public String getSnote() {
        return this.snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }


}