package com.my.business.modular.rollconspredict.dao;

import org.apache.ibatis.annotations.Mapper;
import com.my.business.modular.rollconspredict.entity.RollConspredict;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 磨削预测表dao接口
 * @author  生成器生成
 * @date 2020-11-20 10:23:36
 * */
@Mapper
public interface RollConspredictDao {

	/**
	 * 添加记录
	 * @param rollConspredict  对象实体
	 * */
	public void insertDataRollConspredict(RollConspredict rollConspredict);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public void deleteDataRollConspredictOne(@Param("indocno") Long indocno);
	
	/**
     * 删除多个对象
     * @param value sql语句
     */
    public void deleteDataRollConspredictMany(String value);
	
	/**
	 * 修改记录
	 * @param rollConspredict  对象实体
	 * */
	public void updateDataRollConspredict(RollConspredict rollConspredict);
	
	
	/**
     * 分页查看查看记录
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    public List<RollConspredict> findDataRollConspredictByPage(@Param("pageIndex") Integer pageIndex,@Param("pageSize") Integer pageSize);
    
    /**
     * 根据条件查看记录的总数
     * @return 对象数据集合
     */
    public Integer findDataRollConspredictByPageSize();
    
    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    public RollConspredict findDataRollConspredictByIndocno(@Param("indocno") Long indocno);
    
    /***
     * 根据钢卷号查询数据
     * @param indocno 用户id
     * @return
     */
    public RollConspredict findDataRollConspredictByAact_coilid(@Param("act_coilid") String act_coilid);
	
	/**
	 * 查看记录
	 * @return 对象数据集合
	 * */
	public List<RollConspredict> findDataRollConspredict();
	
}
