package com.my.business.modular.rollconspredict.service;

import com.my.business.sys.common.entity.ResultData;
import com.my.business.modular.rollconspredict.entity.RollConspredict;
import java.util.List;

/**
* 磨削预测表接口服务类
* @author  生成器生成
* @date 2020-11-20 10:23:37
*/
public interface RollConspredictService {

	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollConspredict(String data,Long userId,String sname);
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollConspredictOne(Long indocno);
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollConspredictMany(String str_id);
	
	/**
     * 修改记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData updateDataRollConspredict(String data,Long userId,String sname);
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollConspredictByPage(String data);
    
    /**
     * 查看一条数据信息
     * @param data 分页参数字符串
     */
    public ResultData findDataRollConspredictByIndocno(String data);
	
	/**
	 * 查看记录
	 * */
	public List<RollConspredict> findDataRollConspredict();
}
