package com.my.business.modular.rollconspredict.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.util.CodiUtil;

import com.my.business.modular.rollconspredict.entity.RollConspredict;
import com.my.business.modular.rollconspredict.entity.JRollConspredict;
import com.my.business.modular.rollconspredict.dao.RollConspredictDao;
import com.my.business.modular.rollconspredict.service.RollConspredictService;
import com.my.business.sys.common.entity.ResultData;

/**
* 磨削预测表接口具体实现类
* @author  生成器生成
* @date 2020-11-20 10:23:37
*/
@Service
public class RollConspredictServiceImpl implements RollConspredictService {
	
	@Autowired
	private RollConspredictDao rollConspredictDao;
	
	/**
     * 添加记录
     * @param data json字符串
     * @param userId 用户id
     * @param sname 用户姓名
     */
	public ResultData insertDataRollConspredict(String data,Long userId,String sname){
		try{
			JRollConspredict jrollConspredict = JSON.parseObject(data,JRollConspredict.class);
    		RollConspredict rollConspredict = jrollConspredict.getRollConspredict();
    		CodiUtil.newRecord(userId,sname,rollConspredict);
            rollConspredictDao.insertDataRollConspredict(rollConspredict);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
    	}catch(Exception e){
    		e.printStackTrace();
    		return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null); 
    	}
	};
	
	/**
	 * 根据主键删除对象
	 * @param indocno  对象主键
	 * */
	public ResultData deleteDataRollConspredictOne(Long indocno){
		try {
            rollConspredictDao.deleteDataRollConspredictOne(indocno);
             return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 根据主键字符串删除多个对象
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollConspredictMany(String str_id) {
        try {
        	String sql = "delete roll_conspredict where indocno in(" + str_id +")";
            rollConspredictDao.deleteDataRollConspredictMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
     * 修改记录
     * @param userId 用户id
     * @param sname 用户姓名
     * @param sysUser 对象实体
     */
	public ResultData updateDataRollConspredict(String data,Long userId,String sname){
		try {
			JRollConspredict jrollConspredict = JSON.parseObject(data,JRollConspredict.class);
    		RollConspredict rollConspredict = jrollConspredict.getRollConspredict();
        	CodiUtil.editRecord(userId,sname,rollConspredict);
            rollConspredictDao.updateDataRollConspredict(rollConspredict);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null); 
        }
	};
	
	/**
     * 分页查看记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollConspredictByPage(String data) {
        try {
        	JRollConspredict jrollConspredict = JSON.parseObject(data, JRollConspredict.class); 
        	JSONObject jsonObject  = null;
        	Integer pageIndex = jrollConspredict.getPageIndex();
        	Integer pageSize = jrollConspredict.getPageSize();
        	
        	if(null == pageIndex || null == pageSize){
        		return ResultData.ResultDataFaultSelf("分页参数没有传", null); 
        	}
        	
        	if(null!=jrollConspredict.getCondition()){
    			jsonObject  = JSON.parseObject(jrollConspredict.getCondition().toString());
    		}
    
    		List<RollConspredict> list = rollConspredictDao.findDataRollConspredictByPage((pageIndex-1)*pageSize, pageSize);
    		Integer count = rollConspredictDao.findDataRollConspredictByPageSize();
    		return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
    
    /**
     * 根据主键查询单条记录
     * @param data 分页参数字符串
     */
    public ResultData findDataRollConspredictByIndocno(String data) {
        try {
        	JRollConspredict jrollConspredict = JSON.parseObject(data, JRollConspredict.class); 
        	Long indocno = jrollConspredict.getIndocno();
        	
    		RollConspredict rollConspredict = rollConspredictDao.findDataRollConspredictByIndocno(indocno);
    		return ResultData.ResultDataSuccess(rollConspredict);
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null); 
        }
    }
	
	/**
	 * 查看记录
	 * @return list 对象集合返回
	 * */
	public List<RollConspredict> findDataRollConspredict(){
		List<RollConspredict> list = rollConspredictDao.findDataRollConspredict();
		return list;
	};
}
