package com.my.business.modular.rollconspredict.entity;

import java.util.Date;
import com.my.business.sys.common.entity.BaseEntity;

/**
* 磨削预测表实体类
* @author  生成器生成
* @date 2020-11-20 10:36:14
*/
public class RollConspredict extends BaseEntity{
		
	private Long curtime;  //时间戳
	private String act_coilid;  //钢卷号
	private String fw_1_t;  //精轧工作辊1机架上辊
	private String fw_2_t;  //精轧工作辊2机架上辊
	private String fw_3_t;  //精轧工作辊3机架上辊
	private String fw_4_t;  //精轧工作辊4机架上辊
	private String fw_5_t;  //精轧工作辊5机架上辊
	private String fw_6_t;  //精轧工作辊6机架上辊
	private String fw_7_t;  //精轧工作辊7机架上辊
	private String fw_1_b;  //精轧工作辊1机架下辊
	private String fw_2_b;  //精轧工作辊2机架下辊
	private String fw_3_b;  //精轧工作辊3机架下辊
	private String fw_4_b;  //精轧工作辊4机架下辊
	private String fw_5_b;  //精轧工作辊5机架下辊
	private String fw_6_b;  //精轧工作辊6机架下辊
	private String fw_7_b;  //精轧工作辊7机架下辊
	private String fb_1_t;  //精轧支撑辊1机架上辊
	private String fb_2_t;  //精轧支撑辊2机架上辊
	private String fb_3_t;  //精轧支撑辊3机架上辊
	private String fb_4_t;  //精轧支撑辊4机架上辊
	private String fb_5_t;  //精轧支撑辊5机架上辊
	private String fb_6_t;  //精轧支撑辊6机架上辊
	private String fb_7_t;  //精轧支撑辊7机架上辊
	private String fb_1_b;  //精轧支撑辊1机架下辊
	private String fb_2_b;  //精轧支撑辊2机架下辊
	private String fb_3_b;  //精轧支撑辊3机架下辊
	private String fb_4_b;  //精轧支撑辊4机架下辊
	private String fb_5_b;  //精轧支撑辊5机架下辊
	private String fb_6_b;  //精轧支撑辊6机架下辊
	private String fb_7_b;  //精轧支撑辊7机架下辊
	private String rw_8_t;  //粗轧工作辊R1机架上辊
	private String rw_9_t;  //粗轧工作辊R2机架上辊
	private String rw_8_b;  //粗轧工作辊R1机架下辊
	private String rw_9_b;  //粗轧工作辊R2机架下辊
	private String rb_8_t;  //粗轧支撑辊RI机架上辊
	private String rb_9_t;  //粗轧支撑辊R2机架上辊
	private String rb_8_b;  //粗轧支撑辊R1机架下辊
	private String rb_9_b;  //粗轧支撑辊R2机架下辊
	private String fe_14_os;  //精轧立辊F1E操作侧
	private String fe_14_ds;  //精轧立辊F1E传动侧
	private String re_12_os;  //粗轧立辊R1E操作侧
	private String re_13_os;  //粗轧立辊R2E操作侧
	private String re_12_ds;  //粗轧立辊R1E传动侧
	private String re_13_ds;  //粗轧立辊R2E传动侧
	private Float fw_1_t_price;  //精轧工作辊1机架上辊预测费用
	private Float fw_2_t_price;  //精轧工作辊2机架上辊预测费用
	private Float fw_3_t_price;  //精轧工作辊3机架上辊预测费用
	private Float fw_4_t_price;  //精轧工作辊4机架上辊预测费用
	private Float fw_5_t_price;  //精轧工作辊5机架上辊预测费用
	private Float fw_6_t_price;  //精轧工作辊6机架上辊预测费用
	private Float fw_7_t_price;  //精轧工作辊7机架上辊预测费用
	private Float fw_1_b_price;  //精轧工作辊1机架下辊预测费用
	private Float fw_2_b_price;  //精轧工作辊2机架下辊预测费用
	private Float fw_3_b_price;  //精轧工作辊3机架下辊预测费用
	private Float fw_4_b_price;  //精轧工作辊4机架下辊预测费用
	private Float fw_5_b_price;  //精轧工作辊5机架下辊预测费用
	private Float fw_6_b_price;  //精轧工作辊6机架下辊预测费用
	private Float fw_7_b_price;  //精轧工作辊7机架下辊预测费用
	private Float fb_1_t_price;  //精轧支撑辊1机架上辊预测费用
	private Float fb_2_t_price;  //精轧支撑辊2机架上辊预测费用
	private Float fb_3_t_price;  //精轧支撑辊3机架上辊预测费用
	private Float fb_4_t_price;  //精轧支撑辊4机架上辊预测费用
	private Float fb_5_t_price;  //精轧支撑辊5机架上辊预测费用
	private Float fb_6_t_price;  //精轧支撑辊6机架上辊预测费用
	private Float fb_7_t_price;  //精轧支撑辊7机架上辊预测费用
	private Float fb_1_b_price;  //精轧支撑辊1机架下辊预测费用
	private Float fb_2_b_price;  //精轧支撑辊2机架下辊预测费用
	private Float fb_3_b_price;  //精轧支撑辊3机架下辊预测费用
	private Float fb_4_b_price;  //精轧支撑辊4机架下辊预测费用
	private Float fb_5_b_price;  //精轧支撑辊5机架下辊预测费用
	private Float fb_6_b_price;  //精轧支撑辊6机架下辊预测费用
	private Float fb_7_b_price;  //精轧支撑辊7机架下辊预测费用
	private Float rw_8_t_price;  //粗轧工作辊R1机架上辊预测费用
	private Float rw_9_t_price;  //粗轧工作辊R2机架上辊预测费用
	private Float rw_8_b_price;  //粗轧工作辊R1机架下辊预测费用
	private Float rw_9_b_price;  //粗轧工作辊R2机架下辊预测费用
	private Float rb_8_t_price;  //粗轧支撑辊RI机架上辊预测费用
	private Float rb_9_t_price;  //粗轧支撑辊R2机架上辊预测费用
	private Float rb_8_b_price;  //粗轧支撑辊RI机架下辊预测费用
	private Float rb_9_b_price;  //粗轧支撑辊R2机架下辊预测费用
	private Float fe_14_os_price;  //精轧立辊F1E操作侧预测费用
	private Float fe_14_ds_price;  //精轧立辊F1E传动侧预测费用
	private Float re_12_os_price;  //粗轧立辊R1E操作侧预测费用
	private Float re_13_os_price;  //粗轧立辊R2E操作侧预测费用
	private Float re_12_ds_price;  //粗轧立辊R1E传动侧预测费用
	private Float re_13_ds_price;  //粗轧立辊R2E传动侧预测费用
	private Float total_price;  //总费用
    
    
	public void setCurtime(Long curtime){
	    this.curtime = curtime;
	}
	public Long getCurtime(){
	    return this.curtime;
	}
	public void setAct_coilid(String act_coilid){
	    this.act_coilid = act_coilid;
	}
	public String getAct_coilid(){
	    return this.act_coilid;
	}
	public void setFw_1_t(String fw_1_t){
	    this.fw_1_t = fw_1_t;
	}
	public String getFw_1_t(){
	    return this.fw_1_t;
	}
	public void setFw_2_t(String fw_2_t){
	    this.fw_2_t = fw_2_t;
	}
	public String getFw_2_t(){
	    return this.fw_2_t;
	}
	public void setFw_3_t(String fw_3_t){
	    this.fw_3_t = fw_3_t;
	}
	public String getFw_3_t(){
	    return this.fw_3_t;
	}
	public void setFw_4_t(String fw_4_t){
	    this.fw_4_t = fw_4_t;
	}
	public String getFw_4_t(){
	    return this.fw_4_t;
	}
	public void setFw_5_t(String fw_5_t){
	    this.fw_5_t = fw_5_t;
	}
	public String getFw_5_t(){
	    return this.fw_5_t;
	}
	public void setFw_6_t(String fw_6_t){
	    this.fw_6_t = fw_6_t;
	}
	public String getFw_6_t(){
	    return this.fw_6_t;
	}
	public void setFw_7_t(String fw_7_t){
	    this.fw_7_t = fw_7_t;
	}
	public String getFw_7_t(){
	    return this.fw_7_t;
	}
	public void setFw_1_b(String fw_1_b){
	    this.fw_1_b = fw_1_b;
	}
	public String getFw_1_b(){
	    return this.fw_1_b;
	}
	public void setFw_2_b(String fw_2_b){
	    this.fw_2_b = fw_2_b;
	}
	public String getFw_2_b(){
	    return this.fw_2_b;
	}
	public void setFw_3_b(String fw_3_b){
	    this.fw_3_b = fw_3_b;
	}
	public String getFw_3_b(){
	    return this.fw_3_b;
	}
	public void setFw_4_b(String fw_4_b){
	    this.fw_4_b = fw_4_b;
	}
	public String getFw_4_b(){
	    return this.fw_4_b;
	}
	public void setFw_5_b(String fw_5_b){
	    this.fw_5_b = fw_5_b;
	}
	public String getFw_5_b(){
	    return this.fw_5_b;
	}
	public void setFw_6_b(String fw_6_b){
	    this.fw_6_b = fw_6_b;
	}
	public String getFw_6_b(){
	    return this.fw_6_b;
	}
	public void setFw_7_b(String fw_7_b){
	    this.fw_7_b = fw_7_b;
	}
	public String getFw_7_b(){
	    return this.fw_7_b;
	}
	public void setFb_1_t(String fb_1_t){
	    this.fb_1_t = fb_1_t;
	}
	public String getFb_1_t(){
	    return this.fb_1_t;
	}
	public void setFb_2_t(String fb_2_t){
	    this.fb_2_t = fb_2_t;
	}
	public String getFb_2_t(){
	    return this.fb_2_t;
	}
	public void setFb_3_t(String fb_3_t){
	    this.fb_3_t = fb_3_t;
	}
	public String getFb_3_t(){
	    return this.fb_3_t;
	}
	public void setFb_4_t(String fb_4_t){
	    this.fb_4_t = fb_4_t;
	}
	public String getFb_4_t(){
	    return this.fb_4_t;
	}
	public void setFb_5_t(String fb_5_t){
	    this.fb_5_t = fb_5_t;
	}
	public String getFb_5_t(){
	    return this.fb_5_t;
	}
	public void setFb_6_t(String fb_6_t){
	    this.fb_6_t = fb_6_t;
	}
	public String getFb_6_t(){
	    return this.fb_6_t;
	}
	public void setFb_7_t(String fb_7_t){
	    this.fb_7_t = fb_7_t;
	}
	public String getFb_7_t(){
	    return this.fb_7_t;
	}
	public void setFb_1_b(String fb_1_b){
	    this.fb_1_b = fb_1_b;
	}
	public String getFb_1_b(){
	    return this.fb_1_b;
	}
	public void setFb_2_b(String fb_2_b){
	    this.fb_2_b = fb_2_b;
	}
	public String getFb_2_b(){
	    return this.fb_2_b;
	}
	public void setFb_3_b(String fb_3_b){
	    this.fb_3_b = fb_3_b;
	}
	public String getFb_3_b(){
	    return this.fb_3_b;
	}
	public void setFb_4_b(String fb_4_b){
	    this.fb_4_b = fb_4_b;
	}
	public String getFb_4_b(){
	    return this.fb_4_b;
	}
	public void setFb_5_b(String fb_5_b){
	    this.fb_5_b = fb_5_b;
	}
	public String getFb_5_b(){
	    return this.fb_5_b;
	}
	public void setFb_6_b(String fb_6_b){
	    this.fb_6_b = fb_6_b;
	}
	public String getFb_6_b(){
	    return this.fb_6_b;
	}
	public void setFb_7_b(String fb_7_b){
	    this.fb_7_b = fb_7_b;
	}
	public String getFb_7_b(){
	    return this.fb_7_b;
	}
	public void setRw_8_t(String rw_8_t){
	    this.rw_8_t = rw_8_t;
	}
	public String getRw_8_t(){
	    return this.rw_8_t;
	}
	public void setRw_9_t(String rw_9_t){
	    this.rw_9_t = rw_9_t;
	}
	public String getRw_9_t(){
	    return this.rw_9_t;
	}
	public void setRw_8_b(String rw_8_b){
	    this.rw_8_b = rw_8_b;
	}
	public String getRw_8_b(){
	    return this.rw_8_b;
	}
	public void setRw_9_b(String rw_9_b){
	    this.rw_9_b = rw_9_b;
	}
	public String getRw_9_b(){
	    return this.rw_9_b;
	}
	public void setRb_8_t(String rb_8_t){
	    this.rb_8_t = rb_8_t;
	}
	public String getRb_8_t(){
	    return this.rb_8_t;
	}
	public void setRb_9_t(String rb_9_t){
	    this.rb_9_t = rb_9_t;
	}
	public String getRb_9_t(){
	    return this.rb_9_t;
	}
	public void setRb_8_b(String rb_8_b){
	    this.rb_8_b = rb_8_b;
	}
	public String getRb_8_b(){
	    return this.rb_8_b;
	}
	public void setRb_9_b(String rb_9_b){
	    this.rb_9_b = rb_9_b;
	}
	public String getRb_9_b(){
	    return this.rb_9_b;
	}
	public void setFe_14_os(String fe_14_os){
	    this.fe_14_os = fe_14_os;
	}
	public String getFe_14_os(){
	    return this.fe_14_os;
	}
	public void setFe_14_ds(String fe_14_ds){
	    this.fe_14_ds = fe_14_ds;
	}
	public String getFe_14_ds(){
	    return this.fe_14_ds;
	}
	public void setRe_12_os(String re_12_os){
	    this.re_12_os = re_12_os;
	}
	public String getRe_12_os(){
	    return this.re_12_os;
	}
	public void setRe_13_os(String re_13_os){
	    this.re_13_os = re_13_os;
	}
	public String getRe_13_os(){
	    return this.re_13_os;
	}
	public void setRe_12_ds(String re_12_ds){
	    this.re_12_ds = re_12_ds;
	}
	public String getRe_12_ds(){
	    return this.re_12_ds;
	}
	public void setRe_13_ds(String re_13_ds){
	    this.re_13_ds = re_13_ds;
	}
	public String getRe_13_ds(){
	    return this.re_13_ds;
	}
	public void setFw_1_t_price(Float fw_1_t_price){
	    this.fw_1_t_price = fw_1_t_price;
	}
	public Float getFw_1_t_price(){
	    return this.fw_1_t_price;
	}
	public void setFw_2_t_price(Float fw_2_t_price){
	    this.fw_2_t_price = fw_2_t_price;
	}
	public Float getFw_2_t_price(){
	    return this.fw_2_t_price;
	}
	public void setFw_3_t_price(Float fw_3_t_price){
	    this.fw_3_t_price = fw_3_t_price;
	}
	public Float getFw_3_t_price(){
	    return this.fw_3_t_price;
	}
	public void setFw_4_t_price(Float fw_4_t_price){
	    this.fw_4_t_price = fw_4_t_price;
	}
	public Float getFw_4_t_price(){
	    return this.fw_4_t_price;
	}
	public void setFw_5_t_price(Float fw_5_t_price){
	    this.fw_5_t_price = fw_5_t_price;
	}
	public Float getFw_5_t_price(){
	    return this.fw_5_t_price;
	}
	public void setFw_6_t_price(Float fw_6_t_price){
	    this.fw_6_t_price = fw_6_t_price;
	}
	public Float getFw_6_t_price(){
	    return this.fw_6_t_price;
	}
	public void setFw_7_t_price(Float fw_7_t_price){
	    this.fw_7_t_price = fw_7_t_price;
	}
	public Float getFw_7_t_price(){
	    return this.fw_7_t_price;
	}
	public void setFw_1_b_price(Float fw_1_b_price){
	    this.fw_1_b_price = fw_1_b_price;
	}
	public Float getFw_1_b_price(){
	    return this.fw_1_b_price;
	}
	public void setFw_2_b_price(Float fw_2_b_price){
	    this.fw_2_b_price = fw_2_b_price;
	}
	public Float getFw_2_b_price(){
	    return this.fw_2_b_price;
	}
	public void setFw_3_b_price(Float fw_3_b_price){
	    this.fw_3_b_price = fw_3_b_price;
	}
	public Float getFw_3_b_price(){
	    return this.fw_3_b_price;
	}
	public void setFw_4_b_price(Float fw_4_b_price){
	    this.fw_4_b_price = fw_4_b_price;
	}
	public Float getFw_4_b_price(){
	    return this.fw_4_b_price;
	}
	public void setFw_5_b_price(Float fw_5_b_price){
	    this.fw_5_b_price = fw_5_b_price;
	}
	public Float getFw_5_b_price(){
	    return this.fw_5_b_price;
	}
	public void setFw_6_b_price(Float fw_6_b_price){
	    this.fw_6_b_price = fw_6_b_price;
	}
	public Float getFw_6_b_price(){
	    return this.fw_6_b_price;
	}
	public void setFw_7_b_price(Float fw_7_b_price){
	    this.fw_7_b_price = fw_7_b_price;
	}
	public Float getFw_7_b_price(){
	    return this.fw_7_b_price;
	}
	public void setFb_1_t_price(Float fb_1_t_price){
	    this.fb_1_t_price = fb_1_t_price;
	}
	public Float getFb_1_t_price(){
	    return this.fb_1_t_price;
	}
	public void setFb_2_t_price(Float fb_2_t_price){
	    this.fb_2_t_price = fb_2_t_price;
	}
	public Float getFb_2_t_price(){
	    return this.fb_2_t_price;
	}
	public void setFb_3_t_price(Float fb_3_t_price){
	    this.fb_3_t_price = fb_3_t_price;
	}
	public Float getFb_3_t_price(){
	    return this.fb_3_t_price;
	}
	public void setFb_4_t_price(Float fb_4_t_price){
	    this.fb_4_t_price = fb_4_t_price;
	}
	public Float getFb_4_t_price(){
	    return this.fb_4_t_price;
	}
	public void setFb_5_t_price(Float fb_5_t_price){
	    this.fb_5_t_price = fb_5_t_price;
	}
	public Float getFb_5_t_price(){
	    return this.fb_5_t_price;
	}
	public void setFb_6_t_price(Float fb_6_t_price){
	    this.fb_6_t_price = fb_6_t_price;
	}
	public Float getFb_6_t_price(){
	    return this.fb_6_t_price;
	}
	public void setFb_7_t_price(Float fb_7_t_price){
	    this.fb_7_t_price = fb_7_t_price;
	}
	public Float getFb_7_t_price(){
	    return this.fb_7_t_price;
	}
	public void setFb_1_b_price(Float fb_1_b_price){
	    this.fb_1_b_price = fb_1_b_price;
	}
	public Float getFb_1_b_price(){
	    return this.fb_1_b_price;
	}
	public void setFb_2_b_price(Float fb_2_b_price){
	    this.fb_2_b_price = fb_2_b_price;
	}
	public Float getFb_2_b_price(){
	    return this.fb_2_b_price;
	}
	public void setFb_3_b_price(Float fb_3_b_price){
	    this.fb_3_b_price = fb_3_b_price;
	}
	public Float getFb_3_b_price(){
	    return this.fb_3_b_price;
	}
	public void setFb_4_b_price(Float fb_4_b_price){
	    this.fb_4_b_price = fb_4_b_price;
	}
	public Float getFb_4_b_price(){
	    return this.fb_4_b_price;
	}
	public void setFb_5_b_price(Float fb_5_b_price){
	    this.fb_5_b_price = fb_5_b_price;
	}
	public Float getFb_5_b_price(){
	    return this.fb_5_b_price;
	}
	public void setFb_6_b_price(Float fb_6_b_price){
	    this.fb_6_b_price = fb_6_b_price;
	}
	public Float getFb_6_b_price(){
	    return this.fb_6_b_price;
	}
	public void setFb_7_b_price(Float fb_7_b_price){
	    this.fb_7_b_price = fb_7_b_price;
	}
	public Float getFb_7_b_price(){
	    return this.fb_7_b_price;
	}
	public void setRw_8_t_price(Float rw_8_t_price){
	    this.rw_8_t_price = rw_8_t_price;
	}
	public Float getRw_8_t_price(){
	    return this.rw_8_t_price;
	}
	public void setRw_9_t_price(Float rw_9_t_price){
	    this.rw_9_t_price = rw_9_t_price;
	}
	public Float getRw_9_t_price(){
	    return this.rw_9_t_price;
	}
	public void setRw_8_b_price(Float rw_8_b_price){
	    this.rw_8_b_price = rw_8_b_price;
	}
	public Float getRw_8_b_price(){
	    return this.rw_8_b_price;
	}
	public void setRw_9_b_price(Float rw_9_b_price){
	    this.rw_9_b_price = rw_9_b_price;
	}
	public Float getRw_9_b_price(){
	    return this.rw_9_b_price;
	}
	public void setRb_8_t_price(Float rb_8_t_price){
	    this.rb_8_t_price = rb_8_t_price;
	}
	public Float getRb_8_t_price(){
	    return this.rb_8_t_price;
	}
	public void setRb_9_t_price(Float rb_9_t_price){
	    this.rb_9_t_price = rb_9_t_price;
	}
	public Float getRb_9_t_price(){
	    return this.rb_9_t_price;
	}
	public void setRb_8_b_price(Float rb_8_b_price){
	    this.rb_8_b_price = rb_8_b_price;
	}
	public Float getRb_8_b_price(){
	    return this.rb_8_b_price;
	}
	public void setRb_9_b_price(Float rb_9_b_price){
	    this.rb_9_b_price = rb_9_b_price;
	}
	public Float getRb_9_b_price(){
	    return this.rb_9_b_price;
	}
	public void setFe_14_os_price(Float fe_14_os_price){
	    this.fe_14_os_price = fe_14_os_price;
	}
	public Float getFe_14_os_price(){
	    return this.fe_14_os_price;
	}
	public void setFe_14_ds_price(Float fe_14_ds_price){
	    this.fe_14_ds_price = fe_14_ds_price;
	}
	public Float getFe_14_ds_price(){
	    return this.fe_14_ds_price;
	}
	public void setRe_12_os_price(Float re_12_os_price){
	    this.re_12_os_price = re_12_os_price;
	}
	public Float getRe_12_os_price(){
	    return this.re_12_os_price;
	}
	public void setRe_13_os_price(Float re_13_os_price){
	    this.re_13_os_price = re_13_os_price;
	}
	public Float getRe_13_os_price(){
	    return this.re_13_os_price;
	}
	public void setRe_12_ds_price(Float re_12_ds_price){
	    this.re_12_ds_price = re_12_ds_price;
	}
	public Float getRe_12_ds_price(){
	    return this.re_12_ds_price;
	}
	public void setRe_13_ds_price(Float re_13_ds_price){
	    this.re_13_ds_price = re_13_ds_price;
	}
	public Float getRe_13_ds_price(){
	    return this.re_13_ds_price;
	}
	public void setTotal_price(Float total_price){
	    this.total_price = total_price;
	}
	public Float getTotal_price(){
	    return this.total_price;
	}

    
}