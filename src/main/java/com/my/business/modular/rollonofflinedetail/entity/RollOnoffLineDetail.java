package com.my.business.modular.rollonofflinedetail.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 轧辊上下线管理子表实体类
 *
 * @author 生成器生成
 * @date 2020-08-17 15:44:48
 */
public class RollOnoffLineDetail extends BaseEntity {

    private Long indocno;  //主键
    private Long ilinkno;  //外键
    private Long steel_typeid;  //钢种id
    private String steel_type;  //钢种
    private String kilometer;  //公里数
    private String tonnage;  //吨位

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Long getIlinkno() {
        return this.ilinkno;
    }

    public void setIlinkno(Long ilinkno) {
        this.ilinkno = ilinkno;
    }

    public Long getSteel_typeid() {
        return this.steel_typeid;
    }

    public void setSteel_typeid(Long steel_typeid) {
        this.steel_typeid = steel_typeid;
    }

    public String getSteel_type() {
        return this.steel_type;
    }

    public void setSteel_type(String steel_type) {
        this.steel_type = steel_type;
    }

    public String getKilometer() {
        return this.kilometer;
    }

    public void setKilometer(String kilometer) {
        this.kilometer = kilometer;
    }

    public String getTonnage() {
        return this.tonnage;
    }

    public void setTonnage(String tonnage) {
        this.tonnage = tonnage;
    }


}