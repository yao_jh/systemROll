package com.my.business.modular.rollonofflinedetail.service;

import com.my.business.modular.rollonofflinedetail.entity.RollOnoffLineDetail;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 轧辊上下线管理子表接口服务类
 *
 * @author 生成器生成
 * @date 2020-08-17 15:44:49
 */
public interface RollOnoffLineDetailService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollOnoffLineDetail(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollOnoffLineDetailOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollOnoffLineDetailMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollOnoffLineDetail(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollOnoffLineDetailByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollOnoffLineDetailByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollOnoffLineDetail> findDataRollOnoffLineDetail();
}
