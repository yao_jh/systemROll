package com.my.business.modular.rollonofflinedetail.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollonofflinedetail.entity.JRollOnoffLineDetail;
import com.my.business.modular.rollonofflinedetail.entity.RollOnoffLineDetail;
import com.my.business.modular.rollonofflinedetail.service.RollOnoffLineDetailService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 轧辊上下线管理子表控制器层
 *
 * @author 生成器生成
 * @date 2020-08-17 15:44:49
 */
@RestController
@RequestMapping("/rollOnoffLineDetail")
public class RollOnoffLineDetailController {

    @Autowired
    private RollOnoffLineDetailService rollOnoffLineDetailService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname 用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollOnoffLineDetail(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollOnoffLineDetailService.insertDataRollOnoffLineDetail(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollOnoffLineDetailOne(@RequestBody String data) {
        try {
            JRollOnoffLineDetail jrollOnoffLineDetail = JSON.parseObject(data, JRollOnoffLineDetail.class);
            return rollOnoffLineDetailService.deleteDataRollOnoffLineDetailOne(jrollOnoffLineDetail.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollOnoffLineDetail jrollOnoffLineDetail = JSON.parseObject(data, JRollOnoffLineDetail.class);
            return rollOnoffLineDetailService.deleteDataRollOnoffLineDetailMany(jrollOnoffLineDetail.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollOnoffLineDetail(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollOnoffLineDetailService.updateDataRollOnoffLineDetail(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollOnoffLineDetailByPage(@RequestBody String data) {
        return rollOnoffLineDetailService.findDataRollOnoffLineDetailByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollOnoffLineDetailByIndocno(@RequestBody String data) {
        return rollOnoffLineDetailService.findDataRollOnoffLineDetailByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollOnoffLineDetail> findDataRollOnoffLineDetail() {
        return rollOnoffLineDetailService.findDataRollOnoffLineDetail();
    }

}
