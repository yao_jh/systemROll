package com.my.business.modular.rollonofflinedetail.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollonofflinedetail.dao.RollOnoffLineDetailDao;
import com.my.business.modular.rollonofflinedetail.entity.JRollOnoffLineDetail;
import com.my.business.modular.rollonofflinedetail.entity.RollOnoffLineDetail;
import com.my.business.modular.rollonofflinedetail.service.RollOnoffLineDetailService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 轧辊上下线管理子表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-08-17 15:44:49
 */
@Service
public class RollOnoffLineDetailServiceImpl implements RollOnoffLineDetailService {

    @Autowired
    private RollOnoffLineDetailDao rollOnoffLineDetailDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollOnoffLineDetail(String data, Long userId, String sname) {
        try {
            JRollOnoffLineDetail jrollOnoffLineDetail = JSON.parseObject(data, JRollOnoffLineDetail.class);
            RollOnoffLineDetail rollOnoffLineDetail = jrollOnoffLineDetail.getRollOnoffLineDetail();
            CodiUtil.newRecord(userId, sname, rollOnoffLineDetail);
            rollOnoffLineDetailDao.insertDataRollOnoffLineDetail(rollOnoffLineDetail);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollOnoffLineDetailOne(Long indocno) {
        try {
            rollOnoffLineDetailDao.deleteDataRollOnoffLineDetailOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollOnoffLineDetailMany(String str_id) {
        try {
            String sql = "delete roll_onoff_line_detail where indocno in(" + str_id + ")";
            rollOnoffLineDetailDao.deleteDataRollOnoffLineDetailMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollOnoffLineDetail(String data, Long userId, String sname) {
        try {
            JRollOnoffLineDetail jrollOnoffLineDetail = JSON.parseObject(data, JRollOnoffLineDetail.class);
            RollOnoffLineDetail rollOnoffLineDetail = jrollOnoffLineDetail.getRollOnoffLineDetail();
            CodiUtil.editRecord(userId, sname, rollOnoffLineDetail);
            rollOnoffLineDetailDao.updateDataRollOnoffLineDetail(rollOnoffLineDetail);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollOnoffLineDetailByPage(String data) {
        try {
            JRollOnoffLineDetail jrollOnoffLineDetail = JSON.parseObject(data, JRollOnoffLineDetail.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollOnoffLineDetail.getPageIndex();
            Integer pageSize = jrollOnoffLineDetail.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollOnoffLineDetail.getCondition()) {
                jsonObject = JSON.parseObject(jrollOnoffLineDetail.getCondition().toString());
            }

            String ilinkno = null;
            if (!StringUtils.isEmpty(jsonObject.get("ilinkno"))) {
                ilinkno = jsonObject.get("ilinkno").toString();
            }

            List<RollOnoffLineDetail> list = rollOnoffLineDetailDao.findDataRollOnoffLineDetailByPage((pageIndex - 1) * pageSize, pageSize, ilinkno);
            Integer count = rollOnoffLineDetailDao.findDataRollOnoffLineDetailByPageSize(ilinkno);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollOnoffLineDetailByIndocno(String data) {
        try {
            JRollOnoffLineDetail jrollOnoffLineDetail = JSON.parseObject(data, JRollOnoffLineDetail.class);
            Long indocno = jrollOnoffLineDetail.getIndocno();

            RollOnoffLineDetail rollOnoffLineDetail = rollOnoffLineDetailDao.findDataRollOnoffLineDetailByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollOnoffLineDetail);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollOnoffLineDetail> findDataRollOnoffLineDetail() {
        List<RollOnoffLineDetail> list = rollOnoffLineDetailDao.findDataRollOnoffLineDetail();
        return list;
    }

}
