package com.my.business.modular.rollonofflinedetail.dao;

import com.my.business.modular.rollonoffline.entity.RollOnoffLineView;
import com.my.business.modular.rollonofflinedetail.entity.RollOnoffLineDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 轧辊上下线管理子表dao接口
 *
 * @author 生成器生成
 * @date 2020-08-17 15:44:49
 */
@Mapper
public interface RollOnoffLineDetailDao {

    /**
     * 添加记录
     *
     * @param rollOnoffLineDetail 对象实体
     */
    void insertDataRollOnoffLineDetail(RollOnoffLineDetail rollOnoffLineDetail);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollOnoffLineDetailOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollOnoffLineDetailMany(String value);

    /**
     * 修改记录
     *
     * @param rollOnoffLineDetail 对象实体
     */
    void updateDataRollOnoffLineDetail(RollOnoffLineDetail rollOnoffLineDetail);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollOnoffLineDetail> findDataRollOnoffLineDetailByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("ilinkno") String ilinkno);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollOnoffLineDetailByPageSize(@Param("ilinkno") String ilinkno);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollOnoffLineDetail findDataRollOnoffLineDetailByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollOnoffLineDetail> findDataRollOnoffLineDetail();

    /***
     * 根据外键删除相关信息
     * @param indocno 用户id
     * @return
     */
    void deleteDataRollOnoffLineDetailManyByIlinkno(@Param("indocno") Long indocno);

    /***
     * 根据外键查询信息
     * @param indocno 用户id
     * @return
     */
    List<RollOnoffLineDetail> findDataRollOnoffLineDetailByIlinkno(Long indocno);

}
