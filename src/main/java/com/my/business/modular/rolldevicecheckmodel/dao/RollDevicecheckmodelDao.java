package com.my.business.modular.rolldevicecheckmodel.dao;

import com.my.business.modular.rolldevicecheckmodel.entity.RollDeviceCheckModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 点检模板数据表dao接口
 *
 * @author 生成器生成
 * @date 2020-07-14 16:08:24
 */
@Mapper
public interface RollDevicecheckmodelDao {

    /**
     * 添加记录
     *
     * @param rollDevicecheckmodel 对象实体
     */
    void insertDataRollDevicecheckmodel(RollDeviceCheckModel rollDevicecheckmodel);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollDevicecheckmodelOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollDevicecheckmodelMany(String value);

    /**
     * 修改记录
     *
     * @param rollDevicecheckmodel 对象实体
     */
    void updateDataRollDevicecheckmodel(RollDeviceCheckModel rollDevicecheckmodel);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollDeviceCheckModel> findDataRollDevicecheckmodelByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollDevicecheckmodelByPageSize();

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollDeviceCheckModel findDataRollDevicecheckmodelByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollDeviceCheckModel> findDataRollDevicecheckmodel();

}
