package com.my.business.modular.rolldevicecheckmodel.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rolldevicecheckmodel.entity.JRollDeviceCheckModel;
import com.my.business.modular.rolldevicecheckmodel.entity.RollDeviceCheckModel;
import com.my.business.modular.rolldevicecheckmodel.service.RollDevicecheckmodelService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;


/**
 * 点检模板数据表控制器层
 *
 * @author 生成器生成
 * @date 2020-07-14 16:08:24
 */
@RestController
@RequestMapping("/rollDevicecheckmodel")
public class RollDevicecheckmodelController {

    @Autowired
    private RollDevicecheckmodelService rollDevicecheckmodelService;

    /**
     * 添加记录
     *
     * @param loginToken 用户token
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollDevicecheckmodel(@RequestBody RollDeviceCheckModel rollDeviceCheckModel, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollDevicecheckmodelService.insertDataRollDevicecheckmodel(rollDeviceCheckModel, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollDevicecheckmodelOne(@RequestBody String data) {
        try {
            JRollDeviceCheckModel jrollDevicecheckmodel = JSON.parseObject(data, JRollDeviceCheckModel.class);
            return rollDevicecheckmodelService.deleteDataRollDevicecheckmodelOne(jrollDevicecheckmodel.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollDeviceCheckModel jrollDevicecheckmodel = JSON.parseObject(data, JRollDeviceCheckModel.class);
            return rollDevicecheckmodelService.deleteDataRollDevicecheckmodelMany(jrollDevicecheckmodel.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    @CrossOrigin
    @RequestMapping(value = {"/updateItems"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateItems(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollDevicecheckmodelService.updateItems(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollDevicecheckmodel(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollDevicecheckmodelService.updateDataRollDevicecheckmodel(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDevicecheckmodelByPage(@RequestBody String data) {
        return rollDevicecheckmodelService.findDataRollDevicecheckmodelByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDevicecheckmodelByIndocno(@RequestBody String data) {
        return rollDevicecheckmodelService.findDataRollDevicecheckmodelByIndocno(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param modelId 模型数据名称
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByModelId/{modelId}"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollDevicecheckmodelByModelId(@PathVariable("modelId") String modelId) {
        return rollDevicecheckmodelService.getCheckModelByModelId(modelId);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollDeviceCheckModel> findDataRollDevicecheckmodel() {
        return rollDevicecheckmodelService.findDataRollDevicecheckmodel();
    }

    /**
     * 粗轧工作辊上线下线轴承座点检表生成
     */
    @CrossOrigin
    @RequestMapping(value = {"/bearingExport"}, method = RequestMethod.GET)
    @ResponseBody
    public void exportBearing(HttpServletResponse response, HttpServletRequest request) throws Exception {
        HSSFWorkbook workbook = new HSSFWorkbook();
        String fileName = "粗轧工作辊轴承座点检表.xls";
        rollDevicecheckmodelService.exporBearingExcel(workbook);
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(fileName, "utf-8"));
        workbook.write(response.getOutputStream());
    }

    /***
     * 根据查询条件导出 点检表
     */
    @CrossOrigin
    @RequestMapping(value = {"/excel/{modelId}"}, method = RequestMethod.GET)
    @ResponseBody
    public void downloadAllClassmate(@PathVariable("modelId") String data, HttpServletResponse response, HttpServletRequest request) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        String fileName = "";//设置要导出的文件的名字
        fileName = rollDevicecheckmodelService.exportodelMExcel(data, fileName, workbook);
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("content-disposition", "attachment;filename=" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1) + ".xls");//URLEncoder.encode(fileName, "utf-8")
        response.setHeader("Access-Control-Expose-Headers", "content-disposition");
        workbook.write(response.getOutputStream());
    }
}
