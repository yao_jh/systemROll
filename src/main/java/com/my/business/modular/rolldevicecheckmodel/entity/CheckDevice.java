package com.my.business.modular.rolldevicecheckmodel.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class CheckDevice {

    // 点检备注
    private String description;
    // 操作员名称
    private String operatorName = "王英璨";
    // 点检设备名称
    private String deviceName;
    // 点检部位列表
    private List<CheckPart> checkParts;


}
