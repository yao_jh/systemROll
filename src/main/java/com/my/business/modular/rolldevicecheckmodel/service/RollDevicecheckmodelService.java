package com.my.business.modular.rolldevicecheckmodel.service;

import com.my.business.modular.rolldevicecheckmodel.entity.RollDeviceCheckModel;
import com.my.business.modular.rolldevicecheckschedule.entity.CheckTreeInfo;
import com.my.business.sys.common.entity.ResultData;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.List;

/**
 * 点检模板数据表接口服务类
 *
 * @author 生成器生成
 * @date 2020-07-14 16:08:24
 */
public interface RollDevicecheckmodelService {

    /**
     * 添加记录
     *
     * @param rollDeviceCheckModel 设备点检模型
     * @param userId               用户id
     * @param sname                用户姓名
     */
    ResultData insertDataRollDevicecheckmodel(RollDeviceCheckModel rollDeviceCheckModel, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollDevicecheckmodelOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollDevicecheckmodelMany(String str_id);

    /**
     * 根据modelId 获取其树形模型的数据
     *
     * @return
     */
    CheckTreeInfo findTreeInfoByIndocno(String modelId);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollDevicecheckmodel(String data, Long userId, String sname);

    /**
     * 修改点检模型数据
     *
     * @param data
     * @param userId
     * @param sname
     * @return
     */
    ResultData updateItems(String data, Long userId, String sname);

    /**
     * 根据id 更新点检结果
     * @param data
     * @param userId
     * @param sname
     * @return
     */
//	public ResultData updateCheckResult(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollDevicecheckmodelByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollDevicecheckmodelByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollDeviceCheckModel> findDataRollDevicecheckmodel();

    /**
     *
     */
    ResultData getCheckModelByModelId(String modelIdStr);

    /**
     * 导出数据到exceL
     */
    String exportodelMExcel(String modelId, String fileName, HSSFWorkbook workbook);

    /**
     * 导出上下线轴承座点检表
     */
    void exporBearingExcel(HSSFWorkbook workbook) throws Exception;

}
