package com.my.business.modular.rolldevicecheckmodel.entity;

import lombok.Data;

import java.util.List;

@Data
public class CheckPart {


    private String checkPartName;// 点检部位名称

    private Boolean isflag = false;// 是否是最底层

    private List<CheckItem> checkItemList;  // 点检部位信息
}
