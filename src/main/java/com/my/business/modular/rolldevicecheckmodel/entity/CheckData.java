package com.my.business.modular.rolldevicecheckmodel.entity;

import com.my.business.modular.rolldevicecheckschedule.entity.CheckDeviceInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class CheckData {

    // 点检时间
    private String checkDateStr;
    // 点检类型
    private String checkType;// 日/停机/年
    // 点检总负责人
    private String checkManagerName;
    // 点检list树形结构 ： 电气ELECTRIC
    private List<CheckDeviceInfo> checkDevicesElectricList;
    // 点检list树形结构 ： 机械MACHINE
    private List<CheckDeviceInfo> checkDevicesMachineList;

    public String getCheckDateStr() {
        return checkDateStr;
    }

    public void setCheckDateStr(String checkDateStr) {
        this.checkDateStr = checkDateStr;
    }

    public String getCheckType() {
        return checkType;
    }

    public void setCheckType(String checkType) {
        this.checkType = checkType;
    }

    public String getCheckManagerName() {
        return checkManagerName;
    }

    public void setCheckManagerName(String checkManagerName) {
        this.checkManagerName = checkManagerName;
    }

    public List<CheckDeviceInfo> getCheckDevicesElectricList() {
        return checkDevicesElectricList;
    }

    public void setCheckDevicesElectricList(List<CheckDeviceInfo> checkDevicesElectricList) {
        this.checkDevicesElectricList = checkDevicesElectricList;
    }

    public List<CheckDeviceInfo> getCheckDevicesMachineList() {
        return checkDevicesMachineList;
    }

    public void setCheckDevicesMachineList(List<CheckDeviceInfo> checkDevicesMachineList) {
        this.checkDevicesMachineList = checkDevicesMachineList;
    }
}
