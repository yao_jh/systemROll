package com.my.business.modular.rolldevicecheckmodel.entity;

import lombok.Data;

@Data
public class CheckItem {

    // 点检项目
    private String checkItem;
    // 点检标准
    private String checkSchedule;
    // 点检结果
    private String checkResult;
    // 异常描述
    private String description;

}
