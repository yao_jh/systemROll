package com.my.business.modular.rolldevicecheckmodel.entity;

import com.my.business.modular.rolldevicecheckschedule.entity.CheckDeviceInfo;
import com.my.business.sys.common.entity.JCommon;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 点检模板数据表json的实体类
 *
 * @author 生成器生成
 * @date 2020-07-14 16:08:24
 */
@Data
@EqualsAndHashCode
public class JRollDeviceCheckModel extends JCommon {

    private Integer pageIndex; // 第几页
    private Integer pageSize; // 每页多少数据
    private Object condition; // 查询条件
    private RollDeviceCheckModel rollDevicecheckmodel;   //对应模块的实体类
    private Long indocno;
    private String str_indocno;

    // 默认的集合
    private List<CheckDeviceInfo> defaultList;// model 数据模型

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Object getCondition() {
        return condition;
    }

    public void setCondition(Object condition) {
        this.condition = condition;
    }

    public RollDeviceCheckModel getRollDevicecheckmodel() {
        return rollDevicecheckmodel;
    }

    public void setRollDevicecheckmodel(RollDeviceCheckModel rollDevicecheckmodel) {
        this.rollDevicecheckmodel = rollDevicecheckmodel;
    }

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getStr_indocno() {
        return str_indocno;
    }

    public void setStr_indocno(String str_indocno) {
        this.str_indocno = str_indocno;
    }

    public List<CheckDeviceInfo> getDefaultList() {
        return defaultList;
    }

    public void setDefaultList(List<CheckDeviceInfo> defaultList) {
        this.defaultList = defaultList;
    }

}