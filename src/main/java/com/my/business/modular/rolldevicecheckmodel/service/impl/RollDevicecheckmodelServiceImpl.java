package com.my.business.modular.rolldevicecheckmodel.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rolldevicecheckmodel.dao.RollDevicecheckmodelDao;
import com.my.business.modular.rolldevicecheckmodel.entity.*;
import com.my.business.modular.rolldevicecheckmodel.service.RollDevicecheckmodelService;
import com.my.business.modular.rolldevicecheckschedule.entity.*;
import com.my.business.modular.rolldevicecheckschedule.service.RollDevicecheckscheduleService;
import com.my.business.modular.rolldevicescheck.dao.RollDevicescheckDao;
import com.my.business.modular.rolldevicescheck.entity.RollDevicescheck;
import com.my.business.modular.rolldevicescheckdict.dao.RollDevicescheckdictDao;
import com.my.business.modular.rolldevicescheckdict.entity.RollDevicescheckdict;
import com.my.business.modular.rolldevicescheckdict.service.RollDevicesCheckDictService;
import com.my.business.modular.rolldevicescheckpart.dao.RollDevicescheckpartDao;
import com.my.business.modular.rolldevicescheckpart.entity.RollDevicescheckpart;
import com.my.business.modular.rolldevicescheckpart.service.RollDevicesCheckPartService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import com.my.business.util.ExcelUtils;
import com.my.business.util.MathUtil;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 点检模板数据表接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-07-14 16:08:24
 */
@Service
public class RollDevicecheckmodelServiceImpl implements RollDevicecheckmodelService {

    // 设备点检中 点检人在第六列
    private static final Integer CHECK_OPERATION_NAME_LINE = 5;
    // 轴承座点检中 点检人在第四列
    private static final Integer CHECK_OPERATION_NAME_LINE_2 = 3;
    // 设备点检中 按照操作工的角色 分为电气和机械两种：
    private static final String CHECK_OPERATER_ELECTRIC = "电气";//电气
    private static final String CHECK_OPERATER_MACHINE = "机械";//机械
    private static final Integer CHECK_OPERATER_ELECTRIC_NUM = 0;
    private static final Integer CHECK_OPERATER_MACHINE_NUM = 1;
    // 设备点检类型： 日检 停机检 年检
    private static final String CHECK_TYPE_DAILY = "日检";//日检
    private static final String CHECK_TYPE_STOP = "停机检";//停机检
    private static final String CHECK_TYPE_YEAR = "年检";//年检
    private static final String CHECK_RESULT_NOT_CHECK = "2";// 点检结果: 以后再测
    @Autowired
    private RollDevicecheckmodelDao rollDevicecheckmodelDao;
    @Autowired
    private RollDevicesCheckDictService rollDevicesCheckDictService;
    @Autowired
    private RollDevicesCheckPartService rollDevicesCheckPartServices;
    @Autowired
    private RollDevicecheckscheduleService rollDevicecheckscheduleService;
    @Autowired
    private RollDevicescheckdictDao rollDevicescheckdictDao;
    @Autowired
    private RollDevicescheckpartDao rollDevicescheckpartDao;
    @Autowired
    private RollDevicescheckDao rollDevicescheckDao;

    /**
     * 根据对象属性去重  属性：indocno
     *
     * @param checkParts
     * @return
     */
    private static List<RollDevicescheckpart> removeDupliById(List<RollDevicescheckpart> checkParts) {
        Set<RollDevicescheckpart> checkPartSet = new TreeSet<>((o1, o2) -> o1.getIndocno().compareTo(o2.getIndocno()));
        checkPartSet.addAll(checkParts);
        return new ArrayList<>(checkPartSet);
    }

    /**
     * 添加 点检项模型数据
     *
     * @param rollDeviceCheckModel RollDeviceCheckModel
     * @param userId               用户id
     * @param sname                用户姓名
     */
    public ResultData insertDataRollDevicecheckmodel(RollDeviceCheckModel rollDeviceCheckModel, Long userId, String sname) {
        try {
            CodiUtil.newRecord(userId, sname, rollDeviceCheckModel);
            rollDevicecheckmodelDao.insertDataRollDevicecheckmodel(rollDeviceCheckModel);
            Long newModelId = rollDeviceCheckModel.getIndocno();
            return ResultData.ResultDataSuccessSelf("添加数据成功", newModelId);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollDevicecheckmodelOne(Long indocno) {
        try {
            rollDevicecheckmodelDao.deleteDataRollDevicecheckmodelOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollDevicecheckmodelMany(String str_id) {
        try {
            String sql = "delete roll_devicecheckmodel where indocno in(" + str_id + ")";
            rollDevicecheckmodelDao.deleteDataRollDevicecheckmodelMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData updateDataRollDevicecheckmodel(String data, Long userId, String sname) {
        try {
            JRollDeviceCheckModel jrollDevicecheckmodel = JSON.parseObject(data, JRollDeviceCheckModel.class);
            RollDeviceCheckModel rollDevicecheckmodel = jrollDevicecheckmodel.getRollDevicecheckmodel();
            CodiUtil.editRecord(userId, sname, rollDevicecheckmodel);
            rollDevicecheckmodelDao.updateDataRollDevicecheckmodel(rollDevicecheckmodel);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    @Override
    public ResultData updateItems(String data, Long userId, String sname) {
        JRollDeviceCheckModel jrollDevicecheckmodel = JSON.parseObject(data, JRollDeviceCheckModel.class);
        Long indocno = jrollDevicecheckmodel.getIndocno();
        List<CheckDeviceInfo> checkDeviceInfos = jrollDevicecheckmodel.getDefaultList();
        if (checkDeviceInfos == null || checkDeviceInfos.size() == 0) {
            return ResultData.ResultDataFaultSelf("数据为空，请检查数据", null);
        }
        List<ModelItem> modelItems = new ArrayList<ModelItem>();
        for (CheckDeviceInfo checkDeviceInfo : checkDeviceInfos) {
            List<CheckPartInfo> checkPartInfos = checkDeviceInfo.getChildren();
            if (checkPartInfos != null && checkPartInfos.size() != 0) {
                for (CheckPartInfo checkPartInfo : checkPartInfos) {
                    List<CheckItemInfo> checkItemInfos = checkPartInfo.getChildren();
                    for (CheckItemInfo checkItemInfo : checkItemInfos) {
                        ModelItem modelItem = new ModelItem();
                        String idStr = checkItemInfo.getId();
                        String checkResult = checkItemInfo.getCheckResult();
                        // TODO: 是否需要对未选择的点检结果做处理 当前都当作以后再测处理
                        // 获取拼装的 三级id 然后重新装配
                        String[] strArr = idStr.split("-");
                        if (strArr.length == 3) {
                            modelItem.setKey(Long.parseLong(strArr[2]));
                            modelItem.setValue(checkResult.equals("") ? CHECK_RESULT_NOT_CHECK : checkResult);
                            modelItems.add(modelItem);
                        }
                    }
                }
            }
        }

        // 根据indocno 找到对应的rollDevicecheckmodel
        RollDeviceCheckModel rollDeviceCheckModel = rollDevicecheckmodelDao.findDataRollDevicecheckmodelByIndocno(indocno);
        rollDeviceCheckModel.setCheck_items(JSON.toJSONString(modelItems));
        CodiUtil.editRecord(userId, sname, rollDeviceCheckModel);
        rollDevicecheckmodelDao.updateDataRollDevicecheckmodel(rollDeviceCheckModel);
        return ResultData.ResultDataSuccess("success");
    }

    private List<CheckDeviceInfo> getInfo(List<Long> checkDictList) {
        List<CheckDeviceInfo> checkDeviceInfos = new ArrayList<CheckDeviceInfo>();
        for (Long dictId : checkDictList) {
            RollDevicescheckdict rollDevicescheckdict = rollDevicescheckdictDao.findDataRollDevicescheckdictByIndocno(dictId);
            checkDeviceInfos = setCheckDataByRoleType(checkDeviceInfos, rollDevicescheckdict, dictId, "");
        }
        return checkDeviceInfos;
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollDevicecheckmodelByPage(String data) {
        try {
            JRollDeviceCheckModel jrollDevicecheckmodel = JSON.parseObject(data, JRollDeviceCheckModel.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollDevicecheckmodel.getPageIndex();
            Integer pageSize = jrollDevicecheckmodel.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollDevicecheckmodel.getCondition()) {
                jsonObject = JSON.parseObject(jrollDevicecheckmodel.getCondition().toString());
            }

            List<RollDeviceCheckModel> list = rollDevicecheckmodelDao.findDataRollDevicecheckmodelByPage(pageIndex, pageSize);
            Integer count = rollDevicecheckmodelDao.findDataRollDevicecheckmodelByPageSize();
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollDevicecheckmodelByIndocno(String data) {
        try {
            JRollDeviceCheckModel jrollDevicecheckmodel = JSON.parseObject(data, JRollDeviceCheckModel.class);
            Long indocno = jrollDevicecheckmodel.getIndocno();

            RollDeviceCheckModel rollDevicecheckmodel = rollDevicecheckmodelDao.findDataRollDevicecheckmodelByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollDevicecheckmodel);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollDeviceCheckModel> findDataRollDevicecheckmodel() {
        List<RollDeviceCheckModel> list = rollDevicecheckmodelDao.findDataRollDevicecheckmodel();
        return list;
    }

    /**
     * 根据modelId 获取点检数据
     */
    public ResultData getCheckModelByModelId(String modelIdStr) {

        // 根据modelId 获取设备和点检信息
        Long modeId = Long.parseLong(modelIdStr);
        RollDeviceCheckModel rollDevicecheckmodel = rollDevicecheckmodelDao.findDataRollDevicecheckmodelByIndocno(modeId);
        RollDevicecheckschedule rollDevicecheckschedule = rollDevicecheckscheduleService.findDataRollDevicecheckscheduleByModelId(modeId);
        String checkItems = rollDevicecheckmodel.getCheck_items();// 获取点检内容
        List<ModelItem> checkModelItems = JSON.parseArray(checkItems, ModelItem.class);
        List<Long> newCheckDictList = new ArrayList<Long>();
        // 取出item 中的key list(checkdictids) 重装checkModels
        if (checkModelItems == null || checkModelItems.size() == 0) {
            return ResultData.ResultDataFaultGd("未分配点检项目，请联系管理员", null);
        }
        // TODO：需要整改 根据modelId 直接获取到有value的数据 而不是只是key的集合

        // 装配CheckData
        CheckData checkData = new CheckData();
        /**
         * 第一层 device_id,checkDevice
         * 第二层 part_id,checkPart
         * 第三层 List<CheckItemInfo>
         */
        List<CheckDeviceInfo> checkDeviceElectricInfos = new ArrayList<CheckDeviceInfo>();
        List<CheckDeviceInfo> checkDeviceMachineInfos = new ArrayList<CheckDeviceInfo>();

        for (ModelItem modelItem : checkModelItems) {// 点检项id
            Long checkDictId = modelItem.getKey();
            RollDevicescheckdict rollDevicescheckdict = rollDevicescheckdictDao.findDataRollDevicescheckdictByIndocno(checkDictId);
            String checkResult = modelItem.getValue();
            // 筛选出来对应点检类型的数据
            if (rollDevicescheckdict.getCheck_role_type() == CHECK_OPERATER_ELECTRIC_NUM) {// 电气装配
                checkDeviceElectricInfos = setCheckDataByRoleType(checkDeviceElectricInfos, rollDevicescheckdict, checkDictId, checkResult);
            } else if (rollDevicescheckdict.getCheck_role_type() == CHECK_OPERATER_MACHINE_NUM) {// 机械装配
                checkDeviceMachineInfos = setCheckDataByRoleType(checkDeviceMachineInfos, rollDevicescheckdict, checkDictId, checkResult);
            } else {
                return ResultData.ResultDataFaultGd("装配类型有误", null);
            }
        }

        checkData.setCheckDevicesElectricList(checkDeviceElectricInfos);
        checkData.setCheckDevicesMachineList(checkDeviceMachineInfos);
        Date checkDate = rollDevicecheckschedule.getCheck_time(); // 获取点检时间
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
        String checkDateStr = sdf.format(checkDate);
        String checkManager = rollDevicecheckmodel.getCheck_manager_name(); // 点检负责人姓名
        String checkTypeStr = getCheckTypeStr(rollDevicecheckmodel.getCheck_type()); // 获取点检类型 日检/停机检/年检
        checkData.setCheckDateStr(checkDateStr); // 点检时间
        checkData.setCheckManagerName(checkManager); // 点检总负责人
        checkData.setCheckType(checkTypeStr); // 点检类型

        return ResultData.ResultDataSuccess(checkData);
    }

    private List<CheckDeviceInfo> setCheckDataByRoleType(List<CheckDeviceInfo> checkDeviceInfos, RollDevicescheckdict rollDevicescheckdict, Long checkDictId, String checkResult) {
        String checkDictName = rollDevicescheckdict.getCheck_content();
        String checkStandard = rollDevicescheckdict.getCheck_standard();
        Long checkPartId = rollDevicescheckdict.getCheck_part_id();
        RollDevicescheckpart rollDevicescheckpart = rollDevicescheckpartDao.findDataRollDevicescheckPartByIndocno(checkPartId);
        String checkPartName = rollDevicescheckpart.getDevice_part_name();
        Long deviceId = rollDevicescheckpart.getDevice_id();

        CheckDeviceInfo checkDeviceInfo = new CheckDeviceInfo();
        List<CheckPartInfo> checkPartInfos = new ArrayList<CheckPartInfo>();

        Map<Long, CheckDeviceInfo> checkDeviceInfoMap = checkDeviceInfos.stream().collect(Collectors.toMap(CheckDeviceInfo::getId, CheckDeviceInfo -> CheckDeviceInfo));
        if (checkDeviceInfoMap.containsKey(deviceId)) {
            checkDeviceInfo = checkDeviceInfoMap.get(deviceId);
            checkPartInfos = checkDeviceInfo.getChildren();
        } else {
            RollDevicescheck rollDevicescheck = rollDevicescheckDao.findDataRollDevicescheckByIndocno(deviceId);
            checkDeviceInfo.setId(deviceId);
            checkDeviceInfo.setName(rollDevicescheck.getDevicename());
            checkDeviceInfo.setDescription(rollDevicescheck.getDescription());
        }

        Map<String, CheckPartInfo> checkPartInfoMap = checkPartInfos.stream().collect(Collectors.toMap(CheckPartInfo::getId, CheckPartInfo -> CheckPartInfo));
        List<CheckItemInfo> checkItemInfos = new ArrayList<CheckItemInfo>();
        CheckPartInfo checkPartInfo = new CheckPartInfo();
        String partIdStr = deviceId + "-" + checkPartId;
        if (checkPartInfoMap.containsKey(partIdStr)) {
            checkPartInfo = checkPartInfoMap.get(partIdStr);
            checkItemInfos = checkPartInfo.getChildren();
        } else {
            checkPartInfo.setId(partIdStr);
            checkPartInfo.setName(checkPartName);
            checkPartInfo.setPid(deviceId);
        }

        CheckItemInfo checkItemInfo = new CheckItemInfo();
        String dictIdStr = partIdStr + "-" + checkDictId;
        checkItemInfo.setId(dictIdStr);
        String checkStateStr = rollDevicescheckdict.getCheck_state() == 0l ? "停机" : "运行";
        checkItemInfo.setName(checkDictName + "（" + checkStateStr + "）");
        checkItemInfo.setPid(partIdStr);
        checkItemInfo.setCheckResult(checkResult);
        checkItemInfo.setCheckSchedule(checkStandard);
        checkItemInfos.add(checkItemInfo);
        checkPartInfo.setChildren(checkItemInfos);

        checkPartInfoMap.put(deviceId + "-" + checkPartId, checkPartInfo);
        checkPartInfos = new ArrayList<CheckPartInfo>(checkPartInfoMap.values());
        checkDeviceInfo.setChildren(checkPartInfos);
        checkDeviceInfoMap.put(deviceId, checkDeviceInfo);
        checkDeviceInfos = new ArrayList<CheckDeviceInfo>(checkDeviceInfoMap.values());
        return checkDeviceInfos;
    }

    @Override
    public CheckTreeInfo findTreeInfoByIndocno(String checkModelIdStr) {
        Long checkModelId = Long.parseLong(checkModelIdStr);
        RollDeviceCheckModel rollDevicecheckmodel = rollDevicecheckmodelDao.findDataRollDevicecheckmodelByIndocno(checkModelId);
        String checkItems = rollDevicecheckmodel.getCheck_items();// 获取点检内容
        List<ModelItem> modelItems = JSON.parseArray(checkItems, ModelItem.class);
        if (modelItems == null || modelItems.size() == 0) {
            return null;// TODO：缺一个报错处理 考虑下以哪种形式
        }
        List<Long> checkDictList = new ArrayList<Long>();
        checkDictList = modelItems.stream().map(ModelItem::getKey).collect(Collectors.toList());

        List<RollDevicescheckdict> rollDevicescheckdicts = rollDevicescheckdictDao.findDataRollDevicescheckdict();
        List<Long> checkDictListAll = rollDevicescheckdicts.stream().map(RollDevicescheckdict::getIndocno).collect(Collectors.toList());
        checkDictListAll.removeAll(checkDictList);

        // 获取所有的dict集合 并求出和checkDictList 的差集
        CheckTreeInfo checkTreeInfo = new CheckTreeInfo();
        List<CheckDeviceInfo> defaultInfoList = getInfo(checkDictList);
        List<CheckDeviceInfo> remainList = getInfo(checkDictListAll);
        checkTreeInfo.setDefaultList(defaultInfoList);
        checkTreeInfo.setRemainList(remainList);
        return checkTreeInfo;
    }

    private String getCheckTypeStr(int checkType) {
        String checkTypeStr = null;
        switch (checkType) {
            case 0:
                checkTypeStr = CHECK_TYPE_DAILY;
                break;
            case 1:
                checkTypeStr = CHECK_TYPE_STOP;
                break;
            case 2:
                checkTypeStr = CHECK_TYPE_YEAR;
                break;
            default:
                break;
        }
        return checkTypeStr;
    }

    /**
     * 到处模型数据到exceL表
     */
    public String exportodelMExcel(String modelId, String fileName, HSSFWorkbook workbook) {
        // 点检类型分为电气 和 机械
        CheckData checkData = (CheckData) getCheckModelByModelId(modelId).getData();
        List<CheckDeviceInfo> checkDataElectric = checkData.getCheckDevicesElectricList();
        List<CheckDeviceInfo> checkDataMachine = checkData.getCheckDevicesMachineList();

        String checkManagerName = checkData.getCheckManagerName();// 点检负责人名称
        String checkTypeStr = checkData.getCheckType();// 检查类型（日检/停机检/年检）
        // 添加日期类型转换
        String checkCompleteTimeStr = checkData.getCheckDateStr();// 点检时间
        fileName = "磨辊间" + checkCompleteTimeStr + checkTypeStr + "汇总表";//设置要导出的文件的名字
        HSSFSheet sheetEle = workbook.createSheet("电气");   //第一页是电气
        HSSFSheet sheetMachine = workbook.createSheet("机械");     //第二页是机械

        getExcel(checkDataElectric, sheetEle, workbook, checkTypeStr);
        getExcel(checkDataMachine, sheetMachine, workbook, checkTypeStr);
        return fileName;
    }

    private void getExcel(List<CheckDeviceInfo> checkDevices, HSSFSheet sheet, HSSFWorkbook workbook, String checkTypeStr) {
        // 设置表头样式 居中 加粗 宋体 20
        HSSFCellStyle cellStyleHead = workbook.createCellStyle();
        cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
        HSSFFont redFontHead = workbook.createFont();
        redFontHead.setFontHeightInPoints((short) 20);//设置字体大小
        redFontHead.setFontName("宋体");//字体型号
        redFontHead.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//粗体显示
        cellStyleHead.setFont(redFontHead);

        // 设置点检人样式： 居中 宋体 18
        HSSFCellStyle cellStyleHead1 = workbook.createCellStyle();
        cellStyleHead1.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
        cellStyleHead1.setBorderBottom(HSSFCellStyle.BORDER_THIN); //下边框
        cellStyleHead1.setBorderLeft(HSSFCellStyle.BORDER_THIN);//左边框
        cellStyleHead1.setBorderTop(HSSFCellStyle.BORDER_THIN);//上边框
        cellStyleHead1.setBorderRight(HSSFCellStyle.BORDER_THIN);//右边框
        HSSFFont redFontHead1 = workbook.createFont();
        redFontHead1.setFontHeightInPoints((short) 18);//设置字体大小
        redFontHead1.setFontName("宋体");//字体
        cellStyleHead1.setFont(redFontHead1);

        //设置列标题样式 居中 宋体 加粗 各种边框 18
        HSSFCellStyle cellStyleBt = workbook.createCellStyle();
        cellStyleBt.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
        cellStyleBt.setBorderBottom(HSSFCellStyle.BORDER_THIN); //下边框
        cellStyleBt.setBorderLeft(HSSFCellStyle.BORDER_THIN);//左边框
        cellStyleBt.setBorderTop(HSSFCellStyle.BORDER_THIN);//上边框
        cellStyleBt.setBorderRight(HSSFCellStyle.BORDER_THIN);//右边框
        HSSFFont redFontBt = workbook.createFont();
        redFontBt.setFontHeightInPoints((short) 18);//设置字体大小
        redFontBt.setFontName("宋体");//字体
        redFontBt.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//粗体显示
        cellStyleBt.setFont(redFontBt);

        //设置普通行样式 顶端对齐 宋体 各种边框 18
        HSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER); // 垂直居中
        cellStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT); // 顶端对齐

        //设置样式对象，这里仅设置了边框属性
        cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN); //下边框
        cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);//左边框
        cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);//上边框
        cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);//右边框
        HSSFFont redFont = workbook.createFont();
        redFont.setFontHeightInPoints((short) 18);//设置字体大小
        redFont.setFontName("宋体");//字体
        cellStyle.setFont(redFont);

        int checkpointNum = 0;
        for (int i = 0; i < checkDevices.size(); i++) {
            CheckDeviceInfo checkDeviceInfo = checkDevices.get(i);
            String deviceName = checkDeviceInfo.getName();// 点检设备名称
            String operateName = checkDeviceInfo.getOperatorName();// 点检负责人姓名

            HSSFRow rowhead = sheet.createRow(0 + checkpointNum); //创建标题列
            checkpointNum = checkpointNum + 1;
            rowhead.setHeightInPoints(25);
            HSSFCell cell_00 = rowhead.createCell(0);   //创建第一行第一列
            cell_00.setCellStyle(cellStyleHead);
            cell_00.setCellValue(deviceName + checkTypeStr + "清单");
            HSSFCell cell_01 = rowhead.createCell(CHECK_OPERATION_NAME_LINE); // 点检人固定在第六列 column = 5
            cell_01.setCellStyle(cellStyleHead1);
            cell_01.setCellValue("点检人：" + operateName);
            // 合并单元格
            CellRangeAddress region = new CellRangeAddress(checkpointNum - 1, checkpointNum - 1, 0, 4);  //每个点检项目1,2,3合并单元格
            sheet.addMergedRegion(region);
            // 第二行是列标题
            HSSFRow row1 = sheet.createRow(checkpointNum);   //创建第二行
            checkpointNum = checkpointNum + 1;
            row1.setHeightInPoints(25);
            HSSFCell cell_10 = row1.createCell(0);
            HSSFCell cell_11 = row1.createCell(1);
            HSSFCell cell_12 = row1.createCell(2);
            HSSFCell cell_13 = row1.createCell(3);
            HSSFCell cell_14 = row1.createCell(4);
            HSSFCell cell_15 = row1.createCell(5);
            cell_10.setCellStyle(cellStyleBt);
            cell_11.setCellStyle(cellStyleBt);
            cell_12.setCellStyle(cellStyleBt);
            cell_13.setCellStyle(cellStyleBt);
            cell_14.setCellStyle(cellStyleBt);
            cell_15.setCellStyle(cellStyleBt);
            cell_10.setCellValue("序号");
            cell_11.setCellValue("点检部位");
            cell_12.setCellValue("点检项目");
            cell_13.setCellValue("点检标准");
            cell_14.setCellValue("点检结果（√/ⅹ)");
            cell_15.setCellValue("异常描述");

            int itemsIndoc = 1;
            List<CheckPartInfo> checkParts = checkDeviceInfo.getChildren();
            for (int a = 0; a < checkParts.size(); a++) {
                CheckPartInfo checkPart = checkParts.get(a);// 塞入的时候做判断 如果有点检项才插入
                String checkPartName = checkPart.getName();
                List<CheckItemInfo> checkItems = checkPart.getChildren();

                // 普通列装配
                for (int b = 0; b < checkItems.size(); b++) {
                    HSSFRow rowList = sheet.createRow(b + checkpointNum);
                    rowList.setHeightInPoints(25);// 设置高度为25
                    HSSFCell cellList_0 = rowList.createCell(0);// 序号
                    cellList_0.setCellStyle(cellStyle);
                    cellList_0.setCellValue(b + itemsIndoc);
                    HSSFCell cellList_1 = rowList.createCell(1);// 点检部位 合并单元格
                    cellList_1.setCellStyle(cellStyle);
                    cellList_1.setCellValue(checkPartName);
                    HSSFCell cellList_2 = rowList.createCell(2);// 点检项目
                    cellList_2.setCellStyle(cellStyle);
                    cellList_2.setCellValue(checkItems.get(b).getName());
                    HSSFCell cellList_3 = rowList.createCell(3);// 点检标准checkSchedule
                    cellList_3.setCellStyle(cellStyle);
                    cellList_3.setCellValue(checkItems.get(b).getCheckSchedule());
                    HSSFCell cellList_4 = rowList.createCell(4);// 点检结果
                    cellList_4.setCellStyle(cellStyle);
                    cellList_4.setCellValue("");
                    HSSFCell cellList_5 = rowList.createCell(5);// 点检结果 合并单元格
                    cellList_5.setCellStyle(cellStyle);// 异常描述
//					cellList_5.setCellValue(checkItems.get(b).getDescription()); 不需要插入 提供给点检操作人员填写
                }

                int itemsSize = checkItems.size();
                itemsIndoc = itemsIndoc + itemsSize;
                if (itemsSize >= 2) {// 超过两行才会合并单元格
                    CellRangeAddress region1 = new CellRangeAddress(checkpointNum, itemsSize + checkpointNum - 1, 1, 1);
                    CellRangeAddress region2 = new CellRangeAddress(checkpointNum, itemsSize + checkpointNum - 1, 5, 5);
                    sheet.addMergedRegion(region1);
                    sheet.addMergedRegion(region2);
                }
                checkpointNum = checkpointNum + itemsSize;
            }
        }

        // 分配列宽
        sheet.setColumnWidth(0, 20 * 100);
        sheet.setColumnWidth(1, 20 * 300);
        sheet.setColumnWidth(2, 20 * 600);
        sheet.setColumnWidth(3, 20 * 400);
        sheet.setColumnWidth(4, 20 * 400);
        sheet.setColumnWidth(5, 20 * 600);
    }

    public void exporBearingExcel(HSSFWorkbook workbook) throws Exception {

        Date checkDate = new Date();// 点检时间
        // 添加日期类型转换
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
        String checkTimeStr = sdf.format(checkDate);
        HSSFSheet sheet = workbook.createSheet("sheet1");   //创建sheet页名称

        BearingExport bearingExport = new BearingExport();
        bearingExport.setHeadlineName("粗轧工作辊上线轴承座点检表");
        bearingExport.setCheckManageName("赵家昌");
        List<String> items = new ArrayList<String>();
        items.add("锁紧板挡块是否异常");
        items.add("耐磨板螺丝是否异常");
        items.add("耐磨板磨损是否异常");
        items.add("横移架是否异常");
        items.add("操作侧定心螺丝是否异常");
        items.add("上下辊护板安装是否异常、胶木条是否紧靠轧辊,螺栓是否松动");
        items.add("防氧化铁皮板安装是否正常（与辊肩间隙不大于5mm)，螺栓是否松动");
        items.add("铜滑块是否需要更换");
        items.add("轴承润滑油嘴是否异常");
        items.add("轧辊辊身是否异常");
        items.add("其它是否异常");
        bearingExport.setCheckItems(items);
        bearingExport.setCheckDateStr(checkTimeStr);
        bearingExport.setCheckException("RW10012/10011检查，无异常。");
        bearingExport.setDescription("每次下机后磨削前、上机前进行各点检一次");

        BearingExport bearingExport1 = new BearingExport();
        bearingExport1.setHeadlineName("粗轧工作辊下线轴承座点检表");
        bearingExport1.setCheckManageName("赵家昌");
        bearingExport1.setCheckItems(items);
        bearingExport1.setCheckDateStr(checkTimeStr);
        bearingExport1.setCheckException("RW10012/10011检查，无异常。");
        bearingExport1.setDescription("每次下机后磨削前、上机前进行各点检一次");

        // 设置表头样式 居中 加粗 宋体 20
        HSSFCellStyle cellStyleHead = workbook.createCellStyle();
        cellStyleHead.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
        HSSFFont redFontHead = workbook.createFont();
        redFontHead.setFontHeightInPoints((short) 20);//设置字体大小
        redFontHead.setFontName("宋体");//字体型号
        redFontHead.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//粗体显示
        cellStyleHead.setFont(redFontHead);

        // 设置点检人样式： 居中 宋体 18
        HSSFCellStyle cellStyleHead1 = workbook.createCellStyle();
        cellStyleHead1.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
        cellStyleHead1.setBorderBottom(HSSFCellStyle.BORDER_THIN); //下边框
        cellStyleHead1.setBorderLeft(HSSFCellStyle.BORDER_THIN);//左边框
        cellStyleHead1.setBorderTop(HSSFCellStyle.BORDER_THIN);//上边框
        cellStyleHead1.setBorderRight(HSSFCellStyle.BORDER_THIN);//右边框
        HSSFFont redFontHead1 = workbook.createFont();
        redFontHead1.setFontHeightInPoints((short) 18);//设置字体大小
        redFontHead1.setFontName("宋体");//字体
        cellStyleHead1.setFont(redFontHead1);

        //设置列标题样式 居中 宋体 加粗 各种边框 18
        HSSFCellStyle cellStyleBt = workbook.createCellStyle();
        cellStyleBt.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
        cellStyleBt.setBorderBottom(HSSFCellStyle.BORDER_THIN); //下边框
        cellStyleBt.setBorderLeft(HSSFCellStyle.BORDER_THIN);//左边框
        cellStyleBt.setBorderTop(HSSFCellStyle.BORDER_THIN);//上边框
        cellStyleBt.setBorderRight(HSSFCellStyle.BORDER_THIN);//右边框
        HSSFFont redFontBt = workbook.createFont();
        redFontBt.setFontHeightInPoints((short) 18);//设置字体大小
        redFontBt.setFontName("宋体");//字体
        redFontBt.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//粗体显示
        cellStyleBt.setFont(redFontBt);

        //设置普通行样式 顶端对齐 宋体 各种边框 18
        HSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER); // 垂直居中
        cellStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT); // 顶端对齐
        cellStyle.setWrapText(true);// 设置自动换行
        //设置样式对象，这里仅设置了边框属性
        cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN); //下边框
        cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);//左边框
        cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);//上边框
        cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);//右边框
        HSSFFont redFont = workbook.createFont();
        redFont.setFontHeightInPoints((short) 18);//设置字体大小
        redFont.setFontName("宋体");//字体
        cellStyle.setFont(redFont);

        List<BearingExport> exports = new ArrayList<BearingExport>();
        exports.add(bearingExport);
        exports.add(bearingExport1);
        int checkpointNum = 0;
        for (BearingExport bearingExport2 : exports) {
            HSSFRow rowhead = sheet.createRow(0 + checkpointNum); //创建标题列
            checkpointNum = checkpointNum + 1;

            rowhead.setHeightInPoints(25);
            HSSFCell cell_00 = rowhead.createCell(0);   //创建第一行第一列
            cell_00.setCellStyle(cellStyleHead);
            cell_00.setCellValue(bearingExport.getHeadlineName());
            HSSFCell cell_01 = rowhead.createCell(CHECK_OPERATION_NAME_LINE_2); // 点检人固定在第四列
            cell_01.setCellStyle(cellStyleHead1);
            cell_01.setCellValue("点检人：" + bearingExport.getCheckManageName());
            // 合并单元格
            CellRangeAddress region = new CellRangeAddress(checkpointNum - 1, checkpointNum - 1, 0, 2);  //每个点检项目1,2,3合并单元格
            sheet.addMergedRegion(region);
            // 第二行是列标题
            HSSFRow row1 = sheet.createRow(checkpointNum);   //创建第二行
            row1.setHeightInPoints(25);
            HSSFCell cell_10 = row1.createCell(0);
            HSSFCell cell_11 = row1.createCell(1);
            HSSFCell cell_12 = row1.createCell(2);
            HSSFCell cell_13 = row1.createCell(3);
            cell_10.setCellStyle(cellStyleBt);
            cell_11.setCellStyle(cellStyleBt);
            cell_12.setCellStyle(cellStyleBt);
            cell_13.setCellStyle(cellStyleBt);
            cell_10.setCellValue("序号");
            cell_11.setCellValue("点检项目");
            cell_12.setCellValue("点检结果（√/ⅹ)");
            cell_13.setCellValue("异常描述");
            checkpointNum = checkpointNum + 1;

            List<String> itemNames = bearingExport2.getCheckItems();
            for (int a = 0; a < itemNames.size(); a++) {
                HSSFRow rowList = sheet.createRow(a + checkpointNum);

                HSSFCell cellList_0 = rowList.createCell(0);// 序号
                cellList_0.setCellStyle(cellStyle);
                cellList_0.setCellValue(a + 1);

                float height = ExcelUtils.getExcelCellAutoHeight(itemNames.get(a), 12f);
                rowList.setHeightInPoints(height);// 设置高度为25
                // 自动转行
                String itemValue = MathUtil.getStringByEnter(11, itemNames.get(a));
                HSSFCell cellList_1 = rowList.createCell(1);// 点检项目
                cellList_1.setCellStyle(cellStyle);
                cellList_1.setCellValue(itemValue);
                HSSFCell cellList_2 = rowList.createCell(2);// 点检结果
                cellList_2.setCellStyle(cellStyle);
                cellList_2.setCellValue(bearingExport2.getCheckResult());
                HSSFCell cellList_3 = rowList.createCell(3);// 异常描述
                cellList_3.setCellStyle(cellStyle);
                cellList_3.setCellValue(bearingExport2.getCheckException());
            }
            checkpointNum = checkpointNum + items.size();
            // 添加备注
            HSSFRow rowLast = sheet.createRow(checkpointNum);
            rowLast.setHeightInPoints(25);
            HSSFCell cellLast_0 = rowLast.createCell(0);
            HSSFCell cellLast_1 = rowLast.createCell(3);
            cellLast_0.setCellStyle(cellStyle);
            cellLast_0.setCellValue("备注：" + bearingExport2.getDescription());
            cellLast_1.setCellStyle(cellStyle);
            cellLast_1.setCellValue("点检日期：" + bearingExport2.getCheckDateStr());
            // 添加点检日期
            CellRangeAddress region0 = new CellRangeAddress(checkpointNum, checkpointNum, 0, 2);
            checkpointNum = checkpointNum + 1;
            CellRangeAddress region1 = new CellRangeAddress(checkpointNum - items.size() - 1, checkpointNum - 2, 3, 3);// 异常信息
            sheet.addMergedRegion(region0);
            sheet.addMergedRegion(region1);
        }
        sheet.setColumnWidth(0, 20 * 100);
        sheet.setColumnWidth(1, 20 * 500);
        sheet.setColumnWidth(2, 20 * 500);
        sheet.setColumnWidth(3, 20 * 610);
    }

}