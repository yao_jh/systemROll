package com.my.business.modular.rolldevicecheckmodel.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class BearingExport {
    // 标题名称
    private String headlineName;
    // 点检人名称
    private String checkManageName;
    // 点检项目
    private List<String> checkItems;
    // 点检结果（√/×）
    private String checkResult = "√";
    // 异常描述
    private String checkException;
    // 备注
    private String description;
    // 点检日期
    private String checkDateStr;

    public String getHeadlineName() {
        return headlineName;
    }

    public void setHeadlineName(String headlineName) {
        this.headlineName = headlineName;
    }

    public String getCheckManageName() {
        return checkManageName;
    }

    public void setCheckManageName(String checkManageName) {
        this.checkManageName = checkManageName;
    }

    public List<String> getCheckItems() {
        return checkItems;
    }

    public void setCheckItems(List<String> checkItems) {
        this.checkItems = checkItems;
    }

    public String getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult;
    }

    public String getCheckException() {
        return checkException;
    }

    public void setCheckException(String checkException) {
        this.checkException = checkException;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCheckDateStr() {
        return checkDateStr;
    }

    public void setCheckDateStr(String checkDateStr) {
        this.checkDateStr = checkDateStr;
    }
}
