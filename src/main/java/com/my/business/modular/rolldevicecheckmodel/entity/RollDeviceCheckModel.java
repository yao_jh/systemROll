package com.my.business.modular.rolldevicecheckmodel.entity;

import com.my.business.sys.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 点检模板数据表实体类
 *
 * @author 生成器生成
 * @date 2020-07-14 16:08:23
 */
@Data
@EqualsAndHashCode
public class RollDeviceCheckModel extends BaseEntity {

    private Long indocno;  //主键
    private Integer check_type;  //检查类型(0/1/2，日检/停机检/年检）
    private String check_items;  //设备检查项目列表，key为字典表id,value为检查状态（0通过，1未通过，2以后再测）
    private String check_manager_name;  //检测人
    private Long check_manager_id;  //检测人id
    private String snote;  //备注
    private String check_role_type;    //检测角色类型 0电气/1机械

    public Long getIndocno() {
        return indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public Integer getCheck_type() {
        return check_type;
    }

    public void setCheck_type(Integer check_type) {
        this.check_type = check_type;
    }

    public String getCheck_items() {
        return check_items;
    }

    public void setCheck_items(String check_items) {
        this.check_items = check_items;
    }

    public String getCheck_manager_name() {
        return check_manager_name;
    }

    public void setCheck_manager_name(String check_manager_name) {
        this.check_manager_name = check_manager_name;
    }

    public Long getCheck_manager_id() {
        return check_manager_id;
    }

    public void setCheck_manager_id(Long check_manager_id) {
        this.check_manager_id = check_manager_id;
    }

    public String getSnote() {
        return snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public String getCheck_role_type() {
        return check_role_type;
    }

    public void setCheck_role_type(String check_role_type) {
        this.check_role_type = check_role_type;
    }

}