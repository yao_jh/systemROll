package com.my.business.modular.rolldevicecheckmodel.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 点检工单模型
 */
@Data
@EqualsAndHashCode
public class ModelItem {

    // 检查项目id
    private Long key;
    // 检查项值 0通过/1未通过/2以后再测
    private String value;

    public Long getKey() {
        return key;
    }

    public void setKey(Long key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
