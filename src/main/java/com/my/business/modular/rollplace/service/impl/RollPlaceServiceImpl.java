package com.my.business.modular.rollplace.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.my.business.modular.rollplace.dao.RollPlaceDao;
import com.my.business.modular.rollplace.entity.JRollPlace;
import com.my.business.modular.rollplace.entity.RollPlace;
import com.my.business.modular.rollplace.service.RollPlaceService;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 场地信息接口具体实现类
 *
 * @author 生成器生成
 * @date 2020-05-20 09:15:24
 */
@Service
public class RollPlaceServiceImpl implements RollPlaceService {

    @Autowired
    private RollPlaceDao rollPlaceDao;

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    public ResultData insertDataRollPlace(String data, Long userId, String sname) {
        try {
            JRollPlace jrollPlace = JSON.parseObject(data, JRollPlace.class);
            RollPlace rollPlace = jrollPlace.getRollPlace();
            CodiUtil.newRecord(userId, sname, rollPlace);
            rollPlaceDao.insertDataRollPlace(rollPlace);
            return ResultData.ResultDataSuccessSelf("添加数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("添加失败，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    public ResultData deleteDataRollPlaceOne(Long indocno) {
        try {
            rollPlaceDao.deleteDataRollPlaceOne(indocno);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    public ResultData deleteDataRollPlaceMany(String str_id) {
        try {
            String sql = "delete roll_place where indocno in(" + str_id + ")";
            rollPlaceDao.deleteDataRollPlaceMany(sql);
            return ResultData.ResultDataSuccessSelf("删除数据数据成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("删除失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param userId 用户id
     * @param sname  用户姓名
     * @param data   sysUser 对象实体
     */
    public ResultData updateDataRollPlace(String data, Long userId, String sname) {
        try {
            JRollPlace jrollPlace = JSON.parseObject(data, JRollPlace.class);
            RollPlace rollPlace = jrollPlace.getRollPlace();
            CodiUtil.editRecord(userId, sname, rollPlace);
            rollPlaceDao.updateDataRollPlace(rollPlace);
            return ResultData.ResultDataSuccessSelf("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("修改失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPlaceByPage(String data) {
        try {
            JRollPlace jrollPlace = JSON.parseObject(data, JRollPlace.class);
            JSONObject jsonObject = null;
            Integer pageIndex = jrollPlace.getPageIndex();
            Integer pageSize = jrollPlace.getPageSize();

            if (null == pageIndex || null == pageSize) {
                return ResultData.ResultDataFaultSelf("分页参数没有传", null);
            }

            if (null != jrollPlace.getCondition()) {
                jsonObject = JSON.parseObject(jrollPlace.getCondition().toString());
            }

            String place_name = null;
            if (null != jsonObject.get("place_name")) {
                place_name = jsonObject.get("place_name").toString();
            }

            List<RollPlace> list = rollPlaceDao.findDataRollPlaceByPage(pageIndex, pageSize, place_name);
            Integer count = rollPlaceDao.findDataRollPlaceByPageSize(place_name);
            return ResultData.ResultDataSuccess(list, count);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data 分页参数字符串
     */
    public ResultData findDataRollPlaceByIndocno(String data) {
        try {
            JRollPlace jrollPlace = JSON.parseObject(data, JRollPlace.class);
            Long indocno = jrollPlace.getIndocno();

            RollPlace rollPlace = rollPlaceDao.findDataRollPlaceByIndocno(indocno);
            return ResultData.ResultDataSuccess(rollPlace);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("查询失败,失败信息为" + e.getMessage(), null);
        }
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    public List<RollPlace> findDataRollPlace() {
        List<RollPlace> list = rollPlaceDao.findDataRollPlace();
        return list;
    }

}
