package com.my.business.modular.rollplace.service;

import com.my.business.modular.rollplace.entity.RollPlace;
import com.my.business.sys.common.entity.ResultData;

import java.util.List;

/**
 * 场地信息接口服务类
 *
 * @author 生成器生成
 * @date 2020-05-20 09:15:24
 */
public interface RollPlaceService {

    /**
     * 添加记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData insertDataRollPlace(String data, Long userId, String sname);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    ResultData deleteDataRollPlaceOne(Long indocno);

    /**
     * 根据主键字符串删除多个对象
     *
     * @param str_id 对象主键字符串
     */
    ResultData deleteDataRollPlaceMany(String str_id);

    /**
     * 修改记录
     *
     * @param data   json字符串
     * @param userId 用户id
     * @param sname  用户姓名
     */
    ResultData updateDataRollPlace(String data, Long userId, String sname);

    /**
     * 分页查看记录
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPlaceByPage(String data);

    /**
     * 查看一条数据信息
     *
     * @param data 分页参数字符串
     */
    ResultData findDataRollPlaceByIndocno(String data);

    /**
     * 查看记录
     */
    List<RollPlace> findDataRollPlace();
}
