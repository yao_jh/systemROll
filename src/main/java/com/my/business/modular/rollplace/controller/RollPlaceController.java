package com.my.business.modular.rollplace.controller;

import com.alibaba.fastjson.JSON;
import com.my.business.modular.rollplace.entity.JRollPlace;
import com.my.business.modular.rollplace.entity.RollPlace;
import com.my.business.modular.rollplace.service.RollPlaceService;
import com.my.business.sys.common.entity.JCommon;
import com.my.business.sys.common.entity.ResultData;
import com.my.business.util.CodiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 场地信息控制器层
 *
 * @author 生成器生成
 * @date 2020-05-20 09:15:24
 */
@RestController
@RequestMapping("/rollPlace")
public class RollPlaceController {

    @Autowired
    private RollPlaceService rollPlaceService;

    /**
     * 添加记录
     *
     * @param data userId 用户id
     * @param data sname  用户姓名
     */
    @CrossOrigin
    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData insertDataRollPlace(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollPlaceService.insertDataRollPlace(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 根据主键删除对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteOne"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataRollPlaceOne(@RequestBody String data) {
        try {
            JRollPlace jrollPlace = JSON.parseObject(data, JRollPlace.class);
            return rollPlaceService.deleteDataRollPlaceOne(jrollPlace.getIndocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 根据主键字符串删除多个对象
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/deleteMany"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData deleteDataSysUserMany(@RequestBody String data) {
        try {
            JRollPlace jrollPlace = JSON.parseObject(data, JRollPlace.class);
            return rollPlaceService.deleteDataRollPlaceMany(jrollPlace.getStr_indocno());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.ResultDataFaultSelf("访问接口错误，错误信息为" + e.getMessage(), null);
        }
    }

    /**
     * 修改记录
     *
     * @param data       json字符串
     * @param loginToken 请求头参数字符串对象
     */
    @CrossOrigin
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData updateDataRollPlace(@RequestBody String data, @RequestHeader(value = "loginToken") String loginToken) {
        JCommon common = JSON.parseObject(loginToken, JCommon.class);
        String sname = common.getSname();
        Long userId = common.getUserId();
        return rollPlaceService.updateDataRollPlace(data, userId, CodiUtil.returnLm(sname));
    }

    /**
     * 分页查看记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByPage"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollPlaceByPage(@RequestBody String data) {
        return rollPlaceService.findDataRollPlaceByPage(data);
    }

    /**
     * 根据主键查询单条记录
     *
     * @param data json字符串
     */
    @CrossOrigin
    @RequestMapping(value = {"/findByIndocno"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultData findDataRollPlaceByIndocno(@RequestBody String data) {
        return rollPlaceService.findDataRollPlaceByIndocno(data);
    }

    /**
     * 查看记录
     *
     * @return list 对象集合返回
     */
    @CrossOrigin
    @RequestMapping(value = {"/findAll"}, method = RequestMethod.POST)
    public List<RollPlace> findDataRollPlace() {
        return rollPlaceService.findDataRollPlace();
    }

}
