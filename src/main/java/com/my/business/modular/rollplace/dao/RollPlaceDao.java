package com.my.business.modular.rollplace.dao;

import com.my.business.modular.rollplace.entity.RollPlace;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 场地信息dao接口
 *
 * @author 生成器生成
 * @date 2020-05-20 09:15:24
 */
@Mapper
public interface RollPlaceDao {

    /**
     * 添加记录
     *
     * @param rollPlace 对象实体
     */
    void insertDataRollPlace(RollPlace rollPlace);

    /**
     * 根据主键删除对象
     *
     * @param indocno 对象主键
     */
    void deleteDataRollPlaceOne(@Param("indocno") Long indocno);

    /**
     * 删除多个对象
     *
     * @param value sql语句
     */
    void deleteDataRollPlaceMany(String value);

    /**
     * 修改记录
     *
     * @param rollPlace 对象实体
     */
    void updateDataRollPlace(RollPlace rollPlace);


    /**
     * 分页查看查看记录
     *
     * @param pageIndex 第几页
     * @param pageSize  每页总数
     * @return 对象数据集合
     */
    List<RollPlace> findDataRollPlaceByPage(@Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("place_name") String place_name);

    /**
     * 根据条件查看记录的总数
     *
     * @return 对象数据集合
     */
    Integer findDataRollPlaceByPageSize(@Param("place_name") String place_name);

    /***
     * 根据主键查询信息
     * @param indocno 用户id
     * @return
     */
    RollPlace findDataRollPlaceByIndocno(@Param("indocno") Long indocno);

    /**
     * 查看记录
     *
     * @return 对象数据集合
     */
    List<RollPlace> findDataRollPlace();

}
