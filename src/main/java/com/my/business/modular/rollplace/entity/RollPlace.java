package com.my.business.modular.rollplace.entity;

import com.my.business.sys.common.entity.BaseEntity;

/**
 * 场地信息实体类
 *
 * @author 生成器生成
 * @date 2020-05-20 09:15:24
 */
public class RollPlace extends BaseEntity {

    private Long indocno;  //主键
    private String place_name;  //场地名称
    private Long iparent;  //父节点ID
    private String sparent;  //父节点名称
    private String snotes;  //备注

    public Long getIndocno() {
        return this.indocno;
    }

    public void setIndocno(Long indocno) {
        this.indocno = indocno;
    }

    public String getPlace_name() {
        return this.place_name;
    }

    public void setPlace_name(String place_name) {
        this.place_name = place_name;
    }

    public Long getIparent() {
        return this.iparent;
    }

    public void setIparent(Long iparent) {
        this.iparent = iparent;
    }

    public String getSparent() {
        return this.sparent;
    }

    public void setSparent(String sparent) {
        this.sparent = sparent;
    }

    public String getSnotes() {
        return this.snotes;
    }

    public void setSnotes(String snotes) {
        this.snotes = snotes;
    }


}